<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
  				<!-- 交易金额 -->
                <div class="row tile_count">
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-signal"></i> 交易笔数</span>
                        <div class="count">${statDealNum}</div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-rmb"></i> 交易总额</span>
                        <div class="count green">${statDealSumAmount}</div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-signal"></i> 昨日交易笔数</span>
                        <div class="count">${statDealNumYesterday}</div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-rmb"></i> 昨日交易金额</span>
                        <div class="count green">${statDealSumAmountYesterday}</div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-signal"></i> 交易商品总数量</span>
                        <div class="count">${statDealGoodsSum}</div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-signal"></i> 昨日交易商品总数量</span>
                        <div class="count">${statOrderGoodsSum}</div>
                    </div>
                </div>
                <!-- /交易金额 -->

                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="x_panel tile fixed_height_480">
                            <div class="x_title">
                                <h2>本月商品交易数量排行</h2>
<!--                                 <ul class="nav navbar-right panel_toolbox"> -->
<!--                                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> -->
<!--                                     </li> -->
<!--                                     <li><a class="close-link"><i class="fa fa-close"></i></a> -->
<!--                                     </li> -->
<!--                                 </ul> -->
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="echarts" style="width: 100%; height:400px;">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-12">
                        <div class="x_panel tile fixed_height_480">
                            <div class="x_title">
                                <h2>本月商品交易总额排行</h2>
<!--                                 <ul class="nav navbar-right panel_toolbox"> -->
<!--                                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> -->
<!--                                     </li> -->
<!--                                     <li><a class="close-link"><i class="fa fa-close"></i></a> -->
<!--                                     </li> -->
<!--                                 </ul> -->
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="echarts" style="width: 100%; height: 400px;">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
             
             
             	<style> 
                     #Validform_msg{display:none !important;} 
     			 </style>   
                
      <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementsByClassName('echarts')[0]);
        var myChart2 = echarts.init(document.getElementsByClassName('echarts')[1]);
        
           var mercollectdays = ${statQuantifySumList}; 
	       var statPricePaySumList =  ${statPricePaySumList};
	       console.log(mercollectdays);
	       console.log(statPricePaySumList);
	       
	            var googsName =[];
	            var googsNum = [];
	            
	            var arrayObj1 = [];
	            var arrayObj2 = [];
        
        for(var i=0;i<mercollectdays.length;i++){
        	googsName[i] = mercollectdays[i].googsName;
        	googsNum[i] = mercollectdays[i].quantifySum;
        }
        
        for(var i=0;i<statPricePaySumList.length;i++){
        	arrayObj1[i] = statPricePaySumList[i].googsName;
        	arrayObj2[i] = statPricePaySumList[i].pricePaySum;
        }
        
        
        option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['数量']
            },
            toolbox: {//加了它就能显示
		        show: true,
		        feature: {
		            magicType: {type: ['line', 'bar']},
		            saveAsImage: {}
		        }
		    },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [{
                type: 'category',
                boundaryGap: false,
                data:googsName
            }],
            yAxis: [{
                type: 'value'
            }],
            series: [{
                name: '数量',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    normal: {}
                },
                data:googsNum
            }]
        };
        
        option1 = {

                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['总额']
                },
                toolbox: {//加了它就能显示
    		        show: true,
    		        feature: {
    		            magicType: {type: ['line', 'bar']},
    		            saveAsImage: {}
    		        }
    		    },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: arrayObj1
                }],
                yAxis: [{
                    type: 'value'
                }],
                series: [{
                    name: '总额',
                    type: 'line',
                    stack: '总量',
                    areaStyle: {
                        normal: {}
                    },
                    data: arrayObj2
                }]
            };


        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        myChart2.setOption(option1);
        
        
    </script>
    
    
    <!--           toolbox: {//加了它就能显示 -->
<!--     		        show: true, -->
<!--     		        feature: { -->
<!--     		            dataZoom: { -->
<!--     		                yAxisIndex: 'none' -->
<!--     		            }, -->
<!--     		            dataView: {readOnly: false}, -->
<!--     		            magicType: {type: ['line', 'bar']}, -->
<!--     		            restore: {}, -->
<!--     		            saveAsImage: {} -->
<!--     		        } -->
<!--     		    }, -->
    
                