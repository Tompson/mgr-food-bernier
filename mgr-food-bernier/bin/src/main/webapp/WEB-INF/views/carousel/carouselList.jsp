<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
			<div class="form-inline">
				<form id="deliverMan_table_form">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="名称" name="name">
                    </div>
                     <div class="form-group">
                        <select name="type" class="form-control">
		                    <option value="">类型</option>
		                    <option value="1">优惠卷</option>
		                    <option value="2">其他</option>
                  		</select>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    <div class="form-group btn-group-vertical alignright">
                    <t:buttonOut url="/carousel/toCarouselAdd.html">
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="carouselFigureAdd()">添加轮播图</a>
                   </t:buttonOut>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="deliverMan_table"></table>
                </div>
                
                <style> /*写了个样式把Validform_msg强制掩藏了*/
                     #Validform_msg{display:none !important;} 
     			 </style>
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>


<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/carousel/page.html';  //TODO
var syslog_events = {
		'click input[type=checkbox]':function(e,value,row,index){
			console.log("row:"+row.status);//1启用2禁用
			var _this=this;
			if(row.status == 1){ //启用就禁用
					$.ajax({ 
	      				url: '<%=request.getContextPath()%>/carousel/disableState.html?id='+row.id, 
	      				context: document.body, 
	      				async: false, 
	      				success: function(vo){
		      				if(vo.success){
		      					layer.msg('操作成功!',{icon:1});
		      					$(_this).prop("checked",false);
		      					row.status=2;
		      				}else{
		      					layer.msg('操作失败!',{icon:5});
		      					$(_this).prop("checked",true);
		      					row.status=1;
		      				}
	      				}
	      			});
			}else{//禁用就启用
				$.ajax({
	    			url:'<%=request.getContextPath()%>/carousel/enableState.html?id='+row.id,
	    			context: document.body, 
	    			async:false,
	    			success: function(vo){
	      				if(vo.success){
	      					layer.msg('操作成功!',{icon:1});
	      					$(_this).prop("checked",true);
	      					row.status=1;
	      				}else{
	      					layer.msg('操作失败!',{icon:5});
	      					$(_this).prop("checked",false);
	      					row.status=2;
	      				}
	  				},
	  				error:function(vo){
	  					console.log(vo);
	  				}
	    		});
			}
		},
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/carousel/deleteCarouselFigure.html?id='+row.id, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#deliverMan_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .edit':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/carousel/tocarouselEdit.html?id='+row.id;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	},
    	'mouseenter .ewm-img':function(e,value,row,index){//鼠标悬停
    		console.log(1);
   		   var evm_offset=$(this).offset();
   	        $("<div class='img-da'><img src="+ $(this).attr("src")+" alt=''></div>").appendTo($("body"));
   	        $(".img-da").css({left:evm_offset.left+50,top:evm_offset.top-100})
    	},
    	'mouseleave .ewm-img':function(e,value,row,index){//鼠标离开
    		 $(".img-da").remove(); 
    	},
    	'click .upMove':function(e,value,row,index){//上移
     		layer.confirm('确定上移吗?', {btn: ['确定','取消']},  
         	  		function(){
         	  			$.ajax({ 
         	          		url: '<%=request.getContextPath()%>/carousel/upMove.html?id='+row.id,
         	          		context: document.body, 
         	          		async: false,
         	          	 	dataType:'json',
         	          		success: function(vo){
         	          			$('#goods_table').bootstrapTable('refresh', null);
         	          			if(vo.success){
         	          				layer.msg('上移成功!',{icon:1});
         	          			}else{
         	          				if(vo.statusCode == 1){
         	          					layer.msg(vo.message,{icon:5});
         	          				}else{
         	          					layer.msg('上移失败!',{icon:5});
         	          				}
         	          				
         	          			}
         	          		}
         	          	});
         	  		}
         	  	);
    	},
    	'click .downMove':function(e,value,row,index){//下移
     		layer.confirm('确定下移吗?', {btn: ['确定','取消']},  
         	  		function(){
         	  			$.ajax({ 
         	          		url: '<%=request.getContextPath()%>/carousel/downMove.html?id='+row.id,
         	          		context: document.body, 
         	          		async: false,
         	          	 	dataType:'json',
         	          		success: function(vo){
         	          			$('#goods_table').bootstrapTable('refresh', null);
         	          			if(vo.success){
         	          				layer.msg('下移成功!',{icon:1});
         	          			}else{
         	          				if(vo.statusCode == 1){
         	          					layer.msg(vo.message,{icon:5});
         	          				}else{
         	          					layer.msg('下移失败!',{icon:5});
         	          				}
         	          				
         	          			}
         	          		}
         	          	});
         	  		}
         	  	);
    	},
    	'click .goodsTop':function(e,value,row,index){//置顶
     		layer.confirm('确定置顶吗?', {btn: ['确定','取消']},  
         	  		function(){
         	  			$.ajax({ 
         	          		url: '<%=request.getContextPath()%>/carousel/goodsTop.html?id='+row.id,
         	          		context: document.body, 
         	          		async: false,
         	          	 	dataType:'json',
         	          		success: function(vo){
         	          			$('#goods_table').bootstrapTable('refresh', null);
         	          			if(vo.success){
         	          				layer.msg('置顶成功!',{icon:1});
         	          			}else{
         	          				if(vo.statusCode == 1){
         	          					layer.msg(vo.message,{icon:5});
         	          				}else{
         	          					layer.msg('置顶失败!',{icon:5});
         	          				}
         	          			}
         	          		}
         	          	});
         	  		}
         	  	);
    	}
    	
    	
};


var deliverMan_columns = [
{field: 'name',title: '名称',valign: 'top',sortable: false},
{field: 'photoUrl',title: '图片',valign: 'top',sortable: false,events:syslog_events,formatter:operateFormatter2},
{field: 'mark',title: '备注',valign: 'top',sortable: false},
{field: 'type',title: '类型',valign: 'top',sortable: false,formatter:operateFormatter5},
{field: 'createDate',title: '创建时间',valign: 'top',sortable: false,formatter:operateFormatter4},
{field: 'status',title: '禁用/启用',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter1},
{field: 'parentId',title: '排序',valign: 'top',sortable: false,events:syslog_events,formatter: operateFormatter6},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#deliverMan_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: deliverMan_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#deliverMan_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#deliverMan_table').bootstrapTable('refresh', null);
	}
	
	function operateFormatter2(value,row,index){
		console.log(value)
		if(!!!value){
			return ['<image  class="ewm-img" width="50" height="50" src="../images/upload.png" alt="" />'].join('');
		}else{
			return ['<image  class="ewm-img" width="50" height="50" src="'+value+'" alt="" />'].join('');
		}
		
	}
	function operateFormatter5(value,row,index){
		if(row.type==1){
			return "代金卷";
		}
		if(row.type==2){
			return "其他";
		}
	}
	function operateFormatter4(value,row,index){
		console.log(row.createDate)
		var date = new Date(row.createDate);
		var time1 = date.Format("yyyy-MM-dd hh:mm:ss");
		return time1;
	}
	//编辑和删除图标
	function operateFormatter(value,row,index){
		return ['<t:buttonOut url="/carousel/tocarouselEdit.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs edit">',
		'编辑','</a></t:buttonOut><t:buttonOut url="/carousel/deleteCarouselFigure.html"><button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button></t:buttonOut>'].join('');
	}
	function operateFormatter6(value,row,index){
		return ['<button type="button" class="btn  btn-link btn-xs upMove">',
		'上移','</button><button type="button" class="btn  btn-link btn-xs downMove">',
		'下移','</button><button type="button" class="btn  btn-link btn-xs goodsTop">',
		'置顶','</button>'].join('');
	}
	//启用和禁用图标
	 function operateFormatter1(value,row,index){
		if(row.status==1){//1启用 2停用     
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" checked="checked">'+
	 		'<span class="ace-tg"></span></div>';
		}
		if(row.status==2){
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" >'+
	 		'<span class="ace-tg"></span></div>';
		}
}
	//添加配送员
    function carouselFigureAdd(){
    	var url= '<%=request.getContextPath()%>/carousel/toCarouselAdd.html';
    	$("#admin_dialog_show").empty();
		$("#admin_dialog_show").load(url);//增加需要先清空再加载
		$("#admin_dialog").modal('show');
	  }
	
	
	

    
	
</script>