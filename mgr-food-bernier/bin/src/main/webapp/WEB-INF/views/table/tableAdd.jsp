<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 <script type="text/javascript">
	function statusChoice(){
		   if($("#status").is(":checked")){
			    $("#hiddenStatus0").val("1"); //启用
			}else{
				$("#hiddenStatus0").val("2"); //禁用
			}
	}
</script>  

 			<!--添加配送员-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">添加</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/table/tableAdd.html">
                    <div class="model-box">
                    	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"><span class="required red">*</span>门店名称：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
	                                <select name="storeId" id="userDj" onchange="" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填">
			                             <option  value="">请选择门店</option> 
			                             <c:forEach items="${stores}" var="item">
				                             	<option  value="${item.id}">${item.storeName}</option>
			                              </c:forEach>
	                         		</select>
                                <span class="col-xs-12 col-md-12  red  form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>餐桌名称：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="name" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                      
                        
                        
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
        </div>
    <!--/添加配送员-->

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('添加成功',{icon:1});    
    					$('#deliverMan_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    					console.log("dataURL:"+vo.data);
    				} else {
    					 layer.msg('添加失败',{icon:5});
    				}
    			}
            });
        });
    </script>
 