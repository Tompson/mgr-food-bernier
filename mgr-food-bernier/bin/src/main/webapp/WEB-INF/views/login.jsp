<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>饭好粥道</title>
    <link rel="shortcut icon" type="image/ico" href="<%=request.getContextPath()%>/images/logo.ico">
 	<link rel="apple-touch-icon-precomposed" href="<%=request.getContextPath()%>/images/logo.png">
    <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no">
   	<script src="<%=request.getContextPath()%>/js/lib/jquery.js" type="text/javascript"></script> 
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/ui/bootstrap/dist/css/bootstrap.min.css">
</head>
<style>
    html,
    body {
        width: 100%;
        height: 100%;
    }
    
    body {
        background: url(<%=request.getContextPath()%>/images/login-background.jpg) no-repeat center fixed;
        background-size: cover;
        position: relative;
    }
    
    .signin {
        width: 450px;
        height: 400px;
        position: absolute;
        background: rgba(255, 255, 255, .6);
        border-radius: 10px;
        left: 50%;
        margin-left: -225px;
        top: 50%;
        margin: -250px 0 0 -225px;
    }
    
    .sigin-tit {
        text-align: center;
        font-size: 30px;
        margin-top: 12px;
    }
    
    .sigin-tit img {
        width: 100px;
        height: 100px;
    }
    
    .sigin-tit small {
        display: block;
        line-height: 1;
        margin-top: 13px;
        letter-spacing: 3px;
    }
    
    .signin-form {
        padding: 32px 50px 0;
    }
    
    .signin-form .signin-txt,
    .signin-sub {
        width: 100%;
        padding-left: 10px;
        height: 40px;
        border: none;
        border-radius: 4px;
        font-size: 16px;
        margin-bottom: 20px;
    }
    
    .userName,
    .userPwd {
        position: relative;
    }
    
    .userName:before {
        content: "";
        width: 23px;
        height: 40px;
        position: absolute;
        top: 0;
        right: 5px;
        background: url(<%=request.getContextPath()%>/images/login_use.png) no-repeat 95%;
    }
    
    .userPwd:before {
        content: "";
        width: 23px;
        height: 40px;
        position: absolute;
        top: 0;
        right: 5px;
        background: url(<%=request.getContextPath()%>/images/login_pas.png) no-repeat 95%;
    }
    
    .signin-form .signin-name {
        background: #fff;
    }
    
    .signin-form .signin-pwd {
        background: #fff;
    }
    
    .signin-form .signin-check {
        width: 16px;
        height: 16px;
        margin-right: 5px;
        vertical-align: middle;
    }
    
    .signin-form .signin-word {
        color: #999;
        vertical-align: middle;
    }
    
    .signin-form .signin-sub {
        background: #337ab7;
        border: none;
        margin-top: 15px;
        height: 40px;
        font-size: 18px;
        color: #fff;
    }
</style>

<script type="text/javascript"> 
		document.onkeydown = function(event){ 
			e = event ? event :(window.event ? window.event : null); 
				if(e.keyCode==13){ 
					//执行的方法 
					if(""==$.trim($("#userId").val())){
						$("#errMsg").html("用户名为空");
						$("#userId").focus();
						return false;
					}else{
						$("#errMsg").html("");//如果不等于空字符串就清空提示信息(成功就清空提示信息)
					}
					
					if(""==$.trim($("#password").val())){
						$("#errMsg").html("密码为空");
						$("#password").focus();
						return false;
					}else{
						$("#errMsg").html("");//如果不等于空字符串就清空提示信息(成功就清空提示信息)
					}
					$("#loginForm").submit();
				} 
		};
			
		$(document).ready(function() {
			setFocus();
			loginValidate();
		});
		

		function setFocus() { 
			document.getElementById('userId').focus();
		}
		function loginValidate() {
			$("#loginBtn").click(function() {
				if ("" == $.trim($("#userId").val())) {
					$("#errMsg").html("用户名为空");
					$("#userId").focus();
					return false;
				}else{
					$("#errMsg").html("");//成功就清空提示信息
				}
				if ("" == $.trim($("#password").val())) {
					$("#errMsg").html("密码为空");
					$("#password").focus();
					return false;
				}else{
					$("#errMsg").html("");//成功就清空提示信息
				}
				$("#loginForm").submit();
			});
		}
</script>

<body>
    <div class="signin">  
        <h2 class="sigin-tit"><img src="<%=request.getContextPath()%>/images/logo.png" alt=""><small>饭好粥道</small></h2>
        <form class="signin-form" id="loginForm" method="post" action="<%=request.getContextPath()%>/system/login.html">
            <div class="userName">
            	<input class="signin-txt signin-name" maxlength="32" type="text"  id="userId" name="loginName" value="" placeholder="用户名">
            </div>
            <div class="userPwd">
            	<input class="signin-txt signin-pwd" maxlength="32" type="password" id="password" name="passwd" value="" placeholder="密码">
            </div>
             <div id="errMsg" class="errMsg">
					${errMsg}
	  		 </div>
            <input class="signin-sub" type="submit" id="loginBtn" name="" value="登录">
        </form>
    </div>
</body>


</html>