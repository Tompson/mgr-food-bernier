<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<script type="text/javascript">
function statusChoice(){
	   if($("#status").is(":checked")){
		   console.log(1);
		    $("#hiddenStatus0").val("1");
		}else{
			console.log(2);
			$("#hiddenStatus0").val("2");
		}
}

</script>
 			 			<!--添加配送员-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">编辑厨师</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/chief/chiefEdit.html">
                     <input type="hidden"  name="id" value="${chief.id }">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>姓名：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="name" value="${chief.name}" class="form-control col-md-7 col-xs-12" datatype="*1-32" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>电话：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" name="phone" value="${chief.phone}" class="form-control col-md-7 col-xs-12" datatype="m"  errormsg="请填写正确的手机号码!" sucmsg=" " nullmsg="必填" oninput="if(value.length>14)value=value.slice(0,14)" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">状态：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="ace-tab">
                                	<c:if test="${chief.status eq 1 }">
                                    	<input class="ace-ck" type="checkbox" id="status" checked="checked" onclick="statusChoice()">
                                    </c:if>
                                    <c:if test="${chief.status eq 2 }">
                                    	<input class="ace-ck" type="checkbox" id="status"  onclick="statusChoice()">
                                    </c:if>
                                    <span class="ace-tg"></span>
                                    <input type="hidden" id="hiddenStatus0" name="status" value="${chief.status}">
                                </div>
                            </div>
                        </div>
                        <c:if test="${empty store }">
                        	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"><span class="required red">*</span>所属门店：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
	                                <select name="storeId" id="userDj" onchange="" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填">
			                             <option  value="">请选择</option> 
			                             <c:forEach items="${stores}" var="item">
				                             	<option  value="${item.id}">${item.storeName}</option>
			                              </c:forEach>
	                         		</select>
                                <span class="col-xs-12 col-md-12  red  form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        </c:if>
                        <c:if test="${not empty store }">
                        	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"><span class="required red">*</span>所属门店：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
	                                <select name="storeId" id="userDj" onchange="" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填">
			                             <option  value="${store.id}">${store.storeName}</option> 
	                         		</select>
                                <span class="col-xs-12 col-md-12  red  form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        
                        </c:if>
                        
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
        </div>
    <!--/添加配送员-->

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('编辑成功',{icon:1});    
    					$('#deliverMan_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg('编辑失败',{icon:5});
    				}
    			}
            });
        });
    </script>