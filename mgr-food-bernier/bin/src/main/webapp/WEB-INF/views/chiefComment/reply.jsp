<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 <!--回复-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">评论回复</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" method="post" accept-charset="utf-8" action="<%=request.getContextPath()%>/orderComment/Reply.html" >
                	<input type="hidden"  name="cid" value="${orderComment.cid}">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">内容：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                               <textarea class="date-picker form-control col-md-7 col-xs-12" name="replyContent" datatype="*1-300" style="height:200px;" placeholder="请输入回复内容(内容不能多于300个字符)">${orderComment.replyContent}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
            </div>
    <!--/回复-->
    
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('回复成功',{icon:1});    
    					$('#orderComment_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg('回复失败',{icon:5});
    				}
    			}
            });
        });
    </script>