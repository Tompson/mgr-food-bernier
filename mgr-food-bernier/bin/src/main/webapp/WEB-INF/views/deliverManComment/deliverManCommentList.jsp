<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
                 <div class="form-inline">
                  <form id="orderComment_table_form">
                  	<div class="form-group">
                        <input type="text" class="form-control" placeholder="订单号" name="payNumber" >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="配送员名" name="deliverManName" >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="会员名" name="memberNick">
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="orderComment_table"></table>
                </div>
                
                <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>


<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/deliverManComment/page.html';  //TODO
var syslog_events = {
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/deliverManComment/deleteOrderComment.html?cid='+row.cid, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#orderComment_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .replyOrderComment':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/orderComment/toReply.html?cid='+row.cid;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	}
};


var orderComment_columns = [
{field: 'payNumber',title: '订单号',valign: 'top',sortable: false},
{field: 'deliverManName',title: '配送员名',valign: 'top',sortable: false},
{field: 'memberNick',title: '会员名',valign: 'top',sortable: false},
{field: 'deliverContent',title: '评论内容',valign: 'top',sortable: false,formatter:deliverContentFormatter},
{field: 'deliverGrade',title: '评论星级',valign: 'top',sortable: false},
{field: 'deliverSdate',title: '评论时间',valign: 'top',sortable: false},
// {field: 'replyContent',title: '评论回复',valign: 'top',sortable: false,formatter:replyContentFormatter},
// {field: 'parentId',title: '排序',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter1},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#orderComment_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: orderComment_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#orderComment_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#orderComment_table').bootstrapTable('refresh', null);
	}
	
	
	
	//回复和删除图标
	function operateFormatter(value,row,index){
		return ['<button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button>'].join('');
	}
	
	
	//上移和置顶
	 function operateFormatter1(value,row,index){
	   return '<t:buttonOut url="/deliverManComment/deleteOrderComment.html"><button type="button" class="btn btn-link btn-xs upMove">上移</button><button type="button" class="btn btn-link btn-xs commentTop" >置顶</button></t:buttonOut>' 
}
	
	function replyContentFormatter(value,row,index){
		if(value == null){
			return "无";
		}else{
			return value;
		}
		
	}
	
	function deliverContentFormatter(value,row,index){
		if(!!!value){//value 为0或者为null或者为""或者为undefined都为false
			return "无";
		}else{
			return value;
		}
		
	}

	
</script>