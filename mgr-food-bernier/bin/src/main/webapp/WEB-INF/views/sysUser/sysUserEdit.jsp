<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<script type="text/javascript">
function statusChoice(){
	   if($("#isinverck").is(":checked")){
		   console.log(1);
		    $("#isinventory").val("1");
		}else{
			console.log(2);
			$("#isinventory").val("2");
		}
}

</script>
 			<!--添加管理员-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">编辑管理员</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" method="post" accept-charset="utf-8" action="<%=request.getContextPath()%>/sysUser/sysUserEdit.html">
                   	  <input type="hidden"  name="uid" value="${sysUser.uid }">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>昵称：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"   name="userName" value="${sysUser.userName}" class="form-control col-md-7 col-xs-12" datatype="*1-7" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>电话：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="phone" value="${sysUser.phone}" class="form-control col-md-7 col-xs-12" datatype="m" oninput="if(value.length>14)value=value.slice(0,14)" errormsg="请填写正确的手机号码!" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))"  sucmsg=" " nullmsg="必填"  type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <!-- 当前点击的账号不等于admin并且登录的账号不等于当前点击的账号就显示-->
                        <c:if test="${sysUser.userAccount ne 'admin' && userInfo.userAccount ne sysUser.userAccount }"> 
                        	 <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12">状态：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                 <div class="ace-tab">
                                  <c:if test="${sysUser.status eq 1 }">
                                    	<input class="ace-ck" type="checkbox" checked="checked" id="isinverck" onclick="statusChoice()">
                                  </c:if>
                                   <c:if test="${sysUser.status eq 2 }">
                                    	<input class="ace-ck" type="checkbox" id="isinverck"   onclick="statusChoice()">
                                  </c:if>
                                    <span class="ace-tg"></span>
                                    <input type="hidden" name="status" id="isinventory" value="${sysUser.status}">
                                </div>
                            </div>
                        	</div>
                        </c:if>

                        
                        
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">
                 退出
              </button>
                    </div>
                </form>
            </div>
    <!--/添加管理员-->

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('编辑成功',{icon:1});    
    					$('#sysUser_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg(vo.message,{icon:5});
    				}
    			}
            });
        });
    </script>
 