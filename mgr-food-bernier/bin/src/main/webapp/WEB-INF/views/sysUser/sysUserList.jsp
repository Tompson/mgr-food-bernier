<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
			<div class="form-inline">
				<form id="sysUser_table_form">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="帐号" name="userAccount">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="昵称" name="userName">
                    </div>
                    <div class="form-group">
                        <select name="status" class="form-control">
		                    <option value="">状态</option>
		                    <option value="1">启用</option>
		                    <option value="2">禁用</option>
                  		</select>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    <div class="form-group btn-group-vertical alignright">
                        <t:buttonOut url="/sysUser/toSysUserAdd.html">
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="sysUserAdd()">添加管理员</a>
                        </t:buttonOut>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="sysUser_table"></table>
                </div>
                
                <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>


<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/sysUser/page.html';  //TODO
var syslog_events = {
		'click input[type=checkbox]':function(e,value,row,index){
			console.log("row:"+row.status);//1启用2禁用
			var _this=this;
			if(row.status == 1){ //启用    就禁用
					$.ajax({ 
	      				url: '<%=request.getContextPath()%>/sysUser/disableState.html?uid='+row.uid, 
	      				context: document.body, 
	      				async: false,
	      				success: function(vo){
		      				if(vo.success){
		      					layer.msg('成功!',{icon:1});
		      					$(_this).prop("checked",false);
		      					row.status=2;
		      				}else{
		      					layer.msg(vo.message,{icon:5});
		      					$(_this).prop("checked",true);
		      					row.status=1;
		      				}
	      				}
	      			});
			}else{//禁用   就启用
				$.ajax({
	    			url:'<%=request.getContextPath()%>/sysUser/enableState.html?uid='+row.uid,
	    			context: document.body, 
	    			async:false,
	    			success: function(vo){
	      				if(vo.success){
	      					layer.msg('成功!',{icon:1});
	      					$(_this).prop("checked",true);
	      					row.status=1; //1启用 2禁用
	      				}else{
	      					layer.msg(vo.message,{icon:5});
	      					$(_this).prop("checked",false);
	      					row.status=2;
	      				}
	  				},
	  				error:function(vo){
	  					console.log(vo);
	  				}
	    		});
			}
		},
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/sysUser/deleteSysUser.html?uid='+row.uid, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#sysUser_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg(vo.message,{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .edit':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/sysUser/toSysUserEdit.html?uid='+row.uid;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	},
    	'click .toAuthruzed':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/sysUser/toAuthruzedUser.html?uid='+row.uid;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	}
    	
};


var goods_columns = [
{field: 'userAccount',title: '账号',valign: 'top',sortable: false},
{field: 'userName',title: '昵称',valign: 'top',sortable: false},
{field: 'phone',title: '电话',valign: 'top',sortable: false},
{field: 'type',title: '账号类型',valign: 'top',sortable: false, formatter:typeFormatter},
{field: 'createTime',title: '创建时间',valign: 'top',sortable: false},
{field: 'status',title: '禁用/启用',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter1},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#sysUser_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: goods_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#sysUser_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#sysUser_table').bootstrapTable('refresh', null);
	}
	
	
	
	//编辑和删除图标
	function operateFormatter(value,row,index){
		return ['<t:buttonOut url="/sysUser/toSysUserEdit.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs edit">',
		'编辑','</a></t:buttonOut><t:buttonOut url="/sysUser/deleteSysUser.html"><button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button></t:buttonOut><t:buttonOut url="/sysUser/toAuthruzedUser.html"><button  class="btn  btn-danger btn-xs toAuthruzed">',
		'授权','</button></t:buttonOut>'].join('');
	}
	 

	//启用和禁用图标
	 function operateFormatter1(value,row,index){
		if(row.status==1){//1启用 2停用     
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" checked="checked">'+
	 		'<span class="ace-tg"></span></div>';
		}
		if(row.status==2){
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" >'+
	 		'<span class="ace-tg"></span></div>';
		}
		
}
//账号类型1平台管理员，2门店管理员，3门店普通员工',
 function typeFormatter(value,row,index){
	if(row.type==1){//1启用 2停用     
		return '平台管理员'
	}
	if(row.type==2){
		return '门店管理员';
	}
	if(row.type==3){
		return '门店普通员工';
	}
		
}
	 
	  //添加商品
    function sysUserAdd(){
    	var url= '<%=request.getContextPath()%>/sysUser/toSysUserAdd.html';
    	$("#admin_dialog_show").empty();
		$("#admin_dialog_show").load(url);//增加需要先清空再加载
		$("#admin_dialog").modal('show');
	  }
    
	
</script>