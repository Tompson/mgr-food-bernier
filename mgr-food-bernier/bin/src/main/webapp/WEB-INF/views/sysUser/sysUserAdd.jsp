<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 <script type="text/javascript">
	function statusChoice(){
		   if($("#status").is(":checked")){
			    $("#hiddenStatus0").val("1"); //启用
			}else{
				$("#hiddenStatus0").val("2"); //禁用
			}
	}
</script>  

 			<!--添加管理员-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">添加管理员</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/sysUser/sysUserAdd.html">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>帐号：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="userAccount" class="form-control col-md-7 col-xs-12" datatype="*1-18,uniquLoginName" maxlength="17" errormsg="后台用户账号已存在"  sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>密码：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="userPass" class="form-control col-md-7 col-xs-12" datatype="*6-32" errormsg="请填写6-32位的密码" sucmsg=" " nullmsg="必填" type="password">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>昵称：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="userName" class="form-control col-md-7 col-xs-12" datatype="*1-7" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>电话：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="phone" class="form-control col-md-7 col-xs-12" datatype="m"  sucmsg=" " errormsg="请填写正确的手机号码!"  nullmsg="必填" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" oninput="if(value.length>14)value=value.slice(0,14)" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">状态：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="ace-tab">
                                    <input class="ace-ck" type="checkbox" id="status" checked="checked" onclick="statusChoice()">
                                    <span class="ace-tg"></span>
                                    <input type="hidden" id="hiddenStatus0" name="status" value="1">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">
                 退出
              </button>
                    </div>
                </form>
        </div>
    <!--/添加管理员-->

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                datatype:{
  		    	  "uniquLoginName":function(gets,obj,curform,regxp){
  		    			/*参数gets是获取到的表单元素值，
  						  obj为当前表单元素，
  						  curform为当前验证的表单，
  						  regxp为内置的一些正则表达式的引用。*/
  		    			var falg = false;
  		            	$.ajax({ 
  		            		url: '<%=request.getContextPath()%>/sysUser/checkAccountName.html?userAccount='+gets, 
  		            		context: document.body, 
  		            		async: false,
  		            		success: function(date){
  		            			//console.log("The inspection results:"+date);
  		            			falg = date;
  		            		}
  		            	});
  		            	// console.log("The inspection results2:");
  		            	return falg;
  		    		}
  		    	},
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('添加成功',{icon:1});    
    					$('#sysUser_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg("添加失败!"+vo.message,{icon:5});
    				}
    			}
            });
        });
    </script>
 