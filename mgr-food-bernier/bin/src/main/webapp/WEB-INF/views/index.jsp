<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="/WEB-INF/views/include.jsp"%>
    
       <script type="text/javascript">
				function changeRightMenu(url){
					$("#bodyRight").empty();//清空indexmain.jsp
					$("#bodyRight").load(url);//重新加载右侧页面
					
					
					$.ajax({//每次调这个方法,就请求后台得到订单状态为未处理的总数(改为异步的)
		  				url: '<%=request.getContextPath()%>/system/getUntreatedOrderNum.html', 
		  				context: document.body, 
		  				async: true,
		  				dataType:'json',
		  				success: function(vo){
		      				if(vo.success){
		      					var html=$("#refreshMenu").html(vo.data);
		      				}else{
		      					layer.msg(vo.message,{icon:5});
		      				}
		  				}
		  			});
				}
				
				
				$(function(){//页面一加载事件
					changeRightMenu('<%=request.getContextPath()%>/system/indexmain.html')
				});	
	  </script>
</head>
<body class="nav-md">
    <div class="container body" >
        <div class="main_container" >
          <!--菜单  -->
               <%@ include file="/WEB-INF/views/menu.jsp"%>
          <!--/菜单  -->

          <!-- top navigation -->
              <%@ include file="/WEB-INF/views/header.jsp"%>
          <!-- /top navigation -->
          <!-- page 中间 -->
	          <div class="right_col" id="bodyRight">
	              <%@ include file="/WEB-INF/views/indexmain.jsp"%>
	          </div>
          <!-- /page 中间 -->

          <!-- footer content -->
             	  <%@ include file="/WEB-INF/views/footer.jsp"%>
          <!-- /footer content -->
        </div>
    </div>
    <!-- 消息推送页面 -->
    <%@ include file="/WEB-INF/views/showMsg.jsp"%>
</body>
</html>