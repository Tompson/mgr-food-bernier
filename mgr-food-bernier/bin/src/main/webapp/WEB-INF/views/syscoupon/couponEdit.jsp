<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">编辑代金券</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" method="post" accept-charset="utf-8" action="<%=request.getContextPath()%>/syscoupon/couponEdit.html">
                   	  <input type="hidden" id=id name="id" value="${sysCoupon.id }">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>金额：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input   name="mony" class="form-control col-md-7 col-xs-12" value='<fmt:formatNumber type="number" value="${sysCoupon.mony}" maxFractionDigits="2" groupingUsed="false" />'   datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" errormsg="金额最多六位,小数点后最多两位！"   sucmsg=" " nullmsg="必填" onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>使用下限：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="usefloor"  value="${sysCoupon.usefloor}"  class="form-control col-md-7 col-xs-12"  onkeyup="this.value=this.value.replace(/\D/g, '')" errormsg="只能输入数字" sucmsg=" " nullmsg="必填"  type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>数量：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input   name="remquantity" value="${sysCoupon.remquantity}"  class="form-control col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/\D/g, '')" errormsg="只能输入数字！" sucmsg=" " nullmsg="必填" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip" id="errormsgSpan"></span>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>开始时间：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  name="starttime" placeholder="选择时间"  value="${sysCoupon.starttime1}"  class="form-control" id="datestart"   errormsg="输入时间！" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"  id="errormsgSpan"  ></span>
                            </div>
                        </div>
                          <script type="text/javascript">
					                  var start = {
					                      elem: '#datestart',
					                      format: 'YYYY/MM/DD hh:mm:ss',
					                      min: laydate.now(0,"YYYY/MM/DD hh:mm"), //设定最小日期为当前日期
					                      max: '2099-06-16 23:59', //最大日期
					                      istime: true,
					                      istoday: false,
					                      choose: function(datas){
					                           end.min = datas; //开始日选好后，重置结束日的最小日期
					                           end.start = datas //将结束日的初始值设定为开始日
					                      }
					                  };
					                  laydate(start);
					       </script>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
            </div>

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                datatype:{
                	  "checkDrawProbability":function(gets,obj,curform,regxp){
  		    			/*参数gets是获取到的表单元素值，
  						  obj为当前表单元素，
  						  curform为当前验证的表单，
  						  regxp为内置的一些正则表达式的引用。*/
  		    			var falg = false;
  		            	$.ajax({ 
  		            		url: '<%=request.getContextPath()%>/voucher/checkDrawProbability.html?drawProbability='+gets+'&id='+$("#voucherId").val(), 
  		            		context: document.body, 
  		            		async: false,
  		            		success: function(date){
  		            			falg = date;
  		            			if(!falg){
  		            				setTimeout(function(){
	    		            				$("#errormsgSpan").html("概率总数不能超过100%");
  		            				}, 3);
  		            			}
  		            			
  		            		}
  		            	});
  		            	return falg;
  		            	
  		    		}
                },
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('编辑成功',{icon:1});    
    					$('#sysUser_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg('编辑失败',{icon:5});
    				}
    			}
            });
        });
    </script>
 