<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>

       <script type="text/javascript">

       function selectChart(){
          var querystarttime = $("#datestart").val();
          var queryendtime = $("#dateend").val();

           $.ajax({ 
                type: "POST",  
                traditional: true,
                 url: '<%=request.getContextPath()%>/sysUser/toChart.html',
                 data: {"querystarttime":querystarttime,"queryendtime":queryendtime},  
                 context: document.body, 
                 async: false,
                 cache:false,
                 success: function(data){

                 query('<%=request.getContextPath()%>/sysUser/toChart.html');
                 }
             });
       }

      
       </script>
    <script type="text/javascript">
       var myChart = echarts.init(document.getElementById('highcharts-cont'));
       
       var orderNocolor = {normal: {color: '#52ae57' } };
       var moneycolor = {normal: {color: '#00bbee' } };
       var mercollectdays =${dataList}; 
       
       var arrayObj =[];
       var orderNo =[];
       var money =[];
      for(var i = 0 ;i<mercollectdays.length;i++){
         arrayObj.push(mercollectdays[i].time);
         orderNo.push(mercollectdays[i].orderNo);
         money.push(mercollectdays[i].saleMoney);
      }

var option = {
    title: {
        text: '订单与销售额统计表'
       
    },
    tooltip: {
        trigger: 'axis',
        
    },
    toolbox: {
        show: true,
        feature: {
            dataZoom: {},
            dataView: {readOnly: false},
            magicType: {type: ['line', 'bar']},
            restore: {},
            saveAsImage: {}
        }
    },
    legend: {
        data:['订单数','销售额']
    },
    xAxis:  {
        type: 'category',
        boundaryGap: true,
        data: arrayObj
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: '{value} K'
        }
    },
    series: [
        {
            name:'订单数',
            type:'line',
            data:orderNo,
            itemStyle:orderNocolor,
        	symbol:'rect'
           
        },
        {
            name:'销售额',
            type:'line',
            data:money,
            itemStyle: moneycolor
            
           
        }
    ]
};
myChart.setOption(option);
       </script>
</head>
<body>

        <section class="main-cont">

     <form id="query_from">

	<%-- <div class="table-responsive">
       <table class="table table-hover" style="width:100%; ">
        <thead>
          <tr>
			<th width="12%" class="gren">微信笔数</th>
			<th width="12%" class="gren">微信金额</th>
			<th width="12%" class="gren">微信笔均值</th>
			<th width="12%" class="blue">支付宝笔数</th>
			<th width="12%" class="blue">支付宝金额</th>
			<th width="12%" class="blue">支付宝笔均值</th>
			<th width="12%" class="blue">总笔数</th>
			<th width="12%" class="blue">总金额</th>
          </tr>
        </thead>
        <tbody>
          <tr>
          
            <td class="gren">${sumWx }</td>
            <td class="gren" ><fmt:formatNumber type="number"  value="${sumwxMoney}" pattern="0.00" /> </td>
            <td class="gren">${wxPingjun }</td>
            
            <td class="blue">${sumAli }</td>
            <td class="blue"><fmt:formatNumber type="number"  value="${sumaliMoney}" pattern="0.00" /></td>
            <td class="blue">${aliPingjun }</td>
            <td class="red">${sumWx + sumAli}</td>
			<td class="red"><fmt:formatNumber type="number"  value="${sumwxMone+sumaliMoney}" pattern="0.00" /></td>
          </tr>
        </tbody>


       </table>
       </div> --%>
       <div id="highcharts-cont" style="width:100%;height: 500px"></div>
        </form>
        <div id="page_data"></div>    
        </section>

</body>
</html>