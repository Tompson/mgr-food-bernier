<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body>
	<div class="mt20">
				 <table data-toggle="table" class="table">
                        <thead>
                            <tr>
                                <th>菜品名称</th>
                                <th>菜品类型</th>
                                <th>价格（元）</th>
                                <th>星期菜品</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                          <c:forEach items="${page.results}" var="item">	
                            <tr>
                                <td>${item.goodsname}</td>
                                <td>${item.cateGoryGoodsName}</td>
                                <td>${item.price}</td>
                                <td>${item.weeksFoodStatus}</td>
                                <td>
                                    <div class="ace-tab">
                                        <input class="ace-ck" type="checkbox" name="" value="" checked="checked">
                                        <span class="ace-tg"></span>
                                    </div>
                                </td>
                                <td><a href="goods_edit.html" class="btn  btn-warning btn-xs">编辑</a><button type="button" class="btn  btn-danger btn-xs">删除</button></td>
                            </tr>
                           </c:forEach>
                        </tbody>
                    </table>
	</div>
<%-- 	<%@ include file="/WEB-INF/views/page.jsp"%> --%>
</body>
</html>