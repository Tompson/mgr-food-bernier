<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<style>
#printbody{ font-size:10px; background:#fff;}
#printbody p{margin-bottom:2px;}
#printbody th{font-size:10px;vertical-align:left;}
#printbody td{font-size:10px;}
</style>

    <div class="row" id="printbody">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h2 class="title tc">饭好粥道</h2>
              <table class="table">
                <tbody>
	                <tr><td>订单号：${sysOrder.payNumber}</td></tr>
	                 <tr><td>订单时间：<fmt:formatDate value="${sysOrder.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td></tr>
	                 <tr><td>订单来源：微信平台</td></tr>
	                 <tr><td>配送地址：${sysOrder.addresDetails}</td></tr>
	                 <tr><td>联系人：${sysOrder.contactsPerson}</td></tr>
	                 <tr><td>联系电话：${sysOrder.contactsPhone}</td></tr>
	                 <tr><td>送达时间：<fmt:formatDate value="${sysOrder.bookTime}" pattern="HH:mm"/></td></tr>
	                 <tr><td>备注：${sysOrder.remark}</td></tr>
                </tbody>
            </table>

<%--             <div class="clearfix"> <!-- 订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6订单取消 7配送员接单 8配送完成 9订单已评价 -->
                <p class="pull-left">订单状态：
	                <c:if test="${sysOrder.orderStatus eq 0}">未处理</c:if>
	                <c:if test="${sysOrder.orderStatus eq 1}">已确认</c:if>
	                <c:if test="${sysOrder.orderStatus eq 2}">备货完成</c:if>
	                <c:if test="${sysOrder.orderStatus eq 3}">配送中</c:if>
	                <c:if test="${sysOrder.orderStatus eq 4}">订单成功</c:if>
	                <c:if test="${sysOrder.orderStatus eq 5}">订单失败</c:if>
	                <c:if test="${sysOrder.orderStatus eq 6}">订单取消</c:if>
	                <c:if test="${sysOrder.orderStatus eq 7}">配送员接单</c:if>
	                <c:if test="${sysOrder.orderStatus eq 8}">配送完成</c:if>
	                <c:if test="${sysOrder.orderStatus eq 9}">订单已评价</c:if>
                </p>
            </div> --%>
          <%--   <div class="clearfix"> <!-- 支付方式 0未使用微信和支付宝支付，1支付宝，2微信支付，3现金支付，4余额支付 -->
                <p class="pull-left">支付方式：
                   <c:if test="${sysOrder.payType eq 0}">未使用微信和支付宝支付</c:if>
                   <c:if test="${sysOrder.payType eq 1}">支付宝</c:if>
                   <c:if test="${sysOrder.payType eq 2}">微信支付</c:if>
                   <c:if test="${sysOrder.payType eq 3}">现金支付</c:if>
                   <c:if test="${sysOrder.payType eq 3}">余额支付</c:if>
                </p>
            </div>
            <div class="clearfix"> <!-- 支付方式 0未使用微信和支付宝支付，1支付宝，2微信支付，3现金支付，4余额支付 -->
                <p class="pull-left">订单来源：
                	微信平台
                </p>
            </div>
            <div class="clearfix">
                <p class="pull-left">配送地址：
                	${sysOrder.addresDetails}
                </p>
            </div>
            <div class="clearfix">
                <p class="pull-left"></p>
                <p class="pull-right"></p>
            </div> --%>
            <table class="table">
                <thead>
                    <tr>
                        <th>菜品名</th>
                        <th>销售价格</th>
                        <th>数量</th>
                        <th>金额</th>
                    </tr>
                </thead>
                <tbody>
	                 <c:forEach items="${sysOrder.orderGoods}" var="item">
	                    <tr>
	                        <td>${item.googsName }</td>
	                        <td>${item.price }</td>
	                        <td>${item.quantify }</td>
	                        <td>${item.pricePay }</td>
	                    </tr>
	                   </c:forEach>
	                    <tr>
	                        <td>优惠价面额：</td>
	                        <td></td>
	                        <td></td>
	                        <td>${empty sysOrder.ouponValue ? 0.0 : sysOrder.ouponValue}</td> 
	                    </tr>
	                     <tr>
	                        <td>外卖费：</td>
	                        <td></td>
	                        <td></td>
	                        <td>${empty sysOrder.outsideMoney ? 0.0 : sysOrder.outsideMoney}</td>
	                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">合计：</td>
                        <td>${quantify}</td> <!-- 数量 -->
                        <td>${sysOrder.totalAmount }</td> <!-- 订单的总金额 -->
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
