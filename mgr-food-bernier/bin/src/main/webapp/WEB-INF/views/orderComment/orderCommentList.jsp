<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
                 <div class="form-inline">
                  <form id="orderComment_table_form">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="订单号" name="payNumber" >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="会员名" name="memberNick">
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="orderComment_table"></table>
                </div>
                
                <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>


<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/orderComment/page.html';  //TODO
var syslog_events = {
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/orderComment/deleteOrderComment.html?cid='+row.cid, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#orderComment_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .replyOrderComment':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/orderComment/toReply.html?cid='+row.cid;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	},
    	'click .upMove':function(e,value,row,index){
     		layer.confirm('确定上移吗?', {btn: ['确定','取消']},  
         	  		function(){
         	  			$.ajax({ 
         	          		url: '<%=request.getContextPath()%>/orderComment/upMove.html?cid='+row.cid, 
         	          		context: document.body, 
         	          		async: false,
         	          	 	dataType:'json',
         	          		success: function(vo){
         	          			$('#orderComment_table').bootstrapTable('refresh', null);
         	          			if(vo.success){
         	          				layer.msg('上移成功!',{icon:1});
         	          			}else{
         	          				if(vo.statusCode == 1){
         	          					layer.msg(vo.message,{icon:5});
         	          				}else{
         	          					layer.msg('上移失败!',{icon:5});
         	          				}
         	          				
         	          			}
         	          		}
         	          	});
         	  		}
         	  	);
    	},
    	'click .commentTop':function(e,value,row,index){
     		layer.confirm('确定置顶吗?', {btn: ['确定','取消']},  
         	  		function(){
         	  			$.ajax({ 
         	          		url: '<%=request.getContextPath()%>/orderComment/commentTop.html?cid='+row.cid, 
         	          		context: document.body, 
         	          		async: false,
         	          	 	dataType:'json',
         	          		success: function(vo){
         	          			$('#orderComment_table').bootstrapTable('refresh', null);
         	          			if(vo.success){
         	          				layer.msg('置顶成功!',{icon:1});
         	          			}else{
         	          				if(vo.statusCode == 1){
         	          					layer.msg(vo.message,{icon:5});
         	          				}else{
         	          					layer.msg('置顶失败!',{icon:5});
         	          				}
         	          			}
         	          		}
         	          	});
         	  		}
         	  	);
    	}
};


var orderComment_columns = [
{field: 'payNumber',title: '订单号',valign: 'top',sortable: false},
{field: 'orderGoods',title: '订单菜品',valign: 'top',sortable: false,formatter:orderGoodsFormatter},
{field: 'memberNick',title: '会员名',valign: 'top',sortable: false},
{field: 'content',title: '评论内容',valign: 'top',sortable: false,formatter:contentFormatter},
{field: 'commentGrade',title: '评论星级',valign: 'top',sortable: false},
{field: 'sdate',title: '评论时间',valign: 'top',sortable: false},
{field: 'replyContent',title: '评论回复',valign: 'top',sortable: false,formatter:replyContentFormatter},
{field: 'parentId',title: '排序',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter1},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#orderComment_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: orderComment_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#orderComment_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#orderComment_table').bootstrapTable('refresh', null);
	}
	
	
	
	//回复和删除图标
	function operateFormatter(value,row,index){
		return ['<t:buttonOut url="/orderComment/toReply.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs replyOrderComment">',
		'回复','</a></t:buttonOut><t:buttonOut url="/orderComment/deleteOrderComment.html"><button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button></t:buttonOut>'].join('');
	}
	
	function orderGoodsFormatter(value,row,index){
		if(value == null){
			return "无";
		}else{
			    var str = "";
			for(var i in value){
				 str += value[i].googsName+" ";
			}
			return str;
		}
	}
	
	//上移和置顶
	 function operateFormatter1(value,row,index){
	   return '<button type="button" class="btn btn-link btn-xs upMove">上移</button><button type="button" class="btn btn-link btn-xs commentTop" >置顶</button>' 
}
	
	function replyContentFormatter(value,row,index){
		
		
		if(value == null){
			return "无";
		}else{
			return value;
		}
		
	}
	
	function contentFormatter(value,row,index){
		if(!!!value){
			return "无";
		}else{
			return value;
		}
		
	}

	
</script>