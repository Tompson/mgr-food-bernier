<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">

	<head>
		<meta charset="utf-8" />
		<title>门店列表</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/common.css" rel="stylesheet" />
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/storelist.css" rel="stylesheet" />
		
		<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
		type="text/javascript"></script>
		<script
			src="<%=request.getContextPath()%>/h5/js/style.min.js"
			type="text/javascript"></script>
		 <script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/storelist.js"></script> 
		<script>
		var requestUrl="<%=request.getContextPath()%>";
		</script>
	</head>

	<body>
		<div class="wrap">
			 <header id="header" class="relative header-bg" style='color:#262722;'>
	            <a class="absolute back-blk" href="javascript:history.go(-1);" title="门店"></a>
	            <div class="tit tit-l">
	                	门店
	            </div>
	            <a class="absolute more-blk" href="javascript:;" title="门店"></a>
	         </header>
	         
        	<div class='search-wrap'>
        		<form class='search container' >
        			<input type="text" placeholder="请输入关键字搜索" id="search" />
        		</form>
        	</div>
        	
        	<section class='storelist clearfix'>
        		<div class='container clearfix'>
        			<ul class='storelist-wrap clearfix' id="teamList" pageNo="${page.pageNo} " pageSize="${page.pageSize}" totalPage="${page.totalPage}">
        			 <c:forEach items="${stores}" var="store">
	        			<li class='clearfix' value="${store.id}"> 
	        					<div class='fl storelist-pic'>
	        						<img src="http://d6.5deyi.com${store.storePhoto}" />
	        					</div>
	        					<div class='fl storelist-cont'>
	        						<div class='storelist-tit clearfix'>
	        							<h1 class='fl txt-ellipsis'>${store.storeName}</h1>
	        							<div class='fl txt-ellipsis'>
	        							 <fmt:formatNumber type="number" value="${store.distance/1000}"  pattern="0.0" maxFractionDigits="1"/>km
	        							</div>
	        						</div>
	        						<div>${store.contactPhone}</div>
	        						<p class="address">${store.address}</p>
	        						<div class='activity-cut'>
		        						<c:forEach items="${store.parts}" var="part">
		        							<c:if test="${part.type eq '1'}">
		        							满<fmt:formatNumber type="number" value="${part.full}"  pattern="0.0" maxFractionDigits="1"/>
			        						减<fmt:formatNumber type="number" value="${part.substact}"  pattern="0.0" maxFractionDigits="1"/>;
		        							</c:if>
		        						</c:forEach>		
		        					</div>
		        					<div class='activity-discount'>
		        						<c:forEach items="${store.parts}" var="part">
		        							<c:if test="${part.type eq '2'}">
		        								<fmt:formatNumber type="number" value="${part.discount}"  pattern="0.0" maxFractionDigits="1"/>折
		        							</c:if>
		        						</c:forEach>
		        					
		        					</div>
	        				  </div>
	        			</li>
        			
        			</c:forEach> 
        				
        				<!-- <li class='clearfix'> 
        					<div class='fl storelist-pic'>
        						<img src="images/storelist1.jpg" />
        					</div>
        					<div class='fl storelist-cont'>
        						<div class='storelist-tit clearfix'>
        							<h1 class='fl txt-ellipsis'>面将军</h1>
        							<div class='fl txt-ellipsis'>1.2km</div>
        						</div>
        						<p>深圳宝安西乡大道506</p>
        						<div class='activity-cut'>满10减5: 满50减10</div>
        						<div class='activity-discount'>7折</div>
        					</div>
        				</li>  -->
        			</ul>
        		</div>
        	</section>
	        
	        
			
        </div>
        
			
			
		</div>
		
		
		

		<script>
			var oHtml = document.getElementsByTagName('html')[0];
			var screenW = document.documentElement.offsetWidth || document.body.offsetWidth;
			oHtml.style.fontSize = 100 * screenW / 720 + "px";
		</script>
		
	</body>

</html>