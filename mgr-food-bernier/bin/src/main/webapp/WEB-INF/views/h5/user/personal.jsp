<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>
       饭好粥道-个人中心
    </title>
    <meta name="keywords" content="我的" />
    <meta name="description" content="我的" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <header id="header" class="header">
            <a class="back" href="index.html?v=1"></a>
            <div class="tit">
               个人中心
            </div>
        </header>
        <div class="user-top pd20 ">
            <img class="price" style="width:90px;height:90px;border-radius:45px" src="${member.profileImg }" alt="">
            <div class="name" style="margin-top:12%" >
                <span style="display:block;" >${member.memberNick }</span> 
                <span  style="">黄金会员</span>
            </div>
            
            
        </div>
        <div class="user-link">
            <div class="group">
                <a href="orderList.html?v=1"><em class="fr"></em><i class="u-icon u-icon6"></i><span>账户余额</span></a>
            </div>
            <div class="group">
                <a href="orderList.html?v=1"><em class="fr"></em><i class="u-icon u-icon3"></i><span>全部订单</span></a>
            </div>
            <div class="group">
                <a href="addressList.html?v=1"><em class="fr"></em><i class="u-icon u-icon1"></i><span>收货地址管理</span></a>
            </div>
            <div class="group">
                <a href="integral.html?v=1"><em class="fr"></em><i class="u-icon u-icon2"></i><span>积分</span></a>
            </div>
            <div class="group">
                <a href="voucher.html?v=1"><em class="fr"></em><i class="u-icon u-icon4"></i><span>代金券</span></a>
            </div>
            <div class="group">
                <a href="tel:${phone }"><em class="fr"></em><i class="u-icon u-icon5"></i><span>联系我们</span></a>
            </div>
        </div>
    </div>

</body>

</html>