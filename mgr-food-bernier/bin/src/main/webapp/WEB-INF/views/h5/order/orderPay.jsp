<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html lang="en">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <meta charset="utf-8" />
    <title>
       饭好粥道-订单支付
    </title>
    <meta name="keywords" content="支付方式" />
    <meta name="description" content="支付方式" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="viewport" content="initial-scale=1,width=device-width" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/mycss.css" />
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
</head>
<body>
    <div class="wrap">
        <!-- 头部 -->
        <header id="header" class="header">
            <a class="back" href="orderList.html?v=3" title="我的订单"></a>
            <div class="tit">
                	选择支付方式
            </div>
        </header>
        <!-- 评价 -->
        <section class="mt10">
            <div class="pay-box">
				<input type="hidden" class="order-pay-no"  value="${payno }"/>
				<input type="hidden" id="sessionMember"  value="${sessionScope.SESSION_WXOPENID }"/>
                <c:if test="${sessionScope.SESSION_WXOPENID!=null }">
                <div class="pay-group pd20 clear">
                    <div class="shop-group-cont ">
                        <p class="shop-group-wx" data-pay-way="wxpay">微信支付</p>
                    </div>
                </div>
                </c:if>
                <div class="pay-group pd20 clear ">
                    <div class="shop-group-cont shop-group-add">
                        <p class="shop-group-zfb" data-pay-way="alipay">支付宝支付</p>
                    </div>
                </div>
            </div>
            <div class="pay-btn pd20">
                <button type="button" class="order-pay">确认支付</button>
            </div>
        </section>

    </div>
    <!-- 支付宝支付提示 -->
	<div class="wxtip" id="JweixinTip">
			<span class="wxtip-icon"></span>
			<p class="wxtip-txt">
				支付宝支付<br />请点击右上角<br />选择在浏览器中打开！！！
			</p>
		</div>
    <script src="<%=request.getContextPath() %>/h5/js/lib/zepto.min.js"></script>
   	<script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
   	<script src="<%=request.getContextPath() %>/h5/js/basicpay.js"></script>
   	<script src="<%=request.getContextPath() %>/h5/js/view/orderPay.js"></script>
</body>

</html>