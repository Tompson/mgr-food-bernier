<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>饭好粥道-今日菜品</title>
<meta name="keywords" content="首页" />
<meta name="description" content="首页" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/details.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />

<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"
	type="text/javascript"></script>
</head>

<body>
	<div class="wrap">
		<header id="header" class="header">
			<a class="back" href="index.html?v=1"></a>
			<div class="tit">今日菜品</div>
			<a href="personal.html?v=3" class="vedio" style="line-height: 1rem;color: #fff;"><img src="<%=request.getContextPath()%>/h5/images/wode.png" class="header-vides" style="width: .45rem; height: .45rem;" alt="">&nbsp;我的</a>
		</header>
		<!-- 轮播 -->
		<div class="slider">
			<ul class="silder-list">
				<c:forEach items="${adList }" var="ad">
					<li><a href="${ad.link }"> <img src="${ad.img }" alt="" /></a></li>
				</c:forEach>
			</ul>
<!-- 			<a class="weekly" href="productOfWeek.html">每周菜品展示</a> -->
		</div>
		<!-- 导航条，按菜品分类跳锚点 -->
		<section class="plot">
		<c:forEach var="category" items="${categoryList }">
            <p class="plot-item" style=""><a href="#category${category.categoryId }">我要点：${category.cgoodsName }</a></p>
        </c:forEach>
        </section>
		<!-- 增加两个按钮 -->
		<div class="head-desc" >
			<div class="btn btn-yellow btn-desc fl" id="groupDesc">
				<div class="ftsz30"  >团购团餐说明</div>
				<div>(多人购折上折)</div>
			</div>
			<div class="btn btn-yellow btn-desc fr" id="goodsWeek">
				<div class="ftsz30" >本周菜品预定</div>
				<div >(提前预定获代金券)</div>
			</div>
		</div>
		
		<!-- 菜品展示 -->
		<input type="hidden" value="${page.params.weeksFoodStatus }" id="current-week"/>
		<section class="content mt10 goodslist" id="goodsList" data-book-type="${sessionScope.book_type.bookType }"
			style="padding-bottom: 1.1rem;"
			data-page-no="${page.pageNo }"
			data-page-size="${page.pageSize }"
			data-total-page="${page.totalPage }"
		>
			<c:forEach var="category" items="${categoryList }">
				<c:if test="${category!=null }">
					<p class="mt10 category-goods"><a name="category${category.categoryId }">${category.cgoodsName }</a></p>
				</c:if>
				<c:forEach items="${page.results }" var="goodsVo">
					<c:if test="${category.cgoodsName==goodsVo.cateGoryGoodsName }">
						<div class="h-item">
							<div class="picture">
								<a href="productDetail.html?goodsId=${goodsVo.id }">
									<img src="${goodsVo.goodsPic }" alt="">
									<c:if test="${goodsVo.status==0}">
										<p class="mark service" data-goods-status="${goodsVo.status}">在售</p>
									</c:if>
									<c:if test="${goodsVo.status==1}">
										<p class="mark forbidden" data-goods-status="${goodsVo.status}">已售完</p>
									</c:if>
								</a>
								<p class="reco">${goodsVo.intro }</p>
							</div>
							<div class="desc">
								<h6 class="tit">${goodsVo.goodsname }</h6>
								<jsp:include page="../grade.jsp" flush="true">
									<jsp:param value="${goodsVo.averageScore  }" name="averageGrades" />
								</jsp:include>
								<p class="sale">销量${goodsVo.sellnum }份</p>
								<p class="price">￥${goodsVo.price }</p>
								<p class="price_to">原价：￥${goodsVo.unit }</p>
								<div class="shop add-2-cart" data-goods-id="${goodsVo.id }"></div>
							</div>
						</div>
					</c:if>
				</c:forEach>
			</c:forEach>
		</section>
		<!-- 结算 -->
		<div class="stms">
			<div class="stms-shop clear">
				<a class="shop fl" href="shopTrolley.html?v=1"> </a>
				<div class="money fl">
					￥<span id="amountPrice">${amountPrice }</span>
				</div>
			</div>
			<a class="btn stms-btn btn-yellow" id="toShopTrolley" href="#">选好了</a>
		</div>
	</div>
<%-- 	<c:if test="${sessionScope.book_type==null }"> --%>
	<!-- <div class="seldate hd">
		<div class="seldate-box">
			<h5 class="tit">选择您的订购</h5>
			<button class="seldate-btn btn btn-yellow hd" id="noon" type="button" data-book-type="1">午餐订购</button>
			<button class="seldate-btn btn btn-yellow hd" id="booknight" data-book-type="4"
				type="button">晚餐预定</button>
			<button class="seldate-btn btn btn-yellow hd" id="night" data-book-type="2"
				type="button">晚餐订购</button>
			<button class="seldate-btn btn btn-yellow hd" id="booktomorrow" data-book-type="3"
				type="button">明天午餐预订</button>
		</div>
	</div> -->
<%-- 	</c:if> --%>
	<script
		src="<%=request.getContextPath()%>/h5/js/ui/silder.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/goodsCommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/productHome.js"></script>
</body>

</html>