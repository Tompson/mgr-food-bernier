<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en" xmlns:wb="http://open.weibo.com/wb">

<head>
<meta charset="utf-8" />
<title>饭好粥道-菜品详情</title>
<!-- <meta name="keywords" content="菜品详情" /> -->
<!-- <meta name="description" content="菜品详情" /> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/details.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
<!-- <script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js?appkey=YOUR APPKEY&debug=true" type="text/javascript" charset="utf-8"></script> -->
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"
	type="text/javascript" charset="utf-8"></script>
</head>
<body style="height:100%">
	<div class="wrap">
		<header id="header" class="header">
			<a class="back" href="javascript:history.go(-1);"></a>
            <div class="tit">${goods.goodsname }</div>
            <a href="index.html?v=1" class="vedio" style="line-height: 1rem;"><img src="<%=request.getContextPath()%>/h5//images/videos.png" class="header-vides" style="width: .35rem; height: .35rem;" alt="">厨房直播</a>
		</header>
		<!-- 图片 -->
		<div class="dtop">
			<img class="photos" id="imgUrl" src="${serverUrl }${goods.goodsPic }" alt="">
		</div>
		<!-- 推荐 -->
			<input type="hidden" id="currentWeek" value="${currentDayOfWeek }" /> 
			<input type="hidden" id="week" value="${goods.weeksFoodStatus }" /> 
			<input type="hidden" id="goodsStatus" value="${goods.status }" /> 
			<input type="hidden" id="goodsId" value="${goods.id }" />
			<input type="hidden" id="goodsName" value="${goods.goodsname }" />
			<input type="hidden" id="intro" value="${goods.intro }" />
		<section class="remd">
            <div class="price"><a class="py" href="#">推荐给朋友<br><em>(获得积分抽奖)</em></a><span class="money">￥${goods.price }</span></div>
            <a class="remd-btn btn btn-yellow add-2-cart" id="addToCart" data-goods-id="${goods.id }" href="javascript:void(0);">加入购物车</a>
            <a class="remd-btn btn btn-red"  id="immediatelyBuy" href="javascript:void(0);">立即购买</a>
        </section>
        <section class="remd">
            <div class="price">原价：<span style="text-decoration: line-through;">￥${goods.unit }</span></div>
        </section>
        	
		<!-- 详情 -->
		<article class="article">${goods.description }</article>
		<!-- 评价 -->
		<section class="comment" id="commentList"
			data-page-no="${page.pageNo }"
			data-page-size="${page.pageSize }"
			data-total-page="${page.totalPage }"
		>
			<c:forEach items="${commentVoList }" var="commentVo">
				<dl class="item pd20">
					<dt class="top clear">
						<div class="fl">
							<span class="phone">${commentVo.memberNick }</span>
						</div>
						<div class="fr">
							<span class="date">${commentVo.formatSdate }</span>
						</div>
					</dt>
					<dd class="info">
						<jsp:include page="../grade.jsp" flush="true">
							<jsp:param value="${commentVo.commentGrade  }"
								name="averageGrades" />
						</jsp:include>
						<div class="desc">
							<p>${commentVo.content }</p>
						</div>
						<c:if test="${commentVo.replyContent!=null }">
							<div class="reply mt10">
								<em class="color-ff5000">店家回复：</em><span>${commentVo.replyContent}</span>
							</div>
						</c:if>
					</dd>
				</dl>
			</c:forEach>
		</section>
		<!-- 分享 -->
		<div class="wxtip" id="JweixinTip">
			<span class="wxtip-icon"></span>
			<p class="wxtip-txt">
				请点击右上角<br />来进行分享！！！
			</p>
		</div>
	</div>
	<script
		src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/h5/js/goodsCommon.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/h5/js/view/productDetail.js"></script>	
</body>

</html>