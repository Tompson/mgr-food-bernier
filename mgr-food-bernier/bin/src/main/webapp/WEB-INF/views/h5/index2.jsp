<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>饭好粥道-今日菜品</title>
<meta name="keywords" content="首页" />
<meta name="description" content="首页" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/details.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />

<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"
	type="text/javascript"></script>
</head>

<body class="wovd">
	<div class="wrap">
		<!-- 轮播 -->
		<div class="slider">
			<ul class="silder-list">
				<c:forEach items="${adList }" var="ad">
					<li><a href="${ad.link }"> <img src="${ad.img }" alt="" /></a></li>
				</c:forEach>
			</ul>
<!-- 			<a class="weekly" href="productOfWeek.html">每周菜品展示</a> -->
		</div>
		<!-- 导航条，按菜品分类跳锚点 -->
		<section class="plot">
		
        </section>
		
		
	
		
	</div>
	<script
		src="<%=request.getContextPath()%>/h5/js/ui/silder.min.js"></script>
	
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/goodsCommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/productHome.js"></script>
</body>

</html>