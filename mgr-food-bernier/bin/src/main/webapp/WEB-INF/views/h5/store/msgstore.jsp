<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title>门店详情</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link type="text/css" href="css/common.css" rel="stylesheet" />
		<link type="text/css" href="css/storelist.css" rel="stylesheet" />
	</head>
	<body>
		<div class="wrap">
			 <header id="header" class="relative">
	            <a class="absolute back" href="javascript:history.go(-1);" title="门店"></a>
	            <div class="tit tit-l">
	                	门店
	            </div>
	            <a class="absolute more" href="javascript:;" title="门店"></a>
	         </header>
	        
	       <section class='clearfix storelist' style='background: #FBD14B;'>
        		<div class='container clearfix'>
        			<ul class='storemsg-wrap clearfix'>
        				<li class='clearfix'> 
        					<div class='fl storemsg-pic'>
        						<img src="images/msgstore1.jpg" />
        					</div>
        					<div class='fl storemsg-cont clearfix'>
        						<h1 class='fl txt-ellipsis'>${store.storeName}</h1>
        						<p class='fl txt-ellipsis clearfix'>${store.address}</p>
        						<div class='fl activity-cut txt-ellipsis w-100'>
        							<c:forEach items="${store.parts}" var="part">
		        							<c:if test="${part.type eq '1'}">
		        							满<fmt:formatNumber type="number" value="${part.full}"  pattern="0.0" maxFractionDigits="1"/>
			        						减<fmt:formatNumber type="number" value="${part.substact}"  pattern="0.0" maxFractionDigits="1"/>;
		        							</c:if>
		        					</c:forEach>
        						</div>
    							<div class='fl activity-discount txt-ellipsis'>
		        					<c:forEach items="${store.parts}" var="part">
		        							<c:if test="${part.type eq '2'}">
		        								<fmt:formatNumber type="number" value="${part.discount}"  pattern="0.0" maxFractionDigits="1"/>折
		        							</c:if>
		        					</c:forEach>
    							</div>
    							<div class='fl txt-ellipsis '>${store.distance}km</div>
        					</div>
        				</li>
        			</ul>
        		</div>
        	</section>
	        
	        <section class='clearfix storemsg'>
	        	<ul class='storemsg-tab clearfix fl'>
	        		 <c:forEach items="${categoryList}" var="category" varStatus="status">
		        		 <c:if test="${status.count == 1}">
		          			<li class="active"> ${category.cgoodsName}</li>
		   				</c:if>
		   				<c:if test="${status.count != 1}">
		          			<li> ${category.cgoodsName}</li>
		   				</c:if>
	        		</c:forEach>
	           </ul>
	        	<div class='storemsg-div clearfix fl'>
	        		<h1>自由便当</h1>
	        		<ul class='clearfix storemsg-ul'> 
	        			<li class='clearfix'>
	        				<a class='fl storemsg-img' href='javascript:;'>
	        					<img src="images/msgstorelist1.jpg" />
	        				</a>
	        				<div class='storemsg-info fl'>
	        					<a class='txt-ellipsis' href='javascript:;'>法式猪扒焗饭</a>
	        					<p class='txt-ellipsis'>鲈鱼澳大利亚进口，少刺进口，少刺......</p>
	        					<p>销量：999份</p>
	        					<div class='googs-price fl'>
	        						<div class='now-price fr'>￥5.0</div>
	        						<div class='original-price fr'>￥5.0</div>
	        					</div>
	        					<div class='goods-num fr'>
	        						<a class='add-goods' href='javascript:;'><img src="images/btn-add.png" /></a>
	        						<span>1</span>
	        						<a class='reduce-goods' href='javascript:;'><img src='images/btn-cut.png' /></a>
	        					</div>
	        				</div>
	        			</li>
	        			<li class='clearfix'>
	        				<a class='fl storemsg-img' href='javascript:;'>
	        					<img src="images/msgstorelist1.jpg" />
	        				</a>
	        				<div class='storemsg-info fl'>
	        					<a class='txt-ellipsis' href='javascript:;'>法式猪扒焗饭</a>
	        					<p class='txt-ellipsis'>鲈鱼澳大利亚进口，少刺进口，少刺......</p>
	        					<p>销量：999份</p>
	        					<div class='googs-price fl'>
	        						<div class='now-price fr'>￥5.0</div>
	        						<div class='original-price fr'>￥5.0</div>
	        					</div>
	        					<div class='goods-num fr'>
	        						<a class='add-goods' href='javascript:;'><img src="images/btn-add.png" /></a>
	        					</div>
	        				</div>
	        			</li>
	        			<li class='clearfix'>
	        				<a class='fl storemsg-img' href='javascript:;'>
	        					<img src="images/msgstorelist1.jpg" />
	        				</a>
	        				<div class='storemsg-info fl'>
	        					<a class='txt-ellipsis' href='javascript:;'>法式猪扒焗饭</a>
	        					<p class='txt-ellipsis'>鲈鱼澳大利亚进口，少刺进口，少刺......</p>
	        					<p>销量：999份</p>
	        					<div class='googs-price fl'>
	        						<div class='now-price fr'>￥5.0</div>
	        						<div class='original-price fr'>￥5.0</div>
	        					</div>
	        					<div class='goods-num fr'>
	        						<a class='add-goods' href='javascript:;'><img src="images/btn-add.png" /></a>
	        					</div>
	        				</div>
	        			</li>
	        			<li class='clearfix'>
	        				<a class='fl storemsg-img' href='javascript:;'>
	        					<img src="images/msgstorelist1.jpg" />
	        				</a>
	        				<div class='storemsg-info fl'>
	        					<a class='txt-ellipsis' href='javascript:;'>法式猪扒焗饭</a>
	        					<p class='txt-ellipsis'>鲈鱼澳大利亚进口，少刺进口，少刺......</p>
	        					<p>销量：999份</p>
	        					<div class='googs-price fl'>
	        						<div class='now-price fr'>￥5.0</div>
	        						<div class='original-price fr'>￥5.0</div>
	        					</div>
	        					<div class='goods-num fr'>
	        						<a class='add-goods' href='javascript:;'><img src="images/btn-add.png" /></a>
	        					</div>
	        				</div>
	        			</li>
	        			<li class='clearfix'>
	        				<a class='fl storemsg-img' href='javascript:;'>
	        					<img src="images/msgstorelist1.jpg" />
	        				</a>
	        				<div class='storemsg-info fl'>
	        					<a class='txt-ellipsis' href='javascript:;'>法式猪扒焗饭</a>
	        					<p class='txt-ellipsis'>鲈鱼澳大利亚进口，少刺进口，少刺......</p>
	        					<p>销量：999份</p>
	        					<div class='googs-price fl'>
	        						<div class='now-price fr'>￥5.0</div>
	        						<div class='original-price fr'>￥5.0</div>
	        					</div>
	        					<div class='goods-num fr'>
	        						<a class='add-goods' href='javascript:;'><img src="images/btn-add.png" /></a>
	        					</div>
	        				</div>
	        			</li>
	        			<li class='clearfix'>
	        				<a class='fl storemsg-img' href='javascript:;'>
	        					<img src="images/msgstorelist1.jpg" />
	        				</a>
	        				<div class='storemsg-info fl'>
	        					<a class='txt-ellipsis' href='javascript:;'>法式猪扒焗饭</a>
	        					<p class='txt-ellipsis'>鲈鱼澳大利亚进口，少刺进口，少刺......</p>
	        					<p>销量：999份</p>
	        					<div class='googs-price fl'>
	        						<div class='now-price fr'>￥5.0</div>
	        						<div class='original-price fr'>￥5.0</div>
	        					</div>
	        					<div class='goods-num fr'>
	        						<a class='add-goods' href='javascript:;'><img src="images/btn-add.png" /></a>
	        					</div>
	        				</div>
	        			</li>
	        			<li class='clearfix'>
	        				<a class='fl storemsg-img' href='javascript:;'>
	        					<img src="images/msgstorelist1.jpg" />
	        				</a>
	        				<div class='storemsg-info fl'>
	        					<a class='txt-ellipsis' href='javascript:;'>法式猪扒焗饭</a>
	        					<p class='txt-ellipsis'>鲈鱼澳大利亚进口，少刺进口，少刺......</p>
	        					<p>销量：999份</p>
	        					<div class='googs-price fl'>
	        						<div class='now-price fr'>￥5.0</div>
	        						<div class='original-price fr'>￥5.0</div>
	        					</div>
	        					<div class='goods-num fr'>
	        						<a class='add-goods' href='javascript:;'><img src="images/btn-add.png" /></a>
	        					</div>
	        				</div>
	        			</li>
	        		</ul>
	        	</div>
	        
	        	<div class='shopcar'>
	        		<div class='fl relative shopcar-icon'>
	        			<img src="images/car.png" />
	        			<div class='absolute carnum'>1</div>
	        		</div>
	        		<div class='carprice fl'>￥5.0</div>
	        		<div class='select-ok fr'>选好了</div>
	        	</div>
	        </section>
	        
	        
	        
	        
	        
	        
	        
	        
	        <script>
				var oHtml = document.getElementsByTagName('html')[0];
				var screenW = document.documentElement.offsetWidth || document.body.offsetWidth;
				oHtml.style.fontSize = 100 * screenW / 720 + "px";
			</script>
	    </div>    
		
	</body>
</html>
