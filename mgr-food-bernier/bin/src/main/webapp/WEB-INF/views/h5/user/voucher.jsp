<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>饭好粥道-我的代金券</title>
<meta name="keywords" content="代金券" />
<meta name="description" content="代金券" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/user.css" />
<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <!-- 头部 -->
        <header id="header" class="header">
            <a class="back" href="javascript:history.go(-1);"></a>
            <div class="tit">
                代金劵
            </div>
        </header>
        <section class="box cupon-wrap pd20">
            <h2 class="cupon-num"></h2>
            <div id="voucherList" data-page-no="${page.pageNo }"
				data-page-size="${page.pageSize }"
				data-total-page="${page.totalPage }">
			<c:forEach items="${page.results }" var="voucher">
            <div class="cupon-item">
                <div class="cupon-top clear">
                    <div class="fl cupon-price">
                        <em>￥</em>${voucher.formatVoucherMoney }
                    </div>
                    <!-- <div class="fr cupon-btn">
                      	  可用
                    </div> -->
                </div>
                <c:if test="${voucher.formatDrawVoucher!=null }">
                <div class="cupon-down clear">
                   <!--  <div class="fl cupon-date">
                       	 有效期至 2016/01/04
                    </div> -->
                    <div class="fr cupon-if">
                      	【 满${voucher.formatDrawVoucher }元可用】
                    </div>
                </div>
                </c:if>
            </div>
            </c:forEach>
            </div>
        </section>
    </div>
    <script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/voucher.js"></script>
</body>

</html>
