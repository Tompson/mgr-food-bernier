<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!--编辑门店-->
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">编辑门店</h4>
    </div>
    <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/store/storeEdit.html">
      <div class="model-box">
        <input type="hidden"  name="id" value="${store.id }">
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>商户名称：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="storeName" value="${store.storeName}" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
           <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>登录账号：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="account" value="${store.account }" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text" disabled="disabled">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
           <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>联系人：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" value="${store.contactPersion }" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>联系人手机号：
          </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name"  name="contactPhone" value="${store.contactPhone }" class="form-control col-md-7 col-xs-12" datatype="m" oninput="if(value.length>14)value=value.slice(0,14)" errormsg="请填写正确的手机号码!" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))"  sucmsg=" " nullmsg="必填"  type="number">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>门店类型：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
              	<c:if test="${store.storeType eq '1' }">
              		 <input id="first-name"  name="type" value="加盟店" class="form-control col-md-7 col-xs-12" disabled="disabled"  >
              	</c:if>
              	<c:if test="${store.storeType eq '2' }">
              		 <input id="first-name"  name="type" value="自营店" class="form-control col-md-7 col-xs-12"  disabled="disabled" >
              	</c:if> 
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          
           <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>门店地址：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="address" value="${store.address }" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
           <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>营业时间：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="openTime" value="${store.openTime }" class="" datatype="*" sucmsg=" " nullmsg="必填" type="text">到
                  <input id="first-name" required="required" name="closeTime" value="${store.closeTime }" class="" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
           <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>起送价：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="startingPrice" value="${store.startingPrice }" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
           <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>配送费：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="deliverFee" value="${store.deliverFee }" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>配送距离：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="deliverDistance" value="${store.deliverDistance }" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>开户行：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="bankName" value="${store.bankName }" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>开户姓名：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="bankPersionName" value="${store.bankPersionName }" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>银行卡号：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="bankCardNo" value="${store.bankCardNo }" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填" type="text">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>银行预留手机号：
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="first-name" required="required" name="bankPhone" value="${store.bankPhone }" class="form-control col-md-7 col-xs-12" datatype="m" oninput="if(value.length>14)value=value.slice(0,14)" errormsg="请填写正确的手机号码!" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))"  sucmsg=" " nullmsg="必填"  type="number">
                  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>门店图片(<em>450*300</em>)：
              
          	</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-item form-item-wd">
	                  <c:if test="${empty store.storePhoto}">
	                  		<img src="<%=request.getContextPath()%>/images/upload.png" alt="" style="width:150px;height: 100px">
	                        <input class="form-file" type="file" name="file" id="goodsPic1" value="" placeholder="">
	                        <input type="hidden" name="storePhoto"  id="gpic1"  datatype="*" sucmsg="&nbsp;" nullmsg="必填" />
	                      <span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
	                  </c:if>
	                    <c:if test="${not empty store.storePhoto}">
	                  		<img src="<%=request.getContextPath()%>${store.storePhoto}" alt="" style="width:150px;height: 100px">
	                        <input class="form-file" type="file" name="file" id="goodsPic1" value="" placeholder="">
	                        <input type="hidden" name="storePhoto"  id="gpic1"  />
	                      <span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
	                  </c:if>   
                  </div>
              </div>
          </div>
      </div>
      <div class="modal-footer mt30">
          <button type="submit" class="btn btn-primary">提交</button>
          <button type="button" class="btn btn btn-default" data-dismiss="modal" id="test1">退出</button>
      </div>
  </form>
</div>
<!-- </script>  -->
            
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('添加成功',{icon:1});    
    					$('#deliverMan_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    					console.log("dataURL:"+vo.data);
    				} else {
    					 layer.msg('添加失败',{icon:5});
    				}
    			}
            });
        });
    </script>
 	<script type="text/javascript">
 $(function(){
 //上传图片
 $(".form-file").on("change", function() {
     var imgFile = $(this)[0];//当前第一个jq对象 jquery对象就是一个数组对象.
     var id=$(this).attr("id");
     var filextension = imgFile.value.substring(imgFile.value.lastIndexOf("."), imgFile.value.length);
     filextension = filextension.toLowerCase();
     if ((filextension != '.jpg') && (filextension != '.gif') && (filextension != '.jpeg') && (filextension != '.png') && (filextension != '.bmp')) {
         alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
         imgFile.focus();
     } else {
         var path;
         if (document.all){//IE
             imgFile.select();
             path = document.selection.createRange().text;
             imgFile.previousSbiling.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")"; //使用滤镜效果 
         } else{//FF
        	 console.log(11111);
        	  $.ajaxFileUpload({  
	  	            url:"<%=request.getContextPath()%>/uploadPic/filesUpload.html",
	  	            secureuri:false,  
	  	            fileElementId:id,//file标签的id  
	  	            dataType: 'json',//返回数据的类型  
	  	            //data:{name:'logan'},//一同上传的数据  
	  	            success: function (data, status) {  
	  	            	$("#"+id).siblings('img')
	  	            	$("#"+id).siblings('img').attr('src',data);
	  	            	$("#"+id).siblings('input[type=hidden]').val(data);
	  	            	
// 	  	            	 $(imgFile).siblings("img").attr("src", data)
	  	            },
	  	            error: function (data, status, e) {  
	  	            	alert("文件上传失败!");   
	  	            }  
	  	        });
         }
      }
 	});
 });

 
 
	function statusChoice(){
		   if($("#status").is(":checked")){
			    $("#hiddenStatus0").val("0"); //启用
			}else{
				$("#hiddenStatus0").val("1"); //禁用
			}
	}
	
</script> 
 	
 	