/**
 * 选择支付方式
 * 1.余额支付 creditpay
 * 2.支付宝wap支付  alipay
 * 3.微信内置浏览器支付  wxpay
 * 4.微信扫码支付 scanpay
 * @param payno 订单号
 * @param style 支付方式
 * @param storeId 门店id
 */
function pay(payno,style,storeId) {
	//支付方式选择
	if (style == "creditpay") {
		creditPay(payno,storeId);
	} else if (style == "wxpay") {
		placeorder(payno);
	} else if (style == "alipay") {
		alipay(payno);
	}else if(style == "scanpay"){
		scanpay(payno);
	}else{
		tips({
			message : "请选择支付方式！",
			skin : "alert"
		});
	}

}

//function

/**
 * 支付前订单验证
 * @param payno
 * @returns {Boolean}
 */
function validateOrder(payno){
	var isPayable=false;
	$.ajax({
		url:"api/validateOrder.html",
		async:false,
		type:"post",
		data:{payno:payno},
		dataType:"json",
		success:function(result){
			if(result&&result.success){
				isPayable=true;
			}else{
				tips({
					message : result.msg,
					skin : "msg"
				});
			}
		},
		error:function(){
			tips({
				message : "系统繁忙！",
				skin : "msg"
			});
		}
	});
	return isPayable;
}


/**
 * 兴团用户余额支付
 * 
 * @param payno
 */
function creditPay(payno,storeId) {
	
	$.post("../pay/creditpay.html", {
		payno : payno,
		storeId:storeId
	}, function(result) {
		debugger;
		if (result && result.success) {
			alert(result.msg);
			window.location.href = "../pay/paySuccess.html?payno=" + payno;
		} else {
			tips({
				message : result.msg,
				skin : "msg"
			});
			windos.location.href="../member/myaccount.html?v=1"
		}
	}, "json");
}
/**
 * 支付宝移动wap支付
 * 
 * @param payno
 */
function alipay(payno) {
	$.post("api/orderPaiedByAlipay.html",{payno:payno},function(result){
		if(result&&result.success){
			window.location.href = result.data;
		}else{
			tips({
				message : result.msg,
				skin : "msg"
			});
		}
	},"json");
}

/**
 * 微信扫描支付
 * 
 * @param payno
 */
function scanpay(payno) {
	$.post("wapScanPay.html",{payno:payno},function(result){
		if(result&&result.success){
			window.location.href = "wxscan.html?url="+result.data;
		}else{
			alert(result.msg);
		}
	},"json");
}

var flag = false;
var order = {};
/**
 * 微信内置浏览器下单
 * 
 * @param payno
 */
function placeorder(payno) {
	$.post("api/orderPaiedByWx.html", {
		payno:payno
	}, function(result) {
		if (result && result.success) {
			order = result.data;
			wxPay();
		}else{
			tips({
				message : "系统繁忙！",
				skin : "msg"
			});
		}
	},"json");
}

/**
 * 微信内置浏览器支付兼容性处理
 */
function wxPay() {
	if (typeof WeixinJSBridge == "undefined") {
		if (document.addEventListener) {
			document.addEventListener('WeixinJSBridgeReady', onBridgeReady,
					false);
		} else if (document.attachEvent) {
			document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
			document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
		}
	} else {
		onBridgeReady();
	}
}

/**
 * 微信内置浏览器支付
 */
function onBridgeReady() {
	WeixinJSBridge.invoke('getBrandWCPayRequest', {
		"appId" : order.appId, // 公众号名称，由商户传入
		"timeStamp" : order.timeStamp, // 时间戳，自1970年以来的秒数
		"nonceStr" : order.nonceStr, // 随机串
		"package" : order["package"],
		"signType" : order.signType, // 微信签名方式 :
		"paySign" : order.paySign
	// 微信签名
	}, function(res) {
		console.log(res);
		// 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回 ok，但并不保证它绝对可靠。
		setTimeout("gotocheckorder()", 2000);
		if (res.err_msg == "get_brand_wcpay_request：ok") {
			// 支付成功 更新订单状态
//			window.location.href = "completeOrder.html?payno="
//					+ order.payno;
		}
	});
}
/**
 * 微信订单查询
 */
function gotocheckorder() {
	window.location.href = "queryorder.html?orderId=" + order.orderId;
}
