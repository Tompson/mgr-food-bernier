function getShopTrolleyNum(goodsId){
	var num=0;
	if(goodsId){
		$.ajax({
			url:"api/getShopTrolleyNum.html",
			type:"get",
			async:false,
			data:{goodsId:goodsId},
			dataType:"json",
			success:function(result){
				if(result&&result.success){
					num=result.data;
				}
			},
		});
	}
	return num;
}

function add2Cart(id,currentWeek){
	var flag=false;
	$(".add-2-cart").on("click", function() {
		if(flag){
			return;
		}
		flag=true;
		/*if(currentWeek&&cacheWeek){
			if(cacheWeek>7){
				cacheWeek=1;
			}
			if(currentWeek!=cacheWeek){
				var str="";
				if(cacheWeek==1){
					str="星期一";
				}else if(cacheWeek==2){
					str="星期二";
				}else if(cacheWeek==3){
					str="星期三";
				}else if(cacheWeek==4){
					str="星期四";
				}else if(cacheWeek==5){
					str="星期五";
				}else if(cacheWeek==6){
					str="星期六";
				}else if(cacheWeek==7){
					str="星期天";
				}
				tips({
					message : "您预定的是"+str+"的菜品，不能将其他日期的菜品加入购物车！",
					skin : "msg"
				});
				return;
			}
		}*/
		//菜品状态 0在售  1已售完
		/*var goodsStatus=$(this).parents(".h-item").find(".mark").data("goods-status");
		if(!goodsStatus){
			goodsStatus=$("#goodsStatus").val();
		}
		if(goodsStatus=='1'){
			tips({
				message : "该菜品已经售完！",
				skin : "msg"
			});
			return;
		}*/
		var goodsId = $(this).data("goods-id");
		var num=0;
		if(goodsId){
			num=getShopTrolleyNum(goodsId);
		}
		num=num+1;
		$.ajax({
			url:"api/add2Cart.html",
			type:"get",
			data:{goodsId : goodsId,num : num,currentWeek:currentWeek},
			dataType:"json",
			success:function(result){
				if(result){
					if(result.success){
						if (id) {
							$("#" + id).html(result.data);
						}
						tips({
							message : "加入购物车成功！",
							skin : "msg"
						});
					}else{
						var errorCode=result.data;
						switch(errorCode){
						case "001":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						case "007":
							tips({
								message : result.msg,
								skin : "msg"
							});
							break;
						case "002":
							tips({
								message : result.msg,
								skin : "msg"
							});
							break;
						case "003":
							tips({
								message : result.msg,
								skin : "confirm"
							},function(){
								location.href="productOfWeek.html?toWeek=tomorrow";
							});
							break;
						default:
							tips({
								message : "加入购物车失败！",
								skin : "msg"
							});
							break;
						}
					}
				}else{
					tips({
						message : "系统繁忙！",
						skin : "msg"
					});
				}
				flag=false;
			},
			error:function(){
				tips({
					message : "系统繁忙！",
					skin : "msg"
				});
				flag=false;
			}
		});
	});
}
/**
 * 购物车非空验证或达到起步价
 * 返回true表示购物车非空且达到起步价
 * @returns {Boolean}
 */
/*function checkShopCartIsEmpty(){
	var flag=false;
	$.ajax({
		url:"api/checkShopCart.html",
		type:"get",
		async:false,
		dataType:"json",
		success:function(result){
			if(result&&result.success){
				flag=true;
			}else{
				tips({
					message : result.msg,
					skin : "alert"
				});
				flag=false;
			}
			
		},
		error:function(){
			tips({
				message : "系统繁忙！",
				skin : "msg"
			});
			flag=false;
		}
	});
	return flag;
}*/
/**
 * 获取星级评分块
 * @param score 评分
 * @returns {String}
 */
function getGrade(score) {
	var html = '<div class="mark">';
	//         		<!-- 0颗星 -->
	if (score < 1) {
		html += '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 2) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 3) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 4) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 5) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score >= 5) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>';
	}
	html += '<em class="seller-star-f">' + score + '</em></div>';
	return html;
}


//过去当前预定类型，选择预定类型，1:预定今日中餐，2:预定今日晚餐,3:预定明日中餐，
//v3版本不在使用
function getCurrentBookType(){
	var bookType="";
	$.ajax({
		url:"api/getCurrentBookType.html",
		type:"get",
		async:false,
		data:{},
		dataType:"json",
		success:function(result){
			if(result&&result.success){
				bookType=result.data;
			}
		}
	});
	
	return bookType;
}

/**
 * 判断当前周是否是当天，如果是当天则判断是否超过营业时间
 * @param week 
 */

function checkCurrentWeek(week){
	var flag=false;
	$("#toShopTrolley").on("click", function(e) {
		e.preventDefault();//取消默认事件
		if(flag){
			return;
		}
		flag=true;
		$.ajax({
			url:"api/checkCurrentWeek.html",
			type:"get",
			data:{week:week},
			dataType:"json",
			async:false,
			success:function(result){
				if(result){
					if(result.success){
						//验证通过
						location.href="shopTrolley.html?v=3";
					}else{
						var errorCode=result.data;
						switch(errorCode){
						case "001":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						case "002":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						case "003":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						}
					}
				}else{
					tips({
						message : "系统繁忙！",
						skin : "msg"
					});
				}
				flag=false;
			},
			error:function(){
				tips({
					message : "系统繁忙！",
					skin : "msg"
				});
				flag=false;
			}
		});
		
	});
}

function addListenFixedCategoryBar(){
	var plot_top = $(".plot").offset().top;   
    $(window.document).on("scroll", function() {
    	
        var wh = $(window).height();
        var s = $("body")[0].scrollTop;
        if (s > plot_top) {
            $(".plot").css("position", "fixed");
        } else {
            $(".plot").css("position", "relative");
        }

    });
}
