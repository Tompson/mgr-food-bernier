$(function() {
	$(".shop-info-add").on("click", function(event) {
		var $this = $(this);
		addNum($this, "add");
	});
	$(".shop-info-meius").on("click", function(event) {
		var $this = $(this);
		addNum($this, "minus");
	});

});

function addNum(_this, sign) {
	var needRemove = false;
	var $shop_show = _this.siblings(".shop-info-show");
	var $shop_item = _this.parents(".category-item");
	var spro_slHtml = parseInt($shop_show.text());
	if (sign === "add") {
		if (spro_slHtml < 999) {
			++spro_slHtml;
		}
	} else {
		if (spro_slHtml > 1) {
			--spro_slHtml;
		} else {
			if($shop_show.text()==1){
				tips({
                    message: "您确定要把该商品移除购物车？",
                    skin: "confirm"
                },function(){
                	needRemove = true;
                	fdfd();
                	if ($shop_item.find(".shop-info-item").length == 1) {
        				// 删除提示
                		if(_this.parents("#goodsList").find(".category-item").length==1){
                			$("#goodsList").remove();
							$(".shop-not").show();
							$("#deleteAll").hide();
							return;
                        }
                        $shop_item.remove();
                        return;
        			}
                	_this.parents(".shop-info-item").remove();
                	return;
                });
			}
		}
	}
	fdfd();
	function fdfd(){
		// 计算总数
		//单价
		/*var objSum = parseFloat(_this.parents(".shop-info-item-price").find("em .unitPrice").text());
//		spro_slHtml = parseInt($shop_show.text());
//		var sum = (spro_slHtml * objSum).toFixed(2);
//		_this.parents(".shop-info-item-price").find("em .subtotalPrice").text(sum);
		var money = 0;
		for (var i = 0; i < $shop_item.find(".shop-info-item").length; i++) {
			var unitPrice=parseFloat($shop_item.find(".shop-info-item-price").find('em .subtotalPrice').eq(i).text());
			var count=parseFloat($shop_item.find(".shop-info-show").eq(i).text());
			money += unitPrice*count;
		}
		money =money.toFixed(2);
		_this.parents(".shop-info").find(".shop-settle-money .amountPrice").text(money);*/
		
		if(needRemove){
			spro_slHtml = 0;
		}
		
		var goodsId = $shop_show.data("goods-id");
		var currentWeek=$shop_item.find(".delete-weeknum").data("weeknum");
		if(!goodsId){
			return;
		}
		
		$.ajax({
			url:"api/add2Cart.html",
			type:"get",
			data:{goodsId:goodsId,num:spro_slHtml,currentWeek:currentWeek},
			dataType:"json",
			success:function(result){
				if(result){
					if(result.success){
						$shop_show.text(spro_slHtml);
						var amountPrice=result.data;
						$shop_item.find(".shop-info-settle .amountPrice").html(amountPrice);
					}else{
						var errorCode=result.data;
						switch(errorCode){
						case "001":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						case "007":
							tips({
								message : result.msg,
								skin : "msg"
							});
							$.ajax({
								url:"api/deleteForbiddenGoods.html",
								type:"get",
								data:{goodsId:goodsId},
								dataType:"json",
								success:function(result){
									if(result&&result.success){
										if ($shop_item.find(".shop-info-item").length == 1) {
					        				// 删除提示
					                		if(_this.parents("#goodsList").find(".category-item").length==1){
					                			$("#goodsList").remove();
												$(".shop-not").show();
												$("#deleteAll").hide();
												return;
					                        }
					                        $shop_item.remove();
					                        return;
					        			}
					                	_this.parents(".shop-info-item").remove();
									}
								},
								error:function(){
									tips({
										message : "系统繁忙！",
										skin : "msg"
									});
								}
							});
							break;
						case "002":
							tips({
								message : result.msg,
								skin : "msg"
							});
							$.ajax({
								url:"api/deleteForbiddenGoods.html",
								type:"get",
								data:{goodsId:goodsId},
								dataType:"json",
								success:function(result){
									if(result&&result.success){
										if ($shop_item.find(".shop-info-item").length == 1) {
					        				// 删除提示
					                		if(_this.parents("#goodsList").find(".category-item").length==1){
					                			$("#goodsList").remove();
												$(".shop-not").show();
												$("#deleteAll").hide();
												return;
					                        }
					                        $shop_item.remove();
					                        return;
					        			}
					                	_this.parents(".shop-info-item").remove();
									}
								},
								error:function(){
									tips({
										message : "系统繁忙！",
										skin : "msg"
									});
								}
							});
							break;
						case "003":
							tips({
								message : "椒君，很抱歉，已到今天打烊时间了，您可以预订其他天的菜品！",
								skin : "alert"
							});
							break;
						default:
							tips({
								message : "加入购物车失败！",
								skin : "msg"
							});
							break;
						}
					}
				}else{
					tips({
						message : "系统繁忙！",
						skin : "msg"
					});
				}
				flag=false;
			},
			error:function(){
				tips({
					message : "系统繁忙！",
					skin : "msg"
				});
				flag=false;
			}
		});
		
	}
	
}