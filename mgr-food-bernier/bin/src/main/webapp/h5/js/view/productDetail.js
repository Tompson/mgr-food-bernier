var flag = true;
$(function() {
	// 添加购物车监听
	add2CartListen();
	// 分享提示显示
	$(".py").on("touchend", function(e) {
		e.preventDefault();
		document.getElementById('JweixinTip').style.display = 'block';
	});
	// 分享提示隐藏
	$("#JweixinTip").on("click", function() {
		$(this).hide();
	});
	var configInfo;
	var url = location.href.split('#')[0];
	console.log(url);
	//获取微信jssdk配置
	$.get("api/initWxConfig.html", {
		url : url
	}, function(result) {
		if (result && result.success) {
			configInfo = result.data;
			wxInit();
		}
	}, "json");
	function wxInit() {
		wx.config({
//			 debug : true, //
			// 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			appId : configInfo.appId, // 必填，公众号的唯一标识
			timestamp : configInfo.timestamp, // 必填，生成签名的时间戳
			nonceStr : configInfo.noncestr, // 必填，生成签名的随机串
			signature : configInfo.signature,// 必填，签名，见附录1
			jsApiList : configInfo.jsApiList
		// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
		});
	}
	wx.ready(function() {
		var title = $("#goodsName").val();
		var desc = $("#intro").val();
		var imgUrl = $("#imgUrl").attr("src");
		console.log("微信配置返回的url--->" + configInfo.url);
		console.log("图片链接-->" + imgUrl);
		// 分享到朋友
		wx.onMenuShareAppMessage({
			title : title, // 分享标题
			desc : desc, // 分享描述
			link : configInfo.url, // 分享链接
			imgUrl : imgUrl, // 分享图标
			type : 'link', // 分享类型,music、video或link，不填默认为link
			dataUrl : '', // 如果type是music或video，则要提供数据链接，默认为空
			success : getShareAward,
			cancel : function() {
				// 用户取消分享后执行的回调函数
				console.log("分享到朋友-->取消");
				$("#JweixinTip").hide();
			}
		});
		// 分享到朋友圈
		wx.onMenuShareTimeline({
			title : title, // 分享标题
			link : configInfo.url, // 分享链接
			imgUrl : imgUrl, // 分享图标
			success : getShareAward,
			cancel : function() {
				// 用户取消分享后执行的回调函数
				console.log("分享到朋友圈-->取消");
				$("#JweixinTip").hide();
			}
		});
		// 分享到腾讯微博
		wx.onMenuShareWeibo({
			title : title, // 分享标题
			desc : desc, // 分享描述
			link : configInfo.url, // 分享链接
			imgUrl : imgUrl, // 分享图标
			success : getShareAward,
			cancel : function() {
				// 用户取消分享后执行的回调函数
				console.log("分享到腾讯微博-->取消");
				$("#JweixinTip").hide();
			}
		});
	});
	//立即购买
	$("#immediatelyBuy").click(
			function() {
				var goodsId = $("#goodsId").val();
				var amount = 1;
				var weeknum=$("#week").val();
				$.ajax({
					url : "api/validateBeforeOrderConfirm.html",
					type : "get",
					data : {
						goodsId : goodsId,
						amount : amount,
						weeknum:weeknum
					},
					dataType : "json",
					success : function(result) {
						if (result) {
							if(result.success){
								location.href = "orderConfirm.html?goodsId="+goodsId + "&amount=" + amount+ "&weeknum=" + weeknum;
							}else{
								var errorCode=result.data;
								switch(errorCode){
								case "001":
									tips({
										message : result.msg,
										skin : "alert"
									});
									break;
								case "007":
									tips({
										message : result.msg,
										skin : "msg"
									});
									break;
								case "002":
									tips({
										message : result.msg,
										skin : "msg"
									});
									break;
								case "003":
									tips({
										message : result.msg,
										skin : "msg"
									});
									break;
								case "004":
									tips({
										message : result.msg,
										skin : "confirm"
									},function(){
										// 加入购物车
										$("#addToCart").trigger("click");
									});
									break;
								default:
//									tips({
//										message : "系统繁忙！",
//										skin : "msg"
//									});
//									location.reload();
									break;
								}
							}
						} else {
							tips({
								message : "系统繁忙！",
								skin : "msg"
							});
						}
					},
					error : function() {
						tips({
							message : "系统繁忙！",
							skin : "msg"
						});
					}
				});
			});
	$(window).on("scroll", function() {
		var p_hg = Math.ceil($("body")[0].scrollHeight) - 50; // 获取当前元素高度
		var s_top = Math.ceil($("body")[0].scrollTop + $(this).height()); // 获取滚动的高度
		var arry = [];
		if (p_hg <= s_top && flag) {
			flag = false;
			tips({
				skin : "loading"
			}); // 加载进度条
			arry = [];
			var id = "commentList";
			var commentList = $("#" + id);
			var pageNo = commentList.data("page-no");
			var pageSize = commentList.data("page-size");
			var totalPage = commentList.data("total-page");
			var params = {};
			if (pageNo && pageSize && totalPage) {
				pageNo = parseInt(pageNo);
				pageSize = parseInt(pageSize);
				totalPage = parseInt(totalPage);
				if (pageNo++ >= totalPage) {
					tips.loadClose();
					return;
				}
				params.pageNo = pageNo;
				params.pageSize = pageSize;
			}
			$.post("api/getCommentList.html", params, function(result) {
				tips.loadClose(); // 关闭进度条
				if (result) {
					if (result.success) {
						renderCommentList(result.data, id);
					} else {
						tips({
							message : "没有更多评论啦！",
							skin : "msg"
						});
					}
				} else {
					tips({
						message : "系统繁忙！",
						skin : "msg"
					});
				}
			});

		}
	});
});
/**
 * 评论列表滚动刷新
 * @param data
 * @param id
 */
function renderCommentList(data, id) {
	var commentList = $("#" + id);
	commentList.data("page-no", data.pageNo);
	commentList.data("page-size", data.pageSize);
	commentList.data("total-page", data.totalPage);
	var array = [];
	var html = null, zhtml = null, shtml = null;
	for (var i = 0; i < data.results.length; i++) {
		var score = data.results[i].commentGrade;
		var replyContent = data.results[i].replyContent;
		zhtml = getGrade(score);
		if (replyContent) {
			shtml = '<div class="reply mt10">'
					+ '<em class="color-ff5000">店家回复：</em><span>'
					+ replyContent + '</span>' + '</div>';
		}
		html = '<dl class="item pd20">' + '<dt class="top clear">'
				+ '<div class="fl">' + '<span class="phone">'
				+ data.results[i].memberNick + '</span>' + '</div>'
				+ '<div class="fr">' + '<span class="date">'
				+ data.results[i].formatSdate + '</span>' + '</div>' + '</dt>'
				+ '<dd class="info">' + zhtml + '<div class="desc">' + '<p>'
				+ data.results[i].content + '</p>' + '</div>' + shtml + '</dd>'
				+ '</dl>';
		array.push(html);
		commentList.append(array.join(""));
	}
	flag = true;
}
// 加入购物车监听
function add2CartListen() {
	var week = $("#week").val();
	add2Cart("", week);
}

// 获取分享奖励积分
function getShareAward() {
	$("#JweixinTip").hide();
	$.get("api/getShareAward.html", {}, function(result) {
		if (result && result.success) {
			tips({
				message : "分享成功，积分+" + result.data,
				skin : "msg"
			});
			setTimeout(function(){
				tips.msgClose();
				tips({
	                "message": "恭喜您获得抽奖机会！点击确认抽奖！",
	                "skin": "confirm"
	            }, function() {
	            	$.ajax({
	            		url:"api/lotteryDraw.html",
	            		type:"get",
	            		data:{category:2},
	            		dataType:"json",
	            		success:function(res){
	            			if(res){
	            				if(res.success){
	            					tips({
	            	                    "message": "恭喜您获得"+res.data+"元代金券，可在【我的】-【代金券】中查看",
	            	                    "skin": "alert"
	            	                });
	            				}else{
	            					tips({
	            	                    "message": "很遗憾，本次您并未中奖~",
	            	                    "skin": "alert"
	            	                });
	            				}
	            			}else{
	            				tips({
	                                "message": "系统繁忙！",
	                                "skin": "msg"
	                            });
	            			}
	            		},
	            		error:function(){
	            			tips({
	                            "message": "系统繁忙！",
	                            "skin": "msg"
	                        });
	            		}
	            	});
	            });
			}, 500);
			
		}else{
			tips({
				message : "分享成功，未获得积分！",
				skin : "msg"
			});
		}
	}, "json");
}