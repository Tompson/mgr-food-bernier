var flag = true;
$(function() {
	// 取消订单
	$(".cancleOrder").on("click", cancleOrder);
	// 继续支付
	$(".continuePay").on("click", continuePay);
	// 确认收货
	$(".sureReceive").on("click", sureReceive);
	// 去评价
	$(".toAdvice").on("click", toAdvice);
});
// 获取orderId，传入this对象
function getOrderId(obj) {
	return $(obj).data("order-id");
}
/* 取消订单 */
function cancleOrder() {
	var orderId = getOrderId(this);
	tips({
		message : "确认取消订单吗",
		skin : "confirm"
	}, function() {
		$.get("api/cancleOrder.html", {
			orderId : orderId
		}, function(result) {
			if (result && result.success) {
				tips({
					message : "订单已取消",
					skin : "msg"
				});
				// 刷新当前页面
				window.location.reload();
			}
		}, "json");
	});
	return false;
}
/* 去支付 */
function continuePay() {
	var orderId = getOrderId(this);
	window.location.href = "orderPay.html?orderId=" + orderId;
	return false;
}
/* 去评价 */
function toAdvice() {
	var orderId = getOrderId(this);
	window.location.href = "evaluate.html?orderId=" + orderId;
	return false;
}
/* 确认收货 */
function sureReceive() {
	var orderId = getOrderId(this);
	tips({
		message : "是否确认收货?",
		skin : "confirm"
	}, function() {
		$.get("api/sureReceive.html", {
			orderId : orderId
		}, function(result) {
			if (result && result.success) {
				// 跳转到订单评价页面
				window.location.href = "evaluate.html?orderId=" + orderId;
			}
		}, "json");
	});
	return false;
}

$(window).on("scroll", function() {
	var p_hg = Math.ceil($("body")[0].scrollHeight) - 50; // 获取当前元素高度
	var s_top = Math.ceil($("body")[0].scrollTop + $(this).height()); // 获取滚动的高度
	var arry = [];
	if (p_hg <= s_top && flag) {
		flag = false;
		tips({
			skin : "loading"
		}); // 加载进度条
		arry = [];
		var id = "orderList";
		var orderList = $("#" + id);
		var pageNo = orderList.data("page-no");
		var pageSize = orderList.data("page-size");
		var totalPage = orderList.data("total-page");
		var params = {};

		if (pageNo && pageSize && totalPage) {
			pageNo = parseInt(pageNo);
			pageSize = parseInt(pageSize);
			totalPage = parseInt(totalPage);

			if (pageNo++ >= totalPage) {
				tips.loadClose();
				return;
			}

			params.pageNo = pageNo;
			params.pageSize = pageSize;
		}
		$.get("api/getOrderList.html", params, function(result) {
			tips.loadClose(); // 关闭进度条
			if (result && result.success) {
				var data = result.data;
				if (data) {
					renderOrderList(data, id);
				} else {
					tips({
						message : "没有更多数据啦！",
						skin : "msg"
					});
				}

			}
		});

	}
});
function renderOrderList(data, id) {
	var orderList = $("#" + id);
	orderList.data("page-no", data.pageNo);
	orderList.data("page-size", data.pageSize);
	orderList.data("total-page", data.totalPage);
	var array = [];
	var html = null,zhtml=null;
	for (var i = 0; i < data.results.length; i++) {
		var goodsList = data.results[i].orderGoodsList;
		var orderStatus = data.results[i].converedOrderStatus;
		html = '<div  class="od-item">' + '<div class="top pd20 clear">'
				+ '<p class="fl">下单时间&nbsp;&nbsp;'
				+ data.results[i].formatOrderTime + '</p>'
				+ '<span class="fr color-ff5000">'
				+ orderStatus + '</span>' + '</div>'
				+ '<div class="info pd20">' + '<h6 class="tit">商品列表</h6>';
		array.push(html);
		for (var j = 0; j < goodsList.length; j++) {
			html = '<a href="#">' + ' <div class="op-item">'
					+ '<p class="name">' + goodsList[j].googsName
					+ '</p><i class="num">x' + goodsList[j].quantify
					+ '</i><em class="price">￥' + goodsList[j].pricePay
					+ '</em>' + '</div>' + '</a>';
			array.push(html);
		}
		var orderId = data.results[i].orderId;
		
		if (orderStatus == "待付款") {
			zhtml = '<a class="btn btn-yellow cancleOrder" data-order-id="'
					+ orderId + '" href="#">取消订单</a>'
					+ '<a class="btn btn-red continuePay" data-order-id="'
					+ orderId + '" href="#">去支付</a>';
		} else if (orderStatus == "配送中") {
			zhtml = '<a class="btn btn-red sureReceive" data-order-id="'
					+ orderId + '" href="#">确认收货</a>';
		} else if (orderStatus == "已完成") {
			zhtml = '<a class="btn btn-red toAdvice" data-order-id="' + orderId
					+ '" href="#">去评价</a><em class="points">(评论可抽奖)</em>';
		} else if (orderStatus == "已评价") {
			zhtml = '<a class="btn btn-gray" data-order-id="' + orderId
					+ '">已评价</a>';
		} else if (orderStatus == "已取消") {
			zhtml = '<a class="btn btn-gray" data-order-id="' + orderId
					+ '">已取消</a>';
		}else{
			zhtml="";
		}

		html = '<div class="op-item">'
				+ '<p class="name">配送费</p><em class="price">￥'
				+ data.results[i].formatOutsideMoney + '</em>' + '</div>'
				+ '</div>' + '<div class="money pd20">'
				+ '<p class="my-price color-ff5000">实付:￥'
				+ data.results[i].formatPayAmount + '</p>' + zhtml + '</div>'
				+ '</div>';
		array.push(html);
	}
	orderList.append(array.join(""));
	flag = true;
	// 取消订单
	$(".cancleOrder").on("click", cancleOrder);
	// 继续支付
	$(".continuePay").on("click", continuePay);
	// 确认收货
	$(".sureReceive").on("click", sureReceive);
	// 去评价
	$(".toAdvice").on("click", toAdvice);
}