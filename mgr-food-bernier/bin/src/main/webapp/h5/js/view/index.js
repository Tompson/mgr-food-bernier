$(function() {
	silder(".slider"); 
	// 用户定位
	var map = new BMap.Map("mymap");
	// 百度地图API功能
	var container = document.getElementById("mymap");
	var userPoint = null;
	var userlat, userlng;
	var request=requstUrl;
	var stores=null;
	
	// 微信js-sdk定位
	var configInfo;
	var url = location.href.split('#')[0];
	console.log(url);
	// 获取微信jssdk配置
	$.get("api/initWxConfig.html", {
		url : url
	}, function(result) {
		if (result && result.success) {
			configInfo = result.data;
			wxInit();
		}
	}, "json");
	function wxInit() {
		wx.config({
			// debug : true, //
			// 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			appId : configInfo.appId, // 必填，公众号的唯一标识
			timestamp : configInfo.timestamp, // 必填，生成签名的时间戳
			nonceStr : configInfo.noncestr, // 必填，生成签名的随机串
			signature : configInfo.signature,// 必填，签名，见附录1
			jsApiList : [ 'getNetworkType', 'previewImage', 'getLocation' ]
		// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
		});
	}
	wx.ready(function() {
		wx.checkJsApi({
			jsApiList : [ 'getNetworkType', 'previewImage', 'getLocation' ],
			success : function(res) {
				console.log("接口检测通过");
			}
		});
		wx.getLocation({
			type : 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
			success : function(res) {
				try {
					var latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
					var longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
					var speed = res.speed; // 速度，以米/每秒计
					var accuracy = res.accuracy; // 位置精度
					console.log("latitude==" + latitude + ",longitude=="
							+ longitude);
					var wxPoint = new BMap.Point(longitude, latitude);
					// 坐标转换完之后的回调函数
					translateCallback = function(data) {
						if (data.status === 0) {
							userlng = data.points[0].lng;
							userlat = data.points[0].lat;
							console.log("转换后的经纬度，lng==" + userlng + ",lat=="
									+ userlat);
							if (!userlat || !userlng) {
								userlat = container.getAttribute("userlat");
								userlng = container.getAttribute("userlng");
								userLocation();
							} else {
								userLocation();
							}
						}
					}
					var convertor = new BMap.Convertor();
					var pointArr = [];
					pointArr.push(wxPoint);
					convertor.translate(pointArr, 1, 5, translateCallback);
				} catch (e) {
					console.log("微信定位异常");
					console.log(e.message);
				}
			},
			cancel : function(res) {
				// window.location.href="none.aspx?msg=拒绝获取地理位置&r=" +
				// Math.random();//拒绝
				console.log("拒绝获取地理位置");
			},
			fail : function() {
				alert("未能获取地理位置！首先检查手机是否启用微信定位。");
			}
		});

	});
	/**
	 * 添加移动到用户控件
	 */
	function addPanToUserCrl() {
		// 定义移动到用户控件
		function panToUserControl() {
			// 默认停靠位置和偏移量
			this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;
			this.defaultOffset = new BMap.Size(10, 90);
		}

		panToUserControl.prototype = new BMap.Control();

		panToUserControl.prototype.initialize = function(map) {
			var div = document.createElement("div");
			// 添加文字说明
			div.appendChild(document.createTextNode("我的位置"));
			// 设置样式
			div.style.cursor = "pointer";
			div.style.backgroundColor = "#ffb000";
			div.style.padding = ".2rem .2rem";
			div.style.font = "bold .3rem/1.2 Helvetica,'\5FAE\8F6F\96C5\9ED1','microsoft yahei'";
			div.style.color = "#fff";
			div.style.opacity = "0.8";
			// 绑定事件,点击移动到自己
			div.onclick = function(e) {
				// 提示框,提示我的位置
				var geoc = new BMap.Geocoder();
				geoc.getLocation(userPoint, function(rs) {
					var addComp = rs.addressComponents;
					var scontent = "我在" + addComp.province + addComp.city
							+ addComp.district + addComp.street
							+ addComp.streetNumber + "附近";
					var opts = {
						// width : 200, // 信息窗口宽度
						// height: 10, // 信息窗口高度
						title : "我的位置", // 信息窗口标题
					// enableMessage:true,//设置允许信息窗发送短息
					// message:"亲耐滴，晚上一起吃个饭吧？戳下面的链接看下地址喔~"
					};
					var infoWindow = new BMap.InfoWindow(scontent, opts); // 创建信息窗口对象
					map.openInfoWindow(infoWindow, userPoint); // 开启信息窗口
				});
				// 必须要加延时，才能移动到中心
				setTimeout(function() {
					map.panTo(userPoint);
				}, 500);
			}
			// 添加DOM元素到地图中
			map.getContainer().appendChild(div);
			// 将DOM元素返回
			return div;
		}
		var panToUser = new panToUserControl();
		map.addControl(panToUser);
	}
	/**
	 * 用户定位
	 */
	function userLocation() {
		if (userlat && userlng) {
			userPoint = new BMap.Point(userlng, userlat);
			var myIcon = new BMap.Icon(request+"/h5/images/userlogo.png", new BMap.Size(40,40));
			var marker=new BMap.Marker(userPoint,{icon:myIcon});  
			map.centerAndZoom(userPoint, 12);
			map.addOverlay(marker);
//			map.
//			setTimeout(function() {
//				map.panTo(userPoint);
//			}, 500);
//			// 获取两点间的距离
//			var distance = parseInt((map.getDistance(point, userPoint))
//					.toFixed(2));
			$.ajax({
				url: request+'/h5/sendMemberLocation.html?lat='+userlat+'&lng='+userlng, 
					context: document.body, 
					async: false,
					dataType:'json',
					success:function(vo){
						
						if(vo.success){
	      					stores=vo.data;
	      				}
					},
			});
			// 商户定位
			stores = JSON.parse(stores);
			for (var i = 0; i < stores.length; i++) {
				var v = stores[i];
				var point = new BMap.Point(v.lng, v.lat);
				console.log(point);
				 
				var marker = new BMap.Marker(point); // 创建标注
				map.addOverlay(marker); // 将标注添加到地图中
				marker.disableDragging(); // 可拖拽
				map.setCurrentCity("深圳"); // 设置地图显示的城市 此项是必须设置的
//				map.centerAndZoom(point, 12); // 初始化地图,设置中心点坐标和地图级别
				map.enableScrollWheelZoom(true); //
				var radius = v.deliverDistance//
				var circle = new BMap.Circle(point, radius);
				circle.setStrokeColor("#CCFFFF");
				circle.setFillColor("#CCFFFF");
				circle.setStrokeOpacity(0.5);
				circle.setFillOpacity(0.2);
				circle.setStrokeWeight(1);
				map.addOverlay(circle);
			}
			/*// 创建提示控件,添加到地图当中
			var warnCtrl = new WarnControl();

			if (distance > parseInt(radius)) {
				// 不在范围内
				map.addControl(warnCtrl);
			} else {
				// 范围内
				map.removeControl(warnCtrl);
			}
			addPanToUserCrl();*/
			
		} else {
			alert("定位失败！");
			$.ajax({
				url: request+'/h5/sendMemberLocation.html?lat='+userlat+'&lng='+userlng, 
					context: document.body, 
					async: false,
					dataType:'json',
					success:function(vo){
						
						if(vo.success){
	      					stores=vo.data;
	      				}
					},
			});
			// 商户定位
			stores = JSON.parse(stores);
			for (var i = 0; i < stores.length; i++) {
				var v = stores[i];
				var point = new BMap.Point(v.lng, v.lat);
				console.log(point);
				var marker = new BMap.Marker(point); // 创建标注
				map.addOverlay(marker); // 将标注添加到地图中
				marker.disableDragging(); // 可拖拽
				map.setCurrentCity("深圳"); // 设置地图显示的城市 此项是必须设置的
//				map.centerAndZoom(point, 12); // 初始化地图,设置中心点坐标和地图级别
				map.enableScrollWheelZoom(true); //
				var radius = v.deliverDistance;//
				var circle = new BMap.Circle(point, radius);
				circle.setStrokeColor("#CCFFFF");
				circle.setFillColor("#CCFFFF");
				circle.setStrokeOpacity(0.5);
				circle.setFillOpacity(0.2);
				circle.setStrokeWeight(1);
				map.addOverlay(circle);
			}
		}
	}

	
});
// 定义一个控件类,提示类控件
function WarnControl() {
	// 默认停靠位置和偏移量
	this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
	this.defaultOffset = new BMap.Size(10, 10);
}

// 通过JavaScript的prototype属性继承于BMap.Control
WarnControl.prototype = new BMap.Control();

// 自定义控件必须实现自己的initialize方法,并且将控件的DOM元素返回
// 在本方法中创建个div元素作为控件的容器,并将其添加到地图容器中
/*WarnControl.prototype.initialize = function(map) {
	// 创建一个DOM元素
	var div = document.createElement("div");
	// 添加文字说明
	div.appendChild(document.createTextNode("您不在配送范围内，请慎重购买"));
	// 设置样式
	div.style.cursor = "pointer";
	// div.style.border = "1px solid gray";
	div.style.backgroundColor = "#fff";
	div.style.padding = ".12rem .12rem";
	div.style.font = "italic bold .3rem/1.2 Helvetica,'\5FAE\8F6F\96C5\9ED1','microsoft yahei'";
	div.style.color = "#FF0000";
	div.style.opacity = "0.8";
	// // 绑定事件,点击一次放大两级
	// div.onclick = function(e) {
	// map.setZoom(map.getZoom() + 2);
	// }
	// 添加DOM元素到地图中
	map.getContainer().appendChild(div);
	// 将DOM元素返回
	return div;
}*/
