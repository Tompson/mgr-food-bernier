$(function() {
			//分类条监听
			addListenFixedCategoryBar();
// 			showBookModel();
			//调用轮播图	
			silder(".slider"); 
			//加入购物车单击事件监听
			add2CartListen();
			//选好了，去购物车单击事件监听
			toShopTrolleyListen();
			//单击跳转团购团餐申请页面
			$("#groupDesc").click(function(){
				location.href="groupBuyDesc.html";
			});
			//单击跳转星期菜品页面
			$("#goodsWeek").click(function(){
				location.href="productOfWeek.html";
			});
			//选择预定类型，1:今日中餐，2:今日晚餐,3:预定明日中餐 4.预定今日晚餐
			/* $(".seldate-btn").on("click", function() {
				var $this=$(this);
				var bookType=$this.data("book-type");
				if(!bookType){
					return;
				}
				var currentWeek=parseInt($("#current-week").val());
				$.get("api/bookOrder.html",{bookType:bookType},function(result){
					if(result&&result.success){
						//预定订单 加入购物车
						$(".seldate").hide();
						var bookType=result.data;
						if(bookType==3){
							var week=currentWeek+1;
							if(week>7){
								week=1;
							}
							window.location.href="productOfWeek.html?week="+week;
						}
					}else{
						//可能页面被篡改，刷新页面重新选择
						location.reload();
					}
				},"json");
			}); */
			
			/* $("#resetBookType").click(function(){
				$.ajax({
					url:"api/resetBookType.html",
					type:"get",
					data:{},
					dataType:"json",
					success:function(result){
						if(result){
							if(result.success){
								showBookModel();
							}else{
								tips({
									message : "系统繁忙！",
									skin : "msg"
								});
							}
						}else{
							
						}
					},
					error:function(){
					}
				});
			}); */
		
		/* 	$(".to-confirm-order").click(function(){
				if(checkShopCartIsEmpty()){
					window.location.href="orderConfirm.html";
				}
				return false;
			}); */
		});
		//弹出预定模块
		/* function showBookModel(){
			var date = new Date();
			var cacheBookType=getCurrentBookType();
			if(!cacheBookType){
				$(".seldate").show();
				if(date.getHours() < 14){
					$("#noon,#booknight").show();
				}else{
					$("#night,#booktomorrow").show();
				}
				//请求判断营业时间，返回错误消息：1.中餐订购不可用，2.晚餐订购不可用
				$.ajax({
					url:"api/getBussessStatus.html",
					type:"get",
					data:{},
					dataType:"json",
					success:function(result){
						if(result){
							if(result.success){
								
							}else{
								if(result.msg==1){
									$("#noon").attr("disabled", true);
								}else if(result.msg==2){
									$("#night").attr("disabled", true);
								}
							}
						}else{
							
						}
					},
					error:function(){
					}
				});
			}
		} */
		//加入购物车监听
		function add2CartListen(){
			var currentWeek=$("#current-week").val();
			add2Cart("amountPrice",currentWeek);
		}
		//选好了，去购物车单击事件监听
		function toShopTrolleyListen(){
			var currentWeek=$("#current-week").val();
			checkCurrentWeek(currentWeek);
		}