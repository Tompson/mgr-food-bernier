$(".exchange").on("click",function(){
		var voucherId=$(this).parents(".cupon-item").data("voucher-id");
		if(!voucherId){
			return;
		}
		$.ajax({
    		url:"api/exchangeVoucher.html",
    		type:"get",
    		data:{voucherId:voucherId},
    		dataType:"json",
    		success:function(res){
    			if(res){
    				if(res.success){
    					var integral=res.data;
    					$("#userIntegral").html(integral);
    					tips({
    	                    "message": "兑换成功,可在【我的】-【代金券】中查看",
    	                    "skin": "alert"
    	                });
    				}else{
    					tips({
    	                    "message": res.msg,
    	                    "skin": "alert"
    	                });
    				}
    			}else{
    				tips({
                        "message": "系统繁忙！",
                        "skin": "msg"
                    });
    			}
    		},
    		error:function(){
    			tips({
                    "message": "系统繁忙！",
                    "skin": "msg"
                });
    		}
    	});
	});
	var flag = true;
	$(window).on("scroll", function() {
		var p_hg = Math.ceil($("body")[0].scrollHeight) - 50; // 获取当前元素高度
		var s_top = Math.ceil($("body")[0].scrollTop + $(this).height()); // 获取滚动的高度
		var arry = [];
		if (p_hg <= s_top && flag) {
			flag = false;
			tips({
				skin : "loading"
			}); // 加载进度条
			arry = [];
			var id = "voucherList";
			var voucherList = $("#" + id);
			var pageNo = voucherList.data("page-no");
			var pageSize = voucherList.data("page-size");
			var totalPage = voucherList.data("total-page");
			var params = {};

			if (pageNo && pageSize && totalPage) {
				pageNo = parseInt(pageNo);
				pageSize = parseInt(pageSize);
				totalPage = parseInt(totalPage);

				if (pageNo++ >= totalPage) {
					tips.loadClose();
					return;
				}

				params.pageNo = pageNo;
				params.pageSize = pageSize;
			}
			$.get("api/getExchangeVoucher.html", params, function(result) {
				tips.loadClose(); // 关闭进度条
				if (result && result.success) {
					var data = result.data;
					if (data) {
						renderVoucherList(data, id);
					} else {
						tips({
							message : "没有更多数据啦！",
							skin : "msg"
						});
					}

				}
			});
		}
	});
	function renderVoucherList(data, id) {
		var voucherList = $("#" + id);
		voucherList.data("page-no", data.pageNo);
		voucherList.data("page-size", data.pageSize);
		voucherList.data("total-page", data.totalPage);
		var array = [];
		var html = null,zhtml=null;
		for (var i = 0; i < data.results.length; i++) {
			var voucherMoney=data.results[i].voucherMoney;
			var exchangeVoucher=data.results[i].exchangeVoucher;
			html='<div class="cupon-item">'+
            '<div class="cupon-top clear">'+
                '<div class="fl cupon-price">'+
                    '<em>￥</em>'+voucherMoney+
                '</div>'+
                '<div class="fr cupon-btn">兑换</div>'+
            '</div>'+
            '<div class="cupon-down clear">'+
                '<div class="fr cupon-if">【所需积分'+exchangeVoucher+'】</div>'+
            '</div>'+
        '</div>';
		array.push(html);
		}
		voucherList.append(array.join(""));
		flag = true;
	}