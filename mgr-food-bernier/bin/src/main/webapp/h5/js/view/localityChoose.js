$(function() {
            var lack = $(".lact-box");
            var address = "";
            var arry = [];
            /*省*/
            $.ajax({
                url: "../h5/json/ChineseCities.json",
                dataType: "json",
                success: function(data) {
                    for (var num in data) {
                        arry.push("<div class='name item'>" + data[num]["name"] + "</div>");
                    }
                    lack.empty();
                    lack.append(arry.join(""));
                }
            });
            /*市或者区*/
            lack.on("click", ".name", function() {
                arry = [];
                var vals = $(this).text();
                address += vals;
                $.ajax({
                    url: "../h5/json/ChineseCities.json",
                    dataType: "json",
                    success: function(data) {
                        for (var num in data) {
                            if (data[num]["name"] === vals) {
                                if (data[num]["city"].length === 1) {
                                    /*一级目录*/
                                    for (var city in data[num]["city"]) {
                                        for (var i = 0; i < data[num]["city"][0]["area"].length; i++) {
                                            arry.push("<div class='area item'>" + data[num]["city"][0]["area"][i] + "</div>");

                                        }
                                    }
                                } else {
                                    /*二级目录*/
                                    for (var num2 in data[num]["city"]) {
                                        arry.push("<div class='city item'>" + data[num]["city"][num2]["name"] + "</div>");
                                    }
                                }
                            }
                        }
                        lack.empty();
                        lack.append(arry.join(""));
                    }
                });
            });
            /*县*/
            lack.on("click", ".city", function() {
                arry = [];
                var vals = $(this).text();
                address += vals;
                $.ajax({
                    url: "../h5/json/ChineseCities.json",
                    dataType: "json",
                    success: function(data) {
                        for (var num in data) {
                            /*三级目录*/
                            for (var num2 in data[num]["city"]) {
                                if (data[num]["city"][num2]["name"] == vals) {
                                    for (var i = 0; i < data[num]["city"][num2]["area"].length; i++) {
                                        arry.push("<div class='area item'>" + data[num]["city"][num2]["area"][i] + "</div>");

                                    }
                                }
                            }
                        }
                        lack.empty();
                        lack.append(arry.join(""));
                    }
                });
            });
            /*选择*/
            lack.on("click", ".area", function() {
                arry = [];
                var vals = $(this).text();
                address += vals;
                //存储地址数据
                localStorage.setItem("address", address);
                location.href = "addressAdd.html";
            });
        });