var flag = true;
	$(window).on("scroll", function() {
		var p_hg = Math.ceil($("body")[0].scrollHeight) - 50; // 获取当前元素高度
		var s_top = Math.ceil($("body")[0].scrollTop + $(this).height()); // 获取滚动的高度
		var arry = [];
		if (p_hg <= s_top && flag) {
			flag = false;
			tips({
				skin : "loading"
			}); // 加载进度条
			arry = [];
			var id = "voucherList";
			var voucherList = $("#" + id);
			var pageNo = voucherList.data("page-no");
			var pageSize = voucherList.data("page-size");
			var totalPage = voucherList.data("total-page");
			var params = {};

			if (pageNo && pageSize && totalPage) {
				pageNo = parseInt(pageNo);
				pageSize = parseInt(pageSize);
				totalPage = parseInt(totalPage);

				if (pageNo++ >= totalPage) {
					tips.loadClose();
					return;
				}

				params.pageNo = pageNo;
				params.pageSize = pageSize;
			}
			$.post("api/getVoucherList.html", params, function(result) {
				tips.loadClose(); // 关闭进度条
				if (result && result.success) {
					var data = result.data;
					if (data) {
						renderVoucherList(data, id);
					} else {
						tips({
							message : "没有更多数据啦！",
							skin : "msg"
						});
					}

				}
			});

		}
	});
	function renderVoucherList(data, id) {
		var voucherList = $("#" + id);
		voucherList.data("page-no", data.pageNo);
		voucherList.data("page-size", data.pageSize);
		voucherList.data("total-page", data.totalPage);
		var array = [];
		var html = "",zhtml="";
		for (var i = 0; i < data.results.length; i++) {
			var formatVoucherMoney=data.results[i].formatVoucherMoney;
			var formatDrawVoucher=data.results[i].formatDrawVoucher;
			if(formatDrawVoucher){
				zhtml='<div class="cupon-down clear">'+
		           '<div class="fr cupon-if">'+
		             	  '满 '+formatDrawVoucher+' 元可用'+
		           '</div>'+
		       '</div>';
			}
			html='<div class="cupon-item">'+
			            '<div class="cupon-top clear">'+
			            '<div class="fl cupon-price">'+
			                '<em>￥</em>'+formatVoucherMoney+
			            '</div>'+
			        '</div>'+zhtml+
			    '</div>';
			array.push(html);
		}
		voucherList.append(array.join(""));
		flag = true;
	}