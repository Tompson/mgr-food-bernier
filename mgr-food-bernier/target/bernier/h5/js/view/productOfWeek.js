/* function toggleBookBtn(){
	var currentWeek = $("#current-week").val();
	var week = $(".week-list .item.on").data("week");
	if (currentWeek == week) {
		$(".stms").show();
	}else{
		$(".stms").hide();
	}
	
} */
$(function(){
	//加入购物车监听
	addtoCartListen();
	//选好了，去购物车单击事件监听
	toShopTrolleyListen();
	//更改星期菜品
	$(".week-list .item").click(function() {
		$(this).parent().find(".item").removeClass("on");
		$(this).addClass("on");
//			toggleBookBtn();
		var week = $(this).data("week");
		tips({
			skin : "loading"
		}); //加载进度条
		$.ajax({
			url : "api/getWeekProduct.html",
			type : "post",
			async:false,
			data : {
				week : week
			},
			dataType : "json",
			success : function(result) {
				tips.loadClose(); //关闭进度条
				if (result && result.success) {
					refleshList(result.data, false);
				}
			},
			error : function() {
				tips.loadClose(); //关闭进度条
				tips({
					message:"系统繁忙！",
					skin : "msg"
				}); 
			}
		});
	});
});
//加入购物车监听
function addtoCartListen(){
	var currentWeek=$(".week-list .item.on").data("week");
	add2Cart("amountPrice",currentWeek);
}
//选好了，去购物车单击事件监听
function toShopTrolleyListen(){
	var currentWeek=$(".week-list .item.on").data("week");
	checkCurrentWeek(currentWeek);
}
//	toggleBookBtn();
/* $(".book-today").click(function(){
	window.location.href="productHome.html?v=1";
	return false;
}); */

//更新页面数据
//isAppend==true 追加 ，否则删除所有数据
function refleshList(data, isAppend) {
	var goodsList = $(".goodslist");
	if (!isAppend) {
		goodsList.empty();
		tips.msgClose();
	}
		
	for (var i = 0; i < data.categoryList.length; i++) {
		var html = '<p class="mt10 category-goods"><a name="category'+data.categoryList[i].categoryId+'">' + data.categoryList[i].cgoodsName + '</a></p>';
		for (var j = 0; j < data.goodsVoList.length; j++) {
			if (data.categoryList[i].cgoodsName == data.goodsVoList[j].cateGoryGoodsName) {
				var shtml='';
				var goodsStatus=data.goodsVoList[j].status;
				if(goodsStatus=='0'){
					shtml='<p class="mark service" data-goods-status="'+goodsStatus+'">在售</p>';
						
				}else if(goodsStatus=='1'){
					shtml='<p class="mark forbidden" data-goods-status="'+goodsStatus+'">已售完</p>';
				}
				html += '<div class="h-item">'
						+ '<div class="picture">'
						+ '<a href="productDetail.html?goodsId='+data.goodsVoList[j].id+'"><img src="'+data.goodsVoList[j].goodsPic+'" alt="">'+shtml+'</a>'
						+ '<p class="reco">'
						+ data.goodsVoList[j].intro
						+ '</p>'
						+ '</div>'
						+ '<div class="desc">'
						+ '<h6 class="tit">'
						+ data.goodsVoList[j].goodsname
						+ '</h6>'
						+ getGrade(data.goodsVoList[j].averageScore)
						+ '<p class="sale">销量'
						+ data.goodsVoList[j].sellnum + '份</p>'
						+ '<p class="price">￥'
						+ data.goodsVoList[j].price + '</p>'
						+'<p class="price_to">原价：￥'+data.goodsVoList[j].unit+'</p>'
						+ '<div class="shop add-2-cart" data-goods-id="'+data.goodsVoList[j].id+'"></div>' + '</div>'
						+ '</div>';
			}
		}
		goodsList.append(html);
	}
	//刷新总价
	$("#amountPrice").html(data.amountPrice);
	addtoCartListen();
	//移除绑定事件
	$('#toShopTrolley').unbind(); 
	toShopTrolleyListen();
}
