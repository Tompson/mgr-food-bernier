 $(function() {
        	//删除地址
            $(".user-address-del").on("click", function() {
            	var $userAddress=$(this).parents(".user-address");
            	var addressId=$userAddress.data("address-id");
            	if(!addressId){
            		return;
            	}
                tips({
                    message: "您确定要删除吗？",
                    skin: "confirm"
                }, function(){
                	$.get("api/deleteAddress.html",{addressId:addressId},function(result){
                		if(result&&result.success){
                			tips({
                                message: "删除成功",
                                skin: "msg"
                            });
                			$userAddress.remove();
                		}else{
                		}
                	},"json");
                    
                });
                return false;
            });
        	//设置为默认地址
            $(".user-addrss-op .fl").on("click", function() {
            	var _this=$(this);
            	var $userAddress=$(this).parents(".user-address");
            	var addressId=$userAddress.data("address-id");
            	if(!addressId){
            		return;
            	}
            	$.get("api/set2Default.html",{addressId:addressId,status:0},function(result){
            		if(result&&result.success){
		                $(".user-address-tip").removeClass("user-address-xz");
		                _this.find("em.user-address-tip").addClass("user-address-xz");
            		}else{
            		}
            	},"json");
            	
            });
        	//编辑地址
            $(".user-address-edit").on("click", function() {
            	var $userAddress=$(this).parents(".user-address");
            	var addressId=$userAddress.data("address-id");
            	if(!addressId){
            		return;
            	}
            	window.location.href="addressModify.html?addressId="+addressId;
                return false;
            });
        });