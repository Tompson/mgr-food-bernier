$(function() {
			//会员禁用状态
			/* var memberStatus = $("#memberStatus").val();
			if (memberStatus == 2) {
				tips({
					message : "您的账号已被禁用，您在本系统购物将受到限制！！！",
					skin : "alert"
				});
			} */
			/*$(".to-confirm-order").click(function() {
				if (checkShopCartIsEmpty()) {
					window.location.href = "orderConfirm.html";
				}
				return false;
			});*/
			//清空购物车
	
			$("#deleteAll").on("click", function() {
				var _this = this;
				var storeId=$("#storeId").val();
				tips({
					message : "您确定要清空购物车吗？",
					skin : "confirm"
				}, function() {
					$.ajax({
						url : "../shopcar/api/emptyShopCart.html",
						type : "get",
						data : {
							storeId:storeId
						},
						dataType : "json",
						success : function(result) {
							if (result) {
								if (result.success) {
									$("#goodsList").remove();
									$(".shop-not").show();
									$("#deleteAll").hide();
								} else {
									tips({
										message : "删除失败！",
										skin : "msg"
									});
								}
							} else {
								tips({
									message : "系统繁忙！",
									skin : "msg"
								});
							}
						},
						error : function() {
							tips({
								message : "系统繁忙！",
								skin : "msg"
							});
						}
					});
				});
			});
			//删除购物车该类菜品
			$(".delete-weeknum").on(
					"click",
					function() {
						var _this = $(this);
						var weeknum = _this.data("weeknum");
						var storeId=$("#storeId").val();
						if (!weeknum) {
							return;
						}
						tips({
							message : "您确定要将该类菜品移出购物车吗？",
							skin : "confirm"
						}, function() {
							$.ajax({
								url : "../order/api/deleteByWeeknum.html",
								type : "get",
								data : {
									weeknum : weeknum,
									storeId:storeId
								},
								dataType : "json",
								success : function(result) {
									if (result) {
										if (result.success) {
											var items = $("#goodsList").find(
													".category-item");
											if (items.length == 1) {
												$("#goodsList").remove();
												$(".shop-not").show();
												$("#deleteAll").hide();
												return;
											}
											_this.parents(".category-item")
													.remove();
										} else {
											tips({
												message : "删除失败！",
												skin : "msg"
											});
										}
									} else {
										tips({
											message : "系统繁忙！",
											skin : "msg"
										});
									}
								},
								error : function() {
									tips({
										message : "系统繁忙！",
										skin : "msg"
									});
								}
							});
						});
					});
			
			$(".toOrderConfirm").on("click",function(e){
				
				e.preventDefault();
				var overTime=$(this).data("over-time");
				if(overTime==1){
					return;
				}
				var weeknum=$(this).data("weeknum");
				var storeId=$("#storeId").val();
				$.ajax({
					url : "../order/api/validateBeforeOrderConfirm.html",
					type : "get",
					data : {
						weeknum : weeknum,
						storeId:storeId
					},
					dataType : "json",
					success : function(result) {
						if (result) {
							
							if(result.success){
								console.log(weeknum+" "+storeId);
								location.href="../order/orderConfirm.html?weeknum="+weeknum+"&&storeId="+storeId;
							}else{
								var errorCode=result.data;
								switch(errorCode){
								case "001":
									tips({
										message : result.msg,
										skin : "alert"
									});
									break;
								case "002":
									tips({
										message : result.msg,
										skin : "msg"
									});
									setTimeout(function(){
										tips.msgClose();
										location.reload();
									}, 300);
									break;
								case "003":
									tips({
										message : result.msg,
										skin : "alert"
									});
									break;
								case "004":
									tips({
										message : result.msg,
										skin : "confirm"
									},function(){
										location.href="../store/msgStore.html?id="+storeId;
									});
									break;
								case "007":
									tips({
										message : result.msg,
										skin : "msg"
									});
									setTimeout(function(){
										tips.msgClose();
										location.reload();
									}, 300);
									break;
//								case "008":
//									tips({
//										message : result.msg,
//										skin : "alert"
//									});
//									break;
								default:
//									tips({
//										message : "系统繁忙！",
//										skin : "msg"
//									});
									location.reload();
									break;
								}
							}
						} else {
							tips({
								message : "系统繁忙！",
								skin : "msg"
							});
						}
					},
					error : function() {
						tips({
							message : "系统繁忙！",
							skin : "msg"
						});
					}
				});
				
			});
		});