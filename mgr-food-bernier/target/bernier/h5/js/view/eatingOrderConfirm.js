var timeList;
var discoutType;//优惠类型；
var weeknum = 1;
var storeId=$("#storeId").val();
var tableId=$("#tableId").val();
$(function() {
	$.ajax({
		url:"../order/getDisCountType.html",
		type:"get",
		data:{
			storeId:storeId
		},
		dataType:"json",
		success:function(result){
			
			if(result){
				console.log(result);
				if(result.success){
					var vo=result.data;
					console.log(vo);
					discoutType=[];
					if(vo.voucher){
						discoutType.push('平台代金卷');
					}
					if(vo.memberDiscount){
						discoutType.push('会员折扣');
					}
					if(vo.full){
						discoutType.push('门店满减');
					}
					if(vo.hasDisCount){
						discoutType.push('门店折扣');
					}
				
				}else{
					discoutType=['平台代金卷','会员折扣','门店满减','门店折扣'];
				}
				console.log(discoutType);
			}else{
				tips({
					message:"系统繁忙",
					skin:"msg"
				});
			}
			//mobiscroll初始化
			mobiscrollInit();
		},
		error : function() {
			tips({
				message : "哎呀！您的网络有问题！",
				skin : "msg"
			});
		}
		
	});

	/*$.ajax({
		url : "../api/getShopTimeList.html",
		type : "get",
		data : {
			weeknum : weeknum
		},
		dataType : "json",
		success : function(result) {
			if (result) {
				if (result.success) {
					timeList = result.data;
				} else {
					tips({
						message : "获取当前时间失败！",
						skin : "msg"
					});
					timeList = [ '00:00', '00:15', '00:30', '00:45', '01:00', '01:15',
					        	  '01:30', '01:45', '02:00', '02:15', '02:30', '02:45', '03:00', '03:15',
					        	  '03:30', '03:45', '04:00', '04:15', '04:30', '04:45', '05:00', '05:15',
					        	  '05:30', '05:45', '06:00', '06:15', '06:30', '06:45', '07:00', '07:15',
					        	  '07:30', '07:45', '08:00', '08:15', '08:30', '08:45', '09:00', '09:15',
					        	  '09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15',
					        	  '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15',
					        	  '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15',
					        	  '15:30', '15:45', '16:00', '16:15', '16:30', '16:45', '17:00', '17:15',
					        	  '17:30', '17:45', '18:00', '18:15', '18:30', '18:45', '19:00', '19:15',
					        	  '19:30', '19:45', '20:00', '20:15', '20:30', '20:45', '21:00', '21:15',
					        	  '21:30', '21:45', '22:00', '22:15', '22:30', '22:45', '23:00', '23:15',
					        	  '23:30', '23:45' ];
				} 
				
				
			} else {
				tips({
					message : "系统繁忙！",
					skin : "msg"
				});
			}
		},
		error : function() {
			tips({
				message : "哎呀！您的网络有问题！",
				skin : "msg"
			});
		}
	});*/
	/*
	// 地址选择
	showAddress();
	// 确认选择地址
	$(".user-address")
			.on(
					"click",
					function() {
						$("#addressInfo").empty();
						var $this = $(this);
						var contacts = $this.find(".user-address-info span.fl")
								.html();
						var phone = $this.find(".user-address-info em.fr")
								.html();
						var addressName = $this.find(".user-address-word p")
								.html();

						var html = '<p class="firm-box—p1">收&nbsp;&nbsp;货&nbsp;&nbsp;人：<em class="mleft10">'
								+ contacts
								+ '</em></p><input id="contacts" name="contactsPerson" type="hidden" value="'
								+ contacts
								+ '"/>'
								+ '<p class="firm-box—p1">联系电话：<em class="mleft10">'
								+ phone
								+ '</em></p><input id="phone" name="contactsPhone" type="hidden" value="'
								+ phone
								+ '"/>'
								+ '<p class="firm-box—p1">收货地址：<em class="mleft10">'
								+ addressName
								+ '</em></p><input id="addressName" name="addresDetails" type="hidden" value="'
								+ addressName + '"/>';
						$("#addressInfo").append(html);
						$(".main-content,.back").show();
						$(".user-box").hide();
					});*/
	// 代金券选择
	$("#showVoucher").on("click", function() {
		$(".main-content,.back").hide();
		$("#voucherList").show();
	});

	// 取消使用代金券
	$("#cancle").on("click", function() {
		$(".main-content,.back").show();
		$("#voucherList").hide();
		$("#showVoucher a").empty();
		var html = '使用代金券';
		$("#showVoucher a").append(html);
		$("#ouponValue").val("");
		var amountPrice = parseFloat($("#amountPriceCopy").val());
		$("#amountPrice").html(amountPrice);
	});

	// 确认选择的代金券
	$(".voucher-item").on("click", function() {
		$("#showVoucher a").empty();
		var $this = $(this);
		var voucherPrice = $this.find(".cupon-price .price").html();
		var html = '代金券&nbsp;&nbsp;&nbsp;&nbsp;￥' + voucherPrice;
		$("#showVoucher a").append(html);
		$("#ouponValue").val(voucherPrice);
		var voucherId = $this.data("id");
		$("#voucherId").val(voucherId);

		var amountPrice=$("#amountPrice1").val();
		amountPrice = (amountPrice - voucherPrice).toFixed(2);
		if (amountPrice <= 0) {
			amountPrice = "0.00";
		}
		$("#amountPrice").html(amountPrice);
		$("#amountPriceCopy").val(amountPrice);
		$(".main-content,.back").show();
		$("#voucherList").hide();
	});
	$("#discount").on("change",function(){

		var type=$(this).val();
		if(type=='平台代金卷'){
			$("#showVoucher").show();
			/*var xcount=$("#xCount");
			if(xcount){
				xcount.remove();
			}*/
			$("#xCount").hide();
		}
		if(type=='会员折扣'){
			
			$.ajax({
			   url:"../order/eatingmemberCount.html",
			   type:"get",
			   dataType : "json",
			   data:{
				   tableId:tableId
				   ,storeId:storeId
			   },
			   success:function(result){
				 
				  if(result&&result.success){
					  var level=result.data;
					  console.log(level);
					  var html="<section class='box payment pd20 clear' id='xCount' name='xCount'>" +
					  		"<span class='fl' >"+level.name+"</span>" +
					  		"<input  class='deliver-time fr' type='text' style='color:red;' value='已减免￥"+level.discountMoney.toFixed(2)+"' >" +
					  		"</section>";
					  appendAfter(html);
					  var amountPrice=level.totalMoney-level.discountMoney;
					  amountPrice=amountPrice.toFixed(2);
					  $("#amountPrice").text(amountPrice);
					  $("#amountPriceCopy").val(amountPrice);
					  $("#showVoucher").hide();
					  $("#voucherId").val("");
				  }else{
					  tips({
						  message:"请求错误",
						  skin:"msg"
					  });
				  }
			   }
			});
		}
		if(type=='门店满减'){
			$.ajax({
				   url:"../order/eatingstoreFull.html",
				   type:"get",
				   dataType : "json",
				   data:{
					   tableId:tableId
					   ,storeId:storeId
				   },
				   success:function(result){
					  if(result&&result.success){
						  var data=result.data;
						  console.log(data);
						  var html="<section class='box payment pd20 clear' id='xCount' name='xCount'>" +
						  		"<span class='fl' >门店满减</span>" +
						  		"<input  class='deliver-time fr' type='text' style='color:red;' value='已减免￥"+data.substact.toFixed(2)+"' >" +
						  		"</section>";
						  appendAfter(html);
						  var amountPrice=data.totalMoney-data.substact;
						  amountPrice=amountPrice.toFixed(2);
						  $("#amountPrice").text(amountPrice);
						  $("#amountPriceCopy").val(amountPrice);
						  $("#showVoucher").hide();
						  $("#voucherId").val("");
					  }else{
						  tips({
							  message:"请求错误",
							  skin:"msg"
						  });
					  }
				   }
				});
		}
		if(type=='门店折扣'){
			$.ajax({
				   url:"../order/eatingstoreDiscount.html",
				   type:"get",
				   dataType : "json",
				   data:{
					   tableId:tableId
					   ,storeId:storeId
				   },
				   success:function(result){
					  if(result&&result.success){
						  var data=result.data;
						  console.log(data);
						  var html="<section class='box payment pd20 clear' id='xCount' name='xCount'>" +
						  		"<span class='fl' >门店折扣</span>" +
						  		"<input  class='deliver-time fr' type='text' style='color:red;' value='已减免￥"+data.substact.toFixed(2)+"' >" +
						  		"</section>";
						  appendAfter(html);
						  var amountPrice=data.totalMoney-data.substact;
						  amountPrice=amountPrice.toFixed(2);
						  $("#amountPrice").text(amountPrice);
						  $("#amountPriceCopy").val(amountPrice);
						  $("#showVoucher").hide();
						  $("#voucherId").val("");
					  }else{
						  tips({
							  message:"请求错误",
							  skin:"msg"
						  });
					  }
				   }
				});
		}
		
	})
});

function mobiscrollInit() {
	mobiscroll.scroller('#discount',{
		theme : mobiscroll.settings.theme,
		display : "bottom",
		lang : "zh",
		headerText:"优惠方式",
		showLabel:false,
		height:30,
		minWidth:80,
		wheels : [ [ {
			label : 'discountType',
			data : discoutType
		} ] ]
	});
}
function validateForm() {
	
	var flag = false;
	var amountPrice = parseFloat($("#amountPrice").html());
	if (amountPrice <= 0) {
		tips({
			message : "订单金额低于起付金额！",
			skin : "alert"
		});
		return flag;
	}
	var goodsId = $("#goodsId").val();
	var amount = $("#amount").val();
	var params = {};
	if (goodsId && amount) {
		params.goodsId = goodsId;
		params.amount = amount;
	}
	params.storeId=storeId;
	params.tableId=tableId;
	$.ajax({
		url : "../order/api/validateBeforeOrderConfirm1.html",
		type : "get",
		async : false,
		data : params,
		dataType : "json",
		success : function(result) {
			debugger;
			if (result) {
				if (result.success) {
				/*	var orderType = $("#orderTypeHd").val();
					if (!orderType) {
						orderType = result.data == "book" ? "2" : "1";
						$("#orderTypeHd").val(orderType);
					}*/
					flag = true;
				} else {
					var errorCode = result.data;
					switch (errorCode) {
					case "001":
						tips({
							message : result.msg,
							skin : "alert"
						});
						break;
					case "002":
						tips({
							message : result.msg,
							skin : "msg"
						});
						break;
					case "003":
						tips({
							message : result.msg,
							skin : "msg"
						});
						break;
					case "004":
						tips({
							message : result.msg,
							skin : "msg"
						});
						break;
					case "005":
						tips({
							message : result.msg,
							skin : "alert"
						});
						break;
					case "006":
						tips({
							message : result.msg,
							skin : "alert"
						});
						break;
					case "007":
						tips({
							message : result.msg,
							skin : "msg"
						});
						break;
					case "008":
						tips({
							message : result.msg,
							skin : "confirm"
						}/*,function(){
							window.location.href="orderList.html"
						}*/);
						break;
					default:
						tips({
							message : "系统繁忙！",
							skin : "msg"
						});
						break;
					}
				}
			} else {
				tips({
					message : "系统繁忙！",
					skin : "msg"
				});
			}
		},
		error : function() {
			tips({
				message : "系统繁忙！",
				skin : "msg"
			});
		}
	});
	return flag;
}
function appendAfter(html){
	var xcount=$("#xCount");
	if(xcount){
		xcount.remove();
	}
	$("#selectVoucher").after(html);
}
