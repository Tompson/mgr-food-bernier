/*获取地址*/
        window.addEventListener("load", function() {
            console.log(localStorage.getItem("address"));
            var adTxt = document.getElementById("addressTxt");
            adTxt.value = localStorage.getItem("address");
            localStorage.removeItem("address");
        });
       
        $("#defaultSetting").click(function(){
        	if($(this).is(":checked")){
        		$(this).val(0)
        	}else{
        		$(this).val(1)
        	}
        })
        $(".save").click(function(){
	        var name=$("#realname").val().trim();
	        var phone=$("#phone").val().trim();
	        var area=$("#addressTxt").val().trim();
	        var addressDetail=$("#addressDetail").val().trim();
	        var status=$("#defaultSetting").val();
	        //校验
	        if(!!!area){
	        	tips({message:"所在地区不能为空！",skin:"alert"});
	        	return;
	        }
	        if(!!!addressDetail){
	        	tips({message:"详细地址不能为空！",skin:"alert"});
	        	return;
	        }
	        if(!!!name){
	        	tips({message:"联系人不能为空!",skin:"alert"});
	        	return;
	        }
	        var regPhone = /^1[34578]\d{9}$/     // 验证手机号码   验证规则：11位数字，以1开头。
	        if(!regPhone.test(phone)){
	        	tips({message:"请输入正确的电话号码！",skin:"alert"});
	        	return;
	        }
	        
	        var params={};
	        params.contacts=name;
	        params.phone=phone;
	        params.addressName=area+addressDetail;
	        params.status=status;
	        
	        $.post("api/saveAddress.html",params,function(result){
	        	if(result&&result.success){
	        		tips({message:"保存成功！",skin:"alert"});
	        		window.location.href="addressList.html";
	        	}else{
	        		tips({message:"保存失败！",skin:"alert"});
	        	}
	        },"json");
	        
        });