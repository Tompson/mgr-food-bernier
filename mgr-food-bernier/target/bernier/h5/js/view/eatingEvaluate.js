$(function(){
    		$(".save").click(function(){
    			debugger;
    			var orderId=$("#order-id").val();
    			var goodsScore=$(".choose-goods-score").html();
//    			var deliverScore=$(".choose-deliver-score").html();
    			var chiefScore=$(".choose-chief-score").html();
    			var content=$("#goodsContent").val().trim();
//    			var deliverContent=$("#deliverContent").val().trim();
    			var chiefContent=$("#chiefContent").val().trim();
    			
    			if(!goodsScore){
    				tips({
	                    "message": "请评价一下菜品星级吧！",
	                    "skin": "alert"
	                });
    				return;
    			}
    			if(!chiefScore){
    				tips({
	                    "message": "请评价一下厨师吧！",
	                    "skin": "alert"
	                });
    				return;
    			}
    			var params={};
    			params.orderId=orderId;
    			params.commentGrade=goodsScore;
//    			params.deliverGrade=deliverScore;
    			params.chiefGrade=chiefScore;
    			params.content=content;
//    			params.deliverContent=deliverContent;
    			params.chiefContent=chiefContent;
    			$.post("../h5/order/api/saveOrderComment.html",params,function(result){
    				if(result&&result.success){
    		            tips({
    	                    "message": "恭喜您获得抽奖机会！点击确认抽奖！",
    	                    "skin": "confirm"
    	                },function(){
    		            	$.ajax({
    		            		url:"../h5/order/api/lotteryDraw.html",
    		            		type:"get",
    		            		asynch:false,
    		            		data:{category:1},
    		            		dataType:"json",
    		            		success:function(res){
    		            			if(res){
    		            				if(res.success){
//     		            					alert("恭喜您获得"+res.data+"元代金券，可在【我的】-【代金券】中查看");
    		            					tips({
    		            	                    "message": "恭喜您获得"+res.data+"元代金券，可在【我的】-【代金券】中查看",
    		            	                    "skin": "confirm"
    		            	                },function(){
    		            	                	window.location.href="../h5/order/orderList.html?orderId="+orderId;
    		            	                	return;
    		            	                });
    		            				}else{
    		            					tips({
    		            	                    "message": "很遗憾，本次您并未中奖~",
    		            	                    "skin": "confirm"
    		            	                },function(){
    		            	                	window.location.href="../h5/order/orderList.html?orderId="+orderId;
    		            	                	return;
    		            	                }); 
//     		            					alert("很遗憾，本次您并未中奖~");
    		            				}
    		            			}else{
    		            				tips({
    		                                "message": "系统繁忙！",
    		                                "skin": "msg"
    		                            });
    		            			}
    		            		},
    		            		error:function(){
    		            			tips({
    		                            "message": "系统繁忙！",
    		                            "skin": "msg"
    		                        });
    		            		}
    		            	});
    	                });
    					//跳转到订单列表页面
// 						window.location.href="orderList.html?orderId="+orderId;
					}else{
						tips({
    	                    "message": "评论保存失败！",
    	                    "skin": "alert"
    	                });
					}
    			},"json");
    		});
    		
    	});
    	
    
        window.addEventListener("load", function() {
            //评分
            var stars_one = document.querySelector(".stars_one"),
               /* stars_two = document.querySelector(".stars_two"),*/
                stars_three = document.querySelector(".stars_three");
            stars_one.addEventListener("touchend", function(e) {
                starsfc(e);
                
            }, false);
            /*stars_two.addEventListener("touchend", function(e) {
                starsfc(e);
            }, false);*/
            stars_three.addEventListener("touchend", function(e) {
                starsfc(e);
            }, false);

            function starsfc(e) {
                if (e.target && e.target.nodeName.toLowerCase() === "span") {
                	e.target.parentNode.querySelector(".choose-txt").textContent=e.target.getAttribute("data-sId");
                    e.target.classList.remove("stars-2");
                    e.target.classList.add("stars-1");
                    var zindex = parseInt(e.target.getAttribute("data-sId"));
                    var stars_icon = e.target.parentNode.getElementsByTagName("span");
                    for (var i = 0; i < stars_icon.length; i++) {
                        var data_sid = parseInt(stars_icon[i].getAttribute("data-sId"));
                        if (data_sid < zindex) {
                            stars_icon[i].classList.remove("stars-2");
                            stars_icon[i].classList.add("stars-1");
                        } else if (data_sid > zindex) {
                            stars_icon[i].classList.remove("stars-1");
                            stars_icon[i].classList.add("stars-2");
                        }
                    }
                }

            }
        });