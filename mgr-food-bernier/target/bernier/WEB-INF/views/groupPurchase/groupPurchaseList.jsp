<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
                 <div class="form-inline">
                  <form id="orderComment_table_form">
                  	
                	<div class="form-group">
                        <input type="text" class="form-control" id="datestart" name="startDate" placeholder="开始时间">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="dateend"  name="endDate" placeholder="结束时间">
                    </div>
                    
                    
                     <div class="form-group"> 
                        <input type="text" class="form-control" placeholder="企业名称" name="companyName" >
                     </div> 
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="联系人" name="contactPerson">
                    </div>
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="手机号" name="phone">
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="orderComment_table"></table>
                </div>
                
                <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>


<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/groupPurchase/page.html';  //TODO
var syslog_events = {
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/groupPurchase/deleteGroupPurchase.html?id='+row.id, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#orderComment_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	
    	'click .replyOrderComment':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/orderComment/toReply.html?cid='+row.cid;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	}
};


var orderComment_columns = [
// {field: 'memberNick',title: '会员昵称',valign: 'top',sortable: false},
{field: 'companyName',title: '企业名称',valign: 'top',sortable: false},
{field: 'companyAddress',title: '企业地址',valign: 'top',sortable: false},
{field: 'employeeNum',title: '员工数量',valign: 'top',sortable: false},
{field: 'otherRequest',title: '对菜品或其他的要求',valign: 'top',sortable: false},
{field: 'contactPerson',title: '联系人',valign: 'top',sortable: false},
{field: 'phone',title: '手机号',valign: 'top',sortable: false},
{field: 'createTime',title: '申请时间',valign: 'top',sortable: false},
// {field: 'replyContent',title: '评论回复',valign: 'top',sortable: false,formatter:replyContentFormatter},
// {field: 'parentId',title: '排序',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter1},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#orderComment_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: orderComment_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#orderComment_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#orderComment_table').bootstrapTable('refresh', null);
	}
	
	
	
	//回复和删除图标
	function operateFormatter(value,row,index){
		return ['<t:buttonOut url="/groupPurchase/deleteGroupPurchase.html"><button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button></t:buttonOut>'].join('');
	}
	
	
	 $(function() {
         //时间
         $('#datestart').datetimepicker({
             format: "yyyy-mm-dd",
             autoclose: true,
             minView: "month",
             maxView: "decade",
             todayBtn: true,
             pickerPosition: "bottom-left"
         }).on("click", function(ev) {
             $("#datestart").datetimepicker("setEndDate", $("#dateend").val());
         });

         $('#dateend').datetimepicker({
             format: "yyyy-mm-dd",
             autoclose: true,
             minView: "month",
             maxView: "decade",
             todayBtn: true,
             pickerPosition: "bottom-left"
         }).on("click", function(ev) {
             $("#dateend").datetimepicker("setStartDate", $("#datestart").val());
         });
     });
</script>