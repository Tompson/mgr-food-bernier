<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<body>

		<form class="registerform"  method="post" accept-charset="utf-8" action="<%=request.getContextPath()%>/system/updatePwd.html">
                    <div class="modal-body form-horizontal">
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label"><span class="required red">*</span>旧密码:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" placeholder="请输入旧密码" datatype="*6-32,comparePad" errormsg="旧密码错误" sucmsg="&nbsp;" nullmsg="必填">
                            </div>
                            <span class="col-sm-2 form-yz Validform_checktip"></span>
                        </div> 
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label"><span class="required red">*</span>新密码:</label>
                            <div class="col-sm-7">	
                                <input type="password" class="form-control" name="newPwd" placeholder=""  datatype="*6-32" errormsg="请填写6-32位的密码" sucmsg="&nbsp;" nullmsg="必填" />
           				 	</div>
                            <span class="col-sm-2 form-yz Validform_checktip"></span>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label"><span class="required red">*</span>确认密码:</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control" name="finalPwd" placeholder="" datatype="*6-32"  recheck="newPwd" errormsg="您两次输入的账号密码不一致！" sucmsg="&nbsp;" nullmsg="必填" />
                            </div>
                            <span class="col-sm-2 form-yz Validform_checktip"></span>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
                
                
<script type="text/javascript">
        $(function(){
		    $(".registerform").Validform({
		    	tiptype:3,
		    	datatype:{
		    	  	  "comparePad":function(gets,obj,curform,regxp){
	  		    			/*参数gets是获取到的表单元素值，
	  						  obj为当前表单元素，
	  						  curform为当前验证的表单，
	  						  regxp为内置的一些正则表达式的引用。*/
	  		    			var falg = false;
	  		            	$.ajax({ 
	  		            		url: '<%=request.getContextPath()%>/system/comparePad.html?pad='+gets, 
	  		            		context: document.body, 
	  		            		async: false,
	  		            		success: function(date){
	  		            			//console.log("The inspection results:"+date);
	  		            			falg = date;
	  		            		}
	  		            	});
	  		            	// console.log("The inspection results2:");
	  		            	return falg;
	  		    		}
		    	},
		    	ajaxPost:true,
				callback:function(data){
					if(data.success){
<%-- 						query('<%=request.getContextPath()%>/system/listView.html');  --%>
						layer.msg('修改密码成功',{icon:1});  
						$("#tjdp").modal('hide');
					}else{
						 layer.msg('修改密码失败',{icon:5});
					}
				}

		    });
		  });
        
        function myClose(){ //页面一加载先隐藏这个模态框
   	     $("#tjdp").modal('hide');
       }
 </script>           
     

</body>
</html>