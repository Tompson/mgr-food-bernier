<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 			<!--添加门店-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">添加门店</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/store/storeAdd.html">
                    <div class="model-box">
                     <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>门店名称：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="storeName" placeholder="请输入商户名称" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>登录账号：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="account" placeholder="请输入登录帐号" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>联系人：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="contactPersion" placeholder="请输入联系人" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>联系人手机号：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="contactPhone" placeholder="请输入联系人手机号" class="form-control col-md-7 col-xs-12"  nullmsg="必填" sucmsg=" " datatype=" /^1\d{10}$/" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>门店类型：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	 <select class="form-control col-md-7 col-xs-12"  name="storeType" >
                                      <option value="">请选择门店类型</option>
                      	              <option value="1" >加盟店</option>
                      	              <option value="2" >自营店</option>
                                  </select>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>门店电话：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input required="required" id="storePhone" name="storePhone" placeholder="请输入门店电话" class="form-control col-md-7 col-xs-12"  datatype="*1-24" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>门店地址：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input required="required" id="address" name="address" placeholder="请添加门店地址" class="form-control col-md-7 col-xs-12"  datatype="*1-24" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>经度：
                            </label>
                             <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  required="required" id="lng" name="lng" readOnly="true"  class="form-control col-md-7 col-xs-12"   sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>纬度：
                            </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input required="required" id="lat" name="lat"  readOnly="true"  class="form-control col-md-7 col-xs-12"   sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
						<!--  <div  id="mymap" style="width:560px;height:280px;margin:0 auto; "></div>
						  <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span> -->
						  
						  <div style="width:560px;height:280px;margin:0 auto;" id="container">
						    <!--<p>搜索:-->
						        <!--<input id="keyword" type="text" size="50"/> <input id="Search" type="button" value="搜索"-->
						                                                           <!--style="cursor: pointer"/>-->
						    <!--</p>-->
						</div>
                         <div class="form-group" style="margin-top: 5px;">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>营业时间：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="datestart" required="required" readonly="readonly" name="openTime" placeholder="9:00" class="" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text" style="border-radius: 3px; width: 46%;border: 1px solid #ccc; height: 34px;margin: 5px 10px 0 0;padding-left: 10px;">到
                                <input id="dateend" required="required" readonly="readonly" name="closeTime" placeholder="22:00" class="" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text" style="border-radius: 3px; width: 45%;border: 1px solid #ccc; height: 34px;    margin: 5px 0px 0 10px;padding-left: 10px;">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>起送价：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-startingPrice" required="required" name="startingPrice" placeholder="请输入起送价" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <script type="text/javascript">
                                 $("#first-startingPrice").keyup(function () {
                                    var reg = $(this).val().match(/\d+\.?\d{0,2}/);
                                    var txt = '';
                                    if (reg != null) {
                                        txt = reg[0];
                                    }
                                    $(this).val(txt);
                                }).change(function () {
                                    $(this).keyup();
                                });
                                </script>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>配送费：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-deliverFee" required="required" name="deliverFee" placeholder="请输入配送费" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                  <script type="text/javascript">
                                 $("#first-deliverFee").keyup(function () {
                                    var reg = $(this).val().match(/\d+\.?\d{0,2}/);
                                    var txt = '';
                                    if (reg != null) {
                                        txt = reg[0];
                                    }
                                    $(this).val(txt);
                                }).change(function () {
                                    $(this).keyup();
                                });
                                </script>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>配送距离(米)：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="deliverDistance" placeholder="请输入配送距离" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>开户行：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="bankName" placeholder="请输入开户银行名称" class="form-control col-md-7 col-xs-12" datatype="*1-20" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>开户姓名：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="bankPersionName" placeholder="请输入开户姓名" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>银行卡号：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="bankCardNo" placeholder="请输入银行卡号" class="form-control col-md-7 col-xs-12"  datatype="n1-36" sucmsg=" " nullmsg="必填" type="numner">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>银行预留手机号：
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="bankPhone" placeholder="请输入银行预留手机号" class="form-control col-md-7 col-xs-12"  errormsg="请填写正确的手机号码!"  nullmsg="必填" sucmsg=" " datatype=" /^1\d{10}$/">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>门店图片(<em>450*300</em>)：
                            
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-item form-item-wd">
                                    <img src="<%=request.getContextPath()%>/images/upload.png" alt="" style="width:150px;height: 100px">
                                      <input class="form-file" type="file" name="file" id="goodsPic1" value="" placeholder="">
                                      <input type="hidden" name="storePhoto"  id="gpic1"  datatype="*" sucmsg="&nbsp;" nullmsg="必填" />
                                    <span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
        </div>  
        
<script>

       
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('添加成功',{icon:1});    
    					$('#deliverMan_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    					console.log("dataURL:"+vo.data);
    				} else {
    					 layer.msg('添加失败',{icon:5});
    				}
    			}
            });
        });
 $(function(){
	 //时间
     $('#datestart').datetimepicker({
     	 format: 'hh:ii',
          autoclose: true,
          keyboardNavigation: false,
          startView: 1,
          minuteStep: 10,
          minView: 0,
          maxView: 1
     }).on("click", function(ev) {
         $("#datestart").datetimepicker("setEndDate", $("#dateend").val());
     });

     $('#dateend').datetimepicker({
     	 format: 'hh:ii',
          autoclose: true,
          keyboardNavigation: false,
          startView: 1,
          minuteStep: 10,
          minView: 0,
          maxView: 1
     }).on("click", function(ev) {
         $("#dateend").datetimepicker("setStartDate", $("#datestart").val());
       
     });
 //上传图片
 $(".form-file").on("change", function() {
     var imgFile = $(this)[0];//当前第一个jq对象 jquery对象就是一个数组对象.
     var id=$(this).attr("id");
     var filextension = imgFile.value.substring(imgFile.value.lastIndexOf("."), imgFile.value.length);
     filextension = filextension.toLowerCase();
     if ((filextension != '.jpg') && (filextension != '.gif') && (filextension != '.jpeg') && (filextension != '.png') && (filextension != '.bmp')) {
         alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
         imgFile.focus();
     } else {
         var path;
         if (document.all){//IE
             imgFile.select();
             path = document.selection.createRange().text;
             imgFile.previousSbiling.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")"; //使用滤镜效果 
         } else{//FF
        	 console.log(11111);
        	  $.ajaxFileUpload({  
	  	            url:"<%=request.getContextPath()%>/uploadPic/filesUpload.html",
	  	            secureuri:false,  
	  	            fileElementId:id,//file标签的id  
	  	            dataType: 'json',//返回数据的类型  
	  	            //data:{name:'logan'},//一同上传的数据  
	  	            success: function (data, status) {  
	  	            	$("#"+id).siblings('img')
	  	            	$("#"+id).siblings('img').attr('src',data);
	  	            	$("#"+id).siblings('input[type=hidden]').val(data);
	  	            	
// 	  	            	 $(imgFile).siblings("img").attr("src", data)
	  	            },
	  	            error: function (data, status, e) {  
	  	            	alert("文件上传失败!");   
	  	            }  
	  	        });
         }
      }
 	});
 });


 
	function statusChoice(){
		   if($("#status").is(":checked")){
			    $("#hiddenStatus0").val("0"); //启用
			}else{
				$("#hiddenStatus0").val("1"); //禁用
			}
	}
	   setTimeout(function () {
		   startMap();
	    }, 200);
	  // 百度地图API功能
	function startMap() {
		var map = new BMap.Map("container");
	    //创建地址解析的实例
	    var myGeo = new BMap.Geocoder();
	    var lng = "116.421172";
	    var lat = "39.924112";
	    //默认根据IP读取当前城市
	    var LocalCity = true;
	    //默认北京市,或经度纬度不正确情况下
	    if (!lng || !lat) {
	        lng = 116.331398;
	        lat = 39.897445;
	    } else {
	        LocalCity = false;
	    }
	    //设置地图中心坐标
	    map.centerAndZoom(new BMap.Point(lng, lat), 12);
	    //添加默认缩放平移控件
	    map.addControl(new BMap.NavigationControl());
	    //开启缩小放大
	    map.enableScrollWheelZoom();
	    //当前城市
	    if (LocalCity) {
	        var myCity = new BMap.LocalCity();
	        myCity.get(setCenter);
	    }else{
	        //设置覆盖物
	        var point = new BMap.Point(lng, lat);
	        setPoint(point);
	    }
	    //搜索
	    $('#Search').bind('click', function () {
	        //清空覆盖物
	        map.clearOverlays();
	        var searchTxt = $("#keyword").val();
	        myGeo.getPoint(searchTxt, function (point) {
	            setPoint(point);
	        }, "深圳市");

	    });
	    //搜索
	    $('#biao').bind('click', function () {
	        //清空覆盖物
	        map.clearOverlays();
	        var center = map.getCenter();
	        var point = new BMap.Point(center.lng, center.lat);
	        setPoint(point);
	    });
	    /**
	     * 回调函数
	     */
	    function setCenter(result) {
	        var cityName = result.name;
	        //把地图设置当前城市
	        map.setCenter(cityName);
	    }
	    /**
	     * 设置覆盖物，获取坐标
	     * @param point
	     */
	    function setPoint(point) {
	        if (point) {
	            //坐标赋值
	          
	            $('#lng').val(point.lng);
	            $('#lat').val(point.lat);
	            Geocoder(point);

	            map.centerAndZoom(point, 12);
	            var marker = new BMap.Marker(point);
	            map.addOverlay(marker);
	            marker.enableDragging();//可以拖动
	            //创建信息窗口
	            var infoWindow = new BMap.InfoWindow("拖动我选择地址");
	            //显示窗口
	            marker.openInfoWindow(infoWindow);
	            //点击监听
	            marker.addEventListener("click", function () {
	                this.openInfoWindow(infoWindow);
	            });
	            //拖动监听
	            marker.addEventListener("dragend", function (e) {
	                //坐标赋值
	                $('#lng').val(e.point.lng);
	                $('#lat').val(e.point.lat);
	                Geocoder(e.point);
	            });
	        }
	    }
	    /**
	     * 根据坐标获取地址
	     * @param point
	     * @constructor
	     */
	    function Geocoder(point) {
	    	var map = new BMap.Map("allmap");
	    	var gc = new BMap.Geocoder(); 
	    	gc.getLocation(point, function(rs) {
	    	console.log(rs.address);
	        $('#address').val(rs.address);
	    	}); 
	    }


	}
	  
	
</script> 
