<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html lang="en">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<head>
<meta charset="utf-8" />
<title>饭好粥道-确认订单</title>
<meta name="keywords" content="确认订单" />
<meta name="description" content="确认订单" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/user.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
	<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/storelist.css" />
	
<!-- mobiscroll start -->
    <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/jquery.js"></script>
<%--  	<link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.animation.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.icons.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.frame.android-holo.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.frame.ios.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.frame.jqm.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.frame.wp.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.scroller.android-holo.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.scroller.ios.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.scroller.jqm.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.scroller.wp.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.image.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.android-holo-light.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.wp-light.css" rel="stylesheet" type="text/css" /> --%>
<%--     <link href="<%=request.getContextPath()%>/h5/mobiscroll/css/mobiscroll.mobiscroll-dark.css" rel="stylesheet" type="text/css" /> --%>
<!--  mobiscroll end  -->
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
</head>

<body>

	<div class='alt-wrap hide'>
		<ul class='alt-cont absolute'>
			<li>堂食</li>
			<li>打包</li>
		</ul>
	</div>
	<div class="wrap">
		<!-- 头部 -->
		<header id="header" class="header">
			<a  style='top:0;' class="back" href="../shopcar/shopCar.html?tableId=${table.id}&storeId=${storeId}"></a>
			<div class="tit">确认订单</div>
		</header>
		<!-- /header -->
		<!-- 确认订单 -->
		
		<div class="main-content">
		   <form action="../order/eatingCreatOrder.html"  method="post" id="sysOrderForm">
			<input type="hidden" value="${table.id}" name="tableId" id="tableId"/>
			<input type="hidden" value="${storeId}"  name="storeId" id="storeId"/>
			<div style="padding-bottom: 1rem;">
			<section class="box payment pd20 clear " id="orderType">
				<div class='container dining-wrap' >
	        		<span class="fl">就餐方式</span>
	        		<input class='fr more-btn 'type="hidden"  id="orderType1"  name="orderType" value="4" style='margin-left: 0.1rem;width: 0.15rem;height:0.4rem;margin-top:0.25rem;background:url(../images/icon-tomore.png);background-size:100% 100%;' /> 
	        		<span class='fr'id="orderTypeCopy">堂食</span>		
	        	</div> 
			</section>
			<section class="box payment pd20 clear">
				<span class="fl">餐桌名称</span> <em class="word fr" id="tableName" >${table.name}</em>
			</section>
			<section class="box payment pd20 clear">
				<span class="fl">在线支付</span> <em class="sed fr"></em>
			</section>
			<section class="box payment pd20 clear" id="selectVoucher">
				<span class="fl">选择优惠</span>
				 <input id="discount" name="discountType" class="deliver-time fr" type="text"  value="" placeholder="请选择"> 
			</section>
			
			<section class="payment box pd20 bb1" id="showVoucher" style="display:none">
				<div class="voucher">
					<p><a href="#" title="">使用代金券</a></p>
					<input id="ouponValue" name="ouponValue" type="hidden" value=""/>
					<input id="voucherId" name="voucherId" type="hidden" value=""/>
				</div>
			</section>
			<div class="box">
				<textarea id="remark" name="remark" class="note" name="" maxlength="200" placeholder="备注"></textarea>
			</div>
				<div class="od-item">
					<div class="info pd20">
						<h6 class="tit">菜品清单</h6>
						<a href=""> <c:if test="${cartList!=null }">
								<!-- 购物车订单 -->
								<c:forEach items="${cartList }" var="vo">
									<div class="op-item">
										<p class="name">${vo.goodsName }</p>
										<i class="num">x${vo.num }</i><em class="price">￥<fmt:formatNumber type="number" value="${vo.formatUnitPrice*vo.num}" maxFractionDigits="2" groupingUsed="false" /></em>
									</div>
								</c:forEach>
							</c:if> <c:if test="${goods!=null }">
								<!-- 立即购买的订单 -->
								<div class="op-item">
									<p class="name">${goods.goodsname }</p>
									<i class="num">x${amount}</i><em class="price">￥${subtotalPrice }</em>
								</div>
								<input type="hidden" name="goodsId" id="goodsId" value="${goods.id }"/>
								<input type="hidden" name="amount" id="amount" value="${amount }"/>
							</c:if>
						</a>
					</div>
				</div>
			</div>
		<div class="od-money-filed clear">
			<p class="od-money-filed-fl color-ff5000 fl">
				金额:<span>￥<em id="amountPrice" >${amountPrice }</em></span><!-- 这个用来展现到页面上 -->
				<input type="hidden" id="amountPriceCopy" name="totalAmount" value="${amountPrice }"/><!-- 这个用来保存数据到order -->
				<input type="hidden" id="amountPrice1"  value="${amountPrice }"/><!-- 这个用来记录原来的总价，选择优惠卷的时候要用 -->
			</p>
			<button class="od-money-filed-fr fr btn-yellow sure-order" type="submit" onclick="return validateForm();">确认下单</button>
		</div>
		</form>
		</div>
		
		<section class="user-box"  style="display: none;" >
			<c:forEach items="${allAddress }" var="address">
				<div class="user-address mt20"
					data-address-id="${address.addressId }">
					<div class="user-address-box">
						<div class="user-address-info clear">
							<span class="fl">${address.contacts }</span><em class="fr">${address.phone }</em>
						</div>
						<div class="user-address-word">
							<p>${address.addressName }</p>
						</div>
					</div>
				</div>
			</c:forEach>
		</section>
		
		<section class="box cupon-wrap pd20" id="voucherList" style="display:none;">
            <h2 class="cupon-num" id="cancle" style="background-color: #eee;text-align: center;">不使用代金券</h2>
            <c:forEach items="${voucherList }" var="voucher">
            <div class="cupon-item voucher-item" data-id="${voucher.id }">
                <div class="cupon-top clear">
                    <div class="fl cupon-price">
                        <em>￥</em><span class="price">${voucher.formatVoucherMoney }</span>
                    </div>
                </div>
                <c:if test="${voucher.formatDrawVoucher!=null }">
                <div class="cupon-down clear">
                    <div class="fr cupon-if">
                      	【 满${voucher.formatDrawVoucher }元可用】
                    </div>
                </div>
                </c:if>
            </div>
            </c:forEach>
        </section>
		
	</div>
	<script>
	
		$(function(){
			$('#orderType').click(function(){
				//debugger;
				$('.alt-wrap').toggle();
			});
			/* $('.alt-wrap').click(function(){
				var _this = $(this);
				_this.toggle();
				
			}); */
			$('.alt-wrap').on('click','li',function(){
				var _this = $(".alt-wrap");
				_this.toggle();
				var name =$(this).text();
				if(name=="堂食"){
					$("#orderType1").val("4");
				}else {
					$("#orderType1").val("3");
				}
				
				$("#orderTypeCopy").text(name);
				
			})
		});
	</script>
	<script
		src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<!-- mobiscroll start -->
	<script src="<%=request.getContextPath()%>/h5/mobiscroll/js/mobiscroll.dom.js"></script>
    <script src="<%=request.getContextPath()%>/h5/mobiscroll/js/mobiscroll.core.js"></script>
    <script src="<%=request.getContextPath()%>/h5/mobiscroll/js/mobiscroll.scrollview.js"></script>
    <script src="<%=request.getContextPath()%>/h5/mobiscroll/js/mobiscroll.frame.js"></script>
    <script src="<%=request.getContextPath()%>/h5/mobiscroll/js/mobiscroll.scroller.js"></script>
    <script src="<%=request.getContextPath()%>/h5/mobiscroll/js/i18n/mobiscroll.i18n.zh.js"></script>
	<!-- mobiscroll end -->
	<script src="<%=request.getContextPath()%>/h5/js/view/eatingOrderConfirm.js"></script>
	
</body>

</html>