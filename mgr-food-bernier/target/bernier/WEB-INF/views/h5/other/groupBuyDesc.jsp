<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>饭好粥道-团购团餐说明</title>
<meta name="keywords" content="团购团餐说明" />
<meta name="description" content="团购团餐说明" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/h5/css/user.css" />
<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/h5/js/style.min.js" type="text/javascript"></script>
</head>

<body>
    <div class="wrap">
        <header id="header" class="header">
            <a class="back" href="javascript:history.go(-1);"></a>
            <div class="tit">
               团购团餐说明
            </div>
        </header>
        <p class="groupon-p">团购、团餐是饭好粥道为团体订购给予便利及优惠的业务，具体说明如下：</p>
        <p class="groupon-tit">一、团购（即将上线）：</p>
        <p class="groupon-p">1、有关快餐团购的说明：当您订购餐品的份数（除汤品和饮料外）达到对应数量时可以享受优惠，比如，订购份数是3-4份为9折，5-7份为8.5折，8份以上为8折。实际优惠政策以饭好粥道即时推出的为准。
            <p class="groupon-p">2、有关正餐团购的说明：当您订购金额达到对应金额时可以享受优惠，比如，订购金额是80-150元为9折，151-250元为8.5折，251元以上为8折。实际优惠政策以饭好粥道即时推出的为准。</p>
            <section class="user-cont">
                <div class="user-exit">
                    <a class="btn" href="productHome.html?v=3">马上点餐</a>
                </div>
            </section>
            <p class="groupon-tit"> 二、团餐</p>
            <p class="groupon-p">饭好粥道对外承包企业团餐（含午餐、晚餐）的业务。该业务可为企业带来如下好处：</p>
            <p class="groupon-p">1、解决企业员工午餐晚餐困境，省事省成本；</p>
            <p class="groupon-p">2、饭好粥道以A级标准建造厨房并在线实时直播查看加工制作情况，餐品安全卫生有保障，食材新鲜无任何添加剂。菜品好口味且不断推出新口味；</p>
            <p class="groupon-p">3、企业团餐以饭好粥道菜品为主，还可按需求每周定制两个菜品；</p>
            <p class="groupon-p">4、企业团餐长期合作价格优惠，具体合作时间及对应优惠幅度以双方协商为准。合作时间最短为一个月。</p>
            <p class="groupon-p">如有团餐外包需求的企业，请填写以下信息申请：</p>
            <div class="user-box">
                <div class="group">
                    <label class="label">企业名称</label>
                    <div class="cont">
                        <input class="ad-txt" id="companyName" type="text" name="" placeholder="请输入企业名称" maxlength="30"/>
                    </div>
                </div>
                <div class="group">
                    <label class="label">企业地址</label>
                    <div class="cont">
                        <input class="ad-txt" id="companyAddress" type="text" name="" placeholder="请输入企业所在地址" maxlength="30"/>
                    </div>
                </div>
                <div class="group">
                    <label class="label">员工数量</label>
                    <div class="cont">
                        <input class="ad-txt" id="employeeNum" name="" type="number" placeholder="请输入员工数量" maxlength="4" oninput="if(value.length>4)value=value.slice(0,4)"/>
                    </div>
                </div>
                <div class="group">
                    <label class="label">联系人</label>
                    <div class="cont">
                        <input class="ad-txt" id="contactPerson" name="" type="text" placeholder="请输入联系人" maxlength="10"/>
                    </div>
                </div>
                <div class="group">
                    <label class="label">手机</label>
                    <div class="cont">
                        <input class="ad-txt" id="phone" name="" type="text" placeholder="请输入手机号码" maxlength="11" oninput="if(value.length>11)value=value.slice(0,11)"/>
                    </div>
                </div>
                <div class="group">
                    <label class="label">备注</label>
                    <div class="cont">
                        <textarea style="width: 90%;" id="otherRequest" class="ad-txt" name="" placeholder="对菜品或其他的要求" rows="4" maxlength="200"></textarea>
                    </div>
                </div>
                <div class="group">
                    <input class="gpon" id="saveInfo" type="button" value="提交">
                    <input class="gpon" type="button" onclick="javascript:history.go(-1);" value="关闭">
                </div>

            </div>
            <p class="groupon-p" style="margin-bottom:.2rem;">
           	 提交信息后两个工作日内将有饭好粥道工作人员与您联系，谢谢！
           	 <br/>
           	 客服电话：${partnerPhone }
            </p>
    </div>
    <%-- <script
		src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script> --%>
    <script
		src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script src="<%=request.getContextPath() %>/h5/js/view/groupBuyDesc.js"></script>
</body>
</html>