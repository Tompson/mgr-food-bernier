<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>
        饭好粥道-选择地区
    </title>
    <meta name="keywords" content="选择地区" />
    <meta name="description" content="选择地区" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <script src="<%=request.getContextPath() %>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <header id="header" class="header">
          <!--   <a class="back" href="javascript:history.go(-1);"></a> -->
            <div class="tit">
                选择地区
            </div>
        </header>
        <section class="lact-box">
        </section>
    </div>
    <script src="<%=request.getContextPath() %>/h5/js/lib/zepto.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/localityChoose.js"></script>
</body>

</html>