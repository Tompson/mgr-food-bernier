<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>饭好粥道-代金券领取</title>
<meta name="keywords" content="代金券领取" />
<meta name="description" content="代金券领取" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/user.css" />
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <!-- 头部 -->
        <header id="header" class="header">
            <a  href="javascript:history.go(-1);"></a>
            <div class="tit">
               代金券领取
            </div>
        </header>
        
        <section class="box cupon-wrap pd20">
            <h2 class="cupon-num" style="margin: .3rem .1rem;line-height: .4rem;">你的好友${member.memberNick }分享了一张饭好粥道的代金券给你，赶紧领取！！！手慢就没有了~</h2>
            <div >
            <div class="cupon-item" >
                <div class="cupon-top clear">
                    <div class="fl cupon-price">
                        <em>￥</em>
                        <fmt:formatNumber type="number" pattern="0.00" value="${voucher.voucherMoney }"/>
                    </div>
                     <div class="fr cupon-btn">
                      	  可用
                    </div>
                </div>
                <div class="cupon-down clear">
<!--                     <div class="fl cupon-date"> -->
<!--                        	 有效期至 2016/01/04 -->
<!--                     </div> -->
	                <c:if test="${voucher.drawVoucher!=null }">
                    <div class="fr cupon-if">
                      	【 满<fmt:formatNumber type="number" pattern="0.00" value="${voucher.drawVoucher }"/>元可用】
                    </div>
	                </c:if>
                </div>
            </div>
            </div>
            
            <div class="user-address-add">
	            <a href="javascript:;" id="gainVoucher"  data-voucher-no="${voucher.id }">领取</a>
	        </div>
        </section>
    </div>
    <script src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
    <script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
    <script type="text/javascript">
    	$(function(){
    		$("#gainVoucher").on("click",function(){
    			//获取代金券
    			var voucherId = $(this).data("voucher-no");
    			tips({
	                "skin": "loading"
	            });
    			$.ajax({
    				url:"api/gain.html",
    				type:"POST",
    				data:{voucherId : voucherId},
    				dataType:"json",
    				success:function(result){
    					tips.loadClose(); // 关闭进度条
    					if (result && result.success) {
    						tips({
    			                "message": "领取成功,可在【我的代金券】中查看",
    			                "skin": "msg"
    			            });
    						window.location.href="<%=request.getContextPath()%>/h5/index.html?v=1";
    					}else{
    						tips({
    			                "message": result.msg,
    			                "skin": "alert"
    			            });
    					}
    				},
    				error:function(){
    					tips.loadClose(); // 关闭进度条
    				}
    			});
    		});
    	});
    </script>
    
</body>

</html>
