<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
        饭好粥道-新增地址                 
    </title>
    <meta name="keywords" content="新增地址" />
    <meta name="description" content="新增地址" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <script src="<%=request.getContextPath() %>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath() %>/h5/js/ui/tip.min.js"></script>
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <header id="header" class="header">
            <a class="back" href="javascript:history.go(-1);"></a>
            <div class="tit">
                新增地址
            </div>
            <button class="save" type="button">保存</button>
        </header>
        <div class="user-box">
            <div class="group">
                <label class="label">所在地区</label>
                <div class="cont">
                    <a href="../localityChoose.html"><input class="ad-txt" id="addressTxt" type="text" placeholder="选择所在的地区" value="" disabled="" /></a>
                </div>
            </div>
            <div class="group">
                <label class="label">详细地址</label>
                <div class="cont">
                    <input class="ad-txt" id="addressDetail" type="text" placeholder="请输入详情地址" maxlength="30" />
                </div>
            </div>
            <div class="group">
                <label class="label">联系人</label>
                <div class="cont">
                    <input class="ad-txt" id="realname" type="text" placeholder="请输入收货人姓名" maxlength="10"/>
                </div>
            </div>

            <div class="group">
                <label class="label">手机号码</label>
                <div class="cont">
                    <input class="ad-txt" id="phone" type="number" placeholder="请输入手机号" oninput="if(value.length>11)value=value.slice(0,11)" />
                </div>
            </div>
            <div class="group mt10">
                <label class="label">设为默认地址</label>
                <div class="cont">
                    <div class="ck fr">
                        <input type="checkbox" id="defaultSetting" name="ck" placeholder="" value="0" checked="checked">
                        <p class="ck-btn"></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/addressAdd.js"></script>

</body>

</html>