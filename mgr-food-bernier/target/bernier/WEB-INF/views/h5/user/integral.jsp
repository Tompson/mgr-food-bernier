<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>
        饭好粥道-我的积分
    </title>
    <meta name="keywords" content="我的积分" />
    <meta name="description" content="我的积分" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <!-- 头部 -->
        <header id="header" class="header">
            <a class="back" href="javascript:history.go(-1);"></a>
            <div class="tit">
                积分
            </div>
        </header>

        <!-- 积分 -->
        <section class="box ">
            <div class="int-box">
                <div class="int-img">
                    <code>${integral }</code>
                    <p>我的积分</p>
                </div>
            </div>
        </section>
        <section class="box mt10 pd20 inte-word">
            <h2>活动规范</h2>
            <p>1.购买商品实付金额或充值金额按一定比例获得积分，计算取整舍小数。订单完成后无退款行为，积分将自动打入积分账户，以供使用。</p>
            <p>2.每次评价订单都会获得一定的积分。</p>
            <p>3.分享商品到社交平台，每天都能获取一定的积分。</p>
            <p>4.积分可用于兑换代金券。</p>
        </section>
         <section class="user-cont">
            <div class="user-exit">
                <a class="btn" href="exchangeVoucher.html">兑换代金劵</a>
            </div>
        </section>
    </div>
    <script src="<%=request.getContextPath() %>/h5/js/lib/zepto.min.js"></script>
    <script src="<%=request.getContextPath() %>/h5/js/ui/tip.min.js"></script>
</body>

</html>