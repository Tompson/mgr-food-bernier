<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
        饭好粥道-我的                 
    </title>
    <meta name="keywords" content="新增地址" />
    <meta name="description" content="新增地址" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <link href="<%=request.getContextPath()%>/h5/mobiscroll2/css/mobiscroll.android-ics.css" rel="stylesheet" type="text/css" />
    <link href="<%=request.getContextPath()%>/h5/mobiscroll2/css/mobiscroll.core.css" rel="stylesheet" type="text/css" />
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
    <style>
    	.link-icon{
    		width: .17rem;
		    height: .3rem;
		    background-position-x: 61.17647059%;
		    background-position-y: 41.93548387%;
		    margin-top: .13rem;
		    background-size: 1.02rem 1.54rem;
		    background-image: url(../images/user.png?100);
    	}
    </style>
</head>

<body>
    <div class="wrap">
        <header id="header" class="header">
            <a class="back" href="javascript:history.go(-1);"></a>
            <div class="tit">
                我的
            </div>
<!--             <button class="save" type="button">保存</button> -->
        </header>
        <div class="user-box">
        	<input type="hidden" value="${member.memberId }" id="memberId">
            <div class="group">
                <label class="label">昵称</label>
                <div class="cont">
                    <input class="ad-txt" id="nickname" type="text" placeholder="请输入昵称" maxlength="16" value="${member.memberNick }"/>
                </div>
            </div>
            <div class="group">
                <label class="label">生日</label>
                <div class="cont">
                	<em class="fr link-icon"></em>
                    <input class="ad-txt" id="birthday" readonly="readonly" type="text" placeholder="请选择" maxlength="10" value="${member.birthday }"/>
                </div>
            </div>

        </div>
        
        <div class="user-address-add">
            <a href="javascript:;" id="sureSaveInfo">确定</a>
        </div>
    </div>
<%--     <script src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script> --%>
    <script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath() %>/h5/js/ui/tip.min.js"></script>
    
     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.core.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.datetime.js" type="text/javascript"></script>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.select.js" type="text/javascript"></script> --%>

<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.jqm.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.ios.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.android.js" type="text/javascript"></script> --%>
    <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.android-ics.js" type="text/javascript"></script>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/mobiscroll.wp.js" type="text/javascript"></script> --%>

<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.core-de.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.core-hu.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.core-fr.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.core-es.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.core-it.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.datetime-de.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.datetime-hu.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.datetime-fr.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.datetime-es.js" type="text/javascript"></script> --%>
<%--     <script src="<%=request.getContextPath()%>/h5/mobiscroll2/js/i18n/mobiscroll.datetime-it.js" type="text/javascript"></script> --%>
    
    
    <script type="text/javascript">
    	$(function(){
    		var curr = new Date().getFullYear();
    		
            var opt = {
                    preset: 'date',
                	dateFormat: 'yyyy-mm-dd',
                    dateOrder: 'yyyymmdd',
                    dayNames: ['周日','周一','周二','周三','周四','周五','周六'],
                    dayNamesShort: ['日','一','二','三','四','五','六'],
                    dayText: '日',
                    hourText: '时',
                    minuteText: '分',
                    monthNames: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
                    monthNamesShort: ['一','二','三','四','五','六','七','八','九','十','十一','十二'],
                    monthText: '月',
                    secText: '秒',
                    timeFormat: 'HH:ii',
                    timeWheels: 'HHii',
                    yearText: '年',
                    nowText: '当前'
                }

    		
    		$("#birthday").scroller('destroy').scroller($.extend(opt, {
                theme: "android-ics light",
                mode: "scroller",
                lang: "zh",
                display: "modal",//bottom,modal
                animate: "none",
                setText: '确定',
                cancelText: '取消',
                headerText: "生日"
            }));
            
    		
    		$("#sureSaveInfo").on("touchend",function(){
    			//参数非空校验
    			var nickname = $("#nickname").val().trim();
    			var birthday = $("#birthday").val().trim();
    			var memberId = $("#memberId").val();
    			if(!nickname){
    				tips({
    					message:"请填写昵称",
    					skin:"alert"
    				});
    				return;
    			}
    			if(!birthday){
    				tips({
    					message:"请选择生日",
    					skin:"alert"
    				});
    				return;
    			}
    			
    			tips({
					skin : "loading"
				}); //加载进度条
    			$.post("api/saveInfo.html", {
    				memberId:memberId,
    				memberNick:nickname,
    				birthday:birthday
    			}, function(result) {
    				tips.loadClose(); //关闭进度条
    				if (result && result.success) {
    					window.location.href="../personal.html";
    				}else{
    					tips({
    						message : "系统繁忙！",
    						skin : "msg"
    					});
    				}
    			},"json");
    		});
    	});
    	
    </script>

</body>

</html>