<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en" style="height:100%">

<head>
<meta charset="utf-8" />
<title>饭好粥道-星期菜品展示</title>
<meta name="keywords" content="星期菜品" />
<meta name="description" content="星期菜品" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/details.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/h5/js/goodsCommon.js"></script>
</head>

<body style="height:100%">
	<div class="wrap">
		<header id="header" class="header">
			<a class="back" href="productHome.html?v=1"></a>
			<div class="tit">星期菜品展示</div>
<!-- 			<a href="personal.html" id="resetBookType" class="save" style="line-height: 1rem;">我的</a> -->
			<a href="personal.html?v=3" class="vedio" style="line-height: 1rem;color: #fff;"><img src="<%=request.getContextPath()%>/h5/images/wode.png" class="header-vides" style="width: .45rem; height: .45rem;" alt="">&nbsp;我的</a>
		</header>
		<nav class="week-nav">
<%-- 			<input id="current-week" type="hidden" value="${currentDayOfWeek }" /> --%>
			<input id="show-week" type="hidden" value="${week }" />
			<ul class="list week-list">
				<li class="item item1" data-week="1" id="aaa">星期一</li>
				<li class="item item2" data-week="2">星期二</li>
				<li class="item item3" data-week="3">星期三</li>
				<li class="item item4" data-week="4">星期四</li>
				<li class="item item5" data-week="5">星期五</li>
				<li class="item item6" data-week="6">星期六</li>
				<li class="item item7" data-week="7">星期日</li>
			</ul>
		</nav>
		<script type="text/javascript">
			//选中当前所在星期数
			var currentWeek = $("#show-week").val();
			if(currentWeek){
				$(".item").removeClass("on");
				$(".item" + currentWeek).addClass("on");
			}
		</script>
		<!-- 按分类跳锚点 -->
		<section class="plot">
		<c:forEach var="category" items="${categoryList }">
            <p class="plot-item" style=""><a href="#category${category.categoryId }">我要点：${category.cgoodsName }</a></p>
        </c:forEach>
        </section>
		<!-- 菜品展示 -->
		<section class="content mt10 goodslist" data-book-type="${sessionScope.book_type.bookType }">
			<c:forEach var="category" items="${categoryList }">
				<c:if test="${category!=null }">
					<p class="mt10 category-goods"><a name="category${category.categoryId }">${category.cgoodsName }</a></p>
				</c:if>
				<c:forEach items="${page.results }" var="goodsVo">
					<c:if test="${category.cgoodsName==goodsVo.cateGoryGoodsName }">
						<div class="h-item">
							<div class="picture">
								<a href="productDetail.html?goodsId=${goodsVo.id }">
								<img src="${goodsVo.goodsPic }" alt="">
								<c:if test="${goodsVo.status==0}">
									<p class="mark service" data-goods-status="${goodsVo.status}">在售</p>
								</c:if>
								<c:if test="${goodsVo.status==1}">
									<p class="mark forbidden" data-goods-status="${goodsVo.status}">已售完</p>
								</c:if>
								</a>
								<p class="reco">${goodsVo.intro }</p>
							</div>
							<div class="desc">
								<h6 class="tit">${goodsVo.goodsname }</h6>
								<jsp:include page="../grade.jsp" flush="true">
									<jsp:param value="${goodsVo.averageScore  }"
										name="averageGrades" />
								</jsp:include>
								<p class="sale">销量${goodsVo.sellnum }份</p>
								<p class="price">￥${goodsVo.price }</p>
								<p class="price_to">原价：￥${goodsVo.unit }</p>
								<div class="shop add-2-cart" data-goods-id="${goodsVo.id }"></div>
							</div>
						</div>
					</c:if>
				</c:forEach>
			</c:forEach>
		</section>
		<!-- 结算 -->
		<!-- <div class="stms" style="display: none;">
			<div class="stms-shop clear"></div>
			<a class="btn stms-btn btn-yellow book-today" href="#">预约今天</a>
		</div> -->
		<div class="stms">
			<div class="stms-shop clear">
				<a class="shop fl" href="shopTrolley.html?v=1"> </a>
				<div class="money fl">
					￥<span id="amountPrice">${amountPrice }</span>
				</div>
			</div>
			<a class="btn stms-btn btn-yellow" id="toShopTrolley" href="#">选好了</a>
		</div>
	</div>
	<script
		src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/productOfWeek.js"></script>
</body>

</html>