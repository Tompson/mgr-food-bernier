<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 			<!--添加配送员-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">分配厨师</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/sysOrder/chiefDis.html">
                	<input type="hidden" id="hiddenStatus0" name="orderId" value="${sysorder.orderId}">
                	<input type="hidden" id="hiddenStatus0" name="orderType" value="${sysorder.orderType}">
                    <div class="model-box">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"><span class="required red">*</span>厨师：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
	                                <select name="chiefId" id="userDj" onchange="" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填">
			                             <option  value="">请选择</option> 
			                             <c:forEach items="${chiefs}" var="item">
				                             	<option  value="${item.id}">${item.name}</option>
			                              </c:forEach>
	                         		</select>
                                <span class="col-xs-12 col-md-12  red  form-yz Validform_checktip"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
        </div>
    <!--/添加配送员-->

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('分配成功',{icon:1});    
    					$('#sysUser_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg('分配失败',{icon:5});
    				}
    			}
            });
        });
    </script>
 