<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
			<!--订单详情-->
			 <script type="text/javascript">
				   $(function(){
					   $(".ewm-img").mouseenter(function(){
						   var evm_offset=$(this).offset();
					        $("<div class='img-da'><img src="+ $(this).attr("src")+" alt=''></div>").appendTo($("body"));
					        $(".img-da").css({left:evm_offset.left+50,top:evm_offset.top-100})
					   });
					   $(".ewm-img").mouseleave(function(){
						   $(".img-da").remove(); 
					   });
					 
				   });
 			</script>
			
			
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">订单详情</h4>
                </div>
                <div class="model-box">
                    <div class="mt30 clear">
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">订单号：</label><span class="xj-text">${sysOrder.payNumber}</span>
                        </div>
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">支付金额(元)：</label><span class="xj-text">${sysOrder.payAmount}</span>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">会员名称：</label><span class="xj-text">${sysOrder.memberNick}</span>
                        </div>
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">手机号：</label><span class="xj-text">${sysOrder.contactsPhone}</span>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">订单总金额(元)：</label><span class="xj-text">${sysOrder.totalAmount}</span>
                        </div>
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">优惠价面额(元)：</label><span class="xj-text">${empty sysOrder.ouponValue? 0 : sysOrder.ouponValue}</span>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">支付方式：</label>
                            <c:if test="${sysOrder.payType eq 0}">
                             	<span class="xj-text">未支付</span>
                            </c:if>
                            <c:if test="${sysOrder.payType eq 1}">
                             	<span class="xj-text">支付宝</span>
                            </c:if>
                            <c:if test="${sysOrder.payType eq 2}">
                             	<span class="xj-text">微信支付</span>
                            </c:if>
                            <c:if test="${sysOrder.payType eq 3}">
                             	<span class="xj-text">现金支付</span>
                            </c:if>
                             <c:if test="${sysOrder.payType eq 4}">
                             	<span class="xj-text">余额支付</span>
                            </c:if>
                        </div>
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">支付状态：</label>
                            <c:if test="${sysOrder.payStatus eq 0}">
                            	<span class="xj-text">未支付</span>
                            </c:if>
                            <c:if test="${sysOrder.payStatus eq 1}">
                            	<span class="xj-text">支付成功</span>
                            </c:if>
                            <c:if test="${sysOrder.payStatus eq 2}">
                            	<span class="xj-text">支付失败</span>
                            </c:if>
                            <c:if test="${sysOrder.payStatus eq 3}">
                            	<span class="xj-text">退款中</span>
                            </c:if>
                            <c:if test="${sysOrder.payStatus eq 4}">
                            	<span class="xj-text">已退款</span>
                            </c:if>
                            <c:if test="${sysOrder.payStatus eq 5}">
                            	<span class="xj-text">退款失败</span>
                            </c:if>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">订单状态：</label>
                            <c:if test="${sysOrder.orderStatus eq 0}">
                            	<span class="xj-text">未处理</span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 1}">
                            	<span class="xj-text">已确认</span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 2}">
                            	<span class="xj-text">备货完成</span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 3}">
                            	<span class="xj-text">配送中</span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 4}">
                            	<span class="xj-text">订单成功 </span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 5}">
                            	<span class="xj-text">订单失败</span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 6}">
                            	<span class="xj-text">订单取消</span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 7}">
                            	<span class="xj-text">配送员接单</span>
                            </c:if>
                            <c:if test="${sysOrder.orderStatus eq 8}">
                            	<span class="xj-text">配送完成</span>
                            </c:if>
                             <c:if test="${sysOrder.orderStatus eq 9}">
                            	<span class="xj-text">订单已评价</span>
                            </c:if>
                        </div>
                        <div class="col-md-6 mt10">
                            <label class="xj-lal">配送员：</label><span class="xj-text">${sysOrder.deliverManName}</span>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="col-md-12 mt10">
                            <label class="xj-lal">收货地址：</label><span class="xj-text">${sysOrder.addresDetails}</span>
                        </div>
                         <div class="col-md-12 mt10">
                            <label class="xj-lal">联系人：</label><span class="xj-text">${sysOrder.contactsPerson}</span>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="col-md-12 mt10">
                            <label class="xj-lal">商品列表：</label>
                            <div class="xj-table">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>菜品名称</th>
                                            <th>菜品编号</th>
                                            <th>售卖价格</th>
<!--                                             <th>促销价格</th> -->
                                            <th>数量</th>
                                            <th>图片</th>
                                            <th>总金额</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                    	<c:forEach items="${sysOrder.orderGoods}" var="item">
	                                        <tr>
	                                            <td>${item.googsName}</td>
	                                            <td>${item.goodsId}</td>
	                                            <td>
	                                            	<fmt:formatNumber type="number" value="${item.price}" pattern="0.00" maxFractionDigits="2" groupingUsed="false" />
	                                            </td>
<!-- 	                                            <td> -->
<%-- 		                      						<fmt:formatNumber type="number" value="${item.priceSale}" pattern="0.00" maxFractionDigits="2" groupingUsed="false"/> --%>
<!-- 		                      					</td> -->
	                                            <td>${item.quantify}</td>
	                                            <td>
	                                                <image style=" width: 50px; height: 50px;" class="ewm-img" src="${item.goodsInfo.goodsPic}" alt="" />
	                                            </td>
	                                            <td>${item.pricePay}</td>
	                                        </tr>
                                         </c:forEach>
                                         
						 
						 
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <div class="clear">
                        <div class="col-md-12 mt10">
                            <label class="xj-lal">备注：</label><span class="xj-text">${sysOrder.remark}</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer mt30">
                    <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                </div>
            </div>
