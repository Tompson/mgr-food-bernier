<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
			<div class="form-inline">
				<form id="sysUser_table_form">
                     <div class="form-group">
                        <input type="text" class="form-control" id="datestart" name="startDate" placeholder="开始时间">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="dateend"  name="endDate" placeholder="结束时间">
                    </div>
                     <script type="text/javascript">
					                  var start = {
					                      elem: '#datestart',
					                      format: 'YYYY/MM/DD hh:mm:ss',
					                      min: '1900-01-01 00:00:00', //设定最小日期为当前日期
					                      max: '2099-06-16 23:59:59', //最大日期
					                      istime: true,
					                      istoday: false,
					                      choose: function(datas){
					                           end.min = datas; //开始日选好后，重置结束日的最小日期
					                           end.start = datas //将结束日的初始值设定为开始日
					                      }
					                  };
					                  var end = {
					                      elem: '#dateend',
					                      format: 'YYYY/MM/DD hh:mm:ss',
					                      min: laydate.now(),
					                      max: '2099-06-16 23:59:59',
					                      istime: true,
					                      istoday: false,
					                      choose: function(datas){
					                          start.max = datas; //结束日选好后，重置开始日的最大日期
					                      }
					                  };
					                  laydate(start);
					                  laydate(end);
					       </script>
                    <div class="form-group btn-group-vertical">
                        <input type="button" id="today" class="btn btn-primary" value="当天"/>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <input type="button" id="days" class="btn btn-primary" value="七天"/>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <input type="button" id="months" class="btn btn-primary" value="本月"/>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="订单号" name="payNumber">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="会员名" name="memberNick">
                    </div>
                    <div class="form-group">
                     <select name="orderStatus" class="form-control"> 
	                     <option value="">订单状态</option> <!-- 订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6订单取消 7配送员接单 8配送完成 9订单已评价  -->
	                     <option value="0">未处理</option>
	                     <option value="1">已确认</option>
	                     <option value="3">配送中</option>
	                     <option value="8">配送完成</option>
	                     <option value="4">订单成功</option>
	                     <option value="9">订单已评价</option>
	                     <option value="5">订单失败</option>
	                     <option value="6">订单取消</option>
	                     <option value="10">已分配厨师</option>
                   </select>
                    </div>
                    <div class="form-group">
                        <select name="payType" class="form-control"> <!-- 支付方式 0无，1支付宝，2微信支付，3现金支付，4余额支付   -->
                     		<option value="">支付方式</option>
                     		<option value="2">微信支付</option>
                     		<option value="1">支付宝</option>
                     		<option value="4">余额支付</option>
                  		 </select>
                    </div>
                    <div class="form-group">
                        <select name="orderType" class="form-control"> <!--  //预定类型:类型:1.普通订单，2预定订单    -->
                     		<option value="">订单类型</option> 
                     		<option value="1">外卖订单</option>
                     		<option value="2">预定订单</option>
                     		<option value="3">到店打包</option>
                     		<option value="4">到店堂食</option>
                   		</select>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="sysUser_table"></table>
                </div>
                
                <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                
                
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>
			
			
			<!-- 打印模态框 -->
		<!-- 	<div class="modal fade in" id="dydd" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" >打印订单</h4>
                </div>
                <div class="model-box">
                    <div style="display:none">
                        <object classid="clsid:AF33188F-6656-4549-99A6-E394F0CE4EA4" codebase="http://www.4Fang.net/4ff/sc_setup.exe" id="pazu" name="pazu">  
   <param name="License" value="FEA4D0E402A3068517158677DBD833D5">  
  </object>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12  col-xs-12 col-sm-12" style="font-size: 12px">
                            <iframe id="iframe_id" src="" width="100%" height="400" border="none" name="myifrm" style="overflow-x:hidden " frameborder="0" scrolling="yes"></iframe>
                        </div>
                    </div>
                    <div class="form-horizontal form-label-left mt10">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">请选择打印机：
                     </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="printers" class="form-control" onchange="listPapers()"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer mt30">
                    <button type="button" id="buttonPrint" class="btn btn-primary">打印</button>
                    <button type="button" class="btn btn btn-default" data-dismiss="modal">
              退出
           </button>
                </div>
            </div>
        </div>
    </div> -->
    
   <!-- 测试lodop打印  start-->
	<div class="modal fade in" id="dydd" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" >打印订单</h4>
                </div>
                <div class="model-box">
			       <!--  <object id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0>  
			            <embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0></embed>  
			        </object>   -->
                    <div class="form-group">
                        <div class="col-md-12  col-xs-12 col-sm-12" style="font-size: 12px">
	                   	<div id="printcontext"> 
	                    </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer mt30">
                   <button type="button"  class="btn btn-primary" onclick="myPrintA();">预览</button>
                    <button type="button"  class="btn btn-primary" onclick="myPrint();">直接打印</button>
                    <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                </div>
            </div>
        </div>
    </div>



<script type="text/javascript">
    /*当天*/
    var datestart=$("#datestart"),dateend=$("#dateend");
    $("#today").on("click",function(){
      datestart.val(laydate.now(0, "YYYY/MM/DD 00:00:00"));
      dateend.val(laydate.now(0, "YYYY/MM/DD hh:mm:ss"));
    });
    /*7天年内*/
     $("#days").on("click",function(){
            datestart.val(laydate.now(-7,"YYYY/MM/DD hh:mm:ss"));
            dateend.val(laydate.now(0,"YYYY/MM/DD hh:mm:ss"));

    });
     /*本月*/
      $("#months").on("click",function(){
          datestart.val(laydate.now(0,"YYYY/MM/01 00:00:00"));
          dateend.val(laydate.now(0,"YYYY/MM/DD hh:mm:ss"));
    });

var printhtml = "";
var url  = '<%=request.getContextPath()%>/sysOrder/page.html';   
var statOrderStatusEqUntreated='${empty statOrderStatusEqUntreated?0:statOrderStatusEqUntreated}';
console.log("统计订单状态等于未处理的数量:"+statOrderStatusEqUntreated);
var syslog_events = {
		'click .confirm':function(e,value,row,index){//确定订单
			 layer.confirm('确定会员已支付?', {btn: ['确定','取消']}, 
				 function(){
					$.ajax({ 
	      				url: '<%=request.getContextPath()%>/sysOrder/confirm.html?orderId='+row.orderId, 
	      				context: document.body, 
	      				async: false,
	      				dataType:'json',
	      				success: function(vo){
	      					$('#sysUser_table').bootstrapTable('refresh', null);
		      				if(vo.success){
// 		      					var html=vo.data;
// 		      					console.log("统计订单状态等于未处理的数量:"+html);
// 		      					if(!!html){
// 		      						$("#refreshMenu").html(html);
// 		      					}
		      					layer.msg('确定订单成功!',{icon:1});
		      				}else{
		      					layer.msg('确定订单失败!',{icon:5});
		      				}
	      				}
	      			});
			 }
			)
		},
    	'click .startDelivery':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/sysOrder/toStartDelivery.html?orderId='+row.orderId;
    		$("#admin_dialog_show").load(url,function(){//开始配送不用清空
    			$("#admin_dialog").modal('show');
    		}); 
    	},
    	//弹出分配厨师框
    	'click .startCheif':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/sysOrder/toChiefDis.html?orderId='+row.orderId;
    		$("#admin_dialog_show").load(url,function(){//开始配送不用清空
    			$("#admin_dialog").modal('show');
    		}); 
    	},
    	'click .orderDetails':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/sysOrder/orderDetails.html?orderId='+row.orderId;
    		$("#admin_dialog_show").load(url,function(){//订单详情不用清空
    			$("#admin_dialog").modal('show');
    		}); 
    	},
    	'click .endDelivery':function(e,value,row,index){  //配送完成
      	  layer.confirm('确定配送完成吗?', {btn: ['确定','取消']},  
      	   function(){
  			$.ajax({ 
    				url: '<%=request.getContextPath()%>/sysOrder/endDelivery.html?orderId='+row.orderId, 
    				context: document.body, 
    				async: false,
    				dataType:'json',
    				success: function(vo){
    					$('#sysUser_table').bootstrapTable('refresh', null);
        				if(vo.success){
        					layer.msg('确定配送完成!',{icon:1});
        				}else{
        					layer.msg('确定配送失败!',{icon:5});
        				}
    				}
    			});
     		 }
     	)
  },
    	'click .sureTakeDelivery':function(e,value,row,index){ //确定收货
    	  layer.confirm('确定会员已收到货吗?', {btn: ['确定','取消']},  
    	   function(){
			$.ajax({ 
  				url: '<%=request.getContextPath()%>/h5/api/sureReceive.html?orderId='+row.orderId, 
  				context: document.body, 
  				async: false,
  				dataType:'json',
  				success: function(vo){
  					$('#sysUser_table').bootstrapTable('refresh', null);
      				if(vo.success){
      					layer.msg('确定收货成功!',{icon:1});
      				}else{
      					layer.msg('确定收货失败!',{icon:5});
      				}
  				}
  			});
   		 }
   	)
},
'click .print':function(e,value,row,index){//确定打印
	printhtml  = "";
	var  aaa = $("#printcontext").load('<%=request.getContextPath()%>/sysOrder/print.html?orderId='+row.orderId,function(){
	printhtml = aaa.html();
	myPrint();
// myPrintA();//预览
// 	$("#dydd").modal("show");
	});
}
	
};


var goods_columns = [
{field: 'payNumber',title: '订单号',valign: 'top',sortable: false},
{field: 'memberNick',title: '会员名',valign: 'top',sortable: false},
{field: 'totalAmount',title: '订单金额(元)',valign: 'top',sortable: false,formatter:totalAmountFormatter},
{field: 'ouponValue',title: '优惠价面额',valign: 'top',sortable: false,formatter:ouponValueFormatter},
{field: 'payAmount',title: '支付金额(元)',valign: 'top',sortable: false,formatter:payAmountFormatter},
{field: 'payType',title: '支付方式',valign: 'top',sortable: false,formatter:operateFormatterPayType},
{field: 'payStatus',title: '支付状态',valign: 'top',sortable: false,formatter:operateFormatterPayStatus},
{field: 'orderTime',title: '下单时间',valign: 'top',sortable: false},
{field: 'orderType',title: '订单类型',valign: 'top',sortable: false,formatter:operateFormatterOrderType},
{field: 'bookTime',title: '预约时间',valign: 'top',sortable: false},
{field: 'deliverManName',title: '配送员',valign: 'top',sortable: false,formatter:deliverManNameFormatter},
{field: 'chiefName',title: '厨师',valign: 'top',sortable: false,formatter:chiefNameFormatter},
{field: 'storeName',title: '门店',valign: 'top',sortable: false,formatter:storeNameFormatter},
{field: 'orderStatus',title: '订单状态',valign: 'top',sortable: false,formatter:operateFormatterOrderStatus},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter:optionFormatter}
];


$('#sysUser_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: goods_columns,
	responseHandler:function(res) {//加载服务器数据之前的处理程序，可以用来格式化数据。参数：res为从服务器请求到的数据。
		console.log(res);
		console.log(res.statOrderStatusEqUntreated);
			$("#refreshMenu").html(res.statOrderStatusEqUntreated);
		return res;
	 }
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#sysUser_table_form").serializeObject()
					
				}
// 		$("#refreshMenu").html(statOrderStatusEqUntreated);//更新订单管理(里面的值)
		return temp
	}
	function query(){//条件查询
		$('#sysUser_table').bootstrapTable('refresh', null);
	}
	
	
	//PayType支付方式 0未支付，1支付宝，2微信支付，3现金支付，4余额支付 
	function operateFormatterPayType(value,row,index){
		if(value==0){
			return  '无';
		}
		if(value==1){
			return '支付宝';
		}
		if(value==2){
			return '微信支付';
		}
		if(value==3){
			return '现金支付';
		}
		if(value==4){
			return '余额支付 ';
		}
	}
	//PayStatus支付状态 0未支付 1支付成功  2支付失败  3退款中 4 已退款 5退款失败
	function operateFormatterPayStatus(value,row,index){
		if(value==0){
			return  '未支付';
		}
		if(value==1){
			return '支付成功';
		}
		if(value==2){
			return '支付失败';
		}
		if(value==3){
			return '退款中';
		}
		if(value==4){
			return '已退款 ';
		}
		if(value==5){
			return '退款失败 ';
		}
	}
	//预定类型:1.普通订单，2预定订单
	function operateFormatterOrderType(value,row,index){
		if(value == '1'){
			return '外卖订单';
		}
		if(value == '2'){
			return '预定订单';
		}
		if(value == '3'){
			return '到店打包';
		}
		if(value == '4'){
			return '到店堂食';
		}
// 		if(value == 3){
// 			return '明日中餐';
// 		}
	}
	//OrderStatus订单状态 0未处理 1已确认 2备货完成 3配送中 4订单成功 5订单失败 6订单取消 7配送员接单 8配送完成 9订单已评价  
	function operateFormatterOrderStatus(value,row,index){
		if(value==0){
			return  '未处理';
		}
		if(value==1){
			return '已确认';
		}
		if(value==2){
			return '备货完成';
		}
		if(value==3){
			return '配送中';
		}
		if(value==4){
			return '订单成功 ';
		}
		if(value==5){
			return '订单失败 ';
		}
		if(value==6){
			return '订单取消';
		}
		if(value==7){
			return '配送员接单';
		}
		if(value==8){
			return '配送完成 ';
		}
		if(value==9){
			return '订单已评价 ';
		}
		if(value==10){
			return '已分配厨师 ';
		}
	}
	
	
	
	 //开始配送和订单详情和打印按钮 //OrderStatus订单状态 0未处理 1已确认 2备货完成 3配送中 4订单成功 5订单失败 6订单取消 7配送员接单 8配送完成 9订单已评价  
	 function optionFormatter(value,row,index){
		 var html = "";
			if(row.orderStatus ==  0){//未处理
				html += '<t:buttonOut url="/sysOrder/confirm.html"><button href="javascript:void(0)" type="button" class="btn  btn-default btn-xs confirm">确认订单</button></t:buttonOut>';
				}
			if(row.orderStatus == 1){//已确认
				html += '<t:buttonOut url="/sysOrder/toChiefDis.html"><button href="javascript:void(0)" type="button" class="btn  btn-primary btn-xs startCheif">分配厨师</button></t:buttonOut>';
			}
			if(row.orderStatus == 10 && row.orderType!=4 && row.orderType!=3){//分配厨师之后才开始配送
				html += '<t:buttonOut url="/sysOrder/toStartDelivery.html"><button href="javascript:void(0)" type="button" class="btn  btn-primary btn-xs startDelivery">开始配送</button></t:buttonOut>';
			}
			if(row.orderStatus == 3 && row.orderType!=4){//3配送中  1支付成功 
				html += '<t:buttonOut url="/sysOrder/endDelivery.html"><button href="javascript:void(0)" type="button" class="btn  btn-success btn-xs endDelivery">配送完成</button></t:buttonOut>';
			}
			if(row.orderStatus == 8){//8配送完成  到店订单 分配厨师之后直接变为配送完成
				html += '<t:buttonOut url="/h5/api/sureReceive.html"><button href="javascript:void(0)" type="button" class="btn  btn-warning btn-xs sureTakeDelivery">确定收货</button></t:buttonOut>';
			}
			return [html+'<t:buttonOut url="/sysOrder/orderDetails.html"><button href="javascript:void(0)" type="button" class="btn  btn-info btn-xs orderDetails">订单详情</button></t:buttonOut>',
			        '<t:buttonOut url="/sysOrder/print.html"><button href="javascript:void(0)" type="button" class="btn  btn-danger btn-xs print">打印</button></t:buttonOut>'
			        ].join('');
		}
	 
	 
	 
	 
	 function totalAmountFormatter(value,row,index){
		 if(value == null){
			 return value = 0;
		 }else{
			 return value;
		 }
	 }
	 function payAmountFormatter(value,row,index){
		 if(value == null){
			 return value = 0;
		 }else{
			 return value;
		 }
	 }
	 function ouponValueFormatter(value,row,index){
		 if(value == null){
			 return value = 0;
		 }else{
			 return value;
		 }
	 }
	 function deliverManNameFormatter(value,row,index){
		 if(value == null){
			 return value = "无";
		 }else{
			 return value;
		 }
	 }
	 function chiefNameFormatter(value,row,index){
		 if(value == null){
			 return value = "无";
		 }else{
			 return value;
		 }
	 }
	 function storeNameFormatter(value,row,index){
		 if(value == null){
			 return value = "无";
		 }else{
			 return value;
		 }
	 }
    
    
    
    /* 测试打印开始 */
	var LODOP; //声明为全局变量       
	function myPrint() {//直接打印		  
		myAddHtml();       
		LODOP.PRINT();		       
	};
	function myPrintA(){//预览
// 			LODOP=getLodop();  	
// 			var strBodyStyle="<style>"+document.getElementById("style1").innerHTML+"</style>";
// 			LODOP.ADD_PRINT_TEXT(50,50,260,39,"打印与显示样式一致：");
// 			LODOP.ADD_PRINT_HTM(0,0,"100%","100%",printhtml);	
			myAddHtml()
			LODOP.PREVIEW();
	}
			       
	function myAddHtml() {       
		LODOP = getLodop();         
		LODOP.SET_PRINT_PAGESIZE(3,"58mm",0,'CB58');
		LODOP.ADD_PRINT_HTM(0,0,"100%","100%",printhtml);	       
	};	       
	function CheckIsInstall() {	 
		try{ 
		     LODOP=LODOP = getLodop();
			if (LODOP.VERSION) {
				 if (LODOP.CVERSION)
				 alert("当前有C-Lodop云打印可用!\n C-Lodop版本:"+LODOP.CVERSION+"(内含Lodop"+LODOP.VERSION+")"); 
				 else
				 alert("本机已成功安装了Lodop控件！\n 版本号:"+LODOP.VERSION); 
			};
		 }catch(err){ 
		 } 
	}; 
	/* 测试打印结束*/
	
	
</script>