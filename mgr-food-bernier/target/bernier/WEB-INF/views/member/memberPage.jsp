<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>  
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
</script>
</head>
<body>
<br />
                <div class="mt20">
                    <table data-toggle="table" class="table">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>会员名称</th>
                                <th>积分</th>
                                <th>注册时间</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2016101100212125</td>
                                <td>Jesse</td>
                                <td>100</td>
                                <td>2016-10-11 20:55</td>
                                <td>
                                    <div class="ace-tab">
                                        <input class="ace-ck" type="checkbox" name="" value="" checked="checked">
                                        <span class="ace-tg"></span>
                                    </div>
                                </td>
                                <td><button type="button" class="btn  btn-danger btn-xs">删除</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
						    <%@ include file="/WEB-INF/views/page.jsp"%>
</body>
</html>