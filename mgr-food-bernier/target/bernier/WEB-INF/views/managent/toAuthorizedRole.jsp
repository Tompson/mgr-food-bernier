<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/ztree/css/zTreeStyle.css"> 
<script src="<%=request.getContextPath()%>/js/ztree/jquery.ztree.core.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ztree/jquery.ztree.excheck.min.js"></script>
<head>


</head>
<body>
      <div class="modal-content" style=" width: 540px; height:460px; background-color: #fff;border: 1px solid #999;border: 1px solid rgba(0,0,0,.2);border-radius: 6px;box-shadow: 0 3px 9px rgba(0,0,0,.5);">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">角色授权</h4>
		</div>
        <div class="model-tjsq" style="width: 350px; height: 300px; margin: 0 auto; overflow: auto;border:1px solid #ccc;
         border-radius: 5px;">
          <ul id="treeDemo" class="ztree"></ul>
        </div>

        <div class="form-group" style="margin-top: 20px;text-align:center">
              <input class="form-btn" type="button" value="保存" onclick="authorizedRoles(${roleId })"  style="padding: 0 30px;height: 40px;background: #00a5a5;color: #fff;font-size: 16px;border: 1px solid #ccc;border-radius: 5px;"/>
              <input class="form-btn" type="button" value="关闭" onclick="myClose()" style="padding: 0 30px;height: 40px;background: #00a5a5;color: #fff;font-size: 16px;border: 1px solid #ccc;border-radius: 5px;"/>
        </div>
    </div>
		<script type="text/javascript">
    	 	var setting = {
    	          check: {
    	            enable: true,
    	            chkStyle: "checkbox",
    	            chkboxType: { "Y": "ps", "N": "ps" }
    	          },
    	          data: {
    	            simpleData: {
    	              enable: true,
    	              idKey: "id",
  	      			pIdKey: "pId",
  	      			rootPId: 0
    	            }
    	          },
    	          view: {
    	      		addDiyDom: addDiyDom
    	      	}
    	     

    	    };
    	 	function addDiyDom(treeId, treeNode) {
    	 		var aObj = $("#" + treeNode.tId + "_a");
    	 		if ($("#diyBtn_"+treeNode.id).length>0) return;
    	 		var editStr = "<input type='hidden' id='treenode' name='treenode' value='" +treeNode.id+ "'  />"
    	 		aObj.append(editStr);
    	 	};

			
    	 	var menuList = ${menuList};
    	 	console.log("menuList="+menuList);
    	 	var arrayObj = new Array();
    	 	for(var i=0;i<menuList.length;i++){
    	    	var obj=new Object(); 
    	    	obj.id = menuList[i].menuId;
    	    	obj.pId = menuList[i].parentMenuId;
    	    	obj.name = menuList[i].menuName;
    	    	if(menuList[i].checked){
    	    		obj.checked = true;
    	    	}else{
    	    		obj.checked = false;
    	    	}
    	    	obj.open = true;
    	    	arrayObj.push(obj);
    	 		
    	 	}
   	        $.fn.zTree.init($("#treeDemo"), setting, arrayObj);
        </script>
	<script type="text/javascript">
function myClose(){
	$("#admin_dialog").modal('hide');
}
function authorizedRoles(id){
    var flag=true;
	var treeObj=$.fn.zTree.getZTreeObj("treeDemo"),
nodes=treeObj.getCheckedNodes(true),
v="";
	var  menuId=[];
for(var i=0;i<nodes.length;i++){
v+=nodes[i].name + ",";
menuId.push(nodes[i].id);
}
console.log(menuId);	 
$.ajax({  
 type: "POST",  
 traditional: true,
 url: "<%=request.getContextPath()%>/sysUser/authorizedRole.html", 
 data: {"id":id,"menuId":menuId},  
 dataType:"json",  
 async:true,  
 cache:false,  
 success: function(msg){  
		
		console.log(msg);
<%-- 		query('<%=request.getContextPath()%>/role/list.html'); --%>
		console.log(msg.success);
			$("#tjtree").modal('hide');
			if(msg.success == true){
				layer.msg(msg.message,{icon:1});
				myClose();
			}else{
				layer.msg(msg.message,{icon:5});
			}
 },
}); 
}
</script>
		       
	</body>
	</html>