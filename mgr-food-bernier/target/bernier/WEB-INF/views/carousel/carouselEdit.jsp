<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 <script type="text/javascript">
	function statusChoice(){
		   if($("#status").is(":checked")){
			    $("#hiddenStatus0").val("1"); //启用
			}else{
				$("#hiddenStatus0").val("2"); //禁用
			}
	}
</script>  

 			<!--添加轮播图-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">编辑轮播图</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/carousel/carouselEdit.html">
                    <div class="model-box">
                     <input type="hidden"  name="id" value="${carouselFigure.id }">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>名称：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="name" value="${carouselFigure.name }" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>类型：
                            </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                            	 <select class="form-control col-md-7 col-xs-12" value="${carouselFigure.type}" name="type"   id="typeSelect">
                                      <option value="">请选择类型</option>
                      	              <option value="1" <c:if test="${carouselFigure.type eq '1' }">selected="selected"</c:if>>优惠卷</option>
                      	              <option value="2" <c:if test="${carouselFigure.type eq '2' }">selected="selected"</c:if>>其他</option>
                                  </select>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>外链/平台优惠卷编号：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="linkNo" value="${carouselFigure.linkNo }"class="form-control col-md-7 col-xs-12" datatype="*1-68" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group" id="photoUrlDiv">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>图片(<em>450*300</em>)：
                            
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" id="photoUrlDiv">
                                <div class="form-item form-item-wd">
                                <c:if test="${empty carouselFigure.photoUrl }">
                                	<img src="<%=request.getContextPath()%>/images/upload.png" alt="" style="width:150px;height: 100px">
                                      <input class="form-file" type="file" name="file" id="goodsPic1" value="" >
                                      <input type="hidden" name="photoUrl"  id="gpic1"  datatype="*" sucmsg="&nbsp;" nullmsg="必填" />
                                </c:if>
                                 <c:if test="${not empty carouselFigure.photoUrl }">
                                	<img src="${carouselFigure.photoUrl }" alt="" style="width:150px;height: 100px">
                                      <input class="form-file" type="file" name="file" id="goodsPic1" value="${carouselFigure.photoUrl}" >
                               			 <input type="hidden" name="photoUrl"  id="gpic1"   />
                                </c:if>   
                                    <span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
                                </div>
                            </div>
                        </div>
                        
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">状态：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="ace-tab">
                                    <input class="ace-ck" type="checkbox" id="status" checked="checked" onclick="statusChoice()">
                                    <span class="ace-tg"></span>
                                    <input type="hidden" id="hiddenStatus0" name="status" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
        </div>
    <!--/添加配送员-->

                
                
      <script>
      $("#typeSelect").val("${carouselFigure.type}");
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('修改成功',{icon:1});    
    					$('#deliverMan_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    					console.log("dataURL:"+vo.data);
    				} else {
    					 layer.msg(vo.message,{icon:5});
    				}
    			}
            });
        });
        $(function(){
        	 //上传图片
        	 $(".form-file").on("change", function() {
        	     var imgFile = $(this)[0];//当前第一个jq对象 jquery对象就是一个数组对象.
        	     var id=$(this).attr("id");
        	     var filextension = imgFile.value.substring(imgFile.value.lastIndexOf("."), imgFile.value.length);
        	     filextension = filextension.toLowerCase();
        	     if ((filextension != '.jpg') && (filextension != '.gif') && (filextension != '.jpeg') && (filextension != '.png') && (filextension != '.bmp')) {
        	         alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
        	         imgFile.focus();
        	     } else {
        	         var path;
        	         if (document.all){//IE
        	             imgFile.select();
        	             path = document.selection.createRange().text;
        	             imgFile.previousSbiling.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")"; //使用滤镜效果 
        	         } else{//FF
        	        	 console.log(11111);
        	        	  $.ajaxFileUpload({  
        		  	            url:"<%=request.getContextPath()%>/uploadPic/filesUpload.html",
        		  	            secureuri:false,  
        		  	            fileElementId:id,//file标签的id  
        		  	            dataType: 'json',//返回数据的类型  
        		  	            //data:{name:'logan'},//一同上传的数据  
        		  	            success: function (data, status) {  
        		  	            	$("#"+id).siblings('img')
        		  	            	$("#"+id).siblings('img').attr('src',data);
        		  	            	$("#"+id).siblings('input[type=hidden]').val(data);
        		  	            	
//        	 	  	            	 $(imgFile).siblings("img").attr("src", data)
        		  	            },
        		  	            error: function (data, status, e) {  
        		  	            	alert("文件上传失败!");   
        		  	            }  
        		  	        });
        	         }
        	      }
        	 	});
        	 });
        function statusChoice(){
 		   if($("#status").is(":checked")){
 			    $("#hiddenStatus0").val("0"); //启用
 			}else{
 				$("#hiddenStatus0").val("1"); //禁用
 			}
 	}
    </script>
    
 