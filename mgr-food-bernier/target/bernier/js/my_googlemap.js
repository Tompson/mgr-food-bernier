//var lat;
//var lng;
 //获取当前地理位置
 function getCurrentLocation()
 {
	
 	if (navigator.geolocation)
 	{
 		navigator.geolocation.getCurrentPosition(showPosition,showError);
 	}
 	else
 	{
 		console.log("该浏览器不支持定位。");
 	}
 	
 	function showPosition(position)
 	 {
 		var lat = parseFloat(position.coords.latitude);
 		var lng = parseFloat(position.coords.longitude);
 		addCookie("latitude",position.coords.latitude,null);
 		addCookie("longitude",position.coords.longitude,null);
 		
 		setCurrentLocation(lat,lng);
 		
 	 }
 	 function showError(error)
 	 {
 	 	switch(error.code) 
 	 	{
 	 		case error.PERMISSION_DENIED:
 	 			console.log("用户拒绝对获取地理位置的请求。");
 	 			
 	 			break;
 	 		case error.POSITION_UNAVAILABLE:
 	 			console.log("位置信息是不可用的。");
 	 			break;
 	 		case error.TIMEOUT:
 	 			console.log("请求用户地理位置超时。");
 	 			break;
 	 		case error.UNKNOWN_ERROR:
 	 			console.log("未知错误。");
 	 			break;
 	 	}
 	 	var ip = getLocationByIP();
 	 	console.log("ip:"+ip);
 	 	userip(ip);
 	 	var  massage=null;  
 	 	function userip(ip){    
 	 	$.getScript("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip="+ip,  function(){      
	 	 	var prov=remote_ip_info["province"];        
	 	 	var city=remote_ip_info["city"];           
	 	 	var thisAddess=prov+city          
	 	 	jsShow(thisAddess.toString())         
	 	 	});
 	 	}
 	 	function jsShow(address){     
 	 	var geocoder = new google.maps.Geocoder();     
 	 	if(geocoder){           
 	 	geocoder.geocode({'address': address }, function(results, status) {    
 	 	if (status == google.maps.GeocoderStatus.OK) {    
 	 	var GeoCode = ((results[0].geometry.location).toString().replace(/[()]/g, '')).split(",",2);    
 	 	var lat = parseFloat(GeoCode[0]);              
 	 	var lng = parseFloat(GeoCode[1]);  
 	 	addCookie("latitude",lat,null);
 		addCookie("longitude",lng,null);
 		var result=lat+','+lng;                 
 		setCurrentLocation(lat,lng);
 	 	}           
 	 	})        
 	 	} }
 	 	
 	 }
 }
 
 
	 /**
	  * 获取本地IP地址
	  */
	 function getLocationByIP()
	 {
		 var ip ;
		 $.ajax({
	  			url:projectPath+'/memberweb/getIpAddr.html',
	  			type:"post",
	  			contentType:"application/json",
	  			dataType:"json",
	  			async:false,
	  			success:function(result){
	  				ip = result;
	  			},
	  			error:function(){
	  				layer.msg("获取ip异常",{icon:5});
	  			}
	  		});
		 return ip;
	 }
 
 //获取当前地理位置
 function setCurrentLocation(lat,lng)
 {
	 var geocoder = new google.maps.Geocoder;
 		  
 		geocodeLatLng(geocoder);
 		function geocodeLatLng(geocoder) {
 		//经纬度+
 	 	var lat = getCookie("latitude");
 	 	var lng = getCookie("longitude");
 	 	console.log(lat)
 	 	console.log(lng)
 		  var latlng = {lat:parseFloat(lat), lng: parseFloat(lng)};
 		  geocoder.geocode({'location': latlng}, function(results, status) {
 		    if (status === google.maps.GeocoderStatus.OK) {
 		      if (results[0]) {
 					console.log(results);
 					console.log(results[0].address_components[3].long_name);
 		        	var loc = results[0].address_components[3].long_name;
 		        	addCookie("loc",loc,null);
 		        	judgeCity();
 		      } else {
 		        window.alert('No results found');
 		      }
 		    } else {
 		      window.alert('Geocoder failed due to: ' + status);
 		    }
 		  });
 		}
 }
