/**  
* @Title: ICommonDao.java
* @Package com.yiqi.dao
* @Description: 深圳市得壹科技有限公司
*/ 
package com.yiqi.dao.common;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Description: 数据库公用类dao
 * @author hejx
 * @date 2015年10月26日 
 *
 */
@Repository
public interface ICommonDao {

	/**
	 * 功能：根据数据库的序列名称获取序列值
	 * @param seqName 序列名称 增长为 1
	 * @return
	 */
	public String getSeqNextvalByName(@Param("seqName")String seqName);
	/**
	 * 事务下的批量 操作
	 * @param seqName
	 * @param seq 曾涨数
	 * @return
	 */
	public String getSeqNextvalByNameIncre(@Param("seqName")String seqName,@Param("seq")String seq);
}
