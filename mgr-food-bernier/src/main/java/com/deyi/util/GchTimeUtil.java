package com.deyi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GchTimeUtil {
	public static final String[] CHN_WEEK_DAYS = new String[] {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
	public static final SimpleDateFormat CHN_DATE_WEEK = new SimpleDateFormat("yyyy年MM月dd日 EEEE", Locale.CHINA);
	public static final SimpleDateFormat STD_DF_19 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat STD_DF_CN_20 = new SimpleDateFormat("yyyy年MM月dd日  HH:mm:ss");
	public static final SimpleDateFormat STD_DF_10 = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat MERGE_DF_16 = new SimpleDateFormat("yyyy年MM月dd日HH:mm");
	public static final SimpleDateFormat MERGE_YM_8 = new SimpleDateFormat("yyyy年MM月");
	public static final SimpleDateFormat MERGE_YMD = new SimpleDateFormat("yyyy年M月d日");
	public static final SimpleDateFormat STD_DF_TIMESTAMP = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public static String ConverTime(String value) {
        String res = null;
        try {
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
            long time = Long.parseLong(value);
            res = format.format(time * 1000L);
        } catch (NumberFormatException e) {
            res = "";
        }
        return res;
    }
    
    public static String getDFTimeStamp(Date date){
    	return STD_DF_TIMESTAMP.format(date);
    }
    
    public static String getDF19Date(Date date){
    	return STD_DF_19.format(date);
    }
    
    public static Date toDF19Date(String date){
    	try {
			return STD_DF_19.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String getMERGE_YM_8(Date date){
    	return MERGE_YM_8.format(date);
    }
    
    public static String getMERGE_YMD(Date date){
    	return MERGE_YMD.format(date);
    }
    
    public static String getCHNDateWithWeek(Date date){
    	return CHN_DATE_WEEK.format(date);
    }
    
    public static String getCHNDateWithWeekFromDF19(String dateString){
    	if(dateString==null){
    		return "";
    	}
		try {
			Date date = STD_DF_19.parse(dateString);
			return getCHNDateWithWeek(date);
		} catch (Exception e) {
//			e.printStackTrace();
		}	
		return "";
    }
    public static String getCHNDateWithWeekFromDF10(String dateString){
    	try {
    		Date date = STD_DF_10.parse(dateString);
    		return getCHNDateWithWeek(date);
    	} catch (ParseException e) {
//			e.printStackTrace();
    	}
    	return "";
    }
    
    public static String getSTD_DF_10(Date date){
    	return STD_DF_10.format(date);
    }
    
    public static String getSTD_DF_CN_19FromUnixTime10(Integer unixTime){
    	return getSTD_DF_CN_19(new Date(unixTime * 1000L));
    }
    
    public static String getSTD_DF_CN_19(Date date){
    	return STD_DF_CN_20.format(date);
    }
    
    public static Date parseDateFromMERGE_DF_16(String dateString){
    	try {
    		Date date = MERGE_DF_16.parse(dateString);
    		return date;
    	} catch (ParseException e) {
//			e.printStackTrace();
    	}
    	return null;
    }
    
    public static Date addTime(Date date, int seconds){
    	long timestamp = date.getTime() /1000 + seconds;
    	return new Date(timestamp * 1000);
    }
    
    public static int getUnixTime10(){
    	return (int)(System.currentTimeMillis() / 1000);
    }
    
    public static int getUnixTime10(Date date){
    	return (int)(date.getTime() / 1000);
    }
    
    public static Date getCurrentDateStartTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getNextDateStartTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentSaturdayStartTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentWeekStartTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentMonthStartTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentSeasonStartTime(){
    	Calendar cal = Calendar.getInstance();
    	int month = cal.get(Calendar.MONTH);
    	if(month <= 3){
    		month = 0;
    	}else if(month <= 6){
    		month = 3;
    	}else if(month <= 9){
    		month = 6;
    	}else if(month <= 12){
    		month = 9;
    	}
    	cal.set(Calendar.MONTH, month);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentYearStartTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.MONTH, 0);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
}
