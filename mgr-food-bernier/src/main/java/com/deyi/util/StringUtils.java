
package com.deyi.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * 描述：
 * 
 * @author tongbiao
 * @创建时间：Jan 21, 2010
 */
public class StringUtils {
	
	private static AtomicInteger num = new AtomicInteger(0);
	private final static int MAXNUM = 9999;
	private final static double PI = 3.14; // 圆周率
    private final static double R = 6371229;
	public static double getDistance(double longt1, double lat1, double longt2,double lat2) {
		 
        double x, y, distance;
        x = (longt2 - longt1) * PI * R
                * Math.cos(((lat1 + lat2) / 2) * PI / 180) / 180;
        y = (lat2 - lat1) * PI * R / 180;
        distance = Math.hypot(x, y);
        return distance;
    }
	/**
	 * 功能：不定长参数,其中一个参数为null或空则返回true,负责返回false
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isEmpty(String... str) {
		for (String s : str) {
			if (StringUtils.isEmpty(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 功能：不定长参数,其中一个参数为null或空或为空格字符串则返回true,负责返回false
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isBlank(String... str) {
		for (String s : str) {
			if (StringUtils.isBlank(s))
				return true;
		}
		return false;
	}

	/**
	 * 功能：判断字符串是否是数值. 默认允许有正负号,默认允许有小数点
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		boolean sign = true;
		int point_bef = Integer.MAX_VALUE;// 小数点前有几位
		int point_aft = Integer.MAX_VALUE;// 小数点后有几位
		return isNumeric(str, sign, point_bef, point_aft);
	}

	/**
	 * 功能：判断字符串是否是数值
	 * 
	 * @param str
	 * @param sign
	 *          是否允许有正负号
	 * @param point
	 *          是否允许有小数点
	 * @return
	 */
	public static boolean isNumeric(String str, boolean sign, boolean point) {
		int point_bef = Integer.MAX_VALUE;// 小数点前有几位
		int point_aft = Integer.MAX_VALUE;// 小数点后有几位
		if (!point)
			point_aft = 0;

		return isNumeric(str, sign, point_bef, point_aft);
	}

	public static boolean isquert(String str,String pic){
		String quert[] = str.split(",");
		for(int i=0;i<quert.length;i++){
			if(pic.equals(quert[i])){
				return true;
			}
		}
		return false;
	}
	/**  
	 * 将元为单位的转换为分 替换小数点，支持以逗号区分的金额 
	 *  
	 * @param amount 
	 * @return 
	 */  
	public static String changeY2F(String amount){  
	    String currency =  amount.replaceAll("\\$|\\￥|\\,", "");  //处理包含, ￥ 或者$的金额  
	    int index = currency.indexOf(".");  
	    int length = currency.length();  
	    Long amLong = 0l;  
	    if(index == -1){  
	        amLong = Long.valueOf(currency+"00");  
	    }else if(length - index >= 3){  
	        amLong = Long.valueOf((currency.substring(0, index+3)).replace(".", ""));  
	    }else if(length - index == 2){  
	        amLong = Long.valueOf((currency.substring(0, index+2)).replace(".", "")+0);  
	    }else{  
	        amLong = Long.valueOf((currency.substring(0, index+1)).replace(".", "")+"00");  
	    }  
	    return amLong.toString();  
	}
	/**
	 * 获取验证码
	 * @param result
	 * @return
	 */
	public static String getVerificationCode(){
		 String[] beforeShuffle = new String[] { "1","2", "3", "4", "5", "6", "7","8","9" };  
	     List list = Arrays.asList(beforeShuffle);  
	     Collections.shuffle(list);  
	     StringBuilder sb = new StringBuilder();  
	     for (int i = 0; i < list.size(); i++) {  
	         sb.append(list.get(i));  
	     }  
	     String afterShuffle = sb.toString();  
	     String result = afterShuffle.substring(5, 9);  
	     return result;
	}
	/**
	 * 生成制定长度的字母数字随机数
	 * @param length
	 * @return
	 */
    public static String random(int length){//传入的字符串的长度
        StringBuilder builder = new StringBuilder(length);
        for(int i = 0; i < length; i++){
            
            int r = (int) (Math.random()*3);
            int rn1=(int)(48+Math.random()*10);
            int rn2=(int)(65+Math.random()*26);
            int rn3=(int)(97+Math.random()*26);
            
            switch(r){
            case 0:   
                builder.append((char)rn1);
                break;
            case 1:
                builder.append((char)rn2);
                break;
            case 2:
                builder.append((char)rn3);
                break;
            }
        }
        return builder.toString();
    } 
	/**
	 * 功能：判断字符串是否是数值
	 * 
	 * @param str
	 * @param sign
	 *          是否允许有正负号
	 * @param point_bef
	 *          精度,小数点前有几位
	 * @param point_aft
	 *          精度,小数点后有几位,如果为0,则为整数
	 * 
	 * @return
	 */
	public static boolean isNumeric(String str, boolean sign, int point_bef, int point_aft) {
		if (StringUtils.isBlank(str)) {
			return false;
		}
		boolean point = true;// 是否允许小数点
		if (point_aft == 0) {
			point = false;// 不允许有小数点
		} else {
			point = true;
		}
		StringBuffer pat = new StringBuffer();
		if (sign) {
			pat.append("[+|-]?");
		}
		if (point_bef == 0) {
			pat.append("[0]");
		} else {
			pat.append("[0-9]{1,");
			pat.append(point_bef);
			pat.append("}");
		}
		if (point && str.indexOf(".") != -1) {// 允许小数点,并且有小数点
			pat.append("[.]");
			pat.append("[0-9]{1,");// 小数点后必须有一位
			pat.append(point_aft);
			pat.append("}");
		}
		Pattern pattern = Pattern.compile(pat.toString());
		if (!pattern.matcher(str).matches()) {
			return false;
		} else {// 排除如00.1,返回false
			if (str.indexOf(".") != -1 && str.substring(0, str.indexOf(".")).length() > 1
					&& Integer.valueOf(str.substring(0, str.indexOf("."))) == 0) {
				return false;
			} else {
				return true;
			}
		}
	}

	/**
	 * 功能：查看字符串是否有这个子字符串
	 * 
	 * @param str
	 *          主字符串
	 * @param substr
	 *          字字符串
	 * @return
	 */
	public static boolean hasSubstring(String str, String substr) {
		if (str == null || substr == null)
			return false;
		int strLen = str.length();
		int substrLen = substr.length();
		for (int i = 0; (i + substrLen) <= strLen; i++) {
			if (str.substring(i, i + substrLen).equalsIgnoreCase(substr)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 功能：验证是否是正确的手机号
	 * 
	 * @param mobile
	 * @return
	 */
	public static boolean isMobile(String mobile) {
		if (StringUtils.isBlank(mobile))
			return false;
		return Pattern.matches("^(1[3|5|8])\\d{9}$", mobile);
	}

	/**
	 * 
	 * 方法用途: 替换空字符串<br>
	 * 实现步骤: <br>
	 * @param field
	 * @return
	 */
	public static String replaceNull(String field) {
		if (field == null || "undefined".equals(field) || "null".equalsIgnoreCase(field) || "".equals(field.trim())) {
			return "";
		} else {
			return field;
		}
	}

	/**
	 * 判断是否为空
	 * 
	 * @param str
	 * @return boolean
	 * @date 2010-8-21
	 * @author yhd
	 */
	public static boolean strIsNull(String str) {
		if (str == null || "".equals(str.trim())) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 功能：字符串不以"/"结尾，则在串尾加"/"
	 * 
	 * @param s
	 * @return
	 */
	public static String addSlashInEnd(String s) {
		if (s != null) {
			if (!s.endsWith("/")) {
				s = s + "/";
			}
		}
		return s;
	}

	/**
	 * 功能：传入一个数字类型的参数，返回一个小数点后两位的小数
	 * 
	 * @param parm
	 */
	public static String ConverDouble(String parm) {
		if (isNumeric(parm, false, true)) {
			if (parm.indexOf(".") >= 0) {
				String value = parm.substring(parm.indexOf(".") + 1);
				if (value.length() == 1) {
					return parm + "0";
				} else if (value.length() > 2) {
					return parm.substring(0, parm.indexOf(".") + 1) + value.substring(0, 2);
				} else {
					return parm;
				}

			} else {
				return parm + ".00";
			}
		}
		return null;
	}

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean isBlank(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0)
			return true;
		for (int i = 0; i < strLen; i++)
			if (!Character.isWhitespace(str.charAt(i)))
				return false;

		return true;
	}

	public static boolean isNotBlank(String str) {
		return !isBlank(str);
	}

	public static String generatorNumber(int len) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < len; i++) {
			int t = Math.abs(new Random().nextInt()) % 10;
			sb.append(t);
		}
		return sb.toString();
	}

	/**
	 * 
	 * 方法用途: 生成新的文件名<br>
	 * 实现步骤: <br>
	 * @return
	 */
	public static String getNewFileName() {
		StringBuffer sb = new StringBuffer();
		Calendar calendar = Calendar.getInstance();
		sb.append(calendar.get(Calendar.YEAR));
		sb.append(calendar.get(Calendar.MONTH) + 1);
		sb.append(calendar.get(Calendar.DAY_OF_MONTH));
		sb.append(calendar.get(Calendar.HOUR_OF_DAY));
		sb.append(calendar.get(Calendar.MINUTE));
		sb.append(calendar.get(Calendar.SECOND));
		sb.append(num.addAndGet(1));
		if (num.get() > MAXNUM) {
			num.set(0);
		}
		return sb.toString();
	}

	/**
	   * 获取定长的字符串
	   * @param val
	   * @param len
	   * @return
	   */
	public static String getFixedLengthSeq(String val, int len) {
		int valLen = val.length();
		if (valLen < len) {
			StringBuffer seq = new StringBuffer();
			for (int i = 0; i < len - valLen; i++) {
				seq.append("0");
			}
			seq.append(val);
			return seq.toString();
		} else {
			return val.substring(0, len);
		}
	}
	/**
	   * 获取定长的字符串
	   * @param val
	   * @param len
	   * @return
	   */
	public static String getArryToString(String[] params) {
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < params.length; i++){
		sb. append(params[i]);
		}

		String newStr = sb.toString();
		return newStr;
	}
	
	/*将emoji表情替换成***

	@param source
    * @return 过滤后的字符串
    */
   public static String filterEmoji(String source) {
       if(StringUtils.isNotBlank(source)){
           return source.replaceAll("[\\ud800\\udc00-\\udbff\\udfff\\ud800-\\udfff]", "*");
       }else{
           return source;
       }
   }
   
   public static String getUUID32() {
		UUID uuid = UUID.randomUUID();
		String str = uuid.toString();
		return str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18) + str.substring(19, 23)
				+ str.substring(24);
	}
   
   /**
	 * 保留两位小数
	 * 
	 * @param money
	 *            以分为单位
	 * @return
	 */
	public static String formatMoney2(Integer money) {
		double d = money / 100D;
		DecimalFormat format = new DecimalFormat("#.##");
		return format.format(d);
	}
	
	/**
	 * 保留一位小数
	 * 
	 * @param money
	 *            以分为单位
	 * @return
	 */
	public static String formatMoney1(Integer money) {
		double d = money / 100D;
		DecimalFormat format = new DecimalFormat("#0.0");
		return format.format(d);
	}
	
	/**
	 * 保留一位小数
	 * 
	 * @param money
	 *            以分为单位
	 * @return
	 */
	public static String formatMoney0(Integer money) {
		double d = money / 100D;
		return String.valueOf(((int)d));
	}
	
	/**
	 * 精确到两位小数点
	 * @param money 以元为单位
	 * @return
	 */
	public static String formatMoney(double money) {
		DecimalFormat format = new DecimalFormat("#.##");//0  2
		return format.format(money);
	}
	
	/**
	 * 保留两位小数
	 * @param money 以元为单位
	 * @return
	 */
	public static String formatMoney2Byte(Object money) {
		DecimalFormat format = new DecimalFormat("0.00");//0.00  2.00
		return format.format(money);
	}
	/**
	 * 格式化分数
	 * 规则为#.#
	 * @param money
	 * @return
	 */
	public static String formatScore(Object score) {
		DecimalFormat format = new DecimalFormat("#.#");
		return format.format(score);
	}
	
	/**
	 * 指定长度截取，后面用...替代
	 * @param word
	 * @param length
	 * @return
	 */
	public static String trimStringAppendPoints(String word, int length){
		if(word != null && word.length() > length){
			return word.substring(0, length) + "...";
		}
		return word;
	}
	
	
	/**
	 * 驼峰命名转下划线命名
	 * @Author zjz Create on 2016年11月25日 上午11:04:43
	 * @param str
	 * @return
	 */
	public static String entityName2TableName(String str){//example1:toLowerCase
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < str.length(); i++) {
			char chr = str.charAt(i);
			if(Character.isUpperCase(chr) && i!=0){  //如果传过来的字符串包含大写的字符并且字符串数量不等于0
				stringBuffer.append("_").append(Character.toLowerCase(chr));//example1:to_lower_case
            }else{  //example2:append
            	stringBuffer.append(Character.toLowerCase(chr));  //example2:append
            }
		}
		return stringBuffer.toString();
	}
	

	public static void main(String arg[]) {
		// String str = "{extraPerson=, Single=100.0, Double=400.0, dd,
		// date=2010-03-02, currenyCode=RMB},{extraPerson=0.0, Single=300.0,
		// Double=400.0, date=2010-03-03, currenyCode=RMB},{extraPerson=0.0,
		// Single=300.0, Double=400.0, date=2010-03-04, currenyCode=RMB}";
		// String str = "{extraPerson=0.0, Single=300.0,
		// Double=400.0,date=2010-03-02, currenyCode=RMB}";
		// System.out.println(str);
		// stringToData(str);
		// System.out.println(isNumeric("1.11", true, 2, 2));

		// System.out.println(isMobile("18z22312539"));
		// System.out.println(generatorNumber(3));
		BigDecimal bigDecimal = new BigDecimal("6546131.2221326545");
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String format = decimalFormat.format(bigDecimal);
		System.out.println(format);
		
//		String str = "8888888-9999999|777777-5555";
//		String[] arr = str.split("\\|");
//		for (int i = 0; i < arr.length; i++) {
//			System.out.println(arr[i]);
//		}
	}
}
