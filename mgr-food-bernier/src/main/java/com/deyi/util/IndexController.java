//package com.deyi.util;
//
//import org.apache.log4j.Logger;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.ModelAndView;
///**
// * 
// * @author zjz Create on 2016年11月21日 下午5:48:05
// */
//@Controller
//public class IndexController {
//	private Logger log = Logger.getLogger(IndexController.class);
//	@RequestMapping(value={"/","index.html"})
//	public ModelAndView index(ModelAndView mav){
//		log.info("index ....");
//		return new ModelAndView("redirect:/system/index.html");//重定向 登录页面
//	}
// 
//}
