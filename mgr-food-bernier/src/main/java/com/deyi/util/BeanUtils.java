package com.deyi.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.CachedIntrospectionResults;


public abstract class BeanUtils {

	
	/**
	 * 判断对象是否为空
	 * @param obj
	 * @return
	 */
	public static boolean isEmObject(Object obj) {
		if("".equals(obj) || obj==null){
			 return true;
		}
		Class<?> cls = obj.getClass();
        Method[] methods = cls.getDeclaredMethods();
        Field[] fields = cls.getDeclaredFields();
        for (Field field : fields) {
            try {
                String fieldGetName = parGetName(field.getName());
                if (!checkGetMet(methods, fieldGetName)) {
                    continue;
                }
                Method fieldGetMet = cls.getMethod(fieldGetName, new Class[]{});
                Object fieldVal = fieldGetMet.invoke(obj, new Object[]{});
                if (fieldVal != null) {
                    if ("".equals(fieldVal)) {
                       return true;
                    } else {
                        return  false;
                    }
                }
            } catch (Exception e) {
                continue;
            }
        }
		return true;
	}
	
	 /**
     * 拼接某属性的 get方法
     *
     * @param fieldName
     * @return String
     */
    public static String parGetName(String fieldName) {
        if (null == fieldName || "".equals(fieldName)) {
            return null;
        }
        int startIndex = 0;
        if (fieldName.charAt(0) == '_')
            startIndex = 1;
        return "get"
                + fieldName.substring(startIndex, startIndex + 1).toUpperCase()
                + fieldName.substring(startIndex + 1);
    }

    /**
     * 判断是否存在某属性的 get方法
     *
     * @param methods
     * @param fieldGetMet
     * @return boolean
     */
    public static boolean checkGetMet(Method[] methods, String fieldGetMet) {
        for (Method met : methods) {
            if (fieldGetMet.equals(met.getName())) {
                return true;
            }
        }
        return false;
    }


	/**
	 * 获取对象中指定属性的值。
	 * 
	 * @param target
	 *            对象
	 * @param field
	 *            属性
	 * @return 返回对象中指定属性的值。
	 */
	public static Object getField(Object target, Field field) {
		try {
			boolean accessible = field.isAccessible();
			field.setAccessible(true);
			Object result = field.get(target);
			field.setAccessible(accessible);
			return result;
		} catch (Exception e) {
			throw new IllegalStateException("获取对象的属性[" + field.getName()
					+ "]值失败", e);
		}
	}
	
	
	

/*	*//** 
     * 利用spring实现bean之间属性复制 
     * @param source 
     * @param target 
     *//*  
    @SuppressWarnings("unchecked")  
    public static void copyPropertiesBySpring(Object source, Object target) throws Exception {  
        Class actualEditable = target.getClass();  
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);  
        for (int i = 0; i < targetPds.length; i++) {  
            PropertyDescriptor targetPd = targetPds[i];  
            if (targetPd.getWriteMethod() != null) {  
                PropertyDescriptor sourcePd = getPropertyDescriptor(source  
                        .getClass(), targetPd.getName());  
                if (sourcePd != null && sourcePd.getReadMethod() != null) {  
                    try {  
                        Method readMethod = sourcePd.getReadMethod();  
                        if (!Modifier.isPublic(readMethod.getDeclaringClass()  
                                .getModifiers())) {  
                            readMethod.setAccessible(true);  
                        }  
                        Object value = readMethod.invoke(source, new Object[0]);  
                        if(value == null)  
                            continue;  
                        //集合类判空处理  
                        if(value instanceof Collection) {  
//                          Collection newValue = (Collection)value;  
//                          if(newValue.size() <= 0)  
                                continue;  
                        }  
                        Method writeMethod = targetPd.getWriteMethod();  
                        if (!Modifier.isPublic(writeMethod.getDeclaringClass()  
                                .getModifiers())) {  
                            writeMethod.setAccessible(true);  
                        }  
                        writeMethod.invoke(target, new Object[] { value });  
                    } catch (Throwable ex) {  
                    }  
                }  
            }  
        }  
    }     
    *//** 
     * 获取指定类指定名称的属性描述符 
     * @param clazz 
     * @param propertyName 
     * @return 
     * @throws BeansException 
     *//*  
    @SuppressWarnings("unchecked")  
    public static PropertyDescriptor getPropertyDescriptor(Class clazz,  
            String propertyName) throws BeansException {  
        CachedIntrospectionResults cr = CachedIntrospectionResults.forClass(clazz);  
        return cr.getPropertyDescriptor(propertyName);  
    }  
      
      
    *//** 
     * 获取指定类得所有属性描述符 
     * @param clazz 
     * @return 
     * @throws BeansException 
     *//*  
    @SuppressWarnings("unchecked")  
    public static PropertyDescriptor[] getPropertyDescriptors(Class clazz) throws BeansException {  
        CachedIntrospectionResults cr = CachedIntrospectionResults.forClass(clazz);  
        return cr.getBeanInfo().getPropertyDescriptors();  
    }  */
}
