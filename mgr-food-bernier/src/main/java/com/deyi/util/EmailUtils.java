package com.deyi.util;

/** 
* @Author zjz
* @CreateTime:2016年10月24日 下午2:12:19 
* @Description 
*/

import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.deyi.util.PropertiesUtil;

/**
 * 发送简单邮件和html邮件的区别
 * @Description 
 * @Author tangj
 * @CreateTime:2016年10月24日 下午2:43:41
 */
public class EmailUtils {

	public static void sendMail(String to, String title, String context) {
		try {
			System.out.println("邮件发送开始");
			/*String smtphost = PropertiesUtil.getProperty("mail.smtphost");// smtp.163.com
			String fromUser = PropertiesUtil.getProperty("mail.fromUser");//dev@5deyi.com
			String userName = PropertiesUtil.getProperty("mail.fromUser.userName");//dev@5deyi.com
			String password = PropertiesUtil.getProperty("mail.fromUser.password");
			String timeout = PropertiesUtil.getProperty("mail.timeout");
			String serverport = PropertiesUtil.getProperty("mail.serverport");*/
			
			String smtphost = "smtp.qq.com";// smtp.163.com
			String fromUser = "964424723@qq.com";//dev@5deyi.com
			String userName = "964424723@qq.com";//dev@5deyi.com
			String password = "123123123yuan";
			String timeout = "8000";
			String serverport = PropertiesUtil.getProperty("25");
			
			
			JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();

			// 设定mail server
			senderImpl.setHost(smtphost);

			// 建立邮件消息,发送简单邮件和html邮件的区别
			MimeMessage mailMessage = senderImpl.createMimeMessage();
			MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, "UTF-8");
			String nick = javax.mail.internet.MimeUtility.encodeText("Tang");//设置昵称
			// 设置收件人，寄件人
			messageHelper.setTo(to);
			messageHelper.setFrom(nick + " <" + fromUser + ">");
			messageHelper.setSubject(title);

			// true 表示启动HTML格式的邮件
			messageHelper.setText(context, true);

			senderImpl.setUsername(userName); // 根据自己的情况,设置username
			senderImpl.setPassword(password); // 根据自己的情况, 设置password
			Properties prop = new Properties();
			prop.put("mail.smtp.auth", "true"); // 将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确
			prop.put("mail.smtp.timeout", timeout);
			prop.put("mail.smtp.port", serverport);
			senderImpl.setJavaMailProperties(prop);
			// 发送邮件
			System.out.println("邮件发送中...");
			senderImpl.send(mailMessage);

			System.out.println("邮件发送成功");
		} catch (Exception ex) {
			System.out.println("邮件发送失败");
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		sendMail("2609812354@qq.com", "这是一个测试邮件,请不要回复", "这是个测试的账号的内容，如果显示则表示邮件发送成功.");
	}
}
