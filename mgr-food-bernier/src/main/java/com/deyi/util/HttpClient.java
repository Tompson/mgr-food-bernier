package com.deyi.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
* @Description: TODO
* @author hejx
* @date 2015年12月12日 
*
 */
public class HttpClient {

	private static Logger log=LoggerFactory.getLogger(HttpClient.class);
	public static final String BOUNDARYSTR = "aifudao7816510d1hq";
	public static final String BOUNDARY = "--" + BOUNDARYSTR + "\r\n";

	/**
	 * 功能：
	 * 
	 * @param postURL
	 * @param requestBody
	 * @param sendCharset
	 * @param readCharset
	 * @return
	 * @throws Exception 
	 * @throws Exception 
	 */
	public static String send(String postURL, String requestBody, String sendCharset, String readCharset)
			throws Exception {
		HttpURLConnection httpConn = null;

		StringBuffer responseSb = new StringBuffer();

		try {
			URL postUrl = new URL(postURL);
			// 打开连接
			httpConn = (HttpURLConnection) postUrl.openConnection();
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			httpConn.setRequestMethod("POST");
			httpConn.setUseCaches(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=" + sendCharset);
			httpConn.setConnectTimeout(1000 * 10);
			httpConn.setReadTimeout(1000 * 10);
			httpConn.connect();
			DataOutputStream out = new DataOutputStream(httpConn.getOutputStream());
			out.write(requestBody.getBytes(sendCharset));
			out.flush();
			out.close();
			int status = httpConn.getResponseCode();
			
			if (status != HttpURLConnection.HTTP_OK) {
				log.info("发送请求失败，状态码：[" + status + "] 返回信息：" + httpConn.getResponseMessage());
				return null;
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), readCharset));
			String line = null;
			while ((line = reader.readLine()) != null) {
				responseSb.append(line.trim());
			}
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			log.info("发送请求[" + postURL + "]失败，" + ex.getMessage());
			throw ex;
		} finally {
			httpConn.disconnect();
		}
		return responseSb.toString().trim();
	}
	
	public static String getContext(HttpServletRequest request) throws ServletException, IOException {
		 
   	 String resp = "";
   	 try{
   	   // 获取页面内容
   	   java.io.InputStream in = request.getInputStream();
   	   java.io.BufferedReader breader = new BufferedReader(
   	     new InputStreamReader(in, "UTF-8"));
   	   String str = breader.readLine();
   	   while (str != null) {	
	    	    resp +=str;
	    	    str = breader.readLine();
	    	   }
   	  } catch (Exception e) {
   		  e.printStackTrace();
   	  } finally {
   	   }
   		  
   	 return resp;
   	 
   	  
}
	
	

	/**
	 * 
	  * @purpose: 证书方式的请求
	  * @param url
	  * @param xml
	  * @param certPath
	  * @return
	  * @throws Exception
	  * String
	 */
	 public static String sendPostSSL(String url,String xml,String certPath,String merId) throws Exception {
		    String back = "";
	        KeyStore keyStore  = KeyStore.getInstance("PKCS12");
	        //"c:/cert/apiclient_cert.p12"
	        FileInputStream instream = new FileInputStream(new File(certPath));
	        try {
	            keyStore.load(instream, merId.toCharArray());
	        } finally {
	            instream.close();
	        }

	        // Trust own CA and all self-signed certs
	        SSLContext sslcontext = SSLContexts.custom()
	                .loadKeyMaterial(keyStore, merId.toCharArray())
	                .build();
	        // Allow TLSv1 protocol only
	        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
	                sslcontext,
	                new String[] { "TLSv1" },
	                null,
	                SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
	        CloseableHttpClient httpclient = HttpClients.custom()
	                .setSSLSocketFactory(sslsf)
	                .build();
	        try {

	        	StringEntity stringEntity = new StringEntity(xml);
	        	//"https://api.mch.weixin.qq.com/secapi/pay/refund"
	        	HttpPost httpget = new HttpPost(url);
	        	
	        	httpget.setEntity(stringEntity);
	            CloseableHttpResponse response = httpclient.execute(httpget);
	            try {
	                HttpEntity entity = response.getEntity();
	                if (entity != null) {
	                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()));
	                    String text;
	                    while ((text = bufferedReader.readLine()) != null) {
//	                        System.out.println(text);
	                        back += text;
	                    }
	                   
	                }
	                EntityUtils.consume(entity);
	            } finally {
	                response.close();
	            }
	        } finally {
	            httpclient.close();
	        }
	        return back;
	    }
	 
	 /**
		 * http post方式
		 * 方法用途: <br>
		 * @param url
		 * @param params
		 * @param charset 字符集
		 * @return
		 * @throws ClientProtocolException
		 * @throws IOException
		 */
		public static String sendPost(String url, Map<String, String> params,String charset) throws ClientProtocolException, IOException {
			String result = null;
			// 创建HttpClientBuilder
			HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
			// HttpClient
			CloseableHttpClient closeableHttpClient = httpClientBuilder.build();

			HttpPost httpPost = new HttpPost(url);

			// 创建表单参数列表
			List<NameValuePair> qparams = new ArrayList<NameValuePair>();
			Set<String> keys = params.keySet();
			for (String key : keys) {
				qparams.add(new BasicNameValuePair(key, params.get(key)));
			}
			// 填充表单
			httpPost.setEntity(new UrlEncodedFormEntity(qparams, charset));
			
			HttpResponse response = closeableHttpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				entity = new BufferedHttpEntity(entity);
				InputStream in = entity.getContent();
				byte[] read = new byte[1024];
				byte[] all = new byte[0];
				int num;
				while ((num = in.read(read)) > 0) {
					byte[] temp = new byte[all.length + num];
					System.arraycopy(all, 0, temp, 0, all.length);
					System.arraycopy(read, 0, temp, all.length, num);
					all = temp;
				}
				result = new String(all, charset);
				if (null != in) {
					in.close();
				}
				// 关闭流并释放资源
				closeableHttpClient.close();
				httpPost.abort();
			}
			return result;
		}

}
