package com.deyi.util;

public class WxConstants {


//	/**
//	 * 登录用户账号信息，加载到session缓存中
//	 */
//	public static final String WEIXIN_ACCOUNT = "WEIXIN_ACCOUNT";
//	/**
//	 * 微信商城用户登录是的openid
//	 */
//	public static final String USER_OPENID = "USER_OPENID";
//	
	public static final String WX_API_URL = "https://api.weixin.qq.com";
//	
//	public static final String WX_MP_URL = "https://mp.weixin.qq.com";
//	
//	public static final String JSAPI_TICKET_URL = WX_API_URL + "/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
//	
//	// 获取access_token的接口地址（GET） 限200（次/天）
	public final static String WX_ACCESS_TOKEN_URL = WX_API_URL + "/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
////	// 菜单创建（POST） 限100（次/天）
////    public static String menu_create_url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
////    //客服接口地址
////    public static String send_message_url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
//	
//	public static final String WX_ENTERPRISE_TRANSFER_URL = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
//	
//	public static final String WX_REDPACK_SEND_URL = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";
//	
//	public static final String WX_REDPACK_QUERY_URL = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gethbinfo";
//	
//	public static final int WX_SCENE_ID_SCAN_QRCOE = 1;
//	
//	public static final String WX_QR_SCENE = "QR_SCENE";
//	public static final String WX_QR_LIMIT_SCENE = "QR_LIMIT_SCENE";
//	
//	public static final int DEFAULT_EXPIRE_PERIOD = 2592000;
//	
//	public static final String WX_SHOW_QR_CODE_URL = WX_MP_URL + "/cgi-bin/showqrcode?ticket=";
//	
	public static final int T_WX_ERRCODE_SUCC = 0;
	
	public static final String WX_EVENT_SUBSCRIBE = "subscribe";
	
	public static final String WX_EVENT_UNSUBSCRIBE = "unsubscribe";
	
	public static final String WX_EVENT_SCAN = "SCAN";
	
	public static final String WX_EVENT_VIEW = "VIEW";
	
	public static final String WX_EVENT_LOCATION = "LOCATION";
//	
	public static final int T_WX_SUBSCRIBE_STATUS = 1;
	
	public static final int T_WX_UNSUBSCRIBE_STATUS = 0;
//	
//	public static final String T_WX_REDPACK_STATUS_SENDING = "SENDING";
//	
//	public static final String T_WX_REDPACK_STATUS_SENT = "SENT";
//	
//	public static final String T_WX_REDPACK_STATUS_FAILED = "FAILED";
//	
//	public static final String T_WX_REDPACK_STATUS_RECEIVED = "RECEIVED";
//	
//	public static final String T_WX_REDPACK_STATUS_REFUND = "REFUND";
//	
	
}
