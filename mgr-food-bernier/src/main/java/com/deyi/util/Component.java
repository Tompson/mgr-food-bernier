package com.deyi.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.SysUser;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysUserService;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
@org.springframework.stereotype.Component
public class Component<T> {
//	@Autowired
//	protected LoggerUtil loggerUtil;
	
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysOrderService sysOrderService;
	/**
	 * 获取系统用户session信息
	 * @Author zjz Create on 2016年11月19日 下午1:48:29
	 * @param request
	 * @return
	 */
	protected SysUser getSessionUser(HttpServletRequest request) {
		SysUser sysUser = null;
		HttpSession session = request.getSession();
		sysUser = (SysUser) session.getAttribute(Constants.SESSION_USERINFO);
		return sysUser;
	}
	/**
	 * 更新系统用户session信息
	 * @Author zjz Create on 2016年11月19日 下午1:48:12
	 * @param request
	 * @param sysUser
	 */
	protected void updateSession(HttpServletRequest request, SysUser sysUser) {
		try {
			HttpSession session = request.getSession();
			if (sysUser != null) {
				SysUser sysUser2 = sysUserService.selectByPrimaryKey(sysUser.getUid());
				session.setAttribute(Constants.SESSION_USERINFO, sysUser2);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 统计订单状态等于未处理的数量
	 * @Author zjz Create on 2016年11月24日 上午11:32:14
	 * @param mav
	 */
	protected void statOrderStatusEqUn(ModelAndView mav) {
		mav.addObject("statOrderStatusEqUntreated",sysOrderService.statOrderStatusEqUntreated());
	}
	protected void statOrderStatusEq0ReturnVo(ReturnVo<Object> vo) {
		int statOrderStatusEqUntreated = sysOrderService.statOrderStatusEqUntreated();
		vo.setData(statOrderStatusEqUntreated);
	}
	
	/**
	 * 
	 * @Author zjz Create on 2016年11月28日 上午11:35:08
	 * @param reqGrid
	 * @param request
	 * @param page
	 * @return
	 */
	protected GridVo queryPage(RequestGrid<T> reqGrid, HttpServletRequest request, Page<T> page) {
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		return vo;
	}
	
	/**
	 * 根据系统用户里的角色id判断,如果是 1或者2(1:平台管理员2:管理员)就返回true;否则返回false
	 * @Author zjz Create on 2016年11月19日 下午1:42:55
	 * @param request
	 * @param roleid
	 * @return
	 */
//	protected boolean checkUserRole(HttpServletRequest request,String roleid) {
//		try {
//			SysUser user = getSessionUser(request);//获取session
//			if(user==null)
//				return false;
//			
//			String[] roleids = roleid.split(",");//传过来的角色id:(1,2)
//			for(String id:roleids){
//				if(user.getRoleId() == Long.valueOf(id)){//当前登录的角色id是平台管理员或者管理员
//					return true;//就返回true
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return false;//默认返回false
//	}
	
	/**
	 * 
	 * 方法用途: <br>
	 * @param request
	 * @param params 实体类
	 * @param page  分页类
	 */
	protected void setParams(HttpServletRequest request,T params,Page<T> page){
		page.setUrl(request.getRequestURI());//  /kitchen/sysOrder/page.html
		Class<? extends Object> bean = params.getClass();//entity.SysUser
		Field[] fields = bean.getDeclaredFields();//获取声明的字段
		for(Field field:fields){//遍历字段
			try {//implements Serializable  private static final long serialVersionUID = 2360324948063137585L; 
				if(!"serialVersionUID".equals(field.getName())){//字段名不等于serialVersionUID
					//getUserName
					String methodName = "get"+field.getName().substring(0, 1).toUpperCase()+field.getName().substring(1, field.getName().length());
					//getUserName();
					Method method = bean.getDeclaredMethod(methodName);//获取声明的方法
					Object fieldValue = method.invoke(params);//调用该方法 传入参数
					page.getParams().put(field.getName(),fieldValue);//userAccount,null or userAccount 133@qq.com
				}// 其他的参数我们把它封装成一个Map对象
			} catch (IllegalArgumentException e) {
//				loggerUtil.error(e.getMessage());
			} catch (IllegalAccessException e) {
//				loggerUtil.error(e.getMessage());
			} catch (NoSuchMethodException e) {
//				loggerUtil.error(e.getMessage());
			} catch (SecurityException e) {
//				loggerUtil.error(e.getMessage());
			} catch (InvocationTargetException e) {
//				loggerUtil.error(e.getMessage());
			}
		}
	}

	
}
