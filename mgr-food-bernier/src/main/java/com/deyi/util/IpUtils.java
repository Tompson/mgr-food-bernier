package com.deyi.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IpUtils {
	
	
	private static Logger logger = LoggerFactory.getLogger(IpUtils.class);
	/** 
     * 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址; 
     *  
     * @param request 
     * @return 
     * @throws IOException 
     */  
    public final static String getIpAddress(HttpServletRequest request) throws IOException {  
        // 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址  
    	
        String ip = request.getHeader("X-Forwarded-For");  
        if (logger.isInfoEnabled()) {  
            logger.info("getIpAddress(HttpServletRequest) - X-Forwarded-For - String ip=" + ip);  
        }  
  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
                ip = request.getHeader("Proxy-Client-IP");  
                if (logger.isInfoEnabled()) {  
                    logger.info("getIpAddress(HttpServletRequest) - Proxy-Client-IP - String ip=" + ip);  
                }  
            }  
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
                ip = request.getHeader("WL-Proxy-Client-IP");  
                if (logger.isInfoEnabled()) {  
                    logger.info("getIpAddress(HttpServletRequest) - WL-Proxy-Client-IP - String ip=" + ip);  
                }  
            }  
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
                ip = request.getHeader("HTTP_CLIENT_IP");  
                if (logger.isInfoEnabled()) {  
                    logger.info("getIpAddress(HttpServletRequest) - HTTP_CLIENT_IP - String ip=" + ip);  
                }  
            }  
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
                if (logger.isInfoEnabled()) {  
                    logger.info("getIpAddress(HttpServletRequest) - HTTP_X_FORWARDED_FOR - String ip=" + ip);  
                }  
            }  
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
                ip = request.getRemoteAddr();  
                if (logger.isInfoEnabled()) {  
                    logger.info("getIpAddress(HttpServletRequest) - getRemoteAddr - String ip=" + ip);  
                }  
            }  
        } else if (ip.length() > 15) {  
            String[] ips = ip.split(",");  
            for (int index = 0; index < ips.length; index++) {  
                String strIp = (String) ips[index];  
                if (!("unknown".equalsIgnoreCase(strIp))) {  
                    ip = strIp;  
                    break;  
                }  
            }  
        }  
        return ip;  
    }  
    /**
    * 获取指定IP对应的经纬度（为空返回当前机器经纬度）
    *
    * @param ip
    * @return
    */
    public static String[] getIPXY(String ip) {
     
    String ak = "RKvzHhCN0egpiK9nHKfTAnZybnUQGYrH";
    if (null == ip) {
    ip = "";
    }
     
    try {
     
    URL url = new URL("http://api.map.baidu.com/location/ip?ak=" + ak
    + "&ip=" + ip + "&coor=bd09ll");
    InputStream inputStream = url.openStream();
    InputStreamReader inputReader = new InputStreamReader(inputStream);
    BufferedReader reader = new BufferedReader(inputReader);
    StringBuffer sb = new StringBuffer();
    String str;
    do {
    str = reader.readLine();
    sb.append(str);
    } while (null != str);
     
     
    str = sb.toString();
    if (null == str || str.isEmpty()) {
    return null;
    }
     
     
    // 获取坐标位子
    int index = str.indexOf("point");
    int end = str.indexOf("}}", index);
     
     
    if (index == -1 || end == -1) {
    return null;
    }
     
     
    str = str.substring(index - 1, end + 1);
    if (null == str || str.isEmpty()) {
    return null;
    }
     
     
    String[] ss = str.split(":");
    if (ss.length != 4) {
    return null;
    }
     
     
    String x = ss[2].split(",")[0];
    String y = ss[3];
     
     
    x = x.substring(x.indexOf("\"") + 1, x.indexOf("\"", 1));
    y = x.substring(y.indexOf("\"") + 1, y.indexOf("\"", 1));
     
     
    return new String[] { x, y };
     
     
    } catch (MalformedURLException e) {
    e.printStackTrace();
    } catch (IOException e) {
    e.printStackTrace();
    }
     
     
    return null;
    }
}
