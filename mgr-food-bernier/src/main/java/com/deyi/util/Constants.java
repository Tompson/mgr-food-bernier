package com.deyi.util;

public class Constants {
//	public final static String MENU_LIST="menuList";
	/**
	 * 配送员信息加密密匙(固定的uuid)
	 * 
	 */
	public static final String DELIVERMAN_UUID_KEY = "bbec44c5b560451a85c16bb0b3885e4b";
	/**系统用户信息**/
	public final static String SESSION_USERINFO="userInfo";
	public final static String MENU_LIST="menuList";
	//String project_path = PropertiesUtil.getProperty(Constants.PROJECT_PATH);
	public static final String PROJECT_PATH = "project_path"; //zjz加的
	/**
	 * images
	 */
	//String upload_dir = PropertiesUtil.getProperty(Constants.UPLOAD_DIR);
	public static final String UPLOAD_DIR = "upload_dir"; //zjz加的
	public static final String UPLOAD_Table = "upload_table"; //放餐桌的二维码
	/**用户有权限的url**/
	public final static String SESSION_USER_URLS="userUrls";
	
	/**文件上传路径  压缩文件*/
	public final static String FILE_UPLOAD_PATH_HOTEL_ZIP="hotelZip";
	
	/**文件上传路径  图片*/
	public final static String FILE_UPLOAD_PATH_HOTEL_PIC="hotelImage";
	
	
//	
//	
//	/**会员信息**/
	public final static String SESSION_MEMBER = "session_member";
	
	public final static String SESSION_WXOPENID = "SESSION_WXOPENID";
	
	public static final String WX_API_URL = "https://api.weixin.qq.com";
	
//	// 获取access_token的接口地址（GET） 限200（次/天）
	public final static String WX_ACCESS_TOKEN_URL = WX_API_URL + "/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
			
	/**
	 * 签名类型 md5
	 */
	public final static String P_MD5 = "MD5";
	
	public static final String BAIDU_MAP_API = "http://api.map.baidu.com";
	
	//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成 9订单已评价
	//支付状态 0.未支付 1支付成功  2支付失败  3退款中 4 已退款 5退款失败
	/**
	 * 已支付
	 */
	public static final String ORDER_STATUS_PAIED = "1";
	/**
	 * 未支付
	 */
	public static final String ORDER_STATUS_UNPAY = "0";
	/**
	 * 已取消
	 */
	public static final String ORDER_STATUS_CANCLED = "6";
	/**
	 * 已成功
	 */
	public static final String ORDER_STATUS_SUCCESS = "4";
	/**
	 * 已评价
	 */
	public static final String ORDER_STATUS_ADVISED = "9";
	public static final String TOKEN = "5deyicom";
	/**
	 * 默认地址
	 */
	public static final String DEFAULT_ADDRESS = "0";
	/**
	 * 非默认地址
	 */
	public static final String NOT_DEFAULT_ADDRESS = "1";
	
	/**
	 * 预定单
	 */
	public static final String ORDER_TYPE_BOOK = "book";
	/**
	 * session中的预定类型
	 */
	public static final String SESSION_BOOK_TYPE="book_type";
	/**
	 * 外卖订单
	 */
	public static final String ORDER_TYPE_NORMAL = "1";
	/**
	 * 预定订单
	 */
	public static final String ORDER_TYPE_BOOK2 = "2";
	/**
	 * 堂食
	 */
	public static final String ORDER_EatingInStore = "3";
	/**
	 *打包
	 */
	public static final String ORDER_DaBao = "4";
	/**
	 * 预定明日中餐
	 */
//	public static final String BOOK_TYPE_BOOK_TOMORROW_LUNCH = "3";
	/**
	 * 预定今日晚餐
	 */
//	public static final String BOOK_TYPE_BOOK_SUPPER = "4";
	/**
	 * 用于存入session的
	 * 地址页面返回确认订单页面的url
	 */
	public static final String SESSION_ADDRESS_BACK_CONFIRM = "back_confirm";
	
	//支付方式 0未支付，1支付宝，2微信支付，3现金支付，4余额支付
	/**
	 * 支付方式：微信支付
	 */
	public static final String ORDER_PAY_TYPE_WX = "2";
	/**
	 * 支付方式：支付宝支付
	 */
	public static final String ORDER_PAY_TYPE_ALIPAY = "1";
	/**
	 * 支付方式：现金支付
	 */
	public static final String ORDER_PAY_TYPE_CASH = "3";
	/**
	 * 支付方式：余额
	 */
	public static final String ORDER_PAY_TYPE_BALANCE = "4";
	
	//积分类型 1消费赠送积分 2评论赠送积分  3分享赠送积分
	/**
	 * 1  消费赠送积分
	 */
	public static final String MEMBER_INTEGRAL_TYPE_CONSUME = "1";
	/**
	 *  2 评论赠送积分
	 */
	public static final String MEMBER_INTEGRAL_TYPE_ADVICE = "2";
	/**
	 *  3 分享赠送积分
	 */
	public static final String MEMBER_INTEGRAL_TYPE_SHARE = "3";
	/**
	 *  4 消费使用
	 */
	public static final String MEMBER_INTEGRAL_TYPE_USE = "4";
	/**
	 * 菜品的状态-在售
	 */
	public static final String GOODS_STATUS_ONSELL= "0";
	/**
	 * 菜品的状态-已售完
	 */
	public static final String GOODS_STATUS_SELLOUT = "1";//售完
	public static final Integer GOODS_STATUS_STOP=1;//禁用
	/**
	 * 菜品的状态-禁用
	 */
	public static final int GOODS_STATUS_FORBIDDEN = 2;
	public static final String STOREGOODS_STATUS_FORBIDDEN="2";
	/**
	 * session过期时间30分钟
	 */
	public static final int SESSION_EXPIRED_TIME_THIRTY_MINUTE = 1800;
	/**
	 * 菜单菜品数量50个
	 */
	public static final int PAGE_SIZE_FOR_GOODS = 50;
	/**
	 * 代金券状态-已使用 --"3"
	 */
	public static final String VOUCHER_STATUS_USED = "3";
	/**
	 * 代金券状态-未使用 --"2"
	 */
	public static final String VOUCHER_STATUS_UNUSED = "2";
	/**
	 * 抽奖代金券分类-评论 --"1"
	 */
	public static final String VOUCHER_LOTTERY_CATEGORY_EVALUATE = "1";
	/**
	 * 抽奖代金券分类-分享 --"2"
	 */
	public static final String VOUCHER_LOTTERY_CATEGORY_SHARE = "2";
	public static final String VOUCHER_CATEGORY_4="4";//平台代金卷
	/**
	 * 会员状态-正常 --1
	 */
	public static final int MEMBER_STATUS_NORMAL = 1;
	/**
	 * 会员状态-禁用 --2
	 */
	public static final int MEMBER_STATUS_FORBIDDEN = 2;
	
	/**
	 * 会员状态-正常 --1
	 */
	public static final int MEMBER_STATUS_MEMBER_RANK_NORMAL = 1;
	
	/**
	 * 会员状态-删除 --2
	 */
	public static final int MEMBER_STATUS_MEMBER_RANK_DELETED = 2;
	
	/**
	 * 错误码--001
	 * 会员被禁用
	 */
	public static final String ERROR_CODE_001 = "001";
	/**
	 * 错误码--002
	 * 菜品售完
	 */
	public static final String ERROR_CODE_002 = "002";
	/**
	 * 错误码--003
	 * 今天订购时间超过营业时间
	 */
	public static final String ERROR_CODE_003 = "003";
	/**
	 * 错误码--004
	 * 菜品价格未达到起步价
	 */
	public static final String ERROR_CODE_004 = "004";
	/**
	 * 错误码--005
	 * 此时间段不可预约，因为现在时间已超过预定时间了
	 */
	public static final String ERROR_CODE_005 = "005";
	/**
	 * 错误码--006
	 * 不在营业时间范围内（所有预定时间都需在营业时间内）
	 */
	public static final String ERROR_CODE_006 = "006";
	/**
	 * 错误码--007
	 * 菜品禁用
	 */
	public static final String ERROR_CODE_007 = "007";
	/**
	 * 错误码--008
	 * 购物车中菜品已被删除
	 */
	public static final String ERROR_CODE_008 = "008";
	public static final String COUPON_STOP = "3";//代金卷已经结束
	public static final String COUPON_RUNNING = "2";//代金卷进行中
	public static final String COUPON_NOTSTAR = "1";//代金卷未开始
	public static final String voucher_used = "3";//代金卷已经使用了
	public static final String STORE_IS_ACTIVITY = "2";//商店参与平台活动
	public static final String STORE_NOT_ACTIVITY = "1";//商店未参与平台活动
	public static final String STORE_STATUS_1 = "1";//商店启用
	public static final String STORE_STATUS_2= "2";//商店禁用
		
}
