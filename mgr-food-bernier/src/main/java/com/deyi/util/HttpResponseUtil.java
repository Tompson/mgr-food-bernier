package com.deyi.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

/**
 * 请求响应工具类
 * @author zjz Create on 2016年11月25日 下午4:58:22
 */

public class HttpResponseUtil {
	/**
	 * 响应请求，并跳转到传入地址
	 * @Author zjz Create on 2016年11月25日 下午4:58:52
	 * @param response
	 * @param url
	 * @throws IOException
	 */
	public static void renturnUrl(HttpServletResponse response, String url) throws IOException {
		String startContent = ""
				+ "<html>\r\n" 
				+ "	<head>\r\n"
				+ "		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";
		String ajaxContent = ""
				+ "		<script type=\"text/javascript\">" 
				+ "			window.parent.document.location.href='" + url + "';"
				+ "		</script>\r\n";
		String endContent = ""
				+ "	</head>\r\n" 
				+ "	<body>\r\n" 
				+ "	    <span>userTimeOut</span>\r\n" 
				+ "	</body>\r\n"
				+ "</html>";

		response.getWriter().print(startContent + ajaxContent + endContent);//写入要发送的部分内容
		response.getWriter().flush();//将部分内容发送到客户端
	}
}
