package com.deyi.util;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deyi.service.SysCouponService;

@Component
public class TradeTask {
	private static Logger log = LoggerFactory.getLogger(TradeTask.class);
//	@Autowired
//	private IOrderService orderServiceImpl;
//	@Autowired
//	private ICouponService couponServiceImpl;
//	@Autowired
//	private IParamService paramServiceImpl ;
	@Autowired
	private SysCouponService sysCouponService ;
	
	public void test(){
		
		log.info("交易统计开始....");
		try {
			/*Trade trade = orderServiceImpl.queryTrade();
			log.info(trade.toString());
			orderServiceImpl.insertTrade(trade);*/
			
		} catch (Exception e) {
			log.error("交易统计异常",e);
		}
		log.info("交易统计结束....");
	}
	public void updateOrderStatus(){
		
		log.info("更新订单状态和库存开始....");
		try {
			log.info("撤销过期的订单开始....");
//			orderServiceImpl.updateOrderTrolley();;
			log.info("撤销过期的订单结束....");
		} catch (Exception e) {
			log.error("更新订单状态和库存开始",e);
		}
		log.info("更新订单状态和库存结束....");
	}
	
	//已过期的优惠券，更新优惠券状态
	public void updateCouponStatus(){
		log.info("已过期的优惠券，更新优惠券状态开始....");
		try {
			log.info("更新优惠券状态开始....");
//			couponServiceImpl.updateCouponTask();
			log.info("更新优惠券状态结束....");
		} catch (Exception e) {
			log.error("更新优惠券状态开始",e);
		}
		log.info("更新优惠券状态结束....");
	}
	
	//商家已发货，默认天数将订单状态变成已完成状态
		public void orderComplete(){
			log.info("将已发货的订单默认设置为已完成状态开始....");
			try {
				log.info("设置订单为已完成开始....");
				//查询系统参数中设置的默认天数
//				int defaultDays = paramServiceImpl.queryParamValue();
//				orderServiceImpl.updateOrderComplete(defaultDays);
				log.info("设置订单为已完成结束....");
			} catch (Exception e) {
				log.error("设置订单为已完成开始",e);
			}
			log.info("设置订单状态结束....");
		}
		
	//每分鐘 統計 更改已經 開始的優惠卷，將狀態設置為進行中
	public void sysCoupon(){
		log.info("将优惠卷状态改為进行中开始....");
		try {
			log.info("设置订单为已完成开始....");
			sysCouponService.startUpdateSysCouponTime(new Date());
			log.info("将优惠卷状态改為进行中结束....");
		} catch (Exception e) {
			log.error("将优惠卷状态改為进行中异常",e);
		}
		log.info("将优惠卷状态改為进行中结束....");
	}
		

}
