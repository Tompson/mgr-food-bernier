package com.deyi.util;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Map;

import com.deyi.controller.pay.aliPay.AlipayConfig;

public class SignUtils {

	private static final String ALGORITHM = "RSA";

	private static final String SIGN_ALGORITHMS = "SHA1WithRSA";

	private static final String DEFAULT_CHARSET = "UTF-8";

	public static String sign(String content, String privateKey) {
		try {
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
					Base64.decode(privateKey));
			KeyFactory keyf = KeyFactory.getInstance(ALGORITHM);
			PrivateKey priKey = keyf.generatePrivate(priPKCS8);

			java.security.Signature signature = java.security.Signature
					.getInstance(SIGN_ALGORITHMS);

			signature.initSign(priKey);
			signature.update(content.getBytes(DEFAULT_CHARSET));

			byte[] signed = signature.sign();

			return Base64.encode(signed);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	/***
	 * 将对象排序
	 * @param cardConsVo
	 * @return
	 */
	public static String encode(Map<String, String> sParaTemp) {
		StringBuilder builder = new StringBuilder();
		builder.append("partner=\""+sParaTemp.get("partner")+"\"");
		builder.append("&seller_id=\""+sParaTemp.get("seller_id")+"\"");
		builder.append("&out_trade_no=\"" +sParaTemp.get("out_trade_no")+"\"");
		builder.append("&subject=\""+ sParaTemp.get("subject")+"\"");
		builder.append("&body=\"" +sParaTemp.get("body")+"\"");
		builder.append("&total_fee=\""+sParaTemp.get("total_fee")+"\"");
		builder.append("&notify_url=\""+sParaTemp.get("notify_url")+"\"");
		builder.append("&service=\""+sParaTemp.get("service")+"\"");
		builder.append("&payment_type=\""+ sParaTemp.get("payment_type")+"\"");
		builder.append("&_input_charset=\""+sParaTemp.get("_input_charset")+"\"");
		return builder.toString();
	}
	
	/***
	 * 将对象排序
	 * @param cardConsVo
	 * @return
	 */
	public static String encodeAliPay(Map<String, String> sParaTemp) {
		StringBuilder builder = new StringBuilder();
		builder.append("partner=\""+AlipayConfig.partner+"\"");
		builder.append("&seller_id=\""+sParaTemp.get("seller_email")+"\"");
		builder.append("&out_trade_no=\"" +sParaTemp.get("out_trade_no")+"\"");
		builder.append("&subject=\""+ sParaTemp.get("subject")+"\"");
		builder.append("&body=\"" +sParaTemp.get("body")+"\"");
		builder.append("&total_fee=\""+sParaTemp.get("total_fee")+"\"");
		builder.append("&notify_url=\""+AlipayConfig.notity_path+"\"");
		builder.append("&service=\""+AlipayConfig.ali_pay_wap+"\"");
		builder.append("&payment_type=\""+ sParaTemp.get("payment_type")+"\"");
		builder.append("&_input_charset=\""+AlipayConfig.input_charset+"\"");
		return builder.toString();
	}

}
