package com.deyi.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;

public class ZipFiles {
	private static Logger logger = Logger.getLogger(ZipFiles.class);

	public ZipFiles() {
	}
	/**
	 * 解压到指定目录
	 * @param zipPath
	 * @param descDir
	 * @author isea533
	 */
	public static void unZipFiles(String zipPath,String descDir)throws IOException{
		unZipFiles(new File(zipPath), descDir);
	}
	/**
	 * 解压文件到指定目录
	 * @param zipFile
	 * @param descDir
	 * @author isea533
	 */
	@SuppressWarnings("rawtypes")
	public static void unZipFiles(File zipFile,String descDir)throws IOException{
		File pathFile = new File(descDir);
		if(!pathFile.exists()){
			pathFile.mkdirs();
		}
		ZipFile zip = new ZipFile(zipFile);
		for(Enumeration entries = zip.entries();entries.hasMoreElements();){
			ZipEntry entry = (ZipEntry)entries.nextElement();
			String zipEntryName = entry.getName();
			InputStream in = zip.getInputStream(entry);
			String outPath = (descDir+zipEntryName).replaceAll("\\*", "/");;
			//判断路径是否存在,不存在则创建文件路径
			File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
			if(!file.exists()){
				file.mkdirs();
			}
			//判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
			if(new File(outPath).isDirectory()){
				continue;
			}
			//输出文件路径信息
			System.out.println(outPath);
			
			OutputStream out = new FileOutputStream(outPath);
			byte[] buf1 = new byte[1024];
			int len;
			while((len=in.read(buf1))>0){
				out.write(buf1,0,len);
			}
			in.close();
			out.close();
			}
		    zip.close();
		System.out.println("******************解压完毕********************");
	}
	//删除指定文件
	 public void deleteZip(String path) {
	       /* String[] sos = abpath.split("/");
	        String name = ss[ss.length - 1];
	        String path = abpath.replace("/" + name, "");*/
           try{
	        File file = new File(path);// 里面输入特定目录
	        File temp = null;
	        File[] filelist = file.listFiles();
	        for (int i = 0; i < filelist.length; i++) {
	            temp = filelist[i];
	            if (temp.getName().endsWith("zip"))// 获得文件名，如果后缀为“”，这个你自己写，就删除文件
	            {
	                temp.delete();// 删除文件}
	            }
	        }
	        System.out.println("zip文件删除完毕");
	        
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	 }
	 //删除文件
	 public void deleteFile(String path) {
	       /* String[] sos = abpath.split("/");
	        String name = ss[ss.length - 1];
	        String path = abpath.replace("/" + name, "");*/
         try{
	        File file = new File(path);// 里面输入特定目录
	        File temp = null;
	        File[] filelist = file.listFiles();
	        for (int i = 0; i < filelist.length; i++) {
	            temp = filelist[i];
	            temp.delete();// 删除文件	            
	        }
	        System.out.println("文件删除完毕");
	        
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	 }
	 
	
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	
	
	
	
	
	
}

