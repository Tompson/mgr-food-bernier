package com.deyi.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.jxls.transformer.XLSTransformer;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import com.deyi.exception.BusinessException;

import java.io.FileInputStream;  
import java.math.BigDecimal;  
import java.text.SimpleDateFormat;  
import java.util.Calendar;  
import java.util.Date;  
import java.util.Iterator;  
  
import org.apache.poi.hssf.usermodel.HSSFCell;  
import org.apache.poi.hssf.usermodel.HSSFDateUtil;  
import org.apache.poi.hssf.usermodel.HSSFSheet;  
import org.apache.poi.hssf.usermodel.HSSFWorkbook;  
import org.apache.poi.poifs.filesystem.POIFSFileSystem;  
import org.apache.poi.ss.usermodel.Cell;  
import org.apache.poi.ss.usermodel.Row;  

/**
 * Excel包装类。
 */
public class Excel {
	Logger logger = Logger.getLogger(Excel.class);
	/** Excel模版路径 */
	private static String REPORT_DIR = "";
	private XLSTransformer transformer = new XLSTransformer();
	private Workbook workbook;

	/**
	 * 初始化生成Excel。
	 * 
	 * @param templateFileName
	 *            模版文件名称
	 * @param model
	 *            数据模型
	 */
	public void init(HttpServletRequest request,String templateFileName, Map<String, ?> model) {
		try {
			String string = getClass().getResource("/").getPath().replaceAll("%20"," ");
			///C:/Program%20Files/apache-tomcat-7.0.65/webapps/camshop/WEB-INF/classes/
			
			//String string = request.getServletContext().getRealPath("/");
			logger.info("Excel==============string:"+string);
			String[] strs = string.split("/");
			String path = "";
			for (int i = 0; i < strs.length - 2; i++) {
				path += strs[i] + File.separator;
			}
			path = path + templateFileName;
//			path = path.substring(1, path.length());
			logger.info(">>>>>>>>>>模板文件路径为：" + path);
			File file = new File(path);
			
			InputStream in = new FileInputStream(file);

			//InputStream in = getClass().getResourceAsStream(path);
			workbook = transformer.transformXLS(in, model);
			//transformer.t
			in.close();

		} catch (Exception e) {
			throw new BusinessException("生成Excel时发生异常。", e);
		}
	}

	/**
	 * 初始化生成多工作表的Excel。
	 * 
	 * @param data
	 *            Excel数据
	 */
	public void init(ExcelData data) { 
		try {
			// 如果没有设置工作表名称列表，则根据指定的工作表名称字段自动生成。
			List<String> sheetNames = data.getSheetNames();
			if (sheetNames.isEmpty()) {
				for (Object sheetModel : data.getSheetModels()) {
					//sheetNames.add(BeanUtils.getField(sheetModel,
							//data.getSheetNameField().toString()));
				}
			}
			InputStream in = getClass().getResourceAsStream(
					REPORT_DIR + "/" + data.getTemplateFileName());
			workbook = transformer.transformMultipleSheetsList(in,
					data.getSheetModels(), sheetNames,
					data.getSheetModelName(), data.getOtherModel(),
					data.getStartSheetNum());
			in.close();
		} catch (Exception e) {
			throw new BusinessException("生成Excel时发生异常。", e);
		}
	}

	/**
	 * 转换成Excel文件输出流。
	 * 
	 * @return 返回Excel文件输出流。
	 */
	public ByteArrayOutputStream toOutputStream() {
		ByteArrayOutputStream out = null;
		try {
			out = new ByteArrayOutputStream();
			workbook.write(out);
			out.flush();
			return out;
		} catch (Exception e) {
			throw new BusinessException("转换Excel文件输出流时发生异常。", e);
		}
	}

	/**
	 * 转换成Excel文件输入流。
	 * 
	 * @return 返回Excel文件输入流。
	 */
	public ByteArrayInputStream toInputStream() {
		ByteArrayInputStream in = null;
		try {
			ByteArrayOutputStream out = toOutputStream();
			in = new ByteArrayInputStream(
					out.toByteArray());
			out.close();
			return in;
		} catch (Exception e) {
			throw new BusinessException("转换Excel文件输入流时发生异常。", e);
			
		}
	}

	/**
	 * 将Excel文件写入到输出流。
	 * 
	 * @param out
	 *            输出流
	 */
	public void writeTo(OutputStream out) {
		try {
			workbook.write(out);
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.info("Excel==============Exception:"+e.getMessage());
			throw new BusinessException("将Excel文件写入到输出流时发生异常。", e);
		}
	}

	public XLSTransformer getTransformer() {
		return transformer;
	}

	public void setTransformer(XLSTransformer transformer) {
		this.transformer = transformer;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}
	
	
	public InputStream getInputStream(String tpath){
		InputStream in=null;
		try {
			String string = getClass().getResource("/").getPath();
			logger.info("Excel==============string:"+string);
			String[] strs = string.split("/");
			String path = "";
			for (int i = 0; i < strs.length - 2; i++) {
				path += strs[i] + File.separator;
			}
			path = path + tpath;
//			path = path.substring(1, path.length());
			logger.info(">>>>>>>>>>模板文件路径为：" + path);
			File file = new File(path);
			
			in = new FileInputStream(file);
		} catch (Exception e) {
		}
		return in;
	}
	/** 
     * 根据HSSFCell类型设置数据 
     *  
     * @param cell 
     * @return 
     */  
    public String getCellFormatValue(Cell cell) {  
        String cellvalue = "";  
        if (cell != null) {  
            // 判断当前Cell的Type  
            switch (cell.getCellType()) {  
            // 如果当前Cell的Type为NUMERIC  
            case HSSFCell.CELL_TYPE_FORMULA: {  
                // 判断当前的cell是否为Date  
                if (HSSFDateUtil.isCellDateFormatted(cell)) {  
                    // 如果是Date类型则，转化为Data格式  
                    Date date = cell.getDateCellValue();  
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
                    cellvalue = sdf.format(date);  
  
                } else {  
                    // 如果是纯数字  
                    // 取得当前Cell的数值  
                    BigDecimal big = new BigDecimal(cell.getNumericCellValue());  
                    cellvalue = String.valueOf(big.longValue());  
                }  
                break;  
            }  
            case HSSFCell.CELL_TYPE_NUMERIC: {  
            	 if (HSSFDateUtil.isCellDateFormatted(cell)) {  
                     // 如果是Date类型则，转化为Data格式  
                     Date date = cell.getDateCellValue();  
                     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
                     cellvalue = sdf.format(date);  
   
                 } else {  
                     // 如果是纯数字  
                     // 取得当前Cell的数值  
                     BigDecimal big = new BigDecimal(cell.getNumericCellValue());  
                     cellvalue = String.valueOf(big.longValue());  
                 } 
                    // 如果是double，返回结果。  
                break;  
            }  
            // 如果当前Cell的Type为STRIN  
            case HSSFCell.CELL_TYPE_STRING:  
                // 取得当前的Cell字符串  
                cellvalue = cell.getRichStringCellValue().getString();  
                break;  
            // 默认的Cell值  
            default:  
                cellvalue = " ";  
            }  
        } else {  
            cellvalue = "";  
        }  
        return cellvalue;  
  
    }  
    
    public Iterator<Row> readExcel(String filePath) throws Exception {
         POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filePath));  
         HSSFWorkbook workBook = new HSSFWorkbook(fs);  
         HSSFSheet sheet = workBook.getSheetAt(0);  
         Iterator<Row> it = sheet.rowIterator(); 
    	 return  it;
    }
    
    /** 
     * 解析excel文件
     * @param args 
     */  
 /*   public static void main(String[] args) throws Exception {  
    	String filePath = "f:/企业员工.xls"; 
    	Iterator<Row> it =readExcel(filePath);
        while (it.hasNext()) {  
            Row row = it.next();  
            int cellCount = row.getPhysicalNumberOfCells();  
            for (int i = 0; i < cellCount; i++) {  
                Cell cell = row.getCell(i);
                //获取每一行没一列的值，从第二行开始就是我们需要的数据，需要在这里set进对应的实体
                String cellValue = getCellFormatValue(cell);  
                System.out.print(cellValue + "\t\t");  
            }  
            System.out.println();  
        }  
    }*/  
}
