package com.deyi.util.wxmppay;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.deyi.controller.pay.wxPay.WxMicroEntityRes;
import com.deyi.model.ResponseTrade;
import com.deyi.model.WxUnitedOrderRequest;
import com.deyi.util.BizHelper;
import com.deyi.util.ConstantsPay;
import com.deyi.util.HttpClient;
import com.deyi.util.PayStatusEmum;
import com.deyi.util.PayUtils;
import com.deyi.util.PropertiesUtil;
import com.thoughtworks.xstream.XStream;

public class WxQueryOrder {
	private static XStream XSTREAM = new XStream();
	
	private static final Logger LOG  = Logger.getLogger(WxQueryOrder.class);

	/*public static ResponseTrade queryorder(Order order) {
		ResponseTrade trade = new ResponseTrade();
		String wx_mp_appid = PropertiesUtil.getProperty("wx_mp_appid");//
		String wx_mp_mchid = PropertiesUtil.getProperty("wx_mp_mchid");//
		String wx_unified_queryorder = PropertiesUtil.getProperty("wx_unified_queryorder");//

		WxUnitedOrderRequest request = new WxUnitedOrderRequest();
		try {

			request.setAppid(wx_mp_appid);
			request.setMch_id(wx_mp_mchid);
			request.setOut_trade_no(order.getOrderId());
			request.setNonce_str(PayUtils.getRandomString(16));
			request.setSign(signData(request, BizHelper.getWxApiKey()));
			
			XSTREAM.alias("xml", WxUnitedOrderRequest.class);
			String xml = XSTREAM.toXML(request);
			xml = xml.replace("__", "_");
			LOG.info("wx_united_order:requestXml=" + xml);
			
			
			XStream XSTREAM2 = new XStream();
			
			String result = HttpClient.send(wx_unified_queryorder, xml, "UTF-8", "UTF-8");
			XSTREAM2.alias("xml", WxQueryOrderRes.class);
			LOG.info("查询订单["+order.getPayTradeNo()+"]支付返回result="+result);
			
			WxQueryOrderRes orderRes = (WxQueryOrderRes) XSTREAM2.fromXML(result);
			
			if(ConstantsPay.SUCCESS.equals(orderRes.getReturn_code())){
				if(ConstantsPay.SUCCESS.equals(orderRes.getResult_code())){
					trade.setErrorCode("1");
					trade.setErrorMsg(orderRes.getTrade_state_desc());
					WxMicroEntityRes resps = new WxMicroEntityRes();
					resps.setTransaction_id(orderRes.getTransaction_id());
					resps.setTime_end(orderRes.getTime_end());
					resps.setTotal_fee(orderRes.getTotal_fee());
					trade.setWxMicroEntityRes(resps);
					LOG.info("查询订单返回:trade="+trade.toString());
					return trade;
				}else{
					trade.setErrorCode(PayStatusEmum.PAYERROR.getValue()+"");
					trade.setErrorMsg(orderRes.getErr_code_des());
				}
			}else{
				trade.setErrorCode(PayStatusEmum.PAYERROR.getValue()+"");
				trade.setErrorMsg(orderRes.getReturn_msg());
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return trade;

	}*/

	private static String signData(WxUnitedOrderRequest microEntity, String key) throws UnsupportedEncodingException {
		String[] params = { "appid=" + microEntity.getAppid(), "mch_id=" + microEntity.getMch_id(), "nonce_str=" + microEntity.getNonce_str(), "body=" + microEntity.getBody(), "out_trade_no=" + microEntity.getOut_trade_no(), "total_fee=" + microEntity.getTotal_fee(), "spbill_create_ip=" + microEntity.getSpbill_create_ip(), "notify_url=" + microEntity.getNotify_url(), "trade_type=" + microEntity.getTrade_type(), "openid=" + microEntity.getOpenid() };
		String param = PayUtils.genSortParams(params);
		param += "&key=" + key;
		LOG.info("before signData:" + param);
		String sign = DigestUtils.md5Hex(param.getBytes("utf-8")).toUpperCase();
		LOG.info("after signData:sign=" + sign + "");
		return sign;
	}
	
}
