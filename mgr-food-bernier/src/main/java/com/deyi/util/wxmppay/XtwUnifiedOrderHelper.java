/**
 * 
 */
package com.deyi.util.wxmppay;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.deyi.entity.SysOrder;
import com.deyi.model.ResponseTrade;
import com.deyi.model.WxUnitedOrderRequest;
import com.deyi.model.WxUnitedOrderResponse;
import com.deyi.util.BizHelper;
import com.deyi.util.ConstantsPay;
import com.deyi.util.HttpClient;
import com.deyi.util.PayStatusEmum;
import com.deyi.util.PayUtils;
import com.deyi.util.PropertiesUtil;
import com.thoughtworks.xstream.XStream;

/**
 * pure 微信统一下单接口：服务商的
 * @author hejx
 * @2016年4月11日
 */
public class XtwUnifiedOrderHelper {

	private static Logger LOG = LoggerFactory.getLogger(XtwUnifiedOrderHelper.class);
	private static XStream XSTREAM = new XStream();
	
	public static ResponseTrade doPay(SysOrder order, String openid, String tradeType) throws Exception{
		LOG.info("process in doPay...");
		String wx_mp_appid = PropertiesUtil.getProperty("wx_mp_appid");
		String wx_mp_mchid = PropertiesUtil.getProperty("wx_mp_mchid");
//		String wx_mp_submchid = PropertiesUtil.getProperty("wx_mp_submchid");
		String wx_unified_order = PropertiesUtil.getProperty("wx_unified_order");
		String wx_unified_order_notify = PropertiesUtil.getProperty("wx_unified_order_notify");
		
		ResponseTrade trade = new ResponseTrade();
		String paycode = "0";
		String payMsg = "";
//		int total = 0;
		WxUnitedOrderRequest request = new WxUnitedOrderRequest();
		request.setAppid(wx_mp_appid);
		request.setMch_id(wx_mp_mchid);
		request.setNonce_str(PayUtils.getRandomString(16));
		request.setBody("饭好粥到");
//		microEntity.setDetail("");
//		microEntity.setAttach("");
		request.setOut_trade_no(order.getPayNumber());
//		microEntity.setFee_type("");
		//微信是按分计算的，这里要乘100
		request.setTotal_fee((order.getTotalAmount().multiply(new BigDecimal(100))).intValue());
		request.setSpbill_create_ip("119.139.137.163");
//		microEntity.setGoods_tag("");
		request.setNotify_url(wx_unified_order_notify); //回调地址
		request.setTrade_type(tradeType);//"JSAPI");
		request.setOpenid(openid);
//		microEntity.setLimit_pay("");
		
//		microEntity.setDevice_info(device_info);
		request.setSign(signData(request, BizHelper.getWxApiKey()));
		System.out.println(request.toString());
		System.out.println(BizHelper.getWxApiKey());
		XSTREAM.alias("xml", WxUnitedOrderRequest.class);
		String micropayStr = XSTREAM.toXML(request);
		micropayStr = micropayStr.replace("__", "_");
		LOG.info("wx_united_order:requestXml=" + micropayStr);
		
		
		//支付请求
		XStream XSTREAM2 = new XStream();
		String result = HttpClient.send(wx_unified_order, micropayStr, "UTF-8", "UTF-8");
		XSTREAM2.alias("xml", WxUnitedOrderResponse.class);
		LOG.info("wx_united_order:resultXml="+result);
		
		WxUnitedOrderResponse wxPrepayResp = (WxUnitedOrderResponse) XSTREAM2.fromXML(result);
		if(!ConstantsPay.SUCCESS.equals(wxPrepayResp.getReturn_code())){
			paycode = PayStatusEmum.PAYERROR.getValue().toString(); //交易明确失败
			payMsg = wxPrepayResp.getReturn_msg();
			
		}else if(!ConstantsPay.SUCCESS.equals(wxPrepayResp.getResult_code())){
			
			paycode = PayStatusEmum.PAYERROR.getValue().toString();
			payMsg = wxPrepayResp.getErr_code_des();
		}
//		else{
//			wx_qrcode_path += File.separator+order.getOrderNo()+".bmp";
//			PayUtils.createRQ(wxPrepayResp.getCode_url(), wx_qrcode_path);
//			wxPrepayResp.setCode_url(wx_qrcode_path);
//			
//			paycode = PayStatusEmum.SUCCESS.getValue().toString();
//			wxPrepayResp.setTotal_fee(String.valueOf(BigDecimal.valueOf(Integer.valueOf(wxPrepayResp.getTotal_fee())).divide(new BigDecimal(100))));
//		}
		
		trade.setErrorCode(paycode);
		trade.setErrorMsg(payMsg);
		trade.setWxUnifiedRes(wxPrepayResp);
		return trade;
	}
	
	public static ResponseTrade doPay(String money,String openid, String tradeType) throws Exception{
		LOG.info("process in doPay...");
		String wx_mp_appid = PropertiesUtil.getProperty("wx_mp_appid");
		String wx_mp_mchid = PropertiesUtil.getProperty("wx_mp_mchid");
//		String wx_mp_submchid = PropertiesUtil.getProperty("wx_mp_submchid");
		String wx_unified_order = PropertiesUtil.getProperty("wx_unified_order");
		String wx_unified_order_notify_recharge = PropertiesUtil.getProperty("wx_unified_order_notify_recharge");
		ResponseTrade trade = new ResponseTrade();
		String paycode = "0";
		String payMsg = "";
		int total = 0;
		WxUnitedOrderRequest request = new WxUnitedOrderRequest();
		request.setAppid(wx_mp_appid);
		request.setMch_id(wx_mp_mchid);
		request.setNonce_str(PayUtils.getRandomString(16));
		request.setBody("饭好粥到");
//		microEntity.setDetail("");
//		microEntity.setAttach("");
		request.setOut_trade_no(PayUtils.getRandomString(10).concat(String.valueOf(new Date().getTime())));
//		microEntity.setFee_type("");
		//微信是按分计算的，这里要乘100
		request.setTotal_fee((new BigDecimal(money).multiply(new BigDecimal(100))).intValue());
		request.setSpbill_create_ip("183.39.198.54");
//		microEntity.setGoods_tag("");
		request.setNotify_url(wx_unified_order_notify_recharge); //回调地址
		request.setTrade_type(tradeType);//"JSAPI");
		request.setOpenid(openid);
//		microEntity.setLimit_pay("");
		
//		microEntity.setDevice_info(device_info);
		request.setSign(signData(request, BizHelper.getWxApiKey()));
		System.out.println(request.toString());
		System.out.println(BizHelper.getWxApiKey());
		XSTREAM.alias("xml", WxUnitedOrderRequest.class);
		String micropayStr = XSTREAM.toXML(request);
		micropayStr = micropayStr.replace("__", "_");
		LOG.info("wx_united_order:requestXml=" + micropayStr);
		//支付请求
		XStream XSTREAM2 = new XStream();
		String result = HttpClient.send(wx_unified_order, micropayStr, "UTF-8", "UTF-8");
		XSTREAM2.alias("xml", WxUnitedOrderResponse.class);
		LOG.info("wx_united_order:resultXml="+result);
		
		WxUnitedOrderResponse wxPrepayResp = (WxUnitedOrderResponse) XSTREAM2.fromXML(result);
		if(!ConstantsPay.SUCCESS.equals(wxPrepayResp.getReturn_code())){
			paycode = PayStatusEmum.PAYERROR.getValue().toString(); //交易明确失败
			payMsg = wxPrepayResp.getReturn_msg();
			
		}else if(!ConstantsPay.SUCCESS.equals(wxPrepayResp.getResult_code())){
			
			paycode = PayStatusEmum.PAYERROR.getValue().toString();
			payMsg = wxPrepayResp.getErr_code_des();
		}
//		else{
//			wx_qrcode_path += File.separator+order.getOrderNo()+".bmp";
//			PayUtils.createRQ(wxPrepayResp.getCode_url(), wx_qrcode_path);
//			wxPrepayResp.setCode_url(wx_qrcode_path);
//			
//			paycode = PayStatusEmum.SUCCESS.getValue().toString();
//			wxPrepayResp.setTotal_fee(String.valueOf(BigDecimal.valueOf(Integer.valueOf(wxPrepayResp.getTotal_fee())).divide(new BigDecimal(100))));
//		}
		
		trade.setErrorCode(paycode);
		trade.setErrorMsg(payMsg);
		trade.setWxUnifiedRes(wxPrepayResp);
		return trade;
	}
	
	
	
    private static String signData(WxUnitedOrderRequest microEntity,String key) throws UnsupportedEncodingException{
		String[] params = { "appid=" + microEntity.getAppid(),"mch_id=" + microEntity.getMch_id(),
				"nonce_str=" + microEntity.getNonce_str(), "body=" + microEntity.getBody(),
				"out_trade_no=" + microEntity.getOut_trade_no(), "total_fee=" + microEntity.getTotal_fee(),
				"spbill_create_ip=" + microEntity.getSpbill_create_ip(), "notify_url=" + microEntity.getNotify_url(),
				"trade_type=" + microEntity.getTrade_type(), "openid=" + microEntity.getOpenid()};
		String param = PayUtils.genSortParams(params);
		param += "&key=" + key;
		LOG.info("before signData:" + param);
		String sign = DigestUtils.md5Hex(param.getBytes("utf-8")).toUpperCase();
		LOG.info("after signData:sign=" + sign + "");
		return sign;
	}
	
}
