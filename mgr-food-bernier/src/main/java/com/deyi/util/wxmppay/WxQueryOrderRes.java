package com.deyi.util.wxmppay;

import com.deyi.controller.pay.wxPay.WxMicroEntityRes;

public class  WxQueryOrderRes extends  WxMicroEntityRes{

	
	private String trade_type;
	private String trade_state;
	private String bank_type;
	/**
	 * 代金券
	 */
	private String coupon_count;
	private String coupon_batch_id_0;
	private String coupon_id_0;
	private String coupon_fee_0;
	private String coupon_id_1;
	private String coupon_fee_1;
	private String coupon_type_0;
	private String coupon_type_1;
	
	private String trade_state_desc;
	private String recall;
	
	public String getTrade_type() {
		return trade_type;
	}
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}
	public String getTrade_state() {
		return trade_state;
	}
	public void setTrade_state(String trade_state) {
		this.trade_state = trade_state;
	}
	public String getBank_type() {
		return bank_type;
	}
	public void setBank_type(String bank_type) {
		this.bank_type = bank_type;
	}
	public String getCoupon_count() {
		return coupon_count;
	}
	public void setCoupon_count(String coupon_count) {
		this.coupon_count = coupon_count;
	}
	public String getTrade_state_desc() {
		return trade_state_desc;
	}
	public void setTrade_state_desc(String trade_state_desc) {
		this.trade_state_desc = trade_state_desc;
	}
	public String getRecall() {
		return recall;
	}
	public void setRecall(String recall) {
		this.recall = recall;
	}
	public String getCoupon_batch_id_0() {
		return coupon_batch_id_0;
	}
	public void setCoupon_batch_id_0(String coupon_batch_id_0) {
		this.coupon_batch_id_0 = coupon_batch_id_0;
	}
	public String getCoupon_id_0() {
		return coupon_id_0;
	}
	public void setCoupon_id_0(String coupon_id_0) {
		this.coupon_id_0 = coupon_id_0;
	}
	public String getCoupon_fee_0() {
		return coupon_fee_0;
	}
	public void setCoupon_fee_0(String coupon_fee_0) {
		this.coupon_fee_0 = coupon_fee_0;
	}
	public String getCoupon_id_1() {
		return coupon_id_1;
	}
	public void setCoupon_id_1(String coupon_id_1) {
		this.coupon_id_1 = coupon_id_1;
	}
	public String getCoupon_fee_1() {
		return coupon_fee_1;
	}
	public void setCoupon_fee_1(String coupon_fee_1) {
		this.coupon_fee_1 = coupon_fee_1;
	}
	public String getCoupon_type_0() {
		return coupon_type_0;
	}
	public void setCoupon_type_0(String coupon_type_0) {
		this.coupon_type_0 = coupon_type_0;
	}
	public String getCoupon_type_1() {
		return coupon_type_1;
	}
	public void setCoupon_type_1(String coupon_type_1) {
		this.coupon_type_1 = coupon_type_1;
	}
}
