package com.deyi.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectWriter.GeneratorSettings;

public class JournalSeq {
	
   /**
    * 	
    * 方法用途: 根据序列生成指定长度的序号<br>
    * @param seq 序列
    * @return
    */
   public static String getSpecifiedLengthSeq(String seq){
	   int length = 11;//生成11位长度的序号
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<length-seq.length();i++){
			sb.append("0");
		}
		sb.append(seq);
		return sb.toString();
	}
   	/**
   	 * 
   	 * 方法用途: 获取数据库表业务主键<br>
   	 * 实现步骤: <br>
   	 * @param prefix 业务前缀
   	 * @param valueLength 数字位长度
   	 * @param NextVal 数据库自增生成数
   	 * @returnaaa
   	 */
   	public static String getTableBusinessId(String prefix,int valueLength, String NextVal){
		StringBuffer sb = new StringBuffer();
		if(!StringUtils.isBlank(prefix)){
			sb.append(prefix);
		}
		for(int i=0;i<valueLength-NextVal.length();i++){
			sb.append("0");
		}
		sb.append(NextVal);
		return sb.toString();
	}
    /**
     * 	
     * 方法用途: 根据序列生成指定长度的序号<br>
     * @param seq 序列
     * @return
     */
    public static String getSpecifiedLengthSeq(String seq,int length){
 		StringBuffer sb = new StringBuffer();
 		for(int i=0;i<length-seq.length();i++){
 			sb.append("0");
 		}
 		sb.append(seq);
 		return sb.toString();
 	}
    
    /**
     * 生成指定的订单号
     */
    public static String getSeriorId(){
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		StringBuffer randomStr = new StringBuffer(String.valueOf((int) (1 + Math.random() * (9999))));
		for (int i = randomStr.length(); i < 4; i++) {
			randomStr.append("0");
		}
		return format.format(new Date()) + randomStr.toString();
	}
    
    /**
     * 生成订单id
     * @return
     */
    public static String getOrderNo(){
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		StringBuffer randomStr = new StringBuffer(String.valueOf((int) (1 + Math.random() * (999999))));
		for (int i = randomStr.length(); i < 6; i++) {
			randomStr.append("0");
		}
		return format.format(new Date()) + randomStr.toString();
	}
    
}
