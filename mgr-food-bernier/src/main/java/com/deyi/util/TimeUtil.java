package com.deyi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TimeUtil {
	public static final String[] CHN_WEEK_DAYS = new String[] {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
	public static final SimpleDateFormat CHN_DATE_WEEK = new SimpleDateFormat("yyyy年MM月dd日 EEEE", Locale.CHINA);
	public static final SimpleDateFormat STD_DF_19 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat STD_DF_12 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static final SimpleDateFormat STD_DF_CN_20 = new SimpleDateFormat("yyyy年MM月dd日  HH:mm:ss");
	public static final SimpleDateFormat STD_DF_10 = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat MERGE_DF_16 = new SimpleDateFormat("yyyy年MM月dd日HH:mm");
	public static final SimpleDateFormat MERGE_YM_8 = new SimpleDateFormat("yyyy年MM月");
	public static final SimpleDateFormat STD_DF_TIMESTAMP = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public static String ConverTime(String value) {
        String res = null;
        try {
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
            long time = Long.parseLong(value);
            res = format.format(time * 1000L);
        } catch (NumberFormatException e) {
            res = "";
        }
        return res;
    }
    /**
     * 获取格式化的年月日时分
     * eg:yyyy-MM-dd HH:mm
     * @param date
     * @return
     */
    public static String getSTD_DF_12Time(Date date){
    	return STD_DF_12.format(date);
    }
    
    public static String getDFTimeStamp(Date date){
    	return STD_DF_TIMESTAMP.format(date);
    }
    
    public static String getDF19Date(Date date){
    	return STD_DF_19.format(date);
    }
    
    public static String getMERGE_YM_8(Date date){
    	return MERGE_YM_8.format(date);
    }
    
    public static String getCHNDateWithWeek(Date date){
    	return CHN_DATE_WEEK.format(date);
    }
    
    public static String getCHNDateWithWeekFromDF19(String dateString){
    	if(dateString==null){
    		return "";
    	}
		try {
			Date date = STD_DF_19.parse(dateString);
			return getCHNDateWithWeek(date);
		} catch (Exception e) {
//			e.printStackTrace();
		}	
		return "";
    }
    public static String getCHNDateWithWeekFromDF10(String dateString){
    	try {
    		Date date = STD_DF_10.parse(dateString);
    		return getCHNDateWithWeek(date);
    	} catch (ParseException e) {
//			e.printStackTrace();
    	}
    	return "";
    }
    
    public static String getSTD_DF_10(Date date){
    	return STD_DF_10.format(date);
    }
    
    public static String getSTD_DF_19FromUnixTime10(Integer unixTime){
    	return getSTD_DF_19(new Date(unixTime * 1000L));
    }
    
    public static String getSTD_DF_10FromUnixTime10(Integer unixTime){
    	return getSTD_DF_10(new Date(unixTime * 1000L));
    }
    
    public static String getSTD_DF_19(Date date){
    	return STD_DF_19.format(date);
    }
    
    public static Date parseDateFromMERGE_DF_16(String dateString){
    	try {
    		Date date = MERGE_DF_16.parse(dateString);
    		return date;
    	} catch (ParseException e) {
//			e.printStackTrace();
    	}
    	return null;
    }
    
    public static Date parseUnixTime10(String unix10){
    	long time = Long.valueOf(unix10) * 1000L;
    	return new Date(time);
    }
    
    public static Date addTime(Date date, int seconds){
    	long timestamp = date.getTime() /1000 + seconds;
    	return new Date(timestamp * 1000);
    }
    
    public static int getUnixTime10(){
    	return (int)(System.currentTimeMillis() / 1000);
    }
    
    public static int getUnixTime10(Date date){
    	return (int)(date.getTime() / 1000);
    }
    
    public static Date getCurrentDateStartTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentDateEndTime(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	cal.set(Calendar.MILLISECOND, 999);
    	return cal.getTime();
    }
    
    public static Date getCurrentWeekStartDate(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_WEEK, 0);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getMondayOfThisWeek() {
    	  Calendar c = Calendar.getInstance();
    	  int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
    	  if (day_of_week == 0)
    	   day_of_week = 7;
    	  c.add(Calendar.DATE, -day_of_week + 1);
    	  c.set(Calendar.HOUR_OF_DAY, 0);
	      c.set(Calendar.MINUTE, 0);
	      c.set(Calendar.SECOND, 0);
	      c.set(Calendar.MILLISECOND, 0);
    	  return c.getTime();
    }
    
    public static Date getSundayOfThisWeek() {
    	  Calendar c = Calendar.getInstance();
    	  int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
    	  if (day_of_week == 0)
    	   day_of_week = 7;
    	  c.add(Calendar.DATE, -day_of_week + 7);
    	  c.set(Calendar.HOUR_OF_DAY, 23);
	      c.set(Calendar.MINUTE, 59);
	      c.set(Calendar.SECOND, 59);
	      c.set(Calendar.MILLISECOND, 999);
    	  return c.getTime();
    }
    
    public static Date getCurrentMonthStartDate(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentMonthEndDate(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	cal.set(Calendar.MILLISECOND, 999);
    	return cal.getTime();
    }
    
    public static Date getCurrentSeasonStartDate(){
    	Calendar cal = Calendar.getInstance();
    	int month = cal.get(Calendar.MONTH);
    	if(month <= 3){
    		month = 0;
    	}else if(month <= 6){
    		month = 3;
    	}else if(month <= 9){
    		month = 6;
    	}else if(month <= 12){
    		month = 9;
    	}
    	cal.set(Calendar.MONTH, month-1);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentSeasonEndDate(){
    	Calendar cal = Calendar.getInstance();
    	Date currentSeasonStartDate = getCurrentSeasonStartDate();
    	cal.setTime(currentSeasonStartDate);
    	cal.add(Calendar.MONTH, 3);
    	cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	cal.set(Calendar.MILLISECOND, 999);
    	return cal.getTime();
    }
    
    public static Date getCurrentYearStartDate(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.MONTH, 0);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	return cal.getTime();
    }
    
    public static Date getCurrentYearEndDate(){
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.MONTH, 11);
    	cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	cal.set(Calendar.MILLISECOND, 999);
    	return cal.getTime();
    }
    
    /**
     * 获取当天是星期几
     * @return int
     */
    public static int getCurrentDayOfWeeK(){
    	Calendar cal = Calendar.getInstance();
    	int dow = cal.get(Calendar.DAY_OF_WEEK);
    	int dayOfWeek=dow-1;
    	if(dayOfWeek==0){
    		dayOfWeek=7;
    	}
    	return dayOfWeek;
    }
    /**
     * 获取当天时间量，只计算小时和分钟
     * 格式为：num=Hour*60+minute
     * @author limh Create on 2016年11月3日 下午8:17:21
     * @param date 时间
     * @return
     */
    public static int getTimeQuantum(Date date){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	int hour = cal.get(Calendar.HOUR_OF_DAY);
    	int minute = cal.get(Calendar.MINUTE);
    	return hour*60+minute;
    }
    /**
     * 按指定天数增加时间
     * @author limh Create on 2016年11月26日 上午10:33:34
     * @param num
     * @return
     */
    public static Date addDay(int num){
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DAY_OF_MONTH, num);
    	return cal.getTime();
    }
    /**
     * 按传入时间增加分钟
     * @author limh Create on 2016年12月3日 上午10:30:49
     * @param date
     * @param num
     * @return
     */
    public static Date addMinute(Date date,int num){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	cal.add(Calendar.MINUTE, num);
    	return cal.getTime();
    }
    /**
     * 已传入时间为基础，将当前时间的时分设置到传入时间返回
     * @author limh Create on 2016年12月3日 上午10:32:01
     * @param date
     * @return
     */
    public static Date getCurrentTime(Date date){
    	Calendar cal = Calendar.getInstance();
    	int i = cal.get(Calendar.HOUR_OF_DAY);
    	int j = cal.get(Calendar.MINUTE);
    	cal.setTime(date);
    	cal.set(Calendar.HOUR_OF_DAY, i);
    	cal.set(Calendar.MINUTE, j);
    	return cal.getTime();
    }
    /**
     * 根据传入的时间，经过滤返回固定分钟的时间，15分，30分，45分，0分
     * @author limh Create on 2016年12月6日 下午3:38:53
     * @param date
     * @return
     */
    public static Date getFixedTime(Date date){
    	Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int i = cal.get(Calendar.HOUR_OF_DAY);
		int j = cal.get(Calendar.MINUTE);
		if(j<15){
			j=15;
		}else if(j<30){
			j=30;
		}else if(j<45){
			j=45;
		}else if(j<60){
			j=0;
			i=i+1;
		}
		cal.set(Calendar.HOUR_OF_DAY, i);
		cal.set(Calendar.MINUTE, j);
		return cal.getTime();
    }
    //晚上时间过了24点之后，天数+1
    public static Date getDateTomorow(Date date){
    	if(date.getHours()<9){//如果
			  Calendar   calendar   =   new   GregorianCalendar(); 
			  calendar.setTime(date); 
			  calendar.add(calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动 
			  date=calendar.getTime();   //这个时间就是日期往后推一天的结果 
		}
    	return date;
    }
    public static Date getDateFromString(String time){
    	java.util.Date date;
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("HH:mm");//小写的mm表示的是分钟  

			date = sdf.parse(time);
			
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    public static void main(String[] args) {
    	Date date = addDay(0);
    	System.out.println(date);
	}
}
