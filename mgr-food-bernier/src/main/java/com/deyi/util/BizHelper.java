package com.deyi.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.deyi.entity.Member;
import com.deyi.message.SmsClient;
import com.deyi.model.JsApiTicket;
import com.deyi.model.WxEvent;
import com.deyi.model.WxTempMsg2Keywords;
import com.deyi.model.WxTemplateMsgRequest;
import com.deyi.model.WxTemplateMsgValue;
import com.deyi.model.WxUserInfo;
import com.deyi.net.BaiduMapApiClient;
import com.deyi.net.WxApiClient;

public class BizHelper {

	private static Logger LOG = LoggerFactory.getLogger(BizHelper.class);
	
	private static String order_pay_no_prifix=null;
	
	private static String wxAppId = null;

	private static String wxAppSecret = null;

	private static String wxMchId = null;

	private static String wxMchName = null;

	private static String wxApiKey = null;

	private static String wxAccessTokenRequestUrl = null;

	private static String wx_template_msg_for_subscribe_id = null;

	private static String wx_template_msg_for_self_subscribe_id = null;

	private static String wx_template_msg_for_member_upgrade_id = null;

	private static String ws_default_copper_id = null;

	private static String ws_slogan_1 = null;

	private static String ws_slogan_2 = null;

	private static String ws_url_shop_usercenter = null;

	private static Integer ws_consume_redpack_trigger_upper_limit = 0;

	private static double ws_consume_profit_share_ratio = 0d;

	private static String baidu_map_api_ak = null;

	private static int ws_follow_redpack_for_self = 0;

	private static String contextRoot;

	private static String uploadFolderPath;

	private static String uploadFolderName;

	private static String serverUrl;

	private static String serverIp;

	private static String userSecretKey;

	private static String[] noNeedWapAuthUrls;

	private static String[] authForUpgrades;

	public static Boolean wsCreateQrCodeForPermanence;
	
	private static String alipay_md5_key;
	
	private static String alipay_appid;
	
	private static String alipay_partner;
	
	private static String alipay_seller_id;
	
	private static String ali_wap_pay_notify_url;
	
	private static String ali_wap_pay_return_url;
	
	private static String ali_wap_show_url;
	
	private static String partner_location_lat;
	
	private static String partner_location_lng;
	
	private static String partner_address;
	
	private static String partner_delivery_radius;
	
	private static String ali_wap_recharge_notify_url;
	
	private static String ali_wap_recharge_return_url;
	
	public static String getAliWapRechargeNotifyUrl(){
		if (ali_wap_recharge_notify_url == null) {
			ali_wap_recharge_notify_url = PropertiesUtil.getProperty("ali_wap_recharge_notify_url");
		}
		return ali_wap_recharge_notify_url;
	}
	
	public static String getAliWapRechargeReturnUrl(){
		if (ali_wap_recharge_return_url == null) {
			ali_wap_recharge_return_url = PropertiesUtil.getProperty("ali_wap_recharge_return_url");
		}
		return ali_wap_recharge_return_url;
	}
	
	public static String getPartnerDeliveryRadius(){
		if (partner_delivery_radius == null) {
			partner_delivery_radius = PropertiesUtil.getProperty("partner_delivery_radius");
		}
		return partner_delivery_radius;
	}
	
	public static String getPartnerAddress(){
		if (partner_address == null) {
			partner_address = PropertiesUtil.getProperty("partner_address");
		}
		return partner_address;
	}
	
	public static String getPartnerLocationLat(){
		if (partner_location_lat == null) {
			partner_location_lat = PropertiesUtil.getProperty("partner_location_lat");
		}
		return partner_location_lat;
	}
	
	
	public static String getPartnerLocationLng(){
		if (partner_location_lng == null) {
			partner_location_lng = PropertiesUtil.getProperty("partner_location_lng");
		}
		return partner_location_lng;
	}
	
	public static String getAliWapShowUrl(){
		if (ali_wap_show_url == null) {
			ali_wap_show_url = PropertiesUtil.getProperty("ali_wap_show_url");
		}
		return ali_wap_show_url;
	}
	
	public static String getAliWapPayNotifyUrl(){
		if (ali_wap_pay_notify_url == null) {
			ali_wap_pay_notify_url = PropertiesUtil.getProperty("ali_wap_pay_notify_url");
		}
		return ali_wap_pay_notify_url;
	}
	
	public static String getAliWapPayReturnUrl(){
		if (ali_wap_pay_return_url == null) {
			ali_wap_pay_return_url = PropertiesUtil.getProperty("ali_wap_pay_return_url");
		}
		return ali_wap_pay_return_url;
	}
	
	public static String getAlipayAppid() {
		if (alipay_appid == null) {
			alipay_appid = PropertiesUtil.getProperty("alipay_appid");
		}
		return alipay_appid;
	}
	
	public static String getAlipayPartner() {
		if (alipay_partner == null) {
			alipay_partner = PropertiesUtil.getProperty("alipay_partner");
		}
		return alipay_partner;
	}
	
	public static String getAlipaySellerId() {
		if (alipay_seller_id == null) {
			alipay_seller_id = PropertiesUtil.getProperty("alipay_seller_id");
		}
		return alipay_seller_id;
	}
	
	public static String getAlipayMd5Key() {
		if (alipay_md5_key == null) {
			alipay_md5_key = PropertiesUtil.getProperty("alipay_md5_key");
		}
		return alipay_md5_key;
	}
	
	public static String getOrderPayNoPrifix() {
		if (order_pay_no_prifix == null) {
			order_pay_no_prifix = PropertiesUtil.getProperty("order_pay_no_prifix");
		}
		return order_pay_no_prifix;
	}
	
	public static String getWxAppId() {
		if (wxAppId == null) {
			wxAppId = PropertiesUtil.getProperty("wx_mp_appid");
		}
		return wxAppId;
	}

	public static String getWxAppSecret() {
		if (wxAppSecret == null) {
			wxAppSecret = PropertiesUtil.getProperty("wx_mp_appsecret");
		}
		return wxAppSecret;
	}

	public static String getWxMchId() {
		if (wxMchId == null) {
			wxMchId = PropertiesUtil.getProperty("wx_mp_mchid");
		}
		return wxMchId;
	}

	public static String getWxMchName() {
		if (wxMchName == null) {
			wxMchName = PropertiesUtil.getProperty("wx_mp_name");
		}
		return wxMchName;
	}

	public static String getWxApiKey() {
		if (wxApiKey == null) {
			wxApiKey = PropertiesUtil.getProperty("wx_mp_apikey");
		}
		return wxApiKey;
	}

	public static String getWxTempMsgForSubscribeId() {
		if (wx_template_msg_for_subscribe_id == null) {
			wx_template_msg_for_subscribe_id = PropertiesUtil.getProperty("wx_template_msg_for_subscribe_id");
		}
		return wx_template_msg_for_subscribe_id;
	}

	public static String getWxTempMsgForMemberUpgradeId() {
		if (wx_template_msg_for_member_upgrade_id == null) {
			wx_template_msg_for_member_upgrade_id = PropertiesUtil.getProperty("wx_template_msg_for_member_upgrade_id");
		}
		return wx_template_msg_for_member_upgrade_id;
	}

	public static String getWxTempMsgForSelfSubscribeId() {
		if (wx_template_msg_for_self_subscribe_id == null) {
			wx_template_msg_for_self_subscribe_id = PropertiesUtil.getProperty("wx_template_msg_for_self_subscribe_id");
		}
		return wx_template_msg_for_self_subscribe_id;
	}

	public static String getWsSlogan1() {
		if (ws_slogan_1 == null) {
			ws_slogan_1 = PropertiesUtil.getProperty("ws_slogan_1");
		}
		return ws_slogan_1;
	}

	public static String getWsSlogan2() {
		if (ws_slogan_2 == null) {
			ws_slogan_2 = PropertiesUtil.getProperty("ws_slogan_2");
		}
		return ws_slogan_2;
	}

	public static String getWsUrlShopUsercenter() {
		if (ws_url_shop_usercenter == null) {
			ws_url_shop_usercenter = PropertiesUtil.getProperty("ws_url_shop_usercenter");
		}
		return ws_url_shop_usercenter;
	}

	public static String getWsDefaultCopper_id() {
		if (ws_default_copper_id == null) {
			ws_default_copper_id = PropertiesUtil.getProperty("ws_default_copper_id");
		}
		return ws_default_copper_id;
	}

	public static Integer getConsumeRedPackTriggerUpperLimit() {
		if (ws_consume_redpack_trigger_upper_limit == 0) {
			ws_consume_redpack_trigger_upper_limit = Integer.valueOf(PropertiesUtil.getProperty("ws_consume_redpack_trigger_upper_limit"));
		}
		return ws_consume_redpack_trigger_upper_limit;
	}

	public static Integer getFollowRedPackForSelf() {
		if (ws_follow_redpack_for_self == 0) {
			ws_follow_redpack_for_self = Integer.valueOf(PropertiesUtil.getProperty("ws_follow_redpack_for_self"));
		}
		return ws_follow_redpack_for_self;
	}

	public static double getConsumeProfitShareRatio() {
		if (ws_consume_profit_share_ratio <= 0d) {
			ws_consume_profit_share_ratio = Double.valueOf(PropertiesUtil.getProperty("ws_consume_profit_share_ratio"));
		}
		return ws_consume_profit_share_ratio;
	}

	public static String getAccessTokenRequestUrl() {
		if (wxAccessTokenRequestUrl == null) {
			wxAccessTokenRequestUrl = String.format(Constants.WX_ACCESS_TOKEN_URL, getWxAppId(), getWxAppSecret());
		}
		return wxAccessTokenRequestUrl;
	}

	public static Boolean getCreateQrCodeForPermanence() {
		if (wsCreateQrCodeForPermanence == null) {
			wsCreateQrCodeForPermanence = Boolean.valueOf(PropertiesUtil.getProperty("ws_create_qrcode_for_permanence"));
		}
		return wsCreateQrCodeForPermanence;
	}

	public static String getUserSecretKey() {
		if (userSecretKey == null) {
			userSecretKey = PropertiesUtil.getProperty("ws_wap_user_secret_key");
		}
		return userSecretKey;
	}

	public static Map<String, String> createPlaceOrderRequest(String prepayId) {
		String wx_mp_appid = PropertiesUtil.getProperty("wx_mp_appid");//

		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("appId", wx_mp_appid);
		requestMap.put("timeStamp", String.valueOf(GchTimeUtil.getUnixTime10()));
		requestMap.put("nonceStr", PayUtils.getRandomString(16));
		requestMap.put("package", "prepay_id=" + prepayId);
		requestMap.put("signType", Constants.P_MD5);
		try {
			String paySign = signPlaceOrder(requestMap);
			requestMap.put("paySign", paySign);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return requestMap;
	}

	public static String signPlaceOrder(Map<String, String> map) throws UnsupportedEncodingException {
		String wx_mp_apikey = PropertiesUtil.getProperty("wx_mp_apikey");//
		String[] params = { "appId=" + map.get("appId"), "timeStamp=" + map.get("timeStamp"), "nonceStr=" + map.get("nonceStr"), "package=" + map.get("package"), "signType=" + map.get("signType") };
		String param = PayUtils.genSortParams(params);
		LOG.info("param=" + param + ",key=" + wx_mp_apikey + "");
		String sign = DigestUtils.md5Hex((param + "&key=" + wx_mp_apikey).getBytes("utf-8")).toUpperCase();
		LOG.info("sign=" + sign + "");
		return sign;
	}

	public static Integer getSceneIdFromEventKey(String eventKey) {
		return Integer.valueOf(eventKey.replace("qrscene_", ""));
	}

	
	public static WxEvent parseCallbackEvent(Map<String, String> requestMap) {
		if (requestMap != null) {
			WxEvent vo = new WxEvent();
			if (requestMap.containsKey("ToUserName")) {
				vo.setToUserName(requestMap.get("ToUserName"));
			}
			if (requestMap.containsKey("FromUserName")) {
				vo.setFromUserName(requestMap.get("FromUserName"));
			}
			if (requestMap.containsKey("CreateTime")) {
				vo.setCreateTime(requestMap.get("CreateTime"));
			}
			if (requestMap.containsKey("MsgType")) {
				vo.setMsgType(requestMap.get("MsgType"));
			}
			if (requestMap.containsKey("Event")) {
				vo.setEvent(requestMap.get("Event"));
			}
			if (requestMap.containsKey("EventKey")) {
				vo.setEventKey(requestMap.get("EventKey"));
			}
			if (requestMap.containsKey("Ticket")) {
				vo.setTicket(requestMap.get("Ticket"));
			}
			if (requestMap.containsKey("MenuId")) {
				vo.setMenuId(requestMap.get("MenuId"));
			}
			if (requestMap.containsKey("Latitude")) {
				vo.setLatitude(requestMap.get("Latitude"));
			}
			if (requestMap.containsKey("Longitude")) {
				vo.setLongitude(requestMap.get("Longitude"));
			}
			if (requestMap.containsKey("Precision")) {
				vo.setPrecision(requestMap.get("Precision"));
			}

			return vo;
		}
		return null;
	}
	
	
	public static void fillUserInfo(Member member, WxUserInfo wxUserInfo) {
		member.setSubStatus(wxUserInfo.getSubscribe());
		member.setWxOpenId(wxUserInfo.getOpenid());
		member.setMemberNick(StringUtils.filterEmoji(wxUserInfo.getNickname()));
		member.setSex(wxUserInfo.getSex());
		member.setCity(wxUserInfo.getCity());
		member.setProvince(wxUserInfo.getProvince());
		member.setCountry(wxUserInfo.getCountry());
		//TODO 尽量将图片url对应的图片下载下来，不然如果图片更换，url便访问不了
		member.setProfileImg(wxUserInfo.getHeadimgurl());
		member.setWxUnionid(wxUserInfo.getUnionid());
		member.setRemark(wxUserInfo.getRemark());
//		member.setWxGroupid(wxUserInfo.getGroupid());
		member.setSubTime(new Date());
//		member.setMemberRank(1);
//		member.setMember_type(Constants.T_MEMBER_TYPE_COPPER);
	}

	// 会员加入提醒
	public static WxTemplateMsgRequest<WxTempMsg2Keywords> createTemplateMsgForSelfSubscribeRequest(Member member) {
		WxTempMsg2Keywords subscribeVo = new WxTempMsg2Keywords();
		String first = String.format("%s, 欢迎您加入%s", member.getMemberNick(), getWxMchName());
		String keyword1 = String.valueOf(member.getMemberId());
		String keyword2 = GchTimeUtil.getDF19Date(member.getSubTime());
		String remark = BizHelper.getWsSlogan1();
		String color = "#FB7417";

		subscribeVo.setFirst(new WxTemplateMsgValue(first, color));
		subscribeVo.setKeyword1(new WxTemplateMsgValue(keyword1, color));
		subscribeVo.setKeyword2(new WxTemplateMsgValue(keyword2, color));
		subscribeVo.setRemark(new WxTemplateMsgValue(remark, color));

		WxTemplateMsgRequest<WxTempMsg2Keywords> requestVo = new WxTemplateMsgRequest<>();
		requestVo.setData(subscribeVo);
		requestVo.setUrl(getWsUrlShopUsercenter());
		requestVo.setTouser(member.getWxOpenId());
		requestVo.setTemplate_id(getWxTempMsgForSelfSubscribeId());
		return requestVo;
	}
	
	
	public static String getContextRoot() {
		if (contextRoot == null) {
			contextRoot = PropertiesUtil.getProperty("ws_context_root");
		}
		return contextRoot;
	}

	public static String getUploadFolderName() {
		if (uploadFolderName == null) {
			uploadFolderName = PropertiesUtil.getProperty("ws_upload_folder_name");
		}
		return uploadFolderName;
	}

	public static String getServerUrl() {
		if (serverUrl == null) {
			serverUrl = PropertiesUtil.getProperty("ws_server_url");
		}
		return serverUrl;
	}

	public static String getServerIp() {
		if (serverIp == null) {
			serverIp = PropertiesUtil.getProperty("ws_server_ip");
		}
		return serverIp;
	}

	public static String[] getWapNoNeedAuthUrls() {
		if (noNeedWapAuthUrls == null) {
			String value = PropertiesUtil.getProperty("ws_wap_no_need_auth_urls");
			noNeedWapAuthUrls = value.split(",");
		}
		return noNeedWapAuthUrls;
	}

	public static String[] getAuthForUpgrades() {
		if (authForUpgrades == null) {
			String value = PropertiesUtil.getProperty("ws_auth_for_upgrade");
			authForUpgrades = value.split(",");
		}
		return authForUpgrades;
	}

	public static String getUploadFolderPath(HttpServletRequest httpRequest) {
		if (uploadFolderPath == null) {
			String rootPath = httpRequest.getRealPath("/");
			uploadFolderPath = rootPath.replace(BizHelper.getContextRoot(), BizHelper.getUploadFolderName());
		}
		LOG.info("uploadFolderPath=" + uploadFolderPath);
		return uploadFolderPath;
	}

	public static String generateDatetimePath(String basePath) {
		String datetimepath = GchTimeUtil.getSTD_DF_10(new Date());
		datetimepath = basePath + datetimepath.replace("-", File.separator);
		return datetimepath;
	}

	public static String getHttpPath(String filepath) {
		String contextRoot = getContextRoot();
		String uploadFolder = getUploadFolderName();
		if (filepath.contains(contextRoot)) {
			return getHttpPathForKeyword(contextRoot, filepath);
		} else if (filepath.contains(uploadFolder)) {
			return getHttpPathForKeyword(uploadFolder, filepath);
		}
		return null;
	}

	public static String getHttpPathForKeyword(String keyword, String filepath) {
		filepath = filepath.substring((filepath.indexOf(keyword)));// .replaceAll("\\",
																	// "/");
		return getServerUrl() + filepath;
	}

	/**
	 * 得到MD5密码（小写）
	 * 
	 * @param password
	 * @return
	 */
	public static String getMd5Password(String password) {
		return MD5.md5(password + getUserSecretKey()).toLowerCase();
	}

	/**
	 * 检测uri地址是否需要Wap登录授权
	 * 
	 * @param uri
	 * @return
	 */
	public static boolean isNoNeedWapAuth(String uri) {
		noNeedWapAuthUrls = getWapNoNeedAuthUrls();
		for (int i = 0; i < noNeedWapAuthUrls.length; i++) {
			if (uri.contains(noNeedWapAuthUrls[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 检测uri地址是否需要userinfo登录授权
	 * 
	 * @param uri
	 * @return
	 */
	public static boolean isNeedAuthForUpgrades(String uri) {
		authForUpgrades = getAuthForUpgrades();
		for (int i = 0; i < authForUpgrades.length; i++) {
			if (uri.contains(authForUpgrades[i])) {
				return true;
			}
		}
		return false;
	}

	public static String getBaiduMapApiAk() {
		if (baidu_map_api_ak == null) {
			baidu_map_api_ak = PropertiesUtil.getProperty("baidu_map_api_ak");
		}
		return baidu_map_api_ak;
	}

	public static Map<String, String> getCurrentCityByLocation(String latitude, String longitude) {
		Map<String, Object> resultMap = new BaiduMapApiClient().geocoderRenderReverse(latitude + "," + longitude);
		Map<String, String> hashMap = new HashMap<String, String>();
		if (resultMap.containsKey("result")) {
			Map<String, Object> result = (Map<String, Object>) resultMap.get("result");
			if (result.containsKey("addressComponent")) {
				Map<String, Object> addressComponent = (Map<String, Object>) result.get("addressComponent");

				if (addressComponent.containsKey("city")) {
					hashMap.put("city", String.valueOf(addressComponent.get("city")));
					hashMap.put("province", String.valueOf(addressComponent.get("province")));

					return hashMap;
				}
			}
		}
		return null;
	}

	public static String sendVerifyCode(HttpServletRequest httpRequest, String account) {
		HttpSession session = httpRequest.getSession();

		String verificationCode = StringUtils.getVerificationCode();
		System.out.println(verificationCode);
		String mdn = account;
		String message = "【得壹科技】您的验证码为：" + verificationCode + "，请在10分钟内完成验证，如非本人操作，请忽略";
		try {
			// 单次发送短信
			SmsClient.send(mdn, message);
			// 将验证码放入SESSION
			session.setAttribute(account, verificationCode);
			return verificationCode;
		} catch (Exception e) {
			LOG.error("sendVerifyCode:" + e.getMessage());

		}
		return null;
	}
	/**
	 * 订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成 9订单已评价
	 * 支付状态 0.未支付 1支付成功  2支付失败  3退款中 4 已退款 5退款失败
	 * @param orderStatus 订单状态
	 * @return String 待付款 配送中  已完成  已取消
	 */
	public static String converOrderStatus(String orderStatus,String payStatus){
		String result="";
		if("0".equals(payStatus)){
			if("0".equals(orderStatus)){
				result="待付款";
			}else if("6".equals(orderStatus)){
				result="已取消";
			}else{
				result="待付款";
			}
		}else if("1".equals(payStatus)){
			if("0".equals(orderStatus)){
//				result="未处理";
				result="已支付";
			}else if("1".equals(orderStatus)){
//				result="已确认";
				result="已支付";
			}else if("2".equals(orderStatus)){
//				result="备货完成";
				result="已支付";
			}else if("3".equals(orderStatus)){
				result="配送中";
			}else if("4".equals(orderStatus)){
				result="已完成";
//				result="订单成功";
			}else if("5".equals(orderStatus)){
				result="订单失败";
			}else if("6".equals(orderStatus)){
				result="已取消";
			}else if("7".equals(orderStatus)){
				result="配送员接单";
			}else if("8".equals(orderStatus)){
				result="配送完成";
			}else if("9".equals(orderStatus)){
				result="已评价";
			}else if("10".equals(orderStatus)){
				result="已分配厨师";
			}else{
				throw new RuntimeException("不能识别的订单状态");
			}
		}else if("2".equals(payStatus)){
			
		}else if("3".equals(payStatus)){
			
		}else if("4".equals(payStatus)){
			
		}else if("5".equals(payStatus)){
			
		}
	
		return result;
	}
	
	/**
	 * 获取订单流水号
	 * 生成规则
	 * prefix+year+month+day+hour+minute+second+random*3
	 * 年月日时分秒+3位随机数,如20161022180259001
	 * @return string 
	 */
	public static String getPayNo(){
		StringBuffer sb = new StringBuffer();
		//系统配置前缀
		String orderPayNoPrifix = getOrderPayNoPrifix();
		Calendar time = Calendar.getInstance();
		sb.append(orderPayNoPrifix);
		//追加年月日
		sb.append(time.get(Calendar.YEAR)).append(time.get(Calendar.MONTH)).append(time.get(Calendar.DAY_OF_MONTH));
		//追加时分秒
		sb.append(time.get(Calendar.HOUR_OF_DAY)).append(time.get(Calendar.MINUTE)).append(time.get(Calendar.SECOND));
		//追加三位随机数,范围在100-999之间
		sb.append(100+new Random().nextInt(900));
		return sb.toString();
	}
	
	/**
	 * 获取支付宝POST过来反馈信息
	 * 获取所有的请求参数（不包含重复的数组）
	 * @param request
	 * @return
	 */
	public static Map<String, String> getHttpRequestParameters(HttpServletRequest request) {
		// 获取支付宝POST过来反馈信息
		Map<String, String> params = new HashMap<String, String>();
		Map<String, String[]> requestParams = request.getParameterMap();
		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
			}
			// 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			// valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}

		return params;
	}
	/**
	 * 获取当前星期数，并将其转换为字符串返回
	 * @author limh Create on 2016年10月26日 下午2:06:11
	 * @return String
	 */
	public static String getConvertedDayOfWeek(){
		int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
		return String.valueOf(currentDayOfWeeK);
	}
	/**
	 * 转换预定订单类型
	 * 预定类型:1:预定今日午餐，2:预定今日晚餐,3:预定明日中餐
	 * @author limh Create on 2016年10月26日 下午2:08:47
	 * @param bookType
	 * @return
	 */
	/*public static String converOrderType(String bookType){
		String result="";
		if(Constants.BOOK_TYPE_TODAY_LUNCH.equals(bookType)){
			result="订购今日午餐";
		}else if(Constants.BOOK_TYPE_TODAY_SUPPER.equals(bookType)){
			result="订购今日晚餐";
		}else if(Constants.BOOK_TYPE_BOOK_TOMORROW_LUNCH.equals(bookType)){
			result="预定明日午餐";
		}else if(Constants.BOOK_TYPE_BOOK_SUPPER.equals(bookType)){
			result="预定今日晚餐";
		}
		return result;
	}*/
	
	public static String getJsApiSignature(String jsapi_ticket, String noncestr, String timestamp, String url)
			throws UnsupportedEncodingException {
		String[] params = { "jsapi_ticket=" + jsapi_ticket, "noncestr=" + noncestr, "timestamp=" + timestamp,
				"url=" + url };
		String param = PayUtils.genSortParams(params);
		LOG.info("param=" + param);
		String sign = DigestUtils.sha1Hex(param).toLowerCase();
		LOG.info("sign=" + sign + "");
		return sign;
	}
	
	public static String signWapAliPayOrder(Map<String, String> map) throws UnsupportedEncodingException {
		String[] params = { "_input_charset=" + map.get("_input_charset"), "notify_url=" + map.get("notify_url"),
				"out_trade_no=" + map.get("out_trade_no"), "partner=" + map.get("partner"),
				"payment_type=" + map.get("payment_type"), "return_url=" + map.get("return_url"),
				"seller_id=" + map.get("seller_id"), "service=" + map.get("service"), "show_url=" + map.get("show_url"),
				"subject=" + map.get("subject"), "total_fee=" + map.get("total_fee"), };
		String param = PayUtils.genSortParams(params);
		param += BizHelper.getAlipayMd5Key();
		LOG.info("param=" + param);
		String sign = DigestUtils.md5Hex((param).getBytes("utf-8"));//.toUpperCase();
		LOG.info("sign=" + sign + "");
		return sign;
	}
	
	public static String getWapAliPayUrl(Map<String, String> map) throws UnsupportedEncodingException {
		StringBuffer sb = new StringBuffer();
		sb.append(AlipayUtil.ALIPAY_SERVICE_URL);
		Set<String> keySet = map.keySet();
		for (String key : keySet) {
			sb.append(key).append("=").append(URLEncoder.encode(map.get(key), "utf-8")).append("&");
		}
		return sb.substring(0, sb.length() - 1);
	}
	/**
	 * 获取微信jssdk配置信息
	 * @author limh Create on 2016年11月1日 下午2:08:48
	 * @param accessToken
	 * @param url
	 * @return
	 */
	public static Map<String, Object> getWxInitConfig(String accessToken,String url) {
		String wx_mp_appid = PropertiesUtil.getProperty("wx_mp_appid");//
		String[] jsApiList={"onMenuShareAppMessage","onMenuShareTimeline","onMenuShareWeibo"};
		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("appId", wx_mp_appid);
		requestMap.put("timestamp", String.valueOf(GchTimeUtil.getUnixTime10()));
		requestMap.put("noncestr", PayUtils.getRandomString(16));
		requestMap.put("jsApiList", jsApiList);
		if(url==null||"".equals(url)){
			//如果地址未空则默认跳转首页
			url="http://d3.5deyi.com/kitchen/h5/index.html";
		}
		requestMap.put("url", url);
		try {
			WxApiClient apiClient = new WxApiClient();
			JsApiTicket ticketVo = apiClient.getJsApiTicket(accessToken, "jsapi");
			 String string1 = "jsapi_ticket=" + ticketVo.getTicket() +
	                  "&noncestr=" + (String)requestMap.get("noncestr") +
	                  "&timestamp=" + (String)requestMap.get("timestamp") +
	                  "&url=" + (String)requestMap.get("url");
			 System.out.println(string1);
			 try
		        {
		            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
		            crypt.reset();
		            crypt.update(string1.getBytes("UTF-8"));
		            String signature = byteToHex(crypt.digest());
		            requestMap.put("signature", signature);
		            System.out.println(signature);
		        }
		        catch (NoSuchAlgorithmException e)
		        {
		            e.printStackTrace();
		        }
		        catch (UnsupportedEncodingException e)
		        {
		            e.printStackTrace();
		        }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestMap;
	}
	
	 private static String byteToHex(final byte[] hash) {
	        Formatter formatter = new Formatter();
	        for (byte b : hash)
	        {
	            formatter.format("%02x", b);
	        }
	        String result = formatter.toString();
	        formatter.close();
	        return result;
	    }
	
}
