package com.deyi.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * pure
 * @author hejx
 * @2016年3月19日
 */
public class PropertiesUtil {
	private final static Logger LOG = LoggerFactory.getLogger(PropertiesUtil.class);
	/**
	 * 读取配置文件对象
	 */
	private final static Properties proper = new Properties();
	
	static
	{
		try {
		/*	InputStream is =  Thread.currentThread().getContextClassLoader().getResourceAsStream("/config/sysconfig-product.properties");*/
			InputStream is =  Thread.currentThread().getContextClassLoader().getResourceAsStream("/config/sysconfig-htj.properties");
			if(null == is){
				System.out.println();
				LOG.error("配置文件sysconfig.properties没有正常加载...");
			}
			proper.load(is);
		} catch (IOException e) {
			LOG.error("PropertiesUtil error",e);
		}
	}

	/**
	 * 方法用途: 获取配置信息<br>
	 * 实现步骤: <br>
	 * 
	 * @param key
	 *            配置信息的键值
	 * @return
	 */
	public static String getProperty(String key) {
		return proper.getProperty(key);
	}

}
