package com.deyi.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

import com.github.junrar.Archive;
import com.github.junrar.rarfile.FileHeader;

/**
 * @author liuBf 类说明:解压文件公用类
 *
 */
public class UnZipOrRarUtils {
	/*** 这里用到了synchronized ，也就是防止出现并发问题 ***/

	/**
	 * 
	 * @Author tangj Create on 2016年10月28日 下午3:30:06
	 * @param tarFileName
	 *            压缩文件全路径名称
	 * @param extPlace
	 *            解压到的目录
	 * @throws Exception
	 */
	public static synchronized boolean untar(String tarFileName, String extPlace) throws Exception {
		return unRarFile(tarFileName, extPlace);
	}

	/**
	 * 解压zip格式的压缩文件到指定位置
	 * 
	 * @param zipFileName
	 *            压缩文件全路径名称
	 * @param extPlace
	 *            解压到的目录
	 * @param unzipChildrenDirectory
	 *            是否解压其中的文件夹
	 * @param unzipFileStrs
	 *            解压后文件的名称集合
	 * @return
	 * @throws Exception
	 */
	public static synchronized boolean unzip(String zipFileName, String extPlace, boolean unzipChildrenDirectory,
			List<String> unzipFileStrs) throws Exception {
		System.out.println("解压开始");
		long startTime = System.currentTimeMillis();

		boolean flag = unZipFiles(zipFileName, extPlace, unzipChildrenDirectory, unzipFileStrs);

		long endTime = System.currentTimeMillis();
		System.out.println("解压结束,耗费时间： " + (endTime - startTime) + " ms");
		return flag;
	}

	/**
	 * 解压zip格式的压缩文件到指定位置
	 * 
	 * @param zipFileName
	 *            压缩文件
	 * @param extPlace
	 *            解压目录
	 * @param unzipChildrenDirectory
	 *            是否解压其中的文件夹
	 * @param unzipFileStrs
	 *            解压后文件的名称集合
	 * @return
	 * @throws Exception
	 */

	public static boolean unZipFiles(String zipFileName, String extPlace, boolean unzipChildrenDirectory,
			List<String> unzipFileStrs) throws Exception {
		File file = new File(zipFileName);
		boolean unZipFiles = unZipFiles(file, extPlace, unzipChildrenDirectory, unzipFileStrs);
		return unZipFiles;
	}

	/**
	 * 解压zip格式的压缩文件到指定位置
	 * 
	 * @param file
	 *            压缩文件
	 * @param extPlace
	 *            解压目录
	 * @param unzipChildrenDirectory
	 *            是否解压其中的文件夹
	 * @param unzipFileStrs
	 *            解压后文件的名称集合
	 * @return
	 * @throws Exception
	 */

	public static boolean unZipFiles(File file, String extPlace, boolean unzipChildrenDirectory,
			List<String> unzipFileStrs) throws Exception {
		System.setProperty("sun.zip.encoding", System.getProperty("sun.jnu.encoding"));
		
		InputStream is = null;  
        OutputStream os = null;  
        ZipFile zipFile = null;
        
		try {
			if (!file.getName().toLowerCase().endsWith(".zip")) {
				throw new Exception("非zip文件！");
			}

			(new File(extPlace)).mkdirs();
			zipFile = new ZipFile(file, "GBK"); // 处理中文文件名乱码的问题

			if ((!file.exists()) && (file.length() <= 0)) {
				throw new Exception("要解压的文件不存在!");
			}
			String strPath, gbkPath, strtemp;
			File tempFile = new File(extPlace);
			strPath = tempFile.getAbsolutePath();
			Enumeration<?> e = zipFile.getEntries();
			while (e.hasMoreElements()) {
				ZipEntry zipEnt = (ZipEntry) e.nextElement();
				gbkPath = zipEnt.getName();
				if (zipEnt.isDirectory()) {
					// 是否忽略文件夹
					if (unzipChildrenDirectory) {
						strtemp = strPath + File.separator + gbkPath;
						File dir = new File(strtemp);
						dir.mkdirs();
					}
					continue;
				} else {
					if (!unzipChildrenDirectory) {
						int indexOf = gbkPath.indexOf("/");
						if (indexOf != -1) {
							continue;
						}
					}

					

					// 建目录
					gbkPath = zipEnt.getName();
					unzipFileStrs.add(gbkPath);
					strtemp = strPath + File.separator + gbkPath;
					String strsubdir = gbkPath;
					for (int i = 0; i < strsubdir.length(); i++) {
						if (strsubdir.substring(i, i + 1).equalsIgnoreCase("/")) {
							String temp = strPath + File.separator + strsubdir.substring(0, i);
							File subdir = new File(temp);
							if (!subdir.exists())
								subdir.mkdir();
						}
					}
					// 读写文件
	                is = zipFile.getInputStream(zipEnt);  
	                os = new FileOutputStream(strtemp);  
	                byte[] bytes = new byte[1024];  
	                int len = 0;  
	                while ((len = is.read(bytes, 0, bytes.length)) != -1) {  
	                    os.write(bytes, 0, len);  
	                }  
	                is.close();  
	                os.close(); 
				}
			}
			zipFile.close();
			return true;
		} catch (Exception e) {
			
			if(is != null)
				is.close();
			if(os != null)
				os.close();
			if(zipFile != null)
				zipFile.close();
			System.gc();
			System.out.println("解压失败");
			throw e;
		}
	}

	/**
	 * 根据原始rar路径，解压到指定文件夹下.
	 * 
	 * @param srcRarPath
	 *            原始rar路径
	 * @param dstDirectoryPath
	 *            解压到的文件夹
	 * @throws Exception
	 */
	public static boolean unRarFile(String srcRarPath, String dstDirectoryPath) throws Exception {
		return unRarFile(new File(srcRarPath), dstDirectoryPath);
	}

	/**
	 * 根据原始rar路径，解压到指定文件夹下.
	 * 
	 * @param file
	 *            原始rar路径
	 * @param dstDirectoryPath
	 *            解压到的文件夹
	 * @throws Exception
	 */
	public static boolean unRarFile(File file, String dstDirectoryPath) throws Exception {

		try {
			if (!file.getName().toLowerCase().endsWith(".rar")) {
				throw new Exception("非rar文件！");
			}
			if ((!file.exists()) && (file.length() <= 0)) {
				throw new Exception("要解压的文件不存在!");
			}
			File dstDiretory = new File(dstDirectoryPath);
			if (!dstDiretory.exists()) {// 目标目录不存在时，创建该文件夹
				dstDiretory.mkdirs();
			}
			Archive a = null;

			a = new Archive(file);
			if (a != null) {
				// a.getMainHeader().print(); // 打印文件信息.
				FileHeader fh = a.nextFileHeader();
				while (fh != null) {
					// 防止文件名中文乱码问题的处理

					String fileName = fh.getFileNameW().isEmpty() ? fh.getFileNameString() : fh.getFileNameW();
					if (fh.isDirectory()) { // 文件夹
						File fol = new File(dstDirectoryPath + File.separator + fileName);
						fol.mkdirs();
					} else { // 文件
						File out = new File(dstDirectoryPath + File.separator + fileName.trim());
						try {
							if (!out.exists()) {
								if (!out.getParentFile().exists()) {// 相对路径可能多级，可能需要创建父目录.
									out.getParentFile().mkdirs();
								}
								out.createNewFile();
							}
							FileOutputStream os = new FileOutputStream(out);
							a.extractFile(fh, os);
							os.close();
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					fh = a.nextFileHeader();
				}
				a.close();
			}
		} catch (Exception e) {
			System.out.println("解压失败");
			throw e;
		}
		return true;
	}

	/**
	 * 获取压缩文件中的文件数
	 * 
	 * @Author tangj Create on 2016年10月28日 下午3:56:02
	 * @param zipFileName
	 * @return
	 * @throws Exception
	 */
	public static Integer getZipFileNum(String zipFileName) throws Exception {
		return getZipFileNum(new File(zipFileName));
	}

	/**
	 * 获取压缩文件中的文件数
	 * 
	 * @Author tangj Create on 2016年10月28日 下午3:56:02
	 * @param zipFileName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static Integer getZipFileNum(File file) throws Exception {
		System.setProperty("sun.zip.encoding", System.getProperty("sun.jnu.encoding"));
		int num = 0;
		try {
			java.util.zip.ZipFile zipFile = new java.util.zip.ZipFile(file);
			if ((!file.exists()) && (file.length() <= 0)) {
				throw new Exception("文件不存在!");
			}
			num = zipFile.size();
			zipFile.close();
			System.out.println(num);

		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		return num;
	}

	/**
	 * 根据路径删除指定的目录或文件，无论存在与否
	 * 
	 * @param sPath
	 *            要删除的目录或文件
	 * @return 删除成功返回 true，否则返回 false。
	 */
	public static boolean DeleteFolder(String sPath) {
		boolean flag = false;
		File file = new File(sPath);
		// 判断目录或文件是否存在
		if (!file.exists()) { // 不存在返回 false
			return flag;
		} else {
			// 判断是否为文件
			if (file.isFile()) { // 为文件时调用删除文件方法
				return deleteFile(sPath);
			} else { // 为目录时调用删除目录方法
				return deleteDirectory(sPath);
			}
		}
	}

	/**
	 * 删除单个文件
	 * 
	 * @param sPath
	 *            被删除文件的文件名
	 * @return 单个文件删除成功返回true，否则返回false
	 */
	public static boolean deleteFile(String sPath) {
		boolean flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			flag = file.delete();
		}

		if (flag) {
			System.out.println("删除成功");
		} else {
			System.out.println("删除失败");
		}
		return flag;
	}

	/**
	 * 删除目录（文件夹）以及目录下的文件
	 * 
	 * @param sPath
	 *            被删除目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static boolean deleteDirectory(String sPath) {
		// 如果sPath不以文件分隔符结尾，自动添加文件分隔符
		if (!sPath.endsWith(File.separator)) {
			sPath = sPath + File.separator;
		}
		File dirFile = new File(sPath);
		// 如果dir对应的文件不存在，或者不是一个目录，则退出
		if (!dirFile.exists() || !dirFile.isDirectory()) {
			return false;
		}
		boolean flag = true;
		// 删除文件夹下的所有文件(包括子目录)
		File[] files = dirFile.listFiles();
		for (int i = 0; i < files.length; i++) {
			// 删除子文件
			if (files[i].isFile()) {
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag)
					break;
			} // 删除子目录
			else {
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag)
					break;
			}
		}
		if (!flag)
			return false;
		// 删除当前目录
		if (dirFile.delete()) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		try {
			 unzip("C:\\Users\\admin\\Desktop\\笔记\\1\\BeyondCompare.zip","C:\\Users\\admin\\Desktop\\笔记\\1",false,new ArrayList<String>());
			// getZipFileNum("C:\\Users\\admin\\Desktop\\笔记\\1\\BeyondCompare.zip");
//			boolean deleteFile = deleteFile("C:\\Users\\admin\\Desktop\\笔记\\1\\BeyondCompare.zip");
//			System.out.println("删除："+deleteFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}