package com.deyi.model;

import java.util.HashMap;
import java.util.Map;

public class ConditionModel {
	private Map<String,Object> params=new HashMap<>();

	public Map<String, Object> getParams() {
		if(params==null){
			params=new HashMap<>();
		}
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	
}
