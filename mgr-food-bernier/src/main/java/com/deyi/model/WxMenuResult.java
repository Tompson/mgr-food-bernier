package com.deyi.model;

public class WxMenuResult {

	private WxMenu menu;

	public WxMenu getMenu() {
		return menu;
	}

	public void setMenu(WxMenu menu) {
		this.menu = menu;
	}
}
