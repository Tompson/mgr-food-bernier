package com.deyi.model;

public class BookType {
	private String bookType;

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}
}
