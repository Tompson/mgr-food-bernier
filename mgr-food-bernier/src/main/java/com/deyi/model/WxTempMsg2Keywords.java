package com.deyi.model;

public class WxTempMsg2Keywords {

	private WxTemplateMsgValue first;
	
	private WxTemplateMsgValue keyword1;
	
	private WxTemplateMsgValue keyword2;
	
	private WxTemplateMsgValue remark;

	public WxTemplateMsgValue getFirst() {
		return first;
	}

	public void setFirst(WxTemplateMsgValue first) {
		this.first = first;
	}

	public WxTemplateMsgValue getKeyword1() {
		return keyword1;
	}

	public void setKeyword1(WxTemplateMsgValue keyword1) {
		this.keyword1 = keyword1;
	}

	public WxTemplateMsgValue getKeyword2() {
		return keyword2;
	}

	public void setKeyword2(WxTemplateMsgValue keyword2) {
		this.keyword2 = keyword2;
	}

	public WxTemplateMsgValue getRemark() {
		return remark;
	}

	public void setRemark(WxTemplateMsgValue remark) {
		this.remark = remark;
	}
	
	
}
