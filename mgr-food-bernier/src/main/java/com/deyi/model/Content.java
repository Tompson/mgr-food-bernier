package com.deyi.model;
public class Content{
	
	private Location location;
	private String locid;
	private String radius;
	private String confidence;
	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	/**
	 * @return the locid
	 */
	public String getLocid() {
		return locid;
	}
	/**
	 * @param locid the locid to set
	 */
	public void setLocid(String locid) {
		this.locid = locid;
	}
	/**
	 * @return the radius
	 */
	public String getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(String radius) {
		this.radius = radius;
	}
	/**
	 * @return the confidence
	 */
	public String getConfidence() {
		return confidence;
	}
	/**
	 * @param confidence the confidence to set
	 */
	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}
	
	
	
}