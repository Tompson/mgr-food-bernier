package com.deyi.model;
public class Result{
	private String error;
	
	private String loc_time;

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the loc_time
	 */
	public String getLoc_time() {
		return loc_time;
	}

	/**
	 * @param loc_time the loc_time to set
	 */
	public void setLoc_time(String loc_time) {
		this.loc_time = loc_time;
	}

	
}