package com.deyi.model;

public class VoucherQueryCondition {
	private Integer memberId;
	private Double amountPrice;
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public Double getAmountPrice() {
		return amountPrice;
	}
	public void setAmountPrice(Double amountPrice) {
		this.amountPrice = amountPrice;
	}
}
