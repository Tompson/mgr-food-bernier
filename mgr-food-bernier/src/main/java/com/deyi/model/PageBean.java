package com.deyi.model;

import java.util.List;

import com.deyi.util.Page;

public class PageBean<T> {
	
	private List<T> list;
	
	private Page<T> page;

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Page<T> getPage() {
		return page;
	}

	public void setPage(Page<T> page) {
		this.page = page;
	}

	@Override
	public String toString() {
		return "PageBean [list=" + list + ", page=" + page + "]";
	}

}
