package com.deyi.model;

import java.util.Date;

public class WxSceneUsage {
	
	private String id;
	
	private Integer scene_id;
	
	private Integer scene_type;
	
	private String scene_data;
	
	private Integer expires_in;
	
	private Date create_time;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getScene_id() {
		return scene_id;
	}

	public void setScene_id(Integer scene_id) {
		this.scene_id = scene_id;
	}

	public Integer getScene_type() {
		return scene_type;
	}

	public void setScene_type(Integer scene_type) {
		this.scene_type = scene_type;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public String getScene_data() {
		return scene_data;
	}

	public void setScene_data(String scene_data) {
		this.scene_data = scene_data;
	}

}
