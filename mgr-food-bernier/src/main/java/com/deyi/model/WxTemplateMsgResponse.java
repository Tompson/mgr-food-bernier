package com.deyi.model;

public class WxTemplateMsgResponse extends WxErrorVo{

	private Integer msgid;

	public Integer getMsgid() {
		return msgid;
	}

	public void setMsgid(Integer msgid) {
		this.msgid = msgid;
	}
}
