package com.deyi.model;

public class ResultVo<T> {

	private boolean success = true;
	
	private String msg;
	
	private T data;

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ResultVo [success=" + success + ", msg=" + msg + ", data=" + data + "]";
	}
}
