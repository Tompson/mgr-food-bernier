package com.deyi.model.vo;

import java.util.List;

import com.deyi.entity.CategoryGoods;
import com.deyi.util.Page;

/**
 * 封装菜品信息和分类信息
 * @author limh Create on 2016年10月24日 下午4:37:46
 */
public class GoodsInfo {
	private List<GoodsVo> goodsVoList;
	
	private List<CategoryGoods> categoryList;
	
	private Page<GoodsVo> page;
	
	private String amountPrice;
	public List<GoodsVo> getGoodsVoList() {
		return goodsVoList;
	}

	public void setGoodsVoList(List<GoodsVo> goodsVoList) {
		this.goodsVoList = goodsVoList;
	}

	public List<CategoryGoods> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<CategoryGoods> categoryList) {
		this.categoryList = categoryList;
	}

	public Page<GoodsVo> getPage() {
		return page;
	}

	public void setPage(Page<GoodsVo> page) {
		this.page = page;
	}

	public String getAmountPrice() {
		return amountPrice;
	}

	public void setAmountPrice(String amountPrice) {
		this.amountPrice = amountPrice;
	}
}
