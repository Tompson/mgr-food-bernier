package com.deyi.model.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.deyi.entity.MemberVoucher;
import com.deyi.util.StringUtils;

public class MemberVoucherVo extends MemberVoucher{
	
	private BigDecimal voucherMoney;
	
    private String voucherType;

    private Double exchangeVoucher;

    private BigDecimal bookOrderVoucher;

    private BigDecimal drawVoucher;

    private BigDecimal drawProbability;
    
    private Date startTime;

    private Date endTime;
    //代金券金额
    private String formatVoucherMoney;
    //获取代金券最低预定消费金额
    private String formatBookOrderVoucher;
    //使用代金券最低消费金额
    private String formatDrawVoucher;
	public BigDecimal getVoucherMoney() {
		return voucherMoney;
	}

	public void setVoucherMoney(BigDecimal voucherMoney) {
		setFormatVoucherMoney(StringUtils.formatMoney2Byte(voucherMoney));
		this.voucherMoney = voucherMoney;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public Double getExchangeVoucher() {
		return exchangeVoucher;
	}

	public void setExchangeVoucher(Double exchangeVoucher) {
		this.exchangeVoucher = exchangeVoucher;
	}

	public BigDecimal getBookOrderVoucher() {
		return bookOrderVoucher;
	}

	public void setBookOrderVoucher(BigDecimal bookOrderVoucher) {
		setFormatBookOrderVoucher(StringUtils.formatMoney2Byte(bookOrderVoucher));
		this.bookOrderVoucher = bookOrderVoucher;
	}

	public BigDecimal getDrawVoucher() {
		return drawVoucher;
	}

	public void setDrawVoucher(BigDecimal drawVoucher) {
		setFormatDrawVoucher(StringUtils.formatMoney2Byte(drawVoucher));
		this.drawVoucher = drawVoucher;
	}

	public BigDecimal getDrawProbability() {
		return drawProbability;
	}

	public void setDrawProbability(BigDecimal drawProbability) {
		this.drawProbability = drawProbability;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getFormatVoucherMoney() {
		return formatVoucherMoney;
	}

	public void setFormatVoucherMoney(String formatVoucherMoney) {
		this.formatVoucherMoney = formatVoucherMoney;
	}

	public String getFormatBookOrderVoucher() {
		return formatBookOrderVoucher;
	}

	public void setFormatBookOrderVoucher(String formatBookOrderVoucher) {
		this.formatBookOrderVoucher = formatBookOrderVoucher;
	}

	public String getFormatDrawVoucher() {
		return formatDrawVoucher;
	}

	public void setFormatDrawVoucher(String formatDrawVoucher) {
		this.formatDrawVoucher = formatDrawVoucher;
	}
}
