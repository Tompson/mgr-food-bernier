package com.deyi.model.vo;
/**
 * 广告图片及链接
 * @author limh Create on 2016年11月14日 下午3:46:45
 */
public class Ad {
	private String img;
	private String link;
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
