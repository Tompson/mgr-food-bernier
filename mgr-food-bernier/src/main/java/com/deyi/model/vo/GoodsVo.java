package com.deyi.model.vo;

import com.deyi.entity.Goods;
import com.deyi.util.StringUtils;

public class GoodsVo extends Goods{
	//菜品的平均评分
	private String averageScore;
	//当前星期数
	private String  currentWeek;

	public String getAverageScore() {
		return averageScore;
	}
	
	//掉用set方法格式化平均分
	public void setAverageScore(Double averageScore) {
		this.averageScore = StringUtils.formatScore(averageScore);
	}

	public String getCurrentWeek() {
		return currentWeek;
	}

	public void setCurrentWeek(String currentWeek) {
		this.currentWeek = currentWeek;
	}
}
