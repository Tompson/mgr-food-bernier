package com.deyi.model.vo;

import com.deyi.entity.ShopTrolley;
import com.deyi.util.StringUtils;

public class ShopTrolleyVo extends ShopTrolley{
	//菜品单价
	private Double unitPrice;
	//菜品名称
	private	String goodsName;
	//菜品图片的路径
	private String picUrl;
	
	private String formatPrice;
	
	private String formatUnitPrice;
	
	private String cgoodsName;
	//星期数
	private String weeknum;
	
	private Integer storeId;
	
	private Integer tableId;
	private String fromType;
	
	public String getFromType() {
		return fromType;
	}
	public void setFromType(String fromType) {
		this.fromType = fromType;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		setFormatUnitPrice(StringUtils.formatMoney2Byte(unitPrice));
		this.unitPrice = unitPrice;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getFormatPrice() {
		setFormatPrice();
		return formatPrice;
	}
	public void setFormatPrice() {
		this.formatPrice = getPrice()!=null?StringUtils.formatMoney2Byte(getPrice()):null;
	}
	public void setFormatPrice(String formatPrice) {
		this.formatPrice = formatPrice;
	}
	public String getFormatUnitPrice() {
		return formatUnitPrice;
	}
	public void setFormatUnitPrice(String formatUnitPrice) {
		this.formatUnitPrice = formatUnitPrice;
	}
	public String getCgoodsName() {
		return cgoodsName;
	}
	public void setCgoodsName(String cgoodsName) {
		this.cgoodsName = cgoodsName;
	}
	public String getWeeknum() {
		return weeknum;
	}
	public void setWeeknum(String weeknum) {
		this.weeknum = weeknum;
	}
	public Integer getStoreId() {
		return storeId;
	}
	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
	public Integer getTableId() {
		return tableId;
	}
	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}
	
}
