package com.deyi.model.vo;

import com.deyi.util.StringUtils;

public class WeeknumStatistics {
	//星期数
	private String weeknum;
	
	private Double amountPrice;
	//按星期分类的总价
	private String formatAmountPrice;
	public String getWeeknum() {
		return weeknum;
	}
	public void setWeeknum(String weeknum) {
		this.weeknum = weeknum;
	}
	public Double getAmountPrice() {
		return amountPrice;
	}
	public void setAmountPrice(Double amountPrice) {
		setFormatAmountPrice(amountPrice==null?"0.00":StringUtils.formatMoney2Byte(amountPrice));
		this.amountPrice = amountPrice;
	}
	public String getFormatAmountPrice() {
		return formatAmountPrice;
	}
	public void setFormatAmountPrice(String formatAmountPrice) {
		this.formatAmountPrice = formatAmountPrice;
	}
}
