package com.deyi.model.vo;

public class ShopCatVo {
	private double amountPrice;
	private Integer num;
	public double getAmountPrice() {
		return amountPrice;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public void setAmountPrice(double amountPrice) {
		this.amountPrice = amountPrice;
	}
	
	
}
