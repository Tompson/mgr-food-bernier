package com.deyi.model.vo;

import java.util.List;

import com.deyi.entity.OrderGoods;
import com.deyi.entity.Store;
import com.deyi.entity.SysOrder;
import com.deyi.util.BizHelper;
import com.deyi.util.StringUtils;
import com.deyi.util.TimeUtil;

public class SysOrderVo extends SysOrder {
	private String formatOrderTime;
	private String formatPayAmount;
	private String converedOrderStatus;
	private String formatOutsideMoney;
	private String formatTotalAmount;
	private String formatBookTime;
	private Long storeId;
	private String storeName;
	private String storePhone;
	private String storePhoto;
	private List<OrderGoods> orderGoodsList;
	private String formatdiscountMoney;
	
	
	public String getFormatdiscountMoney() {
		setFormatdiscountMoney();
		return formatdiscountMoney;
	}
	public void setFormatdiscountMoney() {
		 Double xx = getDiscountValue();
		this.formatdiscountMoney = xx!=null?StringUtils.formatMoney2Byte(xx):"0.00";
	}
	public String getFormatOrderTime() {
		setFormatOrderTime();
		return formatOrderTime;
	}
	public void setFormatOrderTime() {
		this.formatOrderTime=getOrderTime()!=null?TimeUtil.getSTD_DF_12Time(getOrderTime()):null;
	}
	public String getFormatPayAmount() {
		setFormatPayAmount();
		return formatPayAmount;
	}
	public void setFormatPayAmount() {
		this.formatPayAmount = getPayAccount()!=null?StringUtils.formatMoney2Byte(formatPayAmount):"0.00";
	}
	public String getConveredOrderStatus() {
		setConveredOrderStatus();
		return converedOrderStatus;
	}
	public void setConveredOrderStatus() {
		this.converedOrderStatus = BizHelper.converOrderStatus(getOrderStatus(),getPayStatus());
	}
	public void setFormatOrderTime(String formatOrderTime) {
		this.formatOrderTime = formatOrderTime;
	}
	public void setFormatPayAmount(String formatPayAmount) {
		this.formatPayAmount = formatPayAmount;
	}
	public void setConveredOrderStatus(String converedOrderStatus) {
		this.converedOrderStatus = converedOrderStatus;
	}
	public List<OrderGoods> getOrderGoodsList() {
		return orderGoodsList;
	}
	public void setOrderGoodsList(List<OrderGoods> orderGoodsList) {
		this.orderGoodsList = orderGoodsList;
	}
	public String getFormatOutsideMoney() {
		setFormatOutsideMoney();
		return formatOutsideMoney;
	}
	public void setFormatOutsideMoney(String formatOutsideMoney) {
		this.formatOutsideMoney = formatOutsideMoney;
	}
	public void setFormatOutsideMoney() {
		this.formatOutsideMoney = getOutsideMoney()!=null?StringUtils.formatMoney2Byte(getOutsideMoney()):"0.00";
	}
	public String getFormatTotalAmount() {
		setFormatTotalAmount();
		return formatTotalAmount;
	}
	public void setFormatTotalAmount(String formatTotalAmount) {
		this.formatTotalAmount = formatTotalAmount;
	}
	public void setFormatTotalAmount() {
		this.formatTotalAmount = getTotalAmount()!=null?StringUtils.formatMoney2Byte(getTotalAmount()):"0.00";
	}
	public String getFormatBookTime() {
		setFormatBookTime();
		return formatBookTime;
	}
	public void setFormatBookTime(String formatBookTime) {
		this.formatBookTime = formatBookTime;
	}
	public void setFormatBookTime() {
		this.formatBookTime = getBookTime()!=null?TimeUtil.getSTD_DF_12Time(getBookTime()):"";
	}
	public Long getStoreId() {
		return storeId;
	}
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getStorePhone() {
		return storePhone;
	}
	public void setStorePhone(String storePhone) {
		this.storePhone = storePhone;
	}
	public String getStorePhoto() {
		return storePhoto;
	}
	public void setStorePhoto(String storePhoto) {
		this.storePhoto = storePhoto;
	}
	
}
