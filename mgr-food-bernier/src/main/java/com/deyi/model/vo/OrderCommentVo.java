package com.deyi.model.vo;

import com.deyi.entity.OrderComment;
import com.deyi.util.TimeUtil;

public class OrderCommentVo extends OrderComment {
	//格式化评论时间
	private String formatSdate;
	
	//格式化回复时间
	private String formatReplyTime;

	public String getFormatSdate() {
		setFormatSdate();
		return formatSdate;
	}
	
	public void setFormatSdate(String formatSdate) {
		this.formatSdate = formatSdate;
	}

	public String getFormatReplyTime() {
		setFormatReplyTime();
		return formatReplyTime;
	}

	public void setFormatReplyTime(String formatReplyTime) {
		this.formatReplyTime = formatReplyTime;
	}
	
	public void setFormatSdate() {
		this.formatSdate =getSdate()!=null?TimeUtil.getSTD_DF_12Time(getSdate()):null;
	}
	
	public void setFormatReplyTime() {
		this.formatReplyTime =getReplytime()!=null?TimeUtil.getSTD_DF_12Time(getReplytime()):null;
	}
}
