package com.deyi.model;

import java.util.List;

public class WxMenu {

	List<WxMenuButton> button;

	public List<WxMenuButton> getButton() {
		return button;
	}

	public void setButton(List<WxMenuButton> button) {
		this.button = button;
	}
}
