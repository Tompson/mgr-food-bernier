/**
 * 
 */
package com.deyi.model;

import com.deyi.controller.pay.wxPay.WxMicroEntityRes;

/**
 * pure
 * @author hejx
 * @2016年3月23日
 */
public class ResponseTrade {

	/**
	 * 1.支付成功2.转入退款3未支付4已关闭5已撤销(刷卡支付)6用户支付中7支付失败(其他原因，如银行返回失败)
	 */
	private String errorCode;
	/**
	 * 结果描述
	 */
	private String errorMsg;
	//被扫 返回码图
	private WxUnitedOrderResponse wxUnifiedRes;
	
	
	private WxMicroEntityRes wxMicroEntityRes;
	
	
	
	/**
	 * @return the wxMicroEntityRes
	 */
	public WxMicroEntityRes getWxMicroEntityRes() {
		return wxMicroEntityRes;
	}
	/**
	 * @param wxMicroEntityRes the wxMicroEntityRes to set
	 */
	public void setWxMicroEntityRes(WxMicroEntityRes wxMicroEntityRes) {
		this.wxMicroEntityRes = wxMicroEntityRes;
	}
	/**
	 * @param wxUnifiedRes the wxUnifiedRes to set
	 */
	public void setWxUnifiedRes(WxUnitedOrderResponse wxUnifiedRes) {
		this.wxUnifiedRes = wxUnifiedRes;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	@Override
	public String toString() {
		return "ResponseTrade [errorCode=" + errorCode + ", errorMsg=" + errorMsg + "]";
	}

	public WxUnitedOrderResponse getWxUnifiedRes() {
		return wxUnifiedRes;
	}
}
