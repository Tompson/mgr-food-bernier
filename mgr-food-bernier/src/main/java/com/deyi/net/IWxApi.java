package com.deyi.net;

import com.deyi.model.JsApiTicket;
import com.deyi.model.WxErrorVo;
import com.deyi.model.WxMenu;
import com.deyi.model.WxMenuResult;
import com.deyi.model.WxOauth2Token;
import com.deyi.model.WxTempMsg2Keywords;
import com.deyi.model.WxTempMsg3Keywords;
import com.deyi.model.WxTemplateMsgRequest;
import com.deyi.model.WxTemplateMsgResponse;
import com.deyi.model.WxUserInfo;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;

public interface IWxApi {

	@POST("/cgi-bin/menu/create")
	WxErrorVo createMenu(@Header("encoding") String encoding, @Query("access_token") String accessToken, @Body WxMenu menu);

	@GET("/cgi-bin/menu/get")
	WxMenuResult getMenu(@Query("access_token") String accessToken);

	@GET("/cgi-bin/menu/delete")
	WxErrorVo deleteMenu(@Query("access_token") String accessToken);

	@GET("/sns/oauth2/access_token")
	WxOauth2Token getOauth2AccessToken(@Query("appid") String appid, @Query("secret") String secret,
			@Query("code") String code, @Query("grant_type") String grant_type);
	
	@GET("/cgi-bin/user/info")
	WxUserInfo getUserInfo(@Query("access_token") String accessToken, @Query("openid") String openid);
	@GET("/sns/userinfo")
	WxUserInfo AuthoritiedUserInfo(@Query("access_token")String accessToken, @Query("openid")String openid, @Query("lang") String lang);
	@POST("/cgi-bin/message/template/send")
	WxTemplateMsgResponse sendTemplateMsgForSubscribe(@Query("access_token") String accessToken, @Body WxTemplateMsgRequest<WxTempMsg3Keywords> request);
	@POST("/cgi-bin/message/template/send")
	WxTemplateMsgResponse sendTemplateMsgForSelfSubscribe(@Query("access_token") String accessToken, @Body WxTemplateMsgRequest<WxTempMsg2Keywords> request);

	@POST("/cgi-bin/ticket/getticket")
	JsApiTicket getJsApiTicket(@Query("access_token") String accessToken, @Query("type") String type);

}
