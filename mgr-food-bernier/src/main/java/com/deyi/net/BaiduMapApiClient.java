package com.deyi.net;

import java.util.Map;

import com.deyi.model.BaiDuResponse;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;

import retrofit.RestAdapter;

public class BaiduMapApiClient {

	private IBaiduMapApi getApi(){
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constants.BAIDU_MAP_API).build();
		IBaiduMapApi api = restAdapter.create(IBaiduMapApi.class);
		return api;
	}
	
	public Map<String, Object> geocoderRenderReverse(String location){
		String ak = BizHelper.getBaiduMapApiAk();
		return getApi().geocoderRenderReverse(ak, location, "json");
	}
	
	
	public BaiDuResponse getgeobyip(String ip){
		String ak = BizHelper.getBaiduMapApiAk();
		BaiDuResponse getAddress = getApi().GetAddress(
				ip, //ip "119.139.137.247"
				ak,//ak
				"mb",//mb：终端定位设备类型为移动设备   pc：终端定位设备类型为固定设备
				"bd09ll",//返回坐标类型	可选，bd09（默认）：百度墨卡托坐标  bd09ll：百度经纬度坐标	gcj02：国测局经纬度坐标 
				"utf-8",//编码
				"json" //返回的类型
				);
		
		return getAddress;
	}
	
}
