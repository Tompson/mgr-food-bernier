package com.deyi.net;

import java.util.Map;

import com.deyi.model.BaiDuResponse;

import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface IBaiduMapApi {
	/**
	 * 编码补偿转换
	 * @author limh Create on 2016年10月27日 下午9:52:13
	 * @param ak 
	 * @param location
	 * @param output
	 * @return
	 */
	@POST("/geocoder/v2/")
	Map<String, Object> geocoderRenderReverse(@Query("ak") String ak, @Query("location") String location, @Query("output") String output);
	
	@GET("/highacciploc/v1")
	BaiDuResponse GetAddress(
			@Query("qcip") String qcip, 
			@Query("ak") String ak, 
			@Query("qterm") String qterm, 
			@Query("coord") String coord, 
			@Query("coding") String coding, 
			@Query("callback_type") String callback_type);

}
