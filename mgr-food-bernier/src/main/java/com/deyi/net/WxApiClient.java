package com.deyi.net;

import com.deyi.model.JsApiTicket;
import com.deyi.model.WxErrorVo;
import com.deyi.model.WxMenu;
import com.deyi.model.WxMenuResult;
import com.deyi.model.WxOauth2Token;
import com.deyi.model.WxTempMsg2Keywords;
import com.deyi.model.WxTempMsg3Keywords;
import com.deyi.model.WxTemplateMsgRequest;
import com.deyi.model.WxTemplateMsgResponse;
import com.deyi.model.WxUserInfo;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;

import retrofit.RestAdapter;

public class WxApiClient {
	
	private IWxApi getApi(){
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constants.WX_API_URL).build();
		IWxApi api = restAdapter.create(IWxApi.class);
		return api;
	}
	
	public WxErrorVo createMenu(String accessToken, WxMenu menu){
		return getApi().createMenu("utf-8", accessToken, menu);
	}
	
	public WxMenuResult getMenu(String accessToken){
		return getApi().getMenu(accessToken);
	}
	
	public WxErrorVo deleteMenu(String accessToken){
		return getApi().deleteMenu(accessToken);
	}
	
	public WxOauth2Token getOauth2AccessToken(String code){
		return getApi().getOauth2AccessToken(BizHelper.getWxAppId(), BizHelper.getWxAppSecret(), code, "authorization_code");
	}
	
	public WxUserInfo getAuthoritiedUserInfo(String accessToken, String openid){
		return getApi().AuthoritiedUserInfo(accessToken,openid,"zh_CN");
	}
	

	public WxUserInfo getUserInfo(String accessToken, String openid){
		return getApi().getUserInfo(accessToken, openid);
	}
	public WxTemplateMsgResponse sendTemplateMsgForSubscribe(String accessToken, WxTemplateMsgRequest<WxTempMsg3Keywords> requestVo){
		return getApi().sendTemplateMsgForSubscribe(accessToken, requestVo);
	}
	
	public WxTemplateMsgResponse sendTemplateMsgForSelfSubscribe(String accessToken, WxTemplateMsgRequest<WxTempMsg2Keywords> requestVo){
		return getApi().sendTemplateMsgForSelfSubscribe(accessToken, requestVo);
	}
	
	public JsApiTicket getJsApiTicket(String accessToken, String type){
		return getApi().getJsApiTicket(accessToken, type);
	}
}
