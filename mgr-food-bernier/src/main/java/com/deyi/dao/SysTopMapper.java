package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysTop;
@Repository
public interface SysTopMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysTop record);

    int insertSelective(SysTop record);

    SysTop selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysTop record);

    int updateByPrimaryKey(SysTop record);
}