package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.GroupPurchase;
import com.deyi.util.Page;

@Repository
public interface GroupPurchaseMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GroupPurchase record);

    int insertSelective(GroupPurchase record);

    GroupPurchase selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GroupPurchase record);

    int updateByPrimaryKey(GroupPurchase record);
    /**
     * 分页,并且可以根据会员名称和联系人模糊查询
     * @Author zjz Create on 2016年11月28日 上午11:17:55
     * @param page
     * @return
     */
    List<GroupPurchase> queryPage(Page<GroupPurchase> page);

}