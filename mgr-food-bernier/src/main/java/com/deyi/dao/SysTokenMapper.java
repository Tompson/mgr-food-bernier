package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysToken;
@Repository
public interface SysTokenMapper {
    int deleteByPrimaryKey(String phone);

    int insert(SysToken record);

    int insertSelective(SysToken record);

    SysToken selectByPrimaryKey(String phone);

    int updateByPrimaryKeySelective(SysToken record);

    int updateByPrimaryKey(SysToken record);
}