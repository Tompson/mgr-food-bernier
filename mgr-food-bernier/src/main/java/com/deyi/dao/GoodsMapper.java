package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.Goods;
import com.deyi.entity.StoreGoods;
import com.deyi.model.vo.GoodsVo;
import com.deyi.util.Page;
@Repository
public interface GoodsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Long id);
    
    StoreGoods selectByPrimaryStoreId(@Param("storeId")Integer storeId,@Param("goodsId")Long goodsId);
    int updateByPrimaryKeySelective(Goods record);
    
    int updateByPrimaryKeyStoreIdGoodsId(@Param("sort")Long sort,@Param("storeId")Integer storeId,@Param("goodsId")Long goodsId);

    int updateByPrimaryKey(Goods record);
    
    int updateByPrimaryByStoreIdAndGoodsId(@Param("storeId")Integer storeId,@Param("goodsId")Long goodsId,@Param("status")String status);
    
    String selectByPrimaryByStoreIdAndGoodsId(@Param("storeId")Integer storeId,@Param("goodsId")Long goodsId);
    String selectByPrimaryByStoreIdAndGoodsIdSort(@Param("storeId")Integer storeId,@Param("goodsId")Long goodsId);
    
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<Goods> queryPage(Page<Goods> page);
	
	
	/**
	 * 关系分类表，查出带分类信息的菜品
	 * @author limh Create on 2016年10月23日 下午6:28:37
	 * @param id 菜品ID
	 * @return Goods
	 */
	Goods selectGoodsDetailByPrimaryKey(Long id);
	/**
	 * 分页查询菜品
	 * 菜品分类
	 * 统计菜品平均评分
	 * @author limh Create on 2016年10月24日 上午11:36:30
	 * @param page 
	 * params.goodsName  非空则按菜品名模糊查询
	 * params.weeksFoodStatus 非空则按星期数查询
	 * params.categoryId  非空则按分类id查询
	 * params.goodsId 非空则按菜品id查询
	 * params.status  非空则按菜品状态查询
	 * @return List<GoodsVo> 
	 */
	List<GoodsVo> selectAvgScore4Goods(Page<GoodsVo> page);
	/**
	 * 交易商品总数量
	 * @Author zjz Create on 2016年10月26日 下午3:31:27
	 * @return
	 */
	Integer StatDealGoodsSum(@Param("storeId")Long storeId);
	/**
	 * 分页 菜品销售统计
	 * @Author zjz Create on 2016年10月26日 下午7:58:06
	 * @param page
	 * @return
	 */
	List<Goods>  queryPageStatGoodsAvgComment(Page<Goods> page);
	/**
	 * 根据菜id查询菜的评价(对订单的评价就是对订单里的菜的评价)
	 * @Author zjz Create on 2016年11月18日 下午1:45:19
	 */
	Goods selectGoodsCommentBygoodsId(Long goodsId);
	
	/*排序功能 */
	/**
	 * 
	 * @Title: getTopRecord 
	 * @Description: 获取置顶的记录
	 * @return
	 * Goods
	 * @throws
	 */
	StoreGoods getTopRecord(@Param("storeId")Integer storeId);
	
	/**
	 * 
	 * @Title: switchSort 
	 * @Description: 更新交互排序号，适用于上下移
	 * @param curentId
	 * @param compareId
	 * @return
	 * int
	 * @throws
	 */
	int switchSort(@Param("curentId")Long curentId,@Param("compareId")Long compareId);
	
	/**
	 * 
	 * @Title: getDownRecord 
	 * @Description: 获取排序号下一条记录
	 * @param goodsId
	 * @return
	 * Goods
	 * @throws
	 */
	Goods getDownRecord(Long goodsId);
	
	/**
	 * 
	 * @Title: getUpRecord 
	 * @Description: 获取排序号上一条记录
	 * @param goodsId 
	 * @return
	 * Goods
	 * @throws
	 */
	StoreGoods getUpRecord(@Param("storeId")Integer storeId,@Param("goodsId")Long goodsId);
	
	/**
	 * 
	 * @Title: getMaxSort 
	 * @Description: 获取最大排序号，用于新增菜品是插入使用
	 * @return
	 * long
	 * @throws
	 */
	long getMaxSort();

	List<Goods> queryAll(Long cId);

	List<Goods> selectGoodsListByCategoryIdAndStoreId(@Param("sId")Integer storeId, @Param("cId")Long categoryId);

	StoreGoods selectStoreGoodsByGoodsIdAndStoreId(@Param("sId")Integer storeId, @Param("cId")Long categoryId);
    
	/*排序功能 */
	long getMaxSortByStore(@Param("storeId")Integer storeId);
	
	 /**
	  * 
	  * @Title: queryPage 
	  * @Description: 门店菜品分页
	  * @param @param page
	  * @param @return    设定文件 
	  * @return List<Goods>    返回类型 
	  * @throws
	  */
	List<Goods> queryPageByStore(Page<Goods> page);
	/**
	 * 
	 * @Title: selectGoodsDetailBySgId 
	 * @Description: 查询门店菜品详情
	 * @param sgid
	 * @return
	 * Goods
	 * @throws
	 */
	Goods selectGoodsDetailBySgId(@Param("sgid")Long sgid);
	List<Goods>  selectByPrimaryCategoryId(Long id);
	
}