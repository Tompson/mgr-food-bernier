package com.deyi.dao;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.DeliverMan;
import com.deyi.entity.SysOrder;
import com.deyi.model.vo.SysOrderVo;
import com.deyi.util.Page;
@Repository
public interface SysOrderMapper {
	int deleteByPrimaryKey(Long orderId);

    int insert(SysOrder record);

    int insertSelective(SysOrder record);

    SysOrder selectByPrimaryKey(Long orderId);

    int updateByPrimaryKeySelective(SysOrder record);

    int updateByPrimaryKey(SysOrder record);
    /**
     * 订单列表分页查询
     * @author limh Create on 2016年10月29日 下午5:55:31
     * @param page
     * params.memberId 会员ID 必传
     * params.orderId 订单iD,非空则会增加对orderid的查询
     * @return
     */
    List<SysOrderVo> selectByPage(Page<SysOrderVo> page);
    
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<SysOrder> queryPage(Page<SysOrder> page);
	/**
	 * 更改订单状态为(已确认) 
	 * @Author zjz Create on 2016年10月25日 下午7:54:55
	 * @param sysOrder  只需传个orderId
	 * @return
	 */
	int updateOrderStatus2(SysOrder sysOrder);
	/**
	 * 根据订单id查询订单信息   返回 SysOrder对象
	 * @Author zjz Create on 2016年10月25日 下午7:56:55
	 * @param orderId
	 * @return
	 */
	SysOrder queryOrderById2(Long orderId);
	
	
	/**
	 * 统计交易笔数
	 * @Author zjz Create on 2016年10月26日 下午2:13:09
	 * @return
	 */
	int statDealNum(@Param("storeId")Long storeId);
	/**
	 * 交易总额
	 */
	BigDecimal statDealSumAmount(@Param("storeId")Long storeId);
	/**
	 * 昨日交易笔数
	 */
	int statDealNumYesterday(@Param("storeId")Long storeId);
	/**
	 * 昨日交易金额
	 * @Author zjz Create on 2016年10月26日 下午2:19:18
	 * @return
	 */
	BigDecimal statDealSumAmountYesterday(@Param("storeId")Long storeId);
	/**
	 * 根据订单流水号查询订单
	 * @author limh Create on 2016年10月26日 下午8:29:42
	 * @param payno
	 */
	SysOrder selectByPayNumber(String payno);
	/**
	 * 统计订单状态为未处理 
	 * @Author zjz Create on 2016年11月24日 上午10:09:24
	 * @return int类型,没有就返回0
	 */
	int statOrderStatusEqUntreated();
	
	/**
	 * 分页+配送统计
	 * @Author zjz Create on 2016年10月25日 上午11:32:54
	 * @param page
	 * @return
	 */
	List<SysOrder> storeStat(Page<SysOrder> page);

	int selectTotalNum(Integer memberId);
	/**
	 * 
	 * @Title: statOrderNumber 
	 * @Description:  统计订单数  和销售额 
	 * @param @param querystarttime
	 * @param @param queryendtime
	 * @param @return    设定文件 
	 * @return List<SysOrder>    返回类型 
	 * @throws
	 */
	List<SysOrder> statOrderNumber(@Param("querystarttime")Date querystarttime, @Param("queryendtime")Date queryendtime,@Param("storeId")Long storeId);
}