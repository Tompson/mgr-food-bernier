package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.WxAccessToken;

@Repository
public interface WxAccessTokenMapper {
    int deleteByPrimaryKey(String id);

    int insert(WxAccessToken record);

    int insertSelective(WxAccessToken record);

    WxAccessToken selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(WxAccessToken record);

    int updateByPrimaryKey(WxAccessToken record);
    
    List<WxAccessToken> selectAll();
}