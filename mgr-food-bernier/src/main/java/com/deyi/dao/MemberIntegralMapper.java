package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.MemberIntegral;
@Repository
public interface MemberIntegralMapper {
    int deleteByPrimaryKey(Integer id);
    Integer deleteByMemberId(Integer id);

    int insert(MemberIntegral record);

    int insertSelective(MemberIntegral record);

    MemberIntegral selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MemberIntegral record);

    int updateByPrimaryKey(MemberIntegral record);
    
    MemberIntegral selectByMemberId(Integer memberId);
    /**
     * 查询当天分享的积分
     * @author limh Create on 2016年11月3日 下午9:39:55
     * @param memberId 会员id
     * @return List<MemberIntegral>
     */
    List<MemberIntegral> selectCurrentDayShareByMemberId(Integer memberId);
}