package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysParm;
@Repository
public interface SysParmMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysParm record);

    int insertSelective(SysParm record);

    SysParm selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysParm record);

    int updateByPrimaryKey(SysParm record);
    
    List<SysParm> selectAll();
}