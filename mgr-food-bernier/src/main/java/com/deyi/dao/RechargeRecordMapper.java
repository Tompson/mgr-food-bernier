package com.deyi.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.RechargeRecord;
import com.deyi.entity.SysOrder;

@Repository
public interface RechargeRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RechargeRecord record);

    int insertSelective(RechargeRecord record);

    RechargeRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RechargeRecord record);

    int updateByPrimaryKey(RechargeRecord record);
    /**
	 * 
	 * @Title: statOrderNumber 
	 * @Description:  统计充值金额
	 * @param @param querystarttime
	 * @param @param queryendtime
	 * @param @return    设定文件 
	 * @return List<SysOrder>    返回类型 
	 * @throws
	 */
	List<RechargeRecord> statRechargeNumber(@Param("querystarttime")Date querystarttime, @Param("queryendtime")Date queryendtime);

	List<RechargeRecord> selectBySn(@Param("sn")String out_trade_no);
}