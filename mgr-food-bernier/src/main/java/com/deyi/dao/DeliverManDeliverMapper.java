package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.DeliverManDeliver;
@Repository
public interface DeliverManDeliverMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DeliverManDeliver record);

    int insertSelective(DeliverManDeliver record);

    DeliverManDeliver selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DeliverManDeliver record);

    int updateByPrimaryKey(DeliverManDeliver record);
}