package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.CarouselFigure;
import com.deyi.entity.CategoryGoods;
import com.deyi.util.Page;
@Repository
public interface CarouselFigureDao {
	int deleteByPrimaryKey(Integer id);

    int insert(CarouselFigure record);

    int insertSelective(CarouselFigure record);

    CarouselFigure selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CarouselFigure record);

    int updateByPrimaryKey(CarouselFigure record);
    /**
     * 分页
     */
	List<CarouselFigure> queryPage(Page<CarouselFigure> page);

	CarouselFigure getTopRecord();

	CarouselFigure getUpRecord(@Param("id")long id);

	int switchSort(@Param("curentId")Long curentId,@Param("compareId")Long compareId);

	CarouselFigure getDownRecord(@Param("id")long id);

	List<CarouselFigure> selectTopFive();
	
	/**
	 * 
	 * @Title: getMaxSort
	 * @Description: 查询最大的sort
	 * @param @param storeId
	 * @param @return    设定文件 
	 * @return long    返回类型 
	 * @throws
	 */
	long getMaxSort();


}