package com.deyi.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.deyi.util.StringUtils;

/**
 * 
 * @author zjz Create on 2016年11月25日 上午11:01:36
 */
public class MapperCreater {
	static String basepath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main"
			+ File.separator + "java" + File.separator;
	static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	static DocumentBuilder builder = null;

	public static void writeXML(Document document, String filename) {
		try {
			builder = factory.newDocumentBuilder();
			document.normalize();

			/** 将document中的内容写入文件中 */
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			// 编码
			DOMSource source = new DOMSource(document);
			PrintWriter pw = new PrintWriter(new FileOutputStream(filename));
			StreamResult result = new StreamResult(pw);
			transformer.transform(source, result);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void updateXML(String mapperPackageUrl) {
		try {

			String filepath = mapperPackageUrl.replace(".", File.separator);
			System.out.println(basepath + filepath);
			File file = new File(basepath + filepath);
			if (!file.exists()) {
				file.mkdirs();
			}
			System.out.println(file.getPath());
			File[] tempList = file.listFiles();
			System.out.println("该目录下对象个数：" + tempList.length);
			for (int i = 0; i < tempList.length; i++) {
				System.out.println("第"+(i+1)+"个");
				if (tempList[i].isFile()) {
					File file2 = tempList[i];
					builder = factory.newDocumentBuilder();
					Document document = builder.parse(file2);
					Node rootElement = document.getDocumentElement();

					// 获取具体标签的值
					NodeList nodeList = rootElement.getChildNodes();

					for (int j = 0; j < nodeList.getLength(); j++) {
						Node node = nodeList.item(j);
						if ("resultMap".equals(node.getNodeName())
								&& "BaseResultMap".equals(node.getAttributes().getNamedItem("id").getNodeValue())) {
							
							Element select = document.createElement("select");
							select.setAttribute("id", "selectBySelective");
							select.setAttribute("parameterType", node.getAttributes().getNamedItem("type").getNodeValue());
							select.setAttribute("resultMap", "BaseResultMap");
							
							String tablename = node.getAttributes().getNamedItem("type").getNodeValue();
							String[] split = tablename.split("\\.");
							tablename = tablename.split("\\.")[split.length-1];
							tablename = StringUtils.entityName2TableName(tablename);
							Text selectTextNode1 = document.createTextNode("\n	SELECT ");
							Text selectTextNode2 = document.createTextNode("\n   FROM "+tablename+" "+tablename.charAt(0)+" \n");
							Element include = document.createElement("include");
							include.setAttribute("refid", "Base_Column_List");
							select.appendChild(selectTextNode1);
							select.appendChild(include);
							select.appendChild(selectTextNode2);
							
							Element where = document.createElement("where");
							Text text1 = document.createTextNode("\n 1=1 \n");
							where.appendChild(text1);
							NodeList propertys = node.getChildNodes();
							for(int m = 0; m < propertys.getLength(); m++){
								Node item = propertys.item(m);
								if("id".equals(item.getNodeName()) || "result".equals(item.getNodeName())){
									String column = item.getAttributes().getNamedItem("column").getNodeValue();
									String jdbcType = item.getAttributes().getNamedItem("jdbcType").getNodeValue();
									String property = item.getAttributes().getNamedItem("property").getNodeValue();
									Element ifnode = document.createElement("if");
									ifnode.setAttribute("test", property+" != null");
									Text iftext = document.createTextNode(""
											+ "\n	AND "+tablename.charAt(0)+"."+ column +" = #{"+property+",jdbcType="+jdbcType+"} \n");
									ifnode.appendChild(iftext);
									where.appendChild(ifnode);
									
								}
							}
							select.appendChild(where);
							
							document.getDocumentElement().appendChild(select);
							System.out.println("输出路径" + file2.getAbsolutePath());
							writeXML(document, file2.getAbsolutePath());
							break;
						}

					}
//					break;

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		updateXML("com.deyi.dao.mapper");
	}
}
