package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysStoreGoods;

@Repository
public interface SysStoreGoodsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysStoreGoods record);

    int insertSelective(SysStoreGoods record);

    SysStoreGoods selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysStoreGoods record);

    int updateByPrimaryKey(SysStoreGoods record);
    
    
}