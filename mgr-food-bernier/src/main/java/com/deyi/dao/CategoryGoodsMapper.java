package com.deyi.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.CategoryGoods;
import com.deyi.util.Page;
@Repository
public interface CategoryGoodsMapper {
    int deleteByPrimaryKey(Long categoryId);

    int insert(CategoryGoods record);

    int insertSelective(CategoryGoods record);

    CategoryGoods selectByPrimaryKey(Long categoryId);

    int updateByPrimaryKeySelective(CategoryGoods record);

    int updateByPrimaryKey(CategoryGoods record);
    
    /**
     * 查全部分类
     * @Author zjz Create on 2016年10月22日 下午12:26:14
     * @return
     */
    List<CategoryGoods> queryAll();

	List<CategoryGoods> queryPage(Page<CategoryGoods> page);

	List<CategoryGoods> getCategoryByCategoryName(String cgoodsName);

	CategoryGoods getUpRecord(@Param("id")long categoryId);
	CategoryGoods getDownRecord(@Param("id")long categoryId);
	int switchSort(@Param("curentId")Long curentId,@Param("compareId")Long compareId);

	CategoryGoods getTopRecord();

	List<CategoryGoods> getCategoryListByStoreId(@Param("id")Integer id );
	/**
	 * 
	 * @Title: getMaxSort
	 * @Description: 查询最大的sort
	 * @param @param storeId
	 * @param @return    设定文件 
	 * @return long    返回类型 
	 * @throws
	 */
	long getMaxSort();

}