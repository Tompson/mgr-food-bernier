package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.OrderComment;
import com.deyi.model.vo.OrderCommentVo;
import com.deyi.util.Page;
@Repository
public interface OrderCommentMapper {
    int deleteByPrimaryKey(Long cid);

    int insert(OrderComment record);

    int insertSelective(OrderComment record);

    OrderComment selectByPrimaryKey(Long cid);

    int updateByPrimaryKeySelective(OrderComment record);

    int updateByPrimaryKey(OrderComment record);
    
    /**
	 * 查询跟某个菜有关联的订单的所有评论
	 * @author limh Create on 2016年10月24日 下午3:09:18
	 * @param page
	 * params.goodsId 菜品id，必传
	 * @return List<OrderComment>
	 */
	List<OrderCommentVo> selectGoodsCommemtByGoodsId(Page<OrderCommentVo> page);
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<OrderComment> queryPage(Page<OrderComment> page);
	/**
	 *  配送员评论分页
	 * @Author zjz Create on 2016年11月24日 下午9:08:36
	 * @param page
	 * @return
	 */
	List<OrderComment> queryPageByDeliver(Page<OrderComment> page);
	
	//取到排序是第一个的订单评论
	OrderComment sortingTop();
	//当前点击的订单评论的:上一个订单评论
	OrderComment moveOrderComment(Long parentId);
	//倒叙排取到排序是第一个的订单评论
	OrderComment sortingTop2();
	//当前点击的订单评论的:上一个订单评论
	OrderComment moveOrderComment2(Long parentId);
	//取到排序是第一个的订单评论
	OrderComment sortingTop3();
	//当前点击的订单评论的:上一个订单评论
	OrderComment moveOrderComment3(Long parentId);
	
	/**
	 * 检查数据库是否存在该订单的评论
	 * @author limh Create on 2016年11月17日 上午11:45:12
	 * @param orderId
	 * @return 该订单的评论统计 如果小于1，则可执行插入操作
	 */
	int checkOrderCommentIsUniqueness(Long orderId);
	/**
	 * 查询replynum字段的最大值
	 * @author limh Create on 2016年11月21日 上午11:59:02
	 * @return
	 */
	long selectMaxReplynum();
	/**
	 * 查询replynum字段的最小值
	 * @author limh Create on 2016年11月21日 上午11:59:02
	 * @return
	 */
	long selectMinReplynum();
}