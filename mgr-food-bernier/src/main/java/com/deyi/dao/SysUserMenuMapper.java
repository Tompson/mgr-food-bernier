package com.deyi.dao;

import com.deyi.entity.SysUserMenu;

public interface SysUserMenuMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUserMenu record);

    int insertSelective(SysUserMenu record);

    SysUserMenu selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUserMenu record);

    int updateByPrimaryKey(SysUserMenu record);
}