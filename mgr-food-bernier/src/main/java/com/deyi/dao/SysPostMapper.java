package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysPost;
@Repository
public interface SysPostMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysPost record);

    int insertSelective(SysPost record);

    SysPost selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysPost record);

    int updateByPrimaryKey(SysPost record);
}