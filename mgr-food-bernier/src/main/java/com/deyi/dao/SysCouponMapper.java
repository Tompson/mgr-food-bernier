package com.deyi.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.SysCoupon;
import com.deyi.util.Page;
@Repository
public interface SysCouponMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysCoupon record);

    int insertSelective(SysCoupon record);

    SysCoupon selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysCoupon record);

    int updateByPrimaryKey(SysCoupon record);
    
    int startUpdateSysCouponTime(@Param("time")Date time);
    List<SysCoupon> querySysCouponPage(Page<SysCoupon> page);//积分兑换代金券分页
}