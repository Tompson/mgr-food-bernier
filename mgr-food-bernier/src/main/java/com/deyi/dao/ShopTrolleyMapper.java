package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.ShopTrolley;
import com.deyi.model.ConditionModel;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.model.vo.WeeknumStatistics;
@Repository
public interface ShopTrolleyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShopTrolley record);

    int insertSelective(ShopTrolley record);

    ShopTrolley selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShopTrolley record);

    int updateByPrimaryKey(ShopTrolley record);
    
    List<ShopTrolleyVo> selectByMemberId(Integer memberId);
    
    double amountPriceByMemberId(Integer memberId);
    
    int deleteByMemberId(Integer memberId);
    /**
     * 查询购物车中是否存在某个菜品
     * @author limh Create on 2016年10月24日 下午8:09:06
     * @param record
     * memberId和goodsId必传
     * @return ShopTrolley
     */
    ShopTrolley checkGoodsIsExist(ShopTrolley record);
    /**
     * 统计会员某星期下所有菜品总价
     * @author limh Create on 2016年11月25日 下午5:51:57
     * @param condition
     * @param params.memberId 会员id 必传
     * @param params.weeknum 星期数 必传
     * @return 会员某星期下所有菜品总价
     */
    double amountPriceByWeeknum(ConditionModel condition);
    /**
     * 分类统计菜品价格
     * @author limh Create on 2016年11月26日 上午11:57:04
     * @param memberId
     * @return
     */
    List<WeeknumStatistics> amountPriceByMemberIdCategory(Integer memberId);
    /**
     * 按星期数分类删除
     * @author limh Create on 2016年11月26日 下午4:00:02
     * @param condition
     * @param params.memberId 会员id 必传
     * @param params.weeknum 星期数 必传
     * @return
     */
    int deleteByMemberIdCategory(ConditionModel condition);
    /**
     * 按星期数查询
     * @author limh Create on 2016年11月30日 下午4:08:42
     * @param condition
     * @param params.memberId 会员id 必传
     * @param params.weeknum 星期数 必传
     * @return 
     */
    List<ShopTrolleyVo> selectByCondition(ConditionModel condition);

	List<ShopTrolleyVo> selectByMemberIdAndStoreId(@Param("memberId")Integer memberId, @Param("storeId")Integer storeId);

	ShopCatVo amountPriceAndNumByWeeknum(ConditionModel condition);

	List<Integer> getweekSByMemberIdAndStoreId(@Param("memberId")Integer memberId, @Param("storeId")Integer sId);

	WeeknumStatistics getWeeknumStatistic(@Param("week")Integer w, @Param("memberId")Integer memberId, @Param("storeId")Integer storeId);

	double amountPriceByWeeknumAndStoreId(ConditionModel condition);

	int deleteByMemberIdAndStoreId(@Param("memberId")Integer memberId, @Param("storeId")Integer sId);

	List<ShopTrolleyVo> selectByMemberIdAndStoreIdAndWeek(@Param("memberId")Integer memberId, @Param("storeId") Integer storeId,  @Param("week")Integer week);

	ShopTrolley selectByMemberIdAndStoreGoodsIdAndWeek(@Param("memberId")Integer memberId, @Param("sgId") Integer sgId,  @Param("week")Integer week);
}