package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.DeliverManLocation;
@Repository
public interface DeliverManLocationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DeliverManLocation record);

    int insertSelective(DeliverManLocation record);

    DeliverManLocation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DeliverManLocation record);

    int updateByPrimaryKey(DeliverManLocation record);
}