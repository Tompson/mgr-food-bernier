package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.CouponDetails;
@Repository
public interface CouponDetailsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CouponDetails record);

    int insertSelective(CouponDetails record);

    CouponDetails selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CouponDetails record);

    int updateByPrimaryKey(CouponDetails record);
}