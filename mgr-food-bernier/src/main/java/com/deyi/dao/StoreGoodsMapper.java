package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface StoreGoodsMapper {

	List<String> getGoodsIdByStoreId(Long id, Long cId);
	//根据菜品  所有门店下面该菜品信息
	int delGoodsByStoreId(Long cId);
}
