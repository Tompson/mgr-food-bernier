package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.Store;
import com.deyi.entity.StoreCategory;
import com.deyi.entity.StoreGoods;
import com.deyi.util.Page;

@Repository("storeDao")
public interface StoreDao {

	List<Store> queryPage(Page<Store> page);

	Store selectByPrimaryKey(@Param("id")Integer integer);

	void updateByPrimaryKeySelective(Store store);

	Store selectStoreByAccount(@Param("account")String account);

	void sava(Store store);

	void deleteStoreById(@Param("id") int id);

	List<String> getCategoryListByStoreId(int id);

	List<String> getGoodsIdByStoreId(@Param("id")Long id,@Param("cId") Long cId);

	void deleteStoreCategorysByStoreId(@Param("id")String id);

	void deleteGoodsStoreByStoreId(@Param("id")String id);

	void insertStoreCategory(StoreCategory sg);

	void insertStoreGoods(StoreGoods sg);

	List<Store> queryAll();

	List<Store> selectStoresByUserLocation(@Param("lat")double lat,@Param("lng") double lng);

	List<Store> wxgetStoresList(Page<Store> page);

}
