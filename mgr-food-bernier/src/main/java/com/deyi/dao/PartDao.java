package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


import com.deyi.entity.Part;
import com.deyi.util.Page;

@Repository("partDao")
public interface PartDao {

	List<Part> queryPage(Page<Part> page);
	int deleteByPrimaryKey(Integer id);

    int insert(Part record);

    int insertSelective(Part record);

    Part selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Part record);

    int updateByPrimaryKey(Part record);
	List<Part> wxgetPartsByStoreId(@Param("id")Integer id);
	List<Part> wxgetFullPartsByStoreId(Integer id);
	List<Part> wxgetDiscountPartsByStoreId(Integer id);
   
	
}
