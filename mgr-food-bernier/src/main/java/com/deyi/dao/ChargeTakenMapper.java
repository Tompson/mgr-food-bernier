package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.ChargeTaken;
@Repository
public interface ChargeTakenMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChargeTaken record);

    int insertSelective(ChargeTaken record);

    ChargeTaken selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChargeTaken record);

    int updateByPrimaryKey(ChargeTaken record);
}