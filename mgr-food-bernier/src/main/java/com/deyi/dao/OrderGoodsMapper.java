package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.OrderGoods;
@Repository
public interface OrderGoodsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderGoods record);

    int insertSelective(OrderGoods record);

    OrderGoods selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderGoods record);

    int updateByPrimaryKey(OrderGoods record);
    /**
     * 本月销售数量排行前10商品
     * @Author zjz Create on 2016年10月26日 下午2:38:07
     * @return
     */
    List<OrderGoods> StatQuantifySum(@Param("storeId")Long storeId);
    /**
     * 本月销售金额排行前10商品
     * @Author zjz Create on 2016年10月26日 下午2:38:38
     * @return
     */
    List<OrderGoods> StatPricePaySum(@Param("storeId")Long storeId);
    /**
     * 昨日交易商品总数量
     * @Author zjz Create on 2016年10月26日 下午3:28:42
     * @return
     */
    Integer StatOrderGoodsSum(@Param("storeId")Long storeId);
    /**
     * 查询一笔订单中所有的菜品信息
     * @author limh Create on 2016年10月29日 上午9:53:13
     * @param orderId
     * @return
     */
    List<OrderGoods> selectByOrderId(Long orderId);
}