package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.MemberAddress;
import com.deyi.util.Page;
@Repository
public interface MemberAddressMapper {
    int deleteByPrimaryKey(Integer addressId);
    Integer deleteByPrimaryMemberId(Integer MemberId);

    int insert(MemberAddress record);

    int insertSelective(MemberAddress record);

    MemberAddress selectByPrimaryKey(Integer addressId);

    int updateByPrimaryKeySelective(MemberAddress record);

    int updateByPrimaryKey(MemberAddress record);
    /**
     * 分页查询用户设置的地址
     * @author limh Create on 2016年10月25日 上午10:54:50
     * @param page
     * params.memberId 会员id，必传
     * @return List<MemberAddress>
     */
    List<MemberAddress> selectByPage(Page<MemberAddress> page);
    /**
     * 根据会员id更新地址默认状态
     * @author limh Create on 2016年10月25日 下午4:05:29
     * @param record 
     * memberId 会员id 必传
     * status 地址状态 必传
     */
    int updateStatusByMemberId(MemberAddress record);
    /**
     * 查询默认地址
     * @author limh Create on 2016年10月25日 下午6:18:41
     * @param memberId 会员id
     * @return List<MemberAddress>
     */
    List<MemberAddress> selectDefaultAddressByMemberId(Integer memberId);
}