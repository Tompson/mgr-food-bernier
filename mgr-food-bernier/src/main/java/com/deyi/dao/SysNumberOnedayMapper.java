package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysNumberOneday;

@Repository
public interface SysNumberOnedayMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysNumberOneday record);

    int insertSelective(SysNumberOneday record);

    SysNumberOneday selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysNumberOneday record);

    int updateByPrimaryKey(SysNumberOneday record);
    
    /**根据key 查询对应的时间最大的记录*/
	SysNumberOneday selectByNumberKey(String key);
	
	/**查询每日对应数据*/
	SysNumberOneday selectForKey(SysNumberOneday sysNumberOneday);
}