package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.ChiefComment;
import com.deyi.entity.ChiefCommentWithBLOBs;
import com.deyi.util.Page;
@Repository
public interface ChiefCommentMapper {
    int deleteByPrimaryKey(Long cid);

    int insert(ChiefComment record);

    int insertSelective(ChiefComment record);

    ChiefCommentWithBLOBs selectByPrimaryKey(Long cid);

    int updateByPrimaryKeySelective(ChiefComment record);

    int updateByPrimaryKeyWithBLOBs(ChiefComment record);

    int updateByPrimaryKey(ChiefComment record);
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<ChiefComment> queryPage(Page<ChiefComment> page);
}