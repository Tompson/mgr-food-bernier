package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.MemberLevel;
import com.deyi.util.Page;
@Repository
public interface MemberLevelDao {
    int deleteByPrimaryKey(Integer id);
    int insertSelective(MemberLevel record);

    MemberLevel selectByPrimaryKey(Integer id);
    
    MemberLevel selectByPrimaryName(String name);
    

    int updateByPrimaryKeySelective(MemberLevel record);

	List<MemberLevel> queryPage(Page<MemberLevel> page);
	
	List<MemberLevel> queryAll();
	
	MemberLevel querryByPrimaryKeymemberId(String memberId);
	
	MemberLevel querryByNextLevel(@Param("memberLevelId")Integer memberLevelId);
	
}