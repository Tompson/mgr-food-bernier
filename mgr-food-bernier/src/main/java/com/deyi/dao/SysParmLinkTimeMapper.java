package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysParmLinkTime;

@Repository
public interface SysParmLinkTimeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysParmLinkTime record);

    int insertSelective(SysParmLinkTime record);

    SysParmLinkTime selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysParmLinkTime record);

    int updateByPrimaryKey(SysParmLinkTime record);
    List<SysParmLinkTime> selectAll();
}