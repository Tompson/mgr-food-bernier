package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.Chief;
import com.deyi.util.Page;

@Repository("chiefDao")
public interface ChiefDao {

	List<Chief> queryPage(Page<Chief> page);
	int deleteByPrimaryKey(Integer id);

    int insert(Chief record);

    int insertSelective(Chief record);

    Chief selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Chief record);

    int updateByPrimaryKey(Chief record);
    
    List<Chief> queryAllByStoreId(Integer storeId);
    
   
	
}
