package com.deyi.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.Goods;
import com.deyi.entity.Voucher;
import com.deyi.util.Page;
@Repository
public interface VoucherMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Voucher record);

    int insertSelective(Voucher record);

    Voucher selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Voucher record);

    int updateByPrimaryKey(Voucher record);
    
    List<Voucher> queryExchangePage(Page<Voucher> page);//积分兑换代金券分页
    List<Voucher> queryBookPage(Page<Voucher> page);//预定返代金券分页 
    List<Voucher> queryDrawPage(Page<Voucher> page);//评论，分享抽奖代金券分页
    /**
     * 查询设置好的所有参加抽奖的代金券
     * @author limh Create on 2016年11月15日 下午9:34:49
     * @return
     */
    List<Voucher> selectDrawVoucher();
    /**
     * 查询设置好的所有满足的预定类型的代金券
     * 1.满足最低消费中的最大值
     * 2.满足面额中的最大值
     * @author limh Create on 2016年11月22日 下午4:40:07
     * @return
     */
    List<Voucher> selectSatisfyBookVoucher(BigDecimal totalPrice);
}