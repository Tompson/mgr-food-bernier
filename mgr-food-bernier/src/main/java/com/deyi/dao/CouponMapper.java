package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.Coupon;
@Repository
public interface CouponMapper {
    int deleteByPrimaryKey(Long cid);

    int insert(Coupon record);

    int insertSelective(Coupon record);

    Coupon selectByPrimaryKey(Long cid);

    int updateByPrimaryKeySelective(Coupon record);

    int updateByPrimaryKey(Coupon record);
}