package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.AccountDetails;
@Repository
public interface AccountDetailsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AccountDetails record);

    int insertSelective(AccountDetails record);

    AccountDetails selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AccountDetails record);

    int updateByPrimaryKey(AccountDetails record);
}