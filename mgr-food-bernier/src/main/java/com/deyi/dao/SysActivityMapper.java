package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysActivity;
@Repository
public interface SysActivityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysActivity record);

    int insertSelective(SysActivity record);

    SysActivity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysActivity record);

    int updateByPrimaryKey(SysActivity record);
}