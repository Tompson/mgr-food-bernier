package com.deyi.dao;

import org.springframework.stereotype.Repository;

import com.deyi.entity.SysMoneyAccount;
@Repository
public interface SysMoneyAccountMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysMoneyAccount record);

    int insertSelective(SysMoneyAccount record);

    SysMoneyAccount selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysMoneyAccount record);

    int updateByPrimaryKey(SysMoneyAccount record);
}