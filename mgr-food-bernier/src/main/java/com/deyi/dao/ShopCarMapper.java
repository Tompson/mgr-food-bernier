package com.deyi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.deyi.entity.ShopCar;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;

@Repository
public interface ShopCarMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ShopCar record);

    int insertSelective(ShopCar record);

    ShopCar selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopCar record);

    int updateByPrimaryKey(ShopCar record);
    
    /**
     * 
     * @Title: statPriceAndNumByMemberIdTableId 
     * @Description: 统计堂食购物车
     * @param memberId 会员id
     * @param tableId 桌子id
     * @return
     * ShopCatVo
     * @throws
     */
    ShopCatVo statPriceAndNumByMemberIdTableId(@Param("memberId") Long memberId,@Param("tableId") Long tableId);
    /**
     * 
     * @Title: selectByMemberIdTableId 
     * @Description: 查询购物车列表_堂食
     * @param memberId
     * @param tableId
     * @return
     * ShopTrolleyVo
     * @throws
     */
    List<ShopTrolleyVo> selectByMemberIdTableId(@Param("memberId") Long memberId,@Param("tableId") Long talbleId);
    
    /**
     * 
     * @Title: deleteByMemberIdStoreId 
     * @Description: 清空门店下的购物车
     * @param memberId 
     * @param storeId
     * @return
     * int
     * @throws
     */
    int deleteByMemberIdStoreId(@Param("memberId") Long memberId,@Param("storeId") Long storeId);

	int selectGoodsNum(@Param("memberId") Integer memberId,@Param("tableId")  Integer tableId, @Param("storeGoodsId") Long storeGoodsId);

	ShopCar selectShopCarByMemberIdAndTableIdAndStoreGoodsId(@Param("memberId") Integer memberId,@Param("tableId")  Integer tableId, @Param("storeGoodsId") Long storeGoodsId);

	ShopTrolleyVo selectByMemberIdStoreGoodsId(@Param("memberId")long memberid, @Param("sgid")Long sgid);
}