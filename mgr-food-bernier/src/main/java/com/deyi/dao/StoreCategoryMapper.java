package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface StoreCategoryMapper {

	List<String> getCategoryListByStoreId(int id);
	
	//根据分类  所有门店下面分类
	int delCategoryByStoreId(Long sId);


}
