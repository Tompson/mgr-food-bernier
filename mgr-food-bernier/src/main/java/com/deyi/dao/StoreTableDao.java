package com.deyi.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.deyi.entity.StoreTable;
import com.deyi.util.Page;
@Repository
public interface StoreTableDao {
    int deleteByPrimaryKey(Integer id);

    int insert(StoreTable record);

    int insertSelective(StoreTable record);

    StoreTable selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StoreTable record);

    int updateByPrimaryKey(StoreTable record);
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<StoreTable> queryPage(Page<StoreTable> page);



}