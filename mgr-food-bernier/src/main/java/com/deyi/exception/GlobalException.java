package com.deyi.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.controller.h5.H5Controller;
/**
 * 微信端异常解析
 * @author limh Create on 2016年11月21日 下午3:20:18
 */
public class GlobalException implements HandlerExceptionResolver{
	private Logger logger = LoggerFactory.getLogger(GlobalException.class);
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		String requestURI = request.getRequestURI();
		logger.info("GlobalException-------->"+requestURI);
		System.out.println(requestURI);
		logger.info("globalException--->errormsg:"+ex.getMessage());
		/*if(requestURI.startsWith(request.getContextPath()+ "/h5")){
			ModelAndView mv = new ModelAndView();
			mv.setViewName("h5/error");
			return mv;
		}*/
		return null;
	}

}
