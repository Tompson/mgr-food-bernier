/**
 * 
 */
package com.deyi.message;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.deyi.util.MD5S;
import com.deyi.util.PropertiesUtil;

import sun.misc.BASE64Encoder;

/**
 * pure
 * @author hejx
 * @2016年4月30日
 */
public class SmsClient {

	private static Logger log = LoggerFactory.getLogger(SmsClient.class);
	/**
	 * 发送单个手机；多个号码要修改
	 * @param mobile
	 * @param content
	 * @return
	 */
	public static ResponseBean send(String mobile,String content){
		if(StringUtils.isBlank(mobile) || mobile.length()!=11){
			log.info("发生短信手机号码错误...");
			return null;
		}
		if(StringUtils.isBlank(content)){
			log.info("发生短信内容为空...");
			return null;
		}
		String postURL = PropertiesUtil.getProperty("sms_cshx_url");
//		String postURL = "http://dx.ipyy.net/ensms.ashx";
		String userid = PropertiesUtil.getProperty("sms_cshx_userid");
		String useraccont = PropertiesUtil.getProperty("sms_cshx_account");
		String password = PropertiesUtil.getProperty("sms_cshx_password");

		SimpleDateFormat df=new SimpleDateFormat("MMddHHmmss");		
		String Stamp = df.format(new Date());
		String Secret=MD5S.GetMD5Code(password+Stamp).toUpperCase();
		
		try {
			net.sf.json.JSONObject j=new net.sf.json.JSONObject();
			j.put("UserName", useraccont);
			j.put("Stamp", Stamp);
			j.put("Secret", Secret);
			j.put("Moblie", mobile);
			j.put("Text",content);
			j.put("Ext", "");
			j.put("SendTime", "");
			//获取json字符串
			String json=j.toString();
			byte[] data=json.getBytes("utf-8");
			byte[] key=password.getBytes();
			//获取加密的key
			byte[] nkey=new byte[8];
			System.arraycopy(key, 0, nkey, 0, key.length > 8 ? 8 : key.length);
			//Des加密，base64转码
			String str=new BASE64Encoder().encode(com.deyi.util.DesHelper.encrypt(data, nkey)); 
			
			//发送http请求
			HttpClient client=new HttpClient();
			PostMethod post=new PostMethod(postURL);
			post.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
			NameValuePair UserId=new NameValuePair("UserId",userid);
			NameValuePair Text64=new NameValuePair("Text64",str);
			post.setRequestBody(new NameValuePair[]{UserId,Text64});
			int statu=client.executeMethod(post);
			//返回结果
			String result=post.getResponseBodyAsString();
			log.info("发送短信接口："+result);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) {
		
		send("18666289149", "【得壹科技】您的验证码为：4444，请不要把验证码泄露给他人");
	}
	
}
