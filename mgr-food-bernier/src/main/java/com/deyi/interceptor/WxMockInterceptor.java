package com.deyi.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.deyi.entity.Member;
import com.deyi.service.MemberService;
import com.deyi.util.Constants;


/**
 * @author jimyang
 */
public class WxMockInterceptor extends HandlerInterceptorAdapter {
	private Logger LOG = LoggerFactory.getLogger(WxMockInterceptor.class);	
	@Autowired
	private MemberService memberService;
	
	public boolean preHandle(HttpServletRequest httpRequest, HttpServletResponse response, Object handler)
		throws Exception {
		 boolean isHandle = super.preHandle(httpRequest, response, handler);
		fullMockSessionInfo(httpRequest);
		
		return isHandle;
	}

	/**
	 * This implementation is empty.
	 */
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
	}

	/**
	 * This implementation is empty.
	 */
	public void afterCompletion(
			HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
	
	private void fullMockSessionInfo(HttpServletRequest httpRequest){
//		Member member = memberService.selectByWxOpenId("oEviTuCwuRQT4QWzdUTBCz5PG0nA");//至尚时尚公众号
////		Member member = memberService.selectByWxOpenid("o8KfEwC4LXknHl8lTK6AmviI542g");//得壹科技公众号
//		Integer num = shopTrolleyService.queryShopTrolleyNum(member.getMemberId(),member.getSchoolId());
		
		HttpSession session = httpRequest.getSession();
		session.setAttribute(Constants.SESSION_WXOPENID, "oDQIVvxSDJn5wE3S7QdYPX3qJK6Q");//放羊的喵喵的openid
		Member member = memberService.selectByWxOpenId("oDQIVvxSDJn5wE3S7QdYPX3qJK6Q");
		if(member!=null){
			session.setAttribute(Constants.SESSION_MEMBER, member);
		}
//		session.setAttribute(Constants.SESSION_SHOP_TROLLEY_NUM, num);
	}

}
