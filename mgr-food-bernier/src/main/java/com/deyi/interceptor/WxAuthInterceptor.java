/**  
* @Title: AuthorityInterceptor.java
* @Package com.yiqi.controller
* @Description: 深圳市得壹科技有限公司
*/
package com.deyi.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.deyi.entity.Member;
import com.deyi.entity.SysParm;
import com.deyi.entity.SysUser;
import com.deyi.model.WxOauth2Token;
import com.deyi.net.WxApiClient;
import com.deyi.service.MemberService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.SysParmService;
import com.deyi.service.SysUserService;
import com.deyi.service.impl.BizService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;

/**
 * preHandle方法是进行处理器拦截用的，顾名思义，该方法将在Controller处理之前进行调用，
 * SpringMVC中的Interceptor拦截器是链式的，可以同时存在多个Interceptor，
 * 然后SpringMVC会根据声明的前后顺序一个接一个的执行，
 * 而且所有的Interceptor中的preHandle方法都会在Controller方法调用之前调用。
 * SpringMVC的这种Interceptor链式结构也是可以进行中断的，
 * 这种中断方式是令preHandle的返回值为false，当preHandle的返回值为false的时候整个请求就结束了。
 */
public class WxAuthInterceptor extends HandlerInterceptorAdapter {
	private Logger LOG = LoggerFactory.getLogger(WxAuthInterceptor.class);
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private BizService bizService;
	
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	
	@Autowired
	private SysUserService sysUserService;
	
	@Autowired
	private SysParmService sysParmService;
	
	public boolean preHandle(HttpServletRequest httpRequest, HttpServletResponse response, Object handler)
			throws Exception {
		boolean isHandle = super.preHandle(httpRequest, response, handler);
//		PrintWriter writer =null;
		try{
		response.reset(); 
		HttpSession session = httpRequest.getSession();
		String wxopenid = (String) session.getAttribute(Constants.SESSION_WXOPENID);
		LOG.info("preHandle:---->wxopenid====" + wxopenid);
		String uri = httpRequest.getRequestURI();
		boolean noNeedWapAuth = BizHelper.isNoNeedWapAuth(uri);
		boolean needAuthForUpgrade = BizHelper.isNeedAuthForUpgrades(uri);
		if (noNeedWapAuth && !needAuthForUpgrade) {
			return true;
		}
		String startPage="";
		
		String queryString = httpRequest.getQueryString();
		String code = httpRequest.getParameter("code");

		String state = httpRequest.getParameter("state");
		StringBuffer url = httpRequest.getRequestURL();
		if ((StringUtils.isBlank(code)||StringUtils.isBlank(state)) && StringUtils.isNotBlank(queryString)) {
			url.append("?").append(queryString);
		}
		startPage = url.toString();
		response.setContentType("text/html;charset=UTF-8");
		
//		writer = response.getWriter();
		LOG.info(String.format(">>>url=%s, code=%s, state=%s", startPage, code, state));
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member == null) {
			if (StringUtils.isNotBlank(code) && StringUtils.isNotBlank(state)
					&& fetchOauthToken(code, state, httpRequest,needAuthForUpgrade)) {
				wxopenid = (String) session.getAttribute(Constants.SESSION_WXOPENID);
				String token =(String) session.getAttribute("token");
				member = memberService.selectByWxOpenId(wxopenid);
				if(member == null){
					// 会员关注创建失败了，可以重新调用BizService.newMember()创建
					member = bizService.checkAndCreateMember(wxopenid,token,needAuthForUpgrade);
				}
				//登录成功，清空购物车
//				shopTrolleyService.deleteByMemberId(member.getMemberId());
				return true;
			}

			String redirectUrl = null;
			if (needAuthForUpgrade) {
				redirectUrl = createRedirectForUpgrade(startPage);
			} else {
				redirectUrl = createRedirect(startPage);
			}

			LOG.info(String.format(">>>redirectUrl=%s", redirectUrl));
			try {
				response.sendRedirect(redirectUrl);
			} catch (IOException e) {
				LOG.error("IOException--------1", e);
			}
			return false;
		}
		LOG.info(String.format(">>>sessionInfo=%s", member.toString()));
		//会员存在清空下，每次拦截判断该会员是否存在，避免后台会员被删除了，依然还可以操作
		if(Constants.MEMBER_STATUS_MEMBER_RANK_DELETED==member.getMemberRank()){
			//删除状态，不能登录
			SysUser sysUser = new SysUser();
			sysUser.setUserAccount("admin");
			List<SysUser> ss = sysUserService.checkAccountName(sysUser);
			String phone="";
			if(ss!=null&&!ss.isEmpty()){
				phone=ss.get(0).getPhone();
			}else{
				SysParm realSysParam = sysParmService.getRealSysParam();
				phone=realSysParam.getBookDiscount();
			}
//			writer.println("<p style=\"text-align: center;font-size: 2.6rem;margin-top: 10rem;\">您的账号已被删除</p>");
//			writer.println("<p style=\"text-align: center;font-size: 2.6rem;\"><a href=\"tel:"+phone+"\">联系管理员</a></p>");
//			writer.flush();
			return false;
		}
		return isHandle;
		}catch(Exception e){
			e.printStackTrace();
//			writer.println("<p style=\"text-align: center;font-size: 2.6rem;margin-top: 10rem;\">系统繁忙</p>");
//			writer.flush();
			return false;
		}
	}

	/**
	 * This implementation is empty.
	 */
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	/**
	 * This implementation is empty.
	 */
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	private boolean fetchOauthToken(String code, String state, HttpServletRequest httpRequest, boolean needAuthForUpgrade) {
		
		if (StringUtils.isNotBlank(code) && StringUtils.isNotBlank(state)) {
			try {
				WxApiClient apiClient = new WxApiClient();
				WxOauth2Token h5Token = apiClient.getOauth2AccessToken(code);
				LOG.info("微信授权票据--->token===="+h5Token.toString());
				if (h5Token != null&&h5Token.getOpenid()!=null) {
					httpRequest.getSession().setAttribute(Constants.SESSION_WXOPENID, h5Token.getOpenid());
					httpRequest.getSession().setAttribute("token", h5Token.getAccess_token());
					Member member = memberService.selectByWxOpenId(h5Token.getOpenid());
					if(member == null){
						// 会员关注创建失败了，可以重新调用BizService.newMember()创建
						member = bizService.checkAndCreateMember(h5Token.getOpenid(),h5Token.getAccess_token(),needAuthForUpgrade);
					}
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				LOG.info("获取wxopenid异常");
			}
		}
		return false;
	}

	private String createRedirect(String startPage) {
		try {
			startPage = replaceIpToUrl(startPage);
			startPage = URLEncoder.encode(startPage, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String redirect = String.format(
				"https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect",
				BizHelper.getWxAppId(), startPage);
		// LOG.info("before redirect to " + redirect);
		return redirect;
	}

	private String createRedirectForUpgrade(String startPage) {
		try {
			startPage = replaceIpToUrl(startPage);
			startPage = URLEncoder.encode(startPage, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String redirect = String.format(
				"https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect",
				BizHelper.getWxAppId(), startPage);
		return redirect;
	}

	private String replaceIpToUrl(String url) {
		System.out.println("BizHelper.getServerIp()"+BizHelper.getServerIp());
		System.out.println("BizHelper.getServerUrl()"+BizHelper.getServerUrl());
		return url.replace(BizHelper.getServerIp(), BizHelper.getServerUrl());
		
	}

}
