package com.deyi.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.deyi.entity.SysUser;
import com.deyi.util.Constants;
import com.deyi.util.HttpResponseUtil;
import com.deyi.util.PropertiesUtil;


/**
 * @Description:用户登录拦截器
 * @author tcj
 * @date  2016年5月10日
 */
public class UserInterceptor extends HandlerInterceptorAdapter{

	private final static Logger log = LoggerFactory.getLogger(UserInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info("用户登录拦截Start......");

		HttpSession session = request.getSession();
		SysUser userInfo = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		String uri = request.getRequestURI().toUpperCase()+"";
		StringBuffer requestURL = request.getRequestURL();//绝对路径(完整路径)
		String projectName=request.getContextPath()+"";
		
		String[] strs = uri.split("\\/");
		String uriMap = "";
		if(!projectName.equals("")){//在项目目录下
			if(strs.length > 2){
				for(int i=2;i<strs.length;i++){
					uriMap +="/"+strs[i];
				}
			}else {
				uriMap = "/";
			}
		}else{//把项目直接发布到tomcat的ROOT目录下
			if(strs.length > 1){
				for(int i=1;i<strs.length;i++){
					uriMap +="/"+strs[i];
				}
			}else {
				uriMap = "/";
			}
		}
		//app的url放行
//		if(uriMap.toLowerCase().contains("/api".toLowerCase())){
//			log.info("用户登录拦截End......");
//			return true;
//		}
//		if(uriMap.toLowerCase().contains("/app".toLowerCase())){
//			log.info("用户登录拦截End......");
//			return true;
//		}
/*		//如果是登录页面则放行
		if(request.getRequestURI().indexOf("login.html")>=0){
			log.info("用户登录拦截End......");
			return true;
		}
		//如果是退出页面则放行
		if(request.getRequestURI().indexOf("loginOut.html")>=0){
			log.info("用户登录拦截End......");
			return true;
		}*/
		
		//放过不需要拦截的地址
		String no_backstage_login_filterpath = PropertiesUtil.getProperty("no_backstage_login_filterpath");
		String[] indexStrs = no_backstage_login_filterpath.split(",");
		for (String item : indexStrs) {
			if(uriMap.split("\\.")[0].equalsIgnoreCase(item)){
				return true;
			}
		}
		
		
		//如果用户已登录也放行
		if(session.getAttribute(Constants.SESSION_USERINFO)!=null){
			log.info("用户登录拦截End......");
			return true;
		}
		//session为空跳转到登录
		String loginPath = projectName + "/system/login.html";
		if(userInfo == null){
			try {
				log.info("用户拦截,用户session为空...");
				log.info("[[登陆拦截]]用户session为空<==>"+request.getRequestURI());
				HttpResponseUtil.renturnUrl(response, loginPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
	}
		log.info("[[登陆拦截]]用户session为空<==>"+request.getRequestURI());
		return false;
	}
	

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterCompletion(request, response, handler, ex);
	}
	
}
