package com.deyi.vo;

import java.io.Serializable;

/**
 * 移动端返回值通用类型
 * @author jimyang
 *
 * @param <T>
 */
public class ReturnVo<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean success = true;//默认返回true
	
	private String message = "";//默认返回消息为空字符串
	
	private int statusCode=0;//默认返回的状态为0
	
	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}


	
}
