package com.deyi.vo;

import java.io.Serializable;

public class RequestGrid<T> implements Serializable{
	/**
	 * select * from table limit 20 5
	 */
	private static final long serialVersionUID = 2360324948063137585L;
	private String order;
	private Integer offset;//从第几条索引(20)
	private Integer limit;//每页显示多少条
	
	private T object;//实体类
	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}
	/**
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}
	/**
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}
	/**
	 * @param offset the offset to set
	 */
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	/**
	 * @return the limit
	 */
	public Integer getLimit() {
		return limit;
	}
	/**
	 * @param limit the limit to set
	 */
	public void setLimit(Integer limit) {
		this.limit = limit;
	} 
	
	public Integer getPageNum(){//PageNum当前页等于:从第几条索引(0,10,20)除以要显示多少条(5)加1 20/5+1=5
		return this.offset /this.limit + 1;
	}
	/**
	 * @return the object
	 */
	public T getObject() {
		return object;
	}
	/**
	 * @param object the object to set
	 */
	public void setObject(T object) {
		this.object = object;
	}
	
	
	
	
	
}
