package com.deyi.vo;

public class DiscountTypeVo {
	private boolean hasDisCount=false;//折扣
	private boolean full=false;//满减
	private boolean memberDiscount=false;//会员折扣
	private boolean voucher=false;//平台代金卷
	public boolean isHasDisCount() {
		return hasDisCount;
	}
	public void setHasDisCount(boolean hasDisCount) {
		this.hasDisCount = hasDisCount;
	}
	public boolean isFull() {
		return full;
	}
	public void setFull(boolean full) {
		this.full = full;
	}
	public boolean isMemberDiscount() {
		return memberDiscount;
	}
	public void setMemberDiscount(boolean memberDiscount) {
		this.memberDiscount = memberDiscount;
	}
	public boolean isVoucher() {
		return voucher;
	}
	public void setVoucher(boolean voucher) {
		this.voucher = voucher;
	}
	
}
