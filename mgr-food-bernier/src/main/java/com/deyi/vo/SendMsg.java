package com.deyi.vo;
import java.util.Collection;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.ServerContext;
import org.directwebremoting.ServerContextFactory;
import org.directwebremoting.proxy.dwr.Util;

import com.deyi.entity.SysOrder;
import com.dy.util.PropertiesUtil; 
public class SendMsg{

	    public static void sendMsg(SysOrder order,ServletContext servletContext,HttpServletRequest request){
	    	
	        //得到上下文   
	    	ServerContext context = ServerContextFactory.get(servletContext);
	        //得到要推送到 的页面  camshop为项目名称 ， 一定要加上。  
	        Collection<ScriptSession> sessions = context.getScriptSessionsByPage(request.getContextPath()+"/system/main.html");   
	        //不知道该怎么解释这个 ，   
	        Util util = new Util(sessions);  
	          
	        //下面是创建一个javascript脚本 ， 相当于在页面脚本中添加了一句  show(msg);   
	        ScriptBuffer sb = new ScriptBuffer();  
	        String payNumber = order.getPayNumber();
			String property = PropertiesUtil.getProperty("order_pay_no_prifix");
			if(payNumber.contains(property)){
				String replace = StringUtils.replace(payNumber, property, "");
				order.setPayNumber(replace);
			}
	        sb.appendCall("show", order.getPayNumber(),order.getContactsPerson()); //订单id 和联系人 	          
	        //推送  
	        util.addScript(sb);  
	    }
}
