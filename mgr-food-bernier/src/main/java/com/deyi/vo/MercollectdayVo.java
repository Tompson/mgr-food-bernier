package com.deyi.vo;

import java.util.Date;

public class MercollectdayVo  {
    private Integer id;
	private Date starttime;

	private Date endtime;

	private Date creattime;

	// extend
	private String querystarttime;

	private String queryendtime;
	private Integer orderNo;
	private Double saleMoney;
    private String month;
    private String time;
    private String flag = "1";   // 1.进行日统计   2.进行按月统计
    private String storeid;   
    

	public String getTime() {
		return time;
	}


	public void setTime(String time) {
		this.time = time;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Date getStarttime() {
		return starttime;
	}


	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}


	public Date getEndtime() {
		return endtime;
	}


	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}


	public Date getCreattime() {
		return creattime;
	}


	public void setCreattime(Date creattime) {
		this.creattime = creattime;
	}


	public String getQuerystarttime() {
		return querystarttime;
	}


	public void setQuerystarttime(String querystarttime) {
		this.querystarttime = querystarttime;
	}


	public String getQueryendtime() {
		return queryendtime;
	}


	public void setQueryendtime(String queryendtime) {
		this.queryendtime = queryendtime;
	}


	public Integer getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}


	public Double getSaleMoney() {
		return saleMoney;
	}


	public void setSaleMoney(Double saleMoney) {
		this.saleMoney = saleMoney;
	}


	public String getMonth() {
		return month;
	}


	public void setMonth(String month) {
		this.month = month;
	}


	public String getFlag() {
		return flag;
	}


	public void setFlag(String flag) {
		this.flag = flag;
	}


	public String getStoreid() {
		return storeid;
	}


	public void setStoreid(String storeid) {
		this.storeid = storeid;
	}
    
}
