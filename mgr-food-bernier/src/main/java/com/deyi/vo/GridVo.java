package com.deyi.vo;

import java.util.List;

public class GridVo {//返回一个GridVo对象包含一个集合和long基本类型

	private List<?> rows; //返回的集合对象List<Member>
	
	private long total;//总记录数 如51
	
	
	//以下为扩展的返回数据
	private int statusCode=0;//默认返回的状态为0
	
	private Object object;//默认返回的对象
	
	private Integer statOrderStatusEqUntreated;//统计订单状态等于未处理的数量 相当于返回的数据
	
	
	
	

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Integer getStatOrderStatusEqUntreated() {
		return statOrderStatusEqUntreated;
	}

	public void setStatOrderStatusEqUntreated(Integer statOrderStatusEqUntreated) {
		this.statOrderStatusEqUntreated = statOrderStatusEqUntreated;
	}

	/**
	 * @return the rows
	 */
	public List<?> getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	/**
	 * @return the total
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(long total) {
		this.total = total;
	}
	
	
}

