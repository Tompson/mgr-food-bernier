package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.GroupPurchase;
import com.deyi.entity.SysUser;
import com.deyi.service.GroupPurchaseService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;

/** 
 * @author zjz Create on 2016年11月28日 上午11:21:17 
 */
@Controller
@RequestMapping(value = "groupPurchase")
public class GroupPurchaseController  extends Component<GroupPurchase> {
	private Logger log = LoggerFactory.getLogger(GroupPurchaseController.class);
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("groupPurchase/groupPurchaseList");
		return mav;
	}

	
	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<GroupPurchase> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("团购团套餐说明列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<GroupPurchase> page = new Page<GroupPurchase>();
		GridVo vo = queryPage(reqGrid, request, page);
//		if(reqGrid.getObject() == null){
//			reqGrid.setObject(new GroupPurchase());
//		}
		
		List<GroupPurchase> results = groupPurchaseService.queryPage(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}
	
	
	/**
	 *   删除 
	 */
	@RequestMapping(value="deleteGroupPurchase")
	public @ResponseBody ReturnVo<Object> deleteGroupPurchase(String id,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			groupPurchaseService.deleteByPrimaryKey(Long.parseLong(id));
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}



}
