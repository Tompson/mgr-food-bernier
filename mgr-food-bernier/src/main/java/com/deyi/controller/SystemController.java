package com.deyi.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Menu;
import com.deyi.entity.OrderGoods;
import com.deyi.entity.SysUser;
import com.deyi.service.GoodsService;
import com.deyi.service.MenuService;
import com.deyi.service.OrderGoodsService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysUserService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.MD5S;
import com.deyi.vo.ReturnVo;
import com.google.gson.Gson;


@Controller
@RequestMapping(value ="system")
public class SystemController extends Component<SysUser> {
	
	private Logger log = LoggerFactory.getLogger(SystemController.class);
	
	@Autowired
	private SysUserService sysUserService;
	
	@Autowired
	private SysOrderService sysOrderService;
	
	@Autowired
	private OrderGoodsService orderGoodsService;
	
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private MenuService menuService;
	
	
	@RequestMapping(value = "main")
	public ModelAndView main(ModelAndView mav,String loginName,String passwd,HttpServletRequest request){
		HttpSession session = request.getSession();
//		获取Session中的用户信息,
		SysUser userInfo = (SysUser) session.getAttribute(Constants.SESSION_USERINFO);
		if(userInfo==null){
			mav.addObject("用户名或密码错误,请重新登录!");
			mav.setViewName("login");
			return mav;
		}
		Long storeId=null;
		if(!"1".equals(userInfo.getType())){
			storeId=Long.valueOf(userInfo.getStoreId());
		}
		//统计交易笔数
		int statDealNum = sysOrderService.statDealNum(storeId);
		//交易总额
		BigDecimal statDealSumAmount = sysOrderService.statDealSumAmount(storeId);
		if(statDealSumAmount == null){
			statDealSumAmount = new BigDecimal("0");
		}
		//昨日交易笔数
		int statDealNumYesterday = sysOrderService.statDealNumYesterday(storeId);
		//昨日交易金额
		BigDecimal statDealSumAmountYesterday = sysOrderService.statDealSumAmountYesterday(storeId);
		if(statDealSumAmountYesterday == null){
			statDealSumAmountYesterday = new BigDecimal("0");
		}
		//交易商品总数量
		Integer statDealGoodsSum = goodsService.StatDealGoodsSum(storeId);
		if(statDealGoodsSum == null){
			statDealGoodsSum = new Integer(0);
		}
		//昨日交易商品总数量
		Integer statOrderGoodsSum = orderGoodsService.StatOrderGoodsSum(storeId);//null
		if(statOrderGoodsSum == null){
			statOrderGoodsSum = new Integer(0);
		}
		//本月销售数量排行前10商品
		List<OrderGoods> statQuantifySumList = orderGoodsService.StatQuantifySum(storeId);//3
		//本月销售金额排行前10商品
		List<OrderGoods> statPricePaySumList = orderGoodsService.StatPricePaySum(storeId);//3
		
		mav.addObject("statDealNum",statDealNum);//统计交易笔数
		mav.addObject("statDealSumAmount",statDealSumAmount);//交易总额
		mav.addObject("statDealNumYesterday",statDealNumYesterday);//昨日交易笔数
		mav.addObject("statDealSumAmountYesterday",statDealSumAmountYesterday);//昨日交易金额
		mav.addObject("statDealGoodsSum",statDealGoodsSum);//交易商品总数量
		mav.addObject("statOrderGoodsSum",statOrderGoodsSum);//昨日交易商品总数量
		mav.addObject("statQuantifySumList",new Gson().toJson(statQuantifySumList));//本月销售数量排行前10商品
		mav.addObject("statPricePaySumList",new Gson().toJson(statPricePaySumList));//本月销售金额排行前10商品
		
		//统计订单状态为未处理的订单数 
		statOrderStatusEqUn(mav);
		mav.setViewName("index");
		return mav;
	}
	
	/**
	 * 每次调changeRightMenu方法,就请求后台得到订单状态为未处理的总数,同时检查用户session有值吗?有就继续,没有就退出重新登录
	 * @Author zjz Create on 2016年11月25日 下午2:27:07
	 * @param id
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "getUntreatedOrderNum")
	public  ReturnVo<Object> getUntreatedOrderNum(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			SysUser userInfo = getSessionUser(request);
			if(userInfo == null){
				vo.setSuccess(false);
				vo.setMessage("管理员信息有误请重新登录");
				return vo;
			}
			
			vo.setSuccess(true);
			statOrderStatusEq0ReturnVo(vo);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	


	
	
	
	/**
	 * 用户登录
	 * @return
	 */
	@RequestMapping(value = "login")
	public ModelAndView login(ModelAndView mav,String loginName,String passwd,HttpServletRequest request){
		HttpSession session = request.getSession();
		log.info(loginName+"登录系统......");
		SysUser user = null;
		// 获取Session中的用户信息,
		SysUser userInfo = (SysUser) session.getAttribute(Constants.SESSION_USERINFO);
		
		
		if(null==userInfo){
			if (loginName == null  || passwd == null || "".equals(loginName) || "".equals(passwd)) {
				mav.addObject("errMsg", "用户名或者密码为空");
				mav.setViewName("login");
				return mav;
			}
			
			//1.判断用户名和密码是否有效和状态是否可用
			if(passwd!=null){
				String md5Pwd = MD5S.GetMD5Code(passwd);
				user = sysUserService.checkUser(loginName,md5Pwd);
				if(user!=null){
					if(("2").equals(user.getStatus())){
						mav.addObject("errMsg","该账号已被禁用，无法登陆");
						mav.setViewName("login");
						return mav;
					}
				}
			}
			if(user!=null){
				List<String> urls = new ArrayList<String>();// 用户有权限的url地址
				List<Menu> menuList = menuService.getMenusByUser(user.getUid());
				for (Menu menu : menuList) {
					String url = menu.getMenuUrl();
					if (!StringUtils.isBlank(url)) {
						String[] meunArray = url.split(",");
						for (String str : meunArray) {
							urls.add(str);
						}
					}
				}
				//存入session
				session.setAttribute(Constants.SESSION_USERINFO, user);
				session.setMaxInactiveInterval(86400);
				session.setAttribute(Constants.MENU_LIST, menuList);
				session.setAttribute(Constants.SESSION_USER_URLS, urls);
			}else{
				mav.addObject("errMsg","用户名或密码错误");
				mav.setViewName("login");
				return mav;
			}
		}else{ //如果session里面有数据就把系统用户信息存入session
			session.setAttribute(Constants.SESSION_USERINFO, userInfo);
		}
		
		mav.setViewName("redirect:main.html");//重定向 到主页
		return mav;
	}
	

	/**
	 * 初始化登录页面
	 * 方法用途: 用户首页<br>
	 * @param mav
	 * @return
	 */
	@RequestMapping(value = "index")
	public ModelAndView index(ModelAndView mav){
		mav.setViewName("login");
		return mav;
	}
	
	/**
	 * 跳转到Index页面
	 */
	@RequestMapping(value = "toIndex")
	public ModelAndView toIndex(ModelAndView mav){
		mav.setViewName("index");
		return mav;
	}
	
	/**
	 * pc退出登录
	 * @param mav
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loginOut")
	public ModelAndView loginOut(ModelAndView mav,HttpServletRequest request){
		log.info("退出系统.......");
		HttpSession session  = request.getSession();
		session.invalidate();
		mav.setViewName("login");
		//mav.setViewName("forward:/system/index.html");
		return mav;
	}
	
	@RequestMapping(value="toUpdatePwd")
	public ModelAndView toEditUserPad(ModelAndView mav,HttpServletRequest request){
		HttpSession session  = request.getSession();
		SysUser userInfo = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		mav.addObject("userInfo", userInfo);
		mav.setViewName("sysUser/toUpdatePwd");
		return mav;
	}
	
	/**
	 * 更新用户输入的新密码
	 * @param mav
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "updatePwd")
	public @ResponseBody ReturnVo<Object> updatePwd(ModelAndView mav,HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<>();
		HttpSession session = request.getSession();
		SysUser userInfo = (SysUser) session.getAttribute(Constants.SESSION_USERINFO);
		//获取新密码
		try{
			String newPwd = request.getParameter("newPwd");
			SysUser user = new SysUser();
			String md5Pwd = MD5S.GetMD5Code(newPwd);//加密密码
			user.setUserPass(md5Pwd);
			Integer uid = userInfo.getUid();
			if(null != uid){
				user.setUid(uid);
				sysUserService.updateByPrimaryKeySelective(user);
				vo.setSuccess(true);
			}
			vo.setMessage("更新密码成功!");
		}catch(Exception e){
			vo.setMessage("更新密码失败!");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	/**
	 * 验证原密码是否正确
	 */
	@RequestMapping(value = "comparePad")
	public @ResponseBody Boolean comparePad(ModelAndView mav,String pad, HttpServletRequest request){
		HttpSession session  = request.getSession();
		SysUser userInfo = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		String yuanPad = userInfo.getUserPass();
		String mdPsw = MD5S.GetMD5Code(pad);
		if(mdPsw.equalsIgnoreCase(yuanPad)){
		    return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 点击首页功能
	 */
	@RequestMapping(value = "indexmain")
	public ModelAndView indexmain(ModelAndView mav,HttpServletRequest request){
		HttpSession session = request.getSession();
		//获取Session中的用户信息,
		SysUser userInfo = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(userInfo==null){
			mav.addObject("用户名或密码错误,请重新登录!");
			mav.setViewName("login");
			return mav;
		}
		Long storeId=null;
		if(!"1".equals(userInfo.getType())){
			storeId=Long.valueOf(userInfo.getStoreId());
		}
		//统计交易笔数
		int statDealNum = sysOrderService.statDealNum(storeId);
		//交易总额
		BigDecimal statDealSumAmount = sysOrderService.statDealSumAmount(storeId);
		if(statDealSumAmount == null){
			statDealSumAmount = new BigDecimal("0");
		}
		//昨日交易笔数
		int statDealNumYesterday = sysOrderService.statDealNumYesterday(storeId);
		//昨日交易金额
		BigDecimal statDealSumAmountYesterday = sysOrderService.statDealSumAmountYesterday(storeId);
		if(statDealSumAmountYesterday == null){
			statDealSumAmountYesterday = new BigDecimal("0");
		}
		//交易商品总数量
		Integer statDealGoodsSum = goodsService.StatDealGoodsSum(storeId);
		if(statDealGoodsSum == null){
			statDealGoodsSum = new Integer(0);
		}
		//昨日交易商品总数量
		Integer statOrderGoodsSum = orderGoodsService.StatOrderGoodsSum(storeId);//null
		if(statOrderGoodsSum == null){
			statOrderGoodsSum = new Integer(0);
		}
		
		//本月销售数量排行前10商品
		List<OrderGoods> statQuantifySumList = orderGoodsService.StatQuantifySum(storeId);//3
		//本月销售金额排行前10商品
		List<OrderGoods> statPricePaySumList = orderGoodsService.StatPricePaySum(storeId);//3
		
		mav.addObject("statDealNum",statDealNum);//统计交易笔数
		mav.addObject("statDealSumAmount",statDealSumAmount);//交易总额
		mav.addObject("statDealNumYesterday",statDealNumYesterday);//昨日交易笔数
		mav.addObject("statDealSumAmountYesterday",statDealSumAmountYesterday);//昨日交易金额
		mav.addObject("statDealGoodsSum",statDealGoodsSum);//交易商品总数量
		mav.addObject("statOrderGoodsSum",statOrderGoodsSum);//昨日交易商品总数量
		mav.addObject("statQuantifySumList",new Gson().toJson(statQuantifySumList));//本月销售数量排行前10商品
		mav.addObject("statPricePaySumList",new Gson().toJson(statPricePaySumList));//本月销售金额排行前10商品
		mav.setViewName("indexmain");
		return mav;
	}
}
