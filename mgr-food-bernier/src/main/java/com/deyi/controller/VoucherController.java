package com.deyi.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.SysCoupon;
import com.deyi.entity.SysUser;
import com.deyi.entity.Voucher;
import com.deyi.service.VoucherService;
import com.deyi.util.Component;
import com.deyi.util.Page;
import com.deyi.util.StringUtils;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "voucher")
public class VoucherController extends Component<Voucher> {
	private Logger log = LoggerFactory.getLogger(VoucherController.class);
	@Autowired
	private VoucherService voucherService;
	
	@RequestMapping(value = "exchangeList")
	public ModelAndView exchangeList(ModelAndView mav) throws Exception {//积分兑换代金券
		mav.setViewName("voucher/exchangeList");
		return mav;
	}

	@RequestMapping(value = "bookList")
	public ModelAndView bookList(ModelAndView mav) throws Exception {//预定返代金券
		mav.setViewName("voucher/bookList");
		return mav;
	}
	@RequestMapping(value = "drawList")
	public ModelAndView drawList(ModelAndView mav) throws Exception {//抽奖代金券
		mav.setViewName("voucher/drawList");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "exchangePage")//积分兑换代金券
	public GridVo exchangePage(@RequestBody RequestGrid<Voucher> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		Page<Voucher> page = new Page<Voucher>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<Voucher> results = voucherService.queryExchangePage(page);
		vo.setRows(results);//总共有多少页
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	/**
	 * 跳转新增积分兑换代金券
	 */
	@RequestMapping(value = "toExchangeAdd")
	public ModelAndView toExchangeAdd(ModelAndView mav, HttpServletRequest request) {
		mav.setViewName("voucher/exchangeAdd");
		return mav;
	}
	/**
	 * 新增积分兑换代金券
	 */
	@ResponseBody
	@RequestMapping(value = "exchangeAdd")
	public  ReturnVo<Object> exchangeAdd(ModelAndView mav, HttpServletRequest request,Voucher voucher) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			HttpSession session = request.getSession();
			
			voucher.setVoucherType("1");//代金券类型: 1.积分兑换代金券 2.预订返代金券 3.评论/分享获得抽奖代金券 
			voucherService.insertSelective(voucher);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}


	@RequestMapping(value = "toExchangeEdit")
	public ModelAndView toExchangeEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {

		if (id == null || "".equals(id)) {
			throw new Exception();
		}
		
		Voucher voucher = voucherService.selectByPrimaryKey(Long.parseLong(id));
		
		mav.addObject("voucher", voucher);
		mav.setViewName("voucher/exchangeEdit");
		return mav;
	}

	@RequestMapping(value = "exchangeEdit")
	public @ResponseBody ReturnVo<Object> exchangeEdit(ModelAndView mav, HttpServletRequest request, Voucher voucher) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			voucher.setModifyTime(new Date());
			voucherService.updateByPrimaryKeySelective(voucher);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	@ResponseBody
	@RequestMapping(value = "deleteExchange")
	public  ReturnVo<Object> deleteExchange(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			voucherService.deleteByPrimaryKey(Long.parseLong(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	//以下是预定返代金券代码
	
	@ResponseBody
	@RequestMapping(value = "bookPage")
	public GridVo bookPage(@RequestBody RequestGrid<Voucher> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		Page<Voucher> page = new Page<Voucher>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<Voucher> results = voucherService.queryBookPage(page);
		vo.setRows(results);//总共有多少页
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	/**
	 * 跳转新增积分兑换代金券
	 */
	@RequestMapping(value = "toBookAdd")
	public ModelAndView toBookAdd(ModelAndView mav, HttpServletRequest request) {
		mav.setViewName("voucher/bookAdd");
		return mav;
	}
	/**
	 * 新增积分兑换代金券
	 */
	@ResponseBody
	@RequestMapping(value = "bookAdd")
	public  ReturnVo<Object> bookAdd(ModelAndView mav, HttpServletRequest request,Voucher voucher) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			
			voucher.setVoucherType("2");//代金券类型: 1.积分兑换代金券 2.预订返代金券 3.评论/分享获得抽奖代金券 
			voucherService.insertSelective(voucher);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}


	@RequestMapping(value = "toBookEdit")
	public ModelAndView toBookEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {

		if (id == null || "".equals(id)) {
			throw new Exception();
		}
		
		Voucher voucher = voucherService.selectByPrimaryKey(Long.parseLong(id));
		
		mav.addObject("voucher", voucher);
		mav.setViewName("voucher/bookEdit");
		return mav;
	}

	@RequestMapping(value = "bookEdit")
	public @ResponseBody ReturnVo<Object> bookEdit(ModelAndView mav, HttpServletRequest request, Voucher voucher) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			voucher.setModifyTime(new Date());
			voucherService.updateByPrimaryKeySelective(voucher);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	
	@ResponseBody
	@RequestMapping(value = "deleteBook")
	public  ReturnVo<Object> deleteBook(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			voucherService.deleteByPrimaryKey(Long.parseLong(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	
	
	//以下是评论，分享抽奖代金券
	
	@ResponseBody
	@RequestMapping(value = "drawPage")
	public GridVo drawPage(@RequestBody RequestGrid<Voucher> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		Page<Voucher> page = new Page<Voucher>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<Voucher> results = voucherService.queryDrawPage(page);
		vo.setRows(results);//总共有多少页
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	@RequestMapping(value = "toDrawAdd")
	public ModelAndView toDrawAdd(ModelAndView mav, HttpServletRequest request) {
		mav.setViewName("voucher/drawAdd");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "drawAdd")
	public  ReturnVo<Object> drawAdd(ModelAndView mav, HttpServletRequest request,Voucher voucher) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			
			voucher.setVoucherType("3");//代金券类型: 1.积分兑换代金券 2.预订返代金券 3.评论/分享获得抽奖代金券 
			voucherService.insertSelective(voucher);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}


	@RequestMapping(value = "toDrawEdit")
	public ModelAndView toDrawEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {

		if (id == null || "".equals(id)) {
			throw new Exception();
		}
		
		Voucher voucher = voucherService.selectByPrimaryKey(Long.parseLong(id));
		
		mav.addObject("voucher", voucher);
		mav.setViewName("voucher/drawEdit");
		return mav;
	}

	@RequestMapping(value = "drawEdit")
	public @ResponseBody ReturnVo<Object> drawEdit(ModelAndView mav, HttpServletRequest request, Voucher voucher) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			voucher.setModifyTime(new Date());
			voucherService.updateByPrimaryKeySelective(voucher);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	
	@ResponseBody
	@RequestMapping(value = "deleteDraw")
	public  ReturnVo<Object> deleteDraw(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			voucherService.deleteByPrimaryKey(Long.parseLong(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 检查账号名是否相同
	 * @Author zjz Create on 2016年10月28日 下午3:33:02
	 * @param request
	 * @param userAccount
	 * @param uid
	 * @return
	 */
	@RequestMapping(value = "checkDrawProbability")
	public @ResponseBody Boolean checkDrawProbability(HttpServletRequest request, String drawProbability, Long id) {
		try {
			BigDecimal p = new BigDecimal(drawProbability);
			List<Voucher> selectDrawVoucher = voucherService.selectDrawVoucher();
			BigDecimal temp=BigDecimal.ZERO;
			for(Voucher v:selectDrawVoucher){
				if(id!=null&&id==v.getId()){
					continue;
				}
				BigDecimal drawProbability2 = v.getDrawProbability();
				if(drawProbability2 == null){
					drawProbability2=BigDecimal.ZERO;
				}
				temp = temp.add(drawProbability2);
			}
			temp=temp.add(p);
			int compareTo = BigDecimal.valueOf(100).compareTo(temp);
			if(compareTo>=0){
				//成功
				return true;
			}else{
				//失败
				return false;
			}
		} catch (Exception e) {

		}
		return false;
	}
	
}
