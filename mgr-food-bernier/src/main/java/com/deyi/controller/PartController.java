package com.deyi.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Part;
import com.deyi.entity.Store;
import com.deyi.entity.SysUser;
import com.deyi.service.PartService;
import com.deyi.service.StoreService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;
/**
 * 
 * @author tom
 * 门店活动
 *
 */
@Controller
@RequestMapping(value = "part")
public class PartController extends Component<Part> {
	private Logger log = LoggerFactory.getLogger(PartController.class);
	@Autowired
	private PartService partService;
	@Autowired
	private StoreService storeService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("part/partList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<Part> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		Page<Part> page = new Page<Part>();
		GridVo vo = new GridVo();
		SysUser user = getSessionUser(request);
		if(user==null){
			return vo;
		}
		if(!user.getType().equals("1")){//只查询本门店的员工
			Part object = reqGrid.getObject();
			object.setStoreId(user.getStoreId());
			reqGrid.setObject(object);
		}
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		
		setParams(request, reqGrid.getObject(), page);
		List<Part> results = partService.queryPage(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	@RequestMapping(value = "toPartAdd")
	public ModelAndView toPartAdd(ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
		SysUser user = getSessionUser(request);
		if(user!=null){
			if(!user.getType().equals("1")){
				Store store = storeService.selectByPrimaryKey(user.getStoreId());
				List<Store> stores=new ArrayList<Store>() ;
				stores.add(store);
				mav.addObject("stores", stores);
			}else{
				List<Store> stores = storeService.queryAllStore();
				mav.addObject("stores", stores);
			}
		}
		mav.setViewName("part/partAdd");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "partAdd")
	public  ReturnVo<Object> partAdd(ModelAndView mav, HttpServletRequest request,Part part) {
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			part.setStatus("1");
			partService.insertSelective(part);
			

		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}


	@RequestMapping(value = "toPartEdit")
	public ModelAndView toPartEdit(String id, String storeId,ModelAndView mav, HttpServletRequest request) throws Exception {
		// 获取Session中的用户信息,
		SysUser user = getSessionUser(request);
		Store store1 = storeService.selectByPrimaryKey(Integer.parseInt(storeId));
		if(user!=null){
			if(!user.getType().equals("1")){
				Store store = storeService.selectByPrimaryKey(user.getStoreId());
				List<Store> stores=new ArrayList<Store>() ;
				stores.add(store);
				mav.addObject("stores", stores);
			}else{
				List<Store> stores = storeService.queryAllStore();
				mav.addObject("stores", stores);
			}
		}
		Part part = partService.selectByPrimaryKey(Integer.parseInt(id));
		mav.addObject("part", part);
		mav.addObject("store1", store1);
		mav.setViewName("part/partEdit");
		return mav;
	}

	@RequestMapping(value = "partEdit")
	public @ResponseBody ReturnVo<Object> partEdit(ModelAndView mav, HttpServletRequest request, Part part) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			partService.updateByPrimaryKeySelective(part);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 删除商品
	 */
	@ResponseBody
	@RequestMapping(value = "deletePart")
	public  ReturnVo<Object> deleteDeliverMan(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			partService.deleteByPrimaryKey(Integer.parseInt(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 禁用
	 * @Author zjz Create on 2016年10月21日 下午2:06:03
	 * @param reqGrid
	 * @param mav
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableState")
	public  ReturnVo<Object> disableState(HttpServletRequest request,String id) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品禁用Start......");
		try {
			Part part = partService.selectByPrimaryKey(new Integer(id));
			part.setStatus("2");//禁用
			partService.updateByPrimaryKeySelective(part);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 启用
	 */
	@ResponseBody
	@RequestMapping(value = "enableState")
	public  ReturnVo<Object> enableState(String id, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			Part part = partService.selectByPrimaryKey(new Integer(id));
			part.setStatus("1");//禁用
			partService.updateByPrimaryKeySelective(part);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private String GetFileDir(String... dir) {
		String dirpath = "";
		String separator = File.separator;// \
		for (int i = 0; i < dir.length; i++) {
			dirpath += separator + dir[i];
		}
		dirpath = dirpath + separator;
		return dirpath;
	}
	
	
}
