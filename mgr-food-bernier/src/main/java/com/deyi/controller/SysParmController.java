package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.SysParm;
import com.deyi.entity.SysUser;
import com.deyi.service.SysParmService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.vo.ReturnVo;
import com.google.gson.Gson;

/**
 * 会员信息（顾客）
 * @author zjz
 *
 */
@Controller
@RequestMapping(value ="system")
public class SysParmController extends Component<SysParm> {
	private Logger log = LoggerFactory.getLogger(SysParmController.class);

	@Autowired
	private SysParmService sysParmService;
		/**
	 * 系统参数显示
	 * @param mav
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "showParam")
	public ModelAndView showParam(HttpServletRequest request,ModelAndView mav){
		List<SysParm> paramList  = sysParmService.selectAll();
		if(!paramList.isEmpty()){
			SysParm sysParm = paramList.get(0);
			if(sysParm.getAdvertImage().contains(",")){
				String[] str = sysParm.getAdvertImage().split(",");
				mav.addObject("picS", new Gson().toJson(str));
			}
			mav.addObject("sysParm", sysParm);
		}else{
			mav.addObject("picS", "1");//对象为空,给个默认的图片状态为1
		}
		mav.setViewName("system/showParm");
		return mav;
	}
	
	@RequestMapping(value = "saveParam")
	public @ResponseBody ReturnVo<Object> saveParam(ModelAndView mav,HttpServletRequest request,SysParm sysParm,String startTime1,String endTime1){
		log.info("显示保存参数列表Start......");
		ReturnVo<Object> vo = new ReturnVo<>();
		try{
			SysUser sysUser = (SysUser)request.getSession().getAttribute(Constants.SESSION_USERINFO);
			Integer uid = sysUser.getUid();//用户id
			//判断是插入还是更新
			List<SysParm> paramList  = sysParmService.selectAll();
			if(paramList.isEmpty()){
				sysParm.setUid(uid);//设置用户id
				sysParmService.insertSelective(sysParm);
			}else{
				sysParmService.updateByPrimaryKeySelective(sysParm);
			}
		}catch(Exception e){
			log.error("异常",e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	




}
