package com.deyi.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Chief;
import com.deyi.entity.DeliverMan;
import com.deyi.entity.MemberAddress;
import com.deyi.entity.OrderGoods;
import com.deyi.entity.RechargeRecord;
import com.deyi.entity.Store;
import com.deyi.entity.SysOrder;
import com.deyi.entity.SysUser;
import com.deyi.service.ChiefService;
import com.deyi.service.DeliverManService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.RechargeRecordService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.DateUtils;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.MercollectdayVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.dy.util.PropertiesUtil;
import com.dy.util.StringUtil;
import com.google.gson.Gson;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "sysOrder")
public class SysOrderContorller extends Component<SysOrder>{
	private Logger log = LoggerFactory.getLogger(SysOrderContorller.class);
	@Autowired
	private SysOrderService sysOrderService;
	@Autowired
	private MemberAddressService memberAddressService;
	@Autowired
	private DeliverManService deliverManService;
	
	@Autowired
	private ChiefService chiefService;
	@Autowired
	RechargeRecordService rechargeRecordService;
	@Autowired
	private StoreService storeService;
	
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request) {
		statOrderStatusEqUn(mav);//统计订单状态等于未处理的数量
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		mav.addObject("sysUser", sysUser);
		mav.setViewName("sysOrder/sysOrderList");
		return mav;
	}
	

	/**
	 * 订单列表 分页
	 */
	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<SysOrder> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("订单列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId((long)sysUser.getStoreId());
		}
		
		Page<SysOrder> page = new Page<SysOrder>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		
		List<SysOrder> results = sysOrderService.queryPage(page);
		int statOrderStatusEqUntreated = sysOrderService.statOrderStatusEqUntreated();//统计订单状态为未处理 
		for(SysOrder sysOrder:results){
			String payNumber = sysOrder.getPayNumber();
			String property = PropertiesUtil.getProperty("order_pay_no_prifix");
			if(payNumber.contains(property)){
				String replace = StringUtils.replace(payNumber, property, "");
				sysOrder.setPayNumber(replace);
			}
		}
		
		
		vo.setStatOrderStatusEqUntreated(statOrderStatusEqUntreated);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		
		return vo;
	}

	
	
	/**
	 * 订单详情
	 */
	@RequestMapping(value = "orderDetails")
	public ModelAndView orderDetails(HttpServletRequest request,ModelAndView mav, String orderId) {
		SysOrder sysOrder = sysOrderService.queryOrderById2(Long.parseLong(orderId));
		
		String payNumber = sysOrder.getPayNumber();//支付流水号
		
		String property = PropertiesUtil.getProperty("order_pay_no_prifix");
		if(payNumber.contains(property)){
			String replace = StringUtils.replace(payNumber,property, "");
			sysOrder.setPayNumber(replace);
		}
		
		if(sysOrder!=null){
			Integer addresid = sysOrder.getAddressId();
			if(addresid != null){
				MemberAddress memberAddress = memberAddressService.selectByPrimaryKey(addresid);
				mav.addObject("memberAddress",memberAddress);
			}
		}
		mav.addObject("sysOrder", sysOrder); 
		mav.setViewName("sysOrder/orderDetails");
		return mav;
	}
      /**
       * 
       * @Title: toChiefDishtml 
       * @Description: 弹出分配厨师
       * @param @param mav
       * @param @param request
       * @param @return    设定文件 
       * @return ModelAndView    返回类型 
       * @throws
       */
	  @RequestMapping(value = "toChiefDis")
		public ModelAndView toChiefDis(ModelAndView mav, HttpServletRequest request,String orderId) {
		    //根据订单获取门店id
		    SysOrder sysorder=sysOrderService.queryOrderById2(Long.valueOf(orderId));
		    //根据门店id查询厨师列表
			List<Chief> chiefs=chiefService.queryAllByStoreId(sysorder.getStoreId().intValue());
			mav.addObject("chiefs", chiefs);
			mav.addObject("sysorder", sysorder);
			mav.setViewName("sysOrder/chiefDis");
			return mav;
		}
	  
	  	@ResponseBody
		@RequestMapping(value = "chiefDis")
		public  ModelAndView chiefDis(ModelAndView mav, HttpServletRequest request,SysOrder sysOrder) {
	    	ReturnVo<Object> vo = new ReturnVo<Object>();
			try {
				//如是到店訂單直接變成 8 配送完成
				if("4".equals(sysOrder.getOrderType()) || "3".equals(sysOrder.getOrderType())){
					sysOrder.setOrderStatus("8");
				}else{
					sysOrder.setOrderStatus("10");
				}
				//更新廚師
				sysOrderService.updateByPrimaryKeySelective(sysOrder);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				vo.setSuccess(false);
				vo.setMessage("分配失败");
			}
			
			return mav;
	    }
	
	/**
	 * 确认订单
	 */
	@RequestMapping(value = "confirm")
	public @ResponseBody ReturnVo<Object> confirm(String orderId, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<>();
		try {
			
			String message = sysOrderService.confirmOrder(Long.parseLong(orderId));
			
			
			if ("success".equals(message)) {
				statOrderStatusEq0ReturnVo(vo);
				vo.setSuccess(true);
			} else {
				vo.setSuccess(false);
				vo.setMessage("Abnormal operation!");
			}
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation!");
		}
		return vo;
	}



	/**
	 * 开始配送
	 */
	@RequestMapping(value = "toStartDelivery")
	public ModelAndView toStartDelivery(HttpServletRequest request,ModelAndView mav, String orderId) {
		SysOrder sysOrder = sysOrderService.selectByPrimaryKey(Long.parseLong(orderId));
		mav.addObject("sysOrder", sysOrder); 
		mav.setViewName("sysOrder/startDelivery");
		return mav;
	}
	
	/**
	 * 配送完成
	 */
	@RequestMapping(value = "endDelivery")
	public @ResponseBody ReturnVo<Object> endDelivery(String orderId, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<>();
		try {
			SysOrder sysOrder = sysOrderService.selectByPrimaryKey(Long.parseLong(orderId));
			sysOrder.setOrderStatus("8");//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成  
		    sysOrder.setDeliverEndTime(new Date());
			sysOrderService.updateByPrimaryKeySelective(sysOrder);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation!");
		}
		return vo;
	}
	
	/**
	 * 确认收货 (注释了调用微信端接口)
	 */
//	@RequestMapping(value = "sureTakeDelivery")
//	public @ResponseBody ReturnVo<Object> sureTakeDelivery(String orderId, HttpServletRequest request) {
//		ReturnVo<Object> vo = new ReturnVo<>();
//		try {
//			
//			
//			
////			SysOrder sysOrder = sysOrderService.selectByPrimaryKey(Long.parseLong(orderId));
////			Integer memberId = sysOrder.getMemberId();
//			
////			sysOrder.setOrderStatus("4");//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成  
////		    sysOrderService.updateByPrimaryKeySelective(sysOrder);
//		} catch (Exception e) {
//			log.error("异常", e);
//			vo.setSuccess(false);
//			vo.setMessage("Abnormal operation!");
//		}
//		return vo;
//	}
	
	/**
	 * 跳转打印页面
	 */
	@RequestMapping(value = "print")
	public ModelAndView print(ModelAndView mav, HttpServletRequest request,String orderId) {
		SysOrder sysOrder = sysOrderService.queryOrderById2(Long.parseLong(orderId));
		Integer quantify=0;
		List<OrderGoods> orderGoods = sysOrder.getOrderGoods();
		if(!orderGoods.isEmpty()){
			for(OrderGoods o:orderGoods){
				quantify += o.getQuantify();
			}
		}
		
		mav.addObject("quantify",quantify);
		
		String payNumber = sysOrder.getPayNumber();//支付流水号
		String property = PropertiesUtil.getProperty("order_pay_no_prifix");
		if(payNumber.contains(property)){
			String replace = StringUtils.replace(payNumber,property, "");
			sysOrder.setPayNumber(replace);
		}
		
		mav.addObject("sysOrder",sysOrder);
		mav.setViewName("sysOrder/ifoom");
		return mav;
	}
	@RequestMapping(value = "toChart")
	public ModelAndView toChart(ModelAndView mav, HttpServletRequest request) {
		String flag="";
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			flag="2";
		}else{
			flag="1";
		}
		//查询所有门店
		List<Store> storeList=new ArrayList<Store>();
		List<Store> storeList1=storeService.queryAllStore();
		Store stores=new Store();
		stores.setId(null);
		stores.setStoreName("全部");
		storeList.add(stores);
		for (Store Store1 : storeList1) {
			storeList.add(Store1);
		}
		mav.addObject("storeList",storeList);
		mav.addObject("flag",flag);
		mav.setViewName("statGoodsSales/chart");
		return mav;
	}
	public static void main(String[] args) throws Exception {
		List<String> list=DateUtils.getdayDatetoDate(DateUtils.get7dayDate(), new Date());
		for (String string : list) {
			System.out.println(string);
		}
}
	/**
	 * 统计图表
	 */
	@RequestMapping(value = "newChart")
	public ModelAndView newChart(ModelAndView mav, HttpServletRequest request, MercollectdayVo vo) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		// 按天分组  统计订单数和销售额
		List<SysOrder> listOrderSale=null;
		// 按天分组  统计充值金额
		List<RechargeRecord> listRecharge=null;
		Long storeId=null;
		String data[]=null;
		String flag="";
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			storeId=Long.valueOf(sysUser.getStoreId());
			data= new String[]{"订单数","销售额"};
			flag="2";
		}else{
			if(StringUtil.isBlank(vo.getStoreid())){
				storeId=null;
			}else{
				storeId=Long.valueOf(vo.getStoreid()).longValue();
			}
			data=new String[]{"订单数","销售额","充值金额"};
			flag="1";
		}
		try {
			//如果没有输入开始和结束 时间默认 开始时间为当前时间 七天前
			if(StringUtil.isBlank(vo.getQuerystarttime()) && StringUtil.isBlank(vo.getQueryendtime())){
				listOrderSale=sysOrderService.statOrderNumber(DateUtils.get7dayDate(), new Date(),storeId);
				listRecharge=rechargeRecordService.statRechargeNumber(DateUtils.get7dayDate(), new Date());
			}
			
			else if(StringUtil.isBlank(vo.getQuerystarttime())){
				listOrderSale=sysOrderService.statOrderNumber(DateUtils.get7dayDate(), sdf.parse(vo.getQueryendtime()),storeId);
				listRecharge=rechargeRecordService.statRechargeNumber(DateUtils.get7dayDate(), sdf.parse(vo.getQueryendtime()));
			}
			
			else if(StringUtil.isBlank(vo.getQueryendtime())){
				listOrderSale=sysOrderService.statOrderNumber(sdf.parse(vo.getQuerystarttime()),new Date(),storeId);
				listRecharge=rechargeRecordService.statRechargeNumber(sdf.parse(vo.getQuerystarttime()),new Date());
			}else{
				listOrderSale=sysOrderService.statOrderNumber(sdf.parse(vo.getQuerystarttime()),sdf.parse(vo.getQueryendtime()),storeId);
				listRecharge=rechargeRecordService.statRechargeNumber(sdf.parse(vo.getQuerystarttime()),sdf.parse(vo.getQueryendtime()));
			}
			for (SysOrder orderSale : listOrderSale) {
				Map<String,Object> map =new HashMap<String,Object>();
				for (RechargeRecord rechargeRecord : listRecharge) {
					if(sdf.format(rechargeRecord.getCreateTime()).equals(sdf.format(orderSale.getPayTime()))){
						map.put("rechargeMoney", rechargeRecord.getRechargeMoney());
					}
				}
			map.put("time",sdf.format(orderSale.getPayTime()) );
			map.put("orderNo", orderSale.getStoreOderSum());
			map.put("saleMoney", orderSale.getStoreSalesMony());
			list.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mav.addObject("data",new Gson().toJson(data));
		mav.addObject("flag",new Gson().toJson(flag));
		mav.addObject("dataList", new Gson().toJson(list));
		mav.setViewName("statGoodsSales/newChart");
		return mav;
	}
	/**
	 * 配送员二维码解码
	 * @Author zjz Create on 2016年11月1日 下午7:54:53
	 * @param session
	 * @param code
	 * @param mav
	 * @return
	 */
	@RequestMapping(value = "scancodeSubmit")
	public @ResponseBody ReturnVo<Object> scancodeSubmit(HttpSession session, String code,String orderId ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			byte[] decodeBase64 = Base64.decodeBase64(code.getBytes());//获取编码然后加密得到的是一个数组
			String string = new String(decodeBase64);
			String[] split = StringUtils.split(string, ",");
			if (split.length != 2) {
				vo.setSuccess(false);
				vo.setMessage("二维码解析错误");
				return vo;
			}
			String deliverManId = split[0];
			String uuid = split[1];//bbec44c5b560451a85c16bb0b3885e4b
			if(!Constants.DELIVERMAN_UUID_KEY.equals(uuid)){//bbec44c5b560451a85c16bb0b3885e4b
				vo.setSuccess(false);
				vo.setMessage("二维码解析错误");
				return vo;
			}
			DeliverMan deliverMan = deliverManService.selectByPrimaryKey(new Integer(deliverManId));
			if(deliverMan == null){
				vo.setSuccess(false);
				vo.setMessage("配送员信息有误");
				return vo;
			}
			if("2".equals(deliverMan.getStatus())){//配送员状态：1启用，2停用  
				vo.setSuccess(false);
				vo.setMessage("该配送员被禁用了");
				return vo;
			}
			SysOrder sysOrder = sysOrderService.queryOrderById2(Long.parseLong(orderId));
			if(sysOrder == null){
				vo.setSuccess(false);
				vo.setMessage("订单信息有误");
				return vo;
			}
			
			sysOrder.setDeliverManId(Integer.parseInt(deliverManId));
			sysOrder.setOrderStatus("3");//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6订单取消 7配送员接单 8配送完成 9订单已评价  
			sysOrder.setDeliverStartTime(new Date());
			sysOrderService.updateByPrimaryKeySelective(sysOrder);
			
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}






	
}
