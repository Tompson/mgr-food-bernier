package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.CategoryGoods;
import com.deyi.entity.Goods;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.util.Component;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;
@Controller
@RequestMapping(value="category")
public class CategoryController extends Component<CategoryGoods> {
	private Logger log = LoggerFactory.getLogger(GoodsController.class);
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	@Autowired
	private GoodsService goodsService;
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("category/categoryList");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<CategoryGoods> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("类别列表Start......");
		Page<CategoryGoods> page = new Page<CategoryGoods>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<CategoryGoods> results = categoryGoodsService.queryPage(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}
	@RequestMapping(value="tocategoryAdd")
	public ModelAndView tocategoryAdd(ModelAndView mav,HttpServletRequest request) throws Exception {
		mav.setViewName("category/categoryAdd");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "categoryAdd")
	public ReturnVo<Object> categoryAdd(CategoryGoods categoryGoods,ModelAndView mav ,HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			
			List<CategoryGoods> clist=categoryGoodsService.getCategoryByCategoryName(categoryGoods.getCgoodsName());
			if(clist.size()>0){
				vo.setSuccess(false);
				vo.setMessage("分类名称重复");
				return vo;
			}
			categoryGoodsService.insertSelective(categoryGoods);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMessage("添加失败");
			return vo;
		}
		
		return vo;
	}
	@RequestMapping(value = "toCategoryEdit")
	public ModelAndView toCategoryEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {
		CategoryGoods category = categoryGoodsService.selectByPrimaryKey(Long.parseLong(id));
		mav.addObject("category", category);
		mav.setViewName("category/categoryEdit");
		return mav;
	}
	
	@RequestMapping(value = "categoryEdit")
	public @ResponseBody ReturnVo<Object> categoryEdit(ModelAndView mav, HttpServletRequest request, CategoryGoods categoryGoods) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			CategoryGoods categoryGoods1 =categoryGoodsService.selectByPrimaryKey(categoryGoods.getCategoryId());
			List<CategoryGoods> clist=categoryGoodsService.getCategoryByCategoryName(categoryGoods.getCgoodsName());
		if(clist.size()>0 && !categoryGoods1.getCgoodsName().equals(categoryGoods.getCgoodsName())){//第一个是它本身
			vo.setSuccess(false);
			vo.setMessage("分类名称重复");
			return vo;
		}
			categoryGoodsService.updateByPrimaryKey(categoryGoods);
	   } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMessage("编辑失败");
			return vo;
	   }
		return vo;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "deleteCategory")
	public  ReturnVo<Object> deleteCategory(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			//判断分类下面 是否有菜品
			List<Goods>  goods=goodsService.selectByPrimaryCategoryId( Long.parseLong(id));
			if(goods.size()!=0){
				vo.setMessage("该分类下面存在菜品，不能删除！");
				vo.setSuccess(false);
				return vo;
			}
			categoryGoodsService.deleteByPrimaryKey(Long.parseLong(id));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	/**
	 * 禁用
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableIntegral")
	public  ReturnVo<Object> disableIntegral(HttpServletRequest request,String id) {// 菜品状态: 1启用 2禁用    
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			CategoryGoods category = categoryGoodsService.selectByPrimaryKey(Long.parseLong(id));
			category.setStatus("2");
			categoryGoodsService.updateByPrimaryKeySelective(category);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	/**
	 * 禁用
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "enableIntegral")
	public  ReturnVo<Object> enableIntegral(HttpServletRequest request,String id) {// 菜品状态: 1启用 2禁用    
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			CategoryGoods category = categoryGoodsService.selectByPrimaryKey(Long.parseLong(id));
			category.setStatus("1");
			categoryGoodsService.updateByPrimaryKeySelective(category);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	@RequestMapping(value = "upMove")
	@ResponseBody
	public  ReturnVo<Object> upMove(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			vo = categoryGoodsService.moveUp(id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	@RequestMapping(value = "downMove")
	@ResponseBody
	public  ReturnVo<Object> downMove(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			vo = categoryGoodsService.moveDown(id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	
	@RequestMapping(value = "goodsTop")
	@ResponseBody
	public  ReturnVo<Object> goodsTop(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			vo = categoryGoodsService.toTop(id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	
	
	
	
}
