/**  
* @Title: AliPayService.java
* @Package com.deyi.service.uti
* @Description: 深圳市得壹科技有限公司
*/
package com.deyi.controller.pay.aliPay;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.mortbay.log.Log;

import com.deyi.controller.pay.RSA;
import com.deyi.controller.pay.aliPay.util.AlipayCore;
import com.deyi.util.PropertiesUtil;
import com.deyi.util.SignUtils;

/**
 * @Description: TODO
 * @author gongz
 * @date 2015年12月17日
 *
 */
public class AliPayService {

	/*public PayInfo doPay(AliPublicWithBLOBs aliCustInfo, Order order) throws Exception {

		PropertiesUtil pro = new PropertiesUtil();

		String service = pro.getProperty("ali.pay.wap");
		String notifyUrl = pro.getProperty("al_notify_h5");
		String body = pro.getProperty("wx_api_name");
		int total = 0;
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", service);
		sParaTemp.put("partner", aliCustInfo.getPid());
		sParaTemp.put("seller_id", "15527462453");
		sParaTemp.put("_input_charset", AlipayConfig.input_charset);
		sParaTemp.put("payment_type", "1");
		sParaTemp.put("notify_url", notifyUrl);
		// sParaTemp.put("return_url", return_url);
		sParaTemp.put("out_trade_no", order.getOrderId() + "");
		sParaTemp.put("subject", body);
		if(order.getCouponFaceamt()==null){
			total = BigDecimal.valueOf(Double.valueOf(order.getTotalAmount())).intValue();
		}else{
			total = BigDecimal.valueOf(Double.valueOf(order.getTotalAmount())-order.getCouponFaceamt()).intValue();
		}
		sParaTemp.put("total_fee", BigDecimal.valueOf(Long.valueOf(total))
				.setScale(2, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(100)).toString() + "");
		// b=b.setScale(2, BigDecimal.ROUND_HALF_UP); //四舍五入
		// sParaTemp.put("show_url", show_url);
		sParaTemp.put("body", body);
		// sParaTemp.put("it_b_pay", it_b_pay);
		// sParaTemp.put("extern_token", extern_token);
		// ILLEGAL_SECURITY_PROFILE
		PayInfo payInfo = new PayInfo();
		payInfo.setSign(
				URLEncoder.encode(SignUtils.sign(SignUtils.encode(sParaTemp), aliCustInfo.getPkey().trim()), "utf-8"));
		payInfo.setSign_type(AlipayConfig.sign_type);
		AliPayResult aliPayResult = new AliPayResult();
		aliPayResult.set_input_charset(AlipayConfig.input_charset);
		aliPayResult.setService(service);
		aliPayResult.setPartner(aliCustInfo.getPid());
		aliPayResult.setSeller_id("15527462453");
		aliPayResult.setPayment_type("1");
		aliPayResult.setNotify_url(notifyUrl);
		aliPayResult.setOut_trade_no(order.getOrderId());
		aliPayResult.setSubject(body);
		
		aliPayResult.setTotal_fee(BigDecimal.valueOf(Long.valueOf(total))
				.setScale(2, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(100)).toString() + "");
		aliPayResult.setBody(body);
		payInfo.setAliPayResult(aliPayResult);
		return payInfo;

	}*/

	

	private static Map<String, String> buildRequestPara(Map<String, String> sParaTemp, String pkey) {
		// 除去数组中的空值和签名参数
		Map<String, String> sPara = AlipayCore.paraFilter(sParaTemp);
		// 生成签名结果
		String mysign = buildRequestMysign(sPara, pkey);

		// 签名结果与签名方式加入请求提交参数组中
		sPara.put("sign", mysign);
		sPara.put("sign_type", AlipayConfig.sign_type);
		return sPara;
	}

	private static String buildRequestParaOpenApi(Map<String, String> sParaTemp, String pkey) {
		// 除去数组中的空值和签名参数
		Map<String, String> sPara = AlipayCore.paraFilter(sParaTemp);
		// 生成签名结果
		String mysign = buildRequestMysign(sPara, pkey);

		sPara.put("sign", mysign);
		return mysign;
	}

	/**
	 * 生成签名结果
	 * 
	 * @param sPara
	 *            要签名的数组
	 * @return 签名结果字符串
	 */
	public static String buildRequestMysign(Map<String, String> sPara, String pkey) {

		String prestr = AlipayCore.createLinkString(sPara);

		String mysign = "";
		if (AlipayConfig.sign_type.equals("RSA")) {
			mysign = RSA.sign(prestr, pkey, AlipayConfig.input_charset);
		}
		return mysign;
	}

	/**
	 * 根据反馈回来的信息，生成签名结果
	 * 
	 * @param Params
	 *            通知返回来的参数数组
	 * @param sign
	 *            比对的签名结果
	 * @return 生成的签名结果
	 */
	public static boolean getSignVeryfy(Map<String, String> Params, String sign) {
		// 过滤空值、sign与sign_type参数
		Map<String, String> sParaNew = AlipayCore.paraFilter(Params);
		// 获取待签名字符串
		String preSignStr = AlipayCore.createLinkString(sParaNew);
		// 获得签名验证结果
		boolean isSign = false;
		if (AlipayConfig.sign_type.equals("RSA")) {
			isSign = RSA.verify(preSignStr, sign, AlipayConfig.ali_public_key, AlipayConfig.input_charset);
		}
		return isSign;
	}

}
