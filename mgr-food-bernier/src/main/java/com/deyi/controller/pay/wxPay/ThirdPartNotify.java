/**
 * 
 */
package com.deyi.controller.pay.wxPay;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;
import com.deyi.controller.pay.aliPay.util.AlipayNotify;
import com.deyi.util.ConstantsPay;
import com.deyi.util.DateUtils;
import com.deyi.util.HttpClient;
import com.deyi.util.StringUtils;
import com.deyi.vo.SendMsg;

/**
 * 支付回调处理
 * @author hejx
 * @2016年4月11日
 */
@Controller
@RequestMapping(value="api/notify")
public class ThirdPartNotify implements ServletContextAware {
	private ServletContext servletContext = null;
	private Logger log = LoggerFactory.getLogger(ThirdPartNotify.class);
	
	/*@Autowired
	private INotifyPayService notifyPayService;
	@Autowired
	private OrderMapper orderDao;*/
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	/**
	 * 微信支付回调
	 * @param mav
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="wx_unifed")
	public void wx_unifed(ModelAndView mav,HttpServletRequest request,HttpServletResponse response) {
		log.info("process in wx_unifed...");
		response.setContentType("text/html; charset=utf-8");
		
		PrintWriter print = null;
		try {
			print = response.getWriter();
		} catch (IOException e1) {
			log.error("初始PrintWriter异常...",e1);
			return ;
		}
		try{
			String xml = HttpClient.getContext(request);
			log.info("wx统一下单支付接口返回xml:"+xml);
			
//			String returnXml = notifyPayService.wxUnifed(xml);

//			print.print(returnXml);
		}catch(Exception e){
			log.error("系统异常",e);
			print.print(ConstantsPay.getFormatNotfyFail("系统错误"));
		}
		
	}
	/**
	 * 支付寶支付回调
	 * @param mav
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="aliPaynotifyURL")
  /* public void aliPaynotifyURL(ModelAndView mav,HttpServletRequest request,HttpServletResponse response) {
	        // 获取支付宝POST过来反馈信息
	       Map<String, String> params = new HashMap<String, String>();
	       log.info("支付宝支付结果通知返回+++++++++++++");
	       PrintWriter out = null;
	         try {
	             // out = ServletActionContext.getResponse().getWriter();
	            out =response.getWriter();
	        } catch (IOException e) {
	           e.printStackTrace();
	        }
	         Map requestParams = request.getParameterMap();
	        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
	           String name = (String) iter.next();
	            String[] values = (String[]) requestParams.get(name);
	            String valueStr = "";
	             for (int i = 0; i < values.length; i++) {
	                 valueStr = (i == values.length - 1) ? valueStr + values[i]
	                         : valueStr + values[i] + ",";
	             }
	             // 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
	            // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "UTF-8");
	             params.put(name, valueStr);
	         }
	 
	         // 判断responsetTxt是否为ture，生成的签名结果mysign与获得的签名结果sign是否一致
	        // responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
	         // mysign与sign不等，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
	        
	         boolean responseTxt = AlipayNotify.verify(params);
	         log.info("支付宝支付结果通知返回+++++++++++++params"+params);
	        String trade_status = request.getParameter("trade_status"); // 交易状态
	        log.info("支付宝支付结果通知返回+++++++++++++responseTxt"+responseTxt);
	        if (responseTxt==true) {// 验证成功
	           // 请在这里加上商户的业务逻辑程序代码
	        	 String out_trade_no = request.getParameter("out_trade_no"); // 交易訂單號
	        	 String trade_no = request.getParameter("trade_no ");
	        	 String gmt_payment = request.getParameter("gmt_payment"); //支付时间
	        	 String buyer_id = request.getParameter("buyer_id"); // 支付账号
	        	 log.info("支付宝支付结果通知返回+++++++++++++"+buyer_id);
	        	 Order order = new Order();
					order.setOrderId(out_trade_no);
					order.setOrderStatus("4");
					order.setPayTime(gmt_payment);
					order.setPayStatus("2");
					order.setPayTradeNo(trade_no);//支付宝交易订单号
					order.setIsdisplay("0");//是否隐藏 0表示不隐藏 1表示隐藏
					log.info("支付宝支付结果通知返回================total_fee"+request.getParameter("total_fee"));
					log.info("支付宝支付结果通知返回================total_fee"+Integer.parseInt(StringUtils.changeY2F(request.getParameter("total_fee"))));
					order.setPayAmount(Integer.parseInt(StringUtils.changeY2F(request.getParameter("total_fee"))));
					//1 支付宝  2 微信
					order.setPayType("1");
					order.setPayAccount(buyer_id);
					orderDao.updateByPrimaryKeySelective(order);
					
					orderDao.updateByPrimaryKeySelective(order);
					Order o = orderDao.queryByOrderInfoId(out_trade_no);
					if(o!=null){
						order.setContactsPerson(o.getContactsPerson());
					}
					//订单交易成功推送
					//调用Dwr推
					SendMsg.sendMsg(order,servletContext);
	           if (trade_status.equals("WAIT_BUYER_PAY")) {
	                // 该判断表示买家已在支付宝交易管理中产生了交易记录，但没有付款
	 
	                // 判断该笔订单是否在商户网站中已经做过处理（可参考“集成教程”中“3.4返回数据处理”）
	                // 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
	               // 如果有做过处理，不执行商户的业务程序
	               // 不做处理
	                out.println("success"); // 请不要修改或删除
	            } else if (trade_status.equals("WAIT_SELLER_SEND_GOODS")) {
	                 // 该判断表示买家已在支付宝交易管理中产生了交易记录且付款成功，但卖家没有发货
	 
	                 // 判断该笔订单是否在商户网站中已经做过处理（可参考“集成教程”中“3.4返回数据处理”）
	                // 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
	                // 如果有做过处理，不执行商户的业务程序
	
	                     
	                out.println("success"); // 请不要修改或删除
	           } else if (trade_status.equals("WAIT_BUYER_CONFIRM_GOODS")) {
	                // 该判断表示卖家已经发了货，但买家还没有做确认收货的操作
	
	                // 判断该笔订单是否在商户网站中已经做过处理（可参考“集成教程”中“3.4返回数据处理”）
	                // 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
	                 // 如果有做过处理，不执行商户的业务程序
	 
	                out.println("success"); // 请不要修改或删除
	             } else if (trade_status.equals("TRADE_FINISHED")) {
	               // 该判断表示买家已经确认收货，这笔交易完成
	
	                // 判断该笔订单是否在商户网站中已经做过处理（可参考“集成教程”中“3.4返回数据处理”）
	                 // 如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
	                 // 如果有做过处理，不执行商户的业务程序
	
	
	                 out.println("success"); // 请不要修改或删除
	             } else {
	                 out.println("success"); // 请不要修改或删除
	             }
	         }else {// 验证失败
	             out.println("fail");
	         }
	     }*/
	
	 /**  
     * 将元为单位的转换为分 替换小数点，支持以逗号区分的金额 
     *  
     * @param amount 
     * @return 
     */  
    public static String changeY2F(String amount){  
        String currency =  amount.replaceAll("\\$|\\￥|\\,", "");  //处理包含, ￥ 或者$的金额  
        int index = currency.indexOf(".");  
        int length = currency.length();  
        Long amLong = 0l;  
        if(index == -1){  
            amLong = Long.valueOf(currency+"00");  
        }else if(length - index >= 3){  
            amLong = Long.valueOf((currency.substring(0, index+3)).replace(".", ""));  
        }else if(length - index == 2){  
            amLong = Long.valueOf((currency.substring(0, index+2)).replace(".", "")+0);  
        }else{  
            amLong = Long.valueOf((currency.substring(0, index+1)).replace(".", "")+"00");  
        }  
        return amLong.toString();  
    }
}
