/**  
* @Title: WXJsApiPay.java
* @Package com.xdd.service.uti
* @Description: 深圳市得壹科技有限公司
*/ 
package com.deyi.controller.pay.wxPay;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import com.deyi.util.HttpClient;
import com.deyi.util.PayUtils;
import com.deyi.util.PropertiesUtil;
import com.deyi.util.StringUtils;
import com.thoughtworks.xstream.XStream;

/**
* @Description: TODO
* @author hejx
* @date 2015年12月13日 
*
*/
public class WXJsApiPay {

	private Logger log = Logger.getLogger(WXJsApiPay.class);
	
	private static XStream XSTREAM = new XStream();
	
	/*public WxResult doPay(WxPublic wxPublic,Order order) throws Exception {
		log.info("process in doPay...");
		Map<String,Object> map = new HashMap<String,Object>();
		DeyiPay pay = new DeyiPay();
		int total = 0;
		PropertiesUtil pro = new PropertiesUtil();
		String mchId = wxPublic.getMchId();
		if(order.getCouponFaceamt()==null){
			total = BigDecimal.valueOf(Double.valueOf(order.getTotalAmount())).intValue();
		}else{
			total = BigDecimal.valueOf(Double.valueOf(order.getTotalAmount())-order.getCouponFaceamt()).intValue();
		}
		String appkey = wxPublic.getApplicKey();
		String appid = wxPublic.getAppId();
		String subMchId = wxPublic.getMerchid();
		pay.setGoodsName(pro.getProperty("wx_api_name"));
		pay.setOrderId(String.valueOf(order.getOrderId()));
		pay.setAmount(total);
		//pay.setOpenid(this.getUseropenid(wxPublic, code));
		pay.setNotifyUrl(pro.getProperty("wx_notify_h5_v3"));
		WxResult result1 = getWXPrepayId(pay, mchId, appid, appkey,subMchId);
		long timeStamp = System.currentTimeMillis() / 1000;
		String nonceStr = getRandomString(16);
		List<NameValuePair> signParams = new LinkedList<NameValuePair>();
		signParams.add(new BasicNameValuePair("appid", appid));
		signParams.add(new BasicNameValuePair("noncestr", nonceStr));
		signParams.add(new BasicNameValuePair("package", "Sign=WXPay"));
		signParams.add(new BasicNameValuePair("partnerid",mchId));
		signParams.add(new BasicNameValuePair("prepayid", result1.getPrepayId()));
		signParams.add(new BasicNameValuePair("timestamp", String.valueOf(timeStamp)));
		String sign = genAppSign(signParams,appkey);
		result1.setSign(sign);
		result1.setAppid(appid);
		result1.setMchId(mchId);
		result1.setTimeStamp(timeStamp);
		result1.setNonceStr(nonceStr);
		result1.setAppkey(appkey);
		// 2.封装返回消息
		log.info("doPay end html=" + result1);
		return result1;
	}
*/
	/**
	 * 
	 * 方法用途: 获取微信预支付id<br>
	 * 实现步骤: <br>
	 * @param pay
	 * @param partner
	 * @param appid
	 * @param partnerKey
	 * @return
	 */
	/*private WxResult getWXPrepayId(DeyiPay pay, String mchId, String appid, String partnerKey,String subMchId) throws Exception {
		PropertiesUtil pro = new PropertiesUtil();
		String prepayId = "";
		// 封装微信预支付对象
		WXPrepay wxPrepay = new WXPrepay();
		long timeStamp = System.currentTimeMillis() / 1000;
		wxPrepay.setAppid(appid);
		wxPrepay.setBody(pay.getGoodsName());
		wxPrepay.setMch_id(mchId);
		wxPrepay.setNonce_str(getRandomString(16));
		wxPrepay.setNotify_url(pay.getNotifyUrl());
		wxPrepay.setOut_trade_no(pay.getOrderId());
		//wxPrepay.setOpenid(pay.getOpenid());
		wxPrepay.setSpbill_create_ip("127.0.0.1");
		wxPrepay.setTotal_fee(pay.getAmount());
		wxPrepay.setTrade_type("APP");
		
		wxPrepay.setSign(signData(wxPrepay, partnerKey));
		XSTREAM.alias("xml", WXPrepay.class);
		String prepay = XSTREAM.toXML(wxPrepay);
		prepay = prepay.replace("__", "_");
		log.info("请求xml:"+prepay);
		// 预支付请求
		String result = HttpClient.send(pro.getProperty("wx_prepair_h5_v3"), prepay, "UTF-8", "UTF-8");
		XSTREAM.alias("xml", WXPrepayResp.class);
		log.info("支付返回result="+result);
		WxResult result1 = new WxResult();// 请求返回结果
		// 解析预支付返回对象
		WXPrepayResp wxPrepayResp = (WXPrepayResp) XSTREAM.fromXML(result);
		if (null != wxPrepayResp) {
			if ("SUCCESS".equals(wxPrepayResp.getReturn_code())) {
				if ("SUCCESS".equals(wxPrepayResp.getResult_code())) {
					prepayId = wxPrepayResp.getPrepay_id();
					result1.setAppid(appid);
					result1.setMchId(mchId);
					result1.setNonceStr(wxPrepay.getNonce_str());
					result1.setPay(pay);
					result1.setPrepayId(prepayId);
					result1.setTimeStamp(timeStamp);
				} else {
					log.debug(wxPrepayResp.getErr_code_des());
				}
			} else {
				log.debug(wxPrepayResp.getReturn_msg());
			}
		} else {
			log.debug("解析为空");
		}
		return result1;
	}*/

	/**
	 * 
	 * 方法用途: 生成随机字符串<br>
	 * 实现步骤: <br>
	 * @param length
	 * @return
	 */
	private static String getRandomString(int length) { // length表示生成字符串的长度
		String base = "abcdefghijklmnopqrstuvwxyz0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(base.length());
			sb.append(base.charAt(number));
		}
		return sb.toString().toUpperCase();
	}

	/**
	 * 
	 * 方法用途: 对数据进行签名<br>
	 * 实现步骤: <br>
	 * @param wxPrepay
	 * @param partnerKey
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private String signData(WXPrepay wxPrepay, String partnerKey) throws UnsupportedEncodingException {
		
		String[] params = { "appid=" + wxPrepay.getAppid(), "mch_id=" + wxPrepay.getMch_id(),
				"nonce_str=" + wxPrepay.getNonce_str(), "body=" + wxPrepay.getBody(),
				"out_trade_no=" + wxPrepay.getOut_trade_no(), "total_fee=" + wxPrepay.getTotal_fee(),
				"spbill_create_ip=" + wxPrepay.getSpbill_create_ip(), "notify_url=" + wxPrepay.getNotify_url(),
				"trade_type=" + wxPrepay.getTrade_type()};
		String param = PayUtils.genSortParams(params);
		log.info("param=" + param + ",key="+partnerKey+"");
		String sign = DigestUtils.md5Hex((param+"&key="+partnerKey).getBytes("utf-8")).toUpperCase();
		log.info("预支付生成订单签名sign=" + sign + "");
		return sign;
	}

	/**
	 * 
	 * 方法用途: 获取支付签名<br>
	 * 实现步骤: <br>
	 * @param appid
	 * @param prepayId
	 * @param partnerKey
	 * @param timeStamp
	 * @param nonceStr
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private String getPaySign(String mchId,String appid, String prepayId, String partnerKey, long timeStamp, String nonceStr) throws UnsupportedEncodingException {
		String[] params = { "appId=" + appid, "timeStamp=" + timeStamp,
				"nonceStr=" + nonceStr, "package="+"Sign=WXPay","prepay_id=" + prepayId,"partnerid="+ mchId};
		String param = PayUtils.genSortParams(params);
		log.info("param=" + param + ",partnerKey="+partnerKey+"");
		String sign = DigestUtils.md5Hex((param+"&key="+partnerKey).getBytes("utf-8")).toUpperCase();
		log.info("返回支付签名sign=" + sign + "");
		return sign;
	}

	/**
	 * 获取签名sign
	 * @param params
	 * @return
	 */
	private String genAppSign(List<NameValuePair> params,String appkey) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < params.size(); i++) {
			sb.append(params.get(i).getName());
			sb.append('=');
			sb.append(params.get(i).getValue());
			sb.append('&');
		}
		sb.append("key=");
		sb.append(appkey);

		String appSign = getMessageDigest(sb.toString().getBytes())
				.toUpperCase();
		
		return appSign;
	}

public final static String getMessageDigest(byte[] buffer) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(buffer);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			return null;
		}
	}
	
}
