package com.deyi.controller.pay.aliPay;

public class PayInfo {
		private AliPayResult aliPayResult;
		
		//后台做签名，前台需要先把订单信息传递给后台，再得到签名
		//payInfo += "&sign=\"" + "后台做的签名信息sign";
		private String sign;
		//payInfo += "\"&" + "sign_type=\"RSA\"";
		private String sign_type;
		
		public AliPayResult getAliPayResult() {
			return aliPayResult;
		}
		public void setAliPayResult(AliPayResult aliPayResult) {
			this.aliPayResult = aliPayResult;
		}
		public String getSign() {
			return sign;
		}
		public void setSign(String sign) {
			this.sign = sign;
		}
		public String getSign_type() {
			return sign_type;
		}
		public void setSign_type(String sign_type) {
			this.sign_type = sign_type;
		}
		
		
}
