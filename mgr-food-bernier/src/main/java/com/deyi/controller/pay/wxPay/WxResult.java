package com.deyi.controller.pay.wxPay;


public class WxResult {
//	private DeyiPay pay;
	
	private String prepayId;
	
	private String mchId;
	
	private String appid;
	
	private String sign;
	
	private String appkey;
	
	private long timeStamp;
	
	private String nonceStr;

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

//	public DeyiPay getPay() {
//		return pay;
//	}
//
//	public void setPay(DeyiPay pay) {
//		this.pay = pay;
//	}

	public String getPrepayId() {
		return prepayId;
	}

	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	
}
