package com.deyi.controller.pay.aliPay;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *版本：3.3
 *日期：2012-08-10
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
	
 *提示：如何获取安全校验码和合作身份者ID
 *1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *2.点击“商家服务”(https://b.alipay.com/order/myOrder.htm)
 *3.点击“查询合作者身份(PID)”、“查询安全校验码(Key)”

 *安全校验码查看时，输入支付密码后，页面呈灰色的现象，怎么办？
 *解决方法：
 *1、检查浏览器配置，不让浏览器做弹框屏蔽设置
 *2、更换浏览器或电脑，重新登录查询。
 */

public class AlipayConfig {
	
	//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	// 合作身份者ID，以2088开头由16位纯数字组成的字符串
	public static String partner = "2088412419378294";
	// 商户的私钥
	public static String private_key = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALvQnBoMyRHzBvnEC/AsqQ8xRnzDGSXi4tEL6SJyjDfw7F3k7ELPJ5CHCxcaBG/gRHfmz6LIp7HBQy6p2L0t3RBVtyziRGUp0PRIyHQZHI4tgbjDMaHXYgiP0q1vdi1Q+0CMW3A/xkMZZMQCBeCXu3F74FkFXw3bFFUXU6mNGPAtAgMBAAECgYAkcD3JBBACrn78+F9EkrWeH/ZIAGYhpppQ6pmpuEWiGNkTAnYgJCbN8JVjPwX7WqyOArboIzv9zbwmEofddEd+LYAJPY3jgiaDJ2XVT3WdDlrZw/F1j9AhyEAo67patmRVn/XV6xVmgIKgm+yB1hnjfjbZaWdOUOzOZwVVyYyywQJBAOxes5S/XRRZ06janipfnDYwJ3gaBYgcBbWPgzu99GXfl/6SYeZ5XoWuQy+46RqjbtztmWrV5aAkDue8WHQjpf0CQQDLaZzjJpVH3A0ykCOps335+mgF/SUNYc3Fv05IrFrO+vYAlBYz3YqujGvfECXN1eETeabar/sGggkajoCI6XHxAkEAxZ+gMbuVjRBVYRX3q6Hor32vYkEPYu9a7qQgqvB0yEAbiqLE67eKCebbEalebAE9DcYxwRs5M50yQRo+Mkwj2QJBAMfMvEquWlFURy5UQINvCB2jpcvEJEHpwIotaAKgMS4/eaWFz+0v/gmHammXXdq567QKeSczVlnFHpYURRK2YrECQHM/mv56unfNs0VTTk7DALZhJoUSE4r9IzAX1Pii4HbfxRzoIgIVdxO7LaeHcGQW+qmv17K618y7QWtNaQ4WTGg=";
	
	// 支付宝的公钥，无需修改该值
	public static String ali_public_key  = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";

	//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	

	// 调试用，创建TXT日志文件夹路径
	public static String log_path = "D:\\";

	// 字符编码格式 目前支持 gbk 或 utf-8
	public static String input_charset = "utf-8";
	
	// 签名方式 不需修改
	public static String sign_type = "RSA";

	// 回调地址
	public static String notity_path = "http://114.55.105.250:8080/camshop/api/notify/aliPaynotifyURL.html";
	
	public static String  ali_pay_wap="mobile.securitypay.pay";
	

}
