
package com.deyi.controller.pay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class RSA{
	
	public static final String  SIGN_ALGORITHMS = "SHA1WithRSA";
	
	/**
	* RSA签名
	* @param content 待签名数据
	* @param privateKey 商户私钥
	* @param input_charset 编码格式
	* @return 签名值
	*/
	public static String sign(String content, String privateKey, String input_charset)
	{
        try 
        {
        	PKCS8EncodedKeySpec priPKCS8 	= new PKCS8EncodedKeySpec( Base64.decode(privateKey) ); 
        	KeyFactory keyf 				= KeyFactory.getInstance("RSA");
        	PrivateKey priKey 				= keyf.generatePrivate(priPKCS8);

            java.security.Signature signature = java.security.Signature
                .getInstance(SIGN_ALGORITHMS);

            signature.initSign(priKey);
            signature.update( content.getBytes(input_charset) );

            byte[] signed = signature.sign();
            
            return Base64.encode(signed);
        }
        catch (Exception e) 
        {
        	e.printStackTrace();
        }
        
        return null;
    }
	
	
	/**
	* RSA验签名检查
	* @param content 待签名数据
	* @param sign 签名值
	* @param ali_public_key 支付宝公钥
	* @param input_charset 编码格式
	* @return 布尔值
	*/
	public static boolean verify(String content, String sign, String ali_public_key, String input_charset)
	{
		try 
		{
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	        byte[] encodedKey = Base64.decode(ali_public_key);
	        PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

		
			java.security.Signature signature = java.security.Signature
			.getInstance(SIGN_ALGORITHMS);
		
			signature.initVerify(pubKey);
			signature.update( content.getBytes(input_charset) );
		
			boolean bverify = signature.verify( Base64.decode(sign) );
			return bverify;
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	* 解密
	* @param content 密文
	* @param private_key 商户私钥
	* @param input_charset 编码格式
	* @return 解密后的字符串
	*/
	public static String decrypt(String content, String private_key, String input_charset) throws Exception {
        PrivateKey prikey = getPrivateKey(private_key);

        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, prikey);

        InputStream ins = new ByteArrayInputStream(Base64.decode(content));
        ByteArrayOutputStream writer = new ByteArrayOutputStream();
        //rsa解密的字节大小最多是128，将需要解密的内容，按128位拆开解密
        byte[] buf = new byte[128];
        int bufl;

        while ((bufl = ins.read(buf)) != -1) {
            byte[] block = null;

            if (buf.length == bufl) {
                block = buf;
            } else {
                block = new byte[bufl];
                for (int i = 0; i < bufl; i++) {
                    block[i] = buf[i];
                }
            }

            writer.write(cipher.doFinal(block));
        }

        return new String(writer.toByteArray(), input_charset);
    }

	
	/**
	* 得到私钥
	* @param key 密钥字符串（经过base64编码）
	* @throws Exception
	*/
	public static PrivateKey getPrivateKey(String key) throws Exception {

		byte[] keyBytes;
		
		keyBytes = Base64.decode(key);
		
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
		
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		
		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
		
		return privateKey;
	}
	public static void main(String[] args) throws Exception {
		
		String  rsaalipaypublic = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEGZ/aq+Sql548vnajYHgxfpipYSMuuoxDomv0fBYtoUxTNegiOAugmpfG/tLyBgQYfJQw0Er4GtQtU0XMsWAO3uUraZspPmhEWJA78EgRKpszFON/rwgihv4HYCoF4qWion74y74rOtK8UMAYk+yMvEViUnwhKlZ6cIvnIvgKQQIDAQAB";
		String  rsaprivate = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALCL6tAJXRiyeDFtgwjEy10s/vJI+rSzcQLwtsry0DXcyrLPzHsV4p5l22+iZ2H2aOfWCZARD7Mj3RqpJma8NKVkMNf9Vrp4lMlXGBCUI+B9Okv86SBUy9RRjksGw3tsjfFv9KpDWWcHMFwU8l5TyqW74DFAhTYk8m9vMNj7wPPAgMBAAECgYAQWgU3m4Zp4S3Mti3qw/9p897xJd1CmNyGCejUT+wVZ18dWZHS1+cLxlNGMN2iH5EC3KFXeaBYiK3IxI/WQkmHsDMDM3ZLiQvKL+5kYc8gjolomZO07WXdB6Xr1cauRhj/pNRmnhHCm+jMt9RmFjDFTPLbCM+iCl3Espks2XdroQJBAN5OihbMvi6YvAhnXE4Dv4gdam0HS2XzeKWsAzFKyHGMyiInLfzGv+/T0N8PcPx2f6CYXDpZ468nyDVB+Qb9lykCQQDLTeOmuSURWiezEwfTbK5Vsxe6unn04JTIU0uAZqn/FkGdsJ8roJ6EaaQJP7XAy7MSVnf6I3mIk2/DG5VdWno3AkEAgUdrRpcKgk6QCtMQ+s1zNNTmb0Eut7s/DhlaQfAbYO1fHz1poPZz6CRl81Ingqnmzp3MBKMBpXqdE1uqVkZjAQJBAMcnUrV0sqoP3cne/5CN6y1KVhl2KJfIFBc0+UYby7V1UOuvJA0xt3bQoX7p6RqmsJFrIPZpH9m5TAcdkzCyu/sCQCbgMPRdl8iLgdk3286PNdxukvrX52MC+XX19vArEulPDzu46+6uOiH1gEC6o73XWsMJvlcBejKg6dy/TRVOLbc=";
			System.out.println(rsaprivate.length());
		String ss = sign("hello=name",rsaprivate,"utf-8");
		System.out.println(ss);
		String   mytext   =   java.net.URLEncoder.encode(ss,   "utf-8");   
		
		System.out.println(mytext);
		
			boolean end = verify("hello=name", ss, rsaalipaypublic, "UTF-8");
			
			System.out.println(end);
		
	}
}
