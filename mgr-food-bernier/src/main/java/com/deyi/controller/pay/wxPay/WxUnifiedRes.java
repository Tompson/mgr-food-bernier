/**
 * 
 */
package com.deyi.controller.pay.wxPay;

/**
 * pure
 * @author hejx
 * @2016年4月11日
 */
public class WxUnifiedRes extends WxMicroEntityRes {

	private String prepay_id;
	private String 	code_url;
	public String getPrepay_id() {
		return prepay_id;
	}
	public void setPrepay_id(String prepay_id) {
		this.prepay_id = prepay_id;
	}
	public String getCode_url() {
		return code_url;
	}
	public void setCode_url(String code_url) {
		this.code_url = code_url;
	}
}
