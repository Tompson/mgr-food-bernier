package com.deyi.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Store;
import com.deyi.entity.StoreTable;
import com.deyi.entity.SysUser;
import com.deyi.service.StoreService;
import com.deyi.service.StoreTableService;
import com.deyi.util.BizHelper;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.DateUtils;
import com.deyi.util.Page;
import com.deyi.util.PropertiesUtil;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.dy.util.QrCodeUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("table")
public class StoreTableController  extends  Component<StoreTable>{
	private Logger log = LoggerFactory.getLogger(DeliverManController.class);
	@Autowired
	private StoreTableService storeTableService;
	@Autowired
	private StoreService storeService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("table/tableList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<StoreTable> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		GridVo vo = new GridVo();
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<StoreTable> page = new Page<StoreTable>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		setParams(request, reqGrid.getObject(), page);
		List<StoreTable> results = storeTableService.queryPage(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}
	@RequestMapping(value = "toTableAdd")
	public ModelAndView toTableAdd(ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
		SysUser user = getSessionUser(request);
		if(user!=null){
			if(!user.getType().equals("1")){
				Store store = storeService.selectByPrimaryKey(user.getStoreId());
				List<Store> stores=new ArrayList<Store>() ;
				stores.add(store);
				mav.addObject("stores", stores);
			}else{
				List<Store> stores = storeService.queryAllStore();
				mav.addObject("stores", stores);
			}
		}
		mav.setViewName("table/tableAdd");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "tableAdd")
	public  ReturnVo<Object> tableAdd(ModelAndView mav, HttpServletRequest request,StoreTable storeTable) {
		System.out.println("enter--->url==" + request.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(request));
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			String project_path = PropertiesUtil.getProperty(Constants.PROJECT_PATH);
			String upload_dir = PropertiesUtil.getProperty(Constants.UPLOAD_Table);
			Date date = new Date();
			String dir = GetFileDir(upload_dir, DateUtils.getYear(date) + "", DateUtils.getMonth(date) + "",
					DateUtils.getDay(date) + "");
			String fileSavePath = project_path + dir;

			// 创建目录，如果没有的话
			File savedir = new File(fileSavePath);
			if (!savedir.exists()) {
				savedir.mkdirs();
			}
			String filename = DateUtils.getDbDate() + DateUtils.getRandomNum();
			dir = StringUtils.replace(dir, File.separator, "/");
			String url = dir + filename + ".bmp";
			storeTable.setQrcode(url);
			//如果是门店添加的时候就必须加入相应门店
			if(!"1".equals(sysUser.getType())){
				storeTable.setStoreId(storeTable.getStoreId());
			}
			log.info(url);
			vo.setData(url);
			storeTableService.insertSelectiveReturnId(storeTable);
			String tableId=storeTable.getId().toString();
			String storeId=storeTable.getStoreId().toString();
			StringBuffer requestURL = request.getRequestURL();
			String content =PropertiesUtil.getProperty("ws_server_url")+"bernier/h5/store/eatingInStore.html?storeId="+storeId+"&tableId="+tableId;
			System.out.println("二维码的内容是"+content);
            QrCodeUtil.createRQ(fileSavePath, content, filename);//工具类创建一个二维码
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "deleteTable")
	public  ReturnVo<Object> deleteTable(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			storeTableService.deleteByPrimaryKey(Integer.parseInt(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	@RequestMapping(value = "toTableEdit")
	public ModelAndView toTableEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();// 获取Session中的用户信息
		StoreTable table = storeTableService.selectByPrimaryKey(Integer.parseInt(id));
		mav.addObject("table", table);
		mav.setViewName("table/tableEdit");
		return mav;
	}

	@RequestMapping(value = "tableEdit")
	public @ResponseBody ReturnVo<Object> tableEdit(ModelAndView mav, HttpServletRequest request, StoreTable storeTable) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			storeTableService.updateByPrimaryKeySelective(storeTable);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	
	
	
	
	
	
	private String GetFileDir(String... dir) {
		String dirpath = "";
		String separator = File.separator;// \
		for (int i = 0; i < dir.length; i++) {
			dirpath += separator + dir[i];
		}
		dirpath = dirpath + separator;
		return dirpath;
	}
}
