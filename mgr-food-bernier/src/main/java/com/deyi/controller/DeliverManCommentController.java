package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.OrderComment;
import com.deyi.entity.SysUser;
import com.deyi.service.OrderCommentService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.dy.util.PropertiesUtil;

/** 
 * @author zjz Create on 2016年11月24日 下午9:10:28 
 */
@Controller
@RequestMapping(value ="deliverManComment")
public class DeliverManCommentController extends Component<OrderComment> {
	private Logger log = LoggerFactory.getLogger(OrderCommentController.class);

	@Autowired
	private OrderCommentService orderCommentService;
	
	/**
	 * 订单评论列表页面
	 * @param mav
	 * @param request
	 * @param entity
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request){
		mav.setViewName("deliverManComment/deliverManCommentList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<OrderComment> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<OrderComment> page = new Page<OrderComment>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<OrderComment> results = orderCommentService.queryPageByDeliver(page);
		
		for(OrderComment OrderComment :results){
			String payNumber = OrderComment.getPayNumber();
			String property = PropertiesUtil.getProperty("order_pay_no_prifix");
			if(payNumber.contains(property)){
				String replace = StringUtils.replace(payNumber,property,"");
				OrderComment.setPayNumber(replace);
			}
		}
		
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		return vo;
	}
	
	


	@RequestMapping(value = "toReply")
	public ModelAndView toGoodsEdit(String cid, ModelAndView mav, HttpServletRequest request) throws Exception {

		if (cid == null || "".equals(cid)) {
			throw new Exception();
		}
		
		OrderComment orderComment = orderCommentService.selectByPrimaryKey(Long.valueOf(cid));
		
		mav.addObject("orderComment", orderComment);
		mav.setViewName("deliverManComment/reply");
		return mav;
	}

	@RequestMapping(value = "Reply")
	public @ResponseBody ReturnVo<Object> Reply(ModelAndView mav, HttpServletRequest request, OrderComment orderComment) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			orderCommentService.updateByPrimaryKeySelective(orderComment);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	

	
	/**
	 *   删除 
	 */
	@RequestMapping(value="deleteOrderComment")
	public @ResponseBody ReturnVo<Object> deleteOrderComment(String cid,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			orderCommentService.deleteByPrimaryKey(Long.parseLong(cid));
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
	

}
