package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Chief;
import com.deyi.entity.MemberLevel;
import com.deyi.entity.Store;
import com.deyi.service.MemberLevelService;
import com.deyi.service.impl.MemberLevelServiceImpl;
import com.deyi.util.Component;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value="level")
public class MemberLevelController extends  Component<MemberLevel>  {
	private Logger log = LoggerFactory.getLogger(DeliverManController.class);
	@Autowired
	private MemberLevelService memberLevelServiceImpl;
	
	 @RequestMapping(value = "list")
		public ModelAndView list(ModelAndView mav) throws Exception {
			mav.setViewName("level/levelList");
			return mav;
		}
	    @ResponseBody
		@RequestMapping(value = "page")
		public GridVo page(@RequestBody RequestGrid<MemberLevel> reqGrid,ModelAndView mav ,HttpServletRequest request) {
			log.info("厨师列表Start......");
			Page<MemberLevel> page = new Page<MemberLevel>();
			page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
			page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
			GridVo vo = new GridVo();
			setParams(request, reqGrid.getObject(), page);
			List<MemberLevel> results=memberLevelServiceImpl.queryPage(page);
			vo.setRows(results);
			vo.setTotal(page.getTotalRecord());//总记录数
			JSONObject fromObject = JSONObject.fromObject(vo);
			return vo;
		}
	    @RequestMapping(value = "tolevelEdit")
		public ModelAndView tolevelEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {

			HttpSession session = request.getSession();// 获取Session中的用户信息
			MemberLevel memberLevel = memberLevelServiceImpl.selectByPrimaryKey(Integer.parseInt(id));
			mav.addObject("level",memberLevel);
			mav.setViewName("level/levelEdit");
			return mav;
		}

		@RequestMapping(value = "levelEdit")
		public @ResponseBody ReturnVo<Object> levelEdit(ModelAndView mav, HttpServletRequest request, MemberLevel memberLevel) {
			ReturnVo<Object> vo = new ReturnVo<Object>();
			try {
				MemberLevel memberLevel2=memberLevelServiceImpl.selectByPrimaryKey(memberLevel.getId());
				//根据名称查询是否存在相同名称的 会员等级
				MemberLevel memberLevel1=memberLevelServiceImpl.selectByPrimaryName(memberLevel.getName());
				if(memberLevel1!=null && !memberLevel2.getName().equals(memberLevel.getName())){
					vo.setMessage("已存在相同会员等级名称！");
					vo.setSuccess(false);
					return vo;
				}
				memberLevelServiceImpl.updateByPrimaryKeySelective(memberLevel);
			} catch (Exception e) {
				log.error("异常", e);
				vo.setMessage("系统异常！");
				vo.setSuccess(false);
			}
			return vo;
		}
}
