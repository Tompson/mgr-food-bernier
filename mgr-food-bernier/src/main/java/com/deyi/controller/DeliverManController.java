package com.deyi.controller;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.DeliverMan;
import com.deyi.entity.Member;
import com.deyi.entity.SysUser;
import com.deyi.service.DeliverManService;
import com.deyi.service.MemberService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.DateUtils;
import com.deyi.util.Page;
import com.deyi.util.PropertiesUtil;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.dy.util.QrCodeUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "deliverMan")
public class DeliverManController extends Component<DeliverMan> {
	private Logger log = LoggerFactory.getLogger(DeliverManController.class);
	@Autowired
	private DeliverManService deliverManService;
	
	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("deliverMan/deliverManList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<DeliverMan> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("配送员列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<DeliverMan> page = new Page<DeliverMan>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<DeliverMan> results = deliverManService.queryPage(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	@RequestMapping(value = "toDeliverManAdd")
	public ModelAndView toDeliverManAdd(ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
		mav.setViewName("deliverMan/deliverManAdd");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "deliverManAdd")
	public  ReturnVo<Object> deliverManAdd(ModelAndView mav, HttpServletRequest request,DeliverMan deliverMan) {
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			String project_path = PropertiesUtil.getProperty(Constants.PROJECT_PATH);
			String upload_dir = PropertiesUtil.getProperty(Constants.UPLOAD_DIR);
			Date date = new Date();
			String dir = GetFileDir(upload_dir, DateUtils.getYear(date) + "", DateUtils.getMonth(date) + "",
					DateUtils.getDay(date) + "");
			String fileSavePath = project_path + dir;

			// 创建目录，如果没有的话
			File savedir = new File(fileSavePath);
			if (!savedir.exists()) {
				savedir.mkdirs();
			}
			String filename = DateUtils.getDbDate() + DateUtils.getRandomNum();
			dir = StringUtils.replace(dir, File.separator, "/");
			String url = dir + filename + ".bmp";
			
			deliverMan.setQrCode(url);
			log.info(url);
			vo.setData(url);
			deliverMan.setStoreId(sysUser.getStoreId());
		    deliverManService.insertSelective(deliverMan);
			String deliverManId1 = deliverMan.getDeliverManId().toString();

//			ChargeTaken chargeTaken = new ChargeTaken();
//			chargeTaken.setMemberId(userid);
//			chargeTaken.setQrcodeurl(url);
//			String uuid = UUID.randomUUID().toString();
//			chargeTaken.setUuid(uuid);
//			chargeTaken.setTaken(uuid);
//			chargeTakenService.insertSelective(chargeTaken);
			
			
//			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			// 加密的内容是由配送员id和uuid(uuid为固定的)和系统当前时间组成的，中间由逗号分隔。
			//配送员id
//			String uuidTest = UUID.randomUUID().toString().replaceAll("-", "");
			String content = StringUtils.join(new String[] {deliverManId1,Constants.DELIVERMAN_UUID_KEY},",");//配送员id,固定的uuid,当前系统时间,中间由逗号 隔开
			
					
					
			byte[] encodeBase64 = Base64.encodeBase64(content.getBytes());//获取字节然后加密得到的是一个数组

			QrCodeUtil.createRQ(fileSavePath, new String(encodeBase64), filename);//工具类创建一个二维码

		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}


	@RequestMapping(value = "toDeliverManEdit")
	public ModelAndView toDeliverManEdit(String deliverManId, ModelAndView mav, HttpServletRequest request) throws Exception {

		if (StringUtils.isBlank(deliverManId)) {
			throw new Exception();
		}
		HttpSession session = request.getSession();// 获取Session中的用户信息
		DeliverMan deliverMan = deliverManService.selectByPrimaryKey(Integer.parseInt(deliverManId));
		mav.addObject("deliverMan", deliverMan);
		mav.setViewName("deliverMan/deliverManEdit");
		return mav;
	}

	@RequestMapping(value = "deliverManEdit")
	public @ResponseBody ReturnVo<Object> deliverManEdit(ModelAndView mav, HttpServletRequest request, DeliverMan deliverMan) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			deliverManService.updateByPrimaryKeySelective(deliverMan);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 删除商品
	 */
	@ResponseBody
	@RequestMapping(value = "deleteDeliverMan")
	public  ReturnVo<Object> deleteDeliverMan(String deliverManId, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			deliverManService.deleteByPrimaryKey(Integer.parseInt(deliverManId));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 禁用
	 * @Author zjz Create on 2016年10月21日 下午2:06:03
	 * @param reqGrid
	 * @param mav
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableState")
	public  ReturnVo<Object> disableState(HttpServletRequest request,String deliverManId) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品禁用Start......");
		try {
			DeliverMan deliverMan = deliverManService.selectByPrimaryKey(new Integer(deliverManId));
			deliverMan.setStatus("2");//禁用
			deliverManService.updateByPrimaryKeySelective(deliverMan);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 启用
	 */
	@ResponseBody
	@RequestMapping(value = "enableState")
	public  ReturnVo<Object> enableState(String deliverManId, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品启用Start......");
		try {
			DeliverMan deliverMan = deliverManService.selectByPrimaryKey(new Integer(deliverManId));
			deliverMan.setStatus("1");//启用
			deliverManService.updateByPrimaryKeySelective(deliverMan);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private String GetFileDir(String... dir) {
		String dirpath = "";
		String separator = File.separator;// \
		for (int i = 0; i < dir.length; i++) {
			dirpath += separator + dir[i];
		}
		dirpath = dirpath + separator;
		return dirpath;
	}
	
	
}
