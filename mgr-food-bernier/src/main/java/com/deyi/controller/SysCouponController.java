package com.deyi.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.SysCoupon;
import com.deyi.service.SysCouponService;
import com.deyi.util.Component;
import com.deyi.util.DateUtils;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;
/**
 * 
 * @ClassName: SysCouponController 
 * @Description: 平台代金券管理类
 * @author GZ 
 * @date 2017年5月22日 下午5:06:51 
 *
 */
@Controller
@RequestMapping(value = "syscoupon")
public class SysCouponController extends Component<SysCoupon> {
	private Logger log = LoggerFactory.getLogger(SysCouponController.class);
	@Autowired
	private SysCouponService sysCouponService;
	
	@RequestMapping(value = "couponList")
	public ModelAndView exchangeList(ModelAndView mav) throws Exception {//积分兑换代金券
		mav.setViewName("syscoupon/couponList");
		return mav;
		}
		/**
		 * 
		 * @Title: couponPage 
		 * @Description: 分页
		 * @param @param reqGrid
		 * @param @param mav
		 * @param @param request
		 * @param @return    设定文件 
		 * @return GridVo    返回类型 
		 * @throws
		 */
		@ResponseBody
		@RequestMapping(value = "couponPage")
		public GridVo couponPage(@RequestBody RequestGrid<SysCoupon> reqGrid,ModelAndView mav ,HttpServletRequest request) {
			log.info("商品列表Start......");
			Page<SysCoupon> page = new Page<SysCoupon>();
			page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
			page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
			GridVo vo = new GridVo();
			setParams(request, reqGrid.getObject(), page);
			List<SysCoupon> results = sysCouponService.querySysCouponPage(page);
			vo.setRows(results);//总共有多少页
			vo.setTotal(page.getTotalRecord());//总记录数
			JSONObject fromObject = JSONObject.fromObject(vo);
			return vo;
		}

		/**
		 * 
		 * @Title: toCouponAdd 
		 * @Description: 跳转平台优惠卷页面
		 * @param @param mav
		 * @param @param request
		 * @param @return    设定文件 
		 * @return ModelAndView    返回类型 
		 * @throws
		 */
		@RequestMapping(value = "toCouponAdd")
		public ModelAndView toBookAdd(ModelAndView mav, HttpServletRequest request) {
			mav.setViewName("syscoupon/couponAdd");
			return mav;
		}
		/**
		 * 
		 * @Title: couponAdd 
		 * @Description: 新增平台优惠卷
		 * @param @param mav
		 * @param @param request
		 * @param @param SysCoupon
		 * @param @return    设定文件 
		 * @return ReturnVo<Object>    返回类型 
		 * @throws
		 */
		@ResponseBody
		@RequestMapping(value = "couponAdd")
		public  ReturnVo<Object> couponAdd(ModelAndView mav, HttpServletRequest request,SysCoupon SysCoupon) {
			ReturnVo<Object> vo = new ReturnVo<Object>();
			try {
				SysCoupon.setStatus("1");
				SysCoupon.setCreattime(new Date());
				sysCouponService.insertSelective(SysCoupon);
			} catch (Exception e) {
				log.error("异常", e);
				vo.setMessage("Abnormal operation");
				vo.setSuccess(false);
			}
			return vo;
		}

		/**
		 * 
		 * @Title: toCouponEdit 
		 * @Description: 跳转平台优惠卷页面
		 * @param @param id
		 * @param @param mav
		 * @param @param request
		 * @param @return
		 * @param @throws Exception    设定文件 
		 * @return ModelAndView    返回类型 
		 * @throws
		 */
		@RequestMapping(value = "toCouponEdit")
		public ModelAndView toCouponEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {

			if (id == null || "".equals(id)) {
				throw new Exception();
			}
			SysCoupon SysCoupon = sysCouponService.selectByPrimaryKey(Long.parseLong(id));
			SysCoupon.setStarttime1(DateUtils.formatDateTime(SysCoupon.getStarttime()));
			mav.addObject("sysCoupon", SysCoupon);
			mav.setViewName("syscoupon/couponEdit");
			return mav;
		}
		/**
		 * 
		 * @Title: couponEdit 
		 * @Description: 修改平台优惠卷
		 * @param @param mav
		 * @param @param request
		 * @param @param sysCoupon
		 * @param @return    设定文件 
		 * @return ReturnVo<Object>    返回类型 
		 * @throws
		 */
		@ResponseBody
		@RequestMapping(value = "couponEdit")
		public ReturnVo<Object> couponEdit(ModelAndView mav, HttpServletRequest request, SysCoupon sysCoupon) {
			ReturnVo<Object> vo = new ReturnVo<Object>();
			try {
				sysCouponService.updateByPrimaryKeySelective(sysCoupon);
			} catch (Exception e) {
				log.error("异常", e);
				vo.setMessage("Abnormal operation");
				vo.setSuccess(false);
			}
			return vo;
		}

		/**
		 * 
		 * @Title: deleteCoupon 
		 * @Description: 删除平台优惠卷
		 * @param @param id
		 * @param @param request
		 * @param @return    设定文件 
		 * @return ReturnVo<Object>    返回类型 
		 * @throws
		 */
		@ResponseBody
		@RequestMapping(value = "deleteCoupon")
		public  ReturnVo<Object> deleteCoupon(String id, HttpServletRequest request ) {
			ReturnVo<Object> vo = new ReturnVo<Object>();
			try {
				sysCouponService.deleteByPrimaryKey(Long.parseLong(id));
			} catch (Exception e) {
				log.error("异常", e);
				vo.setMessage("Abnormal operation");
				vo.setSuccess(false);
			}
			return vo;
		}
	
}
