package com.deyi.controller;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.deyi.entity.Menu;
import com.deyi.entity.SysUser;
import com.deyi.entity.UserMenu;
import com.deyi.service.MenuService;
import com.deyi.service.SysUserService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.MD5S;
import com.deyi.util.Page;
import com.deyi.util.ResponseWriteUtils;
import com.deyi.util.StringUtils;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.google.gson.Gson;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "sysUser")
public class SysUserController extends Component<SysUser> {
	private Logger log = LoggerFactory.getLogger(SysUserController.class);
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private MenuService menuService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("sysUser/sysUserList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<SysUser> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("管理员Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<SysUser> page = new Page<SysUser>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码和PageNum当前页，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<SysUser> results = sysUserService.queryPage(page);//这里面已经将字段值设置进去了
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	/**
	 * 跳转新增系统用户
	 */
	@RequestMapping(value = "toSysUserAdd")
	public ModelAndView toSysUserAdd(ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
		mav.setViewName("sysUser/sysUserAdd");
		return mav;
	}
	/**
	 * 新增系统用户
	 */
	@ResponseBody
	@RequestMapping(value = "sysUserAdd")
	public  ReturnVo<Object> sysUserAdd(ModelAndView mav, HttpServletRequest request,SysUser sysUser) {//菜品描述<p>111</p>
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			HttpSession session = request.getSession();
			SysUser attribute = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
			if(attribute == null){
				vo.setSuccess(false);
				vo.setMessage("没有获取到当前登录人的信息,请重新登录");
				return vo;
			}
			String userPass = sysUser.getUserPass();//用户密码
			String md5Pwd = MD5S.GetMD5Code(userPass);//md5加密密码
			//如果是门店添加管理员
			if("2".equals(attribute.getType())){
				sysUser.setType("3");
				sysUser.setStoreId(attribute.getStoreId());
			}else{
				//平台管理员
				sysUser.setType("1");
			}
			sysUser.setUserPass(md5Pwd);
			sysUser.setCreateTime(new Date());
			sysUserService.insertSelective(sysUser);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}


	@RequestMapping(value = "toSysUserEdit")
	public ModelAndView toSysUserEdit(String uid, ModelAndView mav, HttpServletRequest request) throws Exception {
		if (uid == null || "".equals(uid)) {
			throw new Exception();
		}
		SysUser sysUser = sysUserService.selectByPrimaryKey(Integer.parseInt(uid));
		
		mav.addObject("sysUser", sysUser);
		mav.setViewName("sysUser/sysUserEdit");
		return mav;
	}

	@RequestMapping(value = "sysUserEdit")
	public @ResponseBody ReturnVo<Object> sysUserEdit(ModelAndView mav, HttpServletRequest request, SysUser sysUser) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			SysUser attribute = (SysUser)request.getSession().getAttribute(Constants.SESSION_USERINFO);
			SysUser sysUser1 = sysUserService.selectByPrimaryKey(sysUser.getUid());
			if(!"admin".equals(attribute.getUserAccount()) && !sysUser1.getUserAccount().equals(attribute.getUserAccount())){
				vo.setMessage("非admin人员不能编辑别的管理员");
				vo.setSuccess(false);
				return vo;
			}
		
			if(!"admin".equals(attribute.getUserAccount()) && "admin".equals(sysUser1.getUserAccount())){
				vo.setSuccess(false);
				vo.setMessage("admin有最高权限,不能被编辑");
				return vo;
			}
			
			sysUserService.updateByPrimaryKeySelective(sysUser);
			
			SysUser sysUser2 = sysUserService.selectByPrimaryKey(sysUser.getUid());
			//如果自己编辑自己就把session更新了
			if(attribute.getUserAccount().equals(sysUser2.getUserAccount())){
				request.getSession().setAttribute(Constants.SESSION_USERINFO, sysUser2);
			}
			
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	@ResponseBody
	@RequestMapping(value="toAuthruzedUser")
	public ModelAndView toAuthorized(ModelAndView mav, String uid ,HttpServletRequest request) {
		SysUser user = (SysUser)request.getSession().getAttribute(Constants.SESSION_USERINFO);
		List<String> menuIds = menuService.getMenuIdsByUserId(uid);
		List<Menu> menus = null;
		/*if("1".equals(user.getStatus()){//运营商
			 menus = menuService.getAllMenus();
		}else{//代理商
			 menus = menuService.getMenusByUser(loginName);
		}*/
		 menus = menuService.getMenusByUser(user.getUid());
		 for (Menu menu : menus) {
				String menuId = menu.getMenuId();
				if (menuIds.contains(menuId)) {
					menu.setChecked(true);
				} else {
					menu.setChecked(false);
				}
			}
		 mav.addObject("menuList", new Gson().toJson(menus));
		 mav.addObject("roleId", uid);
		 mav.setViewName("managent/toAuthorizedRole");
		 return mav;
	}
	/**
	 * 授权
	 */
	@RequestMapping(value = "authorizedRole")
	@Transactional
	public void authorizedRole(String id, String[] menuId,
			HttpServletRequest request,HttpServletResponse response) {
		ReturnVo<Object> vo = new ReturnVo<>();
		try {
			menuService.deleteUserMenuByUserId(id);
			UserMenu rm=null;
			for (int i = 0; i < menuId.length; i++) {
				rm = new UserMenu();
				rm.setMenuId(Long.valueOf(menuId[i]));
				rm.setUserId(Long.valueOf(id));
				menuService.insertSelective(rm);
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setMessage("授权失败");
			vo.setData("1");
			vo.setSuccess(false);
			ResponseWriteUtils.returnAjax(response, vo);
			return;
		}
		vo.setMessage("授权成功");
		vo.setData("0");
		ResponseWriteUtils.returnAjax(response, vo);
	}
	/**
	 * 删除系统用户
	 */
	@ResponseBody
	@RequestMapping(value = "deleteSysUser")
	public  ReturnVo<Object> deleteSysUser(String uid, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			SysUser attribute = (SysUser)request.getSession().getAttribute(Constants.SESSION_USERINFO);
			
			SysUser sysUser = sysUserService.selectByPrimaryKey(Integer.valueOf(uid));
			if(!"admin".equals(attribute.getUserAccount()) && !sysUser.getUserAccount().equals(attribute.getUserAccount())){
				vo.setMessage("非admin人员不能删除别的管理员");
				vo.setSuccess(false);
				return vo;
			}
			
			if("admin".equals(sysUser.getUserAccount())){
				vo.setMessage("admin有最高权限,不能被删除");
				vo.setSuccess(false);
				return vo;
			}
			
			if(attribute.getUserAccount().equals(sysUser.getUserAccount())){//如果自己把自己删除了
				vo.setMessage("自己不能删除自己,只有admin可以删除任何人员");
				vo.setSuccess(false);
				return vo;
			}
		
			sysUserService.deleteByPrimaryKey(Integer.parseInt(uid));
			
			
			//如果自己把自己删除了就把session清空
			if(attribute.getUserAccount().equals(sysUser.getUserAccount())){
				request.getSession().invalidate();
			}
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 禁用
	 * @Author zjz Create on 2016年10月21日 下午2:06:03
	 * @param reqGrid
	 * @param mav
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableState")
	public  ReturnVo<Object> disableState(HttpServletRequest request,String uid) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品禁用Start......");
		try {
			SysUser sysUser = sysUserService.selectByPrimaryKey(new Integer(uid));
			
			SysUser attribute = (SysUser)request.getSession().getAttribute(Constants.SESSION_USERINFO);
			if(!"admin".equals(attribute.getUserAccount()) && !sysUser.getUserAccount().equals(attribute.getUserAccount())){
				vo.setMessage("非admin人员不能禁用别的管理员");
				vo.setSuccess(false);
				return vo;
			}
			
			if("admin".equals(sysUser.getUserAccount())){
				vo.setSuccess(false);
				vo.setMessage("admin有最高权限,不能被禁用");
				return vo;
			}
			if(attribute.getUserAccount().equals(sysUser.getUserAccount())){
				vo.setSuccess(false);
				vo.setMessage("自己不能禁用自己,只有admin可以禁用任何人员");
				return vo;
			}
			
			sysUser.setStatus("2");//禁用
			sysUserService.updateByPrimaryKeySelective(sysUser);
			
			//目前下面的代码不会走到了
			//如果自己把自己禁用了,就把session清空
			if(attribute.getUserAccount().equals(sysUser.getUserAccount())){
				request.getSession().invalidate();
			}
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 启用
	 */
	@ResponseBody
	@RequestMapping(value = "enableState")
	public  ReturnVo<Object> enableState(String uid, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品启用Start......");
		try {
			SysUser attribute = (SysUser)request.getSession().getAttribute(Constants.SESSION_USERINFO);
			SysUser sysUser = sysUserService.selectByPrimaryKey(new Integer(uid));
			if(!"admin".equals(attribute.getUserAccount()) && !sysUser.getUserAccount().equals(attribute.getUserAccount())){
				vo.setMessage("非admin人员不能启用别的管理员");
				vo.setSuccess(false);
				return vo;
			}
			
			sysUser.setStatus("1");//启用
			sysUserService.updateByPrimaryKeySelective(sysUser);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
	
	
	
	
	
	/**
	 * 检查账号名是否相同
	 * @Author zjz Create on 2016年10月28日 下午3:33:02
	 * @param request
	 * @param userAccount
	 * @param uid
	 * @return
	 */
	@RequestMapping(value = "checkAccountName")
	public @ResponseBody Boolean checkAccountName(HttpServletRequest request, String userAccount, String uid) {
		try {
			SysUser s = new SysUser();
			s.setUserAccount(userAccount);
			List<SysUser> sysUser = sysUserService.checkAccountName(s);
			if (sysUser == null || sysUser.isEmpty()) {//sysUser.isEmpty() 是空返回true
				return true;
			}else {
				if (StringUtils.isBlank(uid)) {//StringUtils.isBlank 是空返回true
					return false;
				} else {
					SysUser sysUser1 = sysUserService.selectByPrimaryKey(Integer.parseInt(uid));
					for(SysUser sysUser2:sysUser){
						if(sysUser2.getUid().intValue() == sysUser1.getUid().intValue()){
							return true;
						}
						return false;
					}
				}
			}
		} catch (Exception e) {

		}
		return false;
	}
	
}
