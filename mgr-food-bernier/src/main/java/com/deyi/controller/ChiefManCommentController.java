package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.deyi.entity.ChiefComment;
import com.deyi.entity.SysUser;
import com.deyi.service.ChiefCommentService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.dy.util.PropertiesUtil;

/**
 * 
 * @ClassName: ChiefManCommentController 
 * @Description: 厨师评论管理类
 * @author GZ 
 * @date 2017年5月18日 下午7:57:50 
 *
 */
@Controller
@RequestMapping(value ="chiefManComment")
public class ChiefManCommentController extends Component<ChiefComment> {
	private Logger log = LoggerFactory.getLogger(ChiefManCommentController.class);

	@Autowired
	private ChiefCommentService cheifCommentService;
	
	/**
	 * 订单评论列表页面
	 * @param mav
	 * @param request
	 * @param entity
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request){
		mav.setViewName("chiefComment/chiefManCommentList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<ChiefComment> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("厨师评论列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<ChiefComment> page = new Page<ChiefComment>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<ChiefComment> results = cheifCommentService.queryPage(page);
		
		for(ChiefComment ChiefComment :results){
			String payNumber = ChiefComment.getPayNumber();
			String property = PropertiesUtil.getProperty("order_pay_no_prifix");
			if(payNumber.contains(property)){
				String replace = StringUtils.replace(payNumber,property,"");
				ChiefComment.setPayNumber(replace);
			}
		}
		
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		return vo;
	}
	
	/**
	 *   删除 
	 */
	@RequestMapping(value="deleteChiefComment")
	public @ResponseBody ReturnVo<Object> deleteChiefComment(String cid,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			cheifCommentService.deleteByPrimaryKey(Long.parseLong(cid));
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
}
