package com.deyi.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Member;
import com.deyi.entity.MemberLevel;
import com.deyi.service.MemberLevelService;
import com.deyi.service.MemberService;
import com.deyi.util.Component;
import com.deyi.util.DateUtils;
import com.deyi.util.MD5S;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.dy.util.StringUtil;

import net.sf.json.JSONObject;

/**
 * 会员信息（顾客）
 * @author zjz
 *
 */
@Controller
@RequestMapping(value ="member")
public class MemberController extends Component<Member> {
	private Logger log = LoggerFactory.getLogger(MemberController.class);

	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberLevelService memberLevelServiceImpl;
	

	/**
	 * 会员列表页面
	 * @param mav
	 * @param request
	 * @param entity
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request,Member entity,Page<Member> page){
		List<MemberLevel> memberLevelList1= memberLevelServiceImpl.queryAll();
		List<MemberLevel> memberLevelList=new ArrayList<MemberLevel>();
		MemberLevel memberLevel=new MemberLevel();
		memberLevel.setId(0);
		memberLevel.setName("会员等级");
		memberLevelList.add(memberLevel);
		for (MemberLevel memberLevel1 : memberLevelList1) {
			memberLevelList.add(memberLevel1);
		}
		mav.addObject("memberLevelList",memberLevelList);
		mav.setViewName("member/memberList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<Member> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		Page<Member> page = new Page<Member>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<Member> results = memberService.queryPage(page);
		for (Member member : results) {
			if(!StringUtil.isBlank(member.getBirthday())){
				try {
					member.setAge(DateUtils.getAge(DateUtils.parse(member.getBirthday())));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
//		mav.addObject("page", page);
//		mav.setViewName("myGoods/goodsPage");
		return vo;
	}
	
	

	/**
	 *  停用
	 */
	@ResponseBody
	@RequestMapping(value="disableState")
	public  ReturnVo<Object> disableState(String memberId,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			Member member = memberService.selectByPrimaryKey(Integer.parseInt(memberId));
			member.setWxGroupid(2);
			memberService.updateByPrimaryKeySelective(member);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	

	/**
	 *   启用 
	 */
	@RequestMapping(value="enableState")
	public @ResponseBody ReturnVo<Object> enableState(String memberId,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			Member member = memberService.selectByPrimaryKey(Integer.parseInt(memberId));
			member.setWxGroupid(1);
			memberService.updateByPrimaryKeySelective(member);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
	/**
	 *   删除 
	 */
	@RequestMapping(value="deleteMember")
	public @ResponseBody ReturnVo<Object> deleteMember(String memberId,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			Member member = memberService.selectByPrimaryKey(Integer.valueOf(memberId));
			if(member.getIntegral()>0||member.getMoney().doubleValue()>0){
				vo.setSuccess(false);
				vo.setMessage("该用户有余额或积分，不允许删除");
				return vo;
			}
			member.setMemberRank(2);//(是否删除) 1未删除 2已删除
			memberService.updateByPrimaryKeySelective(member);
			//删除的时候把和会员有关的数据删掉
//			memberAddressMapper.deleteByPrimaryMemberId(memberId);//删除会员地址
//			memberIntegralService.deleteByMemberId(memberId);//删除会员积分
//			memberVoucherMapper.deleteByMemberId(Long.valueOf(memberId));//删除会员代金券
//			memberService.deleteByPrimaryKey2(Integer.parseInt(memberId));
			//删除会员的时候把会员的session清空.但目前是微信端的浏览器,实际上是清空不了微信端浏览器的session的,只能清空当前使用者的服务器的session
			//request.getSession().invalidate();
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("false");
		}
		return vo;
	}
	
	/**
	 *   恢复
	 */
	@RequestMapping(value="rollbackMember")
	public @ResponseBody ReturnVo<Object> rollbackMember(String memberId,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			Member member = memberService.selectByPrimaryKey(Integer.valueOf(memberId));
			member.setMemberRank(1);//(是否删除) 1未删除 2已删除
			memberService.updateByPrimaryKeySelective(member);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	/**
	 * 
	 * @Title: resetMemberPasswd 
	 * @Description: 重置会员id
	 * @param @param id  会员id
	 * @param @param request
	 * @param @return    设定文件 
	 * @return ReturnVo<Object>    返回类型 
	 * @throws
	 */
	@RequestMapping(value="resetMemberPasswd")
	public @ResponseBody ReturnVo<Object> resetMemberPasswd(String id,HttpServletRequest request){
		ReturnVo<Object> vo=new ReturnVo<>();
		try {
			Member member = memberService.selectByPrimaryKey(Integer.valueOf(id));
			member.setPayPassword(MD5S.GetMD5Code("888888"));
			memberService.updateByPrimaryKeySelective(member);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMessage("重置密码失败");
		}
		return vo;
	}

}
