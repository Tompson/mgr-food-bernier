package com.deyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.deyi.entity.DeliverMan;
import com.deyi.entity.SysUser;
import com.deyi.service.DeliverManService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "statDeliverMan")
public class StatDeliverManController extends Component<DeliverMan> {
	private Logger log = LoggerFactory.getLogger(StatDeliverManController.class);
	@Autowired
	private DeliverManService deliverManService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("statDeliverMan/statDeliverManList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<DeliverMan> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("配送统计Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<DeliverMan> page = new Page<DeliverMan>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<DeliverMan> results = deliverManService.deliverStat(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	
	
}
