package com.deyi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.CategoryGoods;
import com.deyi.entity.Goods;
import com.deyi.entity.SysUser;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "goods")
public class GoodsController extends Component<Goods> {
	private Logger log = LoggerFactory.getLogger(GoodsController.class);
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request) throws Exception {
		//查询所有菜品类型
		List<CategoryGoods> categoryList1= categoryGoodsService.queryAll();
		List<CategoryGoods> categoryList=new ArrayList<CategoryGoods>();
		CategoryGoods categoryGoods=new CategoryGoods();
		categoryGoods.setCategoryId(null);
		categoryGoods.setCgoodsName("类型");
		categoryList.add(categoryGoods);
		for (CategoryGoods categoryGoods1 : categoryList1) {
			categoryList.add(categoryGoods1);
		}
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		mav.addObject("sysUser", sysUser);
		mav.addObject("categoryList",categoryList);
		mav.setViewName("myGoods/goodsList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<Goods> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<Goods> page = new Page<Goods>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<Goods> results=null;
		//如果是门店则根据门店id和商品id查询 是否在售
		if(!"1".equals(sysUser.getType())){
			results = goodsService.queryPageByStore(page);
		}else{
		    results = goodsService.queryPage(page);
		}
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	/**
	 * 跳转新增商品
	 */
	@RequestMapping(value = "toGoodsAdd")
	public ModelAndView toGoodsAdd(ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
		List<CategoryGoods> categoryGoods = new ArrayList<CategoryGoods>();
		categoryGoods=categoryGoodsService.queryAll();
		mav.addObject("categoryGoods",categoryGoods);
		mav.setViewName("myGoods/goodsAdd");
		return mav;
	}
	/**
	 * 新增商品
	 */
	@ResponseBody
	@RequestMapping(value = "goodsAdd")
	public  ReturnVo<Object> goodsAdd(ModelAndView mav, HttpServletRequest request,Goods goods) { //菜品描述<p>111</p>
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			HttpSession session = request.getSession();
			Double price1 = goods.getPrice();
//			String price = StringUtils.formatMoney2Byte(price1);
			goods.setPrice(price1);
			
//			String goodsPic = "";
//			for(String g:goods.getGoodsPics()){
//				goodsPic+=g+",";
//			}
//			goods.setGoodsPic(goodsPic.substring(0, goodsPic.length()));
			long maxSort = goodsService.getMaxSort();
			goods.setSort(maxSort+1);
			goods.setStatus("0");
			goods.setGoodsname(goods.getGoodsname().trim());//去掉菜品名字的空格
			goodsService.insertSelective(goods);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}


	@RequestMapping(value = "toGoodsEdit")
	public ModelAndView toGoodsEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {

		if (id == null || "".equals(id)) {
			throw new Exception();
		}
		
		Goods goods = goodsService.selectByPrimaryKey(Long.valueOf(id));
		
		List<CategoryGoods> categoryGoods = new ArrayList<CategoryGoods>();
		categoryGoods=categoryGoodsService.queryAll();
		mav.addObject("categoryGoods",categoryGoods);
		mav.addObject("goods", goods);
		mav.setViewName("myGoods/goodsEdit");
		return mav;
	}

	@RequestMapping(value = "goodsEdit")
	public @ResponseBody ReturnVo<Object> goodsEdit(ModelAndView mav, HttpServletRequest request, Goods goods) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			Goods selectByPrimaryKey = goodsService.selectByPrimaryKey(goods.getId());
			Integer sellnum = selectByPrimaryKey.getSellnum();//统计的销售数量
			Goods selectGoodsCommentBygoodsId = goodsService.selectGoodsCommentBygoodsId(goods.getId());
			
//			String price = StringUtils.formatMoney2Byte(goods.getPrice());//保留两位小数
			goods.setPrice(goods.getPrice());
			//编辑菜品的时候如果菜品星期状态不等于输入的菜品星期状态就:把菜品的销售数量改为0
//			if(!selectByPrimaryKey.getWeeksFoodStatus().equals(goods.getWeeksFoodStatus())){
//				goods.setSellnum(0);
//			}
			
			goods.setGoodsname(goods.getGoodsname().trim());//去掉菜品名字的空格
			goodsService.updateByPrimaryKeySelective(goods);
			//正常更新菜品后,要把购物车里的菜品单价也更新掉
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 删除商品
	 */
	@ResponseBody
	@RequestMapping(value = "deleteGoods")
	public  ReturnVo<Object> deleteGoods(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			
			goodsService.deleteByPrimaryKey(Long.parseLong(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 已售完
	 * @Author zjz Create on 2016年10月21日 下午2:06:03
	 * @param reqGrid
	 * @param mav
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableState")
	public  ReturnVo<Object> disableState(HttpServletRequest request,String id) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("已售完......");
		try {
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		//根据门店id 和 菜品id 更新 在售或售完
		goodsService.updateByPrimaryByStoreIdAndGoodsId(sysUser.getStoreId(),new Long(id),"1");
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 在售
	 */
	@ResponseBody
	@RequestMapping(value = "enableState")
	public  ReturnVo<Object> enableState(String id, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("在售Start......");
		try {
			HttpSession session = request.getSession();
			SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
			//根据门店id 和 菜品id 更新 在售或售完
			goodsService.updateByPrimaryByStoreIdAndGoodsId(sysUser.getStoreId(),new Long(id),"0");
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
	
	
	/**
	 * 禁用
	 * @Author zjz Create on 2016年10月21日 下午2:06:03
	 * @param reqGrid
	 * @param mav
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableIntegral")
	public  ReturnVo<Object> disableIntegral(HttpServletRequest request,String id) {// 菜品状态: 1启用 2禁用       
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品禁用Start......");
		try {
		Goods goods = goodsService.selectByPrimaryKey(new Long(id));
		goods.setIntegral(2);
		goodsService.updateByPrimaryKeySelective(goods);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 启用
	 */
	@ResponseBody
	@RequestMapping(value = "enableIntegral")
	public  ReturnVo<Object> enableIntegral(String id, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品启用Start......");
		try {
			Goods goods = goodsService.selectByPrimaryKey(new Long(id));
			goods.setIntegral(1);//启用
			goodsService.updateByPrimaryKeySelective(goods);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
	/**
	 * 
	 * @Title: goodsTop 
	 * @Description: 菜品置顶
	 * @param id 菜品id
	 * @param request
	 * @return
	 * ReturnVo<Object>
	 * @throws
	 */
	@RequestMapping(value = "goodsTop")
	@ResponseBody
	public  ReturnVo<Object> goodsTop(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			HttpSession session = request.getSession();
			SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
			vo = goodsService.toTop(sysUser.getStoreId(),id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	
	
	/**
	 * 
	 * @Title: upMove 
	 * @Description: 菜品上移
	 * @param id 菜品id
	 * @param request
	 * @return
	 * ReturnVo<Object>
	 * @throws
	 */
	@RequestMapping(value = "upMove")
	@ResponseBody
	public  ReturnVo<Object> upMove(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			HttpSession session = request.getSession();
			SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
			vo = goodsService.moveUp(sysUser.getStoreId(),id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	
}
