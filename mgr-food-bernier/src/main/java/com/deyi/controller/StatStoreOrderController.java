package com.deyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.deyi.entity.DeliverMan;
import com.deyi.entity.SysOrder;
import com.deyi.entity.SysUser;
import com.deyi.service.DeliverManService;
import com.deyi.service.SysOrderService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "statStoreOrder")
public class StatStoreOrderController extends Component<SysOrder> {
	private Logger log = LoggerFactory.getLogger(StatStoreOrderController.class);
	@Autowired
	private SysOrderService sysOrderService;
	
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("staStoreOrder/statStoreOrderList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<SysOrder> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商户统计Start......");
		Page<SysOrder> page = new Page<SysOrder>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<SysOrder> results = sysOrderService.storeStat(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}

	
	
}
