package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.CategoryGoods;
import com.deyi.entity.Goods;
import com.deyi.entity.Store;
import com.deyi.entity.StoreCategory;
import com.deyi.entity.StoreGoods;
import com.deyi.entity.SysUser;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.StoreService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.MD5S;
import com.deyi.util.Page;
import com.deyi.util.ResponseWriteUtils;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.google.gson.Gson;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value="store")
public class StoreController extends Component<Store> {
	private Logger log = LoggerFactory.getLogger(DeliverManController.class);
	@Autowired
	private StoreService storeService;
	@Autowired
	private CategoryGoodsService  categoryGoodsService;
	@Autowired
	private GoodsService goodsService;
	 @RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		mav.addObject("sysUser", sysUser);
		mav.setViewName("store/storeList");
		return mav;
	}
	
	@RequestMapping(value="page")
	public @ResponseBody GridVo page(@RequestBody RequestGrid<Store> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setId(sysUser.getStoreId());
		}
		log.info("门店列表Start......");
		Page<Store> page = new Page<Store>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<Store> storeList=storeService.queryPage(page);
		vo.setRows(storeList);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}
	/**
	 * 禁用
	 * @Author zjz Create on 2016年10月21日 下午2:06:03
	 * @param reqGrid
	 * @param mav
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableState")
	public  ReturnVo<Object> disableState(HttpServletRequest request,String id) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品禁用Start......");
		try {
			Store store = storeService.selectByPrimaryKey(new Integer(id));
			store.setStatus("2");//禁用
			storeService.updateByPrimaryKeySelective(store,2);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 启用
	 */
	@ResponseBody
	@RequestMapping(value = "enableState")
	public  ReturnVo<Object> enableState(String id, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品启用Start......");
		try {
			Store store = storeService.selectByPrimaryKey(new Integer(id));
			store.setStatus("1");//启用
			storeService.updateByPrimaryKeySelective(store,2);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	@RequestMapping(value = "toStoreAdd")
	public ModelAndView toStoreAdd (ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
		mav.setViewName("store/storeAdd");
		return mav;
	}
	@ResponseBody
	@RequestMapping(value = "storeAdd")
	public  ReturnVo<Object> storeAdd(ModelAndView mav, HttpServletRequest request,Store store) {
		ReturnVo<Object> vo= new ReturnVo<Object>();
		try {
			HttpSession session = request.getSession();
			SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
			
			Store s=storeService.getStoreByAccount(store.getAccount());
			if(s!=null){
				vo.setSuccess(false);
				vo.setMessage("门店帐号已经存在，请重新选择！");
				return vo;
			}
			String passwd= MD5S.GetMD5Code("888888");
			store.setPasswd(passwd);
			storeService.savaStore(store,sysUser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMessage("添加失败");
		}
		
		return vo;
	}
	@RequestMapping(value = "toStoreEdit")
	public ModelAndView toStoreEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		SysUser userInfo = (SysUser) session.getAttribute(Constants.SESSION_USERINFO);
		/*if(userInfo==null){
			mav.addObject("errMsg", "请重新登录！");
			mav.setViewName("login");
			return mav; 
		}*/
		Store store = storeService.selectByPrimaryKey(Integer.parseInt(id));
		mav.addObject("store", store);
		mav.setViewName("store/storeEdit");
		return mav;
	}
	@RequestMapping(value="storeEdit")
	public @ResponseBody ReturnVo<Object> storeEdit(ModelAndView mav, HttpServletRequest request, Store store) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			if(store.getStorePhoto().equals("")){
				store.setStorePhoto(null);
			}
			storeService.updateByPrimaryKeySelective(store,2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	@RequestMapping(value="deleteStore")
	
	public @ResponseBody  ReturnVo<Object> deleteDeliverMan(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		storeService.deleteStoreById(Integer.parseInt(id));
		return vo;
	}
	@RequestMapping(value="resetStorePasswd")
	public @ResponseBody ReturnVo<Object> resetStorePasswd(String id,HttpServletRequest request){
		ReturnVo<Object> vo=new ReturnVo<>();
		try {
			Store store = storeService.selectByPrimaryKey(Integer.parseInt(id));
			store.setPasswd(MD5S.GetMD5Code("888888"));
			int  flag=1;
			storeService.updateByPrimaryKeySelective(store,flag);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMessage("重置密码失败");
		}
		return vo;
	}
	@RequestMapping(value="updateIsActivity")
	public @ResponseBody ReturnVo<Object> updateIsActivity(String id,HttpServletRequest request){
		ReturnVo<Object> vo=new ReturnVo<>();
		try {
			Store store = storeService.selectByPrimaryKey(Integer.parseInt(id));
			String isActivity = store.getIsActivity();
			if("1".equals(isActivity)){
				store.setIsActivity("2");
			}else if("2".equals(isActivity)){
				store.setIsActivity("1");
			}
			storeService.updateByPrimaryKeySelective(store,2);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	@RequestMapping(value="addStoreGoods")
	@Transactional
	public @ResponseBody void addStoreGoods(String id, long[] categoryId,String[] goodsId,
			HttpServletRequest request,HttpServletResponse response){
		ReturnVo<Object> vo=new ReturnVo<>();
		try {
			storeService.deleteStoreCategorysByStoreId(id);
			storeService.deleteStoreGoodsByStoreId(id);
			StoreCategory sc=null;
			if(categoryId!=null){
				for(int i=0;i<categoryId.length;i++){
					sc=new StoreCategory();
					sc.setStoreId(Long.parseLong(id));
					sc.setCategoryId(categoryId[i]);
					storeService.insertStoreCategory(sc);
				}
			}
			if(categoryId!=null){
				StoreGoods sg=null;
				for(int i=0;i<goodsId.length;i++){
					sg=new StoreGoods();
					sg.setStoreId(Long.parseLong(id));
					String[] split = goodsId[i].split("-");
					long cId=Long.parseLong(split[0]);
					long gId=Long.parseLong(split[1]);
					sg.setCategoryId(cId);
					sg.setGoodsId(gId);
					sg.setStatus("0");//默认在售
					//根据门店查询最大sort
					long maxSort = goodsService.getMaxSortByStore(Integer.parseInt(id));
					sg.setSort(maxSort+1);
					sg.setSellnum(0);//默认0
					storeService.insertStoreGoods(sg);
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setMessage("添加失败");
			vo.setData("1");
			vo.setSuccess(false);
			ResponseWriteUtils.returnAjax(response, vo);
			return;
		}
		vo.setMessage("添加成功");
		vo.setData("0");
		ResponseWriteUtils.returnAjax(response, vo);
	}
	
	
	@RequestMapping(value="toaddStoreGoods")
	public ModelAndView toaddStoreGoods (ModelAndView mav ,String id,HttpServletRequest request){
		List<String> categoryIds=storeService.getCategoryListByStoreId(Integer.parseInt(id));
		List<CategoryGoods> categoryList = categoryGoodsService.queryAll();
		for (CategoryGoods category: categoryList) {
			Long cId = category.getCategoryId();
			if(categoryIds.contains(cId+"")){
				category.setCheck(true);
			}else{
				category.setCheck(false);
			}
			List<String> goodIds = storeService.getGoodsIdListByStoreId(Long.parseLong(id),cId);
			List<Goods> goodsList=goodsService.queryAllByCategoryId(cId);
			for (Goods goods : goodsList) {
				if(goodIds.contains(goods.getId()+"")){
					goods.setCheck(true);
				}else{
					goods.setCheck(false);
				}
			}
			category.setGoodsList(goodsList);
		}
		 mav.addObject("categoryList", new Gson().toJson(categoryList));
		 mav.addObject("storeId", id);
		 mav.setViewName("store/toAddGoods");
		 return mav;
	}
	
}
