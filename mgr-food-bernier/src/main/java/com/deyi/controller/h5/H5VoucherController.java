package com.deyi.controller.h5;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Member;
import com.deyi.entity.MemberIntegral;
import com.deyi.entity.MemberVoucher;
import com.deyi.entity.Voucher;
import com.deyi.model.ResultVo;
import com.deyi.model.vo.MemberVoucherVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.Page;

@Controller
@RequestMapping(value="h5/voucher")
public class H5VoucherController {
private Logger LOG = LoggerFactory.getLogger(H5StoreController.class);
	

	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	
	/**
	 * 我的代金券列表
	 * @author limh Create on 2016年11月14日 下午8:39:30
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "voucher")
	public ModelAndView voucher(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Page<MemberVoucherVo> page = new Page<>();
		page.getParams().put("memberId", member.getMemberId());
		List<MemberVoucherVo> memberVoucherList = memberVoucherService.selectByPageExtra(page);
		page.setResults(memberVoucherList);
		mav.addObject("page", page);
		mav.setViewName("h5/user/voucher");
		return mav;
	}
	
	/**
	 * 
	 * @Title: voucherShare 
	 * @Description: 代金券分享页面
	 * @param mav
	 * @param httpRequest
	 * @param voucherId
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "voucherShare",method=RequestMethod.GET)
	public ModelAndView voucherShare(ModelAndView mav, HttpServletRequest httpRequest,String voucherId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		MemberVoucher memberVoucher = memberVoucherService.selectByPrimaryKey(Long.valueOf(voucherId));
		MemberVoucherVo voucher = memberVoucherService.selectVoucherByKey(memberVoucher);
		
		mav.addObject("voucher", voucher);
		mav.addObject("memberId", member.getMemberId());
		mav.setViewName("h5/user/voucherShare");
		return mav;
	}
	
	/**
	 * 
	 * @Title: voucherGain 
	 * @Description: 代金券领取页面
	 * @param mav
	 * @param httpRequest
	 * @param voucherId 个人拥有的代金券id
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "voucherGain",method=RequestMethod.GET)
	public ModelAndView voucherGain(ModelAndView mav, HttpServletRequest httpRequest,String voucherId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		MemberVoucher memberVoucher = memberVoucherService.selectByPrimaryKey(Long.valueOf(voucherId));
		MemberVoucherVo voucher = memberVoucherService.selectVoucherByKey(memberVoucher);
		Long presenterId = voucher.getPresenterId();
		if(presenterId!=null){
			Member member = memberService.selectByPrimaryKey(presenterId.intValue());
			mav.addObject("member",member);
		}
		mav.addObject("voucher", voucher);
		mav.setViewName("h5/user/voucherGain");
		return mav;
	}
	
	/**
	 * 
	 * @Title: gain 
	 * @Description: 领取代金券
	 * @param mav
	 * @param httpRequest
	 * @param voucherId 个人拥有的代金券id
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "api/gain",method=RequestMethod.POST)
	public ResultVo<Object> gain(ModelAndView mav, HttpServletRequest httpRequest,String voucherId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> vo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		MemberVoucher memberVoucher = memberVoucherService.selectByPrimaryKey(Long.valueOf(voucherId));
		if(member==null || memberVoucher ==null){
			vo.setSuccess(false);
			vo.setMsg("信息获取失败");
			return vo;
		}
		
		System.out.println(memberVoucher.getPresenterId()!=null &&memberVoucher.getMemberId() !=memberVoucher.getPresenterId());
		if(member.getMemberId().longValue() == memberVoucher.getMemberId()&&memberVoucher.getPresenterId()==null){
			vo.setSuccess(false);
			vo.setMsg("不能领取自己分享的代金券");
			return vo;
		}
		if("3".equals(memberVoucher.getUseStatus())){//1.不可使用，2.未使用，3.已使用
			//已使用
			vo.setSuccess(false);
			vo.setMsg("不能领取已使用的代金券");
			return vo;
		}
		
		/*if(memberVoucher.getPresenterId()!=null &&memberVoucher.getMemberId() !=memberVoucher.getPresenterId()){
			//已领取
			vo.setSuccess(false);
			vo.setMsg("手太慢了哦~代金券已被他人领取");
			return vo;
		}*/
		ResultVo<Object> vo1 =memberVoucherService.gainVoucher(member.getMemberId(),memberVoucher);
		/*MemberVoucher memberVoucher2 = new MemberVoucher();
		memberVoucher2.setId(memberVoucher.getId());
		memberVoucher2.setMemberId(member.getMemberId().longValue());
		memberVoucherService.updateByPrimaryKeySelective(memberVoucher2);*/
		return vo1;
	}
	
	/**
	 * 
	 * @Title: share 
	 * @Description: 分享代金券
	 * @param mav
	 * @param httpRequest
	 * @param voucherId 个人拥有的代金券id
	 * @return
	 * ResultVo<Object>
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "api/share",method=RequestMethod.POST)
	public ResultVo<Object> share(ModelAndView mav, HttpServletRequest httpRequest,String voucherId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> vo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		MemberVoucher memberVoucher = memberVoucherService.selectByPrimaryKey(Long.valueOf(voucherId));
		if(member==null || memberVoucher ==null){
			vo.setSuccess(false);
			vo.setMsg("信息获取失败");
			return vo;
		}
		
		if(member.getMemberId().longValue()!=memberVoucher.getMemberId().longValue()){
			//不是当前登录用户的代金券
			vo.setSuccess(false);
			vo.setMsg("不能分享他人的代金券");
			return vo;
		}
		
		if("3".equals(memberVoucher.getUseStatus())){//1.不可使用，2.未使用，3.已使用
			//已使用
			vo.setSuccess(false);
			vo.setMsg("不能分享已使用的代金券");
			return vo;
		}
		
		MemberVoucher memberVoucher2 = new MemberVoucher();
		memberVoucher2.setId(memberVoucher.getId());
		memberVoucher2.setPresenterId(member.getMemberId().longValue());
		memberVoucherService.updateByPrimaryKeySelective(memberVoucher2);
		return vo;
	}
	
	/**
	 * 我的代金券列表滚动刷新
	 * @author limh Create on 2016年11月14日 下午8:23:35
	 * @param httpRequest
	 * @param page
	 * @return 下一页的数据
	 */
	@ResponseBody
	@RequestMapping(value = "api/getVoucherList", method = RequestMethod.POST)
	public ResultVo<Object> getVoucherList(HttpServletRequest httpRequest, Page<MemberVoucherVo> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if(page==null){
			page=new Page<>();
		}
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			page.getParams().put("memberId", member.getMemberId());
			List<MemberVoucherVo> memberVoucherList = memberVoucherService.selectByPageExtra(page);
			page.setResults(memberVoucherList);
			resultVo.setData(page);
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
}
