package com.deyi.controller.h5;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deyi.model.ResultVo;
import com.deyi.model.vo.OrderCommentVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.Page;

@Controller
@RequestMapping(value="h5/comment")
public class H5CommontController {
private Logger LOG = LoggerFactory.getLogger(H5Controller.class);
	
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	@ResponseBody
	@RequestMapping(value = "api/getCommentList", method = RequestMethod.POST)
	public ResultVo<Object> getCommentList(HttpServletRequest httpRequest, Page<OrderCommentVo> page,Long goodsId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if (page == null) {
			page = new Page<>();
		}
		page.getParams().put("goodsId", goodsId);
		try{
			List<OrderCommentVo> commentVoList = orderCommentService.selectGoodsCommemtByGoodsId(page);
			if(commentVoList==null||commentVoList.isEmpty()){
				resultVo.setSuccess(false);
				resultVo.setMsg("没有更多评论啦！");
				return resultVo;
			}
			page.setResults(commentVoList);
			resultVo.setData(page);
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
