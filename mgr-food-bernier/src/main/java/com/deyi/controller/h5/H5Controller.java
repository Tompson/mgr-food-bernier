package com.deyi.controller.h5;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.CarouselFigure;
import com.deyi.entity.CategoryGoods;

import com.deyi.entity.Goods;
import com.deyi.entity.GroupPurchase;
import com.deyi.entity.Member;
import com.deyi.entity.MemberAddress;
import com.deyi.entity.MemberIntegral;
import com.deyi.entity.MemberVoucher;
import com.deyi.entity.OrderComment;
import com.deyi.entity.Part;
import com.deyi.entity.ShopTrolley;
import com.deyi.entity.Store;
import com.deyi.entity.StoreGoods;
import com.deyi.entity.SysCoupon;
import com.deyi.entity.SysOrder;
import com.deyi.entity.SysParm;
import com.deyi.entity.SysUser;
import com.deyi.entity.Voucher;
import com.deyi.model.BaiDuResponse;
import com.deyi.model.BookType;
import com.deyi.model.ConditionModel;
import com.deyi.model.Location;
import com.deyi.model.ResponseTrade;
import com.deyi.model.ResultVo;
import com.deyi.model.VoucherQueryCondition;
import com.deyi.model.vo.Ad;
import com.deyi.model.vo.GoodsInfo;
import com.deyi.model.vo.GoodsVo;
import com.deyi.model.vo.MemberVoucherVo;
import com.deyi.model.vo.OrderCommentVo;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.model.vo.SysOrderVo;
import com.deyi.model.vo.WeeknumStatistics;
import com.deyi.net.BaiduMapApiClient;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopCarService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysCouponService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.AlipayUtil;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.DateUtils;
import com.deyi.util.IpUtils;
import com.deyi.util.MessageUtil;
import com.deyi.util.Page;
import com.deyi.util.StringUtils;
import com.deyi.util.TimeUtil;
import com.deyi.util.wxmppay.XtwUnifiedOrderHelper;
import com.dy.util.StringUtil;
import com.google.gson.Gson;

import net.sf.json.JSONArray;
import net.sf.json.groovy.GJson;

@Controller
@RequestMapping(value = "h5")
public class H5Controller {
	private Logger LOG = LoggerFactory.getLogger(H5Controller.class);
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	//
	@Autowired
	private MemberAddressService memberAddressService;
	//
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	//
	@Autowired
	private OrderCommentService orderCommentService;
	
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	//
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;

	
	
	@Autowired
	private  MemberVoucherService memberVoucherService;
	
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	@Autowired
	private SysCouponService sysCouponService ;
	@Autowired
	private ShopCarService shopCarService;
	
	/*********************************
	 * 首页
	 **********************************/
	/**
	 * 首页
	 * @author limh Create on 2016年10月29日 上午11:31:19
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "index")
	public ModelAndView index(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
		
		//商家定位，暂时配置地理位置
		Location partnerLocation = new Location();
		partnerLocation.setLat(BizHelper.getPartnerLocationLat());
		partnerLocation.setLng(BizHelper.getPartnerLocationLng());
		mav.addObject("partnerAddress", BizHelper.getPartnerAddress());
		mav.addObject("partnerLocation", partnerLocation);
		SysParm realSysParam = sysParmService.getRealSysParam();
		if(realSysParam!=null){
			mav.addObject("deliveryRadius", realSysParam.getDeliverScope());
			
		}
		List<Store> stores = storeService.queryAllStore();
		List<Map<String, Object>> data=new ArrayList<Map<String, Object>>();
		mav.addObject("stores", new Gson().toJson(stores));
		//百度地图 ak
		mav.addObject("bmapAK", BizHelper.getBaiduMapApiAk());
		List<CarouselFigure> carouselList =carouselFigureService.selectTopFiveCarouselFigure();
		mav.addObject("carouselList", carouselList);
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member.getGeoLat()==null||member.getGeoLng()==null){
			//ip定位
			String ipAddress = IpUtils.getIpAddress(httpRequest);
			String[] ipxy = IpUtils.getIPXY(ipAddress);
			if(ipxy!=null){
				member.setGeoLng(new BigDecimal(ipxy[0]) );
				member.setGeoLat(new BigDecimal(ipxy[1]) );
			}else{
				member.setGeoLng(new BigDecimal(113.871844));
				member.setGeoLat(new BigDecimal(22.589576));
			}
			memberService.updateByPrimaryKeySelective(member);
		}
		mav.addObject("member", member);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		mav.setViewName("h5/index");
		return mav;
	}
	
	@RequestMapping(value = "error")
	public ModelAndView errorHandle(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		String url="";
		if(wxopenid!=null){
			//app登录
			url="/h5/index.html";
		}
		SysUser userInfo = (SysUser)httpRequest.getSession().getAttribute(Constants.SESSION_USERINFO);
		if(userInfo!=null){
			//后台管理登录
			url="/system/login.html";
		}
		mav.addObject("url", url);
		mav.setViewName("h5/error");
		return mav;
	}
	/********************************
	 * 门店模块
	 *********************************/
	@RequestMapping(value="storeHome")
	public ModelAndView storeHome(ModelAndView mav, HttpServletRequest httpRequest,Page<Store> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);

		page.getParams().put("lat", member.getGeoLat().doubleValue());
		page.getParams().put("lng", member.getGeoLng().doubleValue());
		List<Store> stores=storeService.wxgetStoresList(page);
		for (Store store : stores) {
			List<Part> parts=partService.wxgetPartsByStoreId(store.getId());
			store.setParts(parts);
		}
		page.setResults(stores);
		mav.addObject("stores", stores);
		mav.addObject("page", page);
		mav.setViewName("h5/store/storelist");
		return mav;
	}
	@RequestMapping(value="storeHome1")
	
	public @ResponseBody Page<Store> storeHome1(HttpServletRequest httpRequest,Page<Store> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);

		page.getParams().put("lat", member.getGeoLat().doubleValue());
		page.getParams().put("lng", member.getGeoLng().doubleValue());
		List<Store> stores=storeService.wxgetStoresList(page);
		for (Store store : stores) {
			List<Part> parts=partService.wxgetPartsByStoreId(store.getId());
			store.setParts(parts);
		}
		page.setResults(stores);
		return page;
	}
	@RequestMapping(value="msgStore")
	 public ModelAndView msgStore(ModelAndView mav, HttpServletRequest httpRequest,String id) {
		try {
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			Store store = storeService.selectByPrimaryKey(Integer.parseInt(id));
			try {
				double distance = StringUtils.getDistance(member.getGeoLng().doubleValue(), member.getGeoLat().doubleValue(), store.getLng(), store.getLat());
				BigDecimal b=new   BigDecimal(distance/1000);  
				double f1  =b.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();  
				store.setDistance(f1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				store.setDistance(0.0);
			}
			List<Part> parts=partService.wxgetPartsByStoreId(store.getId());
			store.setParts(parts);
			List<CategoryGoods> categoryList=categoryGoodsService.wxgetCategoryLsit(store.getId());
			for (CategoryGoods categoryGoods : categoryList) {
				List<Goods> goodsList=goodsService.wxgetGoodListByStoreIdAndCategoryId(store.getId(),categoryGoods.getCategoryId());
				categoryGoods.setGoodsList(goodsList);
			}
		
			mav.addObject("store", store);
			mav.addObject("categoryList", categoryList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mav.setViewName("h5/store/msgstore");
		return mav;
	}
	/*********************************
	 * 菜品模块
	 **********************************/
	/**
	 *  菜品主页，今日菜品
	 * @author limh Create on 2016年10月29日 上午11:31:06
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "productHome")
	public ModelAndView productHome(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		/*List<SysParm> sysParams = sysParmService.selectAll();
		if(sysParams!=null&&!sysParams.isEmpty()){
			SysParm sysParm = sysParams.get(0);
			String advertImage = sysParm.getAdvertImage();
			String[] imgs = advertImage.split(",");
			List<Ad> adList=new ArrayList<>();
			//广告1
			Ad ad1 = new Ad();
			ad1.setImg(imgs[0]);
			ad1.setLink(sysParm.getImgLinkA());
			adList.add(ad1);
			//广告2
			Ad ad2 = new Ad();
			ad2.setImg(imgs[1]);
			ad2.setLink(sysParm.getImgLinkB());
			adList.add(ad2);
			//广告3
			Ad ad3 = new Ad();
			ad3.setImg(imgs[2]);
			ad3.setLink(sysParm.getImgLinkC());
			adList.add(ad3);
			mav.addObject("adList", adList);
		}*/
		String currentDayOfWeek = BizHelper.getConvertedDayOfWeek();
		Page<GoodsVo> page = new Page<>();
		page.setPageSize(Constants.PAGE_SIZE_FOR_GOODS);
		page.getParams().put("weeksFoodStatus", currentDayOfWeek);
		List<GoodsVo> goodsVoList = goodsService.selectAvgScore4Goods(page);
		List<CategoryGoods> categoryList = categoryGoodsService.queryAll();
		mav.addObject("categoryList", categoryList);
		page.setResults(goodsVoList);
		mav.addObject("page", page);
		
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			ConditionModel condition = new ConditionModel();
			condition.getParams().put("memberId", member.getMemberId());
			condition.getParams().put("weeknum", currentDayOfWeek);
			double amountPrice = shopTrolleyService.amountPriceByWeeknum(condition);
			mav.addObject("amountPrice", StringUtils.formatMoney2Byte(amountPrice));
		}
		mav.setViewName("h5/product/productHome");
		return mav;
	}
	
	@RequestMapping(value = "groupBuyDesc")
	public ModelAndView groupBuyDesc(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
			SysParm realSysParam = sysParmService.getRealSysParam();
			String partnerPhone = realSysParam.getBookDiscount();
			mav.addObject("partnerPhone", partnerPhone);
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.setViewName("h5/other/groupBuyDesc");
		return mav;
	}
	
	/**
	 * 菜品详情页面
	 * @author limh Create on 2016年10月29日 上午11:30:54
	 * @param mav
	 * @param httpRequest
	 * @param goodsId
	 * @return
	 */
	@RequestMapping(value = "productDetail")
	public ModelAndView productDetail(ModelAndView mav, HttpServletRequest httpRequest, Long goodsId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		if (goodsId != null) {
			Goods goods = goodsService.selectGoodsDetailByPrimaryKey(goodsId);
			mav.addObject("goods", goods);
			// 订单评论表查出所有评论
			Page<OrderCommentVo> page = new Page<>();
			page.getParams().put("goodsId", goodsId);
			List<OrderCommentVo> commentVoList = orderCommentService.selectGoodsCommemtByGoodsId(page);
			mav.addObject("commentVoList", commentVoList);
			mav.addObject("page", page);
			String currentDayOfWeek = BizHelper.getConvertedDayOfWeek();
			mav.addObject("currentDayOfWeek", currentDayOfWeek);
			String serverUrl = BizHelper.getServerUrl();
			mav.addObject("serverUrl", serverUrl);
			mav.setViewName("h5/product/productDetail");
			return mav;
		}
		// TODO 异常
		return mav;
	}

	/**
	 * 星期菜品展示页面
	 * @author limh Create on 2016年10月29日 上午11:30:43
	 * @param mav
	 * @param httpRequest
	 * @param week
	 * @return
	 */
	@RequestMapping(value = "productOfWeek")
	public ModelAndView productOfWeek(ModelAndView mav, HttpServletRequest httpRequest,String week,String toWeek) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		Page<GoodsVo> page = new Page<>();
		page.setPageSize(Constants.PAGE_SIZE_FOR_GOODS);
		String weeknum=null;
		if(week!=null){
			weeknum=week;
		}else{
			int currentDayOfWeek = TimeUtil.getCurrentDayOfWeeK();
			if(toWeek!=null&&"tomorrow".equalsIgnoreCase(toWeek)){
				currentDayOfWeek++;
				if(currentDayOfWeek>7){
					currentDayOfWeek=1;
				}
			}
			String currentWeek=String.valueOf(currentDayOfWeek);
			mav.addObject("currentDayOfWeek", currentWeek);
			weeknum=currentWeek;
			//传入当前星期到页面，用于页面选择，刷新其他页面使用ajax异步刷新
		}
		mav.addObject("week", weeknum);
		page.getParams().put("weeksFoodStatus", weeknum);
		List<GoodsVo> goodsVoList = goodsService.selectAvgScore4Goods(page);
		List<CategoryGoods> categoryList = categoryGoodsService.queryAll();
		mav.addObject("categoryList", categoryList);
		page.setResults(goodsVoList);
		mav.addObject("page", page);
		//统计每天的总价
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			ConditionModel condition = new ConditionModel();
			condition.getParams().put("memberId", member.getMemberId());
			condition.getParams().put("weeknum", weeknum);
			double amountPrice = shopTrolleyService.amountPriceByWeeknum(condition);
			mav.addObject("amountPrice", StringUtils.formatMoney2Byte(amountPrice));
		}
		mav.setViewName("h5/product/productOfWeek");
		return mav;
	}

	/******* api *****/
	/**
	 * 获取星期几当天的菜品
	 * @author limh Create on 2016年10月29日 上午11:29:22
	 * @param httpRequest
	 * @param page
	 * @param week
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getWeekProduct", method = RequestMethod.POST)
	public ResultVo<Object> getWeekProduct(HttpServletRequest httpRequest, Page<GoodsVo> page, String week) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if (page == null) {
			page = new Page<>();
		}
		page.setPageSize(Constants.PAGE_SIZE_FOR_GOODS);
		page.getParams().put("weeksFoodStatus", week);
		List<GoodsVo> goodsVoList = goodsService.selectAvgScore4Goods(page);
		List<CategoryGoods> categoryList = categoryGoodsService.queryAll();
		GoodsInfo goodsInfo = new GoodsInfo();
		goodsInfo.setGoodsVoList(goodsVoList);
		goodsInfo.setCategoryList(categoryList);
		goodsInfo.setPage(page);
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			ConditionModel condition = new ConditionModel();
			condition.getParams().put("memberId", member.getMemberId());
			condition.getParams().put("weeknum", week);
			double amountPrice = shopTrolleyService.amountPriceByWeeknum(condition);
			goodsInfo.setAmountPrice(StringUtils.formatMoney2Byte(amountPrice));
		}
		resultVo.setData(goodsInfo);
		return resultVo;
	}
	
	@ResponseBody
	@RequestMapping(value = "api/getCommentList", method = RequestMethod.POST)
	public ResultVo<Object> getCommentList(HttpServletRequest httpRequest, Page<OrderCommentVo> page,Long goodsId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if (page == null) {
			page = new Page<>();
		}
		page.getParams().put("goodsId", goodsId);
		try{
			List<OrderCommentVo> commentVoList = orderCommentService.selectGoodsCommemtByGoodsId(page);
			if(commentVoList==null||commentVoList.isEmpty()){
				resultVo.setSuccess(false);
				resultVo.setMsg("没有更多评论啦！");
				return resultVo;
			}
			page.setResults(commentVoList);
			resultVo.setData(page);
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 菜品分享，给予积分奖励
	 * @author limh Create on 2016年10月29日 下午2:45:24
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getShareAward", method = RequestMethod.GET)
	public ResultVo<Object> getShareAward(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			int shareAward = memberService.getShareAward(member);
			if(shareAward>=0){
				resultVo.setData(shareAward);
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	
	/**
	 * 微信分享页面数据初始化
	 * @author limh Create on 2016年10月31日 下午9:19:35
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/initWxConfig", method = RequestMethod.GET)
	public ResultVo<Object> getWxConfig(HttpServletRequest httpRequest,String url) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			String accessToken = wxAccessTokenService.getAccessToken().getToken();
			Map<String, Object> wxInitConfig = BizHelper.getWxInitConfig(accessToken,url);
			resultVo.setData(wxInitConfig);
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			resultVo.setSuccess(false);
			return resultVo;
		}
	}
	
	/**
	 * 立即购买
	 * @deprecated v3版本开始废弃
	 * @author limh Create on 2016年10月31日 下午9:46:58
	 * @param httpRequest
	 * @param goodsId 菜品id
	 * @param amount 数量
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/immediatelyBuy", method = RequestMethod.GET)
	public ResultVo<Object> immediatelyBuy(HttpServletRequest httpRequest,Long goodsId,Integer amount){
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if(goodsId!=null){
			//TODO 判断会员禁用，菜品禁用，等
			Goods goods = goodsService.selectByPrimaryKey(goodsId);
			Double price = goods.getPrice();
			if(amount==null){
				amount=1;
			}
			double amountPrice=price*amount;
			SysParm realSysParam = sysParmService.getRealSysParam();
			double startPrice = realSysParam.getStartPrice().doubleValue();
			if(amountPrice<startPrice){
				resultVo.setSuccess(false);
				resultVo.setMsg("订单总价低于起步价,是否将商品加入购物车？");
				return resultVo;
			}
			/*BookType BookType = (BookType) httpRequest.getSession().getAttribute(Constants.SESSION_BOOK_TYPE);
			if(BookType==null){
				resultVo.setSuccess(false);
				resultVo.setMsg("您没有选择订购类型，是否现在就去选择!");
				return resultVo;
			}
			//预定类型
			String bookType = BookType.getBookType();*/
//			String week = goods.getWeeksFoodStatus();
			/*int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
			int bookWeek=currentDayOfWeeK;
			if(Constants.BOOK_TYPE_BOOK_TOMORROW_LUNCH.equals(bookType)){
				bookWeek=currentDayOfWeeK+1;
			}
			if(bookWeek>7){
				bookWeek=1;
			}
			if(!week.equals(String.valueOf(bookWeek))){
				String str="";
				switch(bookWeek){
				case 1:
					str="星期一";
					break;
				case 2:
					str="星期二";
					break;
				case 3:
					str="星期三";
					break;
				case 4:
					str="星期四";
					break;
				case 5:
					str="星期五";
					break;
				case 6:
					str="星期六";
					break;
				case 7:
					str="星期天";
					break;
				}
				resultVo.setSuccess(false);
				resultVo.setMsg("您目前预定的是"+str+"的菜品，不能再预定其他日期的菜品了！");
				return resultVo;
			}*/
			return resultVo;
		}
		return null;
	}
	/**
	 * 检查当前星期是否是当天，如果是当天则判断是否超过当天的最后营业时间
	 * @author limh Create on 2016年11月29日 下午12:10:50
	 * @param httpRequest
	 * @param week
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/checkCurrentWeek", method = RequestMethod.GET)
	public ResultVo<Object> checkCurrentWeek(HttpServletRequest httpRequest, String week) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try {
			boolean checkCurrentWeek = bizService.checkCurrentWeek(week);
			if(checkCurrentWeek){
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_003);
				resultVo.setMsg("今天已经打烊啦！到本周菜品展示页面预订其他天的菜品吧！");
				return resultVo;
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		//不是当天的不考虑营业时间，当天没有超过营业时间也不考虑
		return resultVo;
	}
	
	/**
	 * 获取当前的营业状况
	 * @deprecated v3版本开始废弃
	 * @author limh Create on 2016年11月22日 下午8:07:35
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getBussessStatus", method = RequestMethod.GET)
	public ResultVo<Object> getBussessStatus(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		SysParm realSysParam = sysParmService.getRealSysParam();
		if(realSysParam!=null){
			int now = TimeUtil.getTimeQuantum(new Date());
			//上午营业时间
			int startTimeAm =TimeUtil.getTimeQuantum(realSysParam.getStartTime());
			int endTimeAm = TimeUtil.getTimeQuantum(realSysParam.getEndTime());
			//下午营业时间
			int startTimePm =TimeUtil.getTimeQuantum(realSysParam.getStartTimePm());
			int endTimePm = TimeUtil.getTimeQuantum(realSysParam.getEndTimePm());
			if(now<startTimeAm||(now>endTimeAm&&now<startTimePm)){
				// 不在营业时间内
				resultVo.setSuccess(false);
				resultVo.setMsg("1");
				return resultVo;
			}
			if(now>endTimePm){
				// 不在营业时间内
				resultVo.setSuccess(false);
				resultVo.setMsg("2");
				return resultVo;
			}
		}
		return resultVo;
	}
	
	/**
	 * 重新设置订购类型
	 * @deprecated v3版本开始废弃
	 * @author limh Create on 2016年11月22日 下午8:41:33
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/resetBookType", method = RequestMethod.GET)
	public ResultVo<Object> resetBookType(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			HttpSession session = httpRequest.getSession();
			session.removeAttribute(Constants.SESSION_BOOK_TYPE);
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			resultVo.setSuccess(false);
			return resultVo;
		}
	}
	
	/**
	 * 保存团餐申请的公司信息
	 * @author limh Create on 2016年11月28日 下午3:59:25
	 * @param httpRequest
	 * @param record 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/saveCompanyInfo", method = RequestMethod.POST)
	public ResultVo<Object> saveCompanyInfo(HttpServletRequest httpRequest,GroupPurchase record) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			record.setMemberId(member.getMemberId());
			record.setCreateTime(new Date());
			record.setWxOpenid(wxopenid);
			groupPurchaseService.insertSelective(record);
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			resultVo.setSuccess(false);
			return resultVo;
		}
	}
	
	/*********************************
	 * 订单模块
	 **********************************/
	/**
	 * 跳转订单评论页面
	 * @author limh Create on 2016年10月29日 上午11:29:09
	 * @param mav
	 * @param httpRequestL
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "evaluate", method = RequestMethod.GET)
	public ModelAndView evaluate(ModelAndView mav, HttpServletRequest httpRequest, Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		SysOrder sysOrder = sysOrderService.queryOrderById2(orderId);
		
		mav.addObject("sysOrder",sysOrder);
		mav.addObject("orderId", orderId);
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order.getOrderType().equals(Constants.ORDER_EatingInStore)||order.getOrderType().equals(Constants.ORDER_DaBao)){
			//堂食，打包
			mav.setViewName("h5/order/eatingEvaluate");
		}else{
			mav.setViewName("h5/order/evaluate");
		}
		
		return mav;
	}

	/**
	 * 订单确认页面
	 * @author limh Create on 2016年10月29日 上午11:29:01
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "orderConfirm")
	public ModelAndView orderConfirm(ModelAndView mav, HttpServletRequest httpRequest,Long goodsId,Integer amount,String weeknum,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
		httpRequest.getSession().removeAttribute(Constants.SESSION_ADDRESS_BACK_CONFIRM);
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			SysParm realSysParam = sysParmService.getRealSysParam();
			//查询所有地址
			Page<MemberAddress> page = new Page<>();
			page.getParams().put("memberId", member.getMemberId());
			List<MemberAddress> allAddress = memberAddressService.selectByPage(page);
			if(allAddress!=null&&!allAddress.isEmpty()){
				mav.addObject("allAddress", allAddress);
			}else{
				//没有设置任何地址，跳转到地址页面设置地址
				mav.setViewName("redirect:addressList.html");
				String concat="?";
				if(goodsId!=null){
					concat+="goodsId="+goodsId+"&amount="+amount+"&weeknum="+weeknum;
				}else{
					concat+="weeknum="+weeknum;
				}
				httpRequest.getSession().setAttribute(Constants.SESSION_ADDRESS_BACK_CONFIRM,httpRequest.getServletPath().replace("/h5/", "").concat(concat));
				Log.info("session存入 back_confirm------>   "+httpRequest.getServletPath().replace("/h5/", "").concat(concat));
				return mav;
			}
			// 查询默认收货地址
			List<MemberAddress> addressList = memberAddressService.selectDefaultAddressByMemberId(member.getMemberId());
			if(addressList!=null&&!addressList.isEmpty()){
				mav.addObject("defaultAddress", addressList.get(0));
			}
			// 待确认订单信息，1.购物车中所有菜品 2.直接购买的菜品
			Integer memberId = member.getMemberId();
			double amountPrice=0;
			if(goodsId!=null){
				Goods goods = goodsService.selectByPrimaryKey(goodsId);
				if(goods!=null){
					Double price = goods.getPrice();
					if(amount==null){
						amount=1;
					}
					amountPrice=price*amount;	
				}
				mav.addObject("goods", goods);
				mav.addObject("amount", amount);
				mav.addObject("subtotalPrice", amountPrice);
			}else{
				ConditionModel condition = new ConditionModel();
				condition.getParams().put("memberId", memberId);
				condition.getParams().put("weeknum", weeknum);
				List<ShopTrolleyVo> cartList = shopTrolleyService.selectByCondition(condition);
				amountPrice = shopTrolleyService.amountPriceByWeeknum(condition);
				mav.addObject("cartList", cartList);
			}
			//判断总价是否小于起步价
			double startPrice = realSysParam.getStartPrice().doubleValue();
			mav.addObject("startPrice",startPrice);
			if(amountPrice<startPrice){
				mav.setViewName("redirect:productHome.html");
				return mav;
			}
			//配送费
			double deliverPrice = realSysParam.getDeliverPrice().doubleValue();
			mav.addObject("deliverPrice",deliverPrice);
			amountPrice=amountPrice+deliverPrice;
			//总价，商品总价+配送费-优惠价，此时积分未换算
			mav.addObject("amountPrice", StringUtils.formatMoney2Byte(amountPrice));
			//查询符合条件的优惠券
			VoucherQueryCondition record = new VoucherQueryCondition();
			record.setMemberId(member.getMemberId());
			record.setAmountPrice(amountPrice);
			List<MemberVoucherVo> voucherList = memberVoucherService.selectSatisfactoryVoucher(record);
			mav.addObject("voucherList",voucherList);
			//当前星期数对应的日期
			int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
			int temp=Integer.parseInt(weeknum)-currentDayOfWeeK;
			if(temp<0){
				temp+=7;
			}
			Date addDay = TimeUtil.addDay(temp);
			mav.addObject("currentDate", TimeUtil.getSTD_DF_10(addDay));
			mav.addObject("weeknum", weeknum);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("storeId", storeId);
		mav.setViewName("h5/order/orderConfirm");
		return mav;
	}
	
	/**
	 * 订单列表页面
	 * @author limh Create on 2016年10月29日 上午11:28:50
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "orderList")
	public ModelAndView orderList(ModelAndView mav, HttpServletRequest httpRequest,String fromType,Page<SysOrderVo> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		page.setPageSize(10);
		if (member != null) {
			page.getParams().put("memberId", member.getMemberId());
			Page<SysOrderVo> pageRes = sysOrderService.selectByPage(page);
			
			mav.addObject("page", pageRes);
		}

		String value=fromType==null?"0":"1";
		mav.addObject("fromType", value);
		mav.setViewName("h5/order/orderList");
		return mav;
	}

	/**
	 * 创建订单,重定向到支付方式选择页面
	 * @author limh Create on 2016年10月29日 上午11:28:25
	 * @param mav
	 * @param httpRequest
	 * @param order
	 * @param integral
	 * @return
	 */
	@RequestMapping(value = "creatOrder", method = RequestMethod.POST)
	public ModelAndView creatOrder(ModelAndView mav, HttpServletRequest httpRequest,SysOrder order,Long goodsId,Integer amount,Long voucherId,String time,String date,String weeknum) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			Integer memberId = member.getMemberId();
			if (order != null) {
				String bookTime=date+" "+time;
				SysOrder newOrder=null;
				if(goodsId!=null){
					if(amount==null){
						amount=1;
					}
					//单笔订单
					newOrder = sysOrderService.createSingleOrderAndDetail(order,memberId,bookTime,goodsId,amount,voucherId,weeknum);
				}else{
					//购物车订单
					newOrder = sysOrderService.createOrderAndDetail(order,memberId,bookTime,voucherId,weeknum);
				}
				if (newOrder != null && newOrder.getOrderId() != null) {
					mav.setViewName("redirect:orderPay.html?orderId=" + newOrder.getOrderId());
					return mav;
				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 支付方式选择页面
	 * @author limh Create on 2016年10月29日 上午11:28:10
	 * @param mav
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "orderPay", method = RequestMethod.GET)
	public ModelAndView orderPay(ModelAndView mav, HttpServletRequest httpRequest, Long orderId,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		// 如果有余额支付，可在此查询余额
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order!=null){
			mav.addObject("payno", order.getPayNumber());
		}
		mav.setViewName("h5/order/orderPay");
		return mav;
	}

	/**** api ****/

	/**
	 * 取消订单
	 * @author limh Create on 2016年10月29日 上午11:28:00
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/cancleOrder", method = RequestMethod.GET)
	public ResultVo<Object> cancleOrder(HttpServletRequest httpRequest, Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if (orderId != null) {
			SysOrder sysOrder = new SysOrder();
			sysOrder.setOrderId(orderId);
			sysOrder.setOrderStatus(Constants.ORDER_STATUS_CANCLED);
			int row = sysOrderService.updateByPrimaryKeySelective(sysOrder);
			if (row != -1) {
				return resultVo;
			}

		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 获取营业时间集合
	 * @author limh Create on 2016年12月3日 上午10:10:20
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getShopTimeList", method = RequestMethod.GET)
	public ResultVo<Object> getShopTimeList(HttpServletRequest httpRequest,String weeknum) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try {
			//获取营业时间每30分钟一段的集合,最后修改为15分钟
			List<String> shopTimeList = bizService.getShopTimeList(weeknum);
			if(shopTimeList==null||shopTimeList.isEmpty()){
				resultVo.setSuccess(false);
				return resultVo;
			}
			resultVo.setData(shopTimeList);
			return resultVo;
		} catch (Exception e) {
			e.printStackTrace();
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 系统订单列表滚动刷新
	 * @author limh Create on 2016年11月1日 下午4:40:01
	 * @param httpRequest
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getOrderList", method = RequestMethod.GET)
	public ResultVo<Object> getOrderList(HttpServletRequest httpRequest, Page<SysOrderVo> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if(page==null){
			page=new Page<>();
		}
		page.setPageSize(10);
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			page.getParams().put("memberId", member.getMemberId());
			Page<SysOrderVo> pageRes = sysOrderService.selectByPage(page);
			resultVo.setData(pageRes);
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 确认收货
	 * @author limh Create on 2016年10月29日 上午11:27:53
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/sureReceive", method = RequestMethod.GET)
	public ResultVo<Object> sureReceive(HttpServletRequest httpRequest, Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		/*String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);*/
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		Member member=null;
		if(order!=null){
			member = memberService.selectByPrimaryKey(order.getMemberId());
		}
		if(member!=null){
			int row = sysOrderService.sureReceive(orderId, member);
			if (row != -1) {
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}

	/**
	 * 保存订单评论
	 * @author limh Create on 2016年10月29日 上午11:20:45
	 * @param httpRequest
	 * @param orderComment
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/saveOrderComment", method = RequestMethod.POST)
	public ResultVo<Object> saveOrderComment(HttpServletRequest httpRequest, OrderComment orderComment) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null&&orderComment != null && orderComment.getOrderId() != null){
			int res = sysOrderService.saveOrderComment(orderComment, member);
			if(res!=-1){
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}

	/**
	 * 订单预定，类型预选
	 * @deprecated v3版本开始废弃
	 * @author limh Create on 2016年10月29日 上午11:21:12
	 * @param httpRequest
	 * @param bookType 订购类型，有中餐，晚餐，以及预定今日晚餐，预定明日中餐
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/bookOrder", method = RequestMethod.GET)
	public ResultVo<Object> bookOrder(HttpServletRequest httpRequest,String bookType) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		BookType BookType = new BookType();
		/*if(Constants.BOOK_TYPE_TODAY_LUNCH.equals(bookType)){
			//今日中餐
			BookType.setBookType(Constants.BOOK_TYPE_TODAY_LUNCH);
		}else if(Constants.BOOK_TYPE_TODAY_SUPPER.equals(bookType)){
			//今日晚餐
			BookType.setBookType(Constants.BOOK_TYPE_TODAY_SUPPER);
		}else if(Constants.BOOK_TYPE_BOOK_TOMORROW_LUNCH.equals(bookType)){
			//预定明日中餐
			BookType.setBookType(Constants.BOOK_TYPE_BOOK_TOMORROW_LUNCH);
		}else if(Constants.BOOK_TYPE_BOOK_SUPPER.equals(bookType)){
			//预定今日晚餐
			BookType.setBookType(Constants.BOOK_TYPE_BOOK_SUPPER);
		}else{
			//其他,异常
			BookType=null;
			resultVo.setSuccess(false);
			return resultVo;
		}*/
		//预定类型存入session中,设置过期时间30分钟
		HttpSession session = httpRequest.getSession();
		session.setMaxInactiveInterval(Constants.SESSION_EXPIRED_TIME_THIRTY_MINUTE);
		session.setAttribute(Constants.SESSION_BOOK_TYPE, BookType);
		
		resultVo.setData(bookType);
		return resultVo;
	}
	
	/**
	 * 获取当前session中的预定类型
	 * @author limh Create on 2016年11月3日 下午4:17:42
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getCurrentBookType", method = RequestMethod.GET)
	public ResultVo<Object> getCurrentBookType(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		//预定类型存入session中,设置过期时间30分钟
		HttpSession session = httpRequest.getSession();
		BookType BookType = (BookType) session.getAttribute(Constants.SESSION_BOOK_TYPE);
		if(BookType!=null){
			resultVo.setData(BookType.getBookType());
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 订单支付前的验证
	 * @author limh Create on 2016年10月29日 上午11:22:27
	 * @param httpRequest
	 * @param payno
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/validateOrder", method = RequestMethod.POST)
	public ResultVo<Object> validateOrder(HttpServletRequest httpRequest,String payno) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		SysOrder order = sysOrderService.selectByPayNumber(payno);		
		if(order==null){
			resultVo.setSuccess(false);
			resultVo.setMsg("订单不存在！");
			return resultVo;
		}
		if(Constants.ORDER_STATUS_PAIED.equals(order.getPayStatus())){
			resultVo.setSuccess(false);
			resultVo.setMsg("订单已支付！");
			return resultVo;
		}
		return resultVo;
	}
	
	
	/**
	 * 订单确认前验证
	 * @author limh Create on 2016年11月30日 下午8:59:22
	 * @param httpRequest
	 * @param goodsId 立即购买时的菜品id 可空
	 * @param amount 立即购买时的数量，可空
	 * @param weeknum 星期数
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/validateBeforeOrderConfirm1", method = RequestMethod.GET)
	public ResultVo<Object> validateBeforeOrderConfirm1(HttpServletRequest httpRequest,Long goodsId,Integer amount,Integer storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Store store =storeService.selectByPrimaryKey(storeId);
		try{
			//会员禁用
			boolean checkMemberIsForbidden = bizService.checkMemberIsForbidden(wxopenid);
			if(checkMemberIsForbidden){
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_001);
				resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
				return resultVo;
			}
			//菜品禁用
			double amountPrice=0d;
			SysParm realSysParam = sysParmService.getRealSysParam();
			double startPrice =store.getStartingPrice();
			if(goodsId!=null){
				//立即购买
				//菜品禁用
				boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(goodsId,storeId);
				if(checkGoodsIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_007);
					resultVo.setMsg("该菜品已下架！");
					return resultVo;
				}
				//菜品售完
				boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(goodsId,storeId);
				if(checkGoodsIsSoldOut){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_002);
					resultVo.setMsg("该菜品已售完！");
					return resultVo;
				}
				Goods goods = goodsService.selectByPrimaryKey(goodsId);
			/*	amountPrice=goods.getPrice()*amount;
				if(amountPrice<startPrice){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_004);
					resultVo.setMsg("订单总价低于起步价,是否将商品加入购物车？");
					return resultVo;
				}*/
			}else{
				//购物车
				ConditionModel condition = new ConditionModel();
				condition.getParams().put("memberId", member.getMemberId());
				condition.getParams().put("storeId", storeId);
				List<ShopTrolleyVo> cartList = shopCarService.selectByMemberIdTableId(member.getMemberId().longValue(), storeId.longValue());
				if(cartList==null||cartList.isEmpty()){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_008);
					resultVo.setMsg("订单已经生成！<br/>到【我的】-【全部订单】中查看吧！");
					return resultVo;
				}
				for(ShopTrolleyVo vo:cartList){
					//菜品禁用
					boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(vo.getGoodsId(),vo.getStoreId());
					if(checkGoodsIsForbidden){
						shopTrolleyService.deleteByPrimaryKey(vo.getId());
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_007);
						resultVo.setMsg(vo.getGoodsName()+"已下架！");
						return resultVo;
					}
					//菜品售完
					boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(vo.getGoodsId(),vo.getStoreId());
					if(checkGoodsIsSoldOut){
						shopTrolleyService.deleteByPrimaryKey(vo.getId());
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_002);
						resultVo.setMsg(vo.getGoodsName()+"已售完！");
						return resultVo;
					}
				}
			}
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 订单确认前验证
	 * @author limh Create on 2016年11月30日 下午8:59:22
	 * @param httpRequest
	 * @param goodsId 立即购买时的菜品id 可空
	 * @param amount 立即购买时的数量，可空
	 * @param weeknum 星期数
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/validateBeforeOrderConfirm", method = RequestMethod.GET)
	public ResultVo<Object> validateBeforeOrderConfirm(HttpServletRequest httpRequest,Long goodsId,Integer amount,String weeknum,Integer storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Store store =storeService.selectByPrimaryKey(storeId);
		try{
			//会员禁用
			boolean checkMemberIsForbidden = bizService.checkMemberIsForbidden(wxopenid);
			if(checkMemberIsForbidden){
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_001);
				resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
				return resultVo;
			}
			//判断当天营业时间，如果超过晚上营业时间，提示椒君，很抱歉，已到打烊时间了，您要预订明天餐品吗？
			boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime(weeknum);
			if(checkNowIsOverEndTime){
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_003);
				resultVo.setMsg("很抱歉，已到打烊时间了");
				return resultVo;
			}
			//菜品禁用
			double amountPrice=0d;
			SysParm realSysParam = sysParmService.getRealSysParam();
			double startPrice =store.getStartingPrice();
			if(goodsId!=null){
				//立即购买
				//菜品禁用
				boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(goodsId,storeId);
				if(checkGoodsIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_007);
					resultVo.setMsg("该菜品已下架！");
					return resultVo;
				}
				//菜品售完
				boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(goodsId,storeId);
				if(checkGoodsIsSoldOut){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_002);
					resultVo.setMsg("该菜品已售完！");
					return resultVo;
				}
				Goods goods = goodsService.selectByPrimaryKey(goodsId);
				amountPrice=goods.getPrice()*amount;
				if(amountPrice<startPrice){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_004);
					resultVo.setMsg("订单总价低于起步价,是否将商品加入购物车？");
					return resultVo;
				}
			}else{
				//购物车
				ConditionModel condition = new ConditionModel();
				condition.getParams().put("memberId", member.getMemberId());
				condition.getParams().put("weeknum", weeknum);
				condition.getParams().put("storeId", storeId);
				List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberIdAndStoreId(member.getMemberId(), storeId);
				if(cartList==null||cartList.isEmpty()){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_008);
					resultVo.setMsg("订单已经生成！<br/>到【我的】-【全部订单】中查看吧！");
					return resultVo;
				}
				for(ShopTrolleyVo vo:cartList){
					//菜品禁用
					boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(vo.getGoodsId(),vo.getStoreId());
					if(checkGoodsIsForbidden){
						shopTrolleyService.deleteByPrimaryKey(vo.getId());
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_007);
						resultVo.setMsg(vo.getGoodsName()+"已下架！");
						return resultVo;
					}
					//菜品售完
					boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(vo.getGoodsId(),vo.getStoreId());
					if(checkGoodsIsSoldOut){
						shopTrolleyService.deleteByPrimaryKey(vo.getId());
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_002);
						resultVo.setMsg(vo.getGoodsName()+"已售完！");
						return resultVo;
					}
				}
				amountPrice = shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
				if(amountPrice<startPrice){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_004);
					resultVo.setMsg("订单总价低于起步价,是否继续添加菜品？");
					return resultVo;
				}
			}
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 检验预定时间是否超过营业时间，若在营业时间内则判断是否是预定单
	 * @author limh Create on 2016年12月1日 上午10:57:56
	 * @param httpRequest
	 * @param time 预定时间
	 * @param weeknum 星期数
	 * @return 在营业时间内返回true，若为预定单还会携带时间book返回，若不在营业时间内返回false，提示错误消息
	 */
	@ResponseBody
	@RequestMapping(value = "api/validateBeforeCreatOrder", method = RequestMethod.GET)
	public ResultVo<Object> validateBeforeCreatOrder(HttpServletRequest httpRequest,Long goodsId,Integer amount,String time,String weeknum,Integer storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			resultVo=validateBeforeOrderConfirm(httpRequest,goodsId,amount,weeknum,storeId);
			if(resultVo!=null&&resultVo.getSuccess()){
				resultVo=checkTimeIsShopHours(httpRequest,time,weeknum);
			}
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 
	 * 
	 * @param httpRequest
	 * @param time
	 * @param weeknum
	 * 轮播图中领取代金卷.
	 * 声明事物
	 */
	@ResponseBody
	@RequestMapping(value = "getVoucherFromSlider", method = RequestMethod.GET)
	public ResultVo<Object> getVoucherFromSlider(HttpServletRequest httpRequest,String voucherId){
		ResultVo<Object> vo = new ResultVo<Object>();
		try {
			SysCoupon coupon = sysCouponService.selectByPrimaryKey(Long.parseLong(voucherId));
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			if(coupon==null){
				vo.setSuccess(false);
				vo.setMsg("没有该代金卷");
				return vo;
			}
			if(coupon.getStatus().equals(Constants.COUPON_NOTSTAR)){
				vo.setSuccess(false);
				vo.setMsg("活动还没开始");
				return vo;
			}
			if(coupon.getStatus().equals(Constants.COUPON_STOP)){
				vo.setSuccess(false);
				vo.setMsg("活动已经结束了，下次早点来哦");
				return vo;
			}
			if(coupon.getRemquantity()<=0){
				vo.setSuccess(false);
				vo.setMsg("已经抢完啦，下次早点来哦");
				return vo;
			}
			List<MemberVoucher>couponList= memberVoucherService.selectByMemberIdAndCouponId(member.getMemberId(),coupon.getId());
			if(couponList.size()>0){
				vo.setSuccess(false);
				vo.setMsg("您已经领取过了，请勿重复领取");
				return vo;
			}
			//领取平台代金券
			sysCouponService.wxgetOneCoupon(member,coupon);
			vo.setMsg("恭喜您，成功领取一张代金卷");
			vo.setSuccess(true);
			vo.setData(new Gson().toJson(coupon));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMsg("已经抢完啦，下次早点来哦");
			return vo;
		}
		return vo;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "api/checkTimeIsShopHours", method = RequestMethod.GET)
	public ResultVo<Object> checkTimeIsShopHours(HttpServletRequest httpRequest,String time,String weeknum) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			boolean checkTimeIsShopHours = bizService.checkTimeIsShopHours(time);
			if(checkTimeIsShopHours){
				
				String convertedDayOfWeek = BizHelper.getConvertedDayOfWeek();
				if(convertedDayOfWeek.equals(weeknum)){
					Date date = new Date();
					int now = TimeUtil.getTimeQuantum(date);
					int bookTime=TimeUtil.getTimeQuantum(DateUtils.parseTime2(time));
					if(bookTime<now){
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_005);
						resultVo.setMsg("此时间段不可预约，现在时间是"+DateUtils.formatTime2(date)+"，请选择该时间之后的时间作为预定时间！");
						return resultVo;
					}
					//今天，判断是否为预定单
					boolean checkIsBookOrder = bizService.checkIsBookOrder(time);
					if(!checkIsBookOrder){
						return resultVo;
					}
				}
				resultVo.setData(Constants.ORDER_TYPE_BOOK);
				return resultVo;
			}else{
				SysParm realSysParam = sysParmService.getRealSysParam();
				//上午营业时间
				String startTimeAm = DateUtils.formatTime2(realSysParam.getStartTime());
				String endTimeAm = DateUtils.formatTime2(realSysParam.getEndTime());
				//下午营业时间
				String startTimePm =DateUtils.formatTime2(realSysParam.getStartTimePm());
				String endTimePm = DateUtils.formatTime2(realSysParam.getEndTimePm());
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_006);
				resultVo.setMsg("不在营业时间范围内！上午营业时间："+startTimeAm+"-"+endTimeAm+",下午营业时间："+startTimePm+"-"+endTimePm);
				return resultVo;
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	/*********************************
	 * 购物车模块
	 **********************************/
	/**
	 * 跳转到购物车页面
	 * @author limh Create on 2016年10月29日 上午11:23:08
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "shopTrolley")
	public ModelAndView shopTrolley(ModelAndView mav, HttpServletRequest httpRequest,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Integer sId=Integer.parseInt(storeId);
		Store store=storeService.selectByPrimaryKey(sId);
		try{
			// 需将星期数计算为时间，计算星期数与当天星期数的差值，大于等于0按当前时间+差值，小于等于0按当前时间+7+差值
			int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
			Map<String,String> weekTime=new HashMap<>();
			int [] weeks={1,2,3,4,5,6,7};
			for(int weeknum:weeks){
				int temp=weeknum-currentDayOfWeeK;
				if(temp<0){
					temp+=7;
				}
				Date addDay = TimeUtil.addDay(temp);
				weekTime.put(String.valueOf(weeknum), TimeUtil.getSTD_DF_10(addDay));
			}
			mav.addObject("weekTime", weekTime);
			// 需要按星期分大类
			
			List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberIdAndStoreId(member.getMemberId(),sId);
			mav.addObject("cartList", cartList);
			//分类统计总价
			List<WeeknumStatistics> amountPriceList = shopTrolleyService.amountPriceByMemberIdCategory(member.getMemberId());
			mav.addObject("amountPriceList", amountPriceList);
			mav.addObject("memberStatus", member.getWxGroupid());
			boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime(String.valueOf(currentDayOfWeeK));
			mav.addObject("checkNowIsOverEndTime", checkNowIsOverEndTime);
			mav.addObject("currentDayOfWeeK", String.valueOf(currentDayOfWeeK));
			mav.addObject("storeId", store.getId());
		}catch(Exception e){
			LOG.info("shopTrolley-->throw exception");
			e.printStackTrace();
		}
		mav.setViewName("h5/shopping/shopTrolley");
		return mav;
	}

	/**
	 * 加入购物车
	 * 获取修改购物车中菜品数量
	 * @author limh Create on 2016年10月29日 上午11:23:28
	 * @param httpRequest
	 * @param cart 
	 * @param currentWeek 当前星期数
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/add2Cart", method = RequestMethod.GET)
	public ResultVo<Object> add2Cart(HttpServletRequest httpRequest, ShopTrolley cart,String currentWeek) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);

		if (member != null) {
			try {
				//会员禁用
				boolean checkMemberIsForbidden = bizService.checkMemberIsForbidden(wxopenid);
				if(checkMemberIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_001);
					resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
					return resultVo;
				}
				//判断当天营业时间，如果超过晚上营业时间，提示椒君，很抱歉，已到打烊时间了，您要预订明天餐品吗？
				boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime(currentWeek);
				if(checkNowIsOverEndTime){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_003);
					resultVo.setMsg("很抱歉，已到打烊时间了，您要预订明天餐品吗？");
					return resultVo;
				}
				//菜品禁用
				boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(cart.getGoodsId(),cart.getStoreId());
				if(checkGoodsIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_007);
					resultVo.setMsg("该菜品已下架！");
					return resultVo;
				}
				//菜品售完
				boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(cart.getGoodsId(),cart.getStoreId());
				if(checkGoodsIsSoldOut){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_002);
					resultVo.setMsg("该菜品已售完！");
					return resultVo;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				resultVo.setSuccess(false);
				return resultVo;
			}
			
			cart.setMemberId(member.getMemberId());
			ShopTrolley record = shopTrolleyService.checkGoodsIsExist(cart);
			if (record != null) {
				// 存在-更新
				Goods goods = goodsService.selectByPrimaryKey(record.getGoodsId());
				if (cart.getNum() == 0||Constants.GOODS_STATUS_SELLOUT.equals(goods.getStatus())) {
					shopTrolleyService.deleteByPrimaryKey(record.getId());
				} else {
					// 更新数量和价格
					ShopTrolley update = new ShopTrolley();
					update.setId(record.getId());
					update.setNum(cart.getNum());
					shopTrolleyService.updateByPrimaryKeySelective(update);
				}
			} else {
				if(cart.getGoodsId()!=null){
					Goods goods = goodsService.selectByPrimaryKey(cart.getGoodsId());
					if(Constants.GOODS_STATUS_SELLOUT.equals(goods.getStatus())){
						resultVo.setSuccess(false);
						return resultVo;
					}
					cart.setMemberId(member.getMemberId());
					cart.setMemberName(member.getMemberNick());
					cart.setNum(1);
					StoreGoods sg=goodsService.getStoreGoodsByGoodsIdAndStoreId(cart.getGoodsId(),cart.getStoreId());
					cart.setStoreGoodsId(sg.getId());
					shopTrolleyService.insertSelective(cart);
				}
			}
			// 统计当前星期数的价格
			ConditionModel condition = new ConditionModel();
			condition.getParams().put("memberId", member.getMemberId());
			condition.getParams().put("weeknum",currentWeek);
			condition.getParams().put("storeId", cart.getStoreId());
			ShopCatVo vo=shopTrolleyService.amountPriceAndNumByWeeknum(condition);
			 BigDecimal bg = new BigDecimal(vo.getAmountPrice());
			 double price = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			vo.setAmountPrice(price);
			resultVo.setData(vo);
			return resultVo;
		}
		return null;
	}
	
	/**
	 * 删除购物车中禁用的菜品
	 * @author limh Create on 2016年12月2日 下午5:38:18
	 * @param httpRequest
	 * @param cart
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/deleteForbiddenGoods", method = RequestMethod.GET)
	public ResultVo<Object> deleteForbiddenGoods(HttpServletRequest httpRequest,ShopTrolley cart) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		try {
		Member member = memberService.selectByWxOpenId(wxopenid);
		cart.setMemberId(member.getMemberId());
		ShopTrolley record = shopTrolleyService.checkGoodsIsExist(cart);
		if (record != null) {
			//删除禁用或者售完的菜品
			boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(cart.getGoodsId(),cart.getStoreId());
			boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(cart.getGoodsId(),cart.getStoreId());
			if(checkGoodsIsForbidden||checkGoodsIsSoldOut){
				shopTrolleyService.deleteByPrimaryKey(record.getId());
				return resultVo;
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		resultVo.setSuccess(false);
		return resultVo;
	}

	/**
	 * 清空购物车
	 * @author limh Create on 2016年10月29日 上午11:24:26
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/emptyShopCart", method = RequestMethod.GET)
	public ResultVo<Object> emptyShopCart(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		int row = shopTrolleyService.deleteByMemberId(member.getMemberId());
		if (row != -1) {
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 删除属于该星期数的菜品
	 * @author limh Create on 2016年12月2日 下午5:28:01
	 * @param httpRequest
	 * @param weeknum
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/deleteByWeeknum", method = RequestMethod.GET)
	public ResultVo<Object> deleteByWeeknum(HttpServletRequest httpRequest,String weeknum) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			ConditionModel conditionModel = new ConditionModel();
			conditionModel.getParams().put("memberId", member.getMemberId());
			conditionModel.getParams().put("weeknum", weeknum);
			int row = shopTrolleyService.deleteByMemberIdCategory(conditionModel);
			if (row != -1) {
				return resultVo;
			}
		}catch(Exception e){
			LOG.info("errorMsg--->"+e.getMessage());
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 获取购物车中该菜品的数量
	 * @author limh Create on 2016年11月4日 上午10:12:19
	 * @param httpRequest
	 * @param goodsId
	 * @return 该菜品对应的数量
	 */
	@ResponseBody
	@RequestMapping(value = "api/getShopTrolleyNum", method = RequestMethod.GET)
	public ResultVo<Object> getShopTrolleyNum(HttpServletRequest httpRequest,Long goodsId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			ShopTrolley shopTrolley = new ShopTrolley();
			shopTrolley.setGoodsId(goodsId);
			shopTrolley.setMemberId(member.getMemberId());
			ShopTrolley record = shopTrolleyService.checkGoodsIsExist(shopTrolley);
			if(record!=null){
				resultVo.setData(record.getNum());
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 购物车非空验证
	 * @deprecated v3版本开始废弃
	 * @author limh Create on 2016年10月29日 上午11:24:37
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/checkShopCart", method = RequestMethod.GET)
	public ResultVo<Object> checkShopCart(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			//会员禁用
			member=memberService.selectByPrimaryKey(member.getMemberId());
			if(Constants.MEMBER_STATUS_FORBIDDEN==member.getWxGroupid()){
				resultVo.setSuccess(false);
				resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
				return resultVo;
			}
			BookType BookType = (BookType) httpRequest.getSession().getAttribute(Constants.SESSION_BOOK_TYPE);
			if(BookType==null){
				//未选择预定类型，重新选择
				resultVo.setSuccess(false);
				resultVo.setMsg("您还没有选择订单类型！！！");
				return resultVo;
			}
			String bookType2 = BookType.getBookType();
			SysParm realSysParam = sysParmService.getRealSysParam();
			if(realSysParam!=null){
				int now = TimeUtil.getTimeQuantum(new Date());
				/*if(Constants.BOOK_TYPE_TODAY_LUNCH.equals(bookType2)){
					//如果是订购中餐
					//判断上午营业时间
					int startTimeAm =TimeUtil.getTimeQuantum(realSysParam.getStartTime());
					int endTimeAm = TimeUtil.getTimeQuantum(realSysParam.getEndTime());
					if(now<startTimeAm||now>endTimeAm){
						// 不在营业时间内,暂时不处理
						resultVo.setSuccess(false);
						resultVo.setMsg("椒君，很抱歉，已过中餐时间了，您可以订购晚上的菜品!");
						return resultVo;
					}
				}else if(Constants.BOOK_TYPE_TODAY_SUPPER.equals(bookType2)){
					//如果是订购晚餐
					//判断下午营业时间
					int startTimePm =TimeUtil.getTimeQuantum(realSysParam.getStartTimePm());
					int endTimePm = TimeUtil.getTimeQuantum(realSysParam.getEndTimePm());
					if(now<startTimePm||now>endTimePm){
						// 不在营业时间内,暂时不处理
						resultVo.setSuccess(false);
						resultVo.setMsg("椒君，很抱歉，已到打烊时间了，您可以预定明天的菜品！");
						return resultVo;
					}
				}*/
			}
			List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberId(member.getMemberId());
			if(cartList!=null&&!cartList.isEmpty()){
				for(ShopTrolleyVo cart:cartList){
					Goods goods = goodsService.selectByPrimaryKey(cart.getGoodsId());
					if(goods==null){
						continue;
					}
					if(Constants.GOODS_STATUS_SELLOUT.equals(goods.getStatus())){
						resultVo.setSuccess(false);
						resultVo.setMsg(goods.getGoodsname()+"已售完！");
						return resultVo;
					}
				}
				
				double amountPrice = shopTrolleyService.amountPriceByMemberId(member.getMemberId());
				double startPrice = realSysParam.getStartPrice().doubleValue();
				if(amountPrice<startPrice){
					resultVo.setSuccess(false);
					resultVo.setMsg("订单总价低于起步价！");
					return resultVo;
				}
				return resultVo;
			}
			resultVo.setSuccess(false);
			resultVo.setMsg("购物车为空！");
			return resultVo;
		}
		resultVo.setSuccess(false);
		resultVo.setMsg("系统繁忙！");
		return resultVo;
	}

	/*********************************
	 * 支付模块
	 **********************************/
	/**
	 * 微信支付
	 * @author limh Create on 2016年10月28日 下午5:00:53
	 * @param httpRequest
	 * @param payno
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/orderPaiedByWx", method = RequestMethod.POST)
	public ResultVo<Map<String, String>> wxPay(HttpServletRequest httpRequest, String payno) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Map<String, String>> result = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		String openid = null;
		if (member != null) {
			openid=member.getWxOpenId();
		}
		if (openid != null && payno != null){
			SysOrder order = sysOrderService.selectByPayNumber(payno);
			//TODO 调用查询接口，查询订单状态，若无则调用统一下单接口，若还未支付则调用支付接口
			// 调用统一下单接口，获得prepay_id
			try {
				ResponseTrade trade = XtwUnifiedOrderHelper.doPay(order, openid, "JSAPI");

				if (trade.getWxUnifiedRes()!= null&& StringUtils.isNotBlank(trade.getWxUnifiedRes().getPrepay_id())) {
					Map<String, String> requestMap = BizHelper.createPlaceOrderRequest(trade.getWxUnifiedRes().getPrepay_id());
					/*WxApiClient apiClient = new WxApiClient();
					String accessToken = wxAccessTokenService.getAccessToken().getToken();
					JsApiTicket ticketVo = apiClient.getJsApiTicket(accessToken, "jsapi");
					if(ticketVo != null && StringUtils.isNotBlank(ticketVo.getTicket())){
						String signature = BizHelper.getJsApiSignature(ticketVo.getTicket(), requestMap.get("noncestr"), requestMap.get("timestamp"),null);
						requestMap.put("signature", signature);
					}*/
					requestMap.put("orderId", order.getOrderId().toString());
					result.setData(requestMap);
					result.setSuccess(true);
					return result;
				}
				result.setMsg(trade.getErrorMsg());
			} catch (Exception e) {
				LOG.error("微信支付-统一下单异常："+e.getMessage());
				e.printStackTrace();
			}
		}
		result.setSuccess(false);
		LOG.info("微信支付失败===>"+result.toString());
		return result;
	}
	/**
	 * 微信支付异步回调
	 * @author limh Create on 2016年10月28日 下午5:00:34
	 * @param httpRequest
	 * @param response
	 */
	@RequestMapping(value = "pay_notify_url")
	public void wxPayNotify(HttpServletRequest httpRequest,HttpServletResponse response) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String respMessage = wxPayService.handlePayNotify(httpRequest);
		LOG.info("return notify_url result:" + respMessage);
		PrintWriter out=null;
		try {
			out=response.getWriter();
			out.print(respMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(out!=null){
				out.close();
			}
		}
	}
	
	/**
	 * 微信支付成功后查询订单状态，返回成功或失败页面
	 * @author limh Create on 2016年10月28日 下午5:00:25
	 * @param mav
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "queryorder")
	public ModelAndView queryorder(ModelAndView mav, HttpServletRequest httpRequest,Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order!=null){
			if(Constants.ORDER_STATUS_PAIED.equals(order.getPayStatus())){
				//支付成功，跳转到我的订单页面
			}else if(Constants.ORDER_STATUS_UNPAY.equals(order.getPayStatus())){
				//调用订单查询接口，查询订单
			}
		}
		mav.setViewName("redirect:orderList.html");
		return mav;
	}
	
	/**
	 * 支付宝支付
	 * @author limh Create on 2016年10月28日 下午4:59:42
	 * @param httpRequest
	 * @param payno 订单号，非订单id
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "api/orderPaiedByAlipay")
	public ResultVo<String> wapAliPay(HttpServletRequest httpRequest, String payno){
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<String> resultVo = new ResultVo<String>();
		SysOrder order = sysOrderService.selectByPayNumber(payno);
		//TODO 调用查询接口，查询订单状态，若无则调用统一下单接口，若还未支付则调用支付接口
		if(order==null){
			resultVo.setSuccess(false);
			resultVo.setMsg("订单不存在或已经关闭!");
			return resultVo;
		}
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("subject", "饭好粥到");
		requestMap.put("out_trade_no", order.getPayNumber());
		
		requestMap.put("service", AlipayUtil.SERVICE);
		requestMap.put("_input_charset", AlipayUtil.INPUT_CHARSET);
		requestMap.put("payment_type", AlipayUtil.PAYMENT_TYPE);
		requestMap.put("partner", BizHelper.getAlipayPartner());
		//notify_url 异步通知，return_url 同步通知，两者选其一即可
		requestMap.put("notify_url", BizHelper.getAliWapPayNotifyUrl());
//		requestMap.put("return_url", BizHelper.getAliWapPayReturnUrl());
		requestMap.put("total_fee", StringUtils.formatMoney2Byte(order.getTotalAmount()));
		requestMap.put("seller_id", BizHelper.getAlipaySellerId());
//		requestMap.put("show_url", BizHelper.getAliWapShowUrl());
		try {
			String sign = BizHelper.signWapAliPayOrder(requestMap);
			requestMap.put("sign", sign);
			requestMap.put("sign_type", AlipayUtil.SIGN_TYPE);
			String payUrl = BizHelper.getWapAliPayUrl(requestMap);
			LOG.info("payUrl=" + payUrl);
			resultVo.setSuccess(true);
			resultVo.setData(payUrl);
			return resultVo;
		} catch (Exception e) {
			LOG.info("error=" + e.getMessage());
			resultVo.setSuccess(false);
			resultVo.setMsg("系统繁忙!");
			return resultVo;
		}
	}
	
	/**
	 * 支付宝支付异步通知
	 * @author limh Create on 2016年10月29日 上午11:24:57
	 * @param httpRequest
	 * @param response
	 */
	@RequestMapping(value="ali_notify_url")
	public void aliNotifyUrl(HttpServletRequest httpRequest,HttpServletResponse response) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		Map<String, String> requestMap = BizHelper.getHttpRequestParameters(httpRequest);
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out=null;
		try {
			out = response.getWriter();
			if(AlipayUtil.verify(requestMap)){//验证成功
				String responseContext = sysOrderService.notifyAliJs(httpRequest);
				out.print(responseContext);
			}else{//验证失败
				out.print("fail");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null){
				out.close();
			}
		}
	}

	/*********************************
	 * 地址管理
	 **********************************/
	/**
	 * 跳转到添加地址页面
	 * @author limh Create on 2016年10月29日 上午11:25:26
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "addressAdd")
	public ModelAndView addressAdd(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		mav.setViewName("h5/user/addressAdd");
		return mav;
	}

	/**
	 * 跳转到修改地址页面
	 * @author limh Create on 2016年10月29日 上午11:25:37
	 * @param mav
	 * @param httpRequest
	 * @param addressId
	 * @return
	 */
	@RequestMapping(value = "addressModify")
	public ModelAndView addressModify(ModelAndView mav, HttpServletRequest httpRequest,Integer addressId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		MemberAddress address = memberAddressService.selectByPrimaryKey(addressId);
		if(address!=null){
			mav.addObject("address", address);
		}
		mav.setViewName("h5/user/addressModify");
		return mav;
	}

	/**
	 * 地址列表
	 * @author limh Create on 2016年10月29日 上午11:25:47
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "addressList")
	public ModelAndView addressList(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Page<MemberAddress> page = new Page<>();
		//分页刷新暂不做
		page.setPageSize(50);
		page.getParams().put("memberId", member.getMemberId());
		List<MemberAddress> addressList = memberAddressService.selectByPage(page);
		page.setResults(addressList);
		mav.addObject("page", page);
		mav.setViewName("h5/user/addressList");
		return mav;
	}

	/**
	 * 地区级联选择页面
	 * @author limh Create on 2016年10月29日 上午11:26:02
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "localityChoose")
	public ModelAndView localityChoose(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		mav.setViewName("h5/user/localityChoose");
		return mav;
	}

	/**** api ***/

	/**
	 * 保存地址
	 * @author limh Create on 2016年10月29日 上午11:26:15
	 * @param httpRequest
	 * @param address
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/saveAddress", method = RequestMethod.POST)
	public ResultVo<Object> saveAddress(HttpServletRequest httpRequest, MemberAddress address) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			int result = memberAddressService.changeAddress(address, member);
			if (result != -1) {
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 设置为默认地址
	 * @author limh Create on 2016年10月29日 上午11:26:29
	 * @param httpRequest
	 * @param address
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/set2Default", method = RequestMethod.GET)
	public ResultVo<Object> set2DefaultAddress(HttpServletRequest httpRequest, MemberAddress address) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			int result = memberAddressService.update2DefaultAddress(address, member);
			if (result != -1) {
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 删除地址
	 * @author limh Create on 2016年10月29日 上午11:26:38
	 * @param httpRequest
	 * @param addressId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/deleteAddress", method = RequestMethod.GET)
	public ResultVo<Object> deleteAddress(HttpServletRequest httpRequest, Integer addressId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		int result = memberAddressService.deleteByPrimaryKey(addressId);
		if(result!=-1){
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}

	/*********************************
	 * 会员模块
	 **********************************/
	/**
	 * 跳转到我的积分页面
	 * @author limh Create on 2016年10月29日 上午11:26:46
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "integral")
	public ModelAndView integral(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		MemberIntegral memberIntegral = memberIntegralService.selectByMemberId(member.getMemberId());
		Integer integral = memberIntegral.getIntegral();
		mav.addObject("integral", integral);
		SysParm realSysParam = sysParmService.getRealSysParam();
		String integralProportion = realSysParam.getIntegralProportion();
		integralProportion = integralProportion.replace(",", ":");
		mav.addObject("integralProportion", integralProportion);
		mav.setViewName("h5/user/integral");
		return mav;
	}
	
	/**
	 * 我的代金券列表
	 * @author limh Create on 2016年11月14日 下午8:39:30
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "voucher")
	public ModelAndView voucher(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Page<MemberVoucherVo> page = new Page<>();
		page.getParams().put("memberId", member.getMemberId());
		List<MemberVoucherVo> memberVoucherList = memberVoucherService.selectByPage(page);
		page.setResults(memberVoucherList);
		mav.addObject("page", page);
		mav.setViewName("h5/user/voucher");
		return mav;
	}
	
	/**
	 * 兑换代金券页面
	 * @author limh Create on 2016年11月14日 下午8:40:29
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "exchangeVoucher")
	public ModelAndView exchangeVoucher(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		MemberIntegral memberIntegral = memberIntegralService.selectByMemberId(member.getMemberId());
		Integer integral = memberIntegral.getIntegral();
		mav.addObject("integral", integral);
		Page<Voucher> page = new Page<>();
		List<Voucher> voucherList = voucherService.queryExchangePage(page);
		page.setResults(voucherList);
		mav.addObject("page", page);
		mav.setViewName("h5/user/exchangeVoucher");
		return mav;
	}
	@RequestMapping(value="sendMemberLocation")
	public @ResponseBody ResultVo<Object> sendMemberLocation( HttpServletRequest httpRequest,String lat,String lng){
		ResultVo<Object> vo = new ResultVo<>();
		if((!StringUtil.isBlank(lat))&&(!StringUtil.isBlank(lng))){
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			member.setGeoLat(new BigDecimal(lat));
			member.setGeoLng(new BigDecimal(lng));
			member.setGeoTime(new Date());
			memberService.updateByPrimaryKey(member);
			List<Store> stores = storeService.selectStoresByUserLocation(Double.parseDouble(lat),Double.parseDouble(lng));
			vo.setData(new Gson().toJson(stores));
		}else{
			List<Store> stores = storeService.queryAllStore();
			vo.setData(new Gson().toJson(stores));
		}
		
		return vo;
	}
	/**
	 * 跳转到我的个人页面
	 * @author limh Create on 2016年10月29日 上午11:27:12
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value ="personal")
	public ModelAndView personal(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			mav.addObject("member", member);
			SysParm realSysParam = sysParmService.getRealSysParam();
			mav.addObject("phone", realSysParam.getBookDiscount());
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.setViewName("h5/user/personal");
		return mav;
	}
	
	
	
	/***api***/
	/**
	 * 积分兑换代金券
	 * @author limh Create on 2016年11月16日 上午11:32:17
	 * @param httpRequest
	 * @param voucherId 代金券Id
	 * @return 剩余的积分
	 */
	@ResponseBody
	@RequestMapping(value = "api/exchangeVoucher", method = RequestMethod.GET)
	public ResultVo<Object> converIntegral(HttpServletRequest httpRequest, Long voucherId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			try {
				Integer r = memberService.exchangeVoucher(member, voucherId);
				if(r==-1){
					resultVo.setSuccess(false);
					resultVo.setMsg("积分不够！");
					return resultVo;
				}
				resultVo.setData(r);
				return resultVo;
			} catch (NullPointerException | SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 积分换算成金额
	 * @author limh Create on 2016年10月28日 下午5:59:52
	 * @param httpRequest
	 * @param integral 积分
	 * @deprecated
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "api/converIntegral", method = RequestMethod.POST)
	public ResultVo<Object> converIntegral(HttpServletRequest httpRequest, Integer integral,Double amountPrice) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			MemberIntegral memberIntegral = memberIntegralService.selectByMemberId(member.getMemberId());
			Integer maxIntegral = memberIntegral.getIntegral();
			//积分换算的金额
			double converPrice = 0;
			if(integral>maxIntegral){
				//默认转为最大积分
				converPrice = sysParmService.converIntegral(maxIntegral);
			}else{
				converPrice = sysParmService.converIntegral(integral);
			}
			if(converPrice>amountPrice){
				converPrice=amountPrice;
			}
			resultVo.setData(converPrice);
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 我的代金券列表滚动刷新
	 * @author limh Create on 2016年11月14日 下午8:23:35
	 * @param httpRequest
	 * @param page
	 * @return 下一页的数据
	 */
	@ResponseBody
	@RequestMapping(value = "api/getVoucherList", method = RequestMethod.POST)
	public ResultVo<Object> getVoucherList(HttpServletRequest httpRequest, Page<MemberVoucherVo> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if(page==null){
			page=new Page<>();
		}
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			page.getParams().put("memberId", member.getMemberId());
			List<MemberVoucherVo> memberVoucherList = memberVoucherService.selectByPage(page);
			page.setResults(memberVoucherList);
			resultVo.setData(page);
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	
	/**
	 * 可兑换代金券列表滚动刷新
	 * @author limh Create on 2016年11月14日 下午8:23:35
	 * @param httpRequest
	 * @param page
	 * @return 下一页的数据
	 */
	@ResponseBody
	@RequestMapping(value = "api/getExchangeVoucher", method = RequestMethod.POST)
	public ResultVo<Object> getExchangeVoucher(HttpServletRequest httpRequest, Page<Voucher> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if(page==null){
			page=new Page<>();
		}
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			List<Voucher> voucherList = voucherService.queryExchangePage(page);
			page.setResults(voucherList);
			resultVo.setData(page);
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
				//余额支付
			@RequestMapping(value="creditpay")
			public @ResponseBody ResultVo<Object>  creditpay(HttpServletRequest httpRequest,String storeId,String payno){
				ResultVo<Object> resultVo=new ResultVo<>();
				String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
				Member member = memberService.selectByWxOpenId(wxopenid);
				resultVo=memberService.creditpay(member,storeId,payno,httpRequest);
				return  resultVo;
			}
			//跳转到支付成功页面
			@RequestMapping(value="paysuccess")
			public ModelAndView paysuccess(ModelAndView mav, HttpServletRequest httpRequest,String payno){
				SysOrder order = sysOrderService.selectByPayNumber(payno);
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				if(order.getPayTime()==null){
					mav.setViewName("redirect:orderPay.html?orderId="+order.getOrderId()+"&&storeId="+order.getStoreId()+"&&type="+order.getPayType());
					return mav;
				}
				String time=sdf.format(order.getPayTime());
				order.setPayDate(time);
				String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
				Member member = memberService.selectByWxOpenId(wxopenid);
				mav.addObject("order", order);
				List<SysParm> paramList = sysParmService.selectAll();
				if(paramList!=null&&paramList.size()>0){
					mav.addObject("sysParm", paramList.get(0));
				}
				mav.setViewName("h5/comment/paySuccess");
				return mav;
			}
			//跳转到订单评论的页面
			@RequestMapping(value="goComment")
			public ModelAndView goComment(ModelAndView mav, HttpServletRequest httpRequest,String payno,String storeId){
				mav.addObject("payno", payno);
				mav.addObject("storeId", storeId);
				mav.setViewName("h5/order/evaluate");
				return mav;
			}
	
}
