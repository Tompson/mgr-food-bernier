package com.deyi.controller.h5;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Member;
import com.deyi.entity.SysOrder;
import com.deyi.entity.SysParm;
import com.deyi.model.ResponseTrade;
import com.deyi.model.ResultVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.RechargeRecordService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.AlipayUtil;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.PayUtils;
import com.deyi.util.StringUtils;
import com.deyi.util.wxmppay.XtwUnifiedOrderHelper;

@Controller
@RequestMapping(value="h5/pay")
public class H5PayController {
private Logger LOG = LoggerFactory.getLogger(H5StoreController.class);
	

	@Autowired
	private RechargeRecordService rechargeRecordService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	/**
	 * 支付方式选择页面
	 * @author limh Create on 2016年10月29日 上午11:28:10
	 * @param mav
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "orderPay", method = RequestMethod.GET)
	public ModelAndView orderPay(ModelAndView mav, HttpServletRequest httpRequest, Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		// 如果有余额支付，可在此查询余额
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order!=null){
			mav.addObject("payno", order.getPayNumber());
		}
		mav.setViewName("h5/order/orderPay");
		return mav;
	}
	/*********************************
	 * 支付模块
	 **********************************/
	/**
	 * 微信支付
	 * @author limh Create on 2016年10月28日 下午5:00:53
	 * @param httpRequest
	 * @param payno
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/orderPaiedByWx", method = RequestMethod.POST)
	public ResultVo<Map<String, String>> wxPay(HttpServletRequest httpRequest, String payno) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Map<String, String>> result = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		String openid = null;
		if (member != null) {
			openid=member.getWxOpenId();
		}
		if (openid != null && payno != null){
			SysOrder order = sysOrderService.selectByPayNumber(payno);
			//TODO 调用查询接口，查询订单状态，若无则调用统一下单接口，若还未支付则调用支付接口
			// 调用统一下单接口，获得prepay_id
			try {
				ResponseTrade trade = XtwUnifiedOrderHelper.doPay(order, openid, "JSAPI");

				if (trade.getWxUnifiedRes()!= null&& StringUtils.isNotBlank(trade.getWxUnifiedRes().getPrepay_id())) {
					Map<String, String> requestMap = BizHelper.createPlaceOrderRequest(trade.getWxUnifiedRes().getPrepay_id());
					/*WxApiClient apiClient = new WxApiClient();
					String accessToken = wxAccessTokenService.getAccessToken().getToken();
					JsApiTicket ticketVo = apiClient.getJsApiTicket(accessToken, "jsapi");
					if(ticketVo != null && StringUtils.isNotBlank(ticketVo.getTicket())){
						String signature = BizHelper.getJsApiSignature(ticketVo.getTicket(), requestMap.get("noncestr"), requestMap.get("timestamp"),null);
						requestMap.put("signature", signature);
					}*/
					requestMap.put("orderId", order.getOrderId().toString());
					result.setData(requestMap);
					result.setSuccess(true);
					return result;
				}
				result.setMsg(trade.getErrorMsg());
			} catch (Exception e) {
				LOG.error("微信支付-统一下单异常："+e.getMessage());
				e.printStackTrace();
			}
		}
		result.setSuccess(false);
		LOG.info("微信支付失败===>"+result.toString());
		return result;
	}
	/**
	 * 微信支付异步回调
	 * @author limh Create on 2016年10月28日 下午5:00:34
	 * @param httpRequest
	 * @param response
	 */
	@RequestMapping(value = "pay_notify_url")
	public void wxPayNotify(HttpServletRequest httpRequest,HttpServletResponse response) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String respMessage = wxPayService.handlePayNotify(httpRequest);
		LOG.info("return notify_url result:" + respMessage);
		PrintWriter out=null;
		try {
			 out=response.getWriter();
			out.print(respMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null){
				out.close();
			}
		}
	}
	
	/**
	 * 微信支付成功后查询订单状态，返回成功或失败页面
	 * @author limh Create on 2016年10月28日 下午5:00:25
	 * @param mav
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "queryorder")
	public ModelAndView queryorder(ModelAndView mav, HttpServletRequest httpRequest,Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order!=null){
			if(Constants.ORDER_STATUS_PAIED.equals(order.getPayStatus())){
				//支付成功，跳转到我的订单页面
			}else if(Constants.ORDER_STATUS_UNPAY.equals(order.getPayStatus())){
				//调用订单查询接口，查询订单
			}
		}
		mav.setViewName("redirect:orderList.html");
		return mav;
	}
	
	/**
	 * 支付宝支付
	 * @author limh Create on 2016年10月28日 下午4:59:42
	 * @param httpRequest
	 * @param payno 订单号，非订单id
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "api/orderPaiedByAlipay")
	public ResultVo<String> wapAliPay(HttpServletRequest httpRequest, String payno){
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<String> resultVo = new ResultVo<String>();
		SysOrder order = sysOrderService.selectByPayNumber(payno);
		//TODO 调用查询接口，查询订单状态，若无则调用统一下单接口，若还未支付则调用支付接口
		if(order==null){
			resultVo.setSuccess(false);
			resultVo.setMsg("订单不存在或已经关闭!");
			return resultVo;
		}
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("subject", "饭好粥到");
		requestMap.put("out_trade_no", order.getPayNumber());
		
		requestMap.put("service", AlipayUtil.SERVICE);
		requestMap.put("_input_charset", AlipayUtil.INPUT_CHARSET);
		requestMap.put("payment_type", AlipayUtil.PAYMENT_TYPE);
		requestMap.put("partner", BizHelper.getAlipayPartner());
		//notify_url 异步通知，return_url 同步通知，两者选其一即可
		requestMap.put("notify_url", BizHelper.getAliWapPayNotifyUrl());
		requestMap.put("return_url", BizHelper.getAliWapPayReturnUrl()+"?payno="+ order.getPayNumber());
		requestMap.put("total_fee", StringUtils.formatMoney2Byte(order.getTotalAmount()));
		requestMap.put("seller_id", BizHelper.getAlipaySellerId());
//		requestMap.put("show_url", BizHelper.getAliWapShowUrl());
		try {
			String sign = BizHelper.signWapAliPayOrder(requestMap);
			requestMap.put("sign", sign);
			requestMap.put("sign_type", AlipayUtil.SIGN_TYPE);
			String payUrl = BizHelper.getWapAliPayUrl(requestMap);
			LOG.info("payUrl=" + payUrl);
			resultVo.setSuccess(true);
			resultVo.setData(payUrl);
			return resultVo;
		} catch (Exception e) {
			LOG.info("error=" + e.getMessage());
			resultVo.setSuccess(false);
			resultVo.setMsg("系统繁忙!");
			return resultVo;
		}
	}
	
	/**
	 * 支付宝支付异步通知
	 * @author limh Create on 2016年10月29日 上午11:24:57
	 * @param httpRequest
	 * @param response
	 */
	@RequestMapping(value="ali_notify_url")
	public void aliNotifyUrl(HttpServletRequest httpRequest,HttpServletResponse response) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		Map<String, String> requestMap = BizHelper.getHttpRequestParameters(httpRequest);
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out=null;
		try {
			out = response.getWriter();
			if(AlipayUtil.verify(requestMap)){//验证成功
				String responseContext = sysOrderService.notifyAliJs(httpRequest);
				out.print(responseContext);
			}else{//验证失败
				out.print("fail");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null){
				out.close();
			}
		}
	}
	
	/**
	 * 
	 * @Title: rechargeByWx 
	 * @Description: 微信充值
	 * @param httpRequest
	 * @param money 充值金额
	 * @return
	 * ResultVo<Map<String,String>>
	 * @throws
	 */
	@RequestMapping(value = "api/rechargeByWx", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo<Map<String, String>> rechargeByWx(HttpServletRequest httpRequest, String money) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Map<String, String>> result = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		String openid = null;
		if (member != null) {
			openid=member.getWxOpenId();
		}
		if (openid != null && money != null){
			// 调用统一下单接口，获得prepay_id
			try {
				ResponseTrade trade = XtwUnifiedOrderHelper.doPay(money, openid, "JSAPI");
				if (trade.getWxUnifiedRes()!= null&& StringUtils.isNotBlank(trade.getWxUnifiedRes().getPrepay_id())) {
					Map<String, String> requestMap = BizHelper.createPlaceOrderRequest(trade.getWxUnifiedRes().getPrepay_id());
					result.setData(requestMap);
					result.setSuccess(true);
					return result;
				}
				result.setMsg(trade.getErrorMsg());
			} catch (Exception e) {
				LOG.error("微信支付-统一下单异常："+e.getMessage());
				e.printStackTrace();
			}
		}
		result.setSuccess(false);
		LOG.info("微信支付失败===>"+result.toString());
		return result;
	}
	
	/**
	 * 
	 * @Title: rechargeByAlipay 
	 * @Description: 支付宝充值
	 * @param httpRequest
	 * @param money
	 * @return
	 * ResultVo<String>
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "api/rechargeByAlipay")
	public ResultVo<String> rechargeByAlipay(HttpServletRequest httpRequest, String money,String memberId){
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<String> resultVo = new ResultVo<String>();
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("subject", memberId);
		requestMap.put("out_trade_no", PayUtils.getRandomString(10).concat(String.valueOf(new Date().getTime())));
		
		requestMap.put("service", AlipayUtil.SERVICE);
		requestMap.put("_input_charset", AlipayUtil.INPUT_CHARSET);
		requestMap.put("payment_type", AlipayUtil.PAYMENT_TYPE);
		requestMap.put("partner", BizHelper.getAlipayPartner());
		//notify_url 异步通知，return_url 同步通知，两者选其一即可
		requestMap.put("notify_url", BizHelper.getAliWapRechargeNotifyUrl());
//		requestMap.put("return_url", BizHelper.getAliWapRechargeReturnUrl());
		requestMap.put("total_fee",money);
		requestMap.put("seller_id", BizHelper.getAlipaySellerId());
//		requestMap.put("show_url", BizHelper.getAliWapShowUrl());
		try {
			String sign = BizHelper.signWapAliPayOrder(requestMap);
			requestMap.put("sign", sign);
			requestMap.put("sign_type", AlipayUtil.SIGN_TYPE);
			String payUrl = BizHelper.getWapAliPayUrl(requestMap);
			LOG.info("payUrl=" + payUrl);
			resultVo.setSuccess(true);
			resultVo.setData(payUrl);
			return resultVo;
		} catch (Exception e) {
			LOG.info("error=" + e.getMessage());
			resultVo.setSuccess(false);
			resultVo.setMsg("系统繁忙!");
			return resultVo;
		}
	}
	
	/**
	 * @Title: alipayRechargeNotify 
	 * @Description: 支付宝充值异步回调
	 * @param httpRequest
	 * @param response
	 * void
	 * @throws
	 */
	@RequestMapping(value="alipayRechargeNotify")
	public void alipayRechargeNotify(HttpServletRequest httpRequest,HttpServletResponse response,String memberId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		Map<String, String> requestMap = BizHelper.getHttpRequestParameters(httpRequest);
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out=null;
		try {
			out = response.getWriter();
			if(AlipayUtil.verify(requestMap)){//验证成功
				String responseContext = rechargeRecordService.saveRechargeRecordAli(httpRequest);
				out.print(responseContext);
			}else{//验证失败
				out.print("fail");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null){
				out.close();
			}
		}
	}
	
	
//	@RequestMapping(value = "alipayRechargeReturn", method = RequestMethod.GET)
//	public ModelAndView alipayRechargeReturn(ModelAndView mav, HttpServletRequest httpRequest) {
//		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
//				+ BizHelper.getHttpRequestParameters(httpRequest));
//		// 如果有余额支付，可在此查询余额
//		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
//		if(order!=null){
//			mav.addObject("payno", order.getPayNumber());
//		}
//		mav.setViewName("h5/order/orderPay");
//		return mav;
//	}
	
	/**
	 * 
	 * @Title: wxRechargeNotify 
	 * @Description: 微信充值异步回调
	 * @param httpRequest
	 * @param response
	 * void
	 * @throws
	 */
	@RequestMapping(value = "wxRechargeNotify")
	public void wxRechargeNotify(HttpServletRequest httpRequest,HttpServletResponse response) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String respMessage = rechargeRecordService.saveRechargeRecordWx(httpRequest);
		LOG.info("return wxRechargeNotify result:" + respMessage);
		PrintWriter out=null;
		try {
			 out=response.getWriter();
			out.print(respMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null){
				out.close();
			}
		}
	}
	/*//余额支付
	@RequestMapping(value="creditpay")
	public @ResponseBody ResultVo<Object>  creditpay(HttpServletRequest httpRequest,String storeId,String payno){
		ResultVo<Object> resultVo=new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		resultVo=memberService.creditpay(member,storeId,payno);
		return  resultVo;
	}
	@RequestMapping(value="paySuccess")
	public ModelAndView paySuccess(ModelAndView mav, HttpServletRequest httpRequest,String payno){
		SysOrder order = sysOrderService.selectByPayNumber(payno);
		SimpleDateFormat sdf=new SimpleDateFormat("YYYY-MM-DD HH:MM:SS");
		String time=sdf.format(order.getPayTime());
		order.setPayDate(time);
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		List<SysParm> paramList = sysParmService.selectAll();
		if(paramList!=null&&paramList.size()>0){
			mav.addObject("sysParm", paramList.get(0));
		}
		mav.addObject("order", order);
		mav.setViewName("h5/comment/paySuccess");
		return mav;
	}
	*/
	
	
	
	
	
	
	
	
	
	
	
}
