package com.deyi.controller.h5;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Member;
import com.deyi.service.MemberService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.StringUtils;
import com.deyi.vo.ReturnVo;

@Controller
@RequestMapping(value="h5/member")
public class H5MemberController {
	private Logger LOG = LoggerFactory.getLogger(H5MemberController.class);
	
	@Autowired
	private MemberService memberService;
	
	/**
	 * 
	 * @Title: myaccount 
	 * @Description: 我的账户页面
	 * @param mav
	 * @param httpRequest
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "myaccount", method = RequestMethod.GET)
	public ModelAndView myaccount(ModelAndView mav, HttpServletRequest httpRequest,String memberId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member==null){
			mav.addObject("memberId",memberId);
			mav.setViewName("h5/user/account");
			return mav;
		}
		BigDecimal money = member.getMoney();
		mav.addObject("money",StringUtils.formatMoney2Byte(money));
		mav.setViewName("h5/user/account");
		return mav;
	}
	
	@RequestMapping(value = "myInfo", method = RequestMethod.GET)
	public ModelAndView myInfo(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		mav.addObject("member",member);
		mav.setViewName("h5/user/editInfo");
		return mav;
	}
	
	@ResponseBody
	@RequestMapping(value = "api/saveInfo", method = RequestMethod.POST)
	public Object saveInfo(HttpServletRequest httpRequest,Member member) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ReturnVo<Object> vo = new ReturnVo<>();
		try{
			memberService.updateByPrimaryKeySelective(member);
		}catch(Exception e){
			vo.setSuccess(false);
			e.printStackTrace();
		}
		return vo;
	}
}
