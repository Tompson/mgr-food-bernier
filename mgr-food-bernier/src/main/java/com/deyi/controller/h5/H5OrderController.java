package com.deyi.controller.h5;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Goods;
import com.deyi.entity.Member;
import com.deyi.entity.MemberAddress;
import com.deyi.entity.MemberIntegral;
import com.deyi.entity.MemberLevel;
import com.deyi.entity.OrderComment;
import com.deyi.entity.Part;
import com.deyi.entity.Store;
import com.deyi.entity.StoreTable;
import com.deyi.entity.SysOrder;
import com.deyi.entity.SysParm;
import com.deyi.model.BookType;
import com.deyi.model.ConditionModel;
import com.deyi.model.ResponseTrade;
import com.deyi.model.ResultVo;
import com.deyi.model.VoucherQueryCondition;
import com.deyi.model.vo.MemberVoucherVo;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.model.vo.SysOrderVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberLevelService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopCarService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.StoreTableService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.AlipayUtil;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.util.StringUtils;
import com.deyi.util.TimeUtil;
import com.deyi.util.wxmppay.XtwUnifiedOrderHelper;
import com.deyi.vo.DiscountTypeVo;
import com.deyi.vo.DiscountVo;

@Controller
@RequestMapping(value="h5/order")
public class H5OrderController {
	private Logger LOG = LoggerFactory.getLogger(H5OrderController.class);
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	@Autowired 
	private MemberLevelService memberLevelService;
	@Autowired 
	private ShopCarService shopCarService;
	@Autowired
	private StoreTableService storeTableService;
	/*********************************
	 * 订单模块
	 **********************************/
	/**
	 * 跳转订单评论页面
	 * @author limh Create on 2016年10月29日 上午11:29:09
	 * @param mav
	 * @param httpRequestL
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "evaluate", method = RequestMethod.GET)
	public ModelAndView evaluate(ModelAndView mav, HttpServletRequest httpRequest, Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		SysOrder sysOrder = sysOrderService.queryOrderById2(orderId);
		
		mav.addObject("sysOrder",sysOrder);
		mav.addObject("orderId", orderId);
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order.getOrderType().equals(Constants.ORDER_EatingInStore)||order.getOrderType().equals(Constants.ORDER_DaBao)){
			//堂食，打包
			mav.setViewName("h5/order/eatingEvaluate");
		}else{
			mav.setViewName("h5/order/evaluate");
		}
		
		return mav;
	}
	/**
	 * 删除属于该星期数的菜品
	 * @author limh Create on 2016年12月2日 下午5:28:01
	 * @param httpRequest
	 * @param weeknum
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/deleteByWeeknum", method = RequestMethod.GET)
	public ResultVo<Object> deleteByWeeknum(HttpServletRequest httpRequest,String weeknum,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			ConditionModel conditionModel = new ConditionModel();
			conditionModel.getParams().put("memberId", member.getMemberId());
			conditionModel.getParams().put("weeknum", weeknum);
			conditionModel.getParams().put("storeId", storeId);
			int row = shopTrolleyService.deleteByMemberIdCategory(conditionModel);
			if (row != -1) {
				return resultVo;
			}
		}catch(Exception e){
			LOG.info("errorMsg--->"+e.getMessage());
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 订单确认页面
	 * @author limh Create on 2016年10月29日 上午11:29:01
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "orderConfirm")
	public ModelAndView orderConfirm(ModelAndView mav, HttpServletRequest httpRequest,Long goodsId,Integer amount,String weeknum,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
		httpRequest.getSession().removeAttribute(Constants.SESSION_ADDRESS_BACK_CONFIRM);
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Store store = storeService.selectByPrimaryKey(Integer.parseInt(storeId));
		if (member != null) {
			
			//查询所有地址
			Page<MemberAddress> page = new Page<>();
			page.getParams().put("memberId", member.getMemberId());
			List<MemberAddress> allAddress = memberAddressService.selectByPage(page);
			if(allAddress!=null&&!allAddress.isEmpty()){
				mav.addObject("allAddress", allAddress);
			}else{
				//没有设置任何地址，跳转到地址页面设置地址
				mav.setViewName("redirect:addressList.html?v=1");
				String concat="?";
				if(goodsId!=null){
					concat+="goodsId="+goodsId+"&amount="+amount+"&weeknum="+weeknum;
				}else{
					concat+="weeknum="+weeknum+"&storeId="+storeId;
				}
				httpRequest.getSession().setAttribute(Constants.SESSION_ADDRESS_BACK_CONFIRM,httpRequest.getServletPath().replace("/h5/", "").concat(concat));
				Log.info("session存入 back_"
						+ "------>   "+httpRequest.getServletPath().replace("/h5/", "").concat(concat));
				return mav;
			}
			// 查询默认收货地址
			List<MemberAddress> addressList = memberAddressService.selectDefaultAddressByMemberId(member.getMemberId());
			if(addressList!=null&&!addressList.isEmpty()){
				mav.addObject("defaultAddress", addressList.get(0));
			}
			// 待确认订单信息，1.购物车中所有菜品 2.直接购买的菜品
			Integer memberId = member.getMemberId();
			double amountPrice=0;
			if(goodsId!=null){
				Goods goods = goodsService.selectByPrimaryKey(goodsId);
				if(goods!=null){
					Double price = goods.getPrice();
					if(amount==null){
						amount=1;
					}
					amountPrice=price*amount;	
				}
				mav.addObject("goods", goods);
				mav.addObject("amount", amount);
				mav.addObject("subtotalPrice", amountPrice);
			}else{
				ConditionModel condition = new ConditionModel();
				condition.getParams().put("memberId", memberId);
				condition.getParams().put("weeknum", weeknum);
				condition.getParams().put("storeId", storeId);
				List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberIdAndStoreIdAndWeek(member.getMemberId(),Integer.parseInt(storeId),Integer.parseInt(weeknum));
				amountPrice = shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
				mav.addObject("cartList", cartList);
			}
			//判断总价是否小于起步价
			double startPrice =store.getStartingPrice();
			mav.addObject("startPrice",startPrice);
			if(amountPrice<startPrice){
				mav.setViewName("redirect:productHome.html");
				return mav;
			}
			//配送费

			Double deliverPrice = store.getDeliverFee();
			mav.addObject("deliverPrice",deliverPrice);
			amountPrice=amountPrice+deliverPrice;
			//总价，商品总价+配送费-优惠价，此时积分未换算
			mav.addObject("amountPrice", StringUtils.formatMoney2Byte(amountPrice));
			//查询符合条件的优惠券
			VoucherQueryCondition record = new VoucherQueryCondition();
			record.setMemberId(member.getMemberId());
			record.setAmountPrice(amountPrice);
			List<MemberVoucherVo> voucherList = memberVoucherService.selectSatisfactoryVoucher(record);
			mav.addObject("voucherList",voucherList);
			//当前星期数对应的日期
			int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
			int temp=Integer.parseInt(weeknum)-currentDayOfWeeK;
			if(temp<0){
				temp+=7;
			}
			Date addDay = TimeUtil.addDay(temp);
			mav.addObject("currentDate", TimeUtil.getSTD_DF_10(addDay));
			mav.addObject("weeknum", weeknum);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("storeId", storeId);
		mav.setViewName("h5/order/orderConfirm");
		return mav;
	}
	/**
	 * 支付方式选择页面
	 * @author limh Create on 2016年10月29日 上午11:28:10
	 * @param mav
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "orderPay", method = RequestMethod.GET)
	public ModelAndView orderPay(ModelAndView mav, HttpServletRequest httpRequest, Long orderId,String storeId,String type) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		// 如果有余额支付，可在此查询余额
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order!=null){
			mav.addObject("payno", order.getPayNumber());
		}
		mav.addObject("type", type);
		mav.addObject("storeId", storeId);
		mav.setViewName("h5/order/orderPay");
		return mav;
	}
	
	/**
	 * 订单列表页面
	 * @author limh Create on 2016年10月29日 上午11:28:50
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "orderList")
	public ModelAndView orderList(ModelAndView mav, HttpServletRequest httpRequest,String fromType,Page<SysOrderVo> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));		
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			page.getParams().put("memberId", member.getMemberId());
			Page<SysOrderVo> pageRes = sysOrderService.selectByPage(page);
			
			mav.addObject("page", pageRes);
		}

		String value=fromType==null?"0":"1";
		mav.addObject("fromType", value);
		mav.setViewName("h5/order/orderList");
		return mav;
	}

	/**
	 * 创建订单,重定向到支付方式选择页面
	 * @author limh Create on 2016年10月29日 上午11:28:25
	 * @param mav
	 * @param httpRequest
	 * @param order
	 * @param integral
	 * @return
	 */
	@RequestMapping(value = "creatOrder", method = RequestMethod.POST)
	public ModelAndView creatOrder(ModelAndView mav, HttpServletRequest httpRequest,SysOrder order,Long goodsId,Integer amount,Long voucherId,String time,String date,String weeknum,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			Integer memberId = member.getMemberId();
			if (order != null) {
				String bookTime=date+" "+time;
				SysOrder newOrder=null;
				if(goodsId!=null){
					if(amount==null){
						amount=1;
					}
					//单笔订单
					newOrder = sysOrderService.createSingleOrderAndDetail(order,memberId,bookTime,goodsId,amount,voucherId,weeknum);
				}else{
					//购物车订单
					newOrder = sysOrderService.createOrderAndDetail(order,memberId,bookTime,voucherId,weeknum);
				}
				if (newOrder != null && newOrder.getOrderId() != null) {
					mav.setViewName("redirect:orderPay.html?orderId=" + newOrder.getOrderId()+"&&storeId="+storeId+"&&type=1");
					return mav;
				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		mav.addObject("storeId", storeId);
		return mav;
	}
	/**
	 * 堂食创建订单,重定向到支付方式选择页面
	 * @author limh Create on 2016年10月29日 上午11:28:25
	 * @param mav
	 * @param httpRequest
	 * @param order
	 * @param integral
	 * @return
	 */
	@RequestMapping(value = "eatingCreatOrder", method = RequestMethod.POST)
	public ModelAndView eatingCreatOrder(ModelAndView mav, HttpServletRequest httpRequest,SysOrder order,Long goodsId,Integer amount,Long voucherId,String time,String date,String tableId,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			Integer memberId = member.getMemberId();
			if (order != null) {
				String bookTime=date+" "+time;
				SysOrder newOrder=null;
				if(goodsId!=null){
					if(amount==null){
						amount=1;
					}
					//单笔订单
					newOrder = sysOrderService.createSingleOrderAndDetail1(order,memberId,bookTime,goodsId,amount,voucherId);
				}else{
					//购物车订单
					newOrder = sysOrderService.createOrderAndDetail1(order,memberId,bookTime,voucherId);
				}
				if (newOrder != null && newOrder.getOrderId() != null) {
					mav.setViewName("redirect:orderPay.html?orderId=" + newOrder.getOrderId()+"&&storeId="+storeId+"&&type=2");
					return mav;
				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("storeId", storeId);
		return mav;
	}
	/**
	 * 检查当前星期是否是当天，如果是当天则判断是否超过当天的最后营业时间
	 * @author limh Create on 2016年11月29日 下午12:10:50
	 * @param httpRequest
	 * @param week
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/checkCurrentWeek", method = RequestMethod.GET)
	public ResultVo<Object> checkCurrentWeek(HttpServletRequest httpRequest, String week) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try {
			boolean checkCurrentWeek = bizService.checkCurrentWeek(week);
			if(checkCurrentWeek){
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_003);
				resultVo.setMsg("今天已经打烊啦！到本周菜品展示页面预订其他天的菜品吧！");
				return resultVo;
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		//不是当天的不考虑营业时间，当天没有超过营业时间也不考虑
		return resultVo;
	}
	
	/**
	 * 取消订单
	 * @author limh Create on 2016年10月29日 上午11:28:00
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/cancleOrder", method = RequestMethod.GET)
	public ResultVo<Object> cancleOrder(HttpServletRequest httpRequest, Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if (orderId != null) {
			SysOrder sysOrder = new SysOrder();
			sysOrder.setOrderId(orderId);
			sysOrder.setOrderStatus(Constants.ORDER_STATUS_CANCLED);
			int row = sysOrderService.updateByPrimaryKeySelective(sysOrder);
			if (row != -1) {
				return resultVo;
			}

		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 获取营业时间集合
	 * @author limh Create on 2016年12月3日 上午10:10:20
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getShopTimeList", method = RequestMethod.GET)
	public ResultVo<Object> getShopTimeList(HttpServletRequest httpRequest,String weeknum) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try {
			//获取营业时间每30分钟一段的集合,最后修改为15分钟
			List<String> shopTimeList = bizService.getShopTimeList(weeknum);
			if(shopTimeList==null||shopTimeList.isEmpty()){
				resultVo.setSuccess(false);
				return resultVo;
			}
			resultVo.setData(shopTimeList);
			return resultVo;
		} catch (Exception e) {
			e.printStackTrace();
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 系统订单列表滚动刷新
	 * @author limh Create on 2016年11月1日 下午4:40:01
	 * @param httpRequest
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getOrderList", method = RequestMethod.GET)
	public ResultVo<Object> getOrderList(HttpServletRequest httpRequest, Page<SysOrderVo> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		if(page==null){
			page=new Page<>();
		}
		page.setPageSize(10);
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			page.getParams().put("memberId", member.getMemberId());
			Page<SysOrderVo> pageRes = sysOrderService.selectByPage(page);
			resultVo.setData(pageRes);
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 确认收货
	 * @author limh Create on 2016年10月29日 上午11:27:53
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/sureReceive", method = RequestMethod.GET)
	public ResultVo<Object> sureReceive(HttpServletRequest httpRequest, Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		/*String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);*/
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		Member member=null;
		if(order!=null){
			member = memberService.selectByPrimaryKey(order.getMemberId());
		}
		if(member!=null){
			int row = sysOrderService.sureReceive(orderId, member);
			if (row != -1) {
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 获取当前session中的预定类型
	 * @author limh Create on 2016年11月3日 下午4:17:42
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getCurrentBookType", method = RequestMethod.GET)
	public ResultVo<Object> getCurrentBookType(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		//预定类型存入session中,设置过期时间30分钟
		HttpSession session = httpRequest.getSession();
		BookType BookType = (BookType) session.getAttribute(Constants.SESSION_BOOK_TYPE);
		if(BookType!=null){
			resultVo.setData(BookType.getBookType());
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 订单支付前的验证
	 * @author limh Create on 2016年10月29日 上午11:22:27
	 * @param httpRequest
	 * @param payno
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/validateOrder", method = RequestMethod.POST)
	public ResultVo<Object> validateOrder(HttpServletRequest httpRequest,String payno) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		SysOrder order = sysOrderService.selectByPayNumber(payno);		
		if(order==null){
			resultVo.setSuccess(false);
			resultVo.setMsg("订单不存在！");
			return resultVo;
		}
		if(Constants.ORDER_STATUS_PAIED.equals(order.getPayStatus())){
			resultVo.setSuccess(false);
			resultVo.setMsg("订单已支付！");
			return resultVo;
		}
		return resultVo;
	}
	/**
	 * 订单确认前验证
	 * @author limh Create on 2016年11月30日 下午8:59:22
	 * @param httpRequest
	 * @param goodsId 立即购买时的菜品id 可空
	 * @param amount 立即购买时的数量，可空
	 * @param weeknum 星期数
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/validateBeforeOrderConfirm", method = RequestMethod.GET)
	public ResultVo<Object> validateBeforeOrderConfirm(HttpServletRequest httpRequest,Long goodsId,Integer amount,String weeknum,Integer storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Store store=storeService.selectByPrimaryKey(storeId);
		try{
			//会员禁用
			boolean checkMemberIsForbidden = bizService.checkMemberIsForbidden(wxopenid);
			if(checkMemberIsForbidden){
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_001);
				resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
				return resultVo;
			}
			//判断当天营业时间，如果超过晚上营业时间，提示椒君，很抱歉，已到打烊时间了，您要预订明天餐品吗？
			boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime1(weeknum,storeId);
			if(checkNowIsOverEndTime){
				resultVo.setSuccess(false);
				resultVo.setData(Constants.ERROR_CODE_003);
				resultVo.setMsg("很抱歉，已到打烊时间了，您可以选择预定其他天菜品！");
				return resultVo;
			}
			//菜品禁用
			double amountPrice=0d;
			SysParm realSysParam = sysParmService.getRealSysParam();
			double startPrice = store.getStartingPrice();
			if(goodsId!=null){
				//立即购买
				//菜品禁用
				boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(goodsId,storeId);
				if(checkGoodsIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_007);
					resultVo.setMsg("该菜品已下架！");
					return resultVo;
				}
				//菜品售完
				boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(goodsId,storeId);
				if(checkGoodsIsSoldOut){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_002);
					resultVo.setMsg("该菜品已售完！");
					return resultVo;
				}
				Goods goods = goodsService.selectByPrimaryKey(goodsId);
				amountPrice=goods.getPrice()*amount;
				if(amountPrice<startPrice){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_004);
					resultVo.setMsg("订单总价低于起步价,是否将商品加入购物车？");
					return resultVo;
				}
			}else{
				//购物车
				ConditionModel condition = new ConditionModel();
				condition.getParams().put("memberId", member.getMemberId());
				condition.getParams().put("weeknum", weeknum);
				condition.getParams().put("storeId", storeId);

				List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberIdAndStoreIdAndWeek(member.getMemberId(),storeId,Integer.parseInt(weeknum));
				if(cartList==null||cartList.isEmpty()){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_008);
					resultVo.setMsg("订单已经生成！<br/>到【我的】-【全部订单】中查看吧！");
					return resultVo;
				}
				for(ShopTrolleyVo vo:cartList){
					//菜品禁用
					boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(vo.getGoodsId(), vo.getStoreId());
					if(checkGoodsIsForbidden){
						shopTrolleyService.deleteByPrimaryKey(vo.getId());
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_007);
						resultVo.setMsg(vo.getGoodsName()+"已下架！");
						return resultVo;
					}
					//菜品售完
					boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(vo.getGoodsId(),vo.getStoreId());
					if(checkGoodsIsSoldOut){
						shopTrolleyService.deleteByPrimaryKey(vo.getId());
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_002);
						resultVo.setMsg(vo.getGoodsName()+"已售完！");
						return resultVo;
					}
				}
				amountPrice = shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
				if(amountPrice<startPrice){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_004);
					resultVo.setMsg("订单总价低于起步价,是否继续添加菜品？");
					return resultVo;
				}
			}
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	public static void main(String[] args) {
		System.out.println(1-9.5/10);
	}
	//会员折扣
	@RequestMapping(value="memberCount",method = RequestMethod.GET)
	public  @ResponseBody ResultVo<Object> memberCount(HttpServletRequest httpRequest,String storeId,String weeknum,String goodsId){
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try {
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			Store store=storeService.selectByPrimaryKey(Integer.parseInt(storeId));
			double amountPrice=0.00;
			if(StringUtils.isNotBlank(goodsId)){
				Goods goods = goodsService.selectByPrimaryKey(Long.parseLong(goodsId));
				amountPrice = goods.getPrice()*1;
			}else{
				ConditionModel condition = new ConditionModel();
				condition.getParams().put("memberId", member.getMemberId());
				condition.getParams().put("weeknum", weeknum);
				condition.getParams().put("storeId", storeId);
				amountPrice = shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
			}
			
			MemberLevel level = memberLevelService.selectByPrimaryKey(member.getMemberLevelId());
			Double discount = level.getDiscount();
			Double discountMoney=(1-discount/10)*amountPrice;
			level.setDiscountMoney(discountMoney);
			level.setTotalMoney(amountPrice);
			resultVo.setData(level);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			resultVo.setSuccess(false);
			resultVo.setMsg("error");
			e.printStackTrace();
		}
		return resultVo;
	}
	//门店满减
	@RequestMapping(value="storeFull",method = RequestMethod.GET)
	public  @ResponseBody ResultVo<Object> storeFull(HttpServletRequest httpRequest,String storeId,String weeknum,String goodsId){
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try {
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			Store store=storeService.selectByPrimaryKey(Integer.parseInt(storeId));
			double amountPrice =0.00;
			double jian=0.00;//减
			double fullMoney=0.00;//满
			if(StringUtils.isNotBlank(goodsId)){
				Goods goods = goodsService.selectByPrimaryKey(Long.parseLong(goodsId));
				amountPrice = goods.getPrice()*1;
			}else{
				ConditionModel condition = new ConditionModel();
				condition.getParams().put("memberId", member.getMemberId());
				condition.getParams().put("weeknum", weeknum);
				condition.getParams().put("storeId", storeId);
				amountPrice= shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
			}
			List<Part> parts =partService.wxgetFullPartsByStoreId(store.getId());
			for (Part part : parts) {
				if(part.getType().equals("1")){
					double full = part.getFull().doubleValue();
					if(amountPrice>=full){
						jian=part.getSubstact().doubleValue();
						fullMoney=full;
						break;
					}
				}
			}
			DiscountVo vo=new DiscountVo();
			vo.setSubstact(jian);
			vo.setTotalMoney(amountPrice);
			vo.setFull(fullMoney);
			resultVo.setData(vo);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			resultVo.setSuccess(false);
			resultVo.setMsg("error");
			e.printStackTrace();
		}
		return resultVo;
	}
	//门店折扣
		@RequestMapping(value="storeDiscount",method = RequestMethod.GET)
		public  @ResponseBody ResultVo<Object> storeDiscount(HttpServletRequest httpRequest,String storeId,String weeknum,String goodsId){
			LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
					+ BizHelper.getHttpRequestParameters(httpRequest));
			ResultVo<Object> resultVo = new ResultVo<>();
			try {
				String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
				Member member = memberService.selectByWxOpenId(wxopenid);
				Store store=storeService.selectByPrimaryKey(Integer.parseInt(storeId));
				double amountPrice = 0.00;
				double jian=0.00;
				double discount=0.00;
				if(StringUtils.isNotBlank(goodsId)){
					Goods goods = goodsService.selectByPrimaryKey(Long.parseLong(goodsId));
					amountPrice = goods.getPrice()*1;
				}else{
					ConditionModel condition = new ConditionModel();
					condition.getParams().put("memberId", member.getMemberId());
					condition.getParams().put("weeknum", weeknum);
					condition.getParams().put("storeId", storeId);
					amountPrice=shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
				}
				
				List<Part> parts =partService.wxgetDiscountPartsByStoreId(store.getId());
				if(parts!=null&&parts.size()>0){
					Part part = parts.get(0);
					double doubleValue = part.getDiscount().doubleValue()/10;
					jian=(1-doubleValue)*amountPrice;
					discount=doubleValue;
				}
				
				DiscountVo vo=new DiscountVo();
				vo.setSubstact(jian);
				vo.setTotalMoney(amountPrice);
				vo.setDiscount(discount);
				resultVo.setData(vo);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				resultVo.setSuccess(false);
				resultVo.setMsg("error");
				e.printStackTrace();
			}
			return resultVo;
		}
		//堂食会员折扣
		@RequestMapping(value="eatingmemberCount",method = RequestMethod.GET)
		public  @ResponseBody ResultVo<Object> eatingmemberCount(HttpServletRequest httpRequest,String storeId ,String tableId,String goodsId){
			LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
					+ BizHelper.getHttpRequestParameters(httpRequest));
			ResultVo<Object> resultVo = new ResultVo<>();
			try {
				String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
				Member member = memberService.selectByWxOpenId(wxopenid);
				Store store=storeService.selectByPrimaryKey(Integer.parseInt(storeId));
				double amountPrice=0.00;
				if(StringUtils.isNotBlank(goodsId)){
					Goods goods = goodsService.selectByPrimaryKey(Long.parseLong(goodsId));
					amountPrice = goods.getPrice()*1;
				}else{
					ShopCatVo shopCar = shopCarService.statPriceAndNumByMemberIdTableId(member.getMemberId().longValue(), Long.parseLong(tableId));
					amountPrice=shopCar.getAmountPrice();
				}
				MemberLevel level = memberLevelService.selectByPrimaryKey(member.getMemberLevelId());
				Double discount = level.getDiscount();
				Double discountMoney=(1-discount/10)*amountPrice;
				level.setDiscountMoney(discountMoney);
				level.setTotalMoney(amountPrice);
				resultVo.setData(level);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				resultVo.setSuccess(false);
				resultVo.setMsg("error");
				e.printStackTrace();
			}
			return resultVo;
		}
		//堂食门店满减
		@RequestMapping(value="eatingstoreFull",method = RequestMethod.GET)
		public  @ResponseBody ResultVo<Object> eatingstoreFull(HttpServletRequest httpRequest ,String storeId ,String tableId,String goodsId){
			LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
					+ BizHelper.getHttpRequestParameters(httpRequest));
			ResultVo<Object> resultVo = new ResultVo<>();
			try {
				String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
				Member member = memberService.selectByWxOpenId(wxopenid);
				Store store=storeService.selectByPrimaryKey(Integer.parseInt(storeId));
				double amountPrice=0.00;
				if(StringUtils.isNotBlank(goodsId)){
					Goods goods = goodsService.selectByPrimaryKey(Long.parseLong(goodsId));
					amountPrice = goods.getPrice()*1;
				}else{
					ShopCatVo shopCar = shopCarService.statPriceAndNumByMemberIdTableId(member.getMemberId().longValue(), Long.parseLong(tableId));
					amountPrice=shopCar.getAmountPrice();
				}
				
				double jian=0.00;//减
				double fullMoney=0.00;//满
				List<Part> parts =partService.wxgetFullPartsByStoreId(store.getId());
				for (Part part : parts) {
					if(part.getType().equals("1")){
						double full = part.getFull().doubleValue();
						if(amountPrice>=full){
							jian=part.getSubstact().doubleValue();
							fullMoney=full;
							break;
						}
					}
				}
				DiscountVo vo=new DiscountVo();
				vo.setSubstact(jian);
				vo.setTotalMoney(amountPrice);
				vo.setFull(fullMoney);
				resultVo.setData(vo);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				resultVo.setSuccess(false);
				resultVo.setMsg("error");
				e.printStackTrace();
			}
			return resultVo;
		}
		//堂食门店折扣
			@RequestMapping(value="eatingstoreDiscount",method = RequestMethod.GET)
			public  @ResponseBody ResultVo<Object> eatingstoreDiscount(HttpServletRequest httpRequest,String storeId,String tableId,String goodsId){
				LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
						+ BizHelper.getHttpRequestParameters(httpRequest));
				ResultVo<Object> resultVo = new ResultVo<>();
				try {
					String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
					Member member = memberService.selectByWxOpenId(wxopenid);
					Store store=storeService.selectByPrimaryKey(Integer.parseInt(storeId));
					double amountPrice=0.00;
					if(StringUtils.isNotBlank(goodsId)){
						Goods goods = goodsService.selectByPrimaryKey(Long.parseLong(goodsId));
						amountPrice = goods.getPrice()*1;
					}else{
						ShopCatVo shopCar = shopCarService.statPriceAndNumByMemberIdTableId(member.getMemberId().longValue(), Long.parseLong(tableId));
						amountPrice=shopCar.getAmountPrice();
					}
					double jian=0.00;
					double discount=0.00;
					List<Part> parts =partService.wxgetDiscountPartsByStoreId(store.getId());
					if(parts!=null&&parts.size()>0){
						Part part = parts.get(0);
						double doubleValue = part.getDiscount().doubleValue()/10;
						jian=(1-doubleValue)*amountPrice;
						discount=doubleValue;
					}
					
					DiscountVo vo=new DiscountVo();
					vo.setSubstact(jian);
					vo.setTotalMoney(amountPrice);
					vo.setDiscount(discount);
					resultVo.setData(vo);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					resultVo.setSuccess(false);
					resultVo.setMsg("error");
					e.printStackTrace();
				}
				return resultVo;
			}
	//查询某个门店下活动类型代金卷，满减，折扣，
	@RequestMapping(value="getDisCountType",method = RequestMethod.GET)
	public @ResponseBody ResultVo<Object> getDisCountType (HttpServletRequest request,String storeId){
		ResultVo<Object> resultVo=new ResultVo<>();
		try {
			DiscountTypeVo vo=new DiscountTypeVo();
			List<Part> parts = partService.wxgetPartsByStoreId(Integer.parseInt(storeId));
			for (Part part : parts) {
				if(part.getType().equals("1")&&part.getStatus().equals("1")){
					vo.setFull(true);
					
				}
				if(part.getType().equals("2")&&part.getStatus().equals("1")){
					vo.setHasDisCount(true);
				}
			}
			Store store = storeService.selectByPrimaryKey(Integer.parseInt(storeId));
			if(store.getIsActivity().equals(Constants.STORE_IS_ACTIVITY)){//门店参加平台活动才能获取会员优惠和代金卷优惠
				vo.setMemberDiscount(true);
				vo.setVoucher(true);
			}
			resultVo.setData(vo);
			resultVo.setSuccess(true);
			resultVo.setMsg("success");
			return resultVo;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultVo.setSuccess(false);
			return resultVo;
		}
	}
	//余额支付
		@RequestMapping(value="creditpay")
		public @ResponseBody ResultVo<Object>  creditpay(HttpServletRequest httpRequest,String storeId,String payno){
			ResultVo<Object> resultVo=new ResultVo<>();
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			resultVo=memberService.creditpay(member,storeId,payno,httpRequest);
			return  resultVo;
		}
		//跳转到支付成功页面
		@RequestMapping(value="paysuccess")
		public ModelAndView paysuccess(ModelAndView mav, HttpServletRequest httpRequest,String payno){
			SysOrder order = sysOrderService.selectByPayNumber(payno);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(order.getPayTime()==null){
				mav.setViewName("redirect:orderPay.html?orderId="+order.getOrderId()+"&&storeId="+order.getStoreId()+"&&type="+order.getPayType());
				return mav;
			}
			String time=sdf.format(order.getPayTime());
			order.setPayDate(time);
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			mav.addObject("order", order);
			List<SysParm> paramList = sysParmService.selectAll();
			if(paramList!=null&&paramList.size()>0){
				mav.addObject("sysParm", paramList.get(0));
			}
			mav.setViewName("h5/comment/paySuccess");
			return mav;
		}
		//跳转到订单评论的页面
		@RequestMapping(value="goComment")
		public ModelAndView goComment(ModelAndView mav, HttpServletRequest httpRequest,String payno,String storeId){
			mav.addObject("payno", payno);
			mav.addObject("storeId", storeId);
			mav.setViewName("h5/order/evaluate");
			return mav;
		}
		/**
		 * 保存订单评论
		 * @author limh Create on 2016年10月29日 上午11:20:45
		 * @param httpRequest
		 * @param orderComment
		 * @return
		 */
		@ResponseBody
		@RequestMapping(value = "api/saveOrderComment", method = RequestMethod.POST)
		public ResultVo<Object> saveOrderComment(HttpServletRequest httpRequest, OrderComment orderComment) {
			LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
					+ BizHelper.getHttpRequestParameters(httpRequest));
			ResultVo<Object> resultVo = new ResultVo<>();
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			if(member!=null&&orderComment != null && orderComment.getOrderId() != null){
				int res = sysOrderService.saveOrderComment(orderComment, member);
				if(res!=-1){
					return resultVo;
				}
			}
			resultVo.setSuccess(false);
			return resultVo;
		}
		/**
		 * 抽取代金券
		 * @author limh Create on 2016年11月16日 上午9:44:22
		 * @param httpRequest
		 * @param category 抽奖分类，1评论抽奖，2分享抽奖
		 * @return
		 */
		@ResponseBody
		@RequestMapping(value = "api/lotteryDraw", method = RequestMethod.GET)
		public ResultVo<Object> lotteryDraw(HttpServletRequest httpRequest,String category,String wxopenid) {
			LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
					+ BizHelper.getHttpRequestParameters(httpRequest));
			ResultVo<Object> resultVo = new ResultVo<>();
			if(wxopenid==null||wxopenid==""){
				wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			}
			Member member = memberService.selectByWxOpenId(wxopenid);
			if(member!=null){
				try {
					if(Constants.VOUCHER_LOTTERY_CATEGORY_SHARE.equals(category)){
						//每天一次分享抽奖
						List<MemberIntegral> voucherList = memberVoucherService.selectCurrentDayShareByMemberId(member.getMemberId());
						if(voucherList==null||voucherList.size()<1){
							double voucherMoney = memberService.lotteryDraw(member, category);
							if(voucherMoney<0){
								resultVo.setSuccess(false);
								return resultVo;
							}
							resultVo.setData(voucherMoney);
							return resultVo;
						}
						resultVo.setSuccess(false);
						resultVo.setMsg("分享每天只能抽奖一次！");
						return resultVo;
					}else if(Constants.VOUCHER_LOTTERY_CATEGORY_EVALUATE.equals(category)){
						double voucherMoney = memberService.lotteryDraw(member, category);
						if(voucherMoney<0){
							resultVo.setSuccess(false);
							return resultVo;
						}
						resultVo.setData(voucherMoney);
						return resultVo;
					}
				} catch (NullPointerException | SQLException e) {
					e.printStackTrace();
					resultVo.setSuccess(false);
					return resultVo;
				}
			}
			return null;
		}
		/**
		 * 堂事订单确认前验证
		 * @author limh Create on 2016年11月30日 下午8:59:22
		 * @param httpRequest
		 * @param goodsId 立即购买时的菜品id 可空
		 * @param amount 立即购买时的数量，可空
		 * @param weeknum 星期数
		 * @return
		 */
		@ResponseBody
		@RequestMapping(value = "api/validateBeforeOrderConfirm1", method = RequestMethod.GET)
		public ResultVo<Object> validateBeforeOrderConfirm1(HttpServletRequest httpRequest,Long goodsId,Integer amount,Integer storeId,Integer tableId) {
			LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
					+ BizHelper.getHttpRequestParameters(httpRequest));
			ResultVo<Object> resultVo = new ResultVo<>();
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			Store store=storeService.selectByPrimaryKey(storeId);
			try{
				//会员禁用
				boolean checkMemberIsForbidden = bizService.checkMemberIsForbidden(wxopenid);
				if(checkMemberIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_001);
					resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
					return resultVo;
				}
				//判断当天营业时间，如果超过晚上营业时间，提示椒君，很抱歉，已到打烊时间了，您要预订明天餐品吗？
				boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime2(storeId);
				if(checkNowIsOverEndTime){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_003);
					resultVo.setMsg("很抱歉，已到打烊时间了，您可以选择预定其他天菜品！");
					return resultVo;
				}
				//菜品禁用
				double amountPrice=0d;
				SysParm realSysParam = sysParmService.getRealSysParam();
				double startPrice = store.getStartingPrice();
				if(goodsId!=null){
					//立即购买
					//菜品禁用
					boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(goodsId,storeId);
					if(checkGoodsIsForbidden){
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_007);
						resultVo.setMsg("该菜品已下架！");
						return resultVo;
					}
					//菜品售完
					boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(goodsId,storeId);
					if(checkGoodsIsSoldOut){
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_002);
						resultVo.setMsg("该菜品已售完！");
						return resultVo;
					}
					Goods goods = goodsService.selectByPrimaryKey(goodsId);
					/*amountPrice=goods.getPrice()*amount;
					if(amountPrice<startPrice){
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_004);
						resultVo.setMsg("订单总价低于起步价,是否将商品加入购物车？");
						return resultVo;
					}*/
				}else{
					//购物车
					
					Long memberId=member.getMemberId().longValue();
					List<ShopTrolleyVo> cartList = shopCarService.selectByMemberIdTableId(memberId,tableId.longValue());
					if(cartList==null||cartList.isEmpty()){
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_008);
						resultVo.setMsg("订单已经生成！<br/>到【我的】-【全部订单】中查看吧！");
						return resultVo;
					}
					for(ShopTrolleyVo vo:cartList){
						//菜品禁用
						boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(vo.getGoodsId(), vo.getStoreId());
						if(checkGoodsIsForbidden){
							shopTrolleyService.deleteByPrimaryKey(vo.getId());
							resultVo.setSuccess(false);
							resultVo.setData(Constants.ERROR_CODE_007);
							resultVo.setMsg(vo.getGoodsName()+"已下架！");
							return resultVo;
						}
						//菜品售完
						boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(vo.getGoodsId(),vo.getStoreId());
						if(checkGoodsIsSoldOut){
							shopTrolleyService.deleteByPrimaryKey(vo.getId());
							resultVo.setSuccess(false);
							resultVo.setData(Constants.ERROR_CODE_002);
							resultVo.setMsg(vo.getGoodsName()+"已售完！");
							return resultVo;
						}
					}
					ShopCatVo statCar = shopCarService.statPriceAndNumByMemberIdTableId(memberId, tableId.longValue());
					amountPrice = statCar.getAmountPrice();
					if(amountPrice<startPrice){
						resultVo.setSuccess(false);
						resultVo.setData(Constants.ERROR_CODE_004);
						resultVo.setMsg("订单总价低于起步价,是否继续添加菜品？");
						return resultVo;
					}
				}
				return resultVo;
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}
		/**
		 * 堂食页面确认下单
		 * @author limh Create on 2016年10月29日 上午11:29:01
		 * @param mav
		 * @param httpRequest
		 * @return
		 */
		@RequestMapping(value = "orderConfirm1")
		public ModelAndView orderConfirm1(ModelAndView mav, HttpServletRequest httpRequest,Long goodsId,Integer amount,String storeId,String tableId) {
			LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
					+ BizHelper.getHttpRequestParameters(httpRequest));
			try{
			httpRequest.getSession().removeAttribute(Constants.SESSION_ADDRESS_BACK_CONFIRM);
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			Store store = storeService.selectByPrimaryKey(Integer.parseInt(storeId));
			if (member != null) {
			
				// 待确认订单信息，1.购物车中所有菜品 2.直接购买的菜品
				Integer memberId = member.getMemberId();
				double amountPrice=0;
				if(goodsId!=null){
					Goods goods = goodsService.selectByPrimaryKey(goodsId);
					if(goods!=null){
						Double price = goods.getPrice();
						if(amount==null){
							amount=1;
						}
						amountPrice=price*amount;	
					}
					mav.addObject("goods", goods);
					mav.addObject("amount", amount);
					mav.addObject("subtotalPrice", amountPrice);
				}else{
					List<ShopTrolleyVo> cartList = shopCarService.selectByMemberIdTableId(memberId.longValue(), Long.parseLong(tableId));
					ShopCatVo statCar = shopCarService.statPriceAndNumByMemberIdTableId(memberId.longValue(),Long.parseLong(tableId));
					amountPrice = statCar.getAmountPrice();
					mav.addObject("cartList", cartList);
				}
				/*//判断总价是否小于起步价
				double startPrice = store.getStartingPrice();
				mav.addObject("startPrice",startPrice);
				if(amountPrice<startPrice){
					mav.setViewName("redirect:../store/eatingInStore.html?storeId="+storeId+"&&tableId="+tableId);
					return mav;
				}*/
				//总价，商品总价+配送费-优惠价，此时积分未换算
				mav.addObject("amountPrice", StringUtils.formatMoney2Byte(amountPrice));
				//查询符合条件的优惠券
				VoucherQueryCondition record = new VoucherQueryCondition();
				record.setMemberId(member.getMemberId());
				record.setAmountPrice(amountPrice);
				List<MemberVoucherVo> voucherList = memberVoucherService.selectSatisfactoryVoucher(record);
				mav.addObject("voucherList",voucherList);
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			StoreTable table = storeTableService.selectByPrimaryKey(Integer.parseInt(tableId));
			mav.addObject("storeId", storeId);
			mav.addObject("table", table);
			mav.setViewName("h5/order/eatingOrderConfirm");
			return mav;
		}

	/**
	 * 
	 * @Title: eatinConfirm 
	 * @Description: 堂食订单确认
	 * @param mav
	 * @param httpRequest
	 * @param goodsId
	 * @param amount
	 * @param weeknum
	 * @param storeId
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "eatinConfirm")
	public ModelAndView eatinConfirm(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
//		httpRequest.getSession().removeAttribute(Constants.SESSION_ADDRESS_BACK_CONFIRM);
//		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
//		Member member = memberService.selectByWxOpenId(wxopenid);
//		if (member != null) {
//			
//			// 待确认订单信息，1.购物车中所有菜品 2.直接购买的菜品
//			Integer memberId = member.getMemberId();
//			double amountPrice=0;
//			if(goodsId!=null){
//				Goods goods = goodsService.selectByPrimaryKey(goodsId);
//				if(goods!=null){
//					Double price = goods.getPrice();
//					if(amount==null){
//						amount=1;
//					}
//					amountPrice=price*amount;	
//				}
//				mav.addObject("goods", goods);
//				mav.addObject("amount", amount);
//				mav.addObject("subtotalPrice", amountPrice);
//			}else{
//				ConditionModel condition = new ConditionModel();
//				condition.getParams().put("memberId", memberId);
//				condition.getParams().put("weeknum", weeknum);
//				condition.getParams().put("storeId", storeId);
//				List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberIdAndStoreId(member.getMemberId(),Integer.parseInt(storeId));
//				amountPrice = shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
//				mav.addObject("cartList", cartList);
//			}
//			//判断总价是否小于起步价
//			double startPrice = realSysParam.getStartPrice().doubleValue();
//			mav.addObject("startPrice",startPrice);
//			if(amountPrice<startPrice){
//				mav.setViewName("redirect:productHome.html");
//				return mav;
//			}
//			//配送费
//			Store store = storeService.selectByPrimaryKey(Integer.parseInt(storeId));
//			Double deliverPrice = store.getDeliverFee();
//			mav.addObject("deliverPrice",deliverPrice);
//			amountPrice=amountPrice+deliverPrice;
//			//总价，商品总价+配送费-优惠价，此时积分未换算
//			mav.addObject("amountPrice", StringUtils.formatMoney2Byte(amountPrice));
//			//查询符合条件的优惠券
//			VoucherQueryCondition record = new VoucherQueryCondition();
//			record.setMemberId(member.getMemberId());
//			record.setAmountPrice(amountPrice);
//			List<MemberVoucherVo> voucherList = memberVoucherService.selectSatisfactoryVoucher(record);
//			mav.addObject("voucherList",voucherList);
//			//当前星期数对应的日期
//			int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
//			int temp=Integer.parseInt(weeknum)-currentDayOfWeeK;
//			if(temp<0){
//				temp+=7;
//			}
//			Date addDay = TimeUtil.addDay(temp);
//			mav.addObject("currentDate", TimeUtil.getSTD_DF_10(addDay));
//			mav.addObject("weeknum", weeknum);
//		}
		}catch(Exception e){
//			e.printStackTrace();
		}
//		mav.addObject("storeId", storeId);
		mav.setViewName("h5/order/eatinConfirm");
		return mav;
	}
	/**
	 * 地址列表
	 * @author limh Create on 2016年10月29日 上午11:25:47
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "addressList")
	public ModelAndView addressList(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Page<MemberAddress> page = new Page<>();
		//分页刷新暂不做
		page.setPageSize(50);
		page.getParams().put("memberId", member.getMemberId());
		List<MemberAddress> addressList = memberAddressService.selectByPage(page);
		page.setResults(addressList);
		mav.addObject("page", page);
		mav.setViewName("h5/user/addressList");
		return mav;
	}
	/*********************************
	 * 支付模块
	 **********************************/
	/**
	 * 微信支付
	 * @author limh Create on 2016年10月28日 下午5:00:53
	 * @param httpRequest
	 * @param payno
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/orderPaiedByWx", method = RequestMethod.POST)
	public ResultVo<Map<String, String>> wxPay(HttpServletRequest httpRequest, String payno) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Map<String, String>> result = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		String openid = null;
		if (member != null) {
			openid=member.getWxOpenId();
		}
		if (openid != null && payno != null){
			SysOrder order = sysOrderService.selectByPayNumber(payno);
			//TODO 调用查询接口，查询订单状态，若无则调用统一下单接口，若还未支付则调用支付接口
			// 调用统一下单接口，获得prepay_id
			try {
				ResponseTrade trade = XtwUnifiedOrderHelper.doPay(order, openid, "JSAPI");

				if (trade.getWxUnifiedRes()!= null&& StringUtils.isNotBlank(trade.getWxUnifiedRes().getPrepay_id())) {
					Map<String, String> requestMap = BizHelper.createPlaceOrderRequest(trade.getWxUnifiedRes().getPrepay_id());
					/*WxApiClient apiClient = new WxApiClient();
					String accessToken = wxAccessTokenService.getAccessToken().getToken();
					JsApiTicket ticketVo = apiClient.getJsApiTicket(accessToken, "jsapi");
					if(ticketVo != null && StringUtils.isNotBlank(ticketVo.getTicket())){
						String signature = BizHelper.getJsApiSignature(ticketVo.getTicket(), requestMap.get("noncestr"), requestMap.get("timestamp"),null);
						requestMap.put("signature", signature);
					}*/
					requestMap.put("orderId", order.getOrderId().toString());
					result.setData(requestMap);
					result.setSuccess(true);
					return result;
				}
				result.setMsg(trade.getErrorMsg());
			} catch (Exception e) {
				LOG.error("微信支付-统一下单异常："+e.getMessage());
				e.printStackTrace();
			}
		}
		result.setSuccess(false);
		LOG.info("微信支付失败===>"+result.toString());
		return result;
	}
	/**
	 * 微信支付成功后查询订单状态，返回成功或失败页面
	 * @author limh Create on 2016年10月28日 下午5:00:25
	 * @param mav
	 * @param httpRequest
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "queryorder")
	public ModelAndView queryorder(ModelAndView mav, HttpServletRequest httpRequest,Long orderId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		SysOrder order = sysOrderService.selectByPrimaryKey(orderId);
		if(order!=null){
			if(Constants.ORDER_STATUS_PAIED.equals(order.getPayStatus())){
				//支付成功，跳转到我的订单页面
			}else if(Constants.ORDER_STATUS_UNPAY.equals(order.getPayStatus())){
				//调用订单查询接口，查询订单
			}
		}
		mav.setViewName("redirect:orderList.html");
		return mav;
	}
	/**
	 * 支付宝支付
	 * @author limh Create on 2016年10月28日 下午4:59:42
	 * @param httpRequest
	 * @param payno 订单号，非订单id
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "api/orderPaiedByAlipay")
	public ResultVo<String> wapAliPay(HttpServletRequest httpRequest, String payno){
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<String> resultVo = new ResultVo<String>();
		SysOrder order = sysOrderService.selectByPayNumber(payno);
		//TODO 调用查询接口，查询订单状态，若无则调用统一下单接口，若还未支付则调用支付接口
		if(order==null){
			resultVo.setSuccess(false);
			resultVo.setMsg("订单不存在或已经关闭!");
			return resultVo;
		}
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("subject", "饭好粥到");
		requestMap.put("out_trade_no", order.getPayNumber());
		
		requestMap.put("service", AlipayUtil.SERVICE);
		requestMap.put("_input_charset", AlipayUtil.INPUT_CHARSET);
		requestMap.put("payment_type", AlipayUtil.PAYMENT_TYPE);
		requestMap.put("partner", BizHelper.getAlipayPartner());
		//notify_url 异步通知，return_url 同步通知，两者选其一即可
		requestMap.put("notify_url", BizHelper.getAliWapPayNotifyUrl());
		requestMap.put("return_url", BizHelper.getAliWapPayReturnUrl()+"?payno="+ order.getPayNumber());
		requestMap.put("total_fee", StringUtils.formatMoney2Byte(order.getTotalAmount()));
		requestMap.put("seller_id", BizHelper.getAlipaySellerId());
//		requestMap.put("show_url", BizHelper.getAliWapShowUrl());
		try {
			String sign = BizHelper.signWapAliPayOrder(requestMap);
			requestMap.put("sign", sign);
			requestMap.put("sign_type", AlipayUtil.SIGN_TYPE);
			String payUrl = BizHelper.getWapAliPayUrl(requestMap);
			LOG.info("payUrl=" + payUrl);
			resultVo.setSuccess(true);
			resultVo.setData(payUrl);
			return resultVo;
		} catch (Exception e) {
			LOG.info("error=" + e.getMessage());
			resultVo.setSuccess(false);
			resultVo.setMsg("系统繁忙!");
			return resultVo;
		}
	}
	
}
