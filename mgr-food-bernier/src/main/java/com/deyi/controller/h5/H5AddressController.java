package com.deyi.controller.h5;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Goods;
import com.deyi.entity.Member;
import com.deyi.entity.MemberAddress;
import com.deyi.entity.SysParm;
import com.deyi.model.ConditionModel;
import com.deyi.model.ResultVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.Page;

@Controller
@RequestMapping(value="h5/address")
public class H5AddressController {
private Logger LOG = LoggerFactory.getLogger(H5AddressController.class);
	
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	/*********************************
	 * 地址管理
	 **********************************/
	/**
	 * 跳转到添加地址页面
	 * @author limh Create on 2016年10月29日 上午11:25:26
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "addressAdd")
	public ModelAndView addressAdd(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		mav.setViewName("h5/user/addressAdd");
		return mav;
	}

	/**
	 * 跳转到修改地址页面
	 * @author limh Create on 2016年10月29日 上午11:25:37
	 * @param mav
	 * @param httpRequest
	 * @param addressId
	 * @return
	 */
	@RequestMapping(value = "addressModify")
	public ModelAndView addressModify(ModelAndView mav, HttpServletRequest httpRequest,Integer addressId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		MemberAddress address = memberAddressService.selectByPrimaryKey(addressId);
		if(address!=null){
			mav.addObject("address", address);
		}
		mav.setViewName("h5/user/addressModify");
		return mav;
	}

	/**
	 * 地址列表
	 * @author limh Create on 2016年10月29日 上午11:25:47
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "addressList")
	public ModelAndView addressList(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Page<MemberAddress> page = new Page<>();
		//分页刷新暂不做
		page.setPageSize(50);
		page.getParams().put("memberId", member.getMemberId());
		List<MemberAddress> addressList = memberAddressService.selectByPage(page);
		page.setResults(addressList);
		mav.addObject("page", page);
		mav.setViewName("h5/user/addressList");
		return mav;
	}

	/**
	 * 地区级联选择页面
	 * @author limh Create on 2016年10月29日 上午11:26:02
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "localityChoose")
	public ModelAndView localityChoose(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		mav.setViewName("h5/user/localityChoose");
		return mav;
	}

	/**** api ***/

	/**
	 * 保存地址
	 * @author limh Create on 2016年10月29日 上午11:26:15
	 * @param httpRequest
	 * @param address
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/saveAddress", method = RequestMethod.POST)
	public ResultVo<Object> saveAddress(HttpServletRequest httpRequest, MemberAddress address) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			int result = memberAddressService.changeAddress(address, member);
			if (result != -1) {
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 设置为默认地址
	 * @author limh Create on 2016年10月29日 上午11:26:29
	 * @param httpRequest
	 * @param address
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/set2Default", method = RequestMethod.GET)
	public ResultVo<Object> set2DefaultAddress(HttpServletRequest httpRequest, MemberAddress address) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			int result = memberAddressService.update2DefaultAddress(address, member);
			if (result != -1) {
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	/**
	 * 删除地址
	 * @author limh Create on 2016年10月29日 上午11:26:38
	 * @param httpRequest
	 * @param addressId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/deleteAddress", method = RequestMethod.GET)
	public ResultVo<Object> deleteAddress(HttpServletRequest httpRequest, Integer addressId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		int result = memberAddressService.deleteByPrimaryKey(addressId);
		if(result!=-1){
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	
}
