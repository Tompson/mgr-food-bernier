package com.deyi.controller.h5;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Goods;
import com.deyi.entity.Member;
import com.deyi.entity.ShopCar;
import com.deyi.entity.ShopTrolley;
import com.deyi.entity.Store;
import com.deyi.entity.StoreGoods;
import com.deyi.model.ConditionModel;
import com.deyi.model.ResultVo;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.model.vo.WeeknumStatistics;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopCarService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.TimeUtil;

@Controller
@RequestMapping(value="h5/shopcar")
public class H5ShopCarController {
private Logger LOG = LoggerFactory.getLogger(H5StoreController.class);
	

	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	@Autowired
	private ShopCarService shopCarService;
	/**
	 * 加入购物车
	 * 获取修改购物车中菜品数量
	 * @author limh Create on 2016年10月29日 上午11:23:28
	 * @param httpRequest
	 * @param cart 
	 * @param currentWeek 当前星期数
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/add2Cart", method = RequestMethod.GET)
	public ResultVo<Object> add2Cart(HttpServletRequest httpRequest, ShopTrolley cart,String currentWeek) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		try {
			cart.setWeek(Integer.parseInt(currentWeek));
		} catch (NumberFormatException e1) {

			  Calendar cal = Calendar.getInstance();
		      cal.setTime(new Date());
		      int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		      currentWeek=w+"";
		      cart.setWeek(w);
		}
		if (member != null) {
			try {
				//会员禁用
				boolean checkMemberIsForbidden = bizService.checkMemberIsForbidden(wxopenid);
				if(checkMemberIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_001);
					resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
					return resultVo;
				}
				//判断当天营业时间，如果超过晚上营业时间，提示椒君，很抱歉，已到打烊时间了，您要预订明天餐品吗？
				boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime(currentWeek);
				if(checkNowIsOverEndTime){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_003);
					resultVo.setMsg("很抱歉，已到打烊时间了，您要预订明天餐品吗？");
					return resultVo;
				}
				//菜品禁用
				boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(cart.getGoodsId(),cart.getStoreId());
				if(checkGoodsIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_007);
					resultVo.setMsg("该菜品已下架！");
					return resultVo;
				}
				//菜品售完
				boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(cart.getGoodsId(),cart.getStoreId());
				if(checkGoodsIsSoldOut){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_002);
					resultVo.setMsg("该菜品已售完！");
					return resultVo;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				resultVo.setSuccess(false);
				return resultVo;
			}
			
			cart.setMemberId(member.getMemberId());
			ShopTrolley record = shopTrolleyService.checkGoodsIsExist(cart);
			if (record!= null) {
				// 存在-更新
				if(cart.getId()==null&&cart.getFromType()!=null){
					record.setNum(record.getNum()+1);
					shopTrolleyService.updateByPrimaryKeySelective(record);
				}else{
					Goods goods = goodsService.selectByPrimaryKey(record.getGoodsId());
					if (cart.getNum() == 0||Constants.GOODS_STATUS_SELLOUT.equals(goods.getStatus())) {
						shopTrolleyService.deleteByPrimaryKey(record.getId());
					} else {
						// 更新数量和价格
						ShopTrolley update = new ShopTrolley();
						update.setId(record.getId());
						update.setNum(cart.getNum());
						shopTrolleyService.updateByPrimaryKeySelective(update);
					}
				}
			} else {
				
					if(cart.getGoodsId()!=null){
						Goods goods = goodsService.selectByPrimaryKey(cart.getGoodsId());
						if(Constants.GOODS_STATUS_SELLOUT.equals(goods.getStatus())){
							resultVo.setSuccess(false);
							return resultVo;
						}
						cart.setMemberId(member.getMemberId());
						cart.setMemberName(member.getMemberNick());
						cart.setNum(1);
						StoreGoods sg=goodsService.getStoreGoodsByGoodsIdAndStoreId(cart.getGoodsId(),cart.getStoreId());
						cart.setStoreGoodsId(sg.getId());
						shopTrolleyService.insertSelective(cart);
					}
				
				
			}
			// 统计当前星期数的价格
			ConditionModel condition = new ConditionModel();
			condition.getParams().put("memberId", member.getMemberId());
			condition.getParams().put("weeknum",currentWeek);
			condition.getParams().put("storeId", cart.getStoreId());
			ShopCatVo vo=shopTrolleyService.amountPriceAndNumByWeeknum(condition);
			BigDecimal bg = new BigDecimal(vo.getAmountPrice());
			double price = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			vo.setAmountPrice(price);
			resultVo.setData(vo);
			return resultVo;
		}
		return null;
	}
	
	/**
	 * 跳转到购物车页面
	 * @author limh Create on 2016年10月29日 上午11:23:08
	 * @param mav
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "shopTrolley")
	public ModelAndView shopTrolley(ModelAndView mav, HttpServletRequest httpRequest,String storeId,String week) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		Integer sId=Integer.parseInt(storeId);
		Store store=storeService.selectByPrimaryKey(sId);
		try{
			// 需将星期数计算为时间，计算星期数与当天星期数的差值，大于等于0按当前时间+差值，小于等于0按当前时间+7+差值
			int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
			Map<String,String> weekTime=new HashMap<>();
			int [] weeks={1,2,3,4,5,6,7};
			for(int weeknum:weeks){
				int temp=weeknum-currentDayOfWeeK;
				if(temp<0){
					temp+=7;
				}
				Date addDay = TimeUtil.addDay(temp);
				weekTime.put(String.valueOf(weeknum), TimeUtil.getSTD_DF_10(addDay));
			}
			mav.addObject("weekTime", weekTime);
			// 需要按星期分大类
			
			List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberIdAndStoreId(member.getMemberId(),sId);
			mav.addObject("cartList", cartList);
			//分类统计总价
//			List<WeeknumStatistics> amountPriceList = shopTrolleyService.amountPriceByMemberIdCategory(member.getMemberId());
			List<WeeknumStatistics> amountPriceList=new ArrayList<WeeknumStatistics>();
			List<Integer> weekLsit=shopTrolleyService.getweekSByMemberIdAndStoreId(member.getMemberId(),sId);
			for (Integer w : weekLsit) {
				WeeknumStatistics  amountPrice=shopTrolleyService.getWeeknumStatistic(w,member.getMemberId(),sId);
				amountPriceList.add(amountPrice);
			}
			mav.addObject("amountPriceList", amountPriceList);
			mav.addObject("memberStatus", member.getWxGroupid());
			boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime(String.valueOf(currentDayOfWeeK));
			mav.addObject("checkNowIsOverEndTime", checkNowIsOverEndTime);
			mav.addObject("currentDayOfWeeK", String.valueOf(currentDayOfWeeK));
			mav.addObject("storeId", store.getId());
		}catch(Exception e){
			LOG.info("shopTrolley-->throw exception");
			e.printStackTrace();
		}
		mav.setViewName("h5/shopping/shopTrolley");
		return mav;
	}

	/**
	 * 删除购物车中禁用的菜品
	 * @author limh Create on 2016年12月2日 下午5:38:18
	 * @param httpRequest
	 * @param cart
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/deleteForbiddenGoods", method = RequestMethod.GET)
	public ResultVo<Object> deleteForbiddenGoods(HttpServletRequest httpRequest,ShopTrolley cart) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		try {
		Member member = memberService.selectByWxOpenId(wxopenid);
		cart.setMemberId(member.getMemberId());
		ShopTrolley record = shopTrolleyService.checkGoodsIsExist(cart);
		if (record != null) {
			//删除禁用或者售完的菜品
			boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(cart.getGoodsId(),cart.getStoreId());
			boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(cart.getGoodsId(),cart.getStoreId());
			if(checkGoodsIsForbidden||checkGoodsIsSoldOut){
				shopTrolleyService.deleteByPrimaryKey(record.getId());
				return resultVo;
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		resultVo.setSuccess(false);
		return resultVo;
	}

	/**
	 * 清空购物车
	 * @author limh Create on 2016年10月29日 上午11:24:26
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/emptyShopCart", method = RequestMethod.GET)
	public ResultVo<Object> emptyShopCart(HttpServletRequest httpRequest,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		int row = shopTrolleyService.deleteByMemberIdAndStoreId(member.getMemberId(),Integer.parseInt(storeId));
		if (row != -1) {
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	/**
	 * 
	 * @Title: shopCar 
	 * @Description: 购物车页面_堂食
	 * @param mav
	 * @param httpRequest
	 * @param storeId
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "shopCar")
	public ModelAndView shopCar(ModelAndView mav, HttpServletRequest httpRequest,String storeId,String tableId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		LOG.info("门店id  storeId========================="+storeId);
		Integer sId=Integer.parseInt(storeId);
		Store store=storeService.selectByPrimaryKey(sId);
		try{
			Map<String,Object> weekTime=new HashMap<>();
			int currentDayOfWeeK = TimeUtil.getCurrentDayOfWeeK();
			Date addDay = TimeUtil.addDay(0);
			weekTime.put("week", currentDayOfWeeK);
			weekTime.put("date", TimeUtil.getSTD_DF_10(addDay));
			long memberId = member.getMemberId().longValue();
			long storeId2 = sId.longValue();
			long tid = Long.valueOf(tableId);
			List<ShopTrolleyVo> cartList = shopCarService.selectByMemberIdTableId(memberId,tid);
			ShopCatVo statCar = shopCarService.statPriceAndNumByMemberIdTableId(memberId, tid);
			mav.addObject("weekTime", weekTime);
			mav.addObject("cartList", cartList);
			mav.addObject("statCar", statCar);
			mav.addObject("memberStatus", member.getWxGroupid());
			mav.addObject("storeId", store.getId());
			mav.addObject("tableId", tableId);
		}catch(Exception e){
			LOG.info("shopCar-->throw exception");
			e.printStackTrace();
		}
		mav.setViewName("h5/shopping/shopCar");
		return mav;
	}
	
	/**
	 * 
	 * @Title: emptyShopCart2 
	 * @Description: 清空堂食购物车
	 * @param httpRequest
	 * @return
	 * ResultVo<Object>
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "api/emptyShopCart2", method = RequestMethod.GET)
	public ResultVo<Object> emptyShopCart2(HttpServletRequest httpRequest,String storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		int row = shopCarService.deleteByMemberIdStoreId(member.getMemberId().longValue(), Long.valueOf(storeId));
		if (row != -1) {
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "api/add2Cart2", method = RequestMethod.GET)
	public ResultVo<Object> add2Cart2(HttpServletRequest httpRequest, ShopTrolleyVo cart) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if (member != null) {
			try {
				//会员禁用
				boolean checkMemberIsForbidden = bizService.checkMemberIsForbidden(wxopenid);
				if(checkMemberIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_001);
					resultVo.setMsg("您的账号已被禁用，您在本系统购物将受到限制！！！");
					return resultVo;
				}
				//判断当天营业时间，如果超过晚上营业时间，提示椒君，很抱歉，已到打烊时间了，您要预订明天餐品吗？
				boolean checkNowIsOverEndTime = bizService.checkNowIsOverEndTime2(cart.getStoreId());
				if(checkNowIsOverEndTime){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_003);
					resultVo.setMsg("很抱歉，已到打烊时间了！");
					return resultVo;
				}
				//菜品禁用
				boolean checkGoodsIsForbidden = bizService.checkGoodsIsForbidden(cart.getGoodsId());
				if(checkGoodsIsForbidden){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_007);
					resultVo.setMsg("该菜品已下架！");
					return resultVo;
				}
				//菜品售完
				boolean checkGoodsIsSoldOut = bizService.checkGoodsIsSoldOut(cart.getGoodsId(),cart.getStoreId());
				if(checkGoodsIsSoldOut){
					resultVo.setSuccess(false);
					resultVo.setData(Constants.ERROR_CODE_002);
					resultVo.setMsg("该菜品已售完！");
					return resultVo;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				resultVo.setSuccess(false);
				return resultVo;
			}
			cart.setMemberId(member.getMemberId());
			int i =shopCarService.checkIsSelected(member.getMemberId(),cart.getTableId(),cart.getStoreGoodsId());
			if(i<1){
				//不存在，插入
				ShopCar shopCar = new ShopCar();
				shopCar.setMemberId(member.getMemberId().longValue());
				shopCar.setNum(cart.getNum());
				shopCar.setStoreGoodsId(cart.getStoreGoodsId());
				shopCar.setStoreId(cart.getStoreId().longValue());
				shopCar.setTableId(cart.getTableId().longValue());
				shopCarService.insertSelective(shopCar);
			}else{
				// 存在-更新
				if(cart.getId()==null&&cart.getFromType()!=null){
					ShopCar shopCar=shopCarService.selectShopCarByMemberIdAndTableIdAndStoreGoodsId(member.getMemberId(),cart.getTableId(),cart.getStoreGoodsId());
					shopCar.setNum(shopCar.getNum()+1);
					shopCarService.updateByPrimaryKeySelective(shopCar);
				}else{
					long id = cart.getId().longValue();
					if (cart.getNum() == 0) {
						shopCarService.deleteByPrimaryKey(id);
					} else {
						// 更新数量和价格
						ShopCar update = new ShopCar();
						update.setId(id);
						update.setNum(cart.getNum());
						shopCarService.updateByPrimaryKeySelective(update);
					}
				}
			}
			long tableId = cart.getTableId().longValue();
			// 统计当前价格
			ShopCatVo statCar = shopCarService.statPriceAndNumByMemberIdTableId(member.getMemberId().longValue(),tableId );
			if(statCar==null){
				statCar=new ShopCatVo();
				statCar.setAmountPrice(0.00);
				statCar.setNum(0);
			}

			resultVo.setData(statCar);
			return resultVo;
		}
		return null;
	}
	
	
	
}
