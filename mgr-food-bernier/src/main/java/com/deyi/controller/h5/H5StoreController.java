package com.deyi.controller.h5;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.CategoryGoods;
import com.deyi.entity.Goods;
import com.deyi.entity.Member;
import com.deyi.entity.Part;
import com.deyi.entity.ShopTrolley;
import com.deyi.entity.Store;
import com.deyi.model.ConditionModel;
import com.deyi.model.ResultVo;
import com.deyi.model.vo.OrderCommentVo;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopCarService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.IpUtils;
import com.deyi.util.Page;
import com.deyi.util.StringUtils;
import com.deyi.util.TimeUtil;

@Controller
@RequestMapping(value="h5/store")
public class H5StoreController {
	private Logger LOG = LoggerFactory.getLogger(H5StoreController.class);
	

	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	@Autowired
	private ShopCarService shopCarService;
	
	/**
	 * 
	 * @Title: storeHome 
	 * @Description: 门店列表页面
	 * @param mav
	 * @param httpRequest
	 * @param page
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@RequestMapping(value="storeHome")
	public ModelAndView storeHome(ModelAndView mav, HttpServletRequest httpRequest,Page<Store> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member.getGeoLat()==null||member.getGeoLng()==null){
			try {
				String ipAddress = IpUtils.getIpAddress(httpRequest);
				String[] ipxy = IpUtils.getIPXY(ipAddress);
				page.getParams().put("lat", ipxy[1]);
				page.getParams().put("lng", ipxy[0]);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				page.getParams().put("lat", "22.589800");
				page.getParams().put("lng", "113.871703");
			}
		}else{
			page.getParams().put("lat", member.getGeoLat().doubleValue());
			page.getParams().put("lng", member.getGeoLng().doubleValue());
		}
		
		List<Store> stores=storeService.wxgetStoresList(page);
		for (Store store : stores) {
			List<Part> parts=partService.wxgetPartsByStoreId(store.getId());
			store.setParts(parts);
		}
		page.setResults(stores);
		mav.addObject("stores", stores);
		mav.addObject("page", page);
		mav.setViewName("h5/store/storelist");
		return mav;
	}
	
	/**
	 * 
	 * @Title: storeHome1 
	 * @Description: 门店列表分页
	 * @param httpRequest
	 * @param page
	 * @return
	 * Page<Store>
	 * @throws
	 */
	@RequestMapping(value="storeHome1")
	public @ResponseBody Page<Store> storeHome1(HttpServletRequest httpRequest,Page<Store> page) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);

		page.getParams().put("lat", member.getGeoLat().doubleValue());
		page.getParams().put("lng", member.getGeoLng().doubleValue());
		List<Store> stores=storeService.wxgetStoresList(page);
		for (Store store : stores) {
			List<Part> parts=partService.wxgetPartsByStoreId(store.getId());
			store.setParts(parts);
		}
		page.setResults(stores);
		return page;
	}
	
	/**
	 * 门店页面-外卖入口
	 * @param mav
	 * @param httpRequest
	 * @param id 门店id
	 * @param week 星期数，为空默认取今天的星期数
	 * @param toWeek 值为tomorrow时跳转到明天的星期数，空时忽略 
	 * @return门店详情
	 */
	@RequestMapping(value="msgStore")
	public ModelAndView msgStore(ModelAndView mav, HttpServletRequest httpRequest,String id,String week,String toWeek) {
		try {
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			Store store = storeService.selectByPrimaryKey(Integer.parseInt(id));
			try {
				//距离计算
				double distance = StringUtils.getDistance(member.getGeoLng().doubleValue(), member.getGeoLat().doubleValue(), store.getLng(), store.getLat());
				BigDecimal b=new   BigDecimal(distance/1000);  
				double f1  =b.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();  
				store.setDistance(f1);
			} catch (Exception e) {
				store.setDistance(0.0);
			}
			//门店活动
			List<Part> parts=partService.wxgetPartsByStoreId(store.getId());
			store.setParts(parts);
			
			//商品类型
			List<CategoryGoods> categoryList=categoryGoodsService.wxgetCategoryLsit(store.getId());
			for (CategoryGoods categoryGoods : categoryList) {
				List<Goods> goodsList=goodsService.wxgetGoodListByStoreIdAndCategoryId(store.getId(),categoryGoods.getCategoryId());
				categoryGoods.setGoodsList(goodsList);
			}
			//获取星期数
			String weeknum=null;
			if(week!=null){
				weeknum=week;
			}else{
				int currentDayOfWeek = TimeUtil.getCurrentDayOfWeeK();
				if(toWeek!=null&&"tomorrow".equalsIgnoreCase(toWeek)){
					currentDayOfWeek++;
					if(currentDayOfWeek>7){
						currentDayOfWeek=1;
					}
				}
				String currentWeek=String.valueOf(currentDayOfWeek);
				mav.addObject("currentDayOfWeek", currentWeek);
				weeknum=currentWeek;
				//传入当前星期到页面，用于页面选择，刷新其他页面使用ajax异步刷新
			}
			
			// 购物车数量和价格统计
			ConditionModel condition = new ConditionModel();
			condition.getParams().put("memberId", member.getMemberId());
			condition.getParams().put("weeknum",weeknum);
			condition.getParams().put("storeId", id);
			ShopCatVo carStat=shopTrolleyService.amountPriceAndNumByWeeknum(condition);
			mav.addObject("week", weeknum);
			mav.addObject("store", store);
			mav.addObject("categoryList", categoryList);
			mav.addObject("carStat", carStat);//购物车统计
		} catch (Exception e) {
			e.printStackTrace();
		}
		mav.setViewName("h5/store/msgstore");
		return mav;
	}
	
	/**
	 * 
	 * @Title: getStoreGoodsAndCategory 
	 * @Description: 获取门店的所有商品和分类
	 * @param httpRequest
	 * @param week 星期数
	 * @return
	 * Page<Store>
	 * @throws
	 */
	@RequestMapping(value="api/getStoreGoodsAndCategory")
	@ResponseBody
	public  Object getStoreGoodsAndCategory(HttpServletRequest httpRequest,Integer storeId,Integer week) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> vo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		//商品及分类
		List<CategoryGoods> categoryList=categoryGoodsService.wxgetCategoryLsit(storeId);
		for (CategoryGoods categoryGoods : categoryList) {
			List<Goods> goodsList=goodsService.wxgetGoodListByStoreIdAndCategoryId(storeId,categoryGoods.getCategoryId());
			categoryGoods.setGoodsList(goodsList);
		}
		// 购物车数量和价格统计
		ConditionModel condition = new ConditionModel();
		condition.getParams().put("memberId", member.getMemberId());
		condition.getParams().put("weeknum",week);
		condition.getParams().put("storeId", storeId);
		ShopCatVo carStat=shopTrolleyService.amountPriceAndNumByWeeknum(condition);
		
		Map<String, Object> map = new HashMap<>();
		map.put("categoryList", categoryList);
		map.put("carStat", carStat);//购物车统计
		vo.setData(map);
		return vo;
	}
	
	
	
	/**
	 * 门店菜品详情页面
	 * @author limh Create on 2016年10月29日 上午11:30:54
	 * @param mav
	 * @param httpRequest
	 * @param goodsId
	 * @return
	 */
	@RequestMapping(value = "goodsDetail")
	public ModelAndView goodsDetail(ModelAndView mav, HttpServletRequest httpRequest, Long sgid,String week,String fromType,String tableId,Integer storeId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		if (sgid != null) {
			Goods goods = goodsService.selectGoodsDetailBySgId(sgid);
			mav.addObject("goods", goods);
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			// 订单评论表查出所有评论
			Page<OrderCommentVo> page = new Page<>();
			page.getParams().put("storeGoodsId", sgid);
			List<OrderCommentVo> commentVoList = orderCommentService.selectGoodsCommemtByGoodsId(page);
			mav.addObject("commentVoList", commentVoList);
			mav.addObject("page", page);
			String currentDayOfWeek = BizHelper.getConvertedDayOfWeek();
			mav.addObject("currentDayOfWeek", currentDayOfWeek);
//			String serverUrl = BizHelper.getServerUrl();
//			mav.addObject("serverUrl", serverUrl);
			if(fromType.equals("1")){
				ShopTrolley st=shopTrolleyService.selectByMemberIdAndStoreGoodsIdAndWeek(member.getMemberId(), sgid, Integer.parseInt(week));
				int num =st==null?0:st.getNum();
				mav.addObject("num", num);
				mav.addObject("week", week);
				mav.setViewName("h5/product/productDetail");
				mav.addObject("fromType", fromType);
			}else {
				ShopTrolleyVo s = shopCarService.selectByMemberIdStoreGoodsId(member.getMemberId().longValue(), sgid);
				int num =s==null?0:s.getNum();
				mav.addObject("num", num);
				mav.addObject("fromType", fromType);
				mav.addObject("tableId", tableId);
				mav.addObject("storeGoodsId", sgid);
				mav.setViewName("h5/product/eatingProductDetail");
			}
			return mav;
		}
		return mav;
	}
	
	/**
	 * 
	 * @Title: eatingInStore 
	 * @Description: 门店信息，堂食入口
	 * @param mav
	 * @param httpRequest
	 * @param id
	 * @param week
	 * @param toWeek
	 * @return
	 * ModelAndView
	 * @throws
	 */
	@RequestMapping(value="eatingInStore")
	public ModelAndView eatingInStore(ModelAndView mav, HttpServletRequest httpRequest,String storeId,String tableId) {
		try {
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			Store store = storeService.selectByPrimaryKey(Integer.parseInt(storeId));
			if(store.getStatus().equals(Constants.STORE_STATUS_2)){//停用
				mav.setViewName("h5/404");
				return mav;
			}
			//门店活动
			List<Part> parts=partService.wxgetPartsByStoreId(store.getId());
			store.setParts(parts);
			
			//商品类型
			List<CategoryGoods> categoryList=categoryGoodsService.wxgetCategoryLsit(store.getId());
			for (CategoryGoods categoryGoods : categoryList) {
				List<Goods> goodsList=goodsService.wxgetGoodListByStoreIdAndCategoryId(store.getId(),categoryGoods.getCategoryId());
				categoryGoods.setGoodsList(goodsList);
			}
			
			// 购物车数量和价格统计
			ShopCatVo carStat = shopCarService.statPriceAndNumByMemberIdTableId(member.getMemberId().longValue(), Long.valueOf(tableId));
			if(carStat==null){
				carStat=new ShopCatVo();
				carStat.setAmountPrice(0.00);
				carStat.setNum(0);
			}
			mav.addObject("store", store);
			mav.addObject("tableId", tableId);
			mav.addObject("categoryList", categoryList);
			mav.addObject("carStat", carStat);//购物车统计
		} catch (Exception e) {
			e.printStackTrace();
		}
		mav.setViewName("h5/store/eatingInStore");
		return mav;
	}
	
}
