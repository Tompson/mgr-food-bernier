package com.deyi.controller.h5;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.GroupPurchase;
import com.deyi.entity.Member;
import com.deyi.entity.SysParm;
import com.deyi.model.ResultVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;

@Controller
@RequestMapping(value="h5/other")
public class H5OtherController {
private Logger LOG = LoggerFactory.getLogger(H5OtherController.class);
	
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	@RequestMapping(value = "groupBuyDesc")
	public ModelAndView groupBuyDesc(ModelAndView mav, HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		try{
			SysParm realSysParam = sysParmService.getRealSysParam();
			String partnerPhone = realSysParam.getBookDiscount();
			mav.addObject("partnerPhone", partnerPhone);
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.setViewName("h5/other/groupBuyDesc");
		return mav;
	}
	/**
	 * 菜品分享，给予积分奖励
	 * @author limh Create on 2016年10月29日 下午2:45:24
	 * @param httpRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/getShareAward", method = RequestMethod.GET)
	public ResultVo<Object> getShareAward(HttpServletRequest httpRequest) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			int shareAward = memberService.getShareAward(member);
			if(shareAward>=0){
				resultVo.setData(shareAward);
				return resultVo;
			}
		}
		resultVo.setSuccess(false);
		return resultVo;
	}

	/**
	 * 保存团餐申请的公司信息
	 * @author limh Create on 2016年11月28日 下午3:59:25
	 * @param httpRequest
	 * @param record 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "api/saveCompanyInfo", method = RequestMethod.POST)
	public ResultVo<Object> saveCompanyInfo(HttpServletRequest httpRequest,GroupPurchase record) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		try{
			String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
			Member member = memberService.selectByWxOpenId(wxopenid);
			record.setMemberId(member.getMemberId());
			record.setCreateTime(new Date());
			record.setWxOpenid(wxopenid);
			groupPurchaseService.insertSelective(record);
			return resultVo;
		}catch(Exception e){
			e.printStackTrace();
			resultVo.setSuccess(false);
			return resultVo;
		}
	}
	
	
	
	
}
