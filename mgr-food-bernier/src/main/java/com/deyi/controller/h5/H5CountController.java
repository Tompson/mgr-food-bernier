package com.deyi.controller.h5;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deyi.entity.Member;
import com.deyi.entity.MemberIntegral;
import com.deyi.model.ResultVo;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.CategoryGoodsService;
import com.deyi.service.GoodsService;
import com.deyi.service.GroupPurchaseService;
import com.deyi.service.IBizService;
import com.deyi.service.IWxPayService;
import com.deyi.service.MemberAddressService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.PartService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;

@Controller
@RequestMapping(value="h5/count")
public class H5CountController {
	
private Logger LOG = LoggerFactory.getLogger(H5StoreController.class);
	

	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private IBizService bizService;
	
	@Autowired
	private IWxPayService wxPayService;//微信支付回调 成功后消息推送
	
	@Autowired
	private GroupPurchaseService groupPurchaseService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberAddressService memberAddressService;
	 @Autowired
	 private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	@Autowired
	private SysOrderService sysOrderService;
	
	 @Autowired
	 private SysParmService sysParmService;
	@Autowired
	private CategoryGoodsService categoryGoodsService;
	 @Autowired
	 private WxAccessTokenService wxAccessTokenService;
	@Autowired
	private  MemberVoucherService memberVoucherService;
	@Autowired
	private  VoucherService voucherService;
	@Autowired 
	private CarouselFigureService carouselFigureService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private PartService partService;
	/***api***/
	/**
	 * 积分兑换代金券
	 * @author limh Create on 2016年11月16日 上午11:32:17
	 * @param httpRequest
	 * @param voucherId 代金券Id
	 * @return 剩余的积分
	 */
	@ResponseBody
	@RequestMapping(value = "api/exchangeVoucher", method = RequestMethod.GET)
	public ResultVo<Object> converIntegral(HttpServletRequest httpRequest, Long voucherId) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			try {
				Integer r = memberService.exchangeVoucher(member, voucherId);
				if(r==-1){
					resultVo.setSuccess(false);
					resultVo.setMsg("积分不够！");
					return resultVo;
				}
				resultVo.setData(r);
				return resultVo;
			} catch (NullPointerException | SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 积分换算成金额
	 * @author limh Create on 2016年10月28日 下午5:59:52
	 * @param httpRequest
	 * @param integral 积分
	 * @deprecated
	 * @return 
	 */
	@ResponseBody
	@RequestMapping(value = "api/converIntegral", method = RequestMethod.POST)
	public ResultVo<Object> converIntegral(HttpServletRequest httpRequest, Integer integral,Double amountPrice) {
		LOG.info("enter--->url==" + httpRequest.getRequestURL() + "---params=="
				+ BizHelper.getHttpRequestParameters(httpRequest));
		ResultVo<Object> resultVo = new ResultVo<>();
		String wxopenid = (String)httpRequest.getSession().getAttribute(Constants.SESSION_WXOPENID);
		Member member = memberService.selectByWxOpenId(wxopenid);
		if(member!=null){
			MemberIntegral memberIntegral = memberIntegralService.selectByMemberId(member.getMemberId());
			Integer maxIntegral = memberIntegral.getIntegral();
			//积分换算的金额
			double converPrice = 0;
			if(integral>maxIntegral){
				//默认转为最大积分
				converPrice = sysParmService.converIntegral(maxIntegral);
			}else{
				converPrice = sysParmService.converIntegral(integral);
			}
			if(converPrice>amountPrice){
				converPrice=amountPrice;
			}
			resultVo.setData(converPrice);
			return resultVo;
		}
		resultVo.setSuccess(false);
		return resultVo;
	}
}
