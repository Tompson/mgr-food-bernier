package com.deyi.controller.h5;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deyi.model.AccessToken;
import com.deyi.model.ResultVo;
import com.deyi.model.WxErrorVo;
import com.deyi.model.WxMenu;
import com.deyi.model.WxMenuResult;
import com.deyi.net.WxApiClient;
import com.deyi.service.WxAccessTokenService;
import com.deyi.service.impl.WxCallbackService;
import com.deyi.util.Constants;
import com.deyi.util.SignUtil;

/**
 * APP端或PC端统一接口
 * 
 * @author admin
 *
 */

@Controller
@RequestMapping(value = "wxapi")
public class WxApiController {
	Logger LOG = LoggerFactory.getLogger(WxApiController.class);
	
	@Autowired
	private WxAccessTokenService wxAccessTokenService;
	
	@Autowired
	private WxCallbackService wxCallbackService;

	@RequestMapping(value = "callback", method = RequestMethod.GET)
	public void wechatGet(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "signature") String signature, @RequestParam(value = "timestamp") String timestamp,
			@RequestParam(value = "nonce") String nonce, @RequestParam(value = "echostr") String echostr) {

		String printStr = "";
		LOG.info(String.format("GET:/wxapi/callback.html?signature=%s&timestamp=%s&nonce=%s&echostr=%s", signature, timestamp, nonce, echostr));
		if (SignUtil.checkSignature(Constants.TOKEN, signature, timestamp, nonce)) {
			printStr = echostr;
		}
		try {
			response.getWriter().print(printStr);
		} catch (IOException e) {
			LOG.error(String.format("/wxapi/callback.html:error=", e.getMessage()));
		}

	}
	
	@RequestMapping(value = "callback", method = RequestMethod.POST)
	public void wechatPost(HttpServletResponse response,
			HttpServletRequest httpRequest) throws IOException {
		LOG.info("POST:/wxapi/callback.html");
		String respMessage = wxCallbackService.handleCallback(httpRequest);
		PrintWriter out = response.getWriter();
		out.print(respMessage);
		out.close();
	}
	
	@RequestMapping(value = "createMenu", method = RequestMethod.POST)
	public @ResponseBody ResultVo<WxErrorVo> createMenu(@RequestBody WxMenu menu){
		LOG.info("POST:/wxapi/createMenu.html");
		ResultVo<WxErrorVo> vo = new ResultVo<>();
		try {
			AccessToken token = wxAccessTokenService.getAccessToken();
			
			WxApiClient apiClient = new WxApiClient();
			WxErrorVo errorVo = apiClient.createMenu(token.getToken(), menu);
			
			vo.setData(errorVo);
		} catch (Exception e) {
			
		}
		
		return vo;
	}
	
	@RequestMapping(value = "getMenu", method = RequestMethod.GET)
	public @ResponseBody ResultVo<WxMenuResult> getMenu(){
		LOG.info("POST:/wxapi/getMenu.html");
		ResultVo<WxMenuResult> vo = new ResultVo<>();
		try {
			AccessToken token = wxAccessTokenService.getAccessToken();
			
			WxApiClient apiClient = new WxApiClient();
			WxMenuResult menu = apiClient.getMenu(token.getToken());
			vo.setData(menu);
		} catch (Exception e) {
			
		}
		
		return vo;
	}
	
	@RequestMapping(value = "deleteMenu", method = RequestMethod.GET)
	public @ResponseBody ResultVo<WxErrorVo> deleteMenu(){
		LOG.info("POST:/wxapi/deleteMenu.html");
		ResultVo<WxErrorVo> vo = new ResultVo<>();
		try {
			AccessToken token = wxAccessTokenService.getAccessToken();
			
			WxApiClient apiClient = new WxApiClient();
			WxErrorVo errorVo = apiClient.deleteMenu(token.getToken());
			
			vo.setData(errorVo);
		} catch (Exception e) {
			
		}
		
		return vo;
	}
	
}
