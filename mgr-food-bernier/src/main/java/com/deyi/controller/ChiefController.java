package com.deyi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.Chief;
import com.deyi.entity.Store;
import com.deyi.entity.SysUser;
import com.deyi.service.ChiefService;
import com.deyi.service.StoreService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value="chief")
public class ChiefController extends Component<Chief> {
	private Logger log = LoggerFactory.getLogger(DeliverManController.class);
	@Autowired
	private ChiefService chiefService;
	@Autowired
	private StoreService storeService;
	
    @RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("chief/chiefList");
		return mav;
	}
    @ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<Chief> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("厨师列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<Chief> page = new Page<Chief>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<Chief> results=chiefService.queryPage(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}
    @RequestMapping(value = "toChiefAdd")
	public ModelAndView toChiefAdd(ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
    	HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		List<Store>stores=storeService.queryAllStore();
		mav.addObject("stores", stores);
		mav.setViewName("chief/chiefAdd");
		return mav;
	}
    @ResponseBody
	@RequestMapping(value = "chiefAdd")
	public  ModelAndView chiefAdd(ModelAndView mav, HttpServletRequest request,Chief chief) {
    	ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			HttpSession session = request.getSession();
			SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
			int i=chiefService.insertChief(chief);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			vo.setSuccess(false);
			vo.setMessage("添加失败");
		}
		
		return mav;
    }
    @RequestMapping(value = "toChiefEdit")
	public ModelAndView toChiefEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession();// 获取Session中的用户信息
		Chief chief = chiefService.selectByPrimaryKey(Integer.parseInt(id));
		mav.addObject("chief",chief);
		List<Store>stores=storeService.queryAllStore();
		if(chief.getStoreId()!=null&&chief.getStoreId()!=0){
			Store store = storeService.selectByPrimaryKey(chief.getStoreId());
			if(store!=null){
				mav.addObject("store", store);
			}
		}
		mav.addObject("stores", stores);
		mav.setViewName("chief/chiefEdit");
		return mav;
	}

	@RequestMapping(value = "chiefEdit")
	public @ResponseBody ReturnVo<Object> deliverManEdit(ModelAndView mav, HttpServletRequest request, Chief chief) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			chiefService.updateByPrimaryKeySelective(chief);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	/**
	 * 删除商品
	 */
	@ResponseBody
	@RequestMapping(value = "deleteChief")
	public  ReturnVo<Object> deleteChief(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			chiefService.deleteByPrimaryKey(Integer.parseInt(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}

	/**
	 * 禁用
	 * @Author zjz Create on 2016年10月21日 下午2:06:03
	 * @param reqGrid
	 * @param mav
	 * @param request
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "disableState")
	public  ReturnVo<Object> disableState(HttpServletRequest request,String id) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品禁用Start......");
		try {
			Chief chief = chiefService.selectByPrimaryKey(new Integer(id));
			chief.setStatus(2);//禁用
			chiefService.updateByPrimaryKeySelective(chief);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 启用
	 */
	@ResponseBody
	@RequestMapping(value = "enableState")
	public  ReturnVo<Object> enableState(String id, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品启用Start......");
		try {
			Chief chief  = chiefService.selectByPrimaryKey(new Integer(id));
			chief.setStatus(1);//启用
			chiefService.updateByPrimaryKeySelective(chief);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
}
