package com.deyi.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.OrderComment;
import com.deyi.entity.SysUser;
import com.deyi.service.OrderCommentService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;
import com.dy.util.PropertiesUtil;

import net.sf.json.JSONObject;

/**
 * 会员信息（顾客）
 * @author zjz
 *
 */
@Controller
@RequestMapping(value ="orderComment")
public class OrderCommentController extends Component<OrderComment> {
	private Logger log = LoggerFactory.getLogger(OrderCommentController.class);

	@Autowired
	private OrderCommentService orderCommentService;
	String str="";
	

	/**
	 * 订单评论列表页面
	 * @param mav
	 * @param request
	 * @param entity
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request){
		mav.setViewName("orderComment/orderCommentList");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<OrderComment> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("订单评论列表Start......");
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		if(!"1".equals(sysUser.getType())){
			reqGrid.getObject().setStoreId(sysUser.getStoreId());
		}
		Page<OrderComment> page = new Page<OrderComment>();
		page.setPageNo(reqGrid.getPageNum());//pageNo页码，默认是第一页 //PageNum当前页等于:从第几条索引(20)除以要显示多少条(5)加1 20/5+1=5
		page.setPageSize(reqGrid.getLimit());//limit每页显示多少条
		GridVo vo = new GridVo();
		setParams(request, reqGrid.getObject(), page);
		List<OrderComment> results = orderCommentService.queryPage(page);
		
		for(OrderComment orderComment:results){
			String payNumber = orderComment.getPayNumber();
			String property = PropertiesUtil.getProperty("order_pay_no_prifix");
			if(payNumber.contains(property)){
				String replace = StringUtils.replace(payNumber, property, "");
				orderComment.setPayNumber(replace);
			}
		}
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}
	
	


	@RequestMapping(value = "toReply")
	public ModelAndView toGoodsEdit(String cid, ModelAndView mav, HttpServletRequest request) throws Exception {

		if (cid == null || "".equals(cid)) {
			throw new Exception();
		}
		
		OrderComment orderComment = orderCommentService.selectByPrimaryKey(Long.valueOf(cid));
		
		mav.addObject("orderComment", orderComment);
		mav.setViewName("orderComment/reply");
		return mav;
	}

	@RequestMapping(value = "Reply")
	public @ResponseBody ReturnVo<Object> Reply(ModelAndView mav, HttpServletRequest request, OrderComment orderComment) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			orderComment.setReplytime(new Date());
			orderCommentService.updateByPrimaryKeySelective(orderComment);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("Abnormal operation");
			vo.setSuccess(false);
		}
		return vo;
	}
	

	
	/**
	 *   删除 
	 */
	@RequestMapping(value="deleteOrderComment")
	public @ResponseBody ReturnVo<Object> deleteOrderComment(String cid,HttpServletRequest request){
		ReturnVo<Object> vo =new ReturnVo<Object>();
		try{
			orderCommentService.deleteByPrimaryKey(Long.parseLong(cid));
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
		}
		return vo;
	}
	
	/**
	 *订单评论置顶
	 */
	@RequestMapping(value = "commentTop")
	public @ResponseBody ReturnVo<Object> commentTop(ModelAndView mav,String cid,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		//获取Session中的用户信息,
		try{
				OrderComment orderComment = orderCommentService.selectByPrimaryKey(Long.parseLong(cid));
				Long replynum = orderComment.getReplynum(); //我点击的排序号20
				
				OrderComment OrderComment2 = orderCommentService.sortingTop3();
				Long replynum1 = OrderComment2.getReplynum();//页面第一个排序号//27
				
				
				if(replynum.longValue() == replynum1.longValue()){//页面点击的排序号和页面第一个排序号相等
					vo.setMessage("该评论已经置顶,请勿重复置顶");
					vo.setSuccess(false);
					vo.setStatusCode(1);
					return vo;
				}
				orderComment.setReplynum(replynum1+1);
				//更新一下数据库
				orderCommentService.updateByPrimaryKeySelective(orderComment);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}
	
	
	/**
	 *移动商户排序
	 */
	@RequestMapping(value = "upMove")
	public @ResponseBody ReturnVo<Object> upMove(ModelAndView mav,String cid,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
				OrderComment orderComment = orderCommentService.selectByPrimaryKey(Long.parseLong(cid));
				Long replynum = orderComment.getReplynum(); //我点击的排序号:42
				OrderComment orderComment1 = orderCommentService.moveOrderComment3(replynum);
				if(orderComment1 != null){
					Long replynum1 = orderComment1.getReplynum();//上一个排号 :41
					//交换一下
					orderComment.setReplynum(replynum1);
					orderComment1.setReplynum(replynum);
					//更新一下数据库
					orderCommentService.updateByPrimaryKeySelective(orderComment);
					orderCommentService.updateByPrimaryKeySelective(orderComment1);
				}else{
					vo.setStatusCode(1);
					vo.setSuccess(false);
					vo.setMessage("该评论已经是第一个了,请勿重复上移");
					return vo;
				}
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}
	
	
	


}
