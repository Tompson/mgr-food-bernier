package com.deyi.controller;
import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.deyi.entity.CarouselFigure;
import com.deyi.entity.SysCoupon;
import com.deyi.entity.SysUser;
import com.deyi.service.CarouselFigureService;
import com.deyi.service.SysCouponService;
import com.deyi.service.VoucherService;
import com.deyi.util.Component;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.vo.GridVo;
import com.deyi.vo.RequestGrid;
import com.deyi.vo.ReturnVo;

import net.sf.json.JSONObject;

/***
 * 轮播图
 * @author tom Create on 2017年05月16日 上午15:06:17 
 */
@Controller
@RequestMapping(value="carousel")
public class CarouselFigureController extends Component<CarouselFigure>{
	private Logger log = LoggerFactory.getLogger(GroupPurchaseController.class);
	@Autowired
	private CarouselFigureService carouselFigureService;
	@Autowired
	private VoucherService voucherService;
	@Autowired
	private SysCouponService sysCouponService;

	@RequestMapping(value = "list")
	public ModelAndView list(ModelAndView mav) throws Exception {
		mav.setViewName("carousel/carouselList");
		return mav;
	}

	
	@ResponseBody
	@RequestMapping(value = "page")
	public GridVo page(@RequestBody RequestGrid<CarouselFigure> reqGrid,ModelAndView mav ,HttpServletRequest request) {
		log.info("商品列表Start......");
		Page<CarouselFigure> page = new Page<CarouselFigure>();
		GridVo vo = queryPage(reqGrid, request, page);
//		if(reqGrid.getObject() == null){
//			reqGrid.setObject(new GroupPurchase());
//		}
		
		List<CarouselFigure> results = carouselFigureService.queryPage(page);
		vo.setRows(results);
		vo.setTotal(page.getTotalRecord());//总记录数
		JSONObject fromObject = JSONObject.fromObject(vo);
		return vo;
	}
	
	@RequestMapping(value = "toCarouselAdd")
	public ModelAndView toCarouselAdd(ModelAndView mav, HttpServletRequest request) {
		// 获取Session中的用户信息,
		mav.setViewName("carousel/carouselAdd");
		return mav;
	}
	/**
	 * 
	 * @Title: carouselAdd 
	 * @Description: 新增轮播图
	 * @param @param mav
	 * @param @param request
	 * @param @param carouselFigure
	 * @param @return    设定文件 
	 * @return ReturnVo<Object>    返回类型 
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "carouselAdd")
	public  ReturnVo<Object> carouselAdd(ModelAndView mav, HttpServletRequest request,CarouselFigure carouselFigure) {
		HttpSession session = request.getSession();
		SysUser sysUser = (SysUser)session.getAttribute(Constants.SESSION_USERINFO);
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			if(carouselFigure.getType().equals("1")){
				String linkNo = carouselFigure.getLinkNo();
				SysCoupon v = sysCouponService.selectByPrimaryKey(Long.parseLong(linkNo));
				if(v==null){
					vo.setMessage("平台优惠卷不存在，请重新输入");
					vo.setSuccess(false);
					return vo;
				}
			}
			carouselFigureService.insertSelective(carouselFigure);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("异常", e);
			vo.setMessage("增加失败");
			vo.setSuccess(false);
		}
		return vo;
	}
	@RequestMapping(value = "tocarouselEdit")
	public ModelAndView tocarouselEdit(String id, ModelAndView mav, HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();// 获取Session中的用户信息
		CarouselFigure carouselFigure = carouselFigureService.selectByPrimaryKey(Integer.parseInt(id));
		mav.addObject("carouselFigure", carouselFigure);
		mav.setViewName("carousel/carouselEdit");
		return mav;
	}

	@RequestMapping(value = "carouselEdit")
	public @ResponseBody ReturnVo<Object> carouselEdit(ModelAndView mav, HttpServletRequest request, CarouselFigure carouselFigure) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			if(carouselFigure.getType().equals("1")){
				String linkNo = carouselFigure.getLinkNo();
				SysCoupon v = sysCouponService.selectByPrimaryKey(Long.parseLong(linkNo));
				if(v==null){
					vo.setMessage("该代金卷不存在，请重新选择");
					vo.setSuccess(false);
					return vo;
				}
			}
			if(carouselFigure.getPhotoUrl().equals("")){
				carouselFigure.setPhotoUrl(null);
			}
			carouselFigureService.updateByPrimaryKeySelective(carouselFigure);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("修改失败");
			vo.setSuccess(false);
		}
		return vo;
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@RequestMapping(value = "deleteCarouselFigure")
	public  ReturnVo<Object> deleteCarouselFigure(String id, HttpServletRequest request ) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try {
			carouselFigureService.deleteByPrimaryKey(Integer.parseInt(id));
		} catch (Exception e) {
			log.error("异常", e);
			vo.setMessage("删除失败");
			vo.setSuccess(false);
		}
		return vo;
	}

	@ResponseBody
	@RequestMapping(value = "disableState")
	public  ReturnVo<Object> disableState(HttpServletRequest request,String id) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品禁用Start......");
		try {
			CarouselFigure carouselFigure = carouselFigureService.selectByPrimaryKey(new Integer(id));
			carouselFigure.setStatus("2");//禁用
			carouselFigureService.updateByPrimaryKeySelective(carouselFigure);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}

	/**
	 * 启用
	 */
	@ResponseBody
	@RequestMapping(value = "enableState")
	public  ReturnVo<Object> enableState(String id, HttpServletRequest request) {
		ReturnVo<Object> vo = new ReturnVo<Object>();
		log.info("商品启用Start......");
		try {
			CarouselFigure carouselFigure = carouselFigureService.selectByPrimaryKey(new Integer(id));
			carouselFigure.setStatus("2");//禁用
			carouselFigureService.updateByPrimaryKeySelective(carouselFigure);
		} catch (Exception e) {
			log.error("异常", e);
			vo.setSuccess(false);
			vo.setMessage("Abnormal operation");
			e.printStackTrace();
		}
		return vo;
	}
	
	@RequestMapping(value = "upMove")
	@ResponseBody
	public  ReturnVo<Object> upMove(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			vo = carouselFigureService.moveUp(id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	@RequestMapping(value = "downMove")
	@ResponseBody
	public  ReturnVo<Object> downMove(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			vo = carouselFigureService.moveDown(id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	
	@RequestMapping(value = "goodsTop")
	@ResponseBody
	public  ReturnVo<Object> goodsTop(String id,HttpServletRequest request){
		ReturnVo<Object> vo = new ReturnVo<Object>();
		try{
			vo = carouselFigureService.toTop(id, vo);
		}catch(Exception e){
			log.error("异常",e);
			vo.setSuccess(false);
			vo.setMessage("操作失败");
		}
		return vo;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private String GetFileDir(String... dir) {
		String dirpath = "";
		String separator = File.separator;// \
		for (int i = 0; i < dir.length; i++) {
			dirpath += separator + dir[i];
		}
		dirpath = dirpath + separator;
		return dirpath;
	}
	
	
	
	
	
	
	
	
	
	
}
