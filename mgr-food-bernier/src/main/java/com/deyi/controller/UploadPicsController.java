package com.deyi.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.deyi.util.Constants;
import com.deyi.util.DateUtils;
import com.deyi.util.ImageUtils;
import com.deyi.util.PropertiesUtil;
import com.deyi.util.UnZipOrRarUtils;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "uploadPic")
public class UploadPicsController {
	private Logger log = LoggerFactory.getLogger(UploadPicsController.class);
	/**
	 * 文件上传
	 * @Author zjz Create on 2016年11月21日 下午6:09:55
	 * @param file
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "filesUpload", method = RequestMethod.POST)
	public void filesUpload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request,
			HttpServletResponse response) {
		log.info("update->image");
//		String dir  = File.separator + "skyFood"+File.separator+"images"+ File.separator;
		String project_path = PropertiesUtil.getProperty(Constants.PROJECT_PATH);
		String upload_dir = PropertiesUtil.getProperty(Constants.UPLOAD_DIR);
//		String fileSavePath = request.getServletContext().getRealPath("/");// D:\apache-tomcat-7.0.57\webapps\yiqimanager-web
		Date date = new Date();
		String dir = GetFileDir(upload_dir,DateUtils.getYear(date)+"", DateUtils.getMonth(date)+"", DateUtils.getDay(date)+"");
		String fileSavePath = project_path + dir;

		//创建目录，如果没有的话
		File savedir = new File(fileSavePath);
		if(!savedir.exists()){
			savedir.mkdirs();
		}
		
		String originalFilename = file.getOriginalFilename();
		log.info("originalFilename:" + originalFilename);
		String filename = DateUtils.getDbDate() + DateUtils.getRandomNum();
		String suffix = originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length());
		String targerfile = fileSavePath + filename + suffix;
		File targer = new File(targerfile);
		if (!targer.getParentFile().exists()) {// 判断文件夹是否创建，没有创建则创建新文件夹
			targer.getParentFile().mkdirs();
		}
		try {
//			ImageUtils.drawMiniImage(file.getInputStream(), targerfile, 800, ImageUtils.DEFAULT_BIG_IMG_WIDTH);
			//ImageUtils.drawMiniImage(file.getInputStream(), targerfile, 400, ImageUtils.DEFAULT_BIG_IMG_WIDTH);
			
			ImageUtils.drawMiniImage(file.getInputStream(), targerfile, 0, 0);
			// file.transferTo(targer);

//			String url = "/images/" + filename + suffix;
			dir = StringUtils.replace(dir, File.separator, "/");
			String url = dir + filename + suffix;
			log.info("The upload path:" + url);
			// response.setContentType("Content-type:text/html;charset=utf-8");
			response.getWriter().write(new Gson().toJson(url));

		} catch (Exception e) {
			log.error("file[" + targerfile + "]Upload failed");
			e.printStackTrace();
		}

	}
	
	
	/**
	 * zip文件上传
	 * 
	 * @Author zjz Create on 2016年10月27日 下午7:38:12
	 * @param file
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "zipUpload", method = RequestMethod.POST)
	public void zipUpload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request,
			HttpServletResponse response) {
		log.info("zipUpload");
		
//		response.setContentType("Content-type:application/json;charset=utf-8");
		response.setCharacterEncoding("UTF-8");  
		
		boolean success = true;
		String targerfile="";//文件被保存的名字
		String message = "";
		String url="";
		List<String> unzipFileStrs = new ArrayList<String>();
		try {
			String project_path = PropertiesUtil.getProperty(Constants.PROJECT_PATH);// 工程所在路径
			String upload_dir = PropertiesUtil.getProperty(Constants.UPLOAD_DIR);// 上传文件夹名称
			String hotelId = request.getParameter("hotelId");
			String fileName01 = request.getParameter("fileName");
			fileName01 = fileName01==null?"":fileName01;
			hotelId = hotelId==null?"":hotelId;
			hotelId = com.deyi.util.StringUtils.getFixedLengthSeq(hotelId,4);
	
	
			
			String dir = GetFileDir(upload_dir, Constants.FILE_UPLOAD_PATH_HOTEL_ZIP, hotelId);
			String fileSavePath = project_path + dir;
			
			String originalFilename = file.getOriginalFilename();
			log.info("originalFilename:" + originalFilename);
			String filename = hotelId+"_"+fileName01+"_"+DateUtils.getDbDate() + DateUtils.getRandomNum();
			String suffix = originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length());
			targerfile = fileSavePath + filename + suffix;
			File targer = new File(targerfile);
		
		
		
			if (!".zip".equalsIgnoreCase(suffix)) {
	            throw new Exception("上传文件格式不支持："+suffix);
	        }
	
			// 创建目录，如果没有的话
			File savedir = new File(fileSavePath);
			if (!savedir.exists()) {
				savedir.mkdirs();
			}
	
			
			if (!targer.getParentFile().exists()) {// 判断文件夹是否创建，没有创建则创建新文件夹
				targer.getParentFile().mkdirs();
			}
		

//			ImageUtils.drawMiniImage(file.getInputStream(), targerfile, 0, 0);
			InputStream in = file.getInputStream();
			//创建压缩文件
			targer.createNewFile();
			FileOutputStream out = new FileOutputStream(targer);
			int b;
			while ((b = in.read()) != -1)
				out.write(b);
			out.close();
			in.close();
			
			
			//解压
			String unzipPath = project_path + GetFileDir(upload_dir, Constants.FILE_UPLOAD_PATH_HOTEL_PIC, hotelId, "pics");
			//过滤图片数
			Integer zipFileNum = UnZipOrRarUtils.getZipFileNum(targerfile);
			if(zipFileNum>45){
				message="压缩文件内的图片数不能大于45";
				success = false;
				response.getWriter().write(new Gson().toJson("{\"url\":\""+url+"\",\"message\":\""+message+"\",\"success\":"+success+"}"));
				return;
			}
			if(zipFileNum<=0){
				message="压缩文件内的图片数为0";
				success = false;
				response.getWriter().write(new Gson().toJson("{\"url\":\""+url+"\",\"message\":\""+message+"\",\"success\":"+success+"}"));
				return;
			}
			
			//解压压缩文件
			boolean unzip = UnZipOrRarUtils.unzip(targerfile, unzipPath, false,unzipFileStrs);
			if(!unzip){
				message="该压缩文件无法解压,格式错误";
				success = false;
				response.getWriter().write(new Gson().toJson("{\"url\":\""+url+"\",\"message\":\""+message+"\",\"success\":"+success+"}"));
				return;
			}
			
			//删除已解压的压缩包
			UnZipOrRarUtils.deleteFile(targerfile);
			dir = StringUtils.replace(dir, File.separator, "/");
			
			url = dir + filename + suffix;
			log.info("The upload path:" + url);
			
			String t ="";
			String base2 = GetFileDir(upload_dir, Constants.FILE_UPLOAD_PATH_HOTEL_PIC, hotelId, "pics");
			base2 = StringUtils.replace(base2, File.separator, "/");
			for(String s:unzipFileStrs){
				t+=base2+s+",";
			}
			if("".equals(t)){
				message = "解压失败,请检查压缩包格式";
				response.getWriter().write(new Gson().toJson("{\"url\":\""+""+"\",\"message\":\""+message+"\",\"success\":"+false+"}"));
				return;
			}
			t = t.substring(0, t.length()-1);
			url = t;
			response.getWriter().write(new Gson().toJson("{\"url\":\""+url+"\",\"message\":\""+message+"\",\"success\":"+success+"}"));

		} catch (Exception e) {
			log.error("file[" + targerfile + "]Upload failed",e);
			message = "解压失败,请检查压缩包格式";
			try {
				response.getWriter().write(new Gson().toJson("{\"url\":\""+""+"\",\"message\":\""+message+"\",\"success\":"+false+"}"));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}
	
	
	private String GetFileDir(String... dir){
		String dirpath = "";
		String separator = File.separator;// \
		for (int i = 0; i < dir.length; i++) {
			dirpath +=separator + dir[i];
		}
		dirpath = dirpath+ separator;
		return dirpath;
	}
}
