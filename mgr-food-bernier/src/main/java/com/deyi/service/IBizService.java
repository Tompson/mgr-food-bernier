package com.deyi.service;

import java.util.List;

public interface IBizService {
	/**
	 * 检查会员是否禁用
	 * @author limh Create on 2016年11月29日 上午9:45:19
	 * @return 禁用返回true 正常返回false
	 */
	public boolean checkMemberIsForbidden(String wxOpenId)throws Exception;
	/**
	 * 检查菜品是否禁用
	 * @author limh Create on 2016年11月29日 上午9:47:04
	 * @param goodsId
	 * @return 禁用返回true 正常返回false
	 */
	public boolean checkGoodsIsForbidden(Long goodsId)throws Exception;
	
	public boolean checkGoodsIsForbidden(Long goodsId,Integer storeId)throws Exception;
	/**
	 * 检查是否在营业时间内（创建订单时判断）
	 * @author limh Create on 2016年11月29日 上午10:00:07
	 * @param time
	 * @return
	 */
	public boolean checkTimeIsShopHours(String time)throws Exception;
	
	/**
	 * 检查当天时间是否超过营业时间
	 * @author limh Create on 2016年11月29日 上午11:19:47
	 * @param currentWeek
	 * @return  超过当天营业时间返回true，否则false
	 * @throws Exception
	 */
	public boolean checkNowIsOverEndTime(String currentWeek)throws Exception;
	/**
	 * 检查当前周是否是当天，并判断是否超过营业时间
	 * @author limh Create on 2016年11月29日 下午3:11:12
	 * @param week
	 * @return 是当天且超过营业时间返回true，否则返回false
	 * @throws Exception
	 */
	public boolean checkCurrentWeek(String week)throws Exception;
	
	/**
	 * 下当天订单的判断某个时间点是否是预定订单
	 * @author limh Create on 2016年11月30日 下午8:34:22
	 * @param time 时间，格式如：HH：mm
	 * @return 是预定订单返回true，不是返回false
	 * @throws Exception
	 */
	public boolean checkIsBookOrder(String time)throws Exception;
	/**
	 * 获取营业时间集合
	 * 每个元素是为HH：mm时间的字符串，以营业时间开始，每30分钟一段，最后更改为15分钟一段,且分钟固定以15，30，45，0结尾
	 * @author limh Create on 2016年12月3日 上午9:36:00
	 * @return 营业时间开始，每30分钟一段的时间集合
	 * @throws Exception
	 */
	public List<String> getShopTimeList(String week)throws Exception;
	
	/**
	 * 检查菜品是否售完
	 * @author limh Create on 2016年12月6日 下午4:10:27
	 * @param integer 
	 * @param time
	 * @return 售完返回true 否则返回false
	 * @throws Exception
	 */
	public boolean checkGoodsIsSoldOut(Long goodsId, Integer integer)throws Exception;
	public boolean checkNowIsOverEndTime1(String weeknum, Integer storeId);
	public boolean checkNowIsOverEndTime2(Integer storeId);
}
