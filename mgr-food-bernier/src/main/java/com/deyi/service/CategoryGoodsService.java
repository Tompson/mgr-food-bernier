package com.deyi.service;

import java.util.List;

import com.deyi.entity.CategoryGoods;
import com.deyi.util.Page;
import com.deyi.vo.ReturnVo;

/** 
 * @author zjz Create on 2016年10月22日 上午11:45:52 
 */
public interface CategoryGoodsService {
    int deleteByPrimaryKey(Long categoryId);

    int insert(CategoryGoods record);

    int insertSelective(CategoryGoods record);

    CategoryGoods selectByPrimaryKey(Long categoryId);

    int updateByPrimaryKeySelective(CategoryGoods record);

    int updateByPrimaryKey(CategoryGoods record);
    
    List<CategoryGoods> queryAll();

	List<CategoryGoods> queryPage(Page<CategoryGoods> page);

	List<CategoryGoods> getCategoryByCategoryName(String cgoodsName);

	ReturnVo<Object> moveUp(String id, ReturnVo<Object> vo);

	ReturnVo<Object> moveDown(String id, ReturnVo<Object> vo);

	ReturnVo<Object> toTop(String id, ReturnVo<Object> vo);

	List<CategoryGoods> wxgetCategoryLsit(Integer id);

    
	

}
