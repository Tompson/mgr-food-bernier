package com.deyi.service;

import java.util.List;

import com.deyi.entity.Member;
import com.deyi.entity.MemberAddress;
import com.deyi.util.Page;

public interface MemberAddressService {
	int deleteByPrimaryKey(Integer addressId);
	Integer deleteByPrimaryMemberId(Integer MemberId);

	int insert(MemberAddress record);

	int insertSelective(MemberAddress record);

	MemberAddress selectByPrimaryKey(Integer addressId);

	int updateByPrimaryKeySelective(MemberAddress record);

	int updateByPrimaryKey(MemberAddress record);

	/**
	 * 分页查询用户设置的地址
	 * 
	 * @author limh Create on 2016年10月25日 上午10:54:50
	 * @param page
	 *            params.memberId 会员id，必传
	 * @return List<MemberAddress>
	 */
	List<MemberAddress> selectByPage(Page<MemberAddress> page);
	/**
	 * 更新当前地址为默认地址
	 * @author limh Create on 2016年10月25日 下午6:19:28
	 * @param record 地址对象
	 * 需传addressId和status,且status="0"
	 * @param member 会员对象
	 * @return int 成功则返回1 否则返回-1
	 */
	int update2DefaultAddress(MemberAddress record,Member member);
	/**
	 * 修改地址
	 * 如传如addressId，则根据addressId做更新操作
	 * 如果没有传入addressId，则做插入操作
	 * 且若传入的status=="0"，还会调用update2DefaultAddress方法更新当前地址为默认地址
	 * @author limh Create on 2016年10月25日 下午6:21:19
	 * @param record 
	 * @param member
	 * @return int 更新成功则返回0，插入成功则返回1，否则返回-1
	 */
	int changeAddress(MemberAddress record,Member member);
	
    /**
     * 查询默认地址
     * @author limh Create on 2016年10月25日 下午6:18:41
     * @param memberId 会员id
     * @return List<MemberAddress>
     */
    List<MemberAddress> selectDefaultAddressByMemberId(Integer memberId);
}
