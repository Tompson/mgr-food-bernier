package com.deyi.service;

import java.util.List;

import com.deyi.entity.ChiefComment;
import com.deyi.util.Page;


public interface ChiefCommentService {
	int deleteByPrimaryKey(Long cid);

	int insert(ChiefComment record);

	int insertSelective(ChiefComment record);

	ChiefComment selectByPrimaryKey(Long cid);

	int updateByPrimaryKeySelective(ChiefComment record);

	int updateByPrimaryKey(ChiefComment record);
	  /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<ChiefComment> queryPage(Page<ChiefComment> page);
	/**
	 *  配送员评论分页
	 * @Author zjz Create on 2016年11月24日 下午9:08:36
	 * @param page
	 * @return
	 */
	List<ChiefComment> queryPageByDeliver(Page<ChiefComment> page);
	
	
	//取到排序是第一个的订单评论
	ChiefComment sortingTop();
	//当前点击的订单评论的:上一个订单评论
	ChiefComment moveChiefComment(Long parentId);
	//倒叙排取到排序是第一个的订单评论
	ChiefComment sortingTop2();
	//当前点击的订单评论的:上一个订单评论
	ChiefComment moveChiefComment2(Long parentId);
	//取到排序是第一个的订单评论
	ChiefComment sortingTop3();
	//当前点击的订单评论的:上一个订单评论
	ChiefComment moveChiefComment3(Long parentId);
	
	/**
	 * 检查数据库是否存在该订单的评论
	 * @author limh Create on 2016年11月17日 上午11:45:12
	 * @param orderId
	 * @return 该订单的评论统计 如果小于1，则可执行插入操作
	 */
	int checkChiefCommentIsUniqueness(Long orderId);
	
	
	
}
