package com.deyi.service;

import java.util.List;

import com.deyi.entity.WxAccessToken;
import com.deyi.model.AccessToken;
public interface WxAccessTokenService {
	int deleteByPrimaryKey(String id);

	int insert(WxAccessToken record);

	int insertSelective(WxAccessToken record);

	WxAccessToken selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(WxAccessToken record);

	int updateByPrimaryKey(WxAccessToken record);

	List<WxAccessToken> selectAll();
	
	AccessToken getAccessToken();
}
