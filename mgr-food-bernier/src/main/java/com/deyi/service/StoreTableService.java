package com.deyi.service;

import java.util.List;

import com.deyi.entity.StoreTable;
import com.deyi.util.Page;

public interface StoreTableService {

	List<StoreTable> queryPage(Page<StoreTable> page);


	Integer insertSelectiveReturnId(StoreTable storeTable);


	void deleteByPrimaryKey(int parseInt);


	StoreTable selectByPrimaryKey(int parseInt);


	void updateByPrimaryKeySelective(StoreTable storeTable);

}
