package com.deyi.service;

import java.util.List;

import com.deyi.entity.MemberIntegral;
import com.deyi.entity.MemberVoucher;
import com.deyi.model.ResultVo;
import com.deyi.model.VoucherQueryCondition;
import com.deyi.model.vo.MemberVoucherVo;
import com.deyi.util.Page;

public interface MemberVoucherService {
    int deleteByPrimaryKey(Long id);
    Integer deleteByMemberId(Long memberId);

	int insert(MemberVoucher record);

	int insertSelective(MemberVoucher record);

	MemberVoucher selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(MemberVoucher record);

	int updateByPrimaryKey(MemberVoucher record);
	/**
	 * 分页查询指定会员未过期且未使用的优惠券
	 * @author limh Create on 2016年11月15日 上午10:18:05
	 * @param page
	 * params.memberId 会员ID,必传
	 * @return List<MemberVoucherVo> 会员代金券集合
	 */
	List<MemberVoucherVo> selectByPage(Page<MemberVoucherVo> page);
	
	/**
	 * 查询指定会员满足条件的所有代金券
	 * @author limh Create on 2016年11月15日 下午7:01:51
	 * @param record 
	 * memberId 会员ID
	 * amountPrice 订单总价
	 * @return List<MemberVoucherVo> 优惠券集合
	 */
	List<MemberVoucherVo> selectSatisfactoryVoucher(VoucherQueryCondition record);
	
	/**
     * 查询当天分享获得代金券
     * @author limh Create on 2016年11月3日 下午9:39:55
     * @param memberId 会员id
     * @return List<MemberIntegral>
     */
    List<MemberIntegral> selectCurrentDayShareByMemberId(Integer memberId);
	List<MemberVoucher> selectByMemberIdAndCouponId(Integer memberId, Long id);

	/**
	 * 
	 * @Title: selectByPageExtra 
	 * @Description: 分页查询所有的代金券
	 * @param page
	 * params.memberId 会员ID,必传
	 * @return
	 * List<MemberVoucherVo>
	 * @throws
	 */
	List<MemberVoucherVo> selectByPageExtra(Page<MemberVoucherVo> page);
	
	MemberVoucherVo selectVoucherByKey(MemberVoucher record);
	void deleteByMemberIdAndvoucherId(Integer memberId, Integer parseInt);
	ResultVo<Object> gainVoucher(Integer memberId, MemberVoucher memberVoucher);

}
