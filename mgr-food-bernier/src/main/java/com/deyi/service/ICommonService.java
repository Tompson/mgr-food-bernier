package com.deyi.service;

/** 
 * @author zjz Create on 2016年11月25日 上午10:44:41 
 */
public interface ICommonService<T> {
	Integer deleteByPrimaryKey(Long id) throws Exception;//根据主键删除数据(删)

    Integer insertSelective(T record) throws Exception;//动态插入数据有if判断(增)
    

    T selectByPrimaryKey(Long id) throws Exception;//根据主键(查)

    Integer updateByPrimaryKeySelective(T record) throws Exception;//动态改数据有if判断(改)
    

}
