package com.deyi.service;

import java.util.List;

import com.deyi.entity.GroupPurchase;
import com.deyi.util.Page;

/** 
 * @author zjz Create on 2016年11月28日 上午10:36:08 
 */
public interface GroupPurchaseService extends ICommonService<GroupPurchase>{
    /**
     * 分页,并且可以根据会员名称和联系人模糊查询
     * @Author zjz Create on 2016年11月28日 上午11:17:55
     * @param page
     * @return
     */
    List<GroupPurchase> queryPage(Page<GroupPurchase> page);
}
