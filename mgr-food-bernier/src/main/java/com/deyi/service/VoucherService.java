package com.deyi.service;

import java.util.List;

import com.deyi.entity.CategoryGoods;
import com.deyi.entity.Voucher;
import com.deyi.util.Page;
/**
 * 
 * @author zjz Create on 2016年11月12日 下午3:06:25
 */
public interface VoucherService {
	    int deleteByPrimaryKey(Long id);

	    int insert(Voucher record);

	    int insertSelective(Voucher record);

	    Voucher selectByPrimaryKey(Long id);

	    int updateByPrimaryKeySelective(Voucher record);

	    int updateByPrimaryKey(Voucher record);
	    
	    List<Voucher> queryExchangePage(Page<Voucher> page);//积分兑换代金券分页
	    List<Voucher> queryBookPage(Page<Voucher> page);//预定返代金券分页 
	    List<Voucher> queryDrawPage(Page<Voucher> page);//评论，分享抽奖代金券分页
	    
	    /**
	     * 查询设置好的所有参加抽奖的代金券
	     * @author limh Create on 2016年11月15日 下午9:34:49
	     * @return
	     */
	    List<Voucher> selectDrawVoucher();
}
