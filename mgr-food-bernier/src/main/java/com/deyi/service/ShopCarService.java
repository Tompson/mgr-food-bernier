package com.deyi.service;

import java.util.List;

import com.deyi.entity.ShopCar;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;

public interface ShopCarService {
	int deleteByPrimaryKey(Long id);

    int insert(ShopCar record);

    int insertSelective(ShopCar record);

    ShopCar selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopCar record);

    int updateByPrimaryKey(ShopCar record);
    
    /**
     * 
     * @Title: statPriceAndNumByMemberIdTableId 
     * @Description: 统计堂食购物车
     * @param memberId 会员id
     * @param tableId 桌子id
     * @return
     * ShopCatVo
     * @throws
     */
    ShopCatVo statPriceAndNumByMemberIdTableId(Long memberId,Long tableId);
    
    /**
     * 
     * @Title: selectByMemberIdTableId 
     * @Description: 查询购物车列表（堂食）
     * @param memberId
     * @param tableId
     * @return
     * ShopTrolleyVo
     * @throws
     */
    List<ShopTrolleyVo> selectByMemberIdTableId(Long memberId,Long tableId);
    
    /**
     * 
     * @Title: deleteByMemberIdStoreId 
     * @Description: 清空门店下的购物车
     * @param memberId 
     * @param storeId
     * @return
     * int
     * @throws
     */
    int deleteByMemberIdStoreId(Long memberId,Long storeId);

	int checkIsSelected(Integer memberId, Integer tableId, Long storeGoodsId);

	ShopCar selectShopCarByMemberIdAndTableIdAndStoreGoodsId(Integer memberId, Integer tableId, Long storeGoodsId);

	ShopTrolleyVo selectByMemberIdStoreGoodsId(long longValue, Long sgid);
}
