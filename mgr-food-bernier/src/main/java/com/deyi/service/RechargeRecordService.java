package com.deyi.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.deyi.entity.RechargeRecord;
import com.deyi.entity.SysOrder;

public interface RechargeRecordService {
	/**
	 * 
	 * @Title: saveRechargeRecordAli 
	 * @Description: 支付宝支付回调业务处理
	 * @param request
	 * @return
	 * String
	 * @throws
	 */
	String saveRechargeRecordAli(HttpServletRequest request);
	
	/**
	 * 
	 * @Title: saveRechargeRecordWx 
	 * @Description: 微信支付回调业务处理
	 * @param request
	 * @return
	 * String
	 * @throws
	 */
	String saveRechargeRecordWx(HttpServletRequest request);
	
	/**
	 * 
	 * @Title: statOrderNumber 
	 * @Description: 充值金额
	 * @param @param storeId
	 * @param @return    设定文件 
	 * @return int    返回类型 
	 * @throws
	 */
	List<RechargeRecord> statRechargeNumber(Date querystarttime,Date queryendtime);
	
}
