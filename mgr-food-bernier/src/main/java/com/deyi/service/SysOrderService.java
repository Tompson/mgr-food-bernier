package com.deyi.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.deyi.entity.DeliverMan;
import com.deyi.entity.Member;
import com.deyi.entity.OrderComment;
import com.deyi.entity.SysOrder;
import com.deyi.model.vo.SysOrderVo;
import com.deyi.util.Page;

public interface SysOrderService {

	int deleteByPrimaryKey(Long orderId);

	int insert(SysOrder record);

	int insertSelective(SysOrder record);

	SysOrder selectByPrimaryKey(Long orderId);

	int updateByPrimaryKeySelective(SysOrder record);

	int updateByPrimaryKey(SysOrder record);

	/**
	 * 分页查询订单以及订单所包含的菜品
	 * 
	 * @param page
	 *            params.orderId 订单Id,非空则按ID查询
	 * @return
	 */
	Page<SysOrderVo> selectByPage(Page<SysOrderVo> page);

	/**
	 * 创建订单生成订单详情，同时删除购物车
	 * 
	 * @param order
	 *            提交已确认的的订单
	 * @param memberId
	 *            会员Id
	 * @param bookTime
	 *            预定时间
	 * @param voucherId 优惠券id
	 * @return SysOrder 包含插入的订单Id
	 */
	SysOrder createOrderAndDetail(SysOrder order, Integer memberId, String bookTime,Long voucherId,String weeknum);
	/**
	 * 
	 * @author limh Create on 2016年12月1日 下午2:32:56
	 * @param order 提交已确认的的订单
	 * @param memberId 会员Id
	 * @param bookTime  预定时间
	 * @param goodsId 菜品id 
	 * @param amount 数量
	 * @param voucherId 优惠券id
	 * @param weeknum 
	 * @return 
	 */
	SysOrder createSingleOrderAndDetail(SysOrder order, Integer memberId, String bookTime,Long goodsId,Integer amount,Long voucherId, String weeknum);
	
	/**
	 * 分页
	 * 
	 * @Author zjz Create on 2016年10月21日 下午5:34:00
	 * @param page
	 * @return
	 */
	List<SysOrder> queryPage(Page<SysOrder> page);

	/**
	 * 更改订单状态为(已确认)
	 * 
	 * @Author zjz Create on 2016年10月25日 下午7:54:55
	 * @param sysOrder
	 *            只需传个orderId
	 * @return
	 */
	int updateOrderStatus2(SysOrder sysOrder);

	/**
	 * 根据订单id查询订单信息 返回 SysOrder对象
	 * 
	 * @Author zjz Create on 2016年10月25日 下午7:56:55
	 * @param orderId
	 * @return
	 */
	SysOrder queryOrderById2(Long orderId);

	/**
	 * 确认订单: 更改订单状态为 已确认订单
	 * 
	 * @Author zjz Create on 2016年10月25日 下午7:56:55
	 * @param orderId
	 * @return 是否确认成功
	 */
	String confirmOrder(Long orderId);

	/**
	 * 根据订单号查询订单
	 * 
	 * @author limh Create on 2016年10月26日 下午8:29:42
	 * @param payno 订单号
	 */
	SysOrder selectByPayNumber(String payno);
	 
		
	/**
	 * 统计交易笔数
	 * @Author zjz Create on 2016年10月26日 下午2:13:09
	 * @return
	 */
	int statDealNum(Long storeId);
	/**
	 * 交易总额
	 */
	BigDecimal statDealSumAmount(Long storeId);
	/**
	 * 昨日交易笔数
	 */
	int statDealNumYesterday(Long storeId);
	/**
	 * 昨日交易金额
	 * @Author zjz Create on 2016年10月26日 下午2:19:18
	 * @return
	 */
	BigDecimal statDealSumAmountYesterday(Long storeId);
	/**
	 * 支付宝回调处理
	 * 对订单状态的修改
	 * @author limh Create on 2016年10月29日 上午10:37:23
	 * @param request
	 * @return
	 */
	String notifyAliJs(HttpServletRequest request);
	/**
	 * 确认收货
	 * 订单状态修改,消费积分累计
	 * @author limh Create on 2016年10月29日 上午10:47:08
	 * @param orderId 订单id 
	 * @param memberId 会员id 
	 * @return 成功返回1 失败返回-1
	 */
	int sureReceive(Long orderId,Member member);
	/**
	 * 保存评论
	 * 评论积分累计
	 * @author limh Create on 2016年10月29日 上午10:57:14
	 * @param orderId
	 * @param memberId
	 * @return 成功返回1 失败返回-1
	 */
	int saveOrderComment(OrderComment orderComment,Member member);
	
	/**
	 * 统计订单状态为未处理 
	 * @Author zjz Create on 2016年11月24日 上午10:09:24
	 * @return int类型,没有就返回0
	 */
	int statOrderStatusEqUntreated();
	
	
	/**
	 * 分页+商户统计
	 * @Author zjz Create on 2016年10月25日 上午11:32:54
	 * @param page
	 * @return
	 */
	List<SysOrder> storeStat(Page<SysOrder> page);

	SysOrder createOrderAndDetail1(SysOrder order, Integer memberId, String bookTime, Long voucherId);
	
	/**
	 * 
	 * @Title: statOrderNumber 
	 * @Description: 统计订单数
	 * @param @param storeId
	 * @param @return    设定文件 
	 * @return int    返回类型 
	 * @throws
	 */
	List<SysOrder> statOrderNumber(Date querystarttime,Date queryendtime,Long storeId);

	SysOrder createSingleOrderAndDetail1(SysOrder order, Integer memberId, String bookTime, Long goodsId,
			Integer amount, Long voucherId);
}
