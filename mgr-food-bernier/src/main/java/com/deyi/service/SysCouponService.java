package com.deyi.service;

import java.util.Date;
import java.util.List;

import com.deyi.entity.Member;
import com.deyi.entity.SysCoupon;
import com.deyi.util.Page;
/**
 * 
 * @ClassName: SysCouponService 
 * @Description: 平台代金卷业务类
 * @author GZ 
 * @date 2017年5月22日 下午5:17:16 
 *
 */
public interface SysCouponService {
	    int deleteByPrimaryKey(Long id);

	    int insert(SysCoupon record);

	    int insertSelective(SysCoupon record);

	    SysCoupon selectByPrimaryKey(Long id);

	    int updateByPrimaryKeySelective(SysCoupon record);

	    int updateByPrimaryKey(SysCoupon record);
	    
	    List<SysCoupon> querySysCouponPage(Page<SysCoupon> page);//积分兑换代金券分页

		void wxgetOneCoupon(Member member,SysCoupon coupon);
		int startUpdateSysCouponTime(Date time);
}
