package com.deyi.service;

import javax.servlet.http.HttpServletRequest;

/** 
 * @author zjz Create on 2016年11月10日 下午12:17:12 
 */
public interface IWxPayService {//微信支付接口
	
	public String handlePayNotify(HttpServletRequest httpRequest);

}
