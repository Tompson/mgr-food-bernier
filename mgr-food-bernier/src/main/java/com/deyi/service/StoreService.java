package com.deyi.service;

import java.util.List;

import com.deyi.entity.Store;
import com.deyi.entity.StoreCategory;
import com.deyi.entity.StoreGoods;
import com.deyi.entity.SysUser;
import com.deyi.util.Page;

public interface StoreService {

	List<Store> queryPage(Page<Store> page);

	Store selectByPrimaryKey(Integer integer);

	void updateByPrimaryKeySelective(Store store,int flag);

	Store getStoreByAccount(String account);

	void sava(Store store);

	void deleteStoreById(int parseInt);

	List<String> getGoodsIdListByStoreId(Long parseInt, Long cId);

	List<String> getCategoryListByStoreId(int parseInt);

	void deleteStoreCategorysByStoreId(String id);

	void deleteStoreGoodsByStoreId(String id);

	void insertStoreCategory(StoreCategory sg);

	void insertStoreGoods(StoreGoods sg);

	List<Store> queryAllStore();
	void savaStore(Store store,SysUser sysUser);

	List<Store> selectStoresByUserLocation(double parseDouble, double parseDouble2);

	List<Store> wxgetStoresList(Page<Store> page);

}
