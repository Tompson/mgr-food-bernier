package com.deyi.service;

import java.util.List;

import com.deyi.entity.Part;
import com.deyi.util.Page;

public interface PartService {

	List<Part> queryPage(Page<Part> page);

	void insertSelective(Part part);

	Part selectByPrimaryKey(int parseInt);

	void updateByPrimaryKeySelective(Part part);

	void deleteByPrimaryKey(int parseInt);

	List<Part> wxgetPartsByStoreId(Integer id);

	List<Part> wxgetFullPartsByStoreId(Integer id);

	List<Part> wxgetDiscountPartsByStoreId(Integer id);

}
