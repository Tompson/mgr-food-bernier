package com.deyi.service;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.deyi.entity.Member;
import com.deyi.model.ResultVo;
import com.deyi.util.Page;

public interface MemberService {
    int deleteByPrimaryKey(Integer memberId);
    /**
     * 自己写的删除方法(区别公共的)
     * @Author zjz Create on 2016年11月9日 上午11:51:07
     * @param accountDetails
     * @return
     */
    Integer deleteByPrimaryKey2(Integer memberId) throws Exception;
    int insert(Member record);

    int insertSelective(Member record);

    Member selectByPrimaryKey(Integer memberId);

    int updateByPrimaryKeySelective(Member record);

    int updateByPrimaryKey(Member record);
    
    Member selectByWxOpenId(String wxOpenId);
    
    Member insertAndReturn(Member entity);
    
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<Member> queryPage(Page<Member> page);
	/**
	 * 获取分享奖励，默认增加设定的积分
	 * @author limh Create on 2016年11月15日 上午10:49:55
	 * @param member
	 * @return 增加的积分
	 */
	int getShareAward(Member member);
	/**
	 * 抽奖，获取代金券
	 * @author limh Create on 2016年11月16日 上午11:47:54
	 * @param member
	 * @param category
	 * @return 代金券面额
	 * @throws NullPointerException
	 * @throws SQLException
	 */
	double lotteryDraw(Member member,String category)throws NullPointerException,SQLException;
	/**
	 * 兑换代金券
	 * @author limh Create on 2016年11月16日 上午11:53:56
	 * @param member
	 * @param voucherId
	 * @return -1或剩余积分  -1表示积分不够
	 * @throws NullPointerException
	 * @throws SQLException
	 */
	Integer exchangeVoucher(Member member,Long voucherId) throws NullPointerException,SQLException;
	ResultVo<Object> creditpay(Member member, String storeId, String payno,HttpServletRequest httpRequest);
	void updateMemberMoney(Member member);
	/**
	 * 
	 * @Title: upgradeMember 
	 * @Description: 会员升级
	 * @param @param memberId    会员id
	 * @param @param integral    会员当前积分 
	 * @param @param memberLevelId 当前会员等级
	 * @return void    返回类型 
	 * @throws
	 */
	void upgradeMember(Integer memberId,Integer memberLevelId,Integer integral);
	
}
