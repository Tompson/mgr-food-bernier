package com.deyi.service;

import java.util.List;

import com.deyi.entity.DeliverMan;
import com.deyi.util.Page;

/** 
 * @author zjz Create on 2016年10月24日 上午8:57:20 
 */
public interface DeliverManService {
	int deleteByPrimaryKey(Integer deliverManId);

    int insert(DeliverMan record);

    int insertSelective(DeliverMan record);

    DeliverMan selectByPrimaryKey(Integer deliverManId);

    int updateByPrimaryKeySelective(DeliverMan record);

    int updateByPrimaryKey(DeliverMan record);
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<DeliverMan> queryPage(Page<DeliverMan> page);
	
	/**
	 * 分页+配送统计
	 * @Author zjz Create on 2016年10月25日 上午11:32:54
	 * @param page
	 * @return
	 */
	List<DeliverMan> deliverStat(Page<DeliverMan> page);

}
