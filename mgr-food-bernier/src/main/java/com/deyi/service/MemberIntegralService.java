package com.deyi.service;

import java.util.List;

import com.deyi.entity.Member;
import com.deyi.entity.MemberIntegral;

public interface MemberIntegralService {
	int deleteByPrimaryKey(Integer id);
	Integer deleteByMemberId(Integer id);

    int insert(MemberIntegral record);

    int insertSelective(MemberIntegral record);

    MemberIntegral selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MemberIntegral record);

    int updateByPrimaryKey(MemberIntegral record);
    
    /**
     * 查询最新的积分
     * @author limh Create on 2016年10月29日 下午12:12:56
     * @param memberId
     * @return
     */
    MemberIntegral selectByMemberId(Integer memberId);
    /**
     * 积分累计更新,做插入操作
     * 1.消费累计
     * 2.评论累计
     * 3.分享累计
     * memberId 会员id 
     * integral 增加的积分
     * @author limh Create on 2016年10月29日 上午10:29:38
     * @param memberId
     * @param integral
     * @param type 积分计算时的类型
     * @return
     */
    int updateIntegral(Member member,Integer integral,String type);
    /**
     * 查询当天分享的积分
     * @author limh Create on 2016年11月3日 下午9:39:55
     * @param memberId 会员id
     * @return List<MemberIntegral>
     */
    List<MemberIntegral> selectCurrentDayShareByMemberId(Integer memberId);
}
