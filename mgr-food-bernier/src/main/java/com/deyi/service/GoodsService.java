package com.deyi.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.deyi.entity.Goods;
import com.deyi.entity.StoreGoods;
import com.deyi.model.vo.GoodsVo;
import com.deyi.util.Page;
import com.deyi.vo.ReturnVo;

/** 
 * @author zjz Create on 2016年10月21日 下午5:46:06 
 */
public interface GoodsService {
    int deleteByPrimaryKey(Long id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Long id);
    
    

    int updateByPrimaryKeySelective(Goods record);
    
    int updateByPrimaryByStoreIdAndGoodsId(Integer storeId,Long id,String status);

    String selectByPrimaryByStoreIdAndGoodsId(Integer storeId,Long id);
    
    String selectByPrimaryByStoreIdAndGoodsIdSort(Integer storeId,Long id);
    
    int updateByPrimaryKey(Goods record);
    /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page 
     * @return 
     */
	List<Goods> queryPage(Page<Goods> page);
	
	/**
	 * 关系分类表，查出带分类信息的菜品
	 * @author limh Create on 2016年10月23日 下午6:28:37
	 * @param id 菜品ID
	 * @return Goods
	 */
	Goods selectGoodsDetailByPrimaryKey(Long id);
	
	/**
	 * 分页查询菜品
	 * 菜品分类
	 * 统计菜品平均评分
	 * @author limh Create on 2016年10月24日 上午11:36:30
	 * @param page 
	 * params.goodsName  非空则按菜品名模糊查询
	 * params.weeksFoodStatus 非空则按星期数查询
	 * params.categoryId  非空则按分类id查询
	 * params.goodsId 非空则按菜品id查询
	 * params.status  非空则按菜品状态查询
	 * @return List<GoodsVo> 
	 */
	List<GoodsVo> selectAvgScore4Goods(Page<GoodsVo> page);
	
	/**
	 * 交易商品总数量
	 * @Author zjz Create on 2016年10月26日 下午3:31:27
	 * @return
	 */
	Integer StatDealGoodsSum(Long storeId);
	
	/**
	 * 分页 菜品销售统计
	 * @Author zjz Create on 2016年10月26日 下午7:58:06
	 * @param page
	 * @return
	 */
	List<Goods>  queryPageStatGoodsAvgComment(Page<Goods> page);
	/**
	 * 根据菜id查询菜的评价(对订单的评价就是对订单里的菜的评价)
	 * @Author zjz Create on 2016年11月18日 下午1:45:19
	 */
	Goods selectGoodsCommentBygoodsId(Long goodsId);
    /**
     * 
     * @Title: toTop 
     * @Description: 置顶功能
     * @param id
     * @param vo
     * @return
     * ReturnVo<Object>
     * @throws
     */
	ReturnVo<Object> toTop(Integer storeId,String id,ReturnVo<Object> vo);
	/**
	 * 
	 * @Title: moveUp 
	 * @Description: 上移
	 * @param id
	 * @param vo
	 * @return
	 * ReturnVo<Object>
	 * @throws
	 */
	ReturnVo<Object> moveUp(Integer storeId,String id,ReturnVo<Object> vo);
	
	/**
	 * 
	 * @Title: getMaxSort 
	 * @Description: 获取最大排序号，用于新增菜品是插入使用
	 * @return
	 * long
	 * @throws
	 */
	long getMaxSort();

	List<Goods> queryAllByCategoryId(Long cId);

	List<Goods> wxgetGoodListByStoreIdAndCategoryId(Integer id, Long categoryId);

	StoreGoods getStoreGoodsByGoodsIdAndStoreId(Long goodsId, Integer storeId);
	long getMaxSortByStore(Integer storeId);
	/**
	 * 
	 * @Title: queryPageByStore 
	 * @Description: 门店菜品分页
	 * @param @param page
	 * @param @return    设定文件 
	 * @return List<Goods>    返回类型 
	 * @throws
	 */
	List<Goods> queryPageByStore(Page<Goods> page);
	
	
	
	
	/**
	 * 
	 * @Title: selectGoodsDetailBySgId 
	 * @Description: 查询门店菜品详情
	 * @param sgid
	 * @return
	 * Goods
	 * @throws
	 */
	Goods selectGoodsDetailBySgId(Long sgid);
	/**
	 * 
	 * @Title: selectByPrimaryCategoryId 
	 * @Description: 根据分类id查询商品信息
	 * @param @param id
	 * @param @return    设定文件 
	 * @return Goods    返回类型 
	 * @throws
	 */
	List<Goods> selectByPrimaryCategoryId(Long id);
}
