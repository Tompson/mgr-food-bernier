package com.deyi.service;

import java.util.List;

import com.deyi.entity.CarouselFigure;
import com.deyi.util.Page;
import com.deyi.vo.ReturnVo;

public interface CarouselFigureService {

	List<CarouselFigure> queryPage(Page<CarouselFigure> page);

	void insertSelective(CarouselFigure carouselFigure);

	CarouselFigure selectByPrimaryKey(int parseInt);

	void updateByPrimaryKeySelective(CarouselFigure carouselFigure);

	void deleteByPrimaryKey(int parseInt);

	ReturnVo<Object> moveUp(String id, ReturnVo<Object> vo);

	ReturnVo<Object> moveDown(String id, ReturnVo<Object> vo);

	ReturnVo<Object> toTop(String id, ReturnVo<Object> vo);

	List<CarouselFigure> selectTopFiveCarouselFigure();

}
