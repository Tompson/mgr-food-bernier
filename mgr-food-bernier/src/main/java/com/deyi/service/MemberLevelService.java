package com.deyi.service;

import java.util.List;

import com.deyi.entity.MemberLevel;
import com.deyi.util.Page;

public interface MemberLevelService {

	MemberLevel selectByPrimaryKey(int parseInt);
	
	MemberLevel selectByPrimaryName(String name);

	List<MemberLevel> queryPage(Page<MemberLevel> page);
	
	List<MemberLevel> queryAll();

	void updateByPrimaryKeySelective(MemberLevel memberLevel);
	
	MemberLevel querryByPrimaryKeymemberId(String memberId);
	/**
	 * 
	 * @Title: querryByPrimaryKeymemberId 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param @param memberId
	 * @param @return    设定文件 
	 * @return MemberLevel    返回类型 
	 * @throws
	 */
	MemberLevel querryByNextLevel(Integer memberLevelId);

}
