package com.deyi.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.deyi.entity.SysUser;
import com.deyi.util.Page;

/** 
 * @author zjz Create on 2016年10月23日 上午11:02:38 
 */
public interface SysUserService {
	 	int deleteByPrimaryKey(Integer uid);

	    int insert(SysUser record);

	    int insertSelective(SysUser record);

	    SysUser selectByPrimaryKey(Integer uid);

	    int updateByPrimaryKeySelective(SysUser record);

	    int updateByPrimaryKey(SysUser record);
	    
	    /**
	     * 分页
	     * @Author zjz Create on 2016年10月21日 下午5:34:00
	     * @param page
	     * @return
	     */
		List<SysUser> queryPage(Page<SysUser> page);
		
		/**
		 * 用户名/和密码查询(用于登录)
		 * @param loginName
		 * @param passwd
		 * @return
		 */
		SysUser checkUser(@Param("loginName")String loginName, @Param("passwd")String passwd);
		
		/**
		 * 根据后台用户账号查,返回的如果是空则继续添加后台用户账号,如果不为空,说明有后台有用户账号了,就提示用户:后台用户账号重复
		 * @Author zjz Create on 2016年10月28日 下午3:56:45
		 * @param userAccount
		 * @return
		 */
		List<SysUser> checkAccountName(SysUser sysUser);

}
