package com.deyi.service;

import java.util.List;

import com.deyi.entity.Menu;
import com.deyi.entity.UserMenu;

public interface MenuService {

	List<Menu> getAllMenus();

	List<Menu> getMenusByUser(Integer integer);

	/**
	 * 根据menuId 
	 */
    Menu getMenuById(String id);

	List<String> getMenuIdsByUserId(String uid);

	void deleteUserMenuByUserId(String id);

	void insertSelective(UserMenu um);
}
