package com.deyi.service;

import java.sql.SQLException;
import java.util.List;

import com.deyi.entity.OrderComment;
import com.deyi.entity.SysOrder;
import com.deyi.model.vo.OrderCommentVo;
import com.deyi.util.Page;


public interface OrderCommentService {
	int deleteByPrimaryKey(Long cid);

	int insert(OrderComment record);

	int insertSelective(OrderComment record);

	OrderComment selectByPrimaryKey(Long cid);

	int updateByPrimaryKeySelective(OrderComment record);

	int updateByPrimaryKey(OrderComment record);
	/**
	 * 保存评论并更新订单状态为已评价
	 * @author limh Create on 2016年11月17日 下午8:15:56
	 * @param order 订单对象
	 * @param orderComment 页面提交的评论内容
	 * @return -1或者1，-1失败，1成功
	 * @throws NullPointerException
	 * @throws SQLException
	 */
	int  saveOrderCommentAndUpdateOrderStatus(SysOrder order,OrderComment orderComment)throws NullPointerException,SQLException;
	/**
	 * 查询跟某个菜有关联的订单的所有评论
	 * @author limh Create on 2016年10月24日 下午3:09:18
	 * @param page
	 * params.goodsId 菜品id，必传
	 * @return List<OrderComment>
	 */
	List<OrderCommentVo> selectGoodsCommemtByGoodsId(Page<OrderCommentVo> page);
	
	  /**
     * 分页
     * @Author zjz Create on 2016年10月21日 下午5:34:00
     * @param page
     * @return
     */
	List<OrderComment> queryPage(Page<OrderComment> page);
	/**
	 *  配送员评论分页
	 * @Author zjz Create on 2016年11月24日 下午9:08:36
	 * @param page
	 * @return
	 */
	List<OrderComment> queryPageByDeliver(Page<OrderComment> page);
	
	
	//取到排序是第一个的订单评论
	OrderComment sortingTop();
	//当前点击的订单评论的:上一个订单评论
	OrderComment moveOrderComment(Long parentId);
	//倒叙排取到排序是第一个的订单评论
	OrderComment sortingTop2();
	//当前点击的订单评论的:上一个订单评论
	OrderComment moveOrderComment2(Long parentId);
	//取到排序是第一个的订单评论
	OrderComment sortingTop3();
	//当前点击的订单评论的:上一个订单评论
	OrderComment moveOrderComment3(Long parentId);
	
	/**
	 * 检查数据库是否存在该订单的评论
	 * @author limh Create on 2016年11月17日 上午11:45:12
	 * @param orderId
	 * @return 该订单的评论统计 如果小于1，则可执行插入操作
	 */
	int checkOrderCommentIsUniqueness(Long orderId);
	
	
	
}
