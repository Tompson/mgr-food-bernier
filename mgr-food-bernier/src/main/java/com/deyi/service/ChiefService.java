package com.deyi.service;

import java.util.List;

import com.deyi.entity.Chief;
import com.deyi.util.Page;

public interface ChiefService {

	List<Chief> queryPage(Page<Chief> page);

	int insertChief(Chief chief);

	Chief selectByPrimaryKey(int parseInt);

	void updateByPrimaryKeySelective(Chief chief);

	void deleteByPrimaryKey(int parseInt);
	//根据门店id查询厨师列表
	List<Chief> queryAllByStoreId(Integer storeId);
}
