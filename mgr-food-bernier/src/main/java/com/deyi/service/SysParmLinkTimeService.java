package com.deyi.service;

import java.util.List;

import com.deyi.entity.SysParmLinkTime;

/**
 * 
 * @author zjz Create on 2016年11月11日 下午2:36:34
 */
public interface SysParmLinkTimeService {
	    int deleteByPrimaryKey(Long id);

	    int insert(SysParmLinkTime record);

	    int insertSelective(SysParmLinkTime record);

	    SysParmLinkTime selectByPrimaryKey(Long id);

	    int updateByPrimaryKeySelective(SysParmLinkTime record);

	    int updateByPrimaryKey(SysParmLinkTime record);
	    
	    List<SysParmLinkTime> selectAll();
}
