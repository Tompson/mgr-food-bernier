package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.stat.TableStat.Condition;
import com.deyi.dao.ShopTrolleyMapper;
import com.deyi.entity.ShopTrolley;
import com.deyi.model.ConditionModel;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.model.vo.WeeknumStatistics;
import com.deyi.service.ShopTrolleyService;

@Service
@Transactional
public class ShopTrolleyServiceImpl implements ShopTrolleyService {
	@Autowired
	private ShopTrolleyMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ShopTrolley record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(ShopTrolley record) {
		return mapper.insertSelective(record);
	}

	@Override
	public ShopTrolley selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ShopTrolley record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ShopTrolley record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<ShopTrolleyVo> selectByMemberId(Integer memberId) {
		return mapper.selectByMemberId(memberId);
	}

	@Override
	public double amountPriceByMemberId(Integer memberId) {
		return mapper.amountPriceByMemberId(memberId);
	}

	@Override
	public int deleteByMemberId(Integer memberId) {
		return mapper.deleteByMemberId(memberId);
	}

	@Override
	public ShopTrolley checkGoodsIsExist(ShopTrolley record) {
		return mapper.checkGoodsIsExist(record);
	}

	@Override
	public double amountPriceByWeeknum(ConditionModel condition) {
		return mapper.amountPriceByWeeknum(condition);
	}

	@Override
	public List<WeeknumStatistics> amountPriceByMemberIdCategory(Integer memberId) {
		return mapper.amountPriceByMemberIdCategory(memberId);
	}

	@Override
	public int deleteByMemberIdCategory(ConditionModel condition) {
		return mapper.deleteByMemberIdCategory(condition);
	}

	@Override
	public List<ShopTrolleyVo> selectByCondition(ConditionModel condition) {
		return mapper.selectByCondition(condition);
	}

	@Override
	public List<ShopTrolleyVo> selectByMemberIdAndStoreId(Integer memberId, Integer storeId) {
		return mapper.selectByMemberIdAndStoreId(memberId,storeId);
//		return mapper.selectByMemberId(memberId);
	}

	@Override
	public ShopCatVo amountPriceAndNumByWeeknum(ConditionModel condition) {
		// TODO Auto-generated method stub
		return mapper.amountPriceAndNumByWeeknum(condition);
	}

	@Override
	public List<Integer> getweekSByMemberIdAndStoreId(Integer memberId, Integer sId) {
		// TODO Auto-generated method stub
		return mapper.getweekSByMemberIdAndStoreId(memberId,sId);
	}

	@Override
	public WeeknumStatistics getWeeknumStatistic(Integer w,Integer memberId,Integer storeId) {
		// TODO Auto-generated method stub
		return mapper.getWeeknumStatistic(w,memberId,storeId);
	}

	@Override
	public double amountPriceByWeeknumAndStoreId(ConditionModel condition) {
		// TODO Auto-generated method stub
		return mapper.amountPriceByWeeknumAndStoreId(condition);
	}

	@Override
	public int deleteByMemberIdAndStoreId(Integer memberId, Integer storeId) {
		// TODO Auto-generated method stub
		return mapper.deleteByMemberIdAndStoreId(memberId,storeId);
	}

	@Override
	public List<ShopTrolleyVo> selectByMemberIdAndStoreIdAndWeek(Integer memberId, Integer storeId, Integer week) {
		// TODO Auto-generated method stub
		return mapper.selectByMemberIdAndStoreIdAndWeek( memberId,  storeId,  week);
	}

	@Override
	public ShopTrolley selectByMemberIdAndStoreGoodsIdAndWeek(Integer memberId, Long sgid, Integer week) {
		// TODO Auto-generated method stub
		return mapper.selectByMemberIdAndStoreGoodsIdAndWeek(memberId, sgid.intValue(),  week);
	}

}
