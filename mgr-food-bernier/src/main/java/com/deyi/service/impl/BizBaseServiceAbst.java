package com.deyi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.deyi.dao.GoodsMapper;
import com.deyi.dao.MemberMapper;
import com.deyi.dao.SysParmMapper;

public abstract class BizBaseServiceAbst {
	@Autowired
	protected MemberMapper memberMapper;
	
	@Autowired
	protected GoodsMapper goodsMapper;
	
	@Autowired
	protected SysParmMapper sysParmMapper;
}
