package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.MemberLevelDao;
import com.deyi.entity.MemberLevel;
import com.deyi.service.MemberLevelService;
import com.deyi.util.Page;
@Service
public class MemberLevelServiceImpl implements MemberLevelService {
	@Autowired
	private  MemberLevelDao memberLevelDao;

	public List<MemberLevel> queryPage(Page<MemberLevel> page) {
		// TODO Auto-generated method stub
		return memberLevelDao.queryPage(page);
	}

	public MemberLevel selectByPrimaryKey(int id) {
		// TODO Auto-generated method stub
		return memberLevelDao.selectByPrimaryKey(id);
	}

	public void updateByPrimaryKeySelective(MemberLevel memberLevel) {
		// TODO Auto-generated method stub
		memberLevelDao.updateByPrimaryKeySelective(memberLevel);
	}

	@Override
	public List<MemberLevel> queryAll() {
		return memberLevelDao.queryAll();
	}

	@Override
	public MemberLevel selectByPrimaryName(String name) {
		
		return memberLevelDao.selectByPrimaryName(name);
	}

	@Override
	public MemberLevel querryByPrimaryKeymemberId(String memberId) {
		return memberLevelDao.querryByPrimaryKeymemberId(memberId);
	}

	@Override
	public MemberLevel querryByNextLevel(Integer memberLevelId) {
		//查询会员等级下级信息
		return memberLevelDao.querryByNextLevel(memberLevelId+1);
	}
}
