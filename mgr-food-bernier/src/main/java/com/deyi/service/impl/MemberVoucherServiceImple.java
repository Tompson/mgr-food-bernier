package com.deyi.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.support.logging.Log;
import com.deyi.dao.MemberVoucherMapper;
import com.deyi.entity.MemberIntegral;
import com.deyi.entity.MemberVoucher;
import com.deyi.entity.SysCoupon;
import com.deyi.model.ResultVo;
import com.deyi.model.VoucherQueryCondition;
import com.deyi.model.vo.MemberVoucherVo;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.SysCouponService;
import com.deyi.util.Constants;
import com.deyi.util.Page;
@Service
public class MemberVoucherServiceImple implements MemberVoucherService{
	@Autowired
	private MemberVoucherMapper mapper;
	@Autowired
	private SysCouponService sysCouponService;

	@Override
	public int deleteByPrimaryKey(Long id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(MemberVoucher record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(MemberVoucher record) {
		return mapper.insertSelective(record);
	}

	@Override
	public MemberVoucher selectByPrimaryKey(Long id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(MemberVoucher record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(MemberVoucher record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<MemberVoucherVo> selectByPage(Page<MemberVoucherVo> page) {
		return mapper.selectByPage(page);
	}

	@Override
	public List<MemberVoucherVo> selectSatisfactoryVoucher(VoucherQueryCondition record) {
		return mapper.selectSatisfactoryVoucher(record);
	}

	@Override
	public List<MemberIntegral> selectCurrentDayShareByMemberId(Integer memberId) {
		return mapper.selectCurrentDayShareByMemberId(memberId);
	}

	@Override
	public Integer deleteByMemberId(Long memberId) {
		return mapper.deleteByMemberId(memberId);
	}

	@Override
	public List<MemberVoucher> selectByMemberIdAndCouponId(Integer memberId, Long id) {
		return mapper.selectByMemberIdAndCouponId(memberId,id);
	}

	@Override
	public List<MemberVoucherVo> selectByPageExtra(Page<MemberVoucherVo> page) {
		return mapper.selectByPageExtra(page);
	}

	@Override
	public MemberVoucherVo selectVoucherByKey(MemberVoucher record) {
		return mapper.selectVoucherByKey(record);
	}

	@Override
	public void deleteByMemberIdAndvoucherId(Integer memberId, Integer voucherId) {
		// TODO Auto-generated method stub
	  mapper.deleteByMemberIdAndvoucherId(memberId,voucherId);
	}

	@Override
	@Transactional
	public ResultVo<Object> gainVoucher(Integer memberId, MemberVoucher memberVoucher) {
		// TODO Auto-generated method stub
		ResultVo<Object> vo=new ResultVo<>();
		if(!memberVoucher.getCategory().equals(Constants.VOUCHER_CATEGORY_4)){
			vo.setSuccess(false);
			vo.setMsg("非平台代金卷不允许领取");
			return vo;
		}
		SysCoupon coupon = sysCouponService.selectByPrimaryKey(memberVoucher.getVoucherId());
		if(coupon.getRemquantity()<0||(!coupon.getStatus().equals(Constants.COUPON_RUNNING))){
			vo.setSuccess(false);
			vo.setMsg("手太慢了哦~代金券已被他人领取");
			return vo;
		}
		List<MemberVoucher> list = mapper.selectByMemberIdAndCouponId(memberId, memberVoucher.getVoucherId());
		if(list.size()>0){
			vo.setSuccess(false);
			vo.setMsg("不能领取，您已经拥有该代金卷了");
			return vo;
		}
		
		MemberVoucher mv=new MemberVoucher();
		mv.setCategory("4");//平台代金卷
		mv.setCreateTime(new Date());//领取时间
		mv.setMemberId(memberId.longValue());//会员id
		mv.setUseStatus("2");//未使用
		mv.setVoucherId(coupon.getId());//平台代金券id
		mv.setIfOverdue("1");//未过期
		mv.setPresenterId(memberVoucher.getMemberId());
		mapper.insertSelective(mv);
		//品台代金券数量减1
		if(coupon.getRemquantity()>1){
			coupon.setRemquantity(coupon.getRemquantity()-1);
			sysCouponService.updateByPrimaryKeySelective(coupon);
		}else{
			//如果抢完了，设置状态为结束
			coupon.setRemquantity(coupon.getRemquantity()-1);
			coupon.setStatus("3");
			sysCouponService.updateByPrimaryKeySelective(coupon);
		}
		vo.setSuccess(true);
		vo.setMsg("恭喜你领取到一张代金卷");
		return vo;
	}


	
}
