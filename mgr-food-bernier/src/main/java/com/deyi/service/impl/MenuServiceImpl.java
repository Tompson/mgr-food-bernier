package com.deyi.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.MenuDao;
import com.deyi.entity.Menu;
import com.deyi.entity.UserMenu;
import com.deyi.service.MenuService;

@Service
public class MenuServiceImpl implements MenuService{
	@Autowired
	private MenuDao menuDao;

	@Override
	public List<Menu> getAllMenus() {
		return menuDao.getAllMenus();
	}

	@Override
	public List<Menu> getMenusByUser(Integer id) {
		
		return menuDao.getMenusByUser(id);
	}

	@Override
	public Menu getMenuById(String id) {
		return menuDao.getMenuById(id);
	}

	@Override
	public List<String> getMenuIdsByUserId(String uid) {
		// TODO Auto-generated method stub
		return menuDao.getMenuIdsByUserId(uid);
	}

	@Override
	public void deleteUserMenuByUserId(String id) {
		// TODO Auto-generated method stub
		menuDao.deleteUserMenuByUserId(id);
	}

	@Override
	public void insertSelective(UserMenu um) {
		// TODO Auto-generated method stub
		menuDao.insetUserMenu(um);
	}

	
	
}
