package com.deyi.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.MemberAddressMapper;
import com.deyi.entity.Member;
import com.deyi.entity.MemberAddress;
import com.deyi.service.MemberAddressService;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.deyi.util.StringUtils;

@Service
@Transactional
public class MemberAddressServiceImpl implements MemberAddressService {
	@Autowired
	private MemberAddressMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(Integer addressId) {
		return mapper.deleteByPrimaryKey(addressId);
	}

	@Override
	public int insert(MemberAddress record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(MemberAddress record) {
		return mapper.insertSelective(record);
	}

	@Override
	public MemberAddress selectByPrimaryKey(Integer addressId) {
		return mapper.selectByPrimaryKey(addressId);
	}

	@Override
	public int updateByPrimaryKeySelective(MemberAddress record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(MemberAddress record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<MemberAddress> selectByPage(Page<MemberAddress> page) {
		return mapper.selectByPage(page);
	}

	@Override
	public int update2DefaultAddress(MemberAddress record,Member member) {
		if(record!=null&&record.getAddressId()!=null&&!StringUtils.isBlank(record.getStatus())){
			if(Constants.DEFAULT_ADDRESS.equals(record.getStatus())){
				//将所有地址更新为非默认状态
				MemberAddress memberAddress = new MemberAddress();
				memberAddress.setMemberId(member.getMemberId());
				memberAddress.setStatus(Constants.NOT_DEFAULT_ADDRESS);
				mapper.updateStatusByMemberId(memberAddress);
				//在将当前地址更新为默认状态
				mapper.updateByPrimaryKeySelective(record);
				return 1;
			}
		}
		return -1;
	}

	@Override
	public int changeAddress(MemberAddress record,Member member) {
		if(record!=null&&!StringUtils.isBlank(record.getContacts(),record.getPhone(),record.getAddressName(),record.getStatus())){
			if(record.getAddressId()!=null){
				//更新
				mapper.updateByPrimaryKeySelective(record);
				update2DefaultAddress(record,member);
				return 0;
			}else{
				//插入
				record.setMemberId(member.getMemberId());
				record.setCreateTime(new Date());
				mapper.insertSelective(record);
				update2DefaultAddress(record,member);
				return 1;
				
				
			}
			
		}
		return -1;
	}

	@Override
	public List<MemberAddress> selectDefaultAddressByMemberId(Integer memberId) {
		return mapper.selectDefaultAddressByMemberId(memberId);
	}

	@Override
	public Integer deleteByPrimaryMemberId(Integer MemberId) {
		return mapper.deleteByPrimaryMemberId(MemberId);
	}

}
