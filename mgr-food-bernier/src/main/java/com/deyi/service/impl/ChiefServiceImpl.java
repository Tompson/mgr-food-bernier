package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.ChiefDao;
import com.deyi.entity.Chief;
import com.deyi.service.ChiefService;
import com.deyi.util.Page;
@Service
public class ChiefServiceImpl implements ChiefService {
	@Autowired
	private ChiefDao chiefDao;
	@Override
	public List<Chief> queryPage(Page<Chief> page) {
		// TODO Auto-generated method stub
		return chiefDao.queryPage(page);
	}
	@Override
	public int insertChief(Chief chief) {
		// TODO Auto-generated method stub
		return chiefDao.insert(chief);
	}
	@Override
	public Chief selectByPrimaryKey(int id) {
		// TODO Auto-generated method stub
		return chiefDao.selectByPrimaryKey(id);
	}
	@Override
	public void updateByPrimaryKeySelective(Chief chief) {
		// TODO Auto-generated method stub
		chiefDao.updateByPrimaryKeySelective(chief);
	}
	@Override
	public void deleteByPrimaryKey(int id) {
		// TODO Auto-generated method stub
		chiefDao.deleteByPrimaryKey(id);
	}
	@Override
	public List<Chief> queryAllByStoreId(Integer storeId) {
		return chiefDao.queryAllByStoreId(storeId);
	}

}
