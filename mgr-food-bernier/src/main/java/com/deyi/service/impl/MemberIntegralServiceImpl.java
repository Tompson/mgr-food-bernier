package com.deyi.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.MemberIntegralMapper;
import com.deyi.entity.Member;
import com.deyi.entity.MemberIntegral;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.util.Constants;

@Service
public class MemberIntegralServiceImpl implements MemberIntegralService {
	@Autowired
	private MemberIntegralMapper mapper;
	
	@Autowired
	private MemberService memberService;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(MemberIntegral record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(MemberIntegral record) {
		return mapper.insertSelective(record);
	}

	@Override
	public MemberIntegral selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(MemberIntegral record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(MemberIntegral record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public MemberIntegral selectByMemberId(Integer memberId) {
		return mapper.selectByMemberId(memberId);
	}

	@Override
	@Transactional
	public int updateIntegral(Member member, Integer integral,String type) {
		MemberIntegral  memberIntegral= selectByMemberId(member.getMemberId());
		if(memberIntegral!=null){
			MemberIntegral newMemberIntegral = new MemberIntegral();
			newMemberIntegral.setIntegralTime(new Date());
			newMemberIntegral.setType(type);
			newMemberIntegral.setIntegral(integral+memberIntegral.getIntegral());
			newMemberIntegral.setMemberId(member.getMemberId());
			newMemberIntegral.setMemberNick(member.getMemberNick());
			if(Constants.MEMBER_INTEGRAL_TYPE_CONSUME.equals(type)){
				newMemberIntegral.setRemarks("消费赠送");
			}else if(Constants.MEMBER_INTEGRAL_TYPE_ADVICE.equals(type)){
				newMemberIntegral.setRemarks("评论赠送");
			}else if(Constants.MEMBER_INTEGRAL_TYPE_SHARE.equals(type)){
				newMemberIntegral.setRemarks("分享赠送");
			}
			
			insertSelective(newMemberIntegral);
			/**
			 * 升级
			 */
			memberService.upgradeMember(newMemberIntegral.getMemberId(),member.getMemberLevelId(), newMemberIntegral.getIntegral());
			return 1;
		}
		return -1;
	}

	@Override
	public List<MemberIntegral> selectCurrentDayShareByMemberId(Integer memberId) {
		return mapper.selectCurrentDayShareByMemberId(memberId);
	}

	@Override
	public Integer deleteByMemberId(Integer id) {
		return mapper.deleteByMemberId(id);
	}

}
