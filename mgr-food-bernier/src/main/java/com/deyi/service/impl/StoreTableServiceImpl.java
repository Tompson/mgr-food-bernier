package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.StoreTableDao;
import com.deyi.entity.StoreTable;
import com.deyi.service.StoreTableService;
import com.deyi.util.Page;
@Service
public class StoreTableServiceImpl implements StoreTableService {
	@Autowired
	private StoreTableDao storeTableDao;

	@Override
	public List<StoreTable> queryPage(Page<StoreTable> page) {
		return storeTableDao.queryPage(page);
	}

	
	@Override
	public Integer insertSelectiveReturnId(StoreTable storeTable) {
		return storeTableDao.insertSelective(storeTable);
	}


	@Override
	public void deleteByPrimaryKey(int id) {
		storeTableDao.deleteByPrimaryKey(id);
	}


	@Override
	public StoreTable selectByPrimaryKey(int id) {
		return storeTableDao.selectByPrimaryKey(id);
	}


	@Override
	public void updateByPrimaryKeySelective(StoreTable storeTable) {
		storeTableDao.updateByPrimaryKey(storeTable);
	}
}
