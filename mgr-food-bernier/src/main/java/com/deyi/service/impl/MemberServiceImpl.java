package com.deyi.service.impl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ServletContextAware;

import com.deyi.dao.MemberAddressMapper;
import com.deyi.dao.MemberMapper;
import com.deyi.dao.MemberVoucherMapper;
import com.deyi.dao.VoucherMapper;
import com.deyi.entity.Member;
import com.deyi.entity.MemberIntegral;
import com.deyi.entity.MemberLevel;
import com.deyi.entity.MemberVoucher;
import com.deyi.entity.SysOrder;
import com.deyi.entity.SysParm;
import com.deyi.entity.Voucher;
import com.deyi.model.ResultVo;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberLevelService;
import com.deyi.service.MemberService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.util.Constants;
import com.deyi.util.LotteryUtil;
import com.deyi.util.Page;
import com.deyi.vo.SendMsg;

@Service
@Transactional
public class MemberServiceImpl implements MemberService, ServletContextAware {
	@Autowired
	private MemberMapper mapper;

	@Autowired
	private SysParmService sysParmService;

	@Autowired
	private MemberIntegralService memberIntegralService;

	@Autowired
	private MemberVoucherMapper memberVoucherMapper;
	private ServletContext servletContext = null;

	@Autowired
	private MemberAddressMapper memberAddressMapper;

	@Autowired
	private VoucherMapper voucherMapper;
	@Autowired
	private SysOrderService sysOrderService;
	@Autowired
	private MemberLevelService memberLevelService;
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	@Override
	public int deleteByPrimaryKey(Integer memberId) {
		return mapper.deleteByPrimaryKey(memberId);
	}

	@Override
	public int insert(Member record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(Member record) {
		return mapper.insertSelective(record);
	}

	@Override
	public Member selectByPrimaryKey(Integer memberId) {
		return mapper.selectByPrimaryKey(memberId);
	}

	@Override
	public int updateByPrimaryKeySelective(Member record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Member record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public Member selectByWxOpenId(String wxOpenId) {
		return mapper.selectByWxOpenId(wxOpenId);
	}

	@Override
	public Member insertAndReturn(Member entity) {
		insertSelective(entity);
		return selectByPrimaryKey(entity.getMemberId());
	}

	@Override
	public List<Member> queryPage(Page<Member> page) {// wxGroupid=0
		return mapper.queryPage(page);
	}

	@Override
	public int getShareAward(Member member) {
		SysParm realSysParam = sysParmService.getRealSysParam();
		// 每次分享积分,每天分享只加一次
		Double shareEvery = realSysParam.getShareEvery();
		List<MemberIntegral> shareList = memberIntegralService.selectCurrentDayShareByMemberId(member.getMemberId());
		if (shareList == null || shareList.size() < 1) {
			memberIntegralService.updateIntegral(member, shareEvery.intValue(), Constants.MEMBER_INTEGRAL_TYPE_SHARE);
			return shareEvery.intValue();
		}
		return -1;
	}

	@Override
	public double lotteryDraw(Member member, String category) throws NullPointerException, SQLException {
		double voucherMoney = 0d;
		List<Voucher> drawVoucherList = voucherMapper.selectDrawVoucher();
		List<BigDecimal> list = new ArrayList<>();
		for (Voucher voucher : drawVoucherList) {
			BigDecimal drawProbability = voucher.getDrawProbability();
			// 因为后台没有设置概率，这里默认给一个概率
			if (drawProbability == null||drawProbability.compareTo(BigDecimal.ZERO) == -1) {
				drawProbability = BigDecimal.ZERO;
			}
			list.add(drawProbability);
		}
		// 中奖的代金券下标
		int index = LotteryUtil.lottery(list);
		// 中奖的代金券
		if(index>=drawVoucherList.size()){
			//未中奖
			return -1;
		}
		Voucher voucher = drawVoucherList.get(index);
		voucherMoney = voucher.getVoucherMoney().doubleValue();
		MemberVoucher memberVoucher = new MemberVoucher();
		memberVoucher.setCreateTime(new Date());
		memberVoucher.setMemberId(member.getMemberId().longValue());
		memberVoucher.setVoucherId(voucher.getId());
		memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_UNUSED);
		memberVoucher.setCategory(category);
		memberVoucherMapper.insertSelective(memberVoucher);
		return voucherMoney;
	}
	//百分百概率
	/*public double lotteryDraw(Member member, String category) throws NullPointerException, SQLException {
		double voucherMoney = 0d;
		List<Voucher> drawVoucherList = voucherMapper.selectDrawVoucher();
		List<BigDecimal> list = new ArrayList<>();
		for (Voucher voucher : drawVoucherList) {
			BigDecimal drawProbability = voucher.getDrawProbability();
			// 因为后台没有设置概率，这里默认给一个概率
			if (drawProbability == null) {
				drawProbability = BigDecimal.valueOf(100);
			}
			if (drawProbability.compareTo(BigDecimal.ZERO) == -1) {
				// 概率小于零
				drawProbability = BigDecimal.ZERO;
			}
			list.add(drawProbability);
		}
		
		 //中奖的代金券下标 int index = LotteryUtil.lottery(list); //中奖的代金券 Voucher
		 voucher = drawVoucherList.get(index);
		 voucherMoney=voucher.getVoucherMoney().doubleValue(); MemberVoucher
		 memberVoucher = new MemberVoucher(); memberVoucher.setCreateTime(new
		 Date()); memberVoucher.setMemberId(member.getMemberId().longValue());
		 memberVoucher.setVoucherId(voucher.getId());
		 memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_UNUSED);
		 memberVoucher.setCategory(category);
		 memberVoucherMapper.insertSelective(memberVoucher);
	}*/

	@Override
	public Integer exchangeVoucher(Member member, Long voucherId) throws NullPointerException, SQLException {
		MemberIntegral memberIntegral = memberIntegralService.selectByMemberId(member.getMemberId());
		Integer maxIntegral = memberIntegral.getIntegral();
		Voucher voucher = voucherMapper.selectByPrimaryKey(voucherId);
		Integer exchangeIntegral = voucher.getExchangeVoucher().intValue();
		if (exchangeIntegral > maxIntegral) {
			return -1;
		}
		Integer integral = maxIntegral - exchangeIntegral;
		if (integral >= 0) {
			// 生成代金券
			MemberVoucher memberVoucher = new MemberVoucher();
			memberVoucher.setCreateTime(new Date());
			memberVoucher.setMemberId(member.getMemberId().longValue());
			memberVoucher.setVoucherId(voucherId);
			memberVoucher.setCategory("1");//积分兑换代金卷
			memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_UNUSED);
			memberVoucherMapper.insertSelective(memberVoucher);
			// 插入最新积分
			MemberIntegral record = new MemberIntegral();
			record.setIntegral(integral);
			record.setIntegralTime(new Date());
			record.setMemberId(member.getMemberId());
			record.setType(Constants.MEMBER_INTEGRAL_TYPE_USE);
			record.setRemarks("兑换代金券");
			memberIntegralService.insertSelective(record);
		}
		return integral;
	}

	@Override
	public Integer deleteByPrimaryKey2(Integer memberId) throws Exception {
		// 删除的时候把和会员有关的数据删掉
		// memberAddressMapper.deleteByPrimaryMemberId(memberId);//删除会员地址
		// memberIntegralService.deleteByMemberId(memberId);//删除会员积分
		// memberVoucherMapper.deleteByMemberId(Long.valueOf(memberId));//删除会员代金券

		return mapper.deleteByPrimaryKey(memberId);
	}

	@Override
	@Transactional
	public ResultVo<Object> creditpay(Member member, String storeId, String payno,HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		ResultVo<Object> resultVo=new ResultVo<>();
		try {
			SysOrder order = sysOrderService.selectByPayNumber(payno);
			double totalAmount = order.getTotalAmount().doubleValue();
			int compareTo = member.getMoney().compareTo(order.getTotalAmount());//比较大小，返回的结果是int类型，-1表示小于，0是等于，1是大于。
			if(compareTo<0){
				resultVo.setSuccess(false);
				resultVo.setMsg("余额不足，请充值");
				return resultVo;
			}
			BigDecimal money=member.getMoney().subtract(order.getTotalAmount());//余额减去消费的总价；
			member.setMoney(money);
			mapper.updateMemberMoney(member);
			order.setPayTime(new Date());
			order.setPayType(Constants.ORDER_PAY_TYPE_BALANCE);//余额支付
			order.setPayStatus(Constants.ORDER_STATUS_PAIED);//支付成功
			order.setPayAmount(order.getTotalAmount());//支付金额
			sysOrderService.updateByPrimaryKeySelective(order);
			// 支付宝订单完成
			//3、显示成功提示音
		    //调用Dwr推
			SendMsg.sendMsg(order,servletContext,httpRequest);
			resultVo.setSuccess(true);
			resultVo.setMsg("支付成功");
		} catch (Exception e) {
			e.printStackTrace();
			resultVo.setSuccess(false);
			resultVo.setMsg("error");
			
		}
		return resultVo;
	}

	@Override
	public void updateMemberMoney(Member member) {
		mapper.updateMemberMoney(member);
	}

	@Override
	public void upgradeMember(Integer memberId,Integer memberLevelId,Integer integral) {
		//升级到下级会员所需积分
		MemberLevel memberLevel=memberLevelService.querryByNextLevel(memberLevelId);
		if(memberLevel!=null){
			//满足条件升级
			if(integral>=memberLevel.getUpCount()){
				Member member=new Member();
				member.setMemberId(memberId);
				member.setMemberLevelId(memberLevelId+1);
				mapper.updateByPrimaryKeySelective(member);
				//递归升级
				upgradeMember(memberId, member.getMemberLevelId(), integral);
			}
		}
		
	}


}
