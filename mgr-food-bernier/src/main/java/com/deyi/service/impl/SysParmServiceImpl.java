package com.deyi.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.SysParmMapper;
import com.deyi.entity.SysParm;
import com.deyi.service.SysParmService;
@Service
public class SysParmServiceImpl implements SysParmService {
	@Autowired
	private SysParmMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysParm record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SysParm record) {
		return mapper.insertSelective(record);
	}

	@Override
	public SysParm selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysParm record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysParm record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<SysParm> selectAll() {
		return mapper.selectAll();
	}

	@Override
	public double converIntegral(Integer integral) {
		if(integral!=null){
			//积分兑换比例，固定比例为100:1元
			BigDecimal bigDecimal = new BigDecimal(integral);
			return bigDecimal.divide(new BigDecimal(100)).doubleValue();
		}
		return 0;
	}

	@Override
	public SysParm getRealSysParam() {
		List<SysParm> sysParamList = selectAll();
		if(!sysParamList.isEmpty()){
			return sysParamList.get(0);
		}
		return null;
	}
	
	

}
