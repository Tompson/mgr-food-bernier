package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.PartDao;
import com.deyi.entity.Part;
import com.deyi.service.PartService;
import com.deyi.util.Page;
@Service
public class PartServiceImpl implements PartService {
	@Autowired
	private PartDao partDao;

	@Override
	public List<Part> queryPage(Page<Part> page) {
		// TODO Auto-generated method stub
		return partDao.queryPage(page);
	}

	@Override
	public void insertSelective(Part part) {
		// TODO Auto-generated method stub
		partDao.insertSelective(part);
	}

	@Override
	public Part selectByPrimaryKey(int parseInt) {
		// TODO Auto-generated method stub
		return partDao.selectByPrimaryKey(parseInt);
	}

	@Override
	public void updateByPrimaryKeySelective(Part part) {
		// TODO Auto-generated method stub
		partDao.updateByPrimaryKeySelective(part);
	}

	@Override
	public void deleteByPrimaryKey(int parseInt) {
		// TODO Auto-generated method stub
		partDao.deleteByPrimaryKey(parseInt);
	}

	@Override
	public List<Part> wxgetPartsByStoreId(Integer id) {
		// TODO Auto-generated method stub
		return partDao.wxgetPartsByStoreId(id);
	}

	@Override
	public List<Part> wxgetFullPartsByStoreId(Integer id) {
		// TODO Auto-generated method stub
		return partDao.wxgetFullPartsByStoreId(id);
	}

	@Override
	public List<Part> wxgetDiscountPartsByStoreId(Integer id) {
		// TODO Auto-generated method stub
		return partDao.wxgetDiscountPartsByStoreId(id);
	}
}
