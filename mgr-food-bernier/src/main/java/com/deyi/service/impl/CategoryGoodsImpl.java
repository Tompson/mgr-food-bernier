package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.CategoryGoodsMapper;
import com.deyi.dao.StoreCategoryMapper;
import com.deyi.entity.CategoryGoods;
import com.deyi.service.CategoryGoodsService;
import com.deyi.util.Page;
import com.deyi.vo.ReturnVo;

/** 
 * @author zjz Create on 2016年10月22日 上午11:46:36 
 */
@Service
public class CategoryGoodsImpl implements CategoryGoodsService{
	@Autowired
	private CategoryGoodsMapper categoryGoodsMapper;
	
	@Autowired
	private StoreCategoryMapper storeCategoryDao;
	
	@Override
	@Transactional
	public int deleteByPrimaryKey(Long categoryId) {
		//删除分类同步删除门店下面对应的分类
		storeCategoryDao.delCategoryByStoreId(categoryId);
		return categoryGoodsMapper.deleteByPrimaryKey(categoryId);
	}

	@Override
	public int insert(CategoryGoods record) {
		return categoryGoodsMapper.insert(record);
	}

	@Override
	public int insertSelective(CategoryGoods record) {
		long maxSort =categoryGoodsMapper.getMaxSort();
		Integer a=new Long(maxSort).intValue();
		record.setSort(a+1);
		return categoryGoodsMapper.insertSelective(record);
	}

	@Override
	public CategoryGoods selectByPrimaryKey(Long categoryId) {
		return categoryGoodsMapper.selectByPrimaryKey(categoryId);
	}

	@Override
	public int updateByPrimaryKeySelective(CategoryGoods record) {
		return categoryGoodsMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(CategoryGoods record) {
		return categoryGoodsMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<CategoryGoods> queryAll() {
		return categoryGoodsMapper.queryAll();
	}

	@Override
	public List<CategoryGoods> queryPage(Page<CategoryGoods> page) {
		// TODO Auto-generated method stub
		return categoryGoodsMapper.queryPage(page);
	}

	@Override
	public List<CategoryGoods> getCategoryByCategoryName(String cgoodsName) {
		// TODO Auto-generated method stub
		return categoryGoodsMapper.getCategoryByCategoryName(cgoodsName);
	}

	@Override
	public ReturnVo<Object> moveUp(String id, ReturnVo<Object> vo) {
		// TODO Auto-generated method stub
		long categoryId=Long.parseLong(id);
		CategoryGoods upRecord=categoryGoodsMapper.getUpRecord(categoryId);
		if(upRecord ==null){//不存在上一个记录
			vo.setStatusCode(1);
			vo.setSuccess(false);
			vo.setMessage("该菜品分类已经是第一个了,请勿重复上移");
			return vo;
		}
		int switchSort = categoryGoodsMapper.switchSort(categoryId, upRecord.getCategoryId());
		System.out.println(switchSort);
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public ReturnVo<Object> moveDown(String id, ReturnVo<Object> vo) {
		// TODO Auto-generated method stub
		long categoryId=Long.parseLong(id);
		CategoryGoods upRecord=categoryGoodsMapper.getDownRecord(categoryId);
		if(upRecord ==null){//不存在上一个记录
			vo.setStatusCode(1);
			vo.setSuccess(false);
			vo.setMessage("该菜品分类已经是最后一个了,请勿重复下移");
			return vo;
		}
		int switchSort = categoryGoodsMapper.switchSort(categoryId, upRecord.getCategoryId());
		System.out.println(switchSort);
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public ReturnVo<Object> toTop(String id, ReturnVo<Object> vo) {
		// TODO Auto-generated method stub
		CategoryGoods category=categoryGoodsMapper.selectByPrimaryKey(Long.parseLong(id));
		Integer sort = category.getSort();
		CategoryGoods topRecord=categoryGoodsMapper.getTopRecord();
		Integer sort2 = topRecord.getSort();
		if(sort == sort2){//页面点击的排序号和置顶排序号相等
			vo.setMessage("该菜品分类已经置顶,请勿重复置顶");
			vo.setSuccess(false);
			vo.setStatusCode(1);
			return vo;
		}
		CategoryGoods category2=new CategoryGoods();
		category2.setCategoryId(category.getCategoryId());
		category2.setSort(sort2+1);
		categoryGoodsMapper.updateByPrimaryKeySelective(category2);
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public List<CategoryGoods> wxgetCategoryLsit(Integer id) {
		// TODO Auto-generated method stub
		return categoryGoodsMapper.getCategoryListByStoreId(id);
	}
	

}
