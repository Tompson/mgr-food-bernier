package com.deyi.service.impl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ServletContextAware;

import com.deyi.dao.MemberVoucherMapper;
import com.deyi.dao.SysOrderMapper;
import com.deyi.dao.VoucherMapper;
import com.deyi.entity.Goods;
import com.deyi.entity.Member;
import com.deyi.entity.MemberLevel;
import com.deyi.entity.MemberVoucher;
import com.deyi.entity.OrderComment;
import com.deyi.entity.OrderGoods;
import com.deyi.entity.Store;
import com.deyi.entity.StoreGoods;
import com.deyi.entity.SysActivity;
import com.deyi.entity.SysOrder;
import com.deyi.entity.SysParm;
import com.deyi.entity.Voucher;
import com.deyi.model.ConditionModel;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.model.vo.SysOrderVo;
import com.deyi.service.GoodsService;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberLevelService;
import com.deyi.service.MemberService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.OrderGoodsService;
import com.deyi.service.ShopCarService;
import com.deyi.service.ShopTrolleyService;
import com.deyi.service.StoreService;
import com.deyi.service.SysOrderService;
import com.deyi.service.SysParmService;
import com.deyi.service.VoucherService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.DateUtils;
import com.deyi.util.Page;
import com.deyi.util.StringUtils;
import com.deyi.vo.SendMsg;

@Service
@Transactional
public class SysOrderServiceImpl implements SysOrderService , ServletContextAware{//1、首先该类实现ServletContextAware接口
	@Autowired
	private SysOrderMapper mapper;
	
	@Autowired
	private VoucherMapper voucherMapper;
	
	@Autowired
	private MemberVoucherMapper memberVoucherMapper;
	
	private ServletContext servletContext = null;
	
	private Logger log=LoggerFactory.getLogger(SysOrderServiceImpl.class);
	
	
	
	@Autowired
	private ShopTrolleyService shopTrolleyService;
	
	@Autowired
	private SysParmService sysParmService;
	
	@Autowired
	private OrderGoodsService orderGoodsService;
	
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private MemberLevelService memberLevelService;
	@Autowired
	private MemberIntegralService memberIntegralService;
	
	@Autowired
	private OrderCommentService orderCommentService;
	@Autowired
	private ShopCarService shopCarService;
	@Autowired
	private StoreService storeService;
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	@Override
	public int deleteByPrimaryKey(Long orderId) {
		return mapper.deleteByPrimaryKey(orderId);
	}

	@Override
	public int insert(SysOrder record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SysOrder record) {
		return mapper.insertSelective(record);
	}

	@Override
	public SysOrder selectByPrimaryKey(Long orderId) {
		return mapper.selectByPrimaryKey(orderId);
	}

	@Override
	public int updateByPrimaryKeySelective(SysOrder record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysOrder record) {
		return mapper.updateByPrimaryKey(record);
	}
	@Override
	public Page<SysOrderVo> selectByPage(Page<SysOrderVo> page) {
		Integer  memberId = (Integer) page.getParams().get("memberId");
		int totalRecord=mapper.selectTotalNum(memberId);
		List<SysOrderVo> list = mapper.selectByPage(page);
		page.setTotalRecord(totalRecord);
		int mo=totalRecord%page.getPageSize()>0?1:0;
		page.setTotalPage(totalRecord/page.getPageSize()+mo);
		page.setResults(list);
		return page;
	}
	@Override
	public SysOrder createOrderAndDetail1(SysOrder order, Integer memberId, String bookTime, Long voucherId) {
		// TODO Auto-generated method stub
		Date date = new Date(); 
//		//预定类型
//		order.setOrderType("4");//堂食订单/
		//预定时间
		order.setBookTime(date);
		//下单时间
		order.setOrderTime(date);
		//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成
		order.setOrderStatus("0");
		//支付状态 0.未支付 1支付成功  2支付失败  3退款中 4 已退款 5退款失败
		order.setPayStatus(Constants.ORDER_STATUS_UNPAY);
		// 0未支付，1支付宝，2微信支付，3现金支付，4余额支付
		order.setPayType("0");
		order.setPayNumber(BizHelper.getPayNo());
		Member member = memberService.selectByPrimaryKey(memberId);
		order.setMemberId(memberId);
		order.setMemberNick(member.getMemberNick());
		ShopCatVo statCar = shopCarService.statPriceAndNumByMemberIdTableId(memberId.longValue(),order.getTableId().longValue());
		double totalPrice = statCar.getAmountPrice();
		order.setDiscountValue(totalPrice-order.getTotalAmount().doubleValue());//优惠金额
		mapper.insertSelective(order);
		// 生成订单详情
		Integer storeId = new Integer(order.getStoreId().intValue());
		List<ShopTrolleyVo> cartList = shopCarService.selectByMemberIdTableId(memberId.longValue(), order.getTableId().longValue());
		for (ShopTrolleyVo vo : cartList) {
			OrderGoods orderGoods = new OrderGoods();
			orderGoods.setOrderId(order.getOrderId());
			orderGoods.setGoodsId(vo.getGoodsId());
			orderGoods.setGoogsName(vo.getGoodsName());
			orderGoods.setPrice(vo.getUnitPrice());
			orderGoods.setQuantify(vo.getNum());
			orderGoods.setPricePay(vo.getUnitPrice()*vo.getNum());
			orderGoods.setStoreGoodsId(vo.getStoreGoodsId());
			orderGoodsService.insertSelective(orderGoods);
		}
		if(voucherId!=null){
			//使用了代金券
			MemberVoucher memberVoucher = new MemberVoucher();
			memberVoucher.setId(voucherId);
			memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_USED);
			memberVoucherMapper.updateByPrimaryKeySelective(memberVoucher);
		}
		// 生成订单清空购物车
		shopCarService.deleteByMemberIdStoreId(memberId.longValue(), order.getStoreId().longValue());
		return order;
	}
	@Override
	public SysOrder createSingleOrderAndDetail1(SysOrder order, Integer memberId, String bookTime, Long goodsId,
			Integer amount, Long voucherId) {
		// TODO Auto-generated method stub
				Date date = new Date(); 
//				//预定类型
//				order.setOrderType("4");//堂食订单/
				//预定时间
				order.setBookTime(date);
				//下单时间
				order.setOrderTime(date);
				//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成
				order.setOrderStatus("0");
				//支付状态 0.未支付 1支付成功  2支付失败  3退款中 4 已退款 5退款失败
				order.setPayStatus(Constants.ORDER_STATUS_UNPAY);
				// 0未支付，1支付宝，2微信支付，3现金支付，4余额支付
				order.setPayType("0");
				order.setPayNumber(BizHelper.getPayNo());
				Member member = memberService.selectByPrimaryKey(memberId);
				order.setMemberId(memberId);
				order.setMemberNick(member.getMemberNick());
				Goods goods = goodsService.selectByPrimaryKey(goodsId);
				double amountPrice = goods.getPrice()*amount;
				BigDecimal totalPrice = new BigDecimal(amountPrice);
				Integer storeId = new Integer(order.getStoreId().intValue());
				Store store = storeService.selectByPrimaryKey(storeId);
				order.setDiscountValue(amountPrice-order.getTotalAmount().doubleValue()+store.getDeliverFee());//减免金额
				order.setTotalAmount(order.getTotalAmount());
				mapper.insertSelective(order);
				// 生成订单详情
				OrderGoods orderGoods = new OrderGoods();
				orderGoods.setOrderId(order.getOrderId());
				orderGoods.setGoodsId(goods.getId());
				orderGoods.setGoogsName(goods.getGoodsname());
				orderGoods.setPrice(goods.getPrice());
				orderGoods.setQuantify(amount);
				orderGoods.setPricePay(amountPrice);
				StoreGoods storeGoods = goodsService.getStoreGoodsByGoodsIdAndStoreId(goodsId,storeId);
				orderGoods.setStoreGoodsId(storeGoods.getId());
				orderGoodsService.insertSelective(orderGoods);
				if(voucherId!=null){
					//使用了代金券
					MemberVoucher memberVoucher = new MemberVoucher();
					memberVoucher.setId(voucherId);
					memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_USED);
					memberVoucherMapper.updateByPrimaryKeySelective(memberVoucher);
				}
				// 生成订单清空购物车
				shopCarService.deleteByMemberIdStoreId(memberId.longValue(), order.getStoreId().longValue());
				return order;
	}
	/**
	 * 创建订单并添加订单详情
	 * @param order 提交的订单
	 * @param memberId 会员Id
	 * @param activity 系统活动
	 * @param bookType 预定时间
	 * @return
	 */
	private SysOrder createOrderAndDetail(SysOrder order, Integer memberId,String bookTime,SysActivity activity,Long voucherId,String weeknum) {
		Date date = new Date(); 
//		//预定类型
//		order.setOrderType(bookType);
		//预定时间
		order.setBookTime(DateUtils.smartFormat(bookTime));
		//下单时间
		order.setOrderTime(date);
		//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成
		order.setOrderStatus("0");
		//支付状态 0.未支付 1支付成功  2支付失败  3退款中 4 已退款 5退款失败
		order.setPayStatus(Constants.ORDER_STATUS_UNPAY);
		// 0未支付，1支付宝，2微信支付，3现金支付，4余额支付
		order.setPayType("0");
		Integer storeId = new Integer(order.getStoreId().intValue());
		Store store = storeService.selectByPrimaryKey(storeId);
		//商品总价
		ConditionModel condition = new ConditionModel();
		condition.getParams().put("memberId", memberId);
		condition.getParams().put("weeknum", weeknum);
		condition.getParams().put("storeId", order.getStoreId());
		double totalPrice = shopTrolleyService.amountPriceByWeeknumAndStoreId(condition);
		order.setDiscountValue(totalPrice-order.getTotalAmount().doubleValue()+store.getDeliverFee());//减免金额
		order.setOutsideMoney(store.getDeliverFee());//外卖费
/*
		double amountPrice = shopTrolleyService.amountPriceByWeeknum(condition);
		//优惠值,包含了积分转化的部分
		BigDecimal totalPrice = new BigDecimal(amountPrice);
		SysParm sysParam = getRealSysParam();
		if(sysParam!=null){
			//配送费
			BigDecimal deliverPrice = sysParam.getDeliverPrice();
			order.setOutsideMoney(deliverPrice.doubleValue());
			totalPrice = totalPrice.add(deliverPrice);
		}
		BigDecimal ouponValue = new BigDecimal(order.getOuponValue()==null?0:order.getOuponValue());
		totalPrice=totalPrice.subtract(ouponValue);
		order.setTotalAmount(totalPrice);*/
		// 设置交易流水号 
		order.setPayNumber(BizHelper.getPayNo());
		Member member = memberService.selectByPrimaryKey(memberId);
		order.setMemberId(memberId);
		order.setMemberNick(member.getMemberNick());
		mapper.insertSelective(order);
		// 生成订单详情
		
		List<ShopTrolleyVo> cartList = shopTrolleyService.selectByMemberIdAndStoreIdAndWeek(memberId, storeId, Integer.parseInt(weeknum));
		for (ShopTrolleyVo vo : cartList) {
			OrderGoods orderGoods = new OrderGoods();
			orderGoods.setOrderId(order.getOrderId());
			orderGoods.setGoodsId(vo.getGoodsId());
			orderGoods.setGoogsName(vo.getGoodsName());
			orderGoods.setPrice(vo.getUnitPrice());
			orderGoods.setQuantify(vo.getNum());
			orderGoods.setPricePay(vo.getUnitPrice()*vo.getNum());
			orderGoods.setStoreGoodsId(vo.getStoreGoodsId());
			orderGoodsService.insertSelective(orderGoods);
		}
		if(voucherId!=null){
			//删除使用了代金券
			MemberVoucher memberVoucher = new MemberVoucher();
			memberVoucher.setId(voucherId);
			memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_USED);
			memberVoucherMapper.updateByPrimaryKeySelective(memberVoucher);
		}
		// 生成订单清空购物车
		shopTrolleyService.deleteByMemberIdCategory(condition);
		return order;
	}
	@Override
	@Transactional
	public SysOrder createSingleOrderAndDetail(SysOrder order, Integer memberId, String bookTime, Long goodsId,
			Integer amount,Long voucherId,String weeknum) {
		Date date = new Date(); 
		//预定类型
//		order.setOrderType(bookType);
		//预定时间
		order.setBookTime(DateUtils.smartFormat(bookTime));
		//下单时间
		order.setOrderTime(date);
		//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成
		order.setOrderStatus("0");
		//支付状态 0.未支付 1支付成功  2支付失败  3退款中 4 已退款 5退款失败
		order.setPayStatus(Constants.ORDER_STATUS_UNPAY);
		// 0未支付，1支付宝，2微信支付，3现金支付，4余额支付
		order.setPayType("0");
		//商品总价
		Goods goods = goodsService.selectByPrimaryKey(goodsId);
		double amountPrice = goods.getPrice()*amount;
		BigDecimal totalPrice = new BigDecimal(amountPrice);
		Integer storeId = new Integer(order.getStoreId().intValue());
		Store store = storeService.selectByPrimaryKey(storeId);
		order.setDiscountValue(amountPrice-order.getTotalAmount().doubleValue()+store.getDeliverFee());//减免金额
		order.setOutsideMoney(store.getDeliverFee());//外卖费
		order.setTotalAmount(order.getTotalAmount());
		// 设置交易流水号 
		order.setPayNumber(BizHelper.getPayNo());
		Member member = memberService.selectByPrimaryKey(memberId);
		order.setMemberId(memberId);
		order.setMemberNick(member.getMemberNick());
		mapper.insertSelective(order);
		// 生成订单详情
		OrderGoods orderGoods = new OrderGoods();
		orderGoods.setOrderId(order.getOrderId());
		orderGoods.setGoodsId(goods.getId());
		orderGoods.setGoogsName(goods.getGoodsname());
		orderGoods.setPrice(goods.getPrice());
		orderGoods.setQuantify(amount);
		orderGoods.setPricePay(amountPrice);
		StoreGoods storeGoods = goodsService.getStoreGoodsByGoodsIdAndStoreId(goodsId,storeId);
		orderGoods.setStoreGoodsId(storeGoods.getId());
		orderGoodsService.insertSelective(orderGoods);
		//删除使用过的代金卷
		if(voucherId!=null){
			//删除使用了代金券
			MemberVoucher memberVoucher = new MemberVoucher();
			memberVoucher.setId(voucherId);
			memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_USED);
			memberVoucherMapper.updateByPrimaryKeySelective(memberVoucher);
		}
		return order;
	}
	/**
	 * 获取系统参数，默认系统参数只有一种
	 * @return
	 */
	private SysParm getRealSysParam(){
		List<SysParm> sysParams = sysParmService.selectAll();
		SysParm sysParm = null;
		if(sysParams!=null&&!sysParams.isEmpty()){
			sysParm=sysParams.get(0);
		}
		return sysParm;
	}
	@Override
	public SysOrder createOrderAndDetail(SysOrder order, Integer memberId,String bookTime,Long voucherId,String weeknum) {
		return createOrderAndDetail(order,memberId,bookTime,null,voucherId,weeknum);
	}

	@Override
	public List<SysOrder> queryPage(Page<SysOrder> page) {
		return mapper.queryPage(page);
	}

	@Override
	public int updateOrderStatus2(SysOrder sysOrder) {
		return mapper.updateOrderStatus2(sysOrder);
	}

	@Override
	public SysOrder queryOrderById2(Long orderId) {
		return mapper.queryOrderById2(orderId);
	}

	@Override
	public String confirmOrder(Long orderId) {
		String message = "";
		SysOrder order = mapper.queryOrderById2(orderId);
		if ( "0".equals(order.getOrderStatus() )) {
			order.setOrderStatus("1");//已确认
			mapper.updateOrderStatus2(order);//更新一下数据库
			message = "success";
		} else {
			message = "fail";
		}
		return message;
	}

	@Override
	public int statDealNum(Long storeId) {
		return mapper.statDealNum(storeId);
	}

	@Override
	public BigDecimal statDealSumAmount(Long storeId) {
		return mapper.statDealSumAmount(storeId);
	}

	@Override
	public int statDealNumYesterday(Long storeId) {
		return mapper.statDealNumYesterday(storeId);
	}

	@Override
	public BigDecimal statDealSumAmountYesterday(Long storeId) {
		return mapper.statDealSumAmountYesterday(storeId);
	}

	@Override
	public SysOrder selectByPayNumber(String payno) {
		return mapper.selectByPayNumber(payno);
	}

	@Override
	public String notifyAliJs(HttpServletRequest request) {
		String out_trade_no = request.getParameter("out_trade_no");
//		String trade_no = request.getParameter("trade_no");
		String result_code = request.getParameter("trade_status");
		String total_fee = request.getParameter("total_fee");
		String gmt_payment = request.getParameter("gmt_payment");
//		Member member = (com.deyi.entity.Member) request.getSession().getAttribute(Constants.SESSION_MEMBER);
		SysOrder order = selectByPayNumber(out_trade_no);
		if(order != null && Constants.ORDER_STATUS_UNPAY.equals(order.getPayStatus())){
			if("TRADE_FINISHED".equalsIgnoreCase(result_code)){
				//交易结束，或能退款的订单支付成功。暂不处理
				//重复通知时不处理
//				LOG.info("重复通知不处理...");
				
			}else if("TRADE_SUCCESS".equalsIgnoreCase(result_code)) {// 交易状态

				// 支付宝订单完成
				//3、显示成功提示音
			    //调用Dwr推
				SendMsg.sendMsg(order,servletContext,request);
				//更新订单状态
				SysOrder sysOrder = new SysOrder();
				sysOrder.setOrderId(order.getOrderId());
				sysOrder.setPayStatus(Constants.ORDER_STATUS_PAIED);
				sysOrder.setPayTime(new Date());
				sysOrder.setPayType(Constants.ORDER_PAY_TYPE_ALIPAY);
				//支付宝返回的金额单位是元，订单中单位为元
				sysOrder.setPayAmount(new BigDecimal(total_fee));
				updateByPrimaryKeySelective(sysOrder);
				
				//更新菜品销售数量
				List<OrderGoods> orderGoodsList = orderGoodsService.selectByOrderId(order.getOrderId());
				for(OrderGoods orderGoods:orderGoodsList){
					Integer quantify = orderGoods.getQuantify();
					Long goodsId = orderGoods.getGoodsId();
					Goods goods = goodsService.selectByPrimaryKey(goodsId);
					Integer sellnum = goods.getSellnum();
					Goods update = new Goods();
					update.setId(goodsId);
					update.setSellnum(sellnum+quantify);
					goodsService.updateByPrimaryKeySelective(update);
				}
				
				
			}
			return "success";
		}
		return "fail";
	}

	@Override
	public int sureReceive(Long orderId, Member member) {
		SysOrder sysOrder = new SysOrder();
		sysOrder.setOrderId(orderId);
		sysOrder.setOrderStatus(Constants.ORDER_STATUS_SUCCESS);
		int row1 = updateByPrimaryKeySelective(sysOrder);
		//消费积分累计
		SysParm realSysParam = sysParmService.getRealSysParam();
		//消费积分比例
		String integralProportion = realSysParam.getIntegralProportion();
		String[] split = integralProportion.split(",");
		SysOrder order = selectByPrimaryKey(orderId);
		int integral=0;
		if(order!=null){
			String orderType = order.getOrderType();
			if(Constants.ORDER_TYPE_BOOK2.equals(orderType)){
				//如果是预定类型，发放代金券
				List<Voucher> voucherList = voucherMapper.selectSatisfyBookVoucher(order.getTotalAmount());
				if(voucherList!=null&&!voucherList.isEmpty()){
					Voucher voucher = voucherList.get(0);
					MemberVoucher memberVoucher = new MemberVoucher();
					memberVoucher.setCreateTime(new Date());
					memberVoucher.setMemberId(member.getMemberId().longValue());
					memberVoucher.setVoucherId(voucher.getId());
					memberVoucher.setUseStatus(Constants.VOUCHER_STATUS_UNUSED);
					memberVoucherMapper.insertSelective(memberVoucher);
				}
			}
			BigDecimal payAmount = order.getPayAmount();
			if(payAmount==null){
				return -1;
			}
			//根据会员id查询 会员对应等级 积分兑换规则
			MemberLevel memberLevel=memberLevelService.querryByPrimaryKeymemberId(member.getMemberId()+"");
			integral=payAmount.multiply(new BigDecimal(memberLevel.getRule())).intValue();
			if(member!=null){
				//更新积分
				int row2 = memberIntegralService.updateIntegral(member, integral,Constants.MEMBER_INTEGRAL_TYPE_CONSUME);
				//更新消费次数
				Member member1=new Member();
				member1.setConsumeCount(member.getConsumeCount()+1);
				member1.setMemberId(member.getMemberId());
				memberService.updateByPrimaryKeySelective(member1);
				if(row1!=-1&&row2!=-1){
					return 1;
				}
			}
		}
		return -1;
	}

	@Override
	public int saveOrderComment(OrderComment orderComment, Member member) {
		SysOrder order = selectByPrimaryKey(orderComment.getOrderId());
		if (order != null&&!StringUtils.isBlank(String.valueOf(orderComment.getCommentGrade()),String.valueOf(orderComment.getDeliverGrade()))) {
			try {
				int r = orderCommentService.saveOrderCommentAndUpdateOrderStatus(order, orderComment);
				if(r==-1){
					return -1;
				}
			} catch (NullPointerException | SQLException e) {
				e.printStackTrace();
				return -1;
			}
			SysParm realSysParam = sysParmService.getRealSysParam();
			//评论每次获取积分
			Double commentOrderEvery = realSysParam.getCommentOrderEvery();
			int row = memberIntegralService.updateIntegral(member,commentOrderEvery.intValue(),Constants.MEMBER_INTEGRAL_TYPE_ADVICE);
			if(row!=-1){
				return 1;
			}
		}
		return -1;
	}

	

	@Override
	public int statOrderStatusEqUntreated() {
		return mapper.statOrderStatusEqUntreated();
	}

	@Override
	public List<SysOrder> storeStat(Page<SysOrder> page) {
		return mapper.storeStat(page);
	}

	@Override
	public List<SysOrder> statOrderNumber(Date querystarttime, Date queryendtime,Long storeId) {
		return mapper.statOrderNumber(querystarttime, queryendtime,storeId);
	}

	




}
