package com.deyi.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;

import org.apache.commons.jexl2.Main;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.entity.Member;
import com.deyi.entity.MemberIntegral;
import com.deyi.model.AccessToken;
import com.deyi.model.WxTempMsg2Keywords;
import com.deyi.model.WxTemplateMsgRequest;
import com.deyi.model.WxUserInfo;
import com.deyi.net.WxApiClient;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberService;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.StringUtils;
import com.google.gson.Gson;

@Service
public class BizService{

	private Logger LOG = LoggerFactory.getLogger(BizService.class);

	@Autowired
	private WxAccessTokenService wxAccessTokenService;

	@Autowired
	private MemberIntegralService memberIntegralService;
	
	@Autowired
	private MemberService memberService;

	@Autowired
	private WxTemplateMsgService wxTemplateMsgService;
	
	public Member checkAndCreateMember(String wxOpenId, String token, boolean needAuthForUpgrade){
		if(StringUtils.isNotBlank(wxOpenId)){
			if(needAuthForUpgrade){
				return newMember1(wxOpenId,token);
			}else{
				return newMember(wxOpenId);
			}
			
		}
		return null;
	}
	@Transactional
	public Member newMember(String wxOpenId) {
		// 检查用户是否已经是会员，如果是，则不进行下面的操作
		Member member = memberService.selectByWxOpenId(wxOpenId);
		if (member == null) {

			member = new Member();
			AccessToken token = wxAccessTokenService.getAccessToken();
			if (token != null) {
				// 调用微信接口，获取微信用户基本信息
				WxApiClient apiClient = new WxApiClient();
				LOG.info("token="+token.getToken());
				LOG.info("wxOpenId="+wxOpenId);
				WxUserInfo wxUserInfo = apiClient.getUserInfo(token.getToken(), wxOpenId);
				LOG.info("wxUserInfo=" + wxUserInfo);
				if(wxUserInfo == null || StringUtils.isBlank(wxUserInfo.getOpenid())){
					return null;
				}
				// 创建会员信息
				//TODO 下载用户头像
				BizHelper.fillUserInfo(member, wxUserInfo);
				member.setWxOpenId(wxOpenId);
				member = memberService.insertAndReturn(member);
				newMemberIntegral(member);
				// 给用户发送加入成功消息
				WxTemplateMsgRequest<WxTempMsg2Keywords> selfVo = BizHelper.createTemplateMsgForSelfSubscribeRequest(member);
				wxTemplateMsgService.sendTemplateMsgForSelfSubscribe(token.getToken(), selfVo);
			}
		}
		return member;
	}
	@Transactional
	public Member newMember1(String wxOpenId,String token) {
		// 检查用户是否已经是会员，如果是，则不进行下面的操作
		Member member = memberService.selectByWxOpenId(wxOpenId);
		if (member == null) {

			member = new Member();
			AccessToken accessToken = wxAccessTokenService.getAccessToken();
			if (token != null) {
				// 调用微信接口，获取微信用户基本信息
				WxApiClient apiClient = new WxApiClient();
				LOG.info("通过网页授权获取用户信息（getAuthoritiedUserInfo方法）");
				LOG.info("token="+token);
				LOG.info("wxOpenId="+wxOpenId);
				WxUserInfo wxUserInfo = apiClient.getAuthoritiedUserInfo(token, wxOpenId);//用net框架请求
//				WxUserInfo wxUserInfo=getAuthoritiedUserInfo(token, wxOpenId);//自己写的方法请求
				LOG.info("wxUserInfo=" + wxUserInfo);
				if(wxUserInfo == null || StringUtils.isBlank(wxUserInfo.getOpenid())){
					return null;
				}
				// 创建会员信息
				//TODO 下载用户头像
				BizHelper.fillUserInfo(member, wxUserInfo);
				member.setWxOpenId(wxOpenId);
				member = memberService.insertAndReturn(member);
				newMemberIntegral(member);
				// 给用户发送加入成功消息
				WxTemplateMsgRequest<WxTempMsg2Keywords> selfVo = BizHelper.createTemplateMsgForSelfSubscribeRequest(member);
				wxTemplateMsgService.sendTemplateMsgForSelfSubscribe(token, selfVo);
			}
		}
		return member;
	}
	private WxUserInfo getAuthoritiedUserInfo(String token, String wxOpenId) {
		// TODO Auto-generated method stub
		try {
			StringBuilder json = new StringBuilder();
			String str="https://api.weixin.qq.com/sns/userinfo?access_token="+token+"&openid="+wxOpenId+"&lang=zh_CN ";
			System.out.println(str);
			URL url=new URL(str);
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			 conn.setRequestProperty("contentType", "utf-8");  
			 conn.setConnectTimeout(5 * 1000);  
			 conn.setRequestMethod("GET");  
			InputStreamReader isr = new InputStreamReader(conn.getInputStream(), "utf-8");
			BufferedReader in = new BufferedReader(isr);  
			StringBuffer buffer = new StringBuffer();  
			String line = "";  
			while ((line = in.readLine()) != null){  
			  buffer.append(line);  
			}  
			String jsons = buffer.toString();
			
			System.out.println(jsons);
			WxUserInfo wxUserInfo = new Gson().fromJson(jsons, WxUserInfo.class);
			return wxUserInfo;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  null;
		} 
		
	}

	/**
	 * 创建会员的积分记录
	 * @author limh Create on 2016年10月29日 下午12:06:20
	 * @param member
	 */
	public void newMemberIntegral(Member member){
		if(member!=null){
			MemberIntegral memberIntegral = new MemberIntegral();
			memberIntegral.setIntegral(0);
			memberIntegral.setIntegralTime(new Date());
			memberIntegral.setMemberId(member.getMemberId());
			memberIntegral.setMemberNick(member.getMemberNick());
			memberIntegral.setRemarks("会员注册！");
			memberIntegralService.insertSelective(memberIntegral);
		}
	}
	
	
}
