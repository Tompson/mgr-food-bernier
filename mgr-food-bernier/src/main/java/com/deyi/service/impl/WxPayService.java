package com.deyi.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ServletContextAware;

import com.deyi.dao.GoodsMapper;
import com.deyi.dao.OrderGoodsMapper;
import com.deyi.dao.SysOrderMapper;
import com.deyi.entity.Goods;
import com.deyi.entity.OrderGoods;
import com.deyi.entity.SysOrder;
import com.deyi.model.WxXmlResult;
import com.deyi.service.GoodsService;
import com.deyi.service.IWxPayService;
import com.deyi.service.OrderGoodsService;
import com.deyi.service.SysOrderService;
import com.deyi.util.Constants;
import com.deyi.util.MessageUtil;
import com.deyi.util.StringUtils;
import com.deyi.vo.SendMsg;
import com.thoughtworks.xstream.XStream;

@Service
public class WxPayService implements  IWxPayService,ServletContextAware{//1、首先该类实现ServletContextAware接口
	Logger LOG = LoggerFactory.getLogger(WxPayService.class);
	
	@Autowired
	private SysOrderMapper sysOrderMapper;
	
	private ServletContext servletContext = null;
	
	@Autowired
	private OrderGoodsMapper orderGoodsMapper;
	
	
	@Autowired
	private GoodsMapper goodsMapper;
	

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext=servletContext;
	}
	@Override
	@Transactional
	public String handlePayNotify(HttpServletRequest httpRequest){
		WxXmlResult resultVo = new WxXmlResult();
		resultVo.setReturn_code("FAIL");
		resultVo.setReturn_msg("系统异常");
		try {
			Map<String, String> requestMap = MessageUtil.parseXml(httpRequest);
			LOG.info("微信支付回调参数---->"+requestMap.toString());
			if(requestMap != null){
				if("SUCCESS".equals(requestMap.get("return_code"))){
					//TODO:校验签名
					if("SUCCESS".equals(requestMap.get("result_code"))){
						
						
						String out_trade_no = requestMap.get("out_trade_no");
						if(StringUtils.isNotBlank(out_trade_no)){
							SysOrder order = null;
							order = sysOrderMapper.selectByPayNumber(out_trade_no);
							if(!Constants.ORDER_STATUS_PAIED.equals(order.getPayStatus())){
								// 微信订单完成
								//3、显示成功提示音
								//调用Dwr推
								SendMsg.sendMsg(order,servletContext,httpRequest);
								
								SysOrder sysOrder = new SysOrder();
								sysOrder.setOrderId(order.getOrderId());
								sysOrder.setPayStatus(Constants.ORDER_STATUS_PAIED);
								sysOrder.setPayTime(new Date());
								sysOrder.setPayType(Constants.ORDER_PAY_TYPE_WX);
								String totalFee = requestMap.get("total_fee");
								//微信返回的金额单位是分，订单中单位为元
								sysOrder.setPayAmount(new BigDecimal(totalFee).divide(new BigDecimal(100)));
								sysOrderMapper.updateByPrimaryKeySelective(sysOrder);
								
								//更新菜品销售数量
								List<OrderGoods> orderGoodsList = orderGoodsMapper.selectByOrderId(order.getOrderId());
								for(OrderGoods orderGoods:orderGoodsList){
									Integer quantify = orderGoods.getQuantify();
									Long goodsId = orderGoods.getGoodsId();
									Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
									if(goods!=null){
										Integer sellnum = goods.getSellnum();
										Goods update = new Goods();
										update.setId(goodsId);
										update.setSellnum(sellnum+quantify);
										goodsMapper.updateByPrimaryKeySelective(update);
									}
								}
								
								
							}
							resultVo.setReturn_code("SUCCESS");
							resultVo.setReturn_msg("OK");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("微信支付回调异常:"+e.getMessage());
		}
		XStream XSTREAM2 = new XStream();
		XSTREAM2.alias("xml", WxXmlResult.class);
		String respMsg = XSTREAM2.toXML(resultVo);
		return respMsg;
	}

}
