package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.CarouselFigureDao;
import com.deyi.dao.CategoryGoodsMapper;
import com.deyi.entity.CarouselFigure;
import com.deyi.service.CarouselFigureService;
import com.deyi.util.Page;
import com.deyi.vo.ReturnVo;
@Service
public class CarouselFigureServiceImpl implements CarouselFigureService {
	@Autowired
	private CarouselFigureDao carouselFigureDao;
	
	@Override
	public List<CarouselFigure> queryPage(Page<CarouselFigure> page) {
		// TODO Auto-generated method stub
		return carouselFigureDao.queryPage(page);
	}

	@Override
	public void insertSelective(CarouselFigure carouselFigure) {
		//查询轮播图最大sort
		long maxSort =carouselFigureDao.getMaxSort();
		carouselFigure.setSort(maxSort+1);
		carouselFigureDao.insertSelective(carouselFigure);
	}

	@Override
	public CarouselFigure selectByPrimaryKey(int id) {
		// TODO Auto-generated method stub
		return carouselFigureDao.selectByPrimaryKey(id);
	}

	@Override
	public void updateByPrimaryKeySelective(CarouselFigure carouselFigure) {
		// TODO Auto-generated method stub
		carouselFigureDao.updateByPrimaryKeySelective(carouselFigure);
	}

	@Override
	public void deleteByPrimaryKey(int id) {
		// TODO Auto-generated method stub
		carouselFigureDao.deleteByPrimaryKey(id);
	}

	@Override
	public ReturnVo<Object> moveUp(String id, ReturnVo<Object> vo) {
		// TODO Auto-generated method stub
		long categoryId=Long.parseLong(id);
		CarouselFigure upRecord=carouselFigureDao.getUpRecord(categoryId);
		if(upRecord ==null){//不存在上一个记录
			vo.setStatusCode(1);
			vo.setSuccess(false);
			vo.setMessage("该类别已经是第一个了,请勿重复上移");
			return vo;
		}
		int switchSort = carouselFigureDao.switchSort(categoryId, upRecord.getId());
		System.out.println(switchSort);
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public ReturnVo<Object> moveDown(String id, ReturnVo<Object> vo) {
		// TODO Auto-generated method stub
		long categoryId=Long.parseLong(id);
		CarouselFigure upRecord=carouselFigureDao.getDownRecord(categoryId);
		if(upRecord ==null){//不存在上一个记录
			vo.setStatusCode(1);
			vo.setSuccess(false);
			vo.setMessage("该类别已经是第一个了,请勿重复上移");
			return vo;
		}
		int switchSort = carouselFigureDao.switchSort(categoryId, upRecord.getId());
		System.out.println(switchSort);
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public ReturnVo<Object> toTop(String id, ReturnVo<Object> vo) {
		// TODO Auto-generated method stub
		CarouselFigure category=carouselFigureDao.selectByPrimaryKey(Integer.parseInt(id));
		Long sort = category.getSort();
		CarouselFigure topRecord=carouselFigureDao.getTopRecord();
		Long sort2 = topRecord.getSort();
		if(sort == sort2){//页面点击的排序号和置顶排序号相等
			vo.setMessage("该类别已经置顶,请勿重复置顶");
			vo.setSuccess(false);
			vo.setStatusCode(1);
			return vo;
		}
		CarouselFigure category2=new CarouselFigure();
		category2.setId(category.getId());
		category2.setSort(sort2+1);
		carouselFigureDao.updateByPrimaryKeySelective(category2);
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public List<CarouselFigure> selectTopFiveCarouselFigure() {
		// TODO Auto-generated method stub
		return carouselFigureDao.selectTopFive();
	}

}
