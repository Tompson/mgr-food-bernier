package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.DeliverManMapper;
import com.deyi.entity.DeliverMan;
import com.deyi.service.DeliverManService;
import com.deyi.util.Page;
@Service
@Transactional
public class DeliverManServiceImpl implements DeliverManService {
	@Autowired
	private DeliverManMapper deliverManMapper;

	@Override
	public int deleteByPrimaryKey(Integer deliverManId) {
		return deliverManMapper.deleteByPrimaryKey(deliverManId);
	}

	@Override
	public int insert(DeliverMan record) {
		return deliverManMapper.insert(record);
	}

	@Override
	public int insertSelective(DeliverMan record) {
		return deliverManMapper.insertSelective(record);
	}

	@Override
	public DeliverMan selectByPrimaryKey(Integer deliverManId) {
		return deliverManMapper.selectByPrimaryKey(deliverManId);
	}

	@Override
	public int updateByPrimaryKeySelective(DeliverMan record) {
		return deliverManMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(DeliverMan record) {
		return deliverManMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<DeliverMan> queryPage(Page<DeliverMan> page) {
		return deliverManMapper.queryPage(page);
	}

	@Override
	public List<DeliverMan> deliverStat(Page<DeliverMan> page) {
		return deliverManMapper.deliverStat(page);
	}

}
