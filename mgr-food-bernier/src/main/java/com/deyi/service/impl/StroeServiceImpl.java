package com.deyi.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.StoreCategoryMapper;
import com.deyi.dao.StoreDao;
import com.deyi.dao.StoreGoodsMapper;
import com.deyi.dao.SysUserMapper;
import com.deyi.entity.Store;
import com.deyi.entity.StoreCategory;
import com.deyi.entity.StoreGoods;
import com.deyi.entity.SysUser;
import com.deyi.service.StoreService;
import com.deyi.util.MD5S;
import com.deyi.util.Page;
@Service
public class StroeServiceImpl implements StoreService {
	@Autowired
	private StoreDao storeDao;
	@Autowired
	private StoreGoodsMapper storeGoodsDao;
	@Autowired 
	private StoreCategoryMapper storeCategoryDao;
	@Autowired 
	private SysUserMapper sysUserMapper;
	@Override
	public List<Store> queryPage(Page<Store> page) {
		// TODO Auto-generated method stub
		return  storeDao.queryPage(page);
	}
	@Override
	public Store selectByPrimaryKey(Integer integer) {
		// TODO Auto-generated method stub
		return storeDao.selectByPrimaryKey(integer);
	}
	@Override
	@Transactional
	public void updateByPrimaryKeySelective(Store store,int flag) {
		
		storeDao.updateByPrimaryKeySelective(store);
		if(flag==1){
			//同步修改管理员表 门店密码
			SysUser sysUser=new  SysUser();
			sysUser.setStoreId(store.getId());
			sysUser.setUserPass(store.getPasswd());
			sysUserMapper.updateByStoreIdSelective(sysUser);
		}
		
	}
	@Override
	public Store getStoreByAccount(String account) {
		// TODO Auto-generated method stub
		return storeDao.selectStoreByAccount(account);
		
	}
	@Override
	@Transactional
	public void sava(Store store) {
		storeDao.sava(store);
		//同步新增门店管理员账号
		SysUser sysUser=new  SysUser();
		sysUser.setCreateTime(new Date());
		sysUser.setPhone(store.getBankPhone());
		sysUser.setStatus("1");
		sysUser.setStoreId(store.getId());
		sysUser.setType("2");
		sysUser.setUserAccount(store.getAccount());
		sysUser.setUserName(store.getStoreName());
		//默认888888
		sysUser.setUserPass(MD5S.GetMD5Code("888888"));
		sysUserMapper.insertSelective(sysUser);
	}
	@Override
	public void deleteStoreById(int id) {
		// TODO Auto-generated method stub
		storeDao.deleteStoreById(id);
	}
	@Override
	public List<String> getGoodsIdListByStoreId(Long id,Long cId) {
		// TODO Auto-generated method stub
		return storeDao.getGoodsIdByStoreId(id,cId);
	}
	@Override
	public List<String> getCategoryListByStoreId(int id) {
		// TODO Auto-generated method stub
		/*return storeCategoryDao.getCategoryListByStoreId(id);*/
		return storeDao.getCategoryListByStoreId(id);
	}
	@Override
	public void deleteStoreCategorysByStoreId(String id) {
		// TODO Auto-generated method stub
		storeDao.deleteStoreCategorysByStoreId(id);
	}
	@Override
	public void deleteStoreGoodsByStoreId(String id) {
		// TODO Auto-generated method stub
		storeDao.deleteGoodsStoreByStoreId(id);
	}
	@Override
	public void insertStoreCategory(StoreCategory sc) {
		// TODO Auto-generated method stub
		storeDao.insertStoreCategory(sc);
	}
	@Override
	public void insertStoreGoods(StoreGoods sg) {
		// TODO Auto-generated method stub
		storeDao.insertStoreGoods(sg);
	}
	@Override
	public List<Store> queryAllStore() {
		// TODO Auto-generated method stub
		return storeDao.queryAll();
	}
	@Override
	public List<Store> selectStoresByUserLocation(double lat, double lng) {
		// TODO Auto-generated method stub
		return storeDao.selectStoresByUserLocation( lat, lng);
	}
	@Override
	public List<Store> wxgetStoresList(Page<Store> page) {
		// TODO Auto-generated method stub
		return storeDao.wxgetStoresList(page);
	}
	@Override
	@Transactional
	public void savaStore(Store store,SysUser sysUser) {
		storeDao.sava(store);
		//同步新增门店管理员账号
		SysUser sysUser1=new  SysUser();
		sysUser1.setCreateTime(new Date());
		sysUser1.setPhone(store.getBankPhone());
		sysUser1.setStatus("1");
		sysUser1.setStoreId(store.getId());
		//操作人是门店则 添加的为门店普通员工
		if("2".equals(sysUser.getType())){
			sysUser1.setType("3");
			sysUser1.setStoreId(sysUser.getStoreId());
		}else if("1".equals(sysUser.getType())){
			sysUser1.setType("2");
			sysUser1.setStoreId(store.getId());
		}
		sysUser1.setUserAccount(store.getAccount());
		sysUser1.setUserName(store.getStoreName());
		//默认888888
		sysUser1.setUserPass(MD5S.GetMD5Code("888888"));
		sysUserMapper.insertSelective(sysUser1);
	}

}
