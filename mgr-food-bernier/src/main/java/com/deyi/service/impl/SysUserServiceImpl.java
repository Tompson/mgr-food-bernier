package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.SysUserMapper;
import com.deyi.entity.SysUser;
import com.deyi.service.SysUserService;
import com.deyi.util.Page;

/** 
 * @author zjz Create on 2016年10月23日 上午11:03:19 
 */
@Service
@Transactional
public class SysUserServiceImpl implements SysUserService{
	@Autowired 
	private  SysUserMapper sysUserMapper;

	@Override
	public int deleteByPrimaryKey(Integer uid) {
		return sysUserMapper.deleteByPrimaryKey(uid);
	}

	@Override
	public int insert(SysUser record) {
		return sysUserMapper.insert(record);
	}

	@Override
	public int insertSelective(SysUser record) {
		return sysUserMapper.insertSelective(record);
	}

	@Override
	public SysUser selectByPrimaryKey(Integer uid) {
		return sysUserMapper.selectByPrimaryKey(uid);
	}

	@Override
	public int updateByPrimaryKeySelective(SysUser record) {
		return sysUserMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysUser record) {
		return sysUserMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<SysUser> queryPage(Page<SysUser> page) {
		return sysUserMapper.queryPage(page);
	}

	@Override
	public SysUser checkUser(String loginName, String passwd) {
		return sysUserMapper.checkUser(loginName, passwd);
	}

	@Override
	public List<SysUser> checkAccountName(SysUser sysUser) {
		return sysUserMapper.checkAccountName(sysUser);
	}

}
