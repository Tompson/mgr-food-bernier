package com.deyi.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.entity.Member;
import com.deyi.model.WxEvent;
import com.deyi.service.MemberService;
import com.deyi.util.BizHelper;
import com.deyi.util.MessageUtil;
import com.deyi.util.StringUtils;
import com.deyi.util.WxConstants;


@Service
public class WxCallbackService {
	Logger LOG = LoggerFactory.getLogger(WxCallbackService.class);
	
	@Autowired
	private MemberService memberService;
//	
//	@Autowired
//	private WxSceneUsageService wxSceneUsageService;
//	
//	@Autowired
//	private WxAccessTokenService wxAccessTokenService;
	
//	@Autowired
//	private WxTemplateMsgService wxTemplateMsgService;
	
	@Autowired
	private BizService bizService;
	
//	@Autowired
//	private WxEventService wxEventService;
	
	public String handleCallback(HttpServletRequest httpRequest){
		String respMsg = "";
		try {
			Map<String, String> requestMap = MessageUtil.parseXml(httpRequest);
			WxEvent callbackVo = BizHelper.parseCallbackEvent(requestMap);
			if(callbackVo != null){
				if(WxConstants.WX_EVENT_SUBSCRIBE.equalsIgnoreCase(callbackVo.getEvent())){
					respMsg=handleSubscribeEvent(callbackVo);
				}else if(WxConstants.WX_EVENT_UNSUBSCRIBE.equalsIgnoreCase(callbackVo.getEvent())){
					respMsg=handleUnsubscribeEvent(callbackVo);
				}else if(WxConstants.WX_EVENT_SCAN.equalsIgnoreCase(callbackVo.getEvent())){
					respMsg=handleSubscribeEvent(callbackVo);
				}else if(WxConstants.WX_EVENT_VIEW.equalsIgnoreCase(callbackVo.getEvent())){
					
				}else if(WxConstants.WX_EVENT_LOCATION.equalsIgnoreCase(callbackVo.getEvent())){
					respMsg=handleLocationEvent(callbackVo);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
		}
		
		return respMsg;
	}

	private String handleUnsubscribeEvent(WxEvent callbackVo) {
		String respMsg = "";
		if(callbackVo==null || callbackVo.getFromUserName()==null){
			return respMsg;
		}
		//更新member表的sub_status和unsub_time字段
		Member member = memberService.selectByWxOpenId(callbackVo.getFromUserName());
		if(member != null){
			Member updating = new Member();
			updating.setMemberId(member.getMemberId());
			updating.setSubStatus(WxConstants.T_WX_UNSUBSCRIBE_STATUS);
			updating.setUnsubTime(new Date());
			memberService.updateByPrimaryKeySelective(updating);
		}
		
		return respMsg;
	}
	
	private String handleLocationEvent(WxEvent callbackVo) {
		String respMsg = "";
		//更新member表的sub_status和unsub_time字段
		Member member = memberService.selectByWxOpenId(callbackVo.getFromUserName());
		if(member != null){
			Member updating = new Member();
			updating.setMemberId(member.getMemberId());
			if(StringUtils.isNotBlank(callbackVo.getLatitude())){
				updating.setGeoLat(new BigDecimal(callbackVo.getLatitude()));
			}
			if(StringUtils.isNotBlank(callbackVo.getLongitude())){
				updating.setGeoLng(new BigDecimal(callbackVo.getLongitude()));
			}
			if(StringUtils.isNotBlank(callbackVo.getPrecision())){
				updating.setGeoPrec(new BigDecimal(callbackVo.getPrecision()));
			}
			updating.setGeoTime(new Date());
			memberService.updateByPrimaryKeySelective(updating);
		}
		
		return respMsg;
	}

	private String handleSubscribeEvent(WxEvent callbackVo){
		String respMsg = "";
		if(callbackVo==null || callbackVo.getFromUserName()==null){
			return respMsg;
		}
		try {
//			wxEventService.checkAndInsert(callbackVo);
			
		} catch (Exception e) {
			LOG.info("handleSubscribeEvent:event=" + e.getMessage());
		}
//		bizService.newMember(callbackVo.getFromUserName());
		
		return respMsg;
	}
	
	public static void main(String[] args) {
		WxEvent WxEvent = new WxEvent();
		WxEvent.setFromUserName("");
		WxEvent.setEventKey("");
	}
}
