package com.deyi.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.deyi.model.WxTempMsg2Keywords;
import com.deyi.model.WxTemplateMsgRequest;
import com.deyi.model.WxTemplateMsgResponse;
import com.deyi.net.WxApiClient;
import com.deyi.util.WxConstants;

@Service
public class WxTemplateMsgService {
	
	Logger LOG = LoggerFactory.getLogger(WxTemplateMsgService.class);
/*	public boolean sendTemplateMsgForSubscribe(String accessToken, WxTemplateMsgRequest<WxTempMsg3Keywords> requestVo){
		WxApiClient apiClient = new WxApiClient();
		try {
			WxTemplateMsgResponse respVo = apiClient.sendTemplateMsgForSubscribe(accessToken, requestVo);
			if(respVo != null && respVo.getErrcode() == WxConstants.T_WX_ERRCODE_SUCC){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}*/
	public boolean sendTemplateMsgForSelfSubscribe(String accessToken, WxTemplateMsgRequest<WxTempMsg2Keywords> requestVo){
		WxApiClient apiClient = new WxApiClient();
		try {
			WxTemplateMsgResponse respVo = apiClient.sendTemplateMsgForSelfSubscribe(accessToken, requestVo);
			if(respVo != null && respVo.getErrcode() == WxConstants.T_WX_ERRCODE_SUCC){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
