package com.deyi.service.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.OrderCommentMapper;
import com.deyi.entity.Chief;
import com.deyi.entity.ChiefComment;
import com.deyi.entity.OrderComment;
import com.deyi.entity.SysOrder;
import com.deyi.model.vo.OrderCommentVo;
import com.deyi.service.ChiefCommentService;
import com.deyi.service.ChiefService;
import com.deyi.service.OrderCommentService;
import com.deyi.service.SysOrderService;
import com.deyi.util.Constants;
import com.deyi.util.Page;
import com.dy.util.StringUtil;
@Service
@Transactional
public class OrderCommentServiceImpl implements OrderCommentService {
	@Autowired
	private OrderCommentMapper mapper;
	
	@Autowired
	private SysOrderService sysOrderService;
	@Autowired
	private ChiefService chiefService;
	@Autowired
	private ChiefCommentService chiefCommentService;
	@Override
	public int deleteByPrimaryKey(Long cid) {
		return mapper.deleteByPrimaryKey(cid);
	}

	@Override
	public int insert(OrderComment record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(OrderComment record) {
		return mapper.insertSelective(record);
	}

	@Override
	public OrderComment selectByPrimaryKey(Long cid) {
		return mapper.selectByPrimaryKey(cid);
	}

	@Override
	public int updateByPrimaryKeySelective(OrderComment record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(OrderComment record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int saveOrderCommentAndUpdateOrderStatus(SysOrder order, OrderComment orderComment) throws NullPointerException,SQLException{
		int res = checkOrderCommentIsUniqueness(order.getOrderId());
		if(res<1){
			orderComment.setOrderId(order.getOrderId());
			orderComment.setMemberId(order.getMemberId());
			orderComment.setMemberNick(order.getMemberNick());
			if(order.getDeliverManId()!=null){
				orderComment.setDeliverManId(order.getDeliverManId());
				orderComment.setDeliverSdate(new Date());
			}
			Integer chiefId = new Integer(order.getChiefId().intValue());
			
			orderComment.setChiefId(chiefId);
			orderComment.setReplynum(getMaxReplynum());
			Date date = new Date();
			orderComment.setSdate(date);
			
			orderComment.setChiefSdate(date);
			int row = insertSelective(orderComment);
			//排序id，评论置顶使用
			OrderComment update = new OrderComment();
			update.setCid(orderComment.getCid());
			update.setParentId(orderComment.getCid());
			updateByPrimaryKeySelective(update);
			//增加厨师评论
			ChiefComment cf=new ChiefComment();
			cf.setOrderId(order.getOrderId());
			cf.setChiefGrade(orderComment.getChiefGrade());
			cf.setChiefManId(chiefId);
			cf.setChiefSdate(date);
			Chief chief = chiefService.selectByPrimaryKey(chiefId);
			cf.setChiefManName(chief.getName());
			cf.setContent(orderComment.getChiefContent());
			cf.setMemberId(order.getMemberId());
			cf.setMemberNick(order.getMemberNick());
			cf.setReplynum(orderComment.getCid());
			chiefCommentService.insert(cf);
			//排序id，评论置顶使用
			if(row!=-1){
				SysOrder sysOrder = new SysOrder();
				sysOrder.setOrderId(order.getOrderId());
				sysOrder.setOrderStatus(Constants.ORDER_STATUS_ADVISED);
				sysOrderService.updateByPrimaryKeySelective(sysOrder);
			}
			return 1;
		}
		return -1;
	}

	@Override
	public List<OrderCommentVo> selectGoodsCommemtByGoodsId(Page<OrderCommentVo> page) {
		return mapper.selectGoodsCommemtByGoodsId(page);
	}

	@Override
	public List<OrderComment> queryPage(Page<OrderComment> page) {
		return mapper.queryPage(page);
	}

	@Override
	public OrderComment sortingTop() {
		return mapper.sortingTop();
	}

	@Override
	public OrderComment moveOrderComment(Long parentId) {
		return mapper.moveOrderComment(parentId);
	}

	@Override
	public int checkOrderCommentIsUniqueness(Long orderId) {
		return mapper.checkOrderCommentIsUniqueness(orderId);
	}
	
	private long getMaxReplynum(){
		long max = mapper.selectMaxReplynum();
		return max+1;
	}

	@Override
	public OrderComment sortingTop2() {
		return mapper.sortingTop2();
	}

	@Override
	public OrderComment moveOrderComment2(Long parentId) {
		return mapper.moveOrderComment2(parentId);
	}
	private long getMinReplynum(){
		long min = mapper.selectMinReplynum();
		return min-1;
	}

	@Override
	public OrderComment sortingTop3() {
		return mapper.sortingTop3();
	}

	@Override
	public OrderComment moveOrderComment3(Long parentId) {
		return mapper.moveOrderComment3(parentId);
	}

	@Override
	public List<OrderComment> queryPageByDeliver(Page<OrderComment> page) {
		return mapper.queryPageByDeliver(page);
	}
}
