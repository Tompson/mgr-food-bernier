package com.deyi.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.SysNumberOnedayMapper;
import com.deyi.entity.SysNumberOneday;

/**
 * 获取每日唯一编号
 * @author zjz Create on 2016年11月21日 下午12:33:26
 */
@Service 
public class SysNumberOndayServiceImpl  {
	private static Logger log = LoggerFactory.getLogger(SysNumberOndayServiceImpl.class);
	@Autowired
	private SysNumberOnedayMapper sysNumberOnedayMapper;
	

	private int insert(SysNumberOneday sysNumberOneday){
		return sysNumberOnedayMapper.insertSelective(sysNumberOneday);
	}
	
	private int update(SysNumberOneday sysNumberOneday){
		return sysNumberOnedayMapper.updateByPrimaryKey(sysNumberOneday);
	}
	
	private SysNumberOneday select(SysNumberOneday qSysNumberOneday){
		SysNumberOneday selectByNumberKey = sysNumberOnedayMapper.selectForKey(qSysNumberOneday);
		return selectByNumberKey;
	}
	
	
	/**获取每日唯一编号
	 * @param qSysNumberOneday
	 * @return String value
	 */
	public synchronized String getValue(SysNumberOneday qSysNumberOneday){
		log.info("获取每日编号开始...... ");
		String returnvalue=null;
		SysNumberOneday sysNumberOneday = this.select(qSysNumberOneday);
		
		if(sysNumberOneday==null){//若没有结果，新增一条(相当于默认值)
			qSysNumberOneday.setNumberValue("1");
			this.insert(qSysNumberOneday);
			returnvalue = this.select(qSysNumberOneday).getNumberValue();
		}else{//若有结果,先更新,再查,再返回
			sysNumberOneday.setNumberValue(String.valueOf(Integer.parseInt(sysNumberOneday.getNumberValue())+1));
			update(sysNumberOneday);
			returnvalue = this.select(qSysNumberOneday).getNumberValue();
//			returnvalue = sysNumberOneday.getNumberValue();
		}
		log.info("获取每日编号结束...... value="+returnvalue);
		return returnvalue;
	}
	
	
}
