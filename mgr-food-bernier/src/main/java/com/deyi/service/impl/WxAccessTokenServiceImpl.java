package com.deyi.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.WxAccessTokenMapper;
import com.deyi.entity.WxAccessToken;
import com.deyi.model.AccessToken;
import com.deyi.service.WxAccessTokenService;
import com.deyi.util.BizHelper;
import com.deyi.util.WxConstants;
import com.deyi.util.WxUtil;

import net.sf.json.JSONObject;

@Service
@Transactional
public class WxAccessTokenServiceImpl implements WxAccessTokenService{
	private static Logger LOG = LoggerFactory.getLogger(WxAccessTokenServiceImpl.class);
	@Autowired
	private WxAccessTokenMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(WxAccessToken record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(WxAccessToken record) {
		return mapper.insertSelective(record);
	}

	@Override
	public WxAccessToken selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(WxAccessToken record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(WxAccessToken record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<WxAccessToken> selectAll() {
		return mapper.selectAll();
	}

	@Override
	public AccessToken getAccessToken() {
		String appid = BizHelper.getWxAppId();
    	String appsecret = BizHelper.getWxAppSecret();
    	String requestUrl = String.format(WxConstants.WX_ACCESS_TOKEN_URL, appid, appsecret);
    	WxAccessToken accessTocken = getRealAccessToken();
    	if(accessTocken!=null){
    		java.util.Date end = new java.util.Date();
    		java.util.Date start = accessTocken.getCreateTime();
    		long remaining = end.getTime()-start.getTime();
        	if(remaining>(accessTocken.getExpiresTime()*1000/2)){
        		LOG.info("remaining=" + remaining);
        		 AccessToken accessToken = null;
                 JSONObject jsonObject = WxUtil.httpRequest(requestUrl, "GET", null);
                 // 如果请求成功
                 if (null != jsonObject) {
                     try {
                         accessToken = new AccessToken();
                         accessToken.setToken(jsonObject.getString("access_token"));
                         accessToken.setExpiresIn(jsonObject.getInt("expires_in"));
                         //凭证过期更新凭证
                         WxAccessToken atyw = new WxAccessToken();
                         atyw.setId(accessTocken.getId());
                         atyw.setExpiresTime(jsonObject.getInt("expires_in"));
                         atyw.setAccessToken(jsonObject.getString("access_token"));
                         atyw.setCreateTime(new Date());
                         updateByPrimaryKeySelective(atyw);
                     } catch (Exception e) {
                         accessToken = null;
                         // 获取token失败
                         String wrongMessage = "获取token失败 errcode:{} errmsg:{}"+jsonObject.getInt("errcode")+jsonObject.getString("errmsg");
                         LOG.info(wrongMessage);
                     }
                 }
                 return accessToken;
        	}else{
        		 AccessToken  accessToken = new AccessToken();
                 accessToken.setToken(accessTocken.getAccessToken());
                 accessToken.setExpiresIn(accessTocken.getExpiresTime());
        		return accessToken;
        	}
    	}else{
    		
    		 AccessToken accessToken = null;
             JSONObject jsonObject = WxUtil.httpRequest(requestUrl, "GET", null);
             // 如果请求成功
             if (null != jsonObject) {
                 try {
                     accessToken = new AccessToken();
                     accessToken.setToken(jsonObject.getString("access_token"));
                     accessToken.setExpiresIn(jsonObject.getInt("expires_in"));
                     
                     WxAccessToken wxToken = new WxAccessToken();
                     wxToken.setExpiresTime(jsonObject.getInt("expires_in"));
                     wxToken.setAccessToken(jsonObject.getString("access_token"));
                     wxToken.setId("1");
                     wxToken.setCreateTime(new Date());
                     insertSelective(wxToken);
                     
                 } catch (Exception e) {
                     accessToken = null;
                     // 获取token失败
                     String wrongMessage = "获取token失败 errcode:{} errmsg:{}"+jsonObject.getInt("errcode")+jsonObject.getString("errmsg");
                     LOG.info(wrongMessage);
                 }
             }
             return accessToken;
    	}
	}
	
	private WxAccessToken getRealAccessToken(){
    	List<WxAccessToken> accessTokens = selectAll();
    	if(accessTokens != null && accessTokens.size() > 0){
    		return accessTokens.get(0);
    	}
    	return null;
    }

}
