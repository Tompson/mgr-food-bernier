package com.deyi.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.SysCouponMapper;
import com.deyi.entity.Member;
import com.deyi.entity.MemberVoucher;
import com.deyi.entity.SysCoupon;
import com.deyi.service.MemberVoucherService;
import com.deyi.service.SysCouponService;
import com.deyi.util.Page;
@Service
public class SysCouponServiceImpl implements SysCouponService {
	@Autowired
	private SysCouponMapper sysCouponMapper;
	@Autowired 
	private MemberVoucherService memberVoucherService;
	@Override
	public int deleteByPrimaryKey(Long id) {
		return sysCouponMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysCoupon record) {
		return sysCouponMapper.insert(record);
	}

	@Override
	public int insertSelective(SysCoupon record) {
		return sysCouponMapper.insertSelective(record);
	}

	@Override
	public SysCoupon selectByPrimaryKey(Long id) {
		return sysCouponMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysCoupon record) {
		return sysCouponMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysCoupon record) {
		return sysCouponMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<SysCoupon> querySysCouponPage(Page<SysCoupon> page) {
		return sysCouponMapper.querySysCouponPage(page);
	}

	@Override
	@Transactional
	public void wxgetOneCoupon(Member member,SysCoupon coupon) {
		// TODO Auto-generated method stub
		MemberVoucher mv=new MemberVoucher();
		mv.setCategory("4");//平台代金卷
		mv.setCreateTime(new Date());//领取时间
		mv.setMemberId(IntToLong(member.getMemberId()));//会员id
		mv.setUseStatus("2");//未使用
		mv.setVoucherId(coupon.getId());//平台代金券id
		mv.setIfOverdue("1");//未过期
		memberVoucherService.insertSelective(mv);
		//品台代金券数量减1
		if(coupon.getRemquantity()>1){
			coupon.setRemquantity(coupon.getRemquantity()-1);
			sysCouponMapper.updateByPrimaryKey(coupon);
		}else{
			//如果抢完了，设置状态为结束
			coupon.setRemquantity(coupon.getRemquantity()-1);
			coupon.setStatus("3");
			sysCouponMapper.updateByPrimaryKey(coupon);
		}
		
	}

public static Long IntToLong(Integer x) {
	Long xx = Integer.valueOf(x).longValue() ;
	return xx;
}

@Override
public int startUpdateSysCouponTime(Date time) {
	return sysCouponMapper.startUpdateSysCouponTime(time);
}

}
