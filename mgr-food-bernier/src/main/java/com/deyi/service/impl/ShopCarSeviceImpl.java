package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.ShopCarMapper;
import com.deyi.entity.ShopCar;
import com.deyi.model.vo.ShopCatVo;
import com.deyi.model.vo.ShopTrolleyVo;
import com.deyi.service.ShopCarService;

@Service
public class ShopCarSeviceImpl implements ShopCarService {
	@Autowired
	private ShopCarMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(Long id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ShopCar record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(ShopCar record) {
		return mapper.insertSelective(record);
	}

	@Override
	public ShopCar selectByPrimaryKey(Long id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ShopCar record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ShopCar record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public ShopCatVo statPriceAndNumByMemberIdTableId(Long memberId, Long tableId) {
		return mapper.statPriceAndNumByMemberIdTableId(memberId, tableId);
	}

	@Override
	public List<ShopTrolleyVo> selectByMemberIdTableId(Long memberId, Long tableId) {
		return mapper.selectByMemberIdTableId(memberId, tableId);
	}

	@Override
	public int deleteByMemberIdStoreId(Long memberId, Long storeId) {
		return mapper.deleteByMemberIdStoreId(memberId, storeId);
	}

	@Override
	public int checkIsSelected(Integer memberId, Integer tableId, Long storeGoodsId) {
		// TODO Auto-generated method stub
		return mapper.selectGoodsNum(memberId,tableId,storeGoodsId);
	}

	@Override
	public ShopCar selectShopCarByMemberIdAndTableIdAndStoreGoodsId(Integer memberId, Integer tableId,
			Long storeGoodsId) {
		// TODO Auto-generated method stub
		return mapper.selectShopCarByMemberIdAndTableIdAndStoreGoodsId(memberId, tableId, storeGoodsId);
	}

	@Override
	public ShopTrolleyVo selectByMemberIdStoreGoodsId(long memberid, Long sgid) {
		// TODO Auto-generated method stub
		return mapper.selectByMemberIdStoreGoodsId(memberid,sgid);
	}

}
