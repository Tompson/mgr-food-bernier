package com.deyi.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.OrderGoodsMapper;
import com.deyi.entity.OrderGoods;
import com.deyi.service.OrderGoodsService;

@Service
@Transactional
public class OrderGoodsServiceImpl implements OrderGoodsService {
	@Autowired
	private OrderGoodsMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(Long id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(OrderGoods record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(OrderGoods record) {
		return mapper.insertSelective(record);
	}

	@Override
	public OrderGoods selectByPrimaryKey(Long id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(OrderGoods record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(OrderGoods record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public List<OrderGoods> StatQuantifySum(Long storeId) {
		return mapper.StatQuantifySum(storeId);
	}

	@Override
	public List<OrderGoods> StatPricePaySum(Long storeId) {
		return mapper.StatPricePaySum(storeId);
	}

	@Override
	public Integer StatOrderGoodsSum(Long storeId) {
		return mapper.StatOrderGoodsSum(storeId);
	}

	@Override
	public List<OrderGoods> selectByOrderId(Long orderId) {
		return mapper.selectByOrderId(orderId);
	}

}
