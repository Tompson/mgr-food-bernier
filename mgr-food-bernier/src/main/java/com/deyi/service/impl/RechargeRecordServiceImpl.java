package com.deyi.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.MemberMapper;
import com.deyi.dao.RechargeRecordMapper;
import com.deyi.entity.Member;
import com.deyi.entity.MemberLevel;
import com.deyi.entity.RechargeRecord;
import com.deyi.model.WxXmlResult;
import com.deyi.service.MemberIntegralService;
import com.deyi.service.MemberLevelService;
import com.deyi.service.RechargeRecordService;
import com.deyi.util.Constants;
import com.deyi.util.MessageUtil;
import com.deyi.util.StringUtils;
import com.thoughtworks.xstream.XStream;

@Service
public class RechargeRecordServiceImpl implements RechargeRecordService {
	Logger LOG = LoggerFactory.getLogger(RechargeRecordServiceImpl.class);
	
	@Autowired
	private RechargeRecordMapper mapper;
	
	@Autowired
	private MemberMapper memberMapper;
	@Autowired
	MemberLevelService memberLevelService;
	@Autowired
	MemberIntegralService memberIntegralService;
	
	@Override
	@Transactional
	public String saveRechargeRecordAli(HttpServletRequest request) {
		String out_trade_no = request.getParameter("out_trade_no");
		String trade_no = request.getParameter("trade_no");
		String result_code = request.getParameter("trade_status");
		String total_fee = request.getParameter("total_fee");
		String memberId = request.getParameter("subject");
		
		if("TRADE_FINISHED".equalsIgnoreCase(result_code)){
			//交易结束，或能退款的订单支付成功。暂不处理
			//重复通知时不处理
			
		}else if("TRADE_SUCCESS".equalsIgnoreCase(result_code)) {// 交易状态
			// 支付宝订单完成
			saveRecord(memberId, "2", total_fee, out_trade_no, trade_no);
			return "success";
		}
		return "fail";
	}

	@Override
	@Transactional
	public String saveRechargeRecordWx(HttpServletRequest request) {
		WxXmlResult resultVo = new WxXmlResult();
		resultVo.setReturn_code("FAIL");
		resultVo.setReturn_msg("系统异常");
		try {
			Map<String, String> requestMap = MessageUtil.parseXml(request);
			LOG.info("微信充值回调参数---->"+requestMap.toString());
			String return_code = requestMap.get("return_code");
			String result_code = requestMap.get("result_code");
			String out_trade_no = requestMap.get("out_trade_no");
			String total_fee = requestMap.get("total_fee");//分
			String transaction_id = requestMap.get("transaction_id");
			String wxopenid = requestMap.get("openid");
			if(requestMap != null){
				if("SUCCESS".equals(return_code) && "SUCCESS".equals(result_code)){
					BigDecimal money = new BigDecimal(total_fee);
					total_fee = StringUtils.formatMoney2Byte(money.divide(BigDecimal.valueOf(100),2, BigDecimal.ROUND_HALF_DOWN));
					saveRecord(wxopenid, "1", total_fee, out_trade_no, transaction_id);
					resultVo.setReturn_code("SUCCESS");
					resultVo.setReturn_msg("OK");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("微信支付回调异常:"+e.getMessage());
		}
		XStream XSTREAM2 = new XStream();
		XSTREAM2.alias("xml", WxXmlResult.class);
		String respMsg = XSTREAM2.toXML(resultVo);
		return respMsg;
	}
	
	
	private void saveRecord(String wxopenid,String type,String total_fee,String out_trade_no,String trade_no){
		Member member = memberMapper.selectByWxOpenId(wxopenid);
		BigDecimal money = new BigDecimal(total_fee);
		RechargeRecord rechargeRecord = new RechargeRecord();
		Member member2 = new Member();
		String remark = "";
		List<RechargeRecord> list=mapper.selectBySn(out_trade_no);//防止多次回调
		if(list==null||list.size()<1){
			if("1".equals(type)){
				remark = "余额充值-微信支付";
				rechargeRecord.setMemberId(Long.valueOf(member.getMemberId()));
				member2.setMemberId(member.getMemberId());
				member2.setMoney(money.add(member.getMoney()));
			}else if("2".equals(type)){
				remark = "余额充值-支付宝支付";
				rechargeRecord.setMemberId(Long.valueOf(wxopenid).longValue());
				member2.setMemberId(Integer.parseInt(wxopenid));
				Member member1 = memberMapper.selectByPrimaryKey(member2.getMemberId());
				member2.setMoney(money.add(member1.getMoney()));
			}
			rechargeRecord.setRechargeMoney(money);
			rechargeRecord.setSn(out_trade_no);
			rechargeRecord.setTradeNo(trade_no);
			rechargeRecord.setType(type);//支付类型(1,微信，2支付宝)
			rechargeRecord.setRemark(remark);
			mapper.insertSelective(rechargeRecord);
			LOG.info("更新到会员的充值金额 为========================="+money);
			//更新会员余额
			int res = memberMapper.updateByPrimaryKeySelective(member2);
			//查询当前会员一元兑换多少积分
			//根据会员id查询 会员对应等级 积分兑换规则
			MemberLevel memberLevel=memberLevelService.querryByPrimaryKeymemberId(member.getMemberId()+"");
			int integral=money.multiply(new BigDecimal(memberLevel.getRule())).intValue();
			/**
			 * 升级
			 */
			memberIntegralService.updateIntegral(member, integral, Constants.MEMBER_INTEGRAL_TYPE_CONSUME);
		}
	}
		

	@Override
	public List<RechargeRecord> statRechargeNumber(Date querystarttime, Date queryendtime) {
		return mapper.statRechargeNumber(querystarttime, queryendtime);
	}
	
}
