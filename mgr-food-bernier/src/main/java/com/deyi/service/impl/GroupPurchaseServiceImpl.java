package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.GroupPurchaseMapper;
import com.deyi.entity.GroupPurchase;
import com.deyi.service.GroupPurchaseService;
import com.deyi.util.Page;

/** 
 * @author zjz Create on 2016年11月28日 上午10:37:36 
 */
@Service
public class GroupPurchaseServiceImpl implements GroupPurchaseService {
	@Autowired
	private GroupPurchaseMapper groupPurchaseMapper ;

	@Override
	public Integer deleteByPrimaryKey(Long id) throws Exception {
		return groupPurchaseMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Integer insertSelective(GroupPurchase record) throws Exception {
		return groupPurchaseMapper.insertSelective(record);
	}

	@Override
	public GroupPurchase selectByPrimaryKey(Long id) throws Exception {
		return groupPurchaseMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer updateByPrimaryKeySelective(GroupPurchase record) throws Exception {
		return groupPurchaseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<GroupPurchase> queryPage(Page<GroupPurchase> page) {
		return groupPurchaseMapper.queryPage(page);
	}

}
