package com.deyi.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.GoodsMapper;
import com.deyi.dao.StoreGoodsMapper;
import com.deyi.entity.Goods;
import com.deyi.entity.StoreGoods;
import com.deyi.model.vo.GoodsVo;
import com.deyi.service.GoodsService;
import com.deyi.util.Page;
import com.deyi.vo.ReturnVo;

/** 
 * @author zjz Create on 2016年10月21日 下午5:46:54 
 */
@Service
public class GoodsServiceImpl implements GoodsService{
	@Autowired
	private GoodsMapper goodsMapper;
	@Autowired
	private StoreGoodsMapper storeGoodsDao;
	@Override
	@Transactional
	public int deleteByPrimaryKey(Long id) {
		//删除菜品需要同时删除门店 对应的中间表信息
		storeGoodsDao.delGoodsByStoreId(id);
		return goodsMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Goods record) {
		return goodsMapper.insert(record);
	}

	@Override
	public int insertSelective(Goods record) {
		return goodsMapper.insertSelective(record);
	}

	@Override
	public Goods selectByPrimaryKey(Long id) {
		return goodsMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Goods record) {
		return goodsMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Goods record) {
		return goodsMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<Goods> queryPage(Page<Goods> page) {//status=0
		return goodsMapper.queryPage(page);
	}

	@Override
	public Goods selectGoodsDetailByPrimaryKey(Long id) {
		return goodsMapper.selectGoodsDetailByPrimaryKey(id);
	}

	@Override
	public List<GoodsVo> selectAvgScore4Goods(Page<GoodsVo> page) {
		return goodsMapper.selectAvgScore4Goods(page);
	}

	@Override
	public Integer StatDealGoodsSum(Long storeId) {
		return goodsMapper.StatDealGoodsSum(storeId);
	}

	@Override
	public List<Goods> queryPageStatGoodsAvgComment(Page<Goods> page) {
		return goodsMapper.queryPageStatGoodsAvgComment(page);
	}

	@Override
	public Goods selectGoodsCommentBygoodsId(Long goodsId) {
		return goodsMapper.selectGoodsCommentBygoodsId(goodsId);
	}

	@Override
	public ReturnVo<Object> toTop(Integer storeId,String id, ReturnVo<Object> vo) {
		//根据门店id  菜品id
		StoreGoods goods = goodsMapper.selectByPrimaryStoreId(storeId,Long.parseLong(id));
		Long sort = goods.getSort(); //我点击的排序号
		//根据门店id  获取排序
		StoreGoods topRecord = goodsMapper.getTopRecord(storeId);
		Long sort2 = topRecord.getSort();//置顶的排序号
		if(sort == sort2){//页面点击的排序号和置顶排序号相等
			vo.setMessage("该菜品已经置顶,请勿重复置顶");
			vo.setSuccess(false);
			vo.setStatusCode(1);
			return vo;
		}
		//替换 序号
		goodsMapper.updateByPrimaryKeyStoreIdGoodsId(topRecord.getSort(),storeId,goods.getGoodsId());
		goodsMapper.updateByPrimaryKeyStoreIdGoodsId(goods.getSort(),topRecord.getStoreId().intValue(),topRecord.getGoodsId());
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public ReturnVo<Object> moveUp(Integer storeId,String id, ReturnVo<Object> vo) {
		long goodsId = Long.parseLong(id);
		//根据门判断 上面还有没有数据
		StoreGoods upRecord = goodsMapper.getUpRecord(storeId,goodsId);
		if(upRecord ==null){//不存在上一个记录
			vo.setStatusCode(1);
			vo.setSuccess(false);
			vo.setMessage("该菜品已是第一个，请勿重复上移");
			return vo;
		}
		//根据门店id  菜品id 获取点击的排序序号
		StoreGoods goods = goodsMapper.selectByPrimaryStoreId(storeId,Long.parseLong(id));
		//替换 sort
		goodsMapper.updateByPrimaryKeyStoreIdGoodsId(goods.getSort(),new Integer(upRecord.getStoreId().intValue()),upRecord.getGoodsId());
		goodsMapper.updateByPrimaryKeyStoreIdGoodsId(upRecord.getSort(),new Integer(goods.getStoreId().intValue()),goods.getGoodsId());
		//int switchSort = goodsMapper.switchSort(goodsId, upRecord.getId());
		//System.out.println(switchSort);
		vo.setSuccess(true);
		vo.setMessage("操作成功");
		return vo;
	}

	@Override
	public long getMaxSort() {
		return goodsMapper.getMaxSort();
	}

	@Override
	public List<Goods> queryAllByCategoryId(Long cId) {
		// TODO Auto-generated method stub
		return goodsMapper.queryAll(cId);
	}

	@Override
	public int updateByPrimaryByStoreIdAndGoodsId(Integer storeId, Long id, String status) {
		return goodsMapper.updateByPrimaryByStoreIdAndGoodsId(storeId, id, status);
	}

	@Override
	public String selectByPrimaryByStoreIdAndGoodsId(Integer storeId, Long id) {
		return goodsMapper.selectByPrimaryByStoreIdAndGoodsId(storeId, id);
	}
	@Override
	public String selectByPrimaryByStoreIdAndGoodsIdSort(Integer storeId, Long id) {
		return goodsMapper.selectByPrimaryByStoreIdAndGoodsIdSort(storeId, id);
	}


	@Override
	public List<Goods> wxgetGoodListByStoreIdAndCategoryId(Integer id, Long categoryId) {
		return goodsMapper.selectGoodsListByCategoryIdAndStoreId(id,categoryId);
	}

	@Override
	public StoreGoods getStoreGoodsByGoodsIdAndStoreId(Long goodsId, Integer storeId) {
		return goodsMapper.selectStoreGoodsByGoodsIdAndStoreId(storeId,goodsId);
	}

	@Override
	public long getMaxSortByStore(Integer storeId) {
		return goodsMapper.getMaxSortByStore(storeId);
	}

	@Override
	public List<Goods> queryPageByStore(Page<Goods> page) {
		return goodsMapper.queryPageByStore(page);
	}


	@Override
	public Goods selectGoodsDetailBySgId(Long sgid) {
		return goodsMapper.selectGoodsDetailBySgId(sgid);
	}

	@Override
	public List<Goods>  selectByPrimaryCategoryId(Long id) {
		return goodsMapper.selectByPrimaryCategoryId(id);
	}

}
