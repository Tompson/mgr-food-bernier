package com.deyi.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.entity.Goods;
import com.deyi.entity.Member;
import com.deyi.entity.Store;
import com.deyi.entity.StoreGoods;
import com.deyi.entity.SysParm;
import com.deyi.service.GoodsService;
import com.deyi.service.IBizService;
import com.deyi.service.StoreService;
import com.deyi.util.BizHelper;
import com.deyi.util.Constants;
import com.deyi.util.DateUtils;
import com.deyi.util.TimeUtil;
import com.dy.util.StringUtil;
@Service
public class BizServiceImpl extends BizBaseServiceAbst implements IBizService {
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private StoreService storeService;
	@Override
	public boolean checkMemberIsForbidden(String wxOpenId) throws Exception{
		Member member = memberMapper.selectByWxOpenId(wxOpenId);
		Integer wxGroupid = member.getWxGroupid();
		if(wxGroupid!=null&&Constants.MEMBER_STATUS_FORBIDDEN==wxGroupid){
			return true;
		}
		return false;
	}

	@Override
	public boolean checkGoodsIsSoldOut(Long goodsId,Integer storeId) throws Exception{
		StoreGoods sg=goodsService.getStoreGoodsByGoodsIdAndStoreId(goodsId,storeId);
		if(sg!=null){
			String status = sg.getStatus();
			if(Constants.GOODS_STATUS_SELLOUT==status){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean checkTimeIsShopHours(String time) throws Exception{
		List<SysParm> sysParm = sysParmMapper.selectAll();
		SysParm realSysParam = sysParm.get(0);
		int now = TimeUtil.getTimeQuantum(DateUtils.parseTime2(time));
		//上午营业时间
		int startTimeAm =TimeUtil.getTimeQuantum(realSysParam.getStartTime());
		int endTimeAm = TimeUtil.getTimeQuantum(realSysParam.getEndTime());
		//下午营业时间
		int startTimePm =TimeUtil.getTimeQuantum(realSysParam.getStartTimePm());
		int endTimePm = TimeUtil.getTimeQuantum(realSysParam.getEndTimePm());
		if(now<startTimeAm||(now>endTimeAm&&now<startTimePm)||now>endTimePm){
			// 不在营业时间内
			return false;
		}
		return true;
	}

	@Override
	public boolean checkNowIsOverEndTime(String currentWeek) throws Exception {
		String week = BizHelper.getConvertedDayOfWeek();
		if(week.equals(currentWeek)){
			//当天
			int now = TimeUtil.getTimeQuantum(new Date());
			List<SysParm> sysParm = sysParmMapper.selectAll();
			SysParm realSysParam = sysParm.get(0);
			int endTimePm = TimeUtil.getTimeQuantum(realSysParam.getEndTimePm());
			if(now>endTimePm){
				//超出营业时间
				return false;
			}
		}
		return false;
	}
	@SuppressWarnings("deprecation")
	@Override
	public boolean checkNowIsOverEndTime1(String weeknum, Integer storeId) {
		// TODO Auto-generated method stub
		try {
			String week = BizHelper.getConvertedDayOfWeek();
			Store store = storeService.selectByPrimaryKey(storeId);
			if(StringUtils.isBlank(store.getCloseTime())||StringUtils.isBlank(store.getOpenTime())){
				return false;
			}
			if(week.equals(weeknum)){
				//当天
				int now = TimeUtil.getTimeQuantum(new Date());
				Date etime = TimeUtil.getDateFromString(store.getCloseTime());
				int endTime = TimeUtil.getTimeQuantum(etime);
				if(etime.getHours()<7){//如果时间超过了晚上12店，那么时间等于24+结束时间
					Date dateFromString = TimeUtil.getDateFromString("23:59");
					int addTime=TimeUtil.getTimeQuantum(dateFromString);
					endTime+=addTime;
				}
				
				Date btime = TimeUtil.getDateFromString(store.getOpenTime());
				int beginTime = TimeUtil.getTimeQuantum(btime);
				if(now<beginTime||now>endTime){
					return true;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
		}
	@Override
	public boolean checkNowIsOverEndTime2(Integer storeId) {
		// TODO Auto-generated method stub
		Store store = storeService.selectByPrimaryKey(storeId);
		int now = TimeUtil.getTimeQuantum(new Date());
		Date etime = TimeUtil.getDateFromString(store.getCloseTime());
		int endTime = TimeUtil.getTimeQuantum(etime);
		if(etime.getHours()<7){//如果时间超过了晚上12店，那么时间等于24+结束时间
			Date dateFromString = TimeUtil.getDateFromString("23:59");
			int addTime=TimeUtil.getTimeQuantum(dateFromString);
			endTime+=addTime;
		}
		Date btime = TimeUtil.getDateFromString(store.getOpenTime());
		int beginTime = TimeUtil.getTimeQuantum(btime);
		if(now<beginTime||now>endTime){
			return true;
		}
		return false;
	}
	@Override
	public boolean checkCurrentWeek(String week) throws Exception {
		String currentWeek = BizHelper.getConvertedDayOfWeek();
		if(currentWeek.equals(week)){
			//当天的星期
			boolean checkNowIsOverEndTime = checkNowIsOverEndTime(currentWeek);
			if(checkNowIsOverEndTime){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean checkIsBookOrder(String time) throws Exception {
		//预定时间
		int bookTime=TimeUtil.getTimeQuantum(DateUtils.parseTime2(time));
		List<SysParm> sysParm = sysParmMapper.selectAll();
		SysParm realSysParam = sysParm.get(0);
		//现在时间
		int now = TimeUtil.getTimeQuantum(new Date());
		//上午营业时间
		int startTimeAm =TimeUtil.getTimeQuantum(realSysParam.getStartTime());
//		int endTimeAm = TimeUtil.getTimeQuantum(realSysParam.getEndTime());
		//下午营业时间
		int startTimePm =TimeUtil.getTimeQuantum(realSysParam.getStartTimePm());
//		int endTimePm = TimeUtil.getTimeQuantum(realSysParam.getEndTimePm());
		if(now<startTimeAm){
			if(bookTime>startTimePm){
				return true;
			}
		}
		return false;
	}

	@Override
	public List<String> getShopTimeList(String week) throws Exception {
		List<SysParm> sysParm = sysParmMapper.selectAll();
		SysParm realSysParam = sysParm.get(0);
		//上午营业时间
		Date startTimeAm = realSysParam.getStartTime();
		Date endTimeAm = realSysParam.getEndTime();
		//下午营业时间
		Date startTimePm = realSysParam.getStartTimePm();
		Date endTimePm = realSysParam.getEndTimePm();
		List<String> list = new ArrayList<>();
		String currentWeek = BizHelper.getConvertedDayOfWeek();
		Date temp=null;
		if(currentWeek.equals(week)){
			//当天
			temp = TimeUtil.getCurrentTime(startTimeAm);
			temp=TimeUtil.getFixedTime(temp);
			boolean checkTimeIsShopHours = checkTimeIsShopHours(DateUtils.formatTime2(temp));
			if(checkTimeIsShopHours){
				//当前时间在营业时间内
				if(temp.before(endTimeAm)){
					//在上午时段内
					while (temp.before(endTimeAm)){
						list.add(DateUtils.formatTime2(temp));
						temp=TimeUtil.addMinute(temp, 15);
					}
					temp=TimeUtil.getFixedTime(startTimePm);
					while (temp.before(endTimePm)){
						list.add(DateUtils.formatTime2(temp));
						temp=TimeUtil.addMinute(temp, 15);
					}
				}else if(temp.before(endTimePm)){
					//下午时间段内
					while (temp.before(endTimePm)){
						list.add(DateUtils.formatTime2(temp));
						temp=TimeUtil.addMinute(temp, 15);
					}
				}
				
			}else{
				//不在营业时间内
				boolean checkNowIsOverEndTime = checkNowIsOverEndTime(DateUtils.formatTime2(temp));
				if(!checkNowIsOverEndTime){
					if(temp.before(startTimeAm)){
						//在上午时段之前
						temp=TimeUtil.getFixedTime(startTimeAm);
						while (temp.before(endTimeAm)){
							list.add(DateUtils.formatTime2(temp));
							temp=TimeUtil.addMinute(temp, 15);
						}
						temp=TimeUtil.getFixedTime(startTimePm);
						while (temp.before(endTimePm)){
							list.add(DateUtils.formatTime2(temp));
							temp=TimeUtil.addMinute(temp, 15);
						}
					}else if(temp.before(startTimePm)){
						//下午时间段之前
						temp=TimeUtil.getFixedTime(startTimePm);
						while (temp.before(endTimePm)){
							list.add(DateUtils.formatTime2(temp));
							temp=TimeUtil.addMinute(temp, 15);
						}
					}
				}
			}
		}else{
			//非当天，可预订所有的
			temp=TimeUtil.getFixedTime(startTimeAm);
			while (temp.before(endTimeAm)){
				list.add(DateUtils.formatTime2(temp));
				temp=TimeUtil.addMinute(temp, 15);
			}
			temp=TimeUtil.getFixedTime(startTimePm);
			while (temp.before(endTimePm)){
				list.add(DateUtils.formatTime2(temp));
				temp=TimeUtil.addMinute(temp, 15);
			}
			
		}
		return list;
	}
	
	@Override
	public boolean checkGoodsIsForbidden(Long goodsId) throws Exception {
		Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
		Integer integral = goods.getIntegral();
		if(Constants.GOODS_STATUS_FORBIDDEN == integral){
			return true;
		}
		return false;
	}

	@Override
	public boolean checkGoodsIsForbidden(Long goodsId,Integer storeId) throws Exception {
		Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
		Integer status = goods.getIntegral();
		if(Constants.GOODS_STATUS_FORBIDDEN==status.intValue()){
			return true;
		}
		return false;
	}

	

	
	
	/*以当前时间为起点按30分钟分段
	 * public List<String> getShopTimeList(String week) throws Exception {
		List<SysParm> sysParm = sysParmMapper.selectAll();
		SysParm realSysParam = sysParm.get(0);
		//上午营业时间
		Date startTimeAm = realSysParam.getStartTime();
		Date endTimeAm = realSysParam.getEndTime();
		//下午营业时间
		Date startTimePm = realSysParam.getStartTimePm();
		Date endTimePm = realSysParam.getEndTimePm();
		List<String> list = new ArrayList<>();
		String currentWeek = BizHelper.getConvertedDayOfWeek();
		Date temp=null;
		if(currentWeek.equals(week)){
			//当天
			temp = TimeUtil.getCurrentTime(startTimeAm);
			boolean checkTimeIsShopHours = checkTimeIsShopHours(DateUtils.formatTime2(temp));
			if(checkTimeIsShopHours){
				//当前时间在营业时间内
				if(temp.before(endTimeAm)){
					//在上午时段内
					while (temp.before(endTimeAm)){
						list.add(DateUtils.formatTime2(temp));
						temp=TimeUtil.addMinute(temp, 30);
					}
					temp=startTimePm;
					while (temp.before(endTimePm)){
						list.add(DateUtils.formatTime2(temp));
						temp=TimeUtil.addMinute(temp, 30);
					}
				}else if(temp.before(endTimePm)){
					//下午时间段内
					while (temp.before(endTimePm)){
						list.add(DateUtils.formatTime2(temp));
						temp=TimeUtil.addMinute(temp, 30);
					}
				}
				
			}else{
				//不在营业时间内
				boolean checkNowIsOverEndTime = checkNowIsOverEndTime(DateUtils.formatTime2(temp));
				if(!checkNowIsOverEndTime){
					if(temp.before(startTimeAm)){
						//在上午时段之前
						temp=startTimeAm;
						while (temp.before(endTimeAm)){
							list.add(DateUtils.formatTime2(temp));
							temp=TimeUtil.addMinute(temp, 30);
						}
						temp=startTimePm;
						while (temp.before(endTimePm)){
							list.add(DateUtils.formatTime2(temp));
							temp=TimeUtil.addMinute(temp, 30);
						}
					}else if(temp.before(startTimePm)){
						//下午时间段之前
						temp=startTimePm;
						while (temp.before(endTimePm)){
							list.add(DateUtils.formatTime2(temp));
							temp=TimeUtil.addMinute(temp, 30);
						}
					}
				}
			}
		}else{
			//非当天，可预订所有的
			temp=startTimeAm;
			while (temp.before(endTimeAm)){
				list.add(DateUtils.formatTime2(temp));
				temp=TimeUtil.addMinute(temp, 30);
			}
			temp=startTimePm;
			while (temp.before(endTimePm)){
				list.add(DateUtils.formatTime2(temp));
				temp=TimeUtil.addMinute(temp, 30);
			}
			
		}
		return list;
	}*/

}
