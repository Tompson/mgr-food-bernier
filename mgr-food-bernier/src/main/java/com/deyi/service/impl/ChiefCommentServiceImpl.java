package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deyi.dao.ChiefCommentMapper;
import com.deyi.entity.ChiefComment;
import com.deyi.service.ChiefCommentService;
import com.deyi.util.Page;
@Service
@Transactional
public class ChiefCommentServiceImpl implements ChiefCommentService {
	@Autowired
	private ChiefCommentMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(Long cid) {
		return mapper.deleteByPrimaryKey(cid);
	}

	@Override
	public int insert(ChiefComment record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(ChiefComment record) {
		return mapper.insertSelective(record);
	}

	@Override
	public ChiefComment selectByPrimaryKey(Long cid) {
		return mapper.selectByPrimaryKey(cid);
	}

	@Override
	public int updateByPrimaryKeySelective(ChiefComment record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ChiefComment record) {
		return mapper.updateByPrimaryKey(record);
	}



	@Override
	public List<ChiefComment> queryPage(Page<ChiefComment> page) {
		return mapper.queryPage(page);
	}

	@Override
	public List<ChiefComment> queryPageByDeliver(Page<ChiefComment> page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChiefComment sortingTop() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChiefComment moveChiefComment(Long parentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChiefComment sortingTop2() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChiefComment moveChiefComment2(Long parentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChiefComment sortingTop3() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChiefComment moveChiefComment3(Long parentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int checkChiefCommentIsUniqueness(Long orderId) {
		// TODO Auto-generated method stub
		return 0;
	}

}
