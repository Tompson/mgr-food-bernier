package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.VoucherMapper;
import com.deyi.entity.Voucher;
import com.deyi.service.VoucherService;
import com.deyi.util.Page;
@Service
public class VoucherServiceImpl implements VoucherService {
	@Autowired
	private VoucherMapper voucherMapper;

	@Override
	public int deleteByPrimaryKey(Long id) {
		return voucherMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Voucher record) {
		return voucherMapper.insert(record);
	}

	@Override
	public int insertSelective(Voucher record) {
		return voucherMapper.insertSelective(record);
	}

	@Override
	public Voucher selectByPrimaryKey(Long id) {
		return voucherMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Voucher record) {
		return voucherMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Voucher record) {
		return voucherMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<Voucher> queryExchangePage(Page<Voucher> page) {
		return voucherMapper.queryExchangePage(page);
	}

	@Override
	public List<Voucher> queryBookPage(Page<Voucher> page) {
		return voucherMapper.queryBookPage(page);
	}

	@Override
	public List<Voucher> queryDrawPage(Page<Voucher> page) {
		return voucherMapper.queryDrawPage(page);
	}

	@Override
	public List<Voucher> selectDrawVoucher() {
		return voucherMapper.selectDrawVoucher();
	}


}
