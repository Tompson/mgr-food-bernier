package com.deyi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deyi.dao.SysParmLinkTimeMapper;
import com.deyi.entity.SysParmLinkTime;
import com.deyi.service.SysParmLinkTimeService;
/**
 * 
 * @author zjz Create on 2016年11月11日 下午2:36:29
 */
@Service
public class SysParmLinkTimeServiceImpl implements SysParmLinkTimeService{

	@Autowired
	private SysParmLinkTimeMapper SysParmLinkTimeMapper;
	@Override 
	public int deleteByPrimaryKey(Long id) {
		return SysParmLinkTimeMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysParmLinkTime record) {
		return SysParmLinkTimeMapper.insert(record);
	}

	@Override
	public int insertSelective(SysParmLinkTime record) {
		return SysParmLinkTimeMapper.insertSelective(record);
	}

	@Override
	public SysParmLinkTime selectByPrimaryKey(Long id) {
		return SysParmLinkTimeMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysParmLinkTime record) {
		return SysParmLinkTimeMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysParmLinkTime record) {
		return SysParmLinkTimeMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<SysParmLinkTime> selectAll() {
		return SysParmLinkTimeMapper.selectAll();
	}


}
