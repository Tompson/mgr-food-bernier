package com.deyi.service;

import java.util.List;

import com.deyi.entity.SysParm;

public interface SysParmService {
    int deleteByPrimaryKey(String id);

    int insert(SysParm record);

    int insertSelective(SysParm record);

    SysParm selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysParm record);

    int updateByPrimaryKey(SysParm record);
    
    List<SysParm> selectAll();
    /**
     * 积分换算成金额，单位元
     * @author limh Create on 2016年10月28日 下午5:57:25
     * @param integral
     * @return double 转换的金额
     */
    double converIntegral(Integer integral);
    /**
     * 获取真实的系统参数
     * @author limh Create on 2016年10月27日 下午3:59:26
     * @return SysParm
     */
    SysParm getRealSysParam();
}
