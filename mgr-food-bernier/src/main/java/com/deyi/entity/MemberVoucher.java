package com.deyi.entity;

import java.util.Date;

public class MemberVoucher {
    private Long id;

	private Long memberId;

	private Long voucherId;

	private Date createTime;

	private Date useTime;

	private String useStatus;

	private String ifOverdue;
	
	private String category;
	
	private Long presenterId;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUseTime() {
		return useTime;
	}

	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}

	public String getUseStatus() {
		return useStatus;
	}

	public void setUseStatus(String useStatus) {
		this.useStatus = useStatus == null ? null : useStatus.trim();
	}

	public String getIfOverdue() {
		return ifOverdue;
	}

	public void setIfOverdue(String ifOverdue) {
		this.ifOverdue = ifOverdue == null ? null : ifOverdue.trim();
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getPresenterId() {
		return presenterId;
	}

	public void setPresenterId(Long presenterId) {
		this.presenterId = presenterId;
	}
}