package com.deyi.entity;

import java.util.Date;

public class SysParmLinkTime {
    private Long id;

    private Long sysParmId;

    private Date startTimePm;

    private Date endTimePm;

    private Date startTimeNight;

    private Date endTimeNight;

    private String imgLinkA;

    private String imgLinkB;

    private String imgLinkC;

    private String imgLinkD;

    private String imgLinkE;
    
    //以下为扩展字段
    private String startTimePm1;
    
    private String endTimePm1;

    public String getStartTimePm1() {
		return startTimePm1;
	}

	public void setStartTimePm1(String startTimePm1) {
		this.startTimePm1 = startTimePm1;
	}

	public String getEndTimePm1() {
		return endTimePm1;
	}

	public void setEndTimePm1(String endTimePm1) {
		this.endTimePm1 = endTimePm1;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSysParmId() {
        return sysParmId;
    }

    public void setSysParmId(Long sysParmId) {
        this.sysParmId = sysParmId;
    }

    public Date getStartTimePm() {
        return startTimePm;
    }

    public void setStartTimePm(Date startTimePm) {
        this.startTimePm = startTimePm;
    }

    public Date getEndTimePm() {
        return endTimePm;
    }

    public void setEndTimePm(Date endTimePm) {
        this.endTimePm = endTimePm;
    }

    public Date getStartTimeNight() {
        return startTimeNight;
    }

    public void setStartTimeNight(Date startTimeNight) {
        this.startTimeNight = startTimeNight;
    }

    public Date getEndTimeNight() {
        return endTimeNight;
    }

    public void setEndTimeNight(Date endTimeNight) {
        this.endTimeNight = endTimeNight;
    }

    public String getImgLinkA() {
        return imgLinkA;
    }

    public void setImgLinkA(String imgLinkA) {
        this.imgLinkA = imgLinkA == null ? null : imgLinkA.trim();
    }

    public String getImgLinkB() {
        return imgLinkB;
    }

    public void setImgLinkB(String imgLinkB) {
        this.imgLinkB = imgLinkB == null ? null : imgLinkB.trim();
    }

    public String getImgLinkC() {
        return imgLinkC;
    }

    public void setImgLinkC(String imgLinkC) {
        this.imgLinkC = imgLinkC == null ? null : imgLinkC.trim();
    }

    public String getImgLinkD() {
        return imgLinkD;
    }

    public void setImgLinkD(String imgLinkD) {
        this.imgLinkD = imgLinkD == null ? null : imgLinkD.trim();
    }

    public String getImgLinkE() {
        return imgLinkE;
    }

    public void setImgLinkE(String imgLinkE) {
        this.imgLinkE = imgLinkE == null ? null : imgLinkE.trim();
    }
}