package com.deyi.entity;

import java.util.Date;

public class SysActivity {
    private Long id;

    private String storeid;

    private String merchantid;

    private String activeType;

    private Double firstDisc;

    private String firstState;

    private Integer lossMinutes;

    private String lossState;

    private String ampleDisc;

    private String ampleState;

    private String prefName;

    private Double prefCmoney;

    private Double prefFmoney;

    private Date prefStartdate;

    private Date prefEnddate;

    private String prefImage;

    private String prefImmed;

    private String prefState;

    private String creator;

    private Date createTime;

    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid == null ? null : storeid.trim();
    }

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid == null ? null : merchantid.trim();
    }

    public String getActiveType() {
        return activeType;
    }

    public void setActiveType(String activeType) {
        this.activeType = activeType == null ? null : activeType.trim();
    }

    public Double getFirstDisc() {
        return firstDisc;
    }

    public void setFirstDisc(Double firstDisc) {
        this.firstDisc = firstDisc;
    }

    public String getFirstState() {
        return firstState;
    }

    public void setFirstState(String firstState) {
        this.firstState = firstState == null ? null : firstState.trim();
    }

    public Integer getLossMinutes() {
        return lossMinutes;
    }

    public void setLossMinutes(Integer lossMinutes) {
        this.lossMinutes = lossMinutes;
    }

    public String getLossState() {
        return lossState;
    }

    public void setLossState(String lossState) {
        this.lossState = lossState == null ? null : lossState.trim();
    }

    public String getAmpleDisc() {
        return ampleDisc;
    }

    public void setAmpleDisc(String ampleDisc) {
        this.ampleDisc = ampleDisc == null ? null : ampleDisc.trim();
    }

    public String getAmpleState() {
        return ampleState;
    }

    public void setAmpleState(String ampleState) {
        this.ampleState = ampleState == null ? null : ampleState.trim();
    }

    public String getPrefName() {
        return prefName;
    }

    public void setPrefName(String prefName) {
        this.prefName = prefName == null ? null : prefName.trim();
    }

    public Double getPrefCmoney() {
        return prefCmoney;
    }

    public void setPrefCmoney(Double prefCmoney) {
        this.prefCmoney = prefCmoney;
    }

    public Double getPrefFmoney() {
        return prefFmoney;
    }

    public void setPrefFmoney(Double prefFmoney) {
        this.prefFmoney = prefFmoney;
    }

    public Date getPrefStartdate() {
        return prefStartdate;
    }

    public void setPrefStartdate(Date prefStartdate) {
        this.prefStartdate = prefStartdate;
    }

    public Date getPrefEnddate() {
        return prefEnddate;
    }

    public void setPrefEnddate(Date prefEnddate) {
        this.prefEnddate = prefEnddate;
    }

    public String getPrefImage() {
        return prefImage;
    }

    public void setPrefImage(String prefImage) {
        this.prefImage = prefImage == null ? null : prefImage.trim();
    }

    public String getPrefImmed() {
        return prefImmed;
    }

    public void setPrefImmed(String prefImmed) {
        this.prefImmed = prefImmed == null ? null : prefImmed.trim();
    }

    public String getPrefState() {
        return prefState;
    }

    public void setPrefState(String prefState) {
        this.prefState = prefState == null ? null : prefState.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}