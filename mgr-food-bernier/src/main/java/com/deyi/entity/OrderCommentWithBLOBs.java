package com.deyi.entity;

public class OrderCommentWithBLOBs extends OrderComment {
    private String replyContent;

    private String deliverContent;

    private String deliverReplyContent;

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent == null ? null : replyContent.trim();
    }

    public String getDeliverContent() {
        return deliverContent;
    }

    public void setDeliverContent(String deliverContent) {
        this.deliverContent = deliverContent == null ? null : deliverContent.trim();
    }

    public String getDeliverReplyContent() {
        return deliverReplyContent;
    }

    public void setDeliverReplyContent(String deliverReplyContent) {
        this.deliverReplyContent = deliverReplyContent == null ? null : deliverReplyContent.trim();
    }
}