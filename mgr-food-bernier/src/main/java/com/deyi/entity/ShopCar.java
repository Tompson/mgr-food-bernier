package com.deyi.entity;

public class ShopCar {
    private Long id;

    private Long memberId;

    private Integer num;

    private Long storeId;

    private Long storeGoodsId;
    private Long tableId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getStoreGoodsId() {
        return storeGoodsId;
    }

    public void setStoreGoodsId(Long storeGoodsId) {
        this.storeGoodsId = storeGoodsId;
    }

	public Long getTableId() {
		return tableId;
	}

	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}

}