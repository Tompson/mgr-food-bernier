package com.deyi.entity;

public class StoreGoods {
	private Long id;
	private Long storeId;
	private Long goodsId;
	private Long categoryId;
	private String status;
	private Long sort;
	private Integer sellnum;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getStoreId() {
		return storeId;
	}
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	public Long getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	public Integer getSellnum() {
		return sellnum;
	}
	public void setSellnum(Integer sellnum) {
		this.sellnum = sellnum;
	}
	
	
}
