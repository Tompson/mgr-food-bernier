package com.deyi.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SysCoupon {
    private Long id;

    private String sn;

    private Double mony;

    private Double usefloor;

    private Integer remquantity;

    private Date starttime;

    private String status;

    private Date creattime;
    private String starttime1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn == null ? null : sn.trim();
    }


    public Double getMony() {
		return mony;
	}

	public void setMony(Double mony) {
		this.mony = mony;
	}


    public Double getUsefloor() {
		return usefloor;
	}

	public void setUsefloor(Double usefloor) {
		this.usefloor = usefloor;
	}

	public Integer getRemquantity() {
        return remquantity;
    }

    public void setRemquantity(Integer remquantity) {
        this.remquantity = remquantity;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getStarttime() {
        return starttime;
    }
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

	public String getStarttime1() {
		return starttime1;
	}

	public void setStarttime1(String starttime1) {
		this.starttime1 = starttime1;
	}
    
}