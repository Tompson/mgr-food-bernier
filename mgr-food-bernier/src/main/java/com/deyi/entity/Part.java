package com.deyi.entity;

import java.math.BigDecimal;

public class Part {
	private Integer id;
	private Integer storeId;
	private String type;
	private BigDecimal full;//满。。钱
	private BigDecimal substact;//减。。钱
	private BigDecimal discount;//折扣
	private String status;
	private String storeName;

	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStoreId() {
		return storeId;
	}
	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BigDecimal getFull() {
		return full;
	}
	public void setFull(BigDecimal full) {
		this.full = full;
	}
	public BigDecimal getSubstact() {
		return substact;
	}
	public void setSubstact(BigDecimal substact) {
		this.substact = substact;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
