package com.deyi.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class OrderComment {
    private Long cid;

    private Long orderId;

    private Integer deliverManId;
    private Integer chiefId;
    private Integer memberId;

    private String memberNick;

    private String content;

    private Date sdate;//评论时间

    private Long replynum;//排序(置顶和上移)

    private Integer commentGrade;

    private Long parentId;//排序(和评论id相等)

    private Integer deliverGrade;
    private Integer chiefGrade;
    private String replyContent;

  

	private Date replytime;
    
    private String deliverContent;
    private String chiefContent;
    private String deliverReplyContent;
    private String chiefReplyContent;
    private Date deliverSdate;
    private Date chiefSdate;
    //以下为扩展字段
    private String payNumber;//订单号
    
    private List<OrderGoods> orderGoods;
    
    private String deliverManName;
    private String chiefName;
    private Integer storeId;
    
    
    
    
    
    
    
    public String getChiefName() {
  		return chiefName;
  	}

  	public void setChiefName(String chiefName) {
  		this.chiefName = chiefName;
  	}

	public String getDeliverManName() {
		return deliverManName;
	}

	public void setDeliverManName(String deliverManName) {
		this.deliverManName = deliverManName;
	}

	public String getDeliverContent() {
		return deliverContent;
	}

	public void setDeliverContent(String deliverContent) {
		this.deliverContent = deliverContent;
	}

	public String getDeliverReplyContent() {
		return deliverReplyContent;
	}

	public void setDeliverReplyContent(String deliverReplyContent) {
		this.deliverReplyContent = deliverReplyContent;
	}
	 @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
	public Date getDeliverSdate() {
		return deliverSdate;
	}

	public void setDeliverSdate(Date deliverSdate) {
		this.deliverSdate = deliverSdate;
	}

	public List<OrderGoods> getOrderGoods() {
		return orderGoods;
	}

	public void setOrderGoods(List<OrderGoods> orderGoods) {
		this.orderGoods = orderGoods;
	}

	public String getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(String payNumber) {
		this.payNumber = payNumber;
	}

	public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getDeliverManId() {
        return deliverManId;
    }

    public void setDeliverManId(Integer deliverManId) {
        this.deliverManId = deliverManId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberNick() {
        return memberNick;
    }

    public void setMemberNick(String memberNick) {
        this.memberNick = memberNick == null ? null : memberNick.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public Integer getCommentGrade() {
        return commentGrade;
    }

    public void setCommentGrade(Integer commentGrade) {
        this.commentGrade = commentGrade;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getDeliverGrade() {
        return deliverGrade;
    }

    public void setDeliverGrade(Integer deliverGrade) {
        this.deliverGrade = deliverGrade;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent == null ? null : replyContent.trim();
    }

    public Date getReplytime() {
        return replytime;
    }

    public void setReplytime(Date replytime) {
        this.replytime = replytime;
    }

	public Long getReplynum() {
		return replynum;
	}

	public void setReplynum(Long replynum) {
		this.replynum = replynum;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	
	public Integer getChiefGrade() {
		return chiefGrade;
	}

	public void setChiefGrade(Integer chiefGrade) {
		this.chiefGrade = chiefGrade;
	}

	public String getChiefContent() {
		return chiefContent;
	}

	public void setChiefContent(String chiefContent) {
		this.chiefContent = chiefContent;
	}

	public Integer getChiefId() {
		return chiefId;
	}

	public void setChiefId(Integer chiefId) {
		this.chiefId = chiefId;
	}

	public String getChiefReplyContent() {
		return chiefReplyContent;
	}

	public void setChiefReplyContent(String chiefReplyContent) {
		this.chiefReplyContent = chiefReplyContent;
	}

	public Date getChiefSdate() {
		return chiefSdate;
	}

	public void setChiefSdate(Date chiefSdate) {
		this.chiefSdate = chiefSdate;
	}
	
}