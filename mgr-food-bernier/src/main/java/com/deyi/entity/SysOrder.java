package com.deyi.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SysOrder {
	private Long orderId;

    private Integer memberId;

    private String memberNick;

    private Integer addressId;//地址id

    private Integer couponId;

    private Integer deliverManId;

    private Date orderTime;

    private Date payTime;

    private String contactsPerson;

    private String contactsPhone;

    private String addresDetails;

    private BigDecimal totalAmount;

    private BigDecimal payAmount;

    private String orderStatus;//订单状态 0未处理 1已确认(门店接单) 2备货完成 3配送中 4订单成功 5订单失败 6取消 7配送员接单 8配送完成  

    private String payStatus;

    private String payType;

    private String payAccount;

    private String remark;

    private Date deliverStartTime;

    private Date deliverEndTime;

    private String printStatus;

    private String isDiscount;

    private Double discountValue;//折扣值(用于系统设置满减后,下单时有就显示没有就为0.00)

    private Date completeDate;

    private String isCoupon;

    private Double ouponValue;

    private Date deliveryDate;

    private String orderType;//预定类型:1:预定今日中餐，2:预定今日晚餐,3:预定明日中餐     

    private String isfirst;

    private String payNumber;//支付流水号

    private String isAllon;

    private Date edate;

    private Double outsideMoney;

    private Date revocationTime;

    private String isdel;

    private String isovertimeFree;

    private String activityType;

    private String activityDesc;

    private BigDecimal refundMoney;
    
    private Date bookTime;//之后加的预约时间
    
    private Long chiefId;//厨师id
    
    private Long storeId;//门店id
    
    private Long tableId;//餐桌id
    
    //以下为扩展字段
    private String startDate;

 	private String endDate;
 	
 	private String deliverManName;//配送员姓名
 	
 	private List<OrderGoods> orderGoods;// 订单商品(一对多)
 	
 	private String chiefName;//厨师名称
     
    private String storeName;//门店名称
    
    private String storeType;//门店类型
    
    private String storePhone;//门店手机号
    
    private Integer storeOderSum;//订单数量
    
    private Double storeSalesMony;//销售额
    
    private String tableName;//餐桌名
    
    //拓展
    private String payDate;
    
    private Double rechargeMoney;//销售额
    
    private String voucherId;
    private String voucherType;
    
    
	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public List<OrderGoods> getOrderGoods() {
		return orderGoods;
	}

	public void setOrderGoods(List<OrderGoods> orderGoods) {
		this.orderGoods = orderGoods;
	}

	public String getDeliverManName() {
		return deliverManName;
	}

	public void setDeliverManName(String deliverManName) {
		this.deliverManName = deliverManName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
	public Date getBookTime() {
		return bookTime;
	}

	public void setBookTime(Date bookTime) {
		this.bookTime = bookTime;
	}

	public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberNick() {
        return memberNick;
    }

    public void setMemberNick(String memberNick) {
        this.memberNick = memberNick == null ? null : memberNick.trim();
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getDeliverManId() {
        return deliverManId;
    }

    public void setDeliverManId(Integer deliverManId) {
        this.deliverManId = deliverManId;
    }

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getContactsPerson() {
        return contactsPerson;
    }

    public void setContactsPerson(String contactsPerson) {
        this.contactsPerson = contactsPerson == null ? null : contactsPerson.trim();
    }

    public String getContactsPhone() {
        return contactsPhone;
    }

    public void setContactsPhone(String contactsPhone) {
        this.contactsPhone = contactsPhone == null ? null : contactsPhone.trim();
    }

    public String getAddresDetails() {
        return addresDetails;
    }

    public void setAddresDetails(String addresDetails) {
        this.addresDetails = addresDetails == null ? null : addresDetails.trim();
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus == null ? null : orderStatus.trim();
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus == null ? null : payStatus.trim();
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    public String getPayAccount() {
        return payAccount;
    }

    public void setPayAccount(String payAccount) {
        this.payAccount = payAccount == null ? null : payAccount.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getDeliverStartTime() {
        return deliverStartTime;
    }

    public void setDeliverStartTime(Date deliverStartTime) {
        this.deliverStartTime = deliverStartTime;
    }

    public Date getDeliverEndTime() {
        return deliverEndTime;
    }

    public void setDeliverEndTime(Date deliverEndTime) {
        this.deliverEndTime = deliverEndTime;
    }

    public String getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(String printStatus) {
        this.printStatus = printStatus == null ? null : printStatus.trim();
    }

    public String getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(String isDiscount) {
        this.isDiscount = isDiscount == null ? null : isDiscount.trim();
    }

    public Double getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Double discountValue) {
        this.discountValue = discountValue;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public String getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(String isCoupon) {
        this.isCoupon = isCoupon == null ? null : isCoupon.trim();
    }

    public Double getOuponValue() {
        return ouponValue;
    }

    public void setOuponValue(Double ouponValue) {
        this.ouponValue = ouponValue;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType == null ? null : orderType.trim();
    }

    public String getIsfirst() {
        return isfirst;
    }

    public void setIsfirst(String isfirst) {
        this.isfirst = isfirst == null ? null : isfirst.trim();
    }

    public String getPayNumber() {
        return payNumber;
    }

    public void setPayNumber(String payNumber) {
        this.payNumber = payNumber == null ? null : payNumber.trim();
    }

    public String getIsAllon() {
        return isAllon;
    }

    public void setIsAllon(String isAllon) {
        this.isAllon = isAllon == null ? null : isAllon.trim();
    }

    public Date getEdate() {
        return edate;
    }

    public void setEdate(Date edate) {
        this.edate = edate;
    }

    public Double getOutsideMoney() {
        return outsideMoney;
    }

    public void setOutsideMoney(Double outsideMoney) {
        this.outsideMoney = outsideMoney;
    }

    public Date getRevocationTime() {
        return revocationTime;
    }

    public void setRevocationTime(Date revocationTime) {
        this.revocationTime = revocationTime;
    }

    public String getIsdel() {
        return isdel;
    }

    public void setIsdel(String isdel) {
        this.isdel = isdel == null ? null : isdel.trim();
    }

    public String getIsovertimeFree() {
        return isovertimeFree;
    }

    public void setIsovertimeFree(String isovertimeFree) {
        this.isovertimeFree = isovertimeFree == null ? null : isovertimeFree.trim();
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType == null ? null : activityType.trim();
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc == null ? null : activityDesc.trim();
    }

    public BigDecimal getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(BigDecimal refundMoney) {
        this.refundMoney = refundMoney;
    }

	public Long getChiefId() {
		return chiefId;
	}

	public void setChiefId(Long chiefId) {
		this.chiefId = chiefId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getChiefName() {
		return chiefName;
	}

	public void setChiefName(String chiefName) {
		this.chiefName = chiefName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	public String getStorePhone() {
		return storePhone;
	}

	public void setStorePhone(String storePhone) {
		this.storePhone = storePhone;
	}

	public Integer getStoreOderSum() {
		return storeOderSum;
	}

	public void setStoreOderSum(Integer storeOderSum) {
		this.storeOderSum = storeOderSum;
	}

	public Double getStoreSalesMony() {
		return storeSalesMony;
	}

	public void setStoreSalesMony(Double storeSalesMony) {
		this.storeSalesMony = storeSalesMony;
	}

	public String getPayDate() {
		return payDate;
	}

	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}

	public Long getTableId() {
		return tableId;
	}

	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}

	public Double getRechargeMoney() {
		return rechargeMoney;
	}

	public void setRechargeMoney(Double rechargeMoney) {
		this.rechargeMoney = rechargeMoney;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
    
}