package com.deyi.entity;

public class MemberLevel {
	private Integer id;
	private String name;
	private Double discount;//折扣
	private Integer upCount;//升级所需积分
	private String rule;//积分兑换规则
	
	//拓展部分
	private Double discountMoney;//减免的金额
	private Double totalMoney;//总的金额
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Integer getUpCount() {
		return upCount;
	}
	public void setUpCount(Integer upCount) {
		this.upCount = upCount;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public Double getDiscountMoney() {
		return discountMoney;
	}
	public void setDiscountMoney(Double discountMoney) {
		this.discountMoney = discountMoney;
	}
	public Double getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(Double totalMoney) {
		this.totalMoney = totalMoney;
	}
	
}
