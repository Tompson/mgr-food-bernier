package com.deyi.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Voucher {
    private Long id;

    private BigDecimal voucherMoney;

    private String voucherType;

    private Double exchangeVoucher;

    private BigDecimal bookOrderVoucher;

    private BigDecimal drawVoucher;

    private BigDecimal drawProbability;

    private Date createTime;

    private Date modifyTime;

    private Date useTime;

    private String voucherStatus;

    private String ifOverdue;

    private Date startTime;

    private Date endTime;

    private String voucherPic;

    private String remarks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getVoucherMoney() {
        return voucherMoney;
    }

    public void setVoucherMoney(BigDecimal voucherMoney) {
        this.voucherMoney = voucherMoney;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType == null ? null : voucherType.trim();
    }

    public Double getExchangeVoucher() {
        return exchangeVoucher;
    }

    public void setExchangeVoucher(Double exchangeVoucher) {
        this.exchangeVoucher = exchangeVoucher;
    }

    public BigDecimal getBookOrderVoucher() {
        return bookOrderVoucher;
    }

    public void setBookOrderVoucher(BigDecimal bookOrderVoucher) {
        this.bookOrderVoucher = bookOrderVoucher;
    }

    public BigDecimal getDrawVoucher() {
        return drawVoucher;
    }

    public void setDrawVoucher(BigDecimal drawVoucher) {
        this.drawVoucher = drawVoucher;
    }

    public BigDecimal getDrawProbability() {
        return drawProbability;
    }

    public void setDrawProbability(BigDecimal drawProbability) {
        this.drawProbability = drawProbability;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getUseTime() {
        return useTime;
    }

    public void setUseTime(Date useTime) {
        this.useTime = useTime;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus == null ? null : voucherStatus.trim();
    }

    public String getIfOverdue() {
        return ifOverdue;
    }

    public void setIfOverdue(String ifOverdue) {
        this.ifOverdue = ifOverdue == null ? null : ifOverdue.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getVoucherPic() {
        return voucherPic;
    }

    public void setVoucherPic(String voucherPic) {
        this.voucherPic = voucherPic == null ? null : voucherPic.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }
}