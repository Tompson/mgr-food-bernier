package com.deyi.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DeliverMan {
    private Integer deliverManId;

    private String deliverManName;

    private String tel;

    private String status;//1启用，2停用  

    private Date createTime;
    
    private String qrCode;//生成二维码的图片地址
    
    //以下为扩展字段
    private Integer deliverOrderNum;//配送订单数
    
    private Double avgDeliverGrade;//配送平均评价
    
    private String startDate;

   	private String endDate;
   	
   	private Integer	storeId;
   	
    
    
    
    

    public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getDeliverOrderNum() {
		return deliverOrderNum;
	}

	public void setDeliverOrderNum(Integer deliverOrderNum) {
		this.deliverOrderNum = deliverOrderNum;
	}

	public Double getAvgDeliverGrade() {
		return avgDeliverGrade;
	}

	public void setAvgDeliverGrade(Double avgDeliverGrade) {
		this.avgDeliverGrade = avgDeliverGrade;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public Integer getDeliverManId() {
        return deliverManId;
    }

    public void setDeliverManId(Integer deliverManId) {
        this.deliverManId = deliverManId;
    }

    public String getDeliverManName() {
        return deliverManName;
    }

    public void setDeliverManName(String deliverManName) {
        this.deliverManName = deliverManName == null ? null : deliverManName.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
    
}