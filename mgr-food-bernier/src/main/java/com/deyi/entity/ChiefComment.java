package com.deyi.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ChiefComment {
    private Long cid;

    private Long orderId;

    private Integer chiefManId;

    private Integer memberId;

    private String memberNick;

    private String content;

    private Date sdate;

    private Long replynum;

    private Integer commentGrade;

    private Long parentId;

    private Integer chiefGrade;

    private Date replytime;

    private Date chiefSdate;
    private String replyContent;
    private String chiefContent;
    private String chiefReplyContent;
  //以下为扩展字段
    
    private String payNumber;//订单号
    
    public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	private Integer storeId;
    
    private String  chiefManName;
    public Long getCid() {
        return cid;
    }
    
    public String getChiefContent() {
		return chiefContent;
	}

	public void setChiefContent(String chiefContent) {
		this.chiefContent = chiefContent;
	}

	public void setCid(Long cid) {
        this.cid = cid;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getChiefManId() {
        return chiefManId;
    }

    public void setChiefManId(Integer chiefManId) {
        this.chiefManId = chiefManId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberNick() {
        return memberNick;
    }

    public void setMemberNick(String memberNick) {
        this.memberNick = memberNick == null ? null : memberNick.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public Long getReplynum() {
        return replynum;
    }

    public void setReplynum(Long replynum) {
        this.replynum = replynum;
    }

    public Integer getCommentGrade() {
        return commentGrade;
    }

    public void setCommentGrade(Integer commentGrade) {
        this.commentGrade = commentGrade;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getChiefGrade() {
        return chiefGrade;
    }

    public void setChiefGrade(Integer chiefGrade) {
        this.chiefGrade = chiefGrade;
    }

    public Date getReplytime() {
        return replytime;
    }

    public void setReplytime(Date replytime) {
        this.replytime = replytime;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getChiefSdate() {
        return chiefSdate;
    }

    public void setChiefSdate(Date chiefSdate) {
        this.chiefSdate = chiefSdate;
    }

	public String getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(String payNumber) {
		this.payNumber = payNumber;
	}

	public String getChiefManName() {
		return chiefManName;
	}

	public void setChiefManName(String chiefManName) {
		this.chiefManName = chiefManName;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getChiefReplyContent() {
		return chiefReplyContent;
	}

	public void setChiefReplyContent(String chiefReplyContent) {
		this.chiefReplyContent = chiefReplyContent;
	}
    
}