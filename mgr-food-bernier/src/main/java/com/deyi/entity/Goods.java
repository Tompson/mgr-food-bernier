package com.deyi.entity;

import java.util.Date;

public class Goods {
    private Long id;

    private Long categoryId;

    private String goodsname;

    private Double price;

    private String weeksFoodStatus;

    private String unit;//菜品原价(只是用来展示的)

    private Integer integral;// 菜品状态: 1启用 2禁用       

    private String intro;

    private String status;//状态:0再售 1已售完           
    
    private String isinventory;

    private Integer inventory;

    private String isopen;

    private Integer aucInventory;

    private String goodsPic;//菜品图片

    private Date createTime;

    private Integer sellnum;
    
    private String description;
    
    private Long sort;
    private String pic1;
    
    //扩展字段菜品分类名称
    private String cateGoryGoodsName;
    
    private String startDate;

 	private String endDate;
 
 	
 	private String sortStatus;//排序状态 1:按销量排序 2:按评分排序
 	
 	private Double avgCommentGrade;//评价评论评分(1、2、3、4、5)  
 	
 	private Integer statSellnum;//统计销售数量
 	
 	private boolean check;
 	
 	private Integer storeId;
 	
 	private Long storeGoodsId;//门店商品id
 	
 	private String storeGoodsStatus;//门店商品在售状态

	public Integer getStatSellnum() {
		return statSellnum;
	}

	public void setStatSellnum(Integer statSellnum) {
		this.statSellnum = statSellnum;
	}

	public Double getAvgCommentGrade() {
		return avgCommentGrade;
	}

	public void setAvgCommentGrade(Double avgCommentGrade) {
		this.avgCommentGrade = avgCommentGrade;
	}

	public String getSortStatus() {
		return sortStatus;
	}

	public void setSortStatus(String sortStatus) {
		this.sortStatus = sortStatus;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCateGoryGoodsName() {
		return cateGoryGoodsName;
	}

	public void setCateGoryGoodsName(String cateGoryGoodsName) {
		this.cateGoryGoodsName = cateGoryGoodsName;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname == null ? null : goodsname.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getWeeksFoodStatus() {
        return weeksFoodStatus;
    }

    public void setWeeksFoodStatus(String weeksFoodStatus) {
        this.weeksFoodStatus = weeksFoodStatus == null ? null : weeksFoodStatus.trim();
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getIsinventory() {
        return isinventory;
    }

    public void setIsinventory(String isinventory) {
        this.isinventory = isinventory == null ? null : isinventory.trim();
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public String getIsopen() {
        return isopen;
    }

    public void setIsopen(String isopen) {
        this.isopen = isopen == null ? null : isopen.trim();
    }

    public Integer getAucInventory() {
        return aucInventory;
    }

    public void setAucInventory(Integer aucInventory) {
        this.aucInventory = aucInventory;
    }

    public String getGoodsPic() {
        return goodsPic;
    }

    public void setGoodsPic(String goodsPic) {
        this.goodsPic = goodsPic == null ? null : goodsPic.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getSellnum() {
        return sellnum;
    }

    public void setSellnum(Integer sellnum) {
        this.sellnum = sellnum;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Long getStoreGoodsId() {
		return storeGoodsId;
	}

	public void setStoreGoodsId(Long storeGoodsId) {
		this.storeGoodsId = storeGoodsId;
	}

	public String getStoreGoodsStatus() {
		return storeGoodsStatus;
	}

	public void setStoreGoodsStatus(String storeGoodsStatus) {
		this.storeGoodsStatus = storeGoodsStatus;
	}

	public String getPic1() {
		return pic1;
	}

	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}
    
}