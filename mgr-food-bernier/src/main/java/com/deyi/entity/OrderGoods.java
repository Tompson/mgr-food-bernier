package com.deyi.entity;

public class OrderGoods {
    private Long id;

    private Long orderId;

    private Long goodsId;

    private String googsName;

    private Double price;

    private Double priceSale;

    private Double pricePay;

    private Integer quantify;

    private String status;

    private String isPrint;
    private Long storeGoodsId;
	//以下为扩展字段
	private String unit;//菜品单位
	
	private Goods goodsInfo;//菜的信息(一对一)
	
	private Integer quantifySum;//购买数量 总和
	
	private Double pricePaySum;//支付价格 促销价或销售价 ×数量 元 总和
	
	private Integer quantifySumYesterday;//购买数量 昨日交易商品总数量
	
	
	
	
	
	
	
	
	
	


	public Integer getQuantifySumYesterday() {
		return quantifySumYesterday;
	}

	public void setQuantifySumYesterday(Integer quantifySumYesterday) {
		this.quantifySumYesterday = quantifySumYesterday;
	}

	public Double getPricePaySum() {
		return pricePaySum;
	}

	public void setPricePaySum(Double pricePaySum) {
		this.pricePaySum = pricePaySum;
	}

	public Integer getQuantifySum() {
		return quantifySum;
	}

	public void setQuantifySum(Integer quantifySum) {
		this.quantifySum = quantifySum;
	}

	public Goods getGoodsInfo() {
		return goodsInfo;
	}

	public void setGoodsInfo(Goods goodsInfo) {
		this.goodsInfo = goodsInfo;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoogsName() {
        return googsName;
    }

    public void setGoogsName(String googsName) {
        this.googsName = googsName == null ? null : googsName.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(Double priceSale) {
        this.priceSale = priceSale;
    }

    public Double getPricePay() {
        return pricePay;
    }

    public void setPricePay(Double pricePay) {
        this.pricePay = pricePay;
    }

    public Integer getQuantify() {
        return quantify;
    }

    public void setQuantify(Integer quantify) {
        this.quantify = quantify;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getIsPrint() {
        return isPrint;
    }

    public void setIsPrint(String isPrint) {
        this.isPrint = isPrint == null ? null : isPrint.trim();
    }

	public Long getStoreGoodsId() {
		return storeGoodsId;
	}

	public void setStoreGoodsId(Long storeGoodsId) {
		this.storeGoodsId = storeGoodsId;
	}
    
}