package com.deyi.entity;

import java.util.Date;

public class SysMenu {
    private String menuid;

    private String menuname;

    private String menuurl;

    private String meunicon;

    private String parentmenuid;

    private String menulevel;

    private String menustatus;

    private String createperson;

    private Date createtime;

    private String updateperson;

    private Date updatetime;

    private String isnode;

    private String menuclass;

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid == null ? null : menuid.trim();
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname == null ? null : menuname.trim();
    }

    public String getMenuurl() {
        return menuurl;
    }

    public void setMenuurl(String menuurl) {
        this.menuurl = menuurl == null ? null : menuurl.trim();
    }

    public String getMeunicon() {
        return meunicon;
    }

    public void setMeunicon(String meunicon) {
        this.meunicon = meunicon == null ? null : meunicon.trim();
    }

    public String getParentmenuid() {
        return parentmenuid;
    }

    public void setParentmenuid(String parentmenuid) {
        this.parentmenuid = parentmenuid == null ? null : parentmenuid.trim();
    }

    public String getMenulevel() {
        return menulevel;
    }

    public void setMenulevel(String menulevel) {
        this.menulevel = menulevel == null ? null : menulevel.trim();
    }

    public String getMenustatus() {
        return menustatus;
    }

    public void setMenustatus(String menustatus) {
        this.menustatus = menustatus == null ? null : menustatus.trim();
    }

    public String getCreateperson() {
        return createperson;
    }

    public void setCreateperson(String createperson) {
        this.createperson = createperson == null ? null : createperson.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getUpdateperson() {
        return updateperson;
    }

    public void setUpdateperson(String updateperson) {
        this.updateperson = updateperson == null ? null : updateperson.trim();
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getIsnode() {
        return isnode;
    }

    public void setIsnode(String isnode) {
        this.isnode = isnode == null ? null : isnode.trim();
    }

    public String getMenuclass() {
        return menuclass;
    }

    public void setMenuclass(String menuclass) {
        this.menuclass = menuclass == null ? null : menuclass.trim();
    }
}