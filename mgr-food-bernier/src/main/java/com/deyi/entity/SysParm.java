package com.deyi.entity;

import java.math.BigDecimal;
import java.security.Timestamp;
import java.util.Date;

public class SysParm {
    private Long id;

    private Integer uid;

    private Date startTime;//营业开始时间  

    private Date endTime;//营业结束时间

    private BigDecimal startPrice;

    private BigDecimal deliverPrice;

    private String bookDiscount;//预定折扣(满减)  用,号隔开 比如(100,20)    

    private String integralProportion;//消费金额-积分兑换比例 用,号隔开 比如(100,1) 

    private String advertImage;//广告图片地址(多个，隔开) 

    private Date creatdate;

    private Double bookEvery;

    private Double commentOrderEvery;

    private Double shareEvery;
    
    private Integer deliverScope;//配送范围
    
    private String videoLink;   //视频链接
    
    //以下为扩展字段
    private Date startTimePm;//营业下午开始时间 (数据库是timestamp)
    
    private Date endTimePm;//营业下午结束时间 (数据库是timestamp)
    
    private String imgLinkA;//广告图片1链接
    private String imgLinkB;// 广告图片2链接
    private String imgLinkC;//广告图片3链接
    
    private Long sysParmLinkTimeId;//系统参数扩展表id更新用
    
    private String startTimePm1;//页面提交过来的是字符串
    private String endTimePm1;//页面提交过来的是字符串
    
    
    
    
    
    
    
    
    
    
    
    public String getStartTimePm1() {
		return startTimePm1;
	}

	public void setStartTimePm1(String startTimePm1) {
		this.startTimePm1 = startTimePm1;
	}

	public String getEndTimePm1() {
		return endTimePm1;
	}

	public void setEndTimePm1(String endTimePm1) {
		this.endTimePm1 = endTimePm1;
	}

	public Long getSysParmLinkTimeId() {
		return sysParmLinkTimeId;
	}

	public void setSysParmLinkTimeId(Long sysParmLinkTimeId) {
		this.sysParmLinkTimeId = sysParmLinkTimeId;
	}

	public Date getStartTimePm() {
		return startTimePm;
	}

	public void setStartTimePm(Date startTimePm) {
		this.startTimePm = startTimePm;
	}

	public Date getEndTimePm() {
		return endTimePm;
	}

	public void setEndTimePm(Date endTimePm) {
		this.endTimePm = endTimePm;
	}

	public String getImgLinkA() {
		return imgLinkA;
	}

	public void setImgLinkA(String imgLinkA) {
		this.imgLinkA = imgLinkA;
	}

	public String getImgLinkB() {
		return imgLinkB;
	}

	public void setImgLinkB(String imgLinkB) {
		this.imgLinkB = imgLinkB;
	}

	public String getImgLinkC() {
		return imgLinkC;
	}

	public void setImgLinkC(String imgLinkC) {
		this.imgLinkC = imgLinkC;
	}



	public Integer getDeliverScope() {
		return deliverScope;
	}

	public void setDeliverScope(Integer deliverScope) {
		this.deliverScope = deliverScope;
	}

	public String getVideoLink() {
		return videoLink;
	}

	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}

	//以下为扩展字段
    private String bookDiscount2;//减
    
    private String integralProportion1;//兑换
    
    private String[] goodsPics;//广告图片集合
    
    
    
    
    
    

    public String[] getGoodsPics() {
		return goodsPics;
	}

	public void setGoodsPics(String[] goodsPics) {
		this.goodsPics = goodsPics;
	}

	public String getIntegralProportion1() {
		return integralProportion1;
	}

	public void setIntegralProportion1(String integralProportion1) {
		this.integralProportion1 = integralProportion1;
	}

	public String getBookDiscount2() {
		return bookDiscount2;
	}

	public void setBookDiscount2(String bookDiscount2) {
		this.bookDiscount2 = bookDiscount2;
	}


    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai") //目前没用
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai") //目前没用
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getDeliverPrice() {
        return deliverPrice;
    }

    public void setDeliverPrice(BigDecimal deliverPrice) {
        this.deliverPrice = deliverPrice;
    }

    public String getBookDiscount() {
        return bookDiscount;
    }

    public void setBookDiscount(String bookDiscount) {
        this.bookDiscount = bookDiscount == null ? null : bookDiscount.trim();
    }

    public String getIntegralProportion() {
        return integralProportion;
    }

    public void setIntegralProportion(String integralProportion) {
        this.integralProportion = integralProportion == null ? null : integralProportion.trim();
    }

    public String getAdvertImage() {
        return advertImage;
    }

    public void setAdvertImage(String advertImage) {
        this.advertImage = advertImage == null ? null : advertImage.trim();
    }

    public Date getCreatdate() {
        return creatdate;
    }

    public void setCreatdate(Date creatdate) {
        this.creatdate = creatdate;
    }

    public Double getBookEvery() {
        return bookEvery;
    }

    public void setBookEvery(Double bookEvery) {
        this.bookEvery = bookEvery;
    }

    public Double getCommentOrderEvery() {
        return commentOrderEvery;
    }

    public void setCommentOrderEvery(Double commentOrderEvery) {
        this.commentOrderEvery = commentOrderEvery;
    }

    public Double getShareEvery() {
        return shareEvery;
    }

    public void setShareEvery(Double shareEvery) {
        this.shareEvery = shareEvery;
    }
}