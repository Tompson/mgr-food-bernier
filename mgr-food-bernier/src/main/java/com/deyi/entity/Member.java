package com.deyi.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Member {
    private Integer memberId;//9位数

    private String memberAccount;

    private String memberNick;

    private String profileImg;

    private Integer sex;

    private String city;

    private String province;

    private String country;

    private String wxUnionid;

    private String remark;

    private Integer wxGroupid;//会员状态 1启用 2禁用

    private Integer memberRank;//(是否删除) 1未删除 2已删除

    private Integer subStatus;

    private Date subTime;

    private Date unsubTime;

    private Date lastvisitTime;

    private String lastvisitIp;

    private String qrImg;

    private String qrContent;

    private Date qrCreatetime;

    private Date qrExpiretime;

    private Integer qrExpiresIn;

    private Integer qrSceneId;

    private BigDecimal geoLat;

    private BigDecimal geoLng;

    private Date geoTime;

    private BigDecimal geoPrec;
    
    private String birthday;
    
    private String wxOpenId;
    private Integer age;
    private Integer  memberLevelId;
    private BigDecimal money;//余额
    private Integer consumeCount;//消费次数
    //以下为扩展字段
    private Integer integral;//会员积分
    private MemberLevel memberLevel;
    
    private String memberNo;
    private String payPassword;

    
    
    private String ageUp;//年龄上
    
    private String ageDown;//年龄下
    
    private String consumeCountUp;//消费次数上
    
    private String consumeCountDown;//消费次数下
    private String phone;
    
    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getIntegral() {
		return integral;
	}

	public void setIntegral(Integer integral) {
		this.integral = integral;
	}

	public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberAccount() {
        return memberAccount;
    }

    public void setMemberAccount(String memberAccount) {
        this.memberAccount = memberAccount == null ? null : memberAccount.trim();
    }

    public String getMemberNick() {
        return memberNick;
    }

    public void setMemberNick(String memberNick) {
        this.memberNick = memberNick == null ? null : memberNick.trim();
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg == null ? null : profileImg.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public String getWxUnionid() {
        return wxUnionid;
    }

    public void setWxUnionid(String wxUnionid) {
        this.wxUnionid = wxUnionid == null ? null : wxUnionid.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getWxGroupid() {
        return wxGroupid;
    }

    public void setWxGroupid(Integer wxGroupid) {
        this.wxGroupid = wxGroupid;
    }

    public Integer getMemberRank() {
        return memberRank;
    }

    public void setMemberRank(Integer memberRank) {
        this.memberRank = memberRank;
    }

    public Integer getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(Integer subStatus) {
        this.subStatus = subStatus;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    public Date getSubTime() {
        return subTime;
    }

    public void setSubTime(Date subTime) {
        this.subTime = subTime;
    }

    public Date getUnsubTime() {
        return unsubTime;
    }

    public void setUnsubTime(Date unsubTime) {
        this.unsubTime = unsubTime;
    }

    public Date getLastvisitTime() {
        return lastvisitTime;
    }

    public void setLastvisitTime(Date lastvisitTime) {
        this.lastvisitTime = lastvisitTime;
    }

    public String getLastvisitIp() {
        return lastvisitIp;
    }

    public void setLastvisitIp(String lastvisitIp) {
        this.lastvisitIp = lastvisitIp == null ? null : lastvisitIp.trim();
    }

    public String getQrImg() {
        return qrImg;
    }

    public void setQrImg(String qrImg) {
        this.qrImg = qrImg == null ? null : qrImg.trim();
    }

    public String getQrContent() {
        return qrContent;
    }

    public void setQrContent(String qrContent) {
        this.qrContent = qrContent == null ? null : qrContent.trim();
    }

    public Date getQrCreatetime() {
        return qrCreatetime;
    }

    public void setQrCreatetime(Date qrCreatetime) {
        this.qrCreatetime = qrCreatetime;
    }

    public Date getQrExpiretime() {
        return qrExpiretime;
    }

    public void setQrExpiretime(Date qrExpiretime) {
        this.qrExpiretime = qrExpiretime;
    }

    public Integer getQrExpiresIn() {
        return qrExpiresIn;
    }

    public void setQrExpiresIn(Integer qrExpiresIn) {
        this.qrExpiresIn = qrExpiresIn;
    }

    public Integer getQrSceneId() {
        return qrSceneId;
    }

    public void setQrSceneId(Integer qrSceneId) {
        this.qrSceneId = qrSceneId;
    }

    public BigDecimal getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(BigDecimal geoLat) {
        this.geoLat = geoLat;
    }

    public BigDecimal getGeoLng() {
        return geoLng;
    }

    public void setGeoLng(BigDecimal geoLng) {
        this.geoLng = geoLng;
    }

    public Date getGeoTime() {
        return geoTime;
    }

    public void setGeoTime(Date geoTime) {
        this.geoTime = geoTime;
    }

    public BigDecimal getGeoPrec() {
        return geoPrec;
    }

    public void setGeoPrec(BigDecimal geoPrec) {
        this.geoPrec = geoPrec;
    }

	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getMemberLevelId() {
		return memberLevelId;
	}

	public void setMemberLevelId(Integer memberLevelId) {
		this.memberLevelId = memberLevelId;
	}

	
	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Integer getConsumeCount() {
		return consumeCount;
	}

	public void setConsumeCount(Integer consumeCount) {
		this.consumeCount = consumeCount;
	}

	public MemberLevel getMemberLevel() {
		return memberLevel;
	}

	public void setMemberLevel(MemberLevel memberLevel) {
		this.memberLevel = memberLevel;
	}
	

	public String getPayPassword() {
		return payPassword;
	}

	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}


	public String getMemberNo() {
		return memberNo;
	}

	@Override
	public String toString() {
		return "Member [memberId=" + memberId + ", memberAccount=" + memberAccount + ", memberNick=" + memberNick
				+ ", profileImg=" + profileImg + ", sex=" + sex + ", city=" + city + ", province=" + province
				+ ", country=" + country + ", wxUnionid=" + wxUnionid + ", remark=" + remark + ", wxGroupid="
				+ wxGroupid + ", memberRank=" + memberRank + ", subStatus=" + subStatus + ", subTime=" + subTime
				+ ", unsubTime=" + unsubTime + ", lastvisitTime=" + lastvisitTime + ", lastvisitIp=" + lastvisitIp
				+ ", qrImg=" + qrImg + ", qrContent=" + qrContent + ", qrCreatetime=" + qrCreatetime + ", qrExpiretime="
				+ qrExpiretime + ", qrExpiresIn=" + qrExpiresIn + ", qrSceneId=" + qrSceneId + ", geoLat=" + geoLat
				+ ", geoLng=" + geoLng + ", geoTime=" + geoTime + ", geoPrec=" + geoPrec + ", birthday=" + birthday
				+ ", wxOpenId=" + wxOpenId + ", age=" + age + ", memberLevelId=" + memberLevelId + ", money=" + money
				+ ", consumeCount=" + consumeCount + ", integral=" + integral + ", memberLevel=" + memberLevel
				+ ", memberNo=" + memberNo + ", payPassword=" + payPassword + ", ageUp=" + ageUp + ", ageDown="
				+ ageDown + ", consumeCountUp=" + consumeCountUp + ", consumeCountDown=" + consumeCountDown + ", phone="
				+ phone + "]";
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getAgeUp() {
		return ageUp;
	}

	public void setAgeUp(String ageUp) {
		this.ageUp = ageUp;
	}

	public String getAgeDown() {
		return ageDown;
	}

	public void setAgeDown(String ageDown) {
		this.ageDown = ageDown;
	}

	public String getConsumeCountUp() {
		return consumeCountUp;
	}

	public void setConsumeCountUp(String consumeCountUp) {
		this.consumeCountUp = consumeCountUp;
	}

	public String getConsumeCountDown() {
		return consumeCountDown;
	}

	public void setConsumeCountDown(String consumeCountDown) {
		this.consumeCountDown = consumeCountDown;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	

	/*@Override
	public String toString() {
		return "Member [memberId=" + memberId + ", memberAccount=" + memberAccount + ", memberNick=" + memberNick
				+ ", profileImg=" + profileImg + ", sex=" + sex + ", city=" + city + ", province=" + province
				+ ", country=" + country + ", wxUnionid=" + wxUnionid + ", remark=" + remark + ", wxGroupid="
				+ wxGroupid + ", memberRank=" + memberRank + ", subStatus=" + subStatus + ", subTime=" + subTime
				+ ", unsubTime=" + unsubTime + ", lastvisitTime=" + lastvisitTime + ", lastvisitIp=" + lastvisitIp
				+ ", qrImg=" + qrImg + ", qrContent=" + qrContent + ", qrCreatetime=" + qrCreatetime + ", qrExpiretime="
				+ qrExpiretime + ", qrExpiresIn=" + qrExpiresIn + ", qrSceneId=" + qrSceneId + ", geoLat=" + geoLat
				+ ", geoLng=" + geoLng + ", geoTime=" + geoTime + ", geoPrec=" + geoPrec + ", wxOpenId=" + wxOpenId
				+ ", integral=" + integral + ", getIntegral()=" + getIntegral() + ", getMemberId()=" + getMemberId()
				+ ", getMemberAccount()=" + getMemberAccount() + ", getMemberNick()=" + getMemberNick()
				+ ", getProfileImg()=" + getProfileImg() + ", getSex()=" + getSex() + ", getCity()=" + getCity()
				+ ", getProvince()=" + getProvince() + ", getCountry()=" + getCountry() + ", getWxUnionid()="
				+ getWxUnionid() + ", getRemark()=" + getRemark() + ", getWxGroupid()=" + getWxGroupid()
				+ ", getMemberRank()=" + getMemberRank() + ", getSubStatus()=" + getSubStatus() + ", getSubTime()="
				+ getSubTime() + ", getUnsubTime()=" + getUnsubTime() + ", getLastvisitTime()=" + getLastvisitTime()
				+ ", getLastvisitIp()=" + getLastvisitIp() + ", getQrImg()=" + getQrImg() + ", getQrContent()="
				+ getQrContent() + ", getQrCreatetime()=" + getQrCreatetime() + ", getQrExpiretime()="
				+ getQrExpiretime() + ", getQrExpiresIn()=" + getQrExpiresIn() + ", getQrSceneId()=" + getQrSceneId()
				+ ", getGeoLat()=" + getGeoLat() + ", getGeoLng()=" + getGeoLng() + ", getGeoTime()=" + getGeoTime()
				+ ", getGeoPrec()=" + getGeoPrec() + ", getWxOpenId()=" + getWxOpenId() + "]";
	}*/
}