package com.deyi.entity;

import java.util.Date;

public class DeliverManDeliver {
    private Integer id;

    private Long orderId;

    private Integer deliverManId;

    private Date getOrderDate;

    private Date beginDeliverDate;

    private Date endDeliverDate;

    private String deliverStatus;

    private Date cancleDate;

    private String cancleReason;

    private Date forecastCompleteDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getDeliverManId() {
        return deliverManId;
    }

    public void setDeliverManId(Integer deliverManId) {
        this.deliverManId = deliverManId;
    }

    public Date getGetOrderDate() {
        return getOrderDate;
    }

    public void setGetOrderDate(Date getOrderDate) {
        this.getOrderDate = getOrderDate;
    }

    public Date getBeginDeliverDate() {
        return beginDeliverDate;
    }

    public void setBeginDeliverDate(Date beginDeliverDate) {
        this.beginDeliverDate = beginDeliverDate;
    }

    public Date getEndDeliverDate() {
        return endDeliverDate;
    }

    public void setEndDeliverDate(Date endDeliverDate) {
        this.endDeliverDate = endDeliverDate;
    }

    public String getDeliverStatus() {
        return deliverStatus;
    }

    public void setDeliverStatus(String deliverStatus) {
        this.deliverStatus = deliverStatus == null ? null : deliverStatus.trim();
    }

    public Date getCancleDate() {
        return cancleDate;
    }

    public void setCancleDate(Date cancleDate) {
        this.cancleDate = cancleDate;
    }

    public String getCancleReason() {
        return cancleReason;
    }

    public void setCancleReason(String cancleReason) {
        this.cancleReason = cancleReason == null ? null : cancleReason.trim();
    }

    public Date getForecastCompleteDate() {
        return forecastCompleteDate;
    }

    public void setForecastCompleteDate(Date forecastCompleteDate) {
        this.forecastCompleteDate = forecastCompleteDate;
    }
}