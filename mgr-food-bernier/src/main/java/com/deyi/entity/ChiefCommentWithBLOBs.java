package com.deyi.entity;

public class ChiefCommentWithBLOBs extends ChiefComment {
    private String replyContent;

    private String chiefContent;

    private String chiefReplyContent;

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent == null ? null : replyContent.trim();
    }

    public String getChiefContent() {
        return chiefContent;
    }

    public void setChiefContent(String chiefContent) {
        this.chiefContent = chiefContent == null ? null : chiefContent.trim();
    }

    public String getChiefReplyContent() {
        return chiefReplyContent;
    }

    public void setChiefReplyContent(String chiefReplyContent) {
        this.chiefReplyContent = chiefReplyContent == null ? null : chiefReplyContent.trim();
    }
}