package com.deyi.entity;

import java.util.Date;
import java.util.List;

public class CategoryGoods {
    private Long categoryId;

    private String cgoodsName;

    private Integer softing;

    private String categoryDesc;

    private Date createTime;
    private Integer sort;
    private String status;
    /*拓展*/
    private boolean check;
    private List<Goods> goodsList;
    private String storeName;
    
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCgoodsName() {
        return cgoodsName;
    }

    public void setCgoodsName(String cgoodsName) {
        this.cgoodsName = cgoodsName == null ? null : cgoodsName.trim();
    }

    public Integer getSofting() {
        return softing;
    }

    public void setSofting(Integer softing) {
        this.softing = softing;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc == null ? null : categoryDesc.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public List<Goods> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<Goods> goodsList) {
		this.goodsList = goodsList;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
}