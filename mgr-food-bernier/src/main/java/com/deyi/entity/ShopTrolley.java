package com.deyi.entity;

public class ShopTrolley {
    private Integer id;

    private Long goodsId;

    private Integer memberId;

    private String memberName;

    private Double price;

    private Integer num;
    
    private Integer storeId;//门店id
    
    private Integer week;//星期数
    private Long storeGoodsId; 
    private String fromType;
    
    public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Long getStoreGoodsId() {
		return storeGoodsId;
	}

	public void setStoreGoodsId(Long storeGoodsId) {
		this.storeGoodsId = storeGoodsId;
	}
	
}