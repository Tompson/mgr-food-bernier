function getWebRootPath() {
    //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht = curWwwPath.substring(0, pos);
    //获取带"/"的项目名，如：/uimcardprj
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
}

function getShopTrolleyNum(goodsId){
	var num=0;
	if(goodsId){
		$.ajax({
			url:"api/getShopTrolleyNum.html",
			type:"get",
			async:false,
			data:{goodsId:goodsId},
			dataType:"json",
			success:function(result){
				if(result&&result.success){
					num=result.data;
				}
			},
		});
	}
	return num;
}

	var flag=false;
	$(".add-2-cart").on("click", function() {
		if(flag){
			return;
		}
		flag=true;
		var goodsId = $(this).data("goods-id");
		var num=0;
		var storeId=$("storeId").val();
		if(goodsId){
			num=getShopTrolleyNum(goodsId);
		}
		num=num+1;
		var webRootPath=getWebRootPath();
		$.ajax({
			url:webRootPath+"/h5/shopcar/api/add2Cart.html",
			type:"get",
			data:{goodsId : goodsId,num : num,currentWeek:currentWeek,storeId:storeId},
			dataType:"json",
			success:function(result){
				if(result){
					if(result.success){
						if (id) {
							$("#" + id).html(result.data);
						}
						tips({
							message : "加入购物车成功！",
							skin : "msg"
						});
					}else{
						var errorCode=result.data;
						switch(errorCode){
						case "001":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						case "007":
							tips({
								message : result.msg,
								skin : "msg"
							});
							break;
						case "002":
							tips({
								message : result.msg,
								skin : "msg"
							});
							break;
						case "003":
							tips({
								message : result.msg,
								skin : "confirm"
							},function(){
								location.href="productOfWeek.html?toWeek=tomorrow";
							});
							break;
						default:
							tips({
								message : "加入购物车失败！",
								skin : "msg"
							});
							break;
						}
					}
				}else{
					tips({
						message : "系统繁忙！",
						skin : "msg"
					});
				}
				flag=false;
			},
			error:function(){
				tips({
					message : "系统繁忙！",
					skin : "msg"
				});
				flag=false;
			}
		});
	});
//}
/**
 * 购物车非空验证或达到起步价
 * 返回true表示购物车非空且达到起步价
 * @returns {Boolean}
 */
/*function checkShopCartIsEmpty(){
	var flag=false;
	$.ajax({
		url:"api/checkShopCart.html",
		type:"get",
		async:false,
		dataType:"json",
		success:function(result){
			if(result&&result.success){
				flag=true;
			}else{
				tips({
					message : result.msg,
					skin : "alert"
				});
				flag=false;
			}
			
		},
		error:function(){
			tips({
				message : "系统繁忙！",
				skin : "msg"
			});
			flag=false;
		}
	});
	return flag;
}*/
/**
 * 获取星级评分块
 * @param score 评分
 * @returns {String}
 */
function getGrade(score) {
	var html = '<div class="mark">';
	//         		<!-- 0颗星 -->
	if (score < 1) {
		html += '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 2) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 3) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 4) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score < 5) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-2"></span>';
	} else if (score >= 5) {
		html += '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>'
				+ '<span class="satrs satrs-1"></span>';
	}
	html += '<em class="seller-star-f">' + score + '</em></div>';
	return html;
}


//过去当前预定类型，选择预定类型，1:预定今日中餐，2:预定今日晚餐,3:预定明日中餐，
//v3版本不在使用
function getCurrentBookType(){
	var bookType="";
	$.ajax({
		url:"api/getCurrentBookType.html",
		type:"get",
		async:false,
		data:{},
		dataType:"json",
		success:function(result){
			if(result&&result.success){
				bookType=result.data;
			}
		}
	});
	
	return bookType;
}

/**
 * 判断当前周是否是当天，如果是当天则判断是否超过营业时间
 * @param week 
 */

function checkCurrentWeek(week){
	var flag=false;
	$("#toShopTrolley").on("click", function(e) {
		e.preventDefault();//取消默认事件
		if(flag){
			return;
		}
		flag=true;
		$.ajax({
			url:"api/checkCurrentWeek.html",
			type:"get",
			data:{week:week},
			dataType:"json",
			async:false,
			success:function(result){
				if(result){
					if(result.success){
						//验证通过
						location.href="shopcar/shopTrolley.html?v=3&&storeId=2";
					}else{
						var errorCode=result.data;
						switch(errorCode){
						case "001":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						case "002":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						case "003":
							tips({
								message : result.msg,
								skin : "alert"
							});
							break;
						}
					}
				}else{
					tips({
						message : "系统繁忙！",
						skin : "msg"
					});
				}
				flag=false;
			},
			error:function(){
				tips({
					message : "系统繁忙！",
					skin : "msg"
				});
				flag=false;
			}
		});
		
	});
}

function addListenFixedCategoryBar(){
	var plot_top = $(".plot").offset().top;   
    $(window.document).on("scroll", function() {
    	
        var wh = $(window).height();
        var s = $("body")[0].scrollTop;
        if (s > plot_top) {
            $(".plot").css("position", "fixed");
        } else {
            $(".plot").css("position", "relative");
        }

    });
}
