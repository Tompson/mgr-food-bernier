/**
 * 
 * @param params 是一個二維 數組
 * @param html  需要轉換的html 
 */
function spliceAll(params,html){
	var div = "";
	$.each(params,function(i,v){
		for(var k = 0;k<=v.length;k++){
			html = html.replace(new RegExp("\\{"+k+"\\}","g"), v[k]);
		}
		div += html;
	})
	return div;
}

function spliceArray(data,string,urlStr,checkStr){
	var split = string.split(",");
	var urlArr = urlStr.split(",");
	var arrayObj = new Array();
	for(var j=0;j<data.length;j++){
		var arrayObj2 = new Array();
		for(var k=0;k<split.length;k++){ //一维长度为i,i为变量，可以根据实际情况改变
			var val = split[k].substring(split[k].length-checkStr.length,split[k].length)
			if(val == checkStr){
				var urlId = split[k].substring(0,split[k].length-checkStr.length);
				var value = data[j][urlId];
				if(value){
					arrayObj2[k] = urlArr[k] += value;
				}else{
					arrayObj2[k] = "";
				}
			}else{
				var value = data[j][split[k]];
				if(value){
					arrayObj2[k] = value;
				}else{
					arrayObj2[k] = "";
				}
			}
		}
		if(arrayObj2){
			
		}
		arrayObj.push(arrayObj2);
	}
	return arrayObj;
}


String.prototype.format=function()  
{  
  if(arguments.length==0) return this;  
  for(var s=this, i=0; i<arguments.length; i++)  
    s=s.replace(new RegExp("\\{"+i+"\\}","g"), arguments[i]);  
  return s;  
};  
