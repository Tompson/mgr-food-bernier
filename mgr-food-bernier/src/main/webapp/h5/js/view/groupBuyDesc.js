$(function(){
		$("#saveInfo").click(function(){
			var companyName=$("#companyName").val();
			var companyAddress=$("#companyAddress").val();
			var employeeNum=$("#employeeNum").val();
			var contactPerson=$("#contactPerson").val();
			var phone=$("#phone").val();
			var otherRequest=$("#otherRequest").val();
			if(!companyName){
				tips({
					message:"请输入企业名称！",
					skin:"alert"
				});
				return;
			}
			if(!companyAddress){
				tips({
					message:"请输入企业地址！",
					skin:"alert"
				});
				return;
			}
			if(!employeeNum){
				tips({
					message:"请输入员工数量！",
					skin:"alert"
				});
				return;
			}
			if(!contactPerson){
				tips({
					message:"请输入联系人！",
					skin:"alert"
				});
				return;
			}
			if(!phone){
				tips({
					message:"请输入手机号码！",
					skin:"alert"
				});
				return;
			}
			var pattern = /^[0-9]{1,4}$/;
			if(!pattern.test(employeeNum)){
				tips({
					message:"企业人数只支持数字！",
					skin:"alert"
				});
				return;
			}
			pattern = /^1[34578]\d{9}$/;
			if(!pattern.test(phone)){
				tips({
					message:"请输入正确的手机号码！",
					skin:"alert"
				});
				return;
			}
			var params={};
			params.phone=phone;
			params.employeeNum=employeeNum;
			params.contactPerson=contactPerson;
			params.companyAddress=companyAddress;
			params.companyName=companyName;
			params.otherRequest=otherRequest;
			$.ajax({
				url : "api/saveCompanyInfo.html",
				type : "post",
				data : params,
				dataType : "json",
				success : function(result) {
					if (result) {
						if (result.success) {
							tips({
								message : "提交成功！",
								skin : "msg"
							});
						} else {
							tips({
								message : "提交失败！",
								skin : "alert"
							});
						}
					} else {
						tips({
							message : "系统繁忙！",
							skin : "alert"
						});
					}
				},
				error : function() {
					tips({
						message : "系统繁忙！",
						skin : "alert"
					});
				}
			});
			
		});
	});