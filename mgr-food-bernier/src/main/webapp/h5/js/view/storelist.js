$(function() {
	$('#storeName').on('click',function(){

		$('#storeName').css('text-align','-webkit-left');
		$('#storeName').css('padding-left','5%');
	})
	//搜索
	$('#search').on('click',function(){

		var storeName=$('#storeName').val();
		var params={};
		var map={
				'storeName':storeName
			};
		params.pageNo = 1;
		params.pageSize = 10;
		params.params=map;
		$.ajax({
			url:requestUrl+"/h5/store/storeHome1.html",
			type:"get",
			contentType:"application/json",
			data:params,
			dataType:"json",
			success:function(page){
				console.log(page);
				searchStores(page);
				$('#storeName').val('');
				$('#storeName').css('text-align','center');
				$('#storeName').css('padding-left','');
			},
			error:function (){
				$('#storeName').val('');
				$('#storeName').css('text-align','center');
				$('#storeName').css('padding-left','');
			}
		});
		
	
	})
	$('.search-wrap #storeName').blur(function(){
		var storeName=$('#storeName').val();
		var params={};
		var map={
				'storeName':storeName
			};
		params.pageNo = 1;
		params.pageSize = 10;
		params.params=map;
		$.ajax({
			url:requestUrl+"/h5/store/storeHome1.html",
			type:"get",
			contentType:"application/json",
			data:params,
			dataType:"json",
			success:function(page){
				console.log(page);
				searchStores(page);
				$('#storeName').val('');
				$('#storeName').css('text-align','center');
				$('#storeName').css('padding-left','');
			},
			error:function (){
				
				$('#storeName').val('');
				$('#storeName').css('text-align','center');
				$('#storeName').css('padding-left','');
			}
		});
		
	});
	//条目点击
	$(".storelist-wrap").on("click","li",function(){
		var id =$(this).val();
		location.href=requestUrl+"/h5/store/msgStore.html?id="+id;
	});
	//滚动刷新团购列表
	var temp =null;
	$(window).on("scroll", function() {
		
		var p_hg = Math.ceil($("body")[0].scrollHeight); // 获取当前元素高度
		var s_top = Math.ceil($("body")[0].scrollTop + $(this).height()); // 获取滚动的高度
		if (p_hg <= s_top) {
			var teamList = $("#teamList");
			var pageNo = teamList.attr("pageNo");
			var pageSize = teamList.attr("pageSize");
			var totalPage = teamList.attr("totalPage");
			var params = {};
			if (pageNo && pageSize && totalPage) {
				pageNo = parseInt(pageNo);
				pageSize = parseInt(pageSize);
				totalPage = parseInt(totalPage);
				if (pageNo++ >= totalPage) {
					return;
				}
				params.pageNo = pageNo;
				params.pageSize = pageSize;
			}
			$.ajax({
				url:requestUrl+"/h5/store/storeHome1.html",
				type:"get",
				contentType:"application/json",
				data:params,
				dataType:"json",
				beforeSend:function(){
					if(temp === pageNo){
						return false;
					}
					temp = pageNo;
				},
				success:function(page){
					renderSubCategory(page);
				}
			});
		}
	});
	
});
	
function searchStores(page){
	// alert(111);
	var teamList = $("#teamList");
	$("#teamList").empty();
	/*
	 * if (!shouldAppend) { teamList.empty(); }
	 */
	/* console.info(data.page.pageNo) */
	teamList.attr("pageNo",   page.pageNo);
	teamList.attr("pageSize", page.pageSize);
	teamList.attr("totalPage",page.totalPage);
	// if (data && data.list) {
	var array = [];
	var html = null;
	var stores =page.results;
	for (var j = 0; j < stores.length; j++) {
		var store=stores[j];
		var distance=store.distance/1000;
		distance=distance.toFixed(1);
		var parts=store.parts;
		var div1="";var div2="";
		for(var i=0;i<parts.length;i++){
			
			var part=parts[i];
			var full=part.full;
			var substact=part.substact;
			if(part.type==1){

				div1=div1+"<span>满"+full+"减"+substact+";</span>"
			}
		}
		
		for(var i=0;i<parts.length;i++){
			var part=parts[i];
			if(part.type==2){
				div2=div2+"<span>"+part.discount.toFixed(1)+"折</span>"
			}
		}
		console.log(div1);
		var html = "";
			html ="<li class='clearfix' value='"+store.id+"'><div class='fl storelist-pic'><img src="+store.storePhoto+" /></div><div class='fl storelist-cont'><div class='storelist-tit clearfix'><h1 class='fl txt-ellipsis'>"+store.storeName+"</h1><div class='fl txt-ellipsis'>"+distance+"km</div>" +
					"</div><div>"+store.contactPhone+"</div><p class='address'>"+store.address+"</p>" +
					"<div class='activity-cut'>"+div1+"</div><div class='activity-discount'>"+div2+"</div></div></li>";

			array.push(html);
	}
	
	teamList.append(array.join(""));
}
function renderSubCategory(page) {
	// alert(111);
	var teamList = $("#teamList");
	/*
	 * if (!shouldAppend) { teamList.empty(); }
	 */
	/* console.info(data.page.pageNo) */
	teamList.attr("pageNo",   page.pageNo);
	teamList.attr("pageSize", page.pageSize);
	teamList.attr("totalPage",page.totalPage);
	// if (data && data.list) {
	var array = [];
	var html = null;
	var stores =page.results;
	for (var j = 0; j < stores.length; j++) {
		var store=stores[j];
		var distance=store.distance/1000;
		distance=distance.toFixed(1);
		var parts=store.parts;
		var div1="";var div2="";
		for(var i=0;i<parts.length;i++){
			
			var part=parts[i];
			var full=part.full;
			var substact=part.substact;
			if(part.type==1){

				div1=div1+"<span>满"+full+"减"+substact+";</span>"
			}
		}
		
		for(var i=0;i<parts.length;i++){
			var part=parts[i];
			if(part.type==2){
				div2=div2+"<span>"+part.discount.toFixed(1)+"折</span>"
			}
		}
		console.log(div1);
		var html = "";
			html ="<li class='clearfix' value='"+store.id+"'><div class='fl storelist-pic'><img src="+store.storePhoto+" /></div><div class='fl storelist-cont'><div class='storelist-tit clearfix'><h1 class='fl txt-ellipsis'>"+store.storeName+"</h1><div class='fl txt-ellipsis'>"+distance+"km</div>" +
					"</div><div>"+store.storePhone+"</div><p class='address'>"+store.address+"</p>" +
					"<div class='activity-cut'>"+div1+"</div><div class='activity-discount'>"+div2+"</div></div></li>";

			array.push(html);
	}
	
	teamList.append(array.join(""));
}
