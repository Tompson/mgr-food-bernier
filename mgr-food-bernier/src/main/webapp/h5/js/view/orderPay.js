 function isWeiXin(){ 
    	var ua = window.navigator.userAgent.toLowerCase(); 
    	if(ua.match(/MicroMessenger/i) == 'micromessenger'){ 
    		return true; 
    	}else{ 
    		return false; 
    	}
   	} 
    //
    $(".shop-group-zfb").on("click", function(e) {
    	var sessionMember=$("#sessionMember").val();
    	if(sessionMember){
			e.preventDefault();
			document.getElementById('JweixinTip').style.display = 'block';
    	}
	});
	// 分享提示隐藏
	$("#JweixinTip").on("click", function(){
		$(this).hide();
	});
    //点击添加选中样式
    $(".pay-group").click(function(){
    	//取消所有选中
    	$(".shop-group-cont").addClass("shop-group-add");
    	//选中点击的
	    $(this).find(".shop-group-cont").removeClass("shop-group-add");
    	var key=$(this).find("p").html();
    });
    
    //点击确认支付调起支付
    $(".order-pay").click(function(){
    	var flag=false;
    	var storeId=$("storeId").val();
    	var payno = $(".order-pay-no").val();
    	var payWay = $(".shop-group-cont:not(.shop-group-add)").find("p").data("pay-way");
    	if (payno&&payWay) {
    		if (flag) {
    			alert("支付中!请等待！");
    			return;
    		}
    		flag = true;
    	}
    	//订单验证
    	if(!validateOrder(payno)){
    		return;
    	}
    	if(payWay=="alipay"){
    		var weixin=isWeiXin();
    		if(weixin){
    			$(".shop-group-zfb").trigger("click");
    			return;
    		}
    	}
    	//支付
    	pay(payno,payWay,storeId);
    });