<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<script type="text/javascript">
function statusChoice(){
	   if($("#status").is(":checked")){
		   console.log(1);
		    $("#hiddenStatus0").val("1");
		}else{
			console.log(2);
			$("#hiddenStatus0").val("2");
		}
}

</script>
 			 			<!--添加配送员-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">修改会员等级</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/level/levelEdit.html">
                     <input type="hidden"  name="id" value="${level.id }">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>等级名称：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="name" value="${level.name}" class="form-control col-md-7 col-xs-12" datatype="*1-32" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>折扣：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="discount" value="${level.discount}"  placeholder="请输入0-10的数字"  class="form-control col-md-7 col-xs-12" datatype="/^(\d(\.\d)?|10)$/"  sucmsg=" " errormsg="请输入0-10的数字" nullmsg="必填" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>升级所需积分：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="upCount" value="${level.upCount}" class="form-control col-md-7 col-xs-12" datatype="*1-32" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>积分兑换规则：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" required="required" name="rule" value="${level.rule}" class="form-control col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/\D/g, '')"  placeholder="请输入消费或充值一元可以获得多少积分"  datatype="*1-32" sucmsg=" " nullmsg="必填" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
        </div>
    <!--/添加配送员-->

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('编辑成功',{icon:1});    
    					$('#deliverMan_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg(vo.message,{icon:5});
    				}
    			}
            });
        });
    </script>