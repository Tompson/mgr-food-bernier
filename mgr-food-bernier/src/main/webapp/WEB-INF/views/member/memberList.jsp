<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
                <div class="form-inline">
                
                	<form id="member_table_form">
                
                    <div class="form-group">
                        <input type="text"  class="form-control" placeholder="编号" name="memberNo">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="会员名称" name="memberNick">
                    </div>
                    <div class="form-group">
                        <select name="wxGroupid" class="form-control">
		                    <option value="">状态</option>
		                    <option value="1">启用</option>
		                    <option value="2">禁用</option>
                  		</select>
                    </div>
                    <div class="form-group">
                       <select name="memberLevelId" id="userDj" onchange="" class="form-control">
			                             <c:forEach items="${memberLevelList}" var="item">
				                             	<option  value="${item.id}">${item.name}</option>
			                              </c:forEach>
	                   </select>
                    </div>
                     <div class="form-group">
                        <select name="sex" class="form-control">
		                    <option value="">性别</option>
		                    <option value="1">男</option>
		                    <option value="2">女</option>
                  		</select>
                    </div>
                    
                     <div class="form-group">
                        <input type="text" class="form-control" id="ageUp" name="ageUp"  onkeyup="this.value=this.value.replace(/\D/g, '')" placeholder="年龄上">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="ageDown"  name="ageDown"  onkeyup="this.value=this.value.replace(/\D/g, '')" placeholder="年龄下">
                    </div>
                    
                     <div class="form-group">
                        <input type="text" class="form-control" id="consumeCountUp" name="consumeCountUp" onkeyup="this.value=this.value.replace(/\D/g, '')" placeholder="次数上">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="consumeCountDown"  name="consumeCountDown" onkeyup="this.value=this.value.replace(/\D/g, '')"  placeholder="次数下">
                    </div>
                    
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    
                      </form>
                      
                	</div>
                	
                	  <!-- 表里面的数据 -->
                <div class="mt20">
                    <table id="member_table"></table>
                </div>
                
               <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                	
                	
                	
<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/member/page.html';  //TODO
var syslog_events = {
		'click input[type=checkbox]':function(e,value,row,index){
			console.log("value:"+value);
			var _this=this;
			if(row.wxGroupid == 1){ //启用就禁用
					$.ajax({ 
	      				url: '<%=request.getContextPath()%>/member/disableState.html?memberId='+row.memberId, 
	      				context: document.body, 
	      				async: false,
	      				success: function(vo){
		      				if(vo.success){
		      					layer.msg('操作成功!',{icon:1});
		      					$(_this).prop("checked",false);
		      					row.wxGroupid=2;
		      				}else{
		      					layer.msg('操作失败!',{icon:5});
		      					$(_this).prop("checked",true);
		      					row.wxGroupid=1;
		      				}
	      				}
	      			});
			}else{//禁用就启用
				$.ajax({
	    			url:'<%=request.getContextPath()%>/member/enableState.html?memberId='+row.memberId,
	    			context: document.body, 
	    			async:false,
	    			success: function(vo){
	      				if(vo.success){
	      					layer.msg('操作成功!',{icon:1});
	      					$(_this).prop("checked",true);
	      					row.wxGroupid=1;
	      				}else{
	      					layer.msg('操作失败!',{icon:5});
	      					$(_this).prop("checked",false);
	      					row.wxGroupid=2;
	      				}
	  				},
	    		});
			}
		},
		'click .resettingPassword':function(e,value,row,index){
    		layer.confirm('确定重置密码吗？',{btn:['确认','取消']},
    		function(){
    			 $.ajax({
    				 url:'<%=request.getContextPath()%>/member/resetMemberPasswd.html?id='+row.memberId,
    				 context:document.body,
    				 async:false,
    				 dataType:'json',
    				 success:function(vo){
    					 $('#member_table').bootstrapTable('refresh', null);
  	          			if(vo.success){
  	          				layer.msg('重置成功，密码为：888888!',{icon:1});
  	          			}else{
  	          				layer.msg('重置失败!',{icon:5});
  	          			}
    				 }
    			 });
    		}
    	 );
    	},
    	'click .delMember':function(e,value,row,index){
    		layer.confirm('确定删除该会员吗?', {btn: ['确定','取消']},  
	     	  		function(){
	     	  			$.ajax({ 
	     	          		url: '<%=request.getContextPath()%>/member/deleteMember.html?memberId='+row.memberId, 
	     	          		context: document.body, 
	     	          		async: false,
	     	          	 	dataType:'json',
	     	          		success: function(vo){
	     	          			$('#member_table').bootstrapTable('refresh', null);
	     	          			if(vo.success){
	     	          				layer.msg('删除成功!',{icon:1});
	     	          			}else{
	     	          				layer.msg('删除失败!',{icon:5});
	     	          			}
	     	          		}
	     	          	});
	     	  		}
	     	  	);
    	},
    	'click .replyMember':function(e,value,row,index){
    		layer.confirm('确定恢复该会员吗?', {btn: ['确定','取消']},  
	     	  		function(){
	     	  			$.ajax({ 
	     	          		url: '<%=request.getContextPath()%>/member/rollbackMember.html?memberId='+row.memberId, 
	     	          		context: document.body, 
	     	          		async: false,
	     	          	 	dataType:'json',
	     	          		success: function(vo){
	     	          			$('#member_table').bootstrapTable('refresh', null);
	     	          			if(vo.success){
	     	          				layer.msg('恢复该会员成功!',{icon:1});
	     	          			}else{
	     	          				layer.msg('恢复该会员失败!',{icon:5});
	     	          			}
	     	          		}
	     	          	});
	     	  		}
	     	  	);
    	},
};


var member_columns = [
{field: 'memberId',title: '编号',valign: 'top',sortable: false},
{field: 'memberNick',title: '会员名称',valign: 'top',sortable: false},
{field: 'sex',title: '性别',valign: 'top',sortable: false,	formatter:function(value,row,index){
	if(value == 0){
 		return	'未知';
	}
	 if(value == 1){
		return '男';
	}
	 if(value  == 2){
		return '女';
	}
}
},
{field: 'age',title: '年龄',valign: 'top',sortable: false},
{field: 'levelName',title: '会员等级',valign: 'top',sortable: false,formatter : operateFormatter2},
{field: 'integral',title: '积分',valign: 'top',sortable: false,formatter:integralFormatter},
{field: 'money',title: '账户余额(元)',valign: 'top',sortable: false, formatter:operateFormatter3},
{field: 'subTime',title: '注册时间',valign: 'top',sortable: false},
{field: 'consumeCount',title: '消费次数',valign: 'top',sortable: false},
{field: 'wxGroupid',title: '禁用/启用',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter1},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#member_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: member_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit, //每页显示多少条
					offset:params.offset, //从第几条索引(20)
					sort:params.sort,
					object:$("#member_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#member_table').bootstrapTable('refresh', null);
	}
	
	
	//删除图标
	function operateFormatter(value,row,index){
		if(row.memberRank == 1){ //(是否删除) 1未删除 2已删除
			return ['<t:buttonOut url="/member/deleteMember.html"><button type="button" class="btn  btn-danger btn-xs delMember">',
				'删除','</button></t:buttonOut>'].join('');
		}else{
			return ['<t:buttonOut url="/member/rollbackMember.html"><button type="button" class="btn  btn-warning btn-xs replyMember">',
				'恢复','</button></t:buttonOut>'].join('');
		}
		
	}
	
	//启用和禁用图标
	 function operateFormatter1(value,row,index){
		if(row.wxGroupid == 1){//状态:1启用，2禁用
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" checked="checked">'+
	 		'<span class="ace-tg"></span></div>';
		}
		if(row.wxGroupid == 2){
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" >'+
	 		'<span class="ace-tg"></span></div>';
		}
}
	
	function  operateFormatter2(value,row,index){
		return row.memberLevel.name;
	}
	function  operateFormatter3(value,row,index){
		return changeTwoDecimal_f(row.money);
	}
	function changeTwoDecimal_f(x)
	{
	   var f_x = parseFloat(x);
	   if (isNaN(f_x))
	   {
	      alert('function:changeTwoDecimal->parameter error');
	      return false;
	   }
	   var f_x = Math.round(x*100)/100;
	   var s_x = f_x.toString();
	   var pos_decimal = s_x.indexOf('.');
	   if (pos_decimal < 0)
	   {
	      pos_decimal = s_x.length;
	      s_x += '.';
	   }
	   while (s_x.length <= pos_decimal + 2)
	   {
	      s_x += '0';
	   }
	   return s_x;
	}
	
	
	function integralFormatter(value,row,index){
		if(value == null){
			return value = 0;
		}else{
			return value;
		}
	}
    
	
</script>