<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"	src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>

 <script type="text/javascript">
 $(function(){
 //上传图片
 $(".form-file").on("change", function() {
     var imgFile = $(this)[0];//当前第一个jq对象 jquery对象就是一个数组对象.
     var id=$(this).attr("id");
     var filextension = imgFile.value.substring(imgFile.value.lastIndexOf("."), imgFile.value.length);
     filextension = filextension.toLowerCase();
     if ((filextension != '.jpg') && (filextension != '.gif') && (filextension != '.jpeg') && (filextension != '.png') && (filextension != '.bmp')) {
         alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
         imgFile.focus();
     } else {
         var path;
         if (document.all){//IE
             imgFile.select();
             path = document.selection.createRange().text;
             imgFile.previousSbiling.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")"; //使用滤镜效果 
         } else{//FF
        	 console.log(11111);
        	  $.ajaxFileUpload({  
	  	            url:"<%=request.getContextPath()%>/uploadPic/filesUpload.html",
	  	            secureuri:false,  
	  	            fileElementId:id,//file标签的id  
	  	            dataType: 'json',//返回数据的类型  
	  	            //data:{name:'logan'},//一同上传的数据  
	  	            success: function (data, status) {  
	  	            	$("#"+id).siblings('img')
	  	            	$("#"+id).siblings('img').attr('src',data);
	  	            	$("#"+id).siblings('input[type=hidden]').val(data);
	  	            	
// 	  	            	 $(imgFile).siblings("img").attr("src", data)
	  	            },
	  	            error: function (data, status, e) {  
	  	            	alert("文件上传失败!");   
	  	            }  
	  	        });
         }
      }
 	});
 });

 
 
	function statusChoice(){
		   if($("#status").is(":checked")){
			    $("#hiddenStatus0").val("0"); //启用
			}else{
				$("#hiddenStatus0").val("1"); //禁用
			}
	}
	
</script>  

		 <div class="mt50">
                    <form  class="demo-form form-horizontal form-label-left" novalidate="" method="post" accept-charset="utf-8" action="<%=request.getContextPath()%>/goods/goodsAdd.html">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>菜品名称：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" name="goodsname" required="required" class="form-control col-md-7 col-xs-12" datatype="*1-8" sucmsg=" " nullmsg="必填" type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"><span class="required red">*</span>菜品类型：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
	                                <select name="categoryId" id="userDj" onchange="" class="form-control col-md-7 col-xs-12" datatype="*" sucmsg=" " nullmsg="必填">
			                             <c:forEach items="${categoryGoods}" var="item">
				                             	<option  value="${item.categoryId}">${item.cgoodsName}</option>
			                              </c:forEach>
	                         		</select>
                                <span class="col-xs-12 col-md-12  red  form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>实际价格：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="middle-price" class="form-control col-md-7 col-xs-12" name="price"  type="text"  errormsg="价格最多六位,小数点后将保留两位小数" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" sucmsg=" "  nullmsg="必填">
                                <script type="text/javascript">
                                 $("#middle-price").keyup(function () {
                                    var reg = $(this).val().match(/\d+\.?\d{0,2}/);
                                    var txt = '';
                                    if (reg != null) {
                                        txt = reg[0];
                                    }
                                    $(this).val(txt);
                                }).change(function () {
                                    $(this).keyup();
                                });
                                </script>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                       <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>原价：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="middle-unit" class="form-control col-md-7 col-xs-12" name="unit"  type="text"  errormsg="价格最多六位,小数点后将保留两位小数" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" sucmsg=" " nullmsg="必填">
                               <script type="text/javascript">
                                 $("#middle-unit").keyup(function () {
                                    var reg = $(this).val().match(/\d+\.?\d{0,2}/);
                                    var txt = '';
                                    if (reg != null) {
                                        txt = reg[0];
                                    }
                                    $(this).val(txt);
                                }).change(function () {
                                    $(this).keyup();
                                });
                                </script>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">菜品描述：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="editor" class="date-picker form-control col-md-7 col-xs-8" name="description"  style="height:500px;"></textarea>
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>菜品简介：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name" name="intro" class="form-control col-md-7 col-xs-12" datatype="*1-64" sucmsg=" " nullmsg="必填"  type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>菜品图片(<em>300*300</em>)：
                            
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-item form-item-wd">
                                    <img src="<%=request.getContextPath()%>/images/upload.png" alt="" style="width:150px;height: 100px">
                                      <input class="form-file" type="file" name="file" id="goodsPic1" value="" placeholder="">
                                      <input type="hidden" name="goodsPic"    datatype="*" sucmsg="&nbsp;" nullmsg="必填" />
                                    <span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>菜品详情图片(<em>450*300</em>)：
                            
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-item form-item-wd">
                                    <img src="<%=request.getContextPath()%>/images/upload.png" alt="" style="width:150px;height: 100px">
                                      <input class="form-file" type="file" name="file" id="goodsPic2" value="" placeholder="">
                                      <input type="hidden" name="pic1"    datatype="*" sucmsg="&nbsp;" nullmsg="必填" />
                                    <span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
                                </div>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">提交</button>
                            </div>
                        </div>

                    </form>
             </div>
            
                 <style>  /*写了个样式把Validform_msg强制掩藏了*/
                     #Validform_msg{display:none !important;} 
     			 </style>
      <script>
        $(function() {
            //正确性校验
                $(".demo-form").Validform({
                tiptype: 3,
                datatype:{
                	
                },
                ajaxPost : true,
                callback : function(vo) {
   				 if (vo.success) {
 							layer.msg('添加成功',{icon:1});    
 							setTimeout(function(){
 								changeRightMenu('<%=request.getContextPath()%>/goods/list.html'); 
 							},100);
		   				} else {
		   					 layer.msg('添加失败',{icon:5});
		 				}  
    			} 
            });

            var editor = new wangEditor('editor');
         	// 普通的自定义菜单
            editor.config.menus = [
                'source',
                '|',     // '|' 是菜单组的分割线
                'bold',
                'underline',
                'italic',
                'strikethrough',
                'eraser',
                'forecolor',
                'bgcolor'
             ];
            editor.create();
        });
    </script>
 