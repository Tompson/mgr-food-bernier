<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
   <%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
			<div class="form-inline">
				<form id="goods_table_form">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="菜品名称" name="goodsname">
                    </div>
                    <div class="form-group">
                        <select name="categoryId" id="userDj" onchange="" class="form-control">
                             <c:forEach items="${categoryList}" var="item">
	                             	<option  value="${item.categoryId}">${item.cgoodsName}</option>
                             </c:forEach>
	                   </select>
                    </div>
                     <div class="form-group" id="integralHidden">
                        <select name="integral" class="form-control">
		                    <option value="">状态</option>
		                    <option value="1">启用</option>
		                    <option value="2">禁用</option>
		                </select>
                    </div>
                    
                    <div class="form-group" id="statusHidden">
                        <select name="status" class="form-control">
		                    <option value="">售卖状态</option>
		                    <option value="0">在售</option>
		                    <option value="1">已售完</option>
		                </select>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    <div class="form-group btn-group-vertical alignright">
                    <t:buttonOut url="/goods/toGoodsAdd.html">
                        <a onclick="goodsAdd()" class="btn btn-primary">新增菜品</a>
                     </t:buttonOut>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据 -->
                <div class="mt20">
                    <table id="goods_table"></table>
                </div>

     			 
     			  <style>  /*写了个样式把Validform_msg强制掩藏了*/
                     #Validform_msg{display:none !important;} 
     			 </style>

<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/goods/page.html';  //TODO
var syslog_events = {
		'click input[type=checkbox]':function(e,value,row,index){ 
			var _this=this;//由于js函数对this绑定的错误处理只能在当前方法中用,不能在里面的方法中用,只有先申明在用
			if($(_this).data("cid")=="1"){//状态:0再售 1已售完    
				if(row.status == 0){ 
					$.ajax({ 
	      				url: '<%=request.getContextPath()%>/goods/disableState.html?id='+row.id, 
	      				context: document.body, 
	      				async: false,
	      				success: function(vo){
		      				if(vo.success){
		      					layer.msg('成功!',{icon:1});
		      					$(_this).prop("checked",false); //$(this).prop("checked",false);这样会报错因为作用域不正确
		      					row.status = 1;
		      				}else{
		      					layer.msg('失败!',{icon:5});
		      					$(_this).prop("checked",true);
		      					row.status = 0;
		      				}
	      				}
	      			});
			}else{//禁用就启用
				$.ajax({
	    			url:'<%=request.getContextPath()%>/goods/enableState.html?id='+row.id,
	    			context: document.body, 
	    			async:false,
	    			success: function(vo){
	      				if(vo.success){
	      					layer.msg('成功!',{icon:1});
	      					$(_this).prop("checked",true);
	      					row.status = 0;
	      				}else{
	      					layer.msg('失败!',{icon:5});
	      					$(_this).prop("checked",false);
	      					row.status = 1;
	      				}
	  				},
	  				error:function(vo){
	  					console.log(vo);
	  				}
	    		});
			}
				
				
			}else{ //菜品状态: 1启用 2禁用
				if($(_this).data("cid") == "2"){
					if(row.integral == 1){
						$.ajax({ 
		      				url: '<%=request.getContextPath()%>/goods/disableIntegral.html?id='+row.id, 
		      				context: document.body, 
		      				async: false,
		      				success: function(vo){
			      				if(vo.success){
			      					layer.msg('成功!',{icon:1});
			      					$(_this).prop("checked",false);  /* $(this).prop("checked",false);这样会报错因为作用域不正确 */
			      					row.integral = 2;
			      				}else{
			      					layer.msg('失败!',{icon:5});
			      					$(_this).prop("checked",true);
			      					row.integral = 1;
			      				}
		      				}
		      			});
						
					}else{
						$.ajax({
							url:'<%=request.getContextPath()%>/goods/enableIntegral.html?id='+row.id,
							context:document.body,
							async: false,
		      				success: function(vo){
			      				if(vo.success){
			      					layer.msg('成功!',{icon:1});
			      					$(_this).prop("checked",true);  //$(this).prop("checked",false);这样会报错因为作用域不正确
			      					row.integral = 1;
			      				}else{
			      					layer.msg('失败!',{icon:5});
			      					$(_this).prop("checked",false);
			      					row.integral = 2;
			      				}
		      				}
							
							
						});
						
					}
					
				}
			}
	
		},
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/goods/deleteGoods.html?id='+row.id, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#goods_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .edit':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/goods/toGoodsEdit.html?id='+row.id;
        	changeRightMenu(url);
    	},
    	'click .upMove':function(e,value,row,index){//上移
     		layer.confirm('确定上移吗?', {btn: ['确定','取消']},  
         	  		function(){
         	  			$.ajax({ 
         	          		url: '<%=request.getContextPath()%>/goods/upMove.html?id='+row.id,
         	          		context: document.body, 
         	          		async: false,
         	          	 	dataType:'json',
         	          		success: function(vo){
         	          			$('#goods_table').bootstrapTable('refresh', null);
         	          			if(vo.success){
         	          				layer.msg('上移成功!',{icon:1});
         	          			}else{
         	          				if(vo.statusCode == 1){
         	          					layer.msg(vo.message,{icon:5});
         	          				}else{
         	          					layer.msg('上移失败!',{icon:5});
         	          				}
         	          				
         	          			}
         	          		}
         	          	});
         	  		}
         	  	);
    	},
    	'click .goodsTop':function(e,value,row,index){//置顶
     		layer.confirm('确定置顶吗?', {btn: ['确定','取消']},  
         	  		function(){
         	  			$.ajax({ 
         	          		url: '<%=request.getContextPath()%>/goods/goodsTop.html?id='+row.id,
         	          		context: document.body, 
         	          		async: false,
         	          	 	dataType:'json',
         	          		success: function(vo){
         	          			$('#goods_table').bootstrapTable('refresh', null);
         	          			if(vo.success){
         	          				layer.msg('置顶成功!',{icon:1});
         	          			}else{
         	          				if(vo.statusCode == 1){
         	          					layer.msg(vo.message,{icon:5});
         	          				}else{
         	          					layer.msg('置顶失败!',{icon:5});
         	          				}
         	          			}
         	          		}
         	          	});
         	  		}
         	  	);
    	}
    	
};


var goods_columns = [
{field: 'goodsname',title: '菜品名称',valign: 'top',sortable: false},
{field: 'cateGoryGoodsName',title: '菜品类型',valign: 'top',sortable: false},
{field: 'price',title: '实际价格(元)',valign: 'top',sortable: false},
{field: 'unit',title: '原价(元)',valign: 'top',sortable: false,formatter:unitFormatter},
{field: 'status',title: '已售完/在售',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter1},
{field: 'integral',title: '禁用/启用',valign: 'top',sortable: false,events:syslog_events, formatter:integralFormatter1},
{field: 'parentId',title: '排序',valign: 'top',sortable: false,events:syslog_events,formatter: operateFormatter2},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#goods_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: goods_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#goods_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#goods_table').bootstrapTable('refresh', null);
	}
	
	//上移和置顶
	 function operateFormatter2(value,row,index){
	   return '<button type="button" class="btn btn-link btn-xs upMove">上移</button><button type="button" class="btn btn-link btn-xs goodsTop" >置顶</button>' 
	}
	
	
	//编辑和删除图标
	function operateFormatter(value,row,index){
		return ['<t:buttonOut url="/goods/toGoodsEdit.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs edit">',
		'编辑','</a></t:buttonOut><t:buttonOut url="/goods/deleteGoods.html"><button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button></t:buttonOut>'].join('');
	}
	
	//状态:0再售 1已售完
	 function operateFormatter1(value,row,index){
		if(row.status==0){//状态:0启用，1禁用，2上架,3下架 
			return '<div class="ace-tab"><input class="ace-ck" data-cid="1" type="checkbox" name="" value="" checked="checked">'+
	 		'<span class="ace-tg"></span></div>';
		}
		if(row.status==1){
			return '<div class="ace-tab"><input class="ace-ck" data-cid="1" type="checkbox" name="" value="" >'+
	 		'<span class="ace-tg"></span></div>';
		}
}
	
	//启用和禁用图标
	function integralFormatter1(value,row,index){
		console.log(value);
		if(value==1){//菜品状态: 1启用 2禁用 选中
			return '<div class="ace-tab"><input class="ace-ck" data-cid="2" type="checkbox" name="" value="" checked="checked">'+
	 		'<span class="ace-tg"></span></div>';
		}
		if(value==2){ //不选中
			return '<div class="ace-tab"><input class="ace-ck" data-cid="2" type="checkbox" name="" value="" >'+
	 		'<span class="ace-tg"></span></div>';
		}
		
	}
	
	  //添加商品
    function goodsAdd(){
    	var url= '<%=request.getContextPath()%>/goods/toGoodsAdd.html';
    	changeRightMenu(url);
	  }
	  
	  function unitFormatter(value,row,index){
		  if(!!!value){
			  return '无';
		  }else{
			  return value;
		  }
		  
	  }
//门店登录才有 在售 售完按钮 门店隐藏禁用启用
$(function(){
   	var type='${sysUser.type}';
   
   	if(type==1){
   		debugger;
   		$('#goods_table').bootstrapTable('hideColumn','status');
   		//平台隐藏排序
   		$('#goods_table').bootstrapTable('hideColumn','parentId');
   		//平台隐藏 在售售完
   		$("#statusHidden").hide();
      	$("#integralHidden").show();
   	}else{
   		debugger;
   		//门店隐藏 状态
   		$('#integralHidden').hide();
   		$("#statusHidden").show();
   		$('#goods_table').bootstrapTable('hideColumn','integral');
   	}
});
    
	
</script>