<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
   
                <div class="form-inline">
                
                	<form id="member_table_form">
                	
                	<div class="form-group">
                        <input type="text" class="form-control" id="datestart" name="startDate" placeholder="开始时间">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="dateend"  name="endDate" placeholder="结束时间">
                    </div>
                     <div class="form-group btn-group-vertical">
                        <button type="button" id="today" class="btn btn-primary">当天</button>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" id="days" class="btn btn-primary">7天内</button>
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" id="months" class="btn btn-primary">本月</button>
                    </div>
                
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="门店名称"  name="storeName">
                    </div>
                   <div class="form-group">
                   <select name="storeType" class="form-control">
                   <option value="">类型</option>
                   <option value="1">加盟店</option>
                   <option value="2">自营店</option>
               		</select>
                 </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    
                      </form>
                      
                	</div>
                	
                	  <!-- 表里面的数据 -->
                <div class="mt20">
                    <table id="member_table"></table>
                </div>
                
                <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                	
                	
                	
<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/statStoreOrder/page.html';   //TODO
var syslog_events = {

};
var member_columns = [
{field: 'storeName',title: '门店名称',valign: 'top',sortable: false},
{field: 'storePhone',title: '手机号',valign: 'top',sortable: false},
{field: 'storeType',title: '门店类型',valign: 'top',sortable: false,formatter:avgDeliverGradeFormatter},
{field: 'storeOderSum',title: '订单数量',valign: 'top',sortable: false},
{field: 'storeSalesMony',title: '销售额',valign: 'top',sortable: false}
];


$('#member_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	showExport:true,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: member_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit, //每页显示多少条
					offset:params.offset, //从第几条索引(20)
					sort:params.sort,
					object:$("#member_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#member_table').bootstrapTable('refresh', null);
	}
	
	
	
	
	
	
	
	function avgDeliverGradeFormatter(value,row,index){
		if(row.storeType==1){
			return ['加盟店'].join('');
		}
		if(row.storeType==2){
			return['自营店'].join('');
		}
	}
	
	
	
    $(function() {
        var start = {
            elem: '#datestart',
            format: 'YYYY/MM/DD',
            min: '2010-1-1', //设定最小日期为当前日期
            max: '2099-06-16', //最大日期
            istime: true,
            istoday: false,
            choose: function(datas) {
                end.min = datas; //开始日选好后，重置结束日的最小日期
                end.start = datas //将结束日的初始值设定为开始日
            }
        };
        var end = {
            elem: '#dateend',
            format: 'YYYY/MM/DD',
            min: '2010-1-1',
            max: '2099-06-16',
            istime: true,
            istoday: false,
            choose: function(datas) {
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        };
        laydate(start);
        laydate(end);

        $(function() {
            /*当天*/
            var datestart = $("#datestart"),
                dateend = $("#dateend");
            $("#today").on("click", function() {
                datestart.val(laydate.now(0, "YYYY/MM/DD 00:00:00"));
                dateend.val(laydate.now(0, "YYYY/MM/DD hh:mm:ss"));
            });
            /*7天内*/
            $("#days").on("click", function() {
                datestart.val(laydate.now(-7, "YYYY/MM/DD hh:mm:ss"));
                dateend.val(laydate.now(0, "YYYY/MM/DD hh:mm:ss"));

            });
            /*本月*/
            $("#months").on("click", function() {
                datestart.val(laydate.now(0, "YYYY/MM/01 00:00:00"));
                dateend.val(laydate.now(0, "YYYY/MM/DD hh:mm:ss"));
            });
        })
    })
	
	
</script>