<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<meta charset="UTF-8">
		<title>门店详情</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/common.css" rel="stylesheet" />
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/storelist.css" rel="stylesheet" />
		<style type="text/css">
		.week-nav {
    width: 100%;
    height: .8rem;
    white-space: nowrap;
    overflow-x: auto;
    overflow-y: hidden;
    background: #393025;
    font-size: 0;
}
.week-nav .item {
    width: 18.5%;
    height: .8rem;
    display: inline-block;
    color: #fff;
    font-size: .26rem;
    line-height: .8rem;
    text-align: center;
}

.week-nav .on {
    background: #fff;
    color: #ff5000;
}

.wrap-goods{
    overflow-y: auto;
    overflow-x: hidden;
    height: 8.4rem;
}
.goods-num .add-goods, .goods-num .reduce-goods{
	width: 0.36rem;
    height: 0.36rem;
}
		</style>
	</head>
	<body>
		<div class="wrap">
			 <!-- <header id="header" class="relative">
	            <a class="absolute back" href="storeHome.html" title="门店"></a>
	            <div class="tit tit-l">
	                	门店
	            </div>
	            <a class="absolute more" href="javascript:;" title="门店"></a>
	         </header> -->
	        
	       <section class='clearfix storelist' style='background: #FBD14B;'>
        		<div class='container clearfix'>
        			<input type="hidden" value="${store.id}" id="storeId">
        			<input type="hidden" value="${tableId}" id="tableId">
        			<ul class='storemsg-wrap clearfix'>
        				<li class='clearfix'> 
        					<div class='fl storemsg-pic'>
        						<img src="${store.storePhoto}" />
        					</div>
        					<div class='fl storemsg-cont clearfix'>
        						<h1 class='txt-ellipsis'>${store.storeName}</h1>
        						<p class='txt-ellipsis clearfix' >
        						${store.storePhone}
        						</p>
        						<p class='txt-ellipsis clearfix'>
	        						${store.address}
        						</p>
        						<div class='activity-cut clearfix w-100'>
        							<c:forEach items="${store.parts}" var="part">
		        							<c:if test="${part.type eq '1'}">
		        							满<fmt:formatNumber type="number" value="${part.full}"  pattern="0.0" maxFractionDigits="1"/>
			        						减<fmt:formatNumber type="number" value="${part.substact}"  pattern="0.0" maxFractionDigits="1"/>;
		        							</c:if>
		        					</c:forEach>
        						</div>
    							<div class='clearfix' >
    								<div class='fl activity-discount txt-ellipsis'>
			        					<c:forEach items="${store.parts}" var="part">
			        							<c:if test="${part.type eq '2'}">
			        								<fmt:formatNumber type="number" value="${part.discount}"  pattern="0.0" maxFractionDigits="1"/>折
			        							</c:if>
			        					</c:forEach>
	    							</div>
	    							<%-- <div class='fr txt-ellipsis ' >${store.distance}km</div> --%>
    							</div>
        					</div>
        				</li>
        			</ul>
        		</div>
        	</section>
<!--         	<section> -->
<!-- 	        	<nav class="week-nav"> -->
<!-- 					<ul class="list week-list"> -->
<%-- 						<li class="item item1 ${week==1?' on':'' }" data-week="1" id="aaa">星期一</li> --%>
<%-- 						<li class="item item2 ${week==2?' on':'' }" data-week="2">星期二</li> --%>
<%-- 						<li class="item item3 ${week==3?' on':'' }" data-week="3">星期三</li> --%>
<%-- 						<li class="item item4 ${week==4?' on':'' }" data-week="4">星期四</li> --%>
<%-- 						<li class="item item5 ${week==5?' on':'' }" data-week="5">星期五</li> --%>
<%-- 						<li class="item item6 ${week==6?' on':'' }" data-week="6">星期六</li> --%>
<%-- 						<li class="item item7 ${week==7?' on':'' }" data-week="7">星期日</li> --%>
<!-- 					</ul> -->
<!-- 				</nav> -->
<!--         	</section> -->
        	
	        
	        <section class='clearfix storemsg' id="mainContent">
	        	<ul class='storemsg-tab clearfix fl' id="leftNav">
	        		 <c:forEach items="${categoryList}" var="category" varStatus="status">
		          		<li class="${status.first?'active':''}">
		          		${category.cgoodsName}
		          		<%-- <a href="#categoryType${status.count}">${category.cgoodsName}</a> --%>
		          		</li>
	        		</c:forEach>
	           </ul>
	        	<div class='storemsg-div clearfix fl wrap-goods bpd' id="goodsList">
	        	<c:forEach items="${categoryList}" var="category" varStatus="status">
	        		 <div class="type-item">
		        		<h1><span id="categoryType${status.count}">${category.cgoodsName}</span></h1>
		        		<ul class='clearfix storemsg-ul' > 
		        			<c:forEach items="${category.goodsList }" var="goods" varStatus="status2">
			        			<li class='clearfix' data-store-gid="${goods.storeGoodsId }">
			        				<a class='fl storemsg-img' href='javascript:;'>
			        					<img src="${goods.goodsPic}" />
			        				</a>
			        				<div class='storemsg-info fl'>
			        					<a class='txt-ellipsis' href='javascript:;'>${goods.goodsname }</a>
			        					<p class='txt-ellipsis'>${goods.intro }</p>
			        					<p>销量：${goods.sellnum } 份</p>
			        					<div class='googs-price fl'>
			        						<div class='now-price fr'>￥<fmt:formatNumber type="number" value="${goods.price}" pattern="0.00" /></div>
			        						<div class='original-price fr'>￥<fmt:formatNumber type="number" value="${goods.unit}" pattern="0.00" /></div>
			        					</div>
			        					<div class='goods-num fr'>
			        						<a class='add-goods' data-goodsid="${goods.id }" data-id="${goods.id }" data-sgid="${goods.storeGoodsId }" data-sid="${goods.storeId }" href='javascript:;' ><img src="<%=request.getContextPath()%>/h5/images/btn-add.png" /></a>
		<!-- 	        						<span>1</span> -->
		<%-- 	        						<a class='reduce-goods' href='javascript:;'><img src='<%=request.getContextPath()%>/h5/images/btn-cut.png' /></a> --%>
			        					</div>
			        				</div>
			        			</li>
		        			</c:forEach>
		        			<c:if test="${status.last|| empty category.goodsList}">
		        			<li class='clearfix' style="height:1.6rem;"></li>
		        			</c:if>
		        		</ul>
		        	</div>
	        	</c:forEach>
	        	</div>
	        	<div class='shopcar' id='catStat'>
	        		<div class='fl relative shopcar-icon jump-shopping-car'>
	        			<img src="<%=request.getContextPath()%>/h5/images/car.png" />
	        			<div class='absolute carnum'>${carStat.num }</div>
	        		</div>
	        		<div class='carprice fl'>￥<fmt:formatNumber type="number" value="${carStat.amountPrice }" pattern="0.00" maxFractionDigits="2"/>  </div>
	        		<div class='select-ok fr jump-shopping-car'>选好了</div>
	        	</div>
	        </section>
	    </div>    
	    
		<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	        <script>
				var oHtml = document.getElementsByTagName('html')[0];
				var screenW = document.documentElement.offsetWidth || document.body.offsetWidth;
				oHtml.style.fontSize = 100 * screenW / 720 + "px";
				
			</script>
			<script type="text/javascript">
			
			var storeId = $("#storeId").val();
			var tableId = $("#tableId").val();
			var storeInfo ={
				"storeId":storeId,
				"tableId":tableId
			};
			sessionStorage.setItem("storeInfo",storeInfo);
			
			$(function(){
/* // 				//点击头部星期导航条
// 				$(".week-list").on("click",".item",function() {
// 					$(this).parent().find(".item").removeClass("on");
// 					$(this).addClass("on");
// //						toggleBookBtn();
// 					var week = $(this).data("week");
// 					tips({
// 						skin : "loading"
// 					}); //加载进度条
// 					$.ajax({
// 						url : "api/getStoreGoodsAndCategory.html",
// 						type : "post",
// 						async:true,
// 						data : {
// 							storeId:storeId,
// 							week : week
// 						},
// 						dataType : "json",
// 						success : function(result) {
// 							tips.loadClose(); //关闭进度条
// 							if (result && result.success) {
// 								refreshMainContent(result.data);
// 								mainContentListen();
// 							}
// 						},
// 						error : function() {
// 							tips.loadClose(); //关闭进度条
// 							tips({
// 								message:"系统繁忙！",
// 								skin : "msg"
// 							}); 
// 						}
// 					});
// 				}); */
				
				mainContentListen();
			})
			
			function mainContentListen(){
				add2CartListener();
				srollListen();
				jumpUrlListen();
			}
			
			//刷新主体区域
			function  refreshMainContent(data){
				var temp = null;
				var main = [];//主体
				var leftNav = [];//左侧导航
				var category = [];//商品分类
				var goods = [];//商品
				
				var categoryList = data.categoryList;//商品及分类
				var totalNum = data.carStat.num || 0;//购物车总数
				var amountPrice = data.carStat.amountPrice || 0;//购物车总价格
				
				temp ="<ul class='storemsg-tab clearfix fl' id='leftNav'>";
	        	main.push(temp);
	        	for(var i=0;i<categoryList.length;i++){
	        		var active =i==0?"active":"";
	        		var cgoodsName =categoryList[i].cgoodsName;
		          	temp ='<li class="'+active+'">\
		          		<a href="#categoryType'+(i+1)+'">'+cgoodsName+'</a>\
		          		</li>';
		          	leftNav.push(temp);
	        	}
	        	main.push(leftNav.join(""));
	           temp = "</ul>\
	        	<div class='storemsg-div clearfix fl wrap-goods bpd' id='goodsList'>";
	        	main.push(temp);
	        	for(var i=0;i<categoryList.length;i++){
	        		var cgoodsName = categoryList[i].cgoodsName;
	        		var goodsList = categoryList[i].goodsList;
	        		
	        		temp ='<div class="type-item">\
		        		<h1><span id="categoryType'+(i+1)+'">'+cgoodsName+'</span></h1>\
		        		<ul class="clearfix storemsg-ul" >';
	        		category.push(temp);
	        		goods = [];//清空
	        		for(var j=0;j<goodsList.length;j++){
	        			var goodsId = goodsList[j].id;
	        			var goodsPic = goodsList[j].goodsPic || "";
	        			var goodsname = goodsList[j].goodsname || "";
	        			var intro = goodsList[j].intro || "";
	        			var sellnum = goodsList[j].sellnum || 0;
	        			var unit = goodsList[j].unit || 0;//原价
	        			var price = goodsList[j].price || 0;//实卖价
	        			var unit2 = parseFloat(unit).toFixed(2);
	        			var price2 = parseFloat(price).toFixed(2);
	        			var storeGoodsId = goodsList[j].storeGoodsId;
	        			temp="<li class='clearfix' data-store-gid='"+storeGoodsId+"'>\
		        				<a class='fl storemsg-img' href='javascript:;'>\
		    					<img src='"+goodsPic+"' />\
		    				</a>\
		    				<div class='storemsg-info fl'>\
		    					<a class='txt-ellipsis' href='javascript:;'>"+goodsname+"</a>\
		    					<p class='txt-ellipsis'>"+intro+"</p>\
		    					<p>销量："+sellnum+" 份</p>\
		    					<div class='googs-price fl'>\
		    						<div class='now-price fr'>￥"+ price2 +"</div>\
		    						<div class='original-price fr'>￥"+ unit2 +"</div>\
		    					</div>\
		    					<div class='goods-num fr'>\
		    						<a class='add-goods' data-goodsid='"+goodsId+"' href='javascript:;' ><img src='../images/btn-add.png'/></a>\
		    					</div>\
		    				</div>\
		    			</li>";
		    			goods.push(temp);	
	        		}
	        		category.push(goods.join(""));
	        		temp = (i==(categoryList.length-1))|| goodsList.length<1?'<li class="clearfix" style="height:1.6rem;"></li>':'';
	        		category.push(temp);	
	        		temp ='</ul>\
		        		</div>';
	        		category.push(temp);
	        	}
	        	main.push(category.join(""));
	        	temp ="</div><div class='shopcar' id='catStat'>";
	        	main.push(temp);
		        temp =refreshCarStat(totalNum,amountPrice);
		        main.push(temp);
		        temp = "</div>";
		        main.push(temp);
		        $("#mainContent").html(main.join(""));
			}
			
			//刷新购物车统计数据
			function refreshCarStat(totalNum,amountPrice){
				var html ="";
				html = "<div class='fl relative shopcar-icon jump-shopping-car'>\
	        			<img src='../images/car.png' />\
	        			<div class='absolute carnum'>"+totalNum+"</div>\
	        		</div>\
	        		<div class='carprice fl'>￥"+amountPrice+"</div>\
	        		<div class='select-ok fr jump-shopping-car'>选好了</div>\
	        	</div>";
        		return html;
			}
			
			//添加到购物车按钮监听
			function add2CartListener(){
				
				//点击＋号加入购物车
				$(".add-goods").on("click",function(){
					var _that = $(this);
					var num = 1;
					var goodsId = _that.data("goodsid");
					var sid = _that.data("sid");
					var sgid = _that.data("sgid");
					var tableid=tableId;
				/* 	var vo = new Add2CarVo(); */
					var vo={};
					vo.goodsId = goodsId;
					vo.num = num;
					vo.storeGoodsId =sgid;
					vo.storeId = sid;
					vo.tableId=tableid;
					vo.fromType="eatingInStore";
					addToCar({
						"data":vo,
						"success":function(data){
							console.log(data);
							tips({
								message:"已加入购物车",
								skin : "msg"
							}); 
							var totalNum = data.num || 0;
							var amountPrice = data.amountPrice || 0;
							$("#catStat .carprice").html(amountPrice.toFixed(2));
							$("#catStat .carnum").html(totalNum);
						},
						"error":function(result){
							tips({
								message : result.msg,
								skin : "msg"
							});
						}
					});
					return false;//阻止冒泡
				});
			}
			
			function srollListen(){
				$("#leftNav").on("touchend","li",function(){
					var $types = $("#goodsList .type-item");
					var start = $types.eq(0).offset().top;
					var curr = $types.eq($(this).index()).offset().top;
					scrollTop = curr -start;
					$("#goodsList").stop().animate({
	                    // 给具有相同效果的元素添加 共同的样式 jd
	                    "scrollTop" : scrollTop
	                },1000);
				});
				
				
				var TOP = 0;
				var _lis = $("#goodsList li");
				var _types = $("#goodsList .type-item");
				var height = _lis.height();
				var offsets = 0.125;//偏差纠正值
				$("#goodsList").scroll(function() {
					var top = $("#goodsList").scrollTop();
					var start = _types.eq(0).offset().top;
					for(var i=_types.length-1;i>=0;i-- ){
						var t = parseInt(_types.eq(i).offset().top -start);
						if(top>=t){
							$("#leftNav li").eq(i).addClass("active").siblings().removeClass("active");
							break;
						}
					}
				});
				
				$("#goodsList").on("click",".storemsg-ul li",function(){
					debugger;
					var sgid = $(this).data("store-gid");//门店菜品id
					var currentWeek = $(".week-list .on").data("week");
					var storeId = $("#storeId").val();
					if(sgid){
						window.location.href ="goodsDetail.html?sgid="+sgid+"&&fromType=2&&storeId="+storeId+"&&tableId="+tableId;
					}
					
				});
			}
			
			function jumpUrlListen(){
				$(".jump-shopping-car").on("click",function(){
					window.location.href = "../shopcar/shopCar.html?storeId="+storeId+"&tableId="+tableId;
				});
			}
			
			function addToCar(param){
				tips({
					"skin":"loading"
				});
				var root = getWebRootPath();
				$.ajax({
					url:root+"/h5/shopcar/api/add2Cart2.html",
					type:"get",
					data:param.data,
					dataType:"json",
					success:function(result){
						tips.loadClose(); //关闭进度条
						if(result){
							if(result.success){
								param.success(result.data);
							}else{
								param.error(result);
							}
						}else{
							tips({
								message : "系统繁忙！",
								skin : "msg"
							});
						}
					},
					error:function(){
						tips.loadClose(); //关闭进度条
						tips({
							message : "系统繁忙！",
							skin : "msg"
						});
					}
				});
			}
			
			function getWebRootPath() {
			    var curWwwPath = window.document.location.href;
			    var pathName = window.document.location.pathname;
			    var pos = curWwwPath.indexOf(pathName);
			    var localhostPaht = curWwwPath.substring(0, pos);
			    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
			    return (localhostPaht + projectName);
			}
			
		</script>	
	</body>
</html>
