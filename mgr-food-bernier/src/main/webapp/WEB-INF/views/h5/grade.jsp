<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="mark">
	<!-- 0颗星 -->
	<c:if
		test="${param.averageGrades>=0.0&&param.averageGrades<1.0 }">
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
	</c:if>
	<!-- 1颗星 -->
	<c:if
		test="${param.averageGrades>=1.0&&param.averageGrades<2.0 }">
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
	</c:if>
	<!-- 2颗星 -->
	<c:if
		test="${param.averageGrades>=2.0&&param.averageGrades<3.0 }">
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
	</c:if>
	<!-- 3颗星 -->
	<c:if
		test="${param.averageGrades>=3.0&&param.averageGrades<4.0 }">
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-2"></span>
		<span class="satrs satrs-2"></span>
	</c:if>
	<!-- 4颗星 -->
	<c:if
		test="${param.averageGrades>=4.0&&param.averageGrades<5.0 }">
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-2"></span>
	</c:if>
	<!-- 5颗星 -->
	<c:if test="${param.averageGrades>=5.0}">
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
		<span class="satrs satrs-1"></span>
	</c:if>
	<em class="seller-star-f">${param.averageGrades }</em>
</div>