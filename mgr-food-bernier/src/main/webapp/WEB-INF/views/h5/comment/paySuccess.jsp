<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html lang="en">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
	<head>
		<meta charset="UTF-8">
		<title>饭好粥到-支付结果</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/common.css" rel="stylesheet" />
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/header.css" rel="stylesheet" />
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/games.css" rel="stylesheet" />
		<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/h5/js/style.min.js" type="text/javascript"></script>
	</head>

	<body>
		<div class="wrap"  >
		 	<header id="header" class="relative" <!-- style="background: #FFFFFF; " -->>
		 		<!-- <a class="absolute close" href="../index.html" title="门店"></a>
				<div class="tit tit-close-l">
					订单详情
				</div>
				<a class="absolute more" href="../index.html" title="门店"></a>  -->
			</header>  
			<section class="games-wrap" >
				<form  action="orderList.html?v=1"  method="post" >
					<input type="hidden" value="${order.orderId}" name="orderId" id="orderId"/>
					<input type="hidden" value="${order.storeId}" name="storeId" id="storeId"/>
					<div class="games-tit">
						<img src="<%=request.getContextPath()%>/h5/images/icon-paysuc.png" />
						<p>
							<c:if test="${order.payStatus eq '0'}">
								未支付
							</c:if>
							<c:if test="${order.payStatus eq '1'}">
								支付成功
							</c:if>
							<c:if test="${order.payStatus eq '2'}">
								支付失败
							</c:if>
							<c:if test="${order.payStatus eq '3'}">
								退款中
							</c:if>
							<c:if test="${order.payStatus eq '4'}">
								已经退款
							</c:if>
							<c:if test="${order.payStatus eq '5'}">
								退款失败
							</c:if>
						</p>
					</div>
					<div class="pay-money">
						<span class='fl'>支付金额</span>
						<span class='fr'>￥${order.payAmount}元</span>
					</div>
					<ul>
						<li>订单编号: ${order.payNumber }</li>
						<li>支付状态: <span>
							<c:if test="${order.payStatus eq '0'}">
								未支付
							</c:if>
							<c:if test="${order.payStatus eq '1'}">
								支付成功
							</c:if>
							<c:if test="${order.payStatus eq '2'}">
								支付失败
							</c:if>
							<c:if test="${order.payStatus eq '3'}">
								退款中
							</c:if>
							<c:if test="${order.payStatus eq '4'}">
								已经退款
							</c:if>
							<c:if test="${order.payStatus eq '5'}">
								退款失败
							</c:if>
						</span></li>
						<li>交易时间:${order.payDate}</li>
						<li>支付方式: 
							<c:if test="${order.payType eq '1' }">支付宝支付</c:if>
							<c:if test="${order.payType eq '2' }">微信支付</c:if>
							<c:if test="${order.payType eq '3' }">现金支付</c:if>
							<c:if test="${order.payType eq '4' }">余额支付</c:if>
						</li>
					</ul>
					<button type="submit">确定</button>
				</form>
			</section>
			<footer class="games-footer w-100 relative" id="foot" >
				<%-- <img src="<%=request.getContextPath()%>/h5/images/games-bg.png" /> --%>
				<img src="${sysParm.videoLink}" />
				<a class="absolute coming" href='http://${sysParm.advertImage}'>
					<img src="<%=request.getContextPath()%>/h5/images/games-coming.png" />
				</a>
				<a class="absolute close" id="closeBtn">
					<!-- <img src="${sysParm.imgLinkA}" /> -->
					<img src="<%=request.getContextPath()%>/h5/images/close-btn.png" />
				</a>
				<p class="absolute">h5小游戏点击进入</p>
			</footer>
		</div>

		<script>
			var oHtml = document.getElementsByTagName('html')[0];
			var screenW = document.documentElement.offsetWidth || document.body.offsetWidth;
			oHtml.style.fontSize = 100 * screenW / 720 + "px";
		</script>
		<script src="<%=request.getContextPath()%>/h5/js/view/paySuccess.js"></script>
	</body>

</html>