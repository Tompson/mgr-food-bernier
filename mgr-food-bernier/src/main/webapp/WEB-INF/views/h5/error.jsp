<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>饭好粥道-错误页面</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"
	name="viewport">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no">
	<style type="text/css">
		.mlr20{
			margin: 0 .2rem;
		}
	</style>
</head>
<body>
	<div class="wrap">
		<div class="build-box" align="center">
			<div style="height:2rem;"></div>
			<img class="build-img" style="width: 70%;height: 100%;"
				src="<%=request.getContextPath()%>/h5/images/error.jpg" alt="">
			<p class="mlr20"><span id="timer">5</span> 秒后自动跳转首页</p>
			<a class="mlr20" href="javascript:window.history.go(-1);">返回</a>
			<a class="mlr20" href="<%=request.getContextPath()%>${url }">回到首页</a>
		</div>
	</div>
	<script type="text/javascript">
		var t=5;
		var timer=setInterval(function(){
			document.getElementById("timer").innerHTML=t;
			if(t<=1){
				clearInterval(timer);
				window.location.href="<%=request.getContextPath()%>${url }";
				return;
			}
			t--;
		}, 1000);
	</script>
</body>
</html>