<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>
        饭好粥道-订单评价
    </title>
    <meta name="keywords" content="订单" />
    <meta name="description" content="订单" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/mycss.css" />
    <script src="<%=request.getContextPath() %>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <header id="header" class="header">
            <a class="back" href="orderList.html?v=3" title="我的订单"></a>
            <div class="tit">
                订单评价
            </div>
            <button class="save" type="button">提交</button>
        </header>
        <div class="evate">
            <div class="top pd20">
            	<input type="hidden" id="order-id" value="${orderId }"/>
                <label for="">菜品评分：</label>
                <div class="stars stars_one">
                    <span data-sId="1" class="stars-icon stars-2"></span>
                    <span data-sId="2" class="stars-icon  stars-2"></span>
                    <span data-sId="3" class="stars-icon  stars-2"></span>
                    <span data-sId="4" class="stars-icon  stars-2"></span>
                    <span data-sId="5" class="stars-icon  stars-2"></span>
                   	<span class="score"><em class="choose-goods-score choose-txt"></em></span>
                </div>
            </div>
             <div class="it  mt10">
                <textarea class="eva-text" id="goodsContent" name="" rows="10" placeholder="点评一下菜品吧！" maxlength="300" ></textarea>
            </div>
            
          <!--   <div class="top pd20">
                <label for="">配送评分：</label>
                <div class="stars stars_two">
                    <span data-sId="1" class="stars-icon stars-2"></span>
                    <span data-sId="2" class="stars-icon  stars-2"></span>
                    <span data-sId="3" class="stars-icon  stars-2"></span>
                    <span data-sId="4" class="stars-icon  stars-2"></span>
                    <span data-sId="5" class="stars-icon  stars-2"></span>
                    <span class="score"><em class="choose-deliver-score choose-txt"></em></span>
                </div>
            </div>
			 <div class="it  mt10">
                <textarea class="eva-text" id="deliverContent" name="" rows="8" placeholder="点评一下配送服务吧！" maxlength="300" ></textarea>
            </div> -->
            
            <div class="top pd20">
                <label for="">厨师评分：</label>
                <div class="stars stars_three">
                    <span data-sId="1" class="stars-icon stars-2"></span>
                    <span data-sId="2" class="stars-icon  stars-2"></span>
                    <span data-sId="3" class="stars-icon  stars-2"></span>
                    <span data-sId="4" class="stars-icon  stars-2"></span>
                    <span data-sId="5" class="stars-icon  stars-2"></span>
                    <span class="score"><em class="choose-chief-score choose-txt"></em></span>
                </div>
            </div>
			 <div class="it  mt10">
                <textarea class="eva-text" id="chiefContent" name="" rows="10" placeholder="点评一下厨师吧！" maxlength="300" ></textarea>
            </div>
           
        </div>
    </div>
    <script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
    <script src="<%=request.getContextPath()%>/h5/js/view/eatingEvaluate.js"></script>
</body>

</html>