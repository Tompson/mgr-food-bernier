<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>饭好粥道-我的订单</title>
<meta name="keywords" content="订单" />
<meta name="description" content="订单" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/common.css" />

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/user.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
	<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/storelist.css" />
<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/h5/js/ui/tip.min.js"></script>
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
	<script>
		var requestUrl="<%=request.getContextPath()%>";
	</script>
</head>

<body>
	<div class="wrap">
		<header id="header" class="header" style="">
		 <%-- <c:if test="${fromType eq '1' }">
		 	<a class="back" href="../h5/personal.html?v=1" style='top:0;'></a>
		 	<div class="tit">订单列表</div>
		 </c:if>
		  <c:if test="${fromType eq '0' }">
			 <a class="back" href="../personal.html?v=1" style='top:0;'></a>
			 <div class="tit">订单列表</div>
		 </c:if> --%>
			<a class="back" href="<%=request.getContextPath()%>/h5/personal.html?v=1" style='top:0;'></a>
		 	<div class="tit">订单列表</div>
		</header>
		<div class="od-box">
			<ul class='storelist-wrap clearfix' id="orderList" pageNo="${page.pageNo} " pageSize="${page.pageSize}" totalPage="${page.totalPage}">
				<c:forEach items="${page.results }" var="orderVo">
					<div class="od-item od-border">
						 <section class='ordertit clearfix'>
				        	<div class='container clearfix'>
				        		<div class='fl ordertit-fl'>
				        			<h1>商家信息</h1>
				        			<p>商家名称：${orderVo.storeName }</p>
				        			<p>商家电话：${orderVo.storePhone }</p>
				        		</div>
				        		<img class='fl icon-user' src="${orderVo.storePhoto }" />
				        	</div>
				        </section>
					
						<div class="top pd20 clear">
							<p class="fl">下单时间&nbsp;&nbsp;${orderVo.formatOrderTime }</p>
							<span class="fr color-ff5000">${orderVo.converedOrderStatus }</span>
						</div>
						<div class="top pd20 clear">
							<p class="fl">送达时间&nbsp;&nbsp;${orderVo.formatBookTime }</p>
							<p class="fr">
							<c:if test="${orderVo.payType==1 }">
								支付宝支付
							</c:if>
							<c:if test="${orderVo.payType==2 }">
								微信支付
							</c:if>
							<c:if test="${orderVo.payType==3 }">
								现金支付
							</c:if>
							<c:if test="${orderVo.payType==4 }">
								余额支付
							</c:if>
							</p>
						</div>
						<div class="info pd20">
							<h6 class="tit">商品列表</h6>
							<c:forEach items="${orderVo.orderGoodsList }" var="orderGoods">
								<a href="#"> <script type="text/javascript">
									//TODO 订单详情链接
								</script>
									<div class="op-item">
										<p class="name">${orderGoods.googsName }</p>
										<i class="num">x${orderGoods.quantify }</i><em class="price">￥${orderGoods.pricePay }</em>
									</div>
								</a>
							</c:forEach>
							<div class="op-item">
								<p class="name">配送费</p>
								<em class="price">￥${orderVo.formatOutsideMoney }</em>
							</div>
							<div class="op-item">
								<p class="name">减免</p>
								<em class="price">￥-${orderVo.discountValue }</em>
							</div>
						</div>
						<div class="money pd20">
							<p class="my-price color-ff5000">总计:￥${orderVo.formatTotalAmount }</p>
							<!-- 订单状态  待付款 配送中  已完成  已取消-->
							<c:if test="${orderVo.converedOrderStatus=='待付款'}">
								<a class="btn btn-yellow cancleOrder"
									data-order-id="${orderVo.orderId }" href="#">取消订单</a>
								<a class="btn btn-red continuePay"
									data-order-id="${orderVo.orderId }" href="#">去支付</a>
							</c:if>
							<c:if test="${orderVo.converedOrderStatus=='配送中'||orderVo.converedOrderStatus=='配送完成'}">
								<a class="btn btn-red sureReceive"
									data-order-id="${orderVo.orderId }" href="#">确认收货</a>
							</c:if>
							 <c:if test="${orderVo.converedOrderStatus=='已完成'}"> 
								<a class="btn btn-red toAdvice" data-order-id="${orderVo.orderId }" href="#">去评价</a>
									<em class="points">(评论可抽奖)</em>
							</c:if> 
							<c:if test="${orderVo.converedOrderStatus=='已评价'}">
								<a class="btn btn-gray" data-order-id="${orderVo.orderId }">已评价</a>
							</c:if>
							<c:if test="${orderVo.converedOrderStatus=='已取消'}">
								<a class="btn btn-gray" data-order-id="${orderVo.orderId }">已取消</a>
							</c:if>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

	</div>
	
	<script src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script src="<%=request.getContextPath()%>/h5/js/view/orderList.js"></script>



</body>

</html>