<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>订单确认</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link type="text/css" href="<%=request.getContextPath() %>/h5/css/common.css" rel="stylesheet" />
		<link type="text/css" href="<%=request.getContextPath() %>/h5/css/storelist.css" rel="stylesheet" />
	</head>
	<body>
		<div class="wrap">
			 <header id="header" class="relative">
	            <a class="absolute back" href="javascript:;" title="门店"></a>
	            <div class="tit tit-l">
	                	门店
	            </div>
	            <a class="absolute more" href="javascript:;" title="门店"></a>
	         </header>
	        
	        <section class="dining">
	        	<div class='border-top border-bt'>
	        		<div class='container dining-wrap'>
	        			<span>就餐方式</span>
	        			<a class='fr more-btn' href='javascript:;'><img src="<%=request.getContextPath() %>/h5/images/icon-tomore.png" /></a>
	        			<span class='fr'>堂食</span>
	        		</div>
	        	</div>
	        	<div class='border-bt'>
	        		<div class='container dining-wrap'>
	        			<span>桌子名称</span>
	        			<span class='fr'>7号桌</span>
	        		</div>
	        	</div>
	        	<div class='border-bt'>
	        		<div class='container dining-wrap'>
	        			<span>在线支付</span>
	        			<a class='fr online-payment' href='javascript:;'><img src="<%=request.getContextPath() %>/h5/images/select.png" /></a>
	        		</div>
	        	</div>
	        	<div class='border-bt'>
	        		<div class='container dining-wrap'>
	        			<span>选择优惠</span>
	        			<a class='fr more-btn' href='javascript:;'><img src="<%=request.getContextPath() %>/h5/images/icon-tomore.png" /></a>
	        			<span class='fr'>平台代金券</span>
	        		</div>
	        	</div>
	        	<div class='border-bt'>
	        		<div class='container dining-wrap'>
	        			<span>选择代金券</span>
	        			<a class='fr more-btn' href='javascript:;'><img src="<%=request.getContextPath() %>/h5/images/icon-tomore.png" /></a>
	        		</div>
	        	</div>
	        	<div class='dining-message'>
	        		<textarea placeholder="备注" class='input-areatext'>
	        			
	        		</textarea>
	        	</div>
	        	
	        	<div class='dishes'>
	        		<div class='container'>
	        			<h1 class='border-bt'>菜品清单</h1>
		        		<div class='dishes-list border-bt'>
		        			<span>法式住扒焗饭</span>
		        			<span>×1</span>
		        			<span>￥19.5</span>
		        		</div>
		        		<div class='dishes-list border-bt'>
		        			<span>辣条</span>
		        			<span>×20</span>
		        			<span>￥100</span>
		        		</div>
	        		</div>
	        	</div>
	        	
	        </section>
	        
	        <section class='submit-order'>
	        	<div class='fl'>￥12.22</div>
	        	<a href='javascript:;' class="fr">提交订单</a>
	        </section>
	    </div>    
		<script>
			var oHtml = document.getElementsByTagName('html')[0];
			var screenW = document.documentElement.offsetWidth || document.body.offsetWidth;
			oHtml.style.fontSize = 100 * screenW / 720 + "px";
		</script>
	</body>
</html>
