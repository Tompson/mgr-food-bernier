<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>饭好粥道-代金券分享</title>
<meta name="keywords" content="代金券分享" />
<meta name="description" content="代金券分享" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/user.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
</head>

<body>
    <div class="wrap">
        <!-- 头部 -->
        <header id="header" class="header">
            <a class="back" href="javascript:history.go(-1);"></a>
            <div class="tit">
               代金券分享
            </div>
        </header>
        <section class="box cupon-wrap pd20">
        	<input value="${voucher.id }" id="voucherId" type="hidden" />
            <h2 class="cupon-num"></h2>
            <div >
            <div class="cupon-item" >
                <div class="cupon-top clear">
                    <div class="fl cupon-price">
                        <em>￥</em>
                        <fmt:formatNumber type="number" pattern="0.00" value="${voucher.voucherMoney }"/>
                    </div>
                     <div class="fr cupon-btn">
                      	  可用
                    </div>
                </div>
                <div class="cupon-down clear">
<!--                     <div class="fl cupon-date"> -->
<!--                        	 有效期至 2016/01/04 -->
<!--                     </div> -->
	                <c:if test="${voucher.drawVoucher!=null }">
                    <div class="fr cupon-if">
                      	【 满<fmt:formatNumber type="number" pattern="0.00" value="${voucher.drawVoucher }"/>元可用】
                    </div>
	                </c:if>
                </div>
            </div>
            </div>
            <div class="user-address-add">
	            <a href="javascript:;" id="shareTip">分享</a>
	        </div>
        </section>
        <!-- 分享 -->
		<div class="wxtip" id="JweixinTip">
			<span class="wxtip-icon"></span>
			<p class="wxtip-txt">
				请点击右上角<br />来进行分享！！！
			</p>
		</div>
    </div>
    <script src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
    <script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
    var configInfo =null;
    var url = location.href.split('#')[0];
    $(function(){
	 	// 分享提示隐藏
		$("#JweixinTip").on("click", function() {
			$(this).hide();
		});
	 	
	 	$("#shareTip").on("click",function(){
	 		$("#JweixinTip").show();
	 	});
		//获取微信jssdk配置
		$.ajax({
			url:"../api/initWxConfig.html",
			type:"get",
			data:{url : url},
			dataType:"json",
			success:function(result){
				if (result && result.success) {
					configInfo = result.data;
					wxInit();
				}
			}
		});
		function wxInit() {
			wx.config({
				 debug : false, //
				// 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				appId : configInfo.appId, // 必填，公众号的唯一标识
				timestamp : configInfo.timestamp, // 必填，生成签名的时间戳
				nonceStr : configInfo.noncestr, // 必填，生成签名的随机串
				signature : configInfo.signature,// 必填，签名，见附录1
				jsApiList : configInfo.jsApiList
			// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
			});
		}
		wx.ready(function(){
			var title = "收到一张优惠券";
			var desc = "你的好友赠送了一张饭好粥道的优惠券，赶快来领取吧!";
			var imgUrl = "";
			linkUrl = configInfo.url.replace("voucherShare","voucherGain");
			// 分享到朋友
			wx.onMenuShareAppMessage({
				title : title, // 分享标题
				desc : desc, // 分享描述
				link : linkUrl, // 分享链接
				imgUrl : imgUrl, // 分享图标
				type : 'link', // 分享类型,music、video或link，不填默认为link
				dataUrl : '', // 如果type是music或video，则要提供数据链接，默认为空
				success : getShareAward,
				cancel : function() {
					// 用户取消分享后执行的回调函数
					console.log("分享到朋友-->取消");
					$("#JweixinTip").hide();
				}
			});
			// 分享到朋友圈
			wx.onMenuShareTimeline({
				title : title, // 分享标题
				link : linkUrl, // 分享链接
				imgUrl : imgUrl, // 分享图标
				success : getShareAward,
				cancel : function() {
					// 用户取消分享后执行的回调函数
					console.log("分享到朋友圈-->取消");
					$("#JweixinTip").hide();
				}
			});
			// 分享到腾讯微博
			wx.onMenuShareWeibo({
				title : title, // 分享标题
				desc : desc, // 分享描述
				link : linkUrl, // 分享链接
				imgUrl : imgUrl, // 分享图标
				success : getShareAward,
				cancel : function() {
					// 用户取消分享后执行的回调函数
					console.log("分享到腾讯微博-->取消");
					$("#JweixinTip").hide();
				}
			});
		});
    });
    
    function getShareAward(){
    	$("#JweixinTip").hide();
    	//更新用户ID
    	var voucherId = $("#voucherId").val();
		tips({
            "skin": "loading"
        });
		$.ajax({
			url:"api/share.html",
			type:"POST",
			data:{voucherId : voucherId},
			dataType:"json",
			success:function(result){
				tips.loadClose(); // 关闭进度条
				if (result && result.success) {
					tips({
		                "message": "分享成功",
		                "skin": "alert"
		            });
				}else{
					tips({
		                "message": result.msg,
		                "skin": "msg"
		            });
				}
			},
			error:function(){
				tips.loadClose(); // 关闭进度条
			}
		});
    	
    }
    </script>
    
</body>

</html>
