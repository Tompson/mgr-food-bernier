<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>饭好粥道-收货地址管理</title>
    <meta name="keywords" content="我的地址" />
    <meta name="description" content="我的地址" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/user.css" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/h5/css/mycss.css" />
    <script src="<%=request.getContextPath() %>/h5/js/lib/jquery.min.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath() %>/h5/js/style.min.js"></script>
</head>
<body>
    <div class="wrap">
        <header id="header" class="header">
            <c:if test="${sessionScope.back_confirm==null }"><a class="back" href="../personal.html"></a></c:if>
            <c:if test="${!empty page.results }"><c:if test="${sessionScope.back_confirm!=null }"><a class="back" href="../${sessionScope.back_confirm }"></a><a class="back-order-confirm" href="../${sessionScope.back_confirm }">确认订单</a></c:if></c:if>
            <div class="tit">
             	  收货地址管理
            </div>
        </header>
        <section class="user-box" style="padding-bottom: 1.3rem;">
        	<c:forEach items="${page.results }" var="address">
            <div class="user-address mt20" data-address-id="${address.addressId }">
                <div class="user-address-box">
                    <div class="user-address-info clear">
                        <span class="fl">${address.contacts }</span><em class="fr">${address.phone }</em>
                    </div>
                    <div class="user-address-word">
                        <p>${address.addressName }</p>
                    </div>
                </div>
                <div class="user-addrss-op clear">
                    <div class="fl">
                    <c:if test="${address.status==0 }"><em class="user-address-tip user-address-xz"></em></c:if>
                    <c:if test="${address.status!=0 }"><em class="user-address-tip"></em></c:if>
                        <span class="user-address-def">默认收货地址</span>
                    </div>
                    <div class="fr">
                        <a class="user-address-edit" href="#">编辑</a>
                        <a class="user-address-del" href="#">删除</a>
                    </div>
                </div>
            </div>
            </c:forEach>
            <c:if test="${empty page.results }">
             <div class="address-not">
                <a href="#">你还没有设置地址，赶紧新增地址吧！</a>
            </div>
            </c:if>
        </section>
        <div class="user-address-add">
            <a href="../address/addressAdd.html">新增收货地址</a>
        </div>
    </div>
    <script src="<%=request.getContextPath() %>/h5/js/lib/zepto.min.js"></script>
    <script src="<%=request.getContextPath() %>/h5/js/ui/tip.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/addressList.js"></script>
</body>

</html>