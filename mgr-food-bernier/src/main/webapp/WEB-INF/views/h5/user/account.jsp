<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>饭好粥道-我的账户</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/common.css" rel="stylesheet" />
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/storelist.css" rel="stylesheet" />
		<link type="text/css" href="<%=request.getContextPath()%>/h5/css/mycss.css" rel="stylesheet" />
	</head>
	<body>
		<div class="wrap">
			 <header id="header" class="relative">
	            <a class="absolute back" href="<%=request.getContextPath()%>/h5/personal.html" ></a>
	            <div class="tit tit-c">
	                	我的账户
	            </div>
	        </header>
	        
	        <section class="recharge container clearfix">
	        	 <form id=''>
<!-- 	        	 	<h1 class="border-bt">设置支付密码</h1> -->
	        	 	<div class="recharge-moneytit">充值金额</div>
	        	 	<ul class="recharge-money clearfix" id="selectWay">
	        	 		<li>
	        	 			<a class="active" data-money="50" href='javascript:;'>充￥50</a>
	        	 		</li>
	        	 		<li>
	        	 			<a data-money="100" href='javascript:;'>充￥100</a>
	        	 		</li>
	        	 		<li>
	        	 			<a data-money="500" href='javascript:;'>充￥500</a>
	        	 		</li>
	        	 		<li>
	        	 			<a  data-money="1000" href='javascript:;'>充￥1000</a>
	        	 		</li>
	        	 	</ul>
	        	 	<div class='recharge-money-others'>
	        	 		<span>其他金额</span>
	        	 		<input type="number" style="color:black;"placeholder="请输入金额" id="otherMoney" onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" />
	        	 	</div>
	        	 </form>
	        </section>
	        
	        <section class='recharge-type container'>
	        	<img src="<%=request.getContextPath()%>/h5/images/border-bg.jpg" />
	        	<div class="pay-way selected" data-way="wx" id="wxpay">
	        		<a href='javascript:;' class='fl weixin'>微信<a>
	        		<a href='javascript:;' class="fr recharge-icon" style='width:0.36rem!important;height:0.36rem!important;border:none!important;margin:0!important;'>
	        			<img src='<%=request.getContextPath()%>/h5/images/selectcd.png' />
	        		</a>
	        	</div>
	        	<div class="pay-way" data-way="alipay" id="alipay">
	        		<a href='javascript:;' class='fl zhifubao'>支付宝<a>
	        		<a href='javascript:;' class="fr recharge-icon" style='width:0.36rem!important;height:0.36rem!important;border:none!important;margin:0!important;'>
	        			<img src='<%=request.getContextPath()%>/h5/images/selectcd2.png' />
	        		</a>
	        	</div>
	        	<button type="submit" id="sureRecharge">充值</button>
	        </section>
		
		<div class="wxtip" id="JweixinTip">
					<span class="wxtip-icon"></span>
					<p class="wxtip-txt">
						请点击右上角<br />在浏览器中打开！！！
					</p>
				</div>
		
		</div>
		<script src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
		<script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
		<script>
			var oHtml = document.getElementsByTagName('html')[0];
			var screenW = document.documentElement.offsetWidth || document.body.offsetWidth;
			oHtml.style.fontSize = 100 * screenW / 720 + "px";
		</script>
		
		
		<script type="text/javascript">
		$(function(){
			checkCtx();
			
			$("#selectWay").on("tap","a",function(){
				if($(this).hasClass("active")){
					$(this).removeClass("active")
				}else{
					$(this).addClass("active").parent().siblings().find("a").removeClass("active");
					$("#otherMoney").val("");
				}
			});
			
			$("#otherMoney").on("input",function(){
				$("#selectWay a").removeClass("active");
			});
			
			$(".pay-way").on("tap",function(){
				$(this).addClass("selected").siblings().removeClass("selected");
				$(this).find("img").attr("src","../images/selectcd.png");
				$(this).siblings().find("img").attr("src","../images/selectcd2.png");
			});
			
			$("#sureRecharge").on("click",function(){
				var way = $(".pay-way.selected").data("way");
				var money = $("#otherMoney").val().trim() || $("#selectWay a.active").data("money");
				if(!money){
					tips({
		                "message": "请选择或输入充值金额！",
		                "skin": "msg"
		            });
					return;
				}
				if(way=="wx"){
					//微信支付
					rechargeByWx(money);
				}else if(way=="alipay"){
					//支付宝支付
					if(isWeiXin()){
						$("#JweixinTip").show();
						return;
					}
					rechargeByAlipay(money);
				}else{
					tips({
		                "message": "请选择支付方式",
		                "skin": "alert"
		            });
				}
			});
			
			// 分享提示隐藏
			$("#JweixinTip").on("click", function() {
				$(this).hide();
			});
			
		})
		
/**
 * 充值预下单
 * @param money
 */
 var order;
function  rechargeByWx(money) {
	$.post("../pay/api/rechargeByWx.html", {
		money:money
	}, function(result) {
		if (result && result.success) {
			order = result.data;
			wxPay();
		}else{
			tips({
				message : "系统繁忙！",
				skin : "msg"
			});
		}
	},"json");
}
		function wxPay() {
			if (typeof WeixinJSBridge == "undefined") {
				if (document.addEventListener) {
					document.addEventListener('WeixinJSBridgeReady', onBridgeReady,
							false);
				} else if (document.attachEvent) {
					document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
					document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
				}
			} else {
				onBridgeReady();
			}
		}
		
		function onBridgeReady() {
			WeixinJSBridge.invoke('getBrandWCPayRequest', {
				"appId" : order.appId, // 公众号名称，由商户传入
				"timeStamp" : order.timeStamp, // 时间戳，自1970年以来的秒数
				"nonceStr" : order.nonceStr, // 随机串
				"package" : order["package"],
				"signType" : order.signType, // 微信签名方式 :
				"paySign" : order.paySign
			// 微信签名
			}, function(res) {
				// 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回 ok，但并不保证它绝对可靠。
				if (res.err_msg == "get_brand_wcpay_request：ok") {
					alert("支付成功")
				}
			});
		}	
		
		//支付宝支付
		function rechargeByAlipay(money) {
			$.post("../pay/api/rechargeByAlipay.html",{money:money,memberId:'${memberId}'},function(result){
				if(result&&result.success){
					window.location.href = result.data;
				}else{
					tips({
						message : result.msg,
						skin : "msg"
					});
				}
			},"json");
		}
		
		function isWeiXin(){ 
	    	var ua = window.navigator.userAgent.toLowerCase(); 
	    	var str=ua.match(/MicroMessenger/i);
	    	if(str == 'micromessenger'){ 
	    		return true; 
	    	}else{ 
	    		return false; 
	    	}
	   	}
		
		function checkCtx(){
			if(!isWeiXin()){
				$(".pay-way").removeClass("selected");
				$("#alipay").addClass("selected");
				$("#alipay").find("img").attr("src","../images/selectcd.png");
				$("#alipay").siblings().find("img").attr("src","../images/selectcd2.png");
				$("#wxpay").hide();
			}
		}
			
		</script>
	</body>
</html>
