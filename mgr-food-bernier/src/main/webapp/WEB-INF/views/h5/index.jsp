<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<html lang="en" class="wovd">
<head>
<meta charset="utf-8" />
<title>饭好粥道-首页</title>
<meta name="keywords" content="首页" />
<meta name="description" content="首页" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/home.css" />
	<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/details.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RKvzHhCN0egpiK9nHKfTAnZybnUQGYrH"></script>

<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/h5/js/style.min.js"
	type="text/javascript"></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"
	type="text/javascript" charset="utf-8"></script>
<style type="text/css" rel="stylesheet">
#mymap {
	width: 100%;
	height: 100%;
	margin: 0;
	overflow: hidden;
}
</style>
</head>

<body class="wovd">
	<div class="wovd">
		<div class="slider">
			<ul class="silder-list">
		
				<c:forEach items="${carouselList}" var="carouselFigure">
				    <c:if test="${carouselFigure.type eq '1' }">
					<!--1.优惠卷 -->
						<li  type="${carouselFigure.linkNo}" data="1"> <a> <img src="${carouselFigure.photoUrl}" alt="" /></a></li>
					</c:if>
					<c:if test="${carouselFigure.type eq '2' }">
					<!--1.其他活动 -->
						<li type="${carouselFigure.linkNo}" data="2"> <a> <img src="${carouselFigure.photoUrl}" alt="" /></a></li>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	
	  <script>
			var storesJSON = '${stores}';
			var requstUrl='<%=request.getContextPath()%>';
			var carouselList= "${fn:length(carouselList)}";
	  </script>
		<div class="maps">
			<div id="mymap" lat="${partnerLocation.lat}"
				lng="${partnerLocation.lng}" userlat="${member.geoLat}"
				userlng="${member.geoLng }" address="${partnerAddress}"
				delivery-radius="${deliveryRadius }" wxlat="${wxLocation.lat}"
				wxlng="${wxLocation.lng }"stores="${stores }"></div>
		</div>
		<footer class="satnav">
			   <a class="btn btn-yellow satnav-btn" href="store/storeHome.html">去逛逛&nbsp;&nbsp;点外卖</a>  
			   <!-- <a class="btn btn-yellow satnav-btn" href="productHome.html">旧版&nbsp;&nbsp;点外卖</a>  -->
			<div class="satnav-user">
				<a class="link" href="personal.html">我的</a>
			</div>
		</footer>
	</div>
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/index.js"></script>
	<script
		src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/goodsCommon.js"></script>
 <%-- <script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/productHome.js"></script>  --%>
 <script
		src="<%=request.getContextPath()%>/h5/js/ui/silder.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/js/ui/layer/layer.js"></script>
		
	
</body>

</html>