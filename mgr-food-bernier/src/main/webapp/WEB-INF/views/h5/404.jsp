<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<html>
  <head>
    <title>饭好粥到</title>
  </head>
  <body>
    <div class="container container-sm">
      <div id="icon-header">
        <span class="fa fa-question-circle"></span>
      </div>
      <div id="text-column">
        <header class="secondary-header">
          <h1>提示: <strong>该门店已经被禁用</strong></h1>
          <p></p>
        </header>
 
        <nav class="secondary-nav">
          <ul>
            <li>
              <a href="../index.html" class="btn btn-primary">
                	返回首页
                <span class="fa fa-exclamation"></span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </body>
</html>