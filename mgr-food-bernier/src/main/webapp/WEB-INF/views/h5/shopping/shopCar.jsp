<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html lang="en" style="height: 100%">

<head>
<meta charset="utf-8" />
<title>饭好粥道-购物车</title>
<meta name="keywords" content="购物车" />
<meta name="description" content="购物车" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/shop.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
</head>

<body style="height: 100%">
	<input type="hidden" id="memberStatus" value="${memberStatus }">
	<input type="hidden" id="storeId" value="${storeId}">
	<input type="hidden" id="tableId" value="${tableId}">
	<div class="wrap" style="height: 100%; box-sizing: border-box">
		<!-- 头部 -->
		<header id="header" class="header">
			<a class="back" href="../store/eatingInStore.html?storeId=${storeId }&&tableId=${tableId}"></a>
			<div class="tit">购物车</div>
			<c:if test="${!empty cartList }">
				<a href="#" class="save" id="deleteAll" style="line-height: 1rem;">清空购物车</a>
			</c:if>
		</header>
		<!-- 购物车 -->
		<input id="storeId" value="${storeId}" type="hidden"/>
		<section class="box" style="height: 100%">
			<div id="goodsList">
				<div class="category-item">
					<h3 class="shop-tit pd20 category-head">
						<c:choose>
							<c:when test="${weekTime.week==1 }">星期一</c:when>
							<c:when test="${weekTime.week==2 }">星期二</c:when>
							<c:when test="${weekTime.week==3 }">星期三</c:when>
							<c:when test="${weekTime.week==4 }">星期四</c:when>
							<c:when test="${weekTime.week==5 }">星期五</c:when>
							<c:when test="${weekTime.week==6 }">星期六</c:when>
							<c:when test="${weekTime.week==7 }">星期日</c:when>
						</c:choose>
						<span>${weekTime.date }</span>
						
<!-- 						<a class="shop-info-del fr delete-weeknum" >删除</a> -->
					</h3>
					<c:forEach items="${cartList }" var="cart">
					<!--  摒弃了菜品的星期属性-->
							<div class="shop-info-item" data-id="cart${cart.id }" id="cart${cart.id }">
								<a class="shop-info-item-img" href="#"><img
									src="${cart.picUrl }" alt=""></a>
								<div class="shop-info-item-cont">
									<a href="#" class="shop-info-item-tit">${cart.goodsName }</a>
									<div class="shop-info-item-price clear">
										<em class="fl unitPrice">￥${cart.formatUnitPrice }</em>
										<div class="shop-info-item-num fr">
											<p class="shop-info-meius">-</p>
											<p class="shop-info-show" data-id="${cart.id }" data-sid="${cart.storeId }" data-sgid="${cart.storeGoodsId }" data-goods-id="${cart.goodsId }">${cart.num }</p>
											<p class="shop-info-add">+</p>
										</div>
									</div>
								</div>
							</div>
					</c:forEach>
					<div class="shop-info-settle">
						<div class="shop-settle-price">
							<p>
								合计:<em class="shop-settle-money">￥<span
									class="amountPrice"><fmt:formatNumber type="number" pattern="0.00" value="${statCar.amountPrice }"/></span></em>
								
								<a  style="color: #ffb000;margin-left:10%" href="../store/eatingInStore.html?storeId=${storeId}&tableId=${tableId}">继续选购</a>
							</p>
						</div>

						<div class="shop-settle-btn">
							<a href="javascript:;" class="toOrderConfirm" >结算</a>
						</div>
					</div>
				</div>
			</div>
			<div class="shop-not ${empty cartList?'':'hd' }">
				<a href="../store/eatingInStore.html?storeId=${storeId}&tableId=${tableId}">去购物</a>
			</div>
		</section>
	</div>
	<script src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/h5/js/goodsCommon.js"></script>
	<script type="text/javascript">
	$(function() {
		var Listsize="${fn:length(cartList)}";
		if(Listsize<1){
			$("#goodsList").remove();
			$("#deleteAll").hide();
		}
		//加
		$(".shop-info-add").on("click", function(event) {
			handle($(this),"add");
		});
		//减
		$(".shop-info-meius").on("click", function(event) {
			
			handle($(this),"minus");
		});
		//清空购物车
		$("#deleteAll").on("click", function() {
			var _this = this;
			var storeId=$("#storeId").val();
			tips({
				message : "您确定要清空购物车吗？",
				skin : "confirm"
			}, function() {
				$.ajax({
					url : "../shopcar/api/emptyShopCart2.html",
					type : "get",
					data : {
						storeId:storeId
					},
					dataType : "json",
					success : function(result) {
						if (result) {
							if (result.success) {
								$("#goodsList").remove();
								$(".shop-not").show();
								$("#deleteAll").hide();
							} else {
								tips({
									message : "删除失败！",
									skin : "msg"
								});
							}
						} else {
							tips({
								message : "系统繁忙！",
								skin : "msg"
							});
						}
					},
					error : function() {
						tips({
							message : "系统繁忙！",
							skin : "msg"
						});
					}
				});
			});
		});
		//删除购物车该类菜品
// 		$(".delete-weeknum").on(
// 				"click",
// 				function() {
// 					var _this = $(this);
// // 					var weeknum = _this.data("weeknum");
// // 					if (!weeknum) {
// // 						return;
// // 					}
// 					tips({
// 						message : "您确定要将该类菜品移出购物车吗？",
// 						skin : "confirm"
// 					}, function() {
// 						$.ajax({
// 							url : "../order/api/deleteByWeeknum.html",
// 							type : "get",
// 							data : {
// 								weeknum : weeknum
// 							},
// 							dataType : "json",
// 							success : function(result) {
// 								if (result) {
// 									if (result.success) {
// 										var items = $("#goodsList").find(
// 												".category-item");
// 										if (items.length == 1) {
// 											$("#goodsList").remove();
// 											$(".shop-not").show();
// 											$("#deleteAll").hide();
// 											return;
// 										}
// 										_this.parents(".category-item")
// 												.remove();
// 									} else {
// 										tips({
// 											message : "删除失败！",
// 											skin : "msg"
// 										});
// 									}
// 								} else {
// 									tips({
// 										message : "系统繁忙！",
// 										skin : "msg"
// 									});
// 								}
// 							},
// 							error : function() {
// 								tips({
// 									message : "系统繁忙！",
// 									skin : "msg"
// 								});
// 							}
// 						});
// 					});
// 				});
		
		$(".toOrderConfirm").on("click",function(e){
			e.preventDefault();
			var overTime=$(this).data("over-time");
			if(overTime==1){
				return;
			}
			var storeId=$("#storeId").val();
			var tableId=$("#tableId").val();
			$.ajax({
				url : "../order/api/validateBeforeOrderConfirm1.html",
				type : "get",
				data : {
					
					storeId:storeId,
					tableId:tableId
				},
				dataType : "json",
				success : function(result) {
					if (result) {
						if(result.success){
							console.log(storeId);
							location.href="../order/orderConfirm1.html?storeId="+storeId+"&&tableId="+tableId;
						}else{
							var errorCode=result.data;
							switch(errorCode){
							case "001":
								tips({
									message : result.msg,
									skin : "alert"
								});
								break;
							case "002":
								tips({
									message : result.msg,
									skin : "msg"
								});
								setTimeout(function(){
									tips.msgClose();
									location.reload();
								}, 300);
								break;
							case "003":
								tips({
									message : result.msg,
									skin : "alert"
								});
								break;
							case "004":
								tips({
									message : result.msg,
									skin : "confirm"
								},function(){
									location.href="../store/eatingInStore.html?storeId="+storeId+"&&tableId="+tableId;
								});
								break;
							case "007":
								tips({
									message : result.msg,
									skin : "msg"
								});
								setTimeout(function(){
									tips.msgClose();
									location.reload();
								}, 300);
								break;
//							case "008":
//								tips({
//									message : result.msg,
//									skin : "alert"
//								});
//								break;
							default:
//								tips({
//									message : "系统繁忙！",
//									skin : "msg"
//								});
								location.reload();
								break;
							}
						}
					} else {
						tips({
							message : "系统繁忙！",
							skin : "msg"
						});
					}
				},
				error : function() {
					tips({
						message : "系统繁忙！",
						skin : "msg"
					});
				}
			});
			
		});
	});
	function handle(that,sign){
		var flag =true;
		var _that = that.siblings(".shop-info-show");
		var num  = parseInt(_that.text().trim());
		if("add"==sign){
			if(!!!num || num>999){
				return ;
			}
			num ++;
		}else if("minus" ==sign){
	
			if(num>1){
				num --;
			}else{
				num=0;
				flag=false;
				tips({
	                message: "您确定要把该商品移除购物车？",
	                skin: "confirm"
	            },function(){
	            	var xx =$(".shop-info-item").length;
	            	if (xx == 1) {
	    				// 删除提示
	    				flag=true;
	    				//start
	            		if(flag){
	            			var id = _that.data("id");
	            			var sid = _that.data("sid");
	            			var sgid = _that.data("sgid");
	            			var goodsId = _that.data("goods-id");
	            			var tableId=$("#tableId").val();
	            			var storeId=$("#storeId").val();
	            			var vo = new Add2CarVo();
	            			vo.id =id;
	            			vo.goodsId = goodsId;
	            			vo.num = num;
	            			vo.storeGoodsId =sgid;
	            			vo.storeId = storeId;
	            			vo.tableId=tableId;
	            			addToCar({
	            				"data":vo,
	            				"success":function(data){
	            					_that.text(num);
	            					var amountPrice=data.amountPrice;
	            					var sum=amountPrice.toFixed(2);//保留两位小
	            					that.parents(".category-item").find(".shop-info-settle .amountPrice").html(sum);
	            				},
	            				"error":function(result){
	            					var errorCode=result.data;
	            					switch(errorCode){
	            					case "001":
	            						tips({
	            							message : result.msg,
	            							skin : "alert"
	            						});
	            						break;
	            					case "007","002":
	            						tips({
	            							message : result.msg,
	            							skin : "msg"
	            						});
	            						$.ajax({
	            							url:"api/deleteForbiddenGoods.html",
	            							type:"get",
	            							data:{
	            								goodsId:goodsId
	            								
	            							},
	            							dataType:"json",
	            							success:function(result){
	            								if(result&&result.success){
	            									if ($(".shop-info-item").length == 1) {
	            				        				// 删除提示
	            				                		if($("#goodsList").find(".category-item").length==1){
	            				                			$("#goodsList").remove();
	            											$(".shop-not").show();
	            											$("#deleteAll").hide();
	            											return;
	            				                        }
	            				                        $(".category-item").remove();
	            				                        return;
	            				        			}
	            				                	$(".shop-info-item").remove();
	            								}
	            							},
	            							error:function(){
	            								tips({
	            									message : "系统繁忙！",
	            									skin : "msg"
	            								});
	            							}
	            						});
	            						break;
	            					default:
	            						tips({
	            							message : "加入购物车失败！",
	            							skin : "msg"
	            						});
	            						break;
	            					}
	            				}
	            			});
	            		}
	    				//end
	    				var length=$("#goodsList").find(".category-item").length;
	            		if(length==1){
	            			$("#goodsList").remove();
							$(".shop-not").show();
							$("#deleteAll").hide();
							return;
	                     } 
	            	$(".category-item").remove();
	                    return;
	    			}
	            	 var xid=_that.data("id");	            	 
	            	$("#cart"+xid).remove();
					return;
	            });
	         
				
			}
		}
		if(flag){
			var id = _that.data("id");
			var sid = _that.data("sid");
			var sgid = _that.data("sgid");
			var goodsId = _that.data("goods-id");
			var tableId=$("#tableId").val();
			var storeId=$("#storeId").val();
			var vo = new Add2CarVo();
			vo.id =id;
			vo.goodsId = goodsId;
			vo.num = num;
			vo.storeGoodsId =sgid;
			vo.storeId = storeId;
			vo.tableId=tableId;
			addToCar({
				"data":vo,
				"success":function(data){
					_that.text(num);
					var amountPrice=data.amountPrice;
					var sum=amountPrice.toFixed(2);//保留两位小
					that.parents(".category-item").find(".shop-info-settle .amountPrice").html(sum);
				},
				"error":function(result){
					var errorCode=result.data;
					switch(errorCode){
					case "001":
						tips({
							message : result.msg,
							skin : "alert"
						});
						break;
					case "007","002":
						tips({
							message : result.msg,
							skin : "msg"
						});
						$.ajax({
							url:"api/deleteForbiddenGoods.html",
							type:"get",
							data:{
								goodsId:goodsId
								
							},
							dataType:"json",
							success:function(result){
								if(result&&result.success){
									if ($(".shop-info-item").length == 1) {
				        				// 删除提示
				                		if($("#goodsList").find(".category-item").length==1){
				                			$("#goodsList").remove();
											$(".shop-not").show();
											$("#deleteAll").hide();
											return;
				                        }
				                        $(".category-item").remove();
				                        return;
				        			}
				                	$(".shop-info-item").remove();
								}
							},
							error:function(){
								tips({
									message : "系统繁忙！",
									skin : "msg"
								});
							}
						});
						break;
					default:
						tips({
							message : "加入购物车失败！",
							skin : "msg"
						});
						break;
					}
				}
			});
		}
		
	}
	
	function Add2CarVo(){
		this.id=null;
		this.goodsId =null;
		this.num =null;
		this.storeId =null;
		this.storeGoodsId =null;
	}
	
	function addToCar(param){
		
		tips({
			"skin":"loading"
		});
		var root = getWebRootPath();
		$.ajax({
			
			url:root+"/h5/shopcar/api/add2Cart2.html",
			type:"get",
			data:param.data,
			dataType:"json",
			success:function(result){
				tips.loadClose(); //关闭进度条
				if(result){
					if(result.success){
						param.success(result.data);
					}else{
						param.error(result);
					}
				}else{
					tips({
						message : result.msg||"系统繁忙",
						skin : "msg"
					});
				}
			},
			error:function(result){
				tips.loadClose(); //关闭进度条
				tips({
					message : result.msg||"系统繁忙",
					skin : "msg"
				});
			}
		});
	}
	
	
	function getWebRootPath() {
	    var curWwwPath = window.document.location.href;
	    var pathName = window.document.location.pathname;
	    var pos = curWwwPath.indexOf(pathName);
	    var localhostPaht = curWwwPath.substring(0, pos);
	    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
	    return (localhostPaht + projectName);
	}
	
	
	</script>
</body>

</html>