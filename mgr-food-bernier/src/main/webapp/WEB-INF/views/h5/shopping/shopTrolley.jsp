<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en" style="height: 100%">

<head>
<meta charset="utf-8" />
<title>饭好粥道-购物车</title>
<meta name="keywords" content="购物车" />
<meta name="description" content="购物车" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/styles.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/shop.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/h5/css/mycss.css" />
<script src="<%=request.getContextPath()%>/h5/js/lib/jquery.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/h5/js/style.min.js"></script>
</head>

<body style="height: 100%">
	<input type="hidden" id="memberStatus" value="${memberStatus }">
	<input type="hidden" id="storeId" value="${storeId}">
	<div class="wrap" style="height: 100%; box-sizing: border-box">
		<!-- 头部 -->
		<header id="header" class="header">
			<a class="back" href="../store/msgStore.html?id=${storeId}"></a>
			<div class="tit">购物车</div>
			<c:if test="${!empty cartList }">
				<a href="#" class="save" id="deleteAll" style="line-height: 1rem;">清空购物车</a>
			</c:if>
		</header>
		<!-- 购物车 -->
		<input id="storeId" value="${storeId}" type="hidden"/>
		<section class="box" style="height: 100%">
			<div id="goodsList">
				<c:forEach items="${amountPriceList}" var="ws">
					<div class="category-item">
						<h3 class="shop-tit pd20 category-head">
							<c:choose>
								<c:when test="${ws.weeknum=='1' }">星期一</c:when>
								<c:when test="${ws.weeknum=='2' }">星期二</c:when>
								<c:when test="${ws.weeknum=='3' }">星期三</c:when>
								<c:when test="${ws.weeknum=='4' }">星期四</c:when>
								<c:when test="${ws.weeknum=='5' }">星期五</c:when>
								<c:when test="${ws.weeknum=='6' }">星期六</c:when>
								<c:when test="${ws.weeknum=='7' }">星期日</c:when>
							</c:choose>
							<span>${weekTime[ws.weeknum] }</span>
						<%-- 	<a style="color: #ffb000;" href="../store/msgStore.html?id=${storeId}">继续选购</a> --%>
							 <a class="shop-info-del fr delete-weeknum"
								data-weeknum="${ws.weeknum }">删除</a>
						</h3>
						<c:forEach items="${cartList }" var="cart">
							 <c:if test="${cart!=null&&cart.week==ws.weeknum }"> 
								<div class="shop-info-item">
									<a class="shop-info-item-img" href="#"><img
										src="${cart.picUrl }" alt=""></a>
									<div class="shop-info-item-cont">
										<a href="#" class="shop-info-item-tit">${cart.goodsName }</a>
										<div class="shop-info-item-price clear">
											<em class="fl unitPrice">￥${cart.formatUnitPrice }</em>
											<div class="shop-info-item-num fr">
												<p class="shop-info-meius">-</p>
												<p class="shop-info-show" data-goods-id="${cart.goodsId }">${cart.num }</p>
												<p class="shop-info-add">+</p>
											</div>
										</div>
									</div>
								</div>
							</c:if>
						</c:forEach>
						<div class="shop-info-settle">
							<div class="shop-settle-price">
								<p>
									合计:<em class="shop-settle-money">￥<span
										class="amountPrice">${ws.formatAmountPrice }</span></em>
									<a style="color: #ffb000;margin-left:10%" href="../store/msgStore.html?id=${storeId}">继续选购</a>
								</p>
							</div>
							<div class="shop-settle-btn">
							<c:if test="${ws.weeknum!=currentDayOfWeeK||checkNowIsOverEndTime==false }">
								<a href="#" class="toOrderConfirm" data-weeknum="${ws.weeknum }" data-over-time="2">结算</a>
							</c:if>
							<c:if test="${ws.weeknum==currentDayOfWeeK&&checkNowIsOverEndTime==true }">
								<a href="#" class="toOrderConfirm" data-weeknum="${ws.weeknum }" data-over-time="1" >结算<span style="font-size: .2rem;color: red;">(已打烊)</span></a>
							</c:if>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<!-- 有商品 -->
			<c:if test="${!empty cartList }">
				<div class="shop-not hd">
					<a href="../store/msgStore.html?id=${storeId}">去购物</a>
				</div>
			</c:if>
			<!-- 无商品 -->
			<c:if test="${empty cartList }">
				<div class="shop-not">
					<a href="../store/msgStore.html?id=${storeId}">去购物</a>
				</div>
			</c:if>
		</section>
	</div>
	<script src="<%=request.getContextPath()%>/h5/js/lib/zepto.min.js"></script>
	<script src="<%=request.getContextPath()%>/h5/js/ui/tip.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/h5/js/goodsCommon.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/h5/js/shop.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/h5/js/view/shopTrolley.js"></script>

</body>

</html>