<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
			<div class="form-inline">
				<form id="deliverMan_table_form">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="姓名" name="deliverManName">
                    </div>
                    <div class="form-group">
                        <select name="status" class="form-control">
		                    <option value="">状态</option>
		                    <option value="1">启用</option>
		                    <option value="2">禁用</option>
                  		</select>
                    </div>
                    
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    <div class="form-group btn-group-vertical alignright">
                    <t:buttonOut url="/deliverMan/toDeliverManAdd.html">
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="deliverManAdd()">添加配送员</a>
                    </t:buttonOut>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="deliverMan_table"></table>
                </div>
                
                <style> /*写了个样式把Validform_msg强制掩藏了*/
                     #Validform_msg{display:none !important;} 
     			 </style>
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>


<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/deliverMan/page.html';  //TODO
var syslog_events = {
		'click input[type=checkbox]':function(e,value,row,index){
			console.log("row:"+row.status);//1启用2禁用
			var _this=this;
			if(row.status == 1){ //启用就禁用
					$.ajax({ 
	      				url: '<%=request.getContextPath()%>/deliverMan/disableState.html?deliverManId='+row.deliverManId, 
	      				context: document.body, 
	      				async: false,
	      				success: function(vo){
		      				if(vo.success){
		      					layer.msg('操作成功!',{icon:1});
		      					$(_this).prop("checked",false);
		      					row.status=2;
		      				}else{
		      					layer.msg('操作失败!',{icon:5});
		      					$(_this).prop("checked",true);
		      					row.status=1;
		      				}
	      				}
	      			});
			}else{//禁用就启用
				$.ajax({
	    			url:'<%=request.getContextPath()%>/deliverMan/enableState.html?deliverManId='+row.deliverManId,
	    			context: document.body, 
	    			async:false,
	    			success: function(vo){
	      				if(vo.success){
	      					layer.msg('操作成功!',{icon:1});
	      					$(_this).prop("checked",true);
	      					row.status=1;
	      				}else{
	      					layer.msg('操作失败!',{icon:5});
	      					$(_this).prop("checked",false);
	      					row.status=2;
	      				}
	  				},
	  				error:function(vo){
	  					console.log(vo);
	  				}
	    		});
			}
		},
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/deliverMan/deleteDeliverMan.html?deliverManId='+row.deliverManId, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#deliverMan_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .edit':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/deliverMan/toDeliverManEdit.html?deliverManId='+row.deliverManId;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	},
    	'mouseenter .ewm-img':function(e,value,row,index){//鼠标悬停
    		console.log(1);
   		   var evm_offset=$(this).offset();
   	        $("<div class='img-da'><img src="+ $(this).attr("src")+" alt=''></div>").appendTo($("body"));
   	        $(".img-da").css({left:evm_offset.left+50,top:evm_offset.top-100})
    	},
    	'mouseleave .ewm-img':function(e,value,row,index){//鼠标离开
    		 $(".img-da").remove(); 
    	}
    	
    	
};


var deliverMan_columns = [
{field: 'qrCode',title: '二维码',valign: 'top',sortable: false,events:syslog_events,formatter:operateFormatter2},
{field: 'deliverManName',title: '姓名',valign: 'top',sortable: false},
{field: 'tel',title: '电话',valign: 'top',sortable: false},
{field: 'createTime',title: '创建时间',valign: 'top',sortable: false},
{field: 'status',title: '禁用/启用',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter1},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#deliverMan_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: deliverMan_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#deliverMan_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#deliverMan_table').bootstrapTable('refresh', null);
	}
	
	function operateFormatter2(value,row,index){
		console.log(value)
		if(!!!value){
			return ['<image  class="ewm-img" width="50" height="50" src="../images/upload.png" alt="" />'].join('');
		}else{
			return ['<image  class="ewm-img" width="50" height="50" src="'+value+'" alt="" />'].join('');
		}
		
	}
	
	//编辑和删除图标
	function operateFormatter(value,row,index){
		return ['<t:buttonOut url="/deliverMan/toDeliverManEdit.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs edit">',
		'编辑','</a></t:buttonOut><t:buttonOut url="/deliverMan/deleteDeliverMan.html"><button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button></t:buttonOut>'].join('');
	}
	
	//启用和禁用图标
	 function operateFormatter1(value,row,index){
		if(row.status==1){//1启用 2停用     
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" checked="checked">'+
	 		'<span class="ace-tg"></span></div>';
		}
		if(row.status==2){
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" >'+
	 		'<span class="ace-tg"></span></div>';
		}
}
	//添加配送员
    function deliverManAdd(){
    	var url= '<%=request.getContextPath()%>/deliverMan/toDeliverManAdd.html';
    	$("#admin_dialog_show").empty();
		$("#admin_dialog_show").load(url);//增加需要先清空再加载
		$("#admin_dialog").modal('show');
	  }
	
	
	

    
	
</script>