<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/ztree/css/zTreeStyle.css"> 
<script src="<%=request.getContextPath()%>/js/ztree/jquery.ztree.core.min.js"></script>
<script src="<%=request.getContextPath()%>/js/ztree/jquery.ztree.excheck.min.js"></script>
<html >
<head>
<script type="text/javascript">
var start = {
        elem: '#datestart',
        format: 'YYYY-MM-DD',
        min: '1900-01-01', //设定最小日期为当前日期
        max: laydate.now(), //最大日期
        istime: true,
        istoday: false,
//         festival: true,//是否显示节日
        choose: function(datas){
             end.min = datas; //开始日选好后，重置结束日的最小日期
             end.start = datas //将结束日的初始值设定为开始日
        }
    };
    var end = {
        elem: '#dateend',
        format: 'YYYY-MM-DD',
        max: laydate.now(),
        min: '1900-06-16',
        istime: true,
        istoday: false,
//         festival: true,//是否显示节日
        choose: function(datas){
            start.max = datas; //结束日选好后，重置开始日的最大日期
        }
    };
    laydate(start);
    laydate(end);
    $(function(){
        var datestart=$("#datestart"),dateend=$("#dateend");
        /*7天年内*/
         $("#days").on("click",function(){
                datestart.val(laydate.now(-7,"YYYY-MM-DD"));
                dateend.val(laydate.now(0,"YYYY-MM-DD"));

        });
         /*本月*/
          $("#months").on("click",function(){
              datestart.val(laydate.now(0,"YYYY-MM-01"));
              dateend.val(laydate.now(0,"YYYY-MM-DD"));
        });

  })
</script>

</head>
<body>
        <section class="main-cont">
        <div class="article">
         <div class="form-inline">
     <form id="query_from">
        <div class="form-group">
      	<input type="text" class="form-control" id="datestart" name="querystarttime" placeholder="开始时间">
      	</div>
      	 <div class="form-group">
        <input type="text" class="form-control" id="dateend"  name="queryendtime" placeholder="结束时间">
        </div>
        <input class="btn btn-primary" type="button" id="days" name="" value="最近7天"/> 
        <input class="btn btn-primary" type="button" id="months" name="" value="本月"/>
        <div class="form-group" id="storeidHide">
                        <select name="storeid" id="storeid" onchange="" class="form-control">
                             <c:forEach items="${storeList}" var="item">
	                             	<option  value="${item.id}">${item.storeName}</option>
                             </c:forEach>
	                   </select>
        </div>
        <input class="btn btn-primary"type="button" name="" value="查询" onclick="query('<%=request.getContextPath()%>/sysOrder/newChart.html');"/>

       <table class="table table-hover" style="width:100%; table-layout:fixed;">
       </table>

        </form>
        </div>
        <div id="page_data"></div>    
            <script type="text/javascript">
             var url = '<%=request.getContextPath()%>/sysOrder/newChart.html';
            $("#page_data").load(url);
        </script>

        </section>
<script type="text/javascript">
var flag=${flag};
if(flag==2){
	$("#storeidHide").hide();
}
</script>
</body>
</html>