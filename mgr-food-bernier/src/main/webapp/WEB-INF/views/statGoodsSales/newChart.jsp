<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>

       <script type="text/javascript">

       function selectChart(){
          var querystarttime = $("#datestart").val();
          var queryendtime = $("#dateend").val();
          
           $.ajax({ 
                type: "POST",  
                traditional: true,
                 url: '<%=request.getContextPath()%>/sysUser/toChart.html',
                 data: {"querystarttime":querystarttime,"queryendtime":queryendtime},  
                 context: document.body, 
                 async: false,
                 cache:false,
                 success: function(data){

                 query('<%=request.getContextPath()%>/sysUser/toChart.html');
                 }
             });
       }

      
       </script>
    <script type="text/javascript">
       var myChart = echarts.init(document.getElementById('highcharts-cont'));
       
       var orderNocolor = {normal: {color: '#52ae57' } };
       var moneycolor = {normal: {color: '#00bbee' } };
       var rechargecolor = {normal: {color: '#ff9900' } };
       var mercollectdays =${dataList}; 
       var flag=${flag};
       var arrayObj =[];
       var orderNo =[];
       var money =[];
       var remoney =[];
      for(var i = 0 ;i<mercollectdays.length;i++){
         arrayObj.push(mercollectdays[i].time);
         orderNo.push(mercollectdays[i].orderNo);
         money.push(mercollectdays[i].saleMoney);
         remoney.push(mercollectdays[i].rechargeMoney);
      }

var option = {
    title: {
        text: '订单与销售额统计表'
       
    },
    tooltip: {
        trigger: 'axis',
        
    },
    toolbox: {
        show: true,
        feature: {
            dataZoom: {},
            dataView: {readOnly: false},
            magicType: {type: ['line', 'bar']},
            restore: {},
            saveAsImage: {}
        }
    },
    legend: {
        data:${data}
    },
    xAxis:  {
        type: 'category',
        boundaryGap: true,
        data: arrayObj
    },
    yAxis:{
        type: 'value',
        axisLabel: {
            formatter: '{value} K'
        }
    },
    series:[

    ]
};
if(flag==1){
	   option.series=[ {
        name:'订单数',
        type:'line',
        data:orderNo,
        itemStyle:orderNocolor,
    	symbol:'rect'
       
    },
    {
        name:'销售额',
        type:'line',
        data:money,
        itemStyle: moneycolor
        
       
    },
    {
        name:'充值金额',
        type:'line',
        data:remoney,
        itemStyle: rechargecolor,
        symbol:'rect'
        
       
    }]
	  
}else{
	   option.series=[ {
        name:'订单数',
        type:'line',
        data:orderNo,
        itemStyle:orderNocolor,
    	symbol:'rect'
       
    },
    {
        name:'销售额',
        type:'line',
        data:money,
        itemStyle: moneycolor
        
       
    }]
}
myChart.setOption(option);
       </script>
</head>
<body>

        <section class="main-cont">

     <form id="query_from">
       <div id="highcharts-cont" style="width:100%;height: 500px"></div>
        </form>
        <div id="page_data"></div>    
        </section>

</body>
</html>