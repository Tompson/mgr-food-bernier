  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> <meta charset="UTF-8">
  <title>饭好粥道</title>
    <link rel="shortcut icon" type="image/ico" href="<%=request.getContextPath()%>/images/logo.ico">
 	<link rel="apple-touch-icon-precomposed" href="<%=request.getContextPath()%>/images/logo.png">
    <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/logo.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
    <link href="<%=request.getContextPath()%>/js/ui/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    	<!-- Font Awesome -->
    <link href="<%=request.getContextPath()%>/js/ui/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- bootstrap-table -->
    <link href="<%=request.getContextPath()%>/js/ui/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    	<!-- Custom Theme Style -->
    <link href="<%=request.getContextPath()%>/css/custom.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <!-- wangEditor-->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/ui/wangEditor/css/wangEditor.min.css">
    <!-- 自己改的样式 -->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/mycss.css">
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RKvzHhCN0egpiK9nHKfTAnZybnUQGYrH<%-- ${bmapAK } --%>"></script>    
    	<!-- jQuery要放在前面 -->
    <script src="<%=request.getContextPath()%>/js/lib/jquery.js"></script>
    	<!-- main.js -->
    <script src="<%=request.getContextPath()%>/js/main.js"></script>
    	<!-- Bootstrap -->
    <script src="<%=request.getContextPath()%>/js/ui/bootstrap/dist/js/bootstrap.min.js"></script>
    	<!-- bootstrap-table -->
    <script src="<%=request.getContextPath()%>/js/ui/bootstrap-table/bootstrap-table.min.js"></script>
    	<!-- Latest compiled and minified Locales -->
    <script src="<%=request.getContextPath()%>/js/ui/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
    	<!-- echarts -->
    <script src="<%=request.getContextPath()%>/js/lib/echarts.common.min.js"></script>
        	<!-- bootstrap-datetimepicker-->
    <script src="<%=request.getContextPath()%>/js/lib/bootstrap-datetimepicker.min.js"></script>
    	<!-- laydate -->
    <script src="<%=request.getContextPath()%>/js/ui/laydate/laydate.js"></script>
   	    <!-- layer -->
    <script src="<%=request.getContextPath()%>/js/ui/layer/layer.js"></script>
    	<!-- Validform-->
    <script src="<%=request.getContextPath()%>/js/ui/Validform/Validform.js"></script>
    <!--  -->
    <script src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>
    	<!-- wangEditor-->
    <script src="<%=request.getContextPath()%>/js/ui/wangEditor/js/wangEditor.js"></script>
    <!-- export -->
	 <script src="<%=request.getContextPath()%>/js/lib/bootstrap-table-export.js"></script>
	 <script src="<%=request.getContextPath()%>/js/lib/bootstrap-table-export.min.js"></script>
	 <script src="<%=request.getContextPath()%>/js/lib/tableExport.js"></script>
    
    <!--[if lt IE 9]>
            <script src="<%=request.getContextPath()%>/js/lib/html5shiv.min.js"></script>
            <script src="<%=request.getContextPath()%>/js/lib/respond.min.js"></script>
      <![endif]-->
 <!-- lodop打印js -->
<script language="javascript" src="<%=request.getContextPath()%>/js/LodopFuncs.js"></script>

<!-- google map -->
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3wOgjcl53d94HgSqvWaqkkZtdxVoSFkA&sensor=true"></script> -->


 