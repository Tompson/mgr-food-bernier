<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
			<div class="form-inline">
				<form id="sysUser_table_form">
                     <div class="form-group">
                        <input type="number" onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" class="form-control" placeholder="代金券金额" name="voucherMoney">
                    </div>
                    <div class="form-group btn-group-vertical">
                        <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                    </div>
                    <div class="form-group btn-group-vertical alignright">
                     <t:buttonOut url="/voucher/toExchangeAdd.html">
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="exchangeAdd()">添加代金券</a>
                       </t:buttonOut>
                    </div>
                   </form>
                </div>
              
                
                <!-- 表里面的数据和分页里面的数据 -->
                <div class="mt20">
                    <table id="sysUser_table"></table>
                </div>
                
                <style> 
                     #Validform_msg{display:none !important;} 
     			 </style>
                
             <!-- 模态框（Modal） -->
			<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" id="admin_dialog_show">
				
				</div>
			</div>


<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/voucher/exchangePage.html';  //TODO
var syslog_events = {
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除该代金券吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/voucher/deleteExchange.html?id='+row.id, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#sysUser_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .edit':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/voucher/toExchangeEdit.html?id='+row.id;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	}
    	
};


var goods_columns = [
{field: 'voucherMoney',title: '金额',valign: 'top',sortable: false},
{field: 'exchangeVoucher',title: '兑换积分',valign: 'top',sortable: false},
{field: 'createTime',title: '创建时间',valign: 'top',sortable: false},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];


$('#sysUser_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: goods_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#sysUser_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#sysUser_table').bootstrapTable('refresh', null);
	}
	
	
	
	//编辑和删除图标
	function operateFormatter(value,row,index){
		return ['<t:buttonOut url="/voucher/toExchangeEdit.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs edit">',
		'编辑','</a></t:buttonOut><t:buttonOut url="/voucher/deleteExchange.html"><button type="button" class="btn  btn-danger btn-xs">',
		'删除','</button></t:buttonOut>'].join('');
	}
	
	
	  //添加代金券
    function exchangeAdd(){
    	var url= '<%=request.getContextPath()%>/voucher/toExchangeAdd.html';
    	$("#admin_dialog_show").empty();
		$("#admin_dialog_show").load(url);//增加需要先清空再加载
		$("#admin_dialog").modal('show');
	  }
    
	
</script>