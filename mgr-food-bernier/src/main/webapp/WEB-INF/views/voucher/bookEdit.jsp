<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">编辑代金券</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" method="post" accept-charset="utf-8" action="<%=request.getContextPath()%>/voucher/bookEdit.html">
                   	  <input type="hidden"  name="id" value="${voucher.id }">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>代金券金额：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"   name="voucherMoney" value='<fmt:formatNumber type="number" value="${voucher.voucherMoney}" maxFractionDigits="2" groupingUsed="false" />'   class="form-control col-md-7 col-xs-12" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" errormsg="金额最多六位,小数点后最多两位！" sucmsg=" " onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" nullmsg="必填" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>预定订单最低金额：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="bookOrderVoucher"  value='<fmt:formatNumber type="number" value="${voucher.bookOrderVoucher}" maxFractionDigits="2" groupingUsed="false" />'  class="form-control col-md-7 col-xs-12" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" errormsg="金额最多六位,小数点后最多两位！"  sucmsg=" " nullmsg="必填" onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
            </div>

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('编辑成功',{icon:1});    
    					$('#sysUser_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg('编辑失败',{icon:5});
    				}
    			}
            });
        });
    </script>
 