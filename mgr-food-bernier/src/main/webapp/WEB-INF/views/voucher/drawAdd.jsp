<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


 			<!--添加代金券-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">添加代金券</h4>
                </div>
                <form id="demo-form2" class="form-horizontal form-label-left mt10" novalidate="" accept-charset="utf-8" method="post" action="<%=request.getContextPath()%>/voucher/drawAdd.html">
                    <div class="model-box">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>代金券金额：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  name=voucherMoney class="form-control col-md-7 col-xs-12" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" errormsg="金额最多六位,小数点后最多两位！"   sucmsg=" " nullmsg="必填" onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>抽奖的代金券最低使用金额：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input   name="drawVoucher" class="form-control col-md-7 col-xs-12" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" errormsg="金额最多六位,小数点后最多两位！" sucmsg=" " nullmsg="必填" onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>抽奖的代金券概率：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  name="drawProbability" placeholder="概率总数不要超过100"  class="form-control col-md-7 col-xs-12" datatype="/^(([0-9]|([1-9][0-9]{0,2}))((\.[0-9]{1,2})?))$/,checkDrawProbability" errormsg="概率最多三位,小数点后最多两位！" sucmsg=" " nullmsg="必填" onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"  id="errormsgSpan"  ></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt30">
                        <button type="submit" class="btn btn-primary">提交</button>
                        <button type="button" class="btn btn btn-default" data-dismiss="modal">退出</button>
                    </div>
                </form>
        </div>
    <!--/添加代金券-->

                
                
      <script>
        $(function() {
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: 3,
                datatype:{
                	  "checkDrawProbability":function(gets,obj,curform,regxp){
    		    			/*参数gets是获取到的表单元素值，
    						  obj为当前表单元素，
    						  curform为当前验证的表单，
    						  regxp为内置的一些正则表达式的引用。*/
    		    			var falg = false;
    		            	$.ajax({ 
    		            		url: '<%=request.getContextPath()%>/voucher/checkDrawProbability.html?drawProbability='+gets, 
    		            		context: document.body, 
    		            		async: false,
    		            		success: function(date){
    		            			falg = date;
    		            			if(!falg){
    		            				setTimeout(function(){
	    		            				$("#errormsgSpan").html("概率总数不能超过100%");
    		            				}, 3);
    		            			}
    		            			
    		            		}
    		            	});
    		            	return falg;
    		            	
    		    		}
  		    	},
                ajaxPost : true,
                callback : function(vo) {
    				if (vo.success) {
    					layer.msg('添加成功',{icon:1});    
    					$('#sysUser_table').bootstrapTable('refresh', null);
    					$("#admin_dialog").modal('hide');
    				} else {
    					 layer.msg('添加失败',{icon:5});
    				}
    			}
            });
        });
    </script>
 