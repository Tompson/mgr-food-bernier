<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 <script type="text/javascript" src="<%=request.getContextPath()%>/bernier/engine.js"></script>   
 <script type="text/javascript" src="<%=request.getContextPath()%>/bernier/util.js"></script>  
 <script type="text/javascript" src="<%=request.getContextPath()%>/bernier/interface/SendMsg.js"></script> 
 <script type="text/javascript" src="<%=request.getContextPath()%>/man/js/jquery/jquery-1.7.2.min.js"></script> 
 <script type="text/javascript">
	$(function() {
		//这句话千万不能少 ，表示允许使用推送技术  
		dwr.engine.setActiveReverseAjax(true);
	});
	//这个函数是提供给后台推送的时候  调用的    
	function show(order) {
		$("#message").text("您有新的订单信息!!!"+order);
	}
</script> 


		<style>
	 		.message{width: 280px; height: 200px; border: 1px solid #ccc; position: fixed;
	 		right:15px;bottom:-200px; z-index:30;background:#fff;}
	 		.message h6{height:40px;border-bottom:1px solid #ccc; line-height: 40px;font-size:16px; background:#23527c;
	 		color:#fff;text-align: center; margin:0;}
	 		.mes-close{line-height: 40px;margin-right:10px;font-size:14px;cursor: pointer;font-style:normal; float:right }
	 		.mes-close:hover{color:red;}
	 		.message a{cursor: pointer;color:#458FCE;}
	 		.mes-word{ text-indent:2em;padding:30px 10px; }
	 		.message .mes-menu{width:100%; position: absolute;bottom:0;height: 30px;border-top:1px solid #ccc;
	 		text-align: right;font-size:12px;line-height: 30px;padding-right:10px;}
 		</style>
 		
 		<script>
 		
 		$(function(){
 		

			//这句话千万不能少 ，表示允许使用推送技术  
 			dwr.engine.setActiveReverseAjax(true);
	 		
	 		$(".mes-close").on("click",function(){
 			$(".message").animate({'bottom':"-200px"},1000);
 	 		});
	 		
	 		$(".mes-menu a").on("click",function(){
	 			window.name=1;
 	 			changeRightMenu('<%=request.getContextPath()%>/sysOrder/list.html'); //把这改了是我订单页面 
	 		});
 		});
 		
  		var setTime=null;//全局变量 每推送一次弹出一次,并且重新记秒
  		//这个函数是提供给后台推送的时候  调用的    
  		function show(payNumber,orderPerson) {
 			clearTimeout(setTime);
  			$(".message").animate({'bottom':0},1000);
 			$("#noteiceOrderId").text(payNumber);
 			$("#contactPerson").text(orderPerson);
  		     if($("#ddsound")){
 		    	$("#ddsound").remove();
 		     }
 			var audio='<audio id="ddsound" autoplay="autoplay">' +
               '<source class="dd2" src="<%=request.getContextPath()%>/sound/orderSound.mp3" type="audio/mpeg"> '+ 
              '</audio>';
			$("#message").append(audio);
 			$("#ddsound").play();
 		      setTime=setTimeout(function(){
 	 			$(".message").animate({'bottom':"-200px"},1000);
	 		},1000);//10秒钟
  		}
 		</script>
 		
 		
	<div id="message" class="message">
		<h6><em class="mes-close fr em">X</em>您有新的订单 </h6>
		<div class="mes-word">
			<p>订单号为:<b id="noteiceOrderId"></b></p><br>
			<p>联系人为:<b id="contactPerson"></b></p>
		</div>
		<p class="mes-menu"><a>跳转到订单列表</a></p>
	</div>

