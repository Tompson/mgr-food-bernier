<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>  
<script type="text/javascript"	src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>

 <script type="text/javascript"> 
<%-- $(function(){
	
	var flag = '${empty picS?1:0}'; 
	  console.log("flag="+flag);
	  if(flag == 0) {
		  var pics = ${picS};
		  $.each(pics, function(i,val){  
			   $(".form-item img").eq(i).attr('src',val);
			   $(".form-item input[name=goodsPics]").eq(i).val(val);
			  });
	  } 
	  
    //上传图片
    $(".form-file").on("change", function() {
        var imgFile = $(this)[0];
        var id=$(this).attr("id");
        var filextension = imgFile.value.substring(imgFile.value.lastIndexOf("."), imgFile.value.length);
        filextension = filextension.toLowerCase();
        if ((filextension != '.jpg') && (filextension != '.gif') && (filextension != '.jpeg') && (filextension != '.png') && (filextension != '.bmp')) {
            alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
            imgFile.focus();
        } else {
          var path;

            if (document.all){//IE
                imgFile.select();
              path = document.selection.createRange().text;
                imgFile.previousSbiling.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")"; //使用滤镜效果 
            } else{//FF
            	  $.ajaxFileUpload({  
            		  url:"<%=request.getContextPath()%>/uploadPic/filesUpload.html",
	 	  	            secureuri:false,  
	 	  	            fileElementId:id,//file标签的id  
	 	  	            dataType: 'json',//返回数据的类型  
	 	  	            //data:{name:'logan'},//一同上传的数据  
	 	  	            success: function (data, status) {  
	 	  	            	console.log(data);
	 	  	            	$("#"+id).siblings('img')
		  	            	$("#"+id).siblings('img').attr('src',data);
		  	            	$("#"+id).siblings('input[type=hidden]').val(data);
// 	 	  	             $(imgFile).siblings("img").attr("src", data)
	 	  	            },
	 	  	            error: function (data, status, e) {  
	 	  	            	alert("文件上传失败!");   
	 	  	            }  
	 	  	        });
            }
        }
    });
	
	
 
   }); --%>
</script> 
<style>
.datetimepicker-minutes table th{visibility: hidden !important;}
.datetimepicker-hours table th{visibility: hidden !important;}
</style>

				<div class="mt50">
                    <form id="demo-form2" class="form-horizontal form-label-left" novalidate="" method="get" accept-charset="utf-8" action="<%=request.getContextPath()%>/system/saveParam.html">
						<input type="hidden" name="id"  value="${sysParm.id}"/>
						<input type="hidden" name="sysParmLinkTimeId"  value="${sysParm.sysParmLinkTimeId}"/>
                      <%--   <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>营业时间：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-group">
                                    <input type="text" name="startTime1" id="datestart" readonly="readonly" value='<fmt:formatDate value="${sysParm.startTime}" pattern="HH:mm"/>'   class="form-control" datatype="*" sucmsg=" " nullmsg="必填">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary rest" >到</button>
                                          </span>
                                    <input type="text" name="endTime1" id="dateend" readonly="readonly"  value='<fmt:formatDate value="${sysParm.endTime}"   pattern="HH:mm"/>'   class="form-control" datatype="*" sucmsg=" " nullmsg="必填">
                                </div>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                       
                        </div> --%>
                       <%--  <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-group">
                                    <input type="text" name="startTimePm1" id="datestart1" readonly="readonly" value='<fmt:formatDate value="${sysParm.startTimePm}" pattern="HH:mm"/>'   class="form-control" datatype="*" sucmsg=" " nullmsg="必填">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary rest" >到</button>
                                          </span>
                                    <input type="text" name="endTimePm1" id="dateend1" readonly="readonly"  value='<fmt:formatDate value="${sysParm.endTimePm}"   pattern="HH:mm"/>'   class="form-control" datatype="*" sucmsg=" " nullmsg="必填">
                                </div>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                       
                        </div> --%>
                       <%--  <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>起步价：
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="startPrice" value='<fmt:formatNumber type="number" value="${sysParm.startPrice}" maxFractionDigits="2" groupingUsed="false" />'  class="form-control col-md-6 col-xs-12" errormsg="价格最多六位,小数点后最多两位" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" 
                                onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" sucmsg=" " nullmsg="必填" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 dashboard-widget-content">元</div>
                        </div> --%>

                       <%--  <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>配送费：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="middle-name" class="form-control col-md-6 col-xs-12"  name="deliverPrice" value='<fmt:formatNumber type="number" value="${sysParm.deliverPrice}" maxFractionDigits="2" groupingUsed="false" />'  errormsg="价格最多六位,小数点后最多两位" datatype="/^(([0-9]|([1-9][0-9]{0,5}))((\.[0-9]{1,2})?))$/" 
                                onKeypress="return (/[0-9.]/.test(String.fromCharCode(event.keyCode)))" sucmsg=" " nullmsg="必填" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 dashboard-widget-content">元</div>
                        </div> --%>
<!--                         <div class="form-group"> -->
<!--                             <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>预定折扣：</label> -->
<!--                             <div class="col-md-6 col-sm-6 col-xs-12"> -->
<!--                                 <div class="input-group"> -->
<!--                                     <span class="input-group-btn"> -->
<!--                                     <button type="button" class="btn btn-primary rest"   >满</button> -->
<!--                                           </span> -->
<%--                                     <input type="number" class="form-control" datatype="/^[1-9]\d{0,5}$/" errormsg="预定折扣满减,请输入6位以内的正整数!"  sucmsg=" " nullmsg="必填" name="bookDiscount" value="${fn:split(sysParm.bookDiscount, ',')[0]}" > --%>
<!--                                     <span class="input-group-btn"> -->
<!--                                     <button type="button" class="btn btn-primary rest"  >减</button> -->
<!--                                           </span> -->
<%--                                     <input type="number" class="form-control" datatype="/^[1-9]\d{0,5}$/" errormsg="预定折扣满减,请输入6位以内的正整数"   sucmsg=" " nullmsg="必填" name="bookDiscount2"  value="${fn:split(sysParm.bookDiscount, ',')[1]}"> --%>
<!--                                 </div> -->
<!--                                 <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span> -->
<!--                             </div> -->
<!--                             <div class="col-md-3 col-sm-3 col-xs-12 dashboard-widget-content">元</div> -->
<!--                         </div> -->
                        
                       <%--    <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>消费金额-积分兑换比例：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-group">
                                   <span class="input-group-btn "><button type="button" class="btn btn-primary rest"   >消费</button></span>
                                    <input type="number" class="form-control" name="integralProportion" value="${fn:split(sysParm.integralProportion, ',')[0]}" datatype="/^[1-9]\d{0,5}$/" errormsg="消费金额-积分兑换比例,请输入6位以内的正整数!" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" sucmsg=" " nullmsg="必填">
                                    <span class="input-group-btn"><button type="button" class="btn btn-primary rest"   >积分</button></span>
                                    <input type="number" class="form-control" name="integralProportion1" value="${fn:split(sysParm.integralProportion, ',')[1]}" datatype="/^[1-9]\d{0,5}$/" errormsg="消费金额-积分兑换比例,请输入6位以内的正整数!" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" sucmsg=" " nullmsg="必填">
                                </div>
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div> --%>
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>评论订单/每次：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="birthday" class="date-picker form-control col-md-6 col-xs-12"   name="commentOrderEvery"  value='<fmt:formatNumber type="number" value="${sysParm.commentOrderEvery}" maxFractionDigits="0" groupingUsed="false" />'  datatype="/^[1-9]\d{0,5}$/" errormsg="请输入6位以内的正整数!"
                                onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" sucmsg=" " nullmsg="必填" type="number">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 dashboard-widget-content">积分</div>
                        </div>
                        
                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>分享/每次：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                <input id="birthday"  name="shareEvery"   value='<fmt:formatNumber type="number" value="${sysParm.shareEvery}" maxFractionDigits="0" groupingUsed="false" />'  class="date-picker form-control col-md-6 col-xs-12" datatype="/^[1-9]\d{0,5}$/" errormsg="请输入6位以内的正整数!" 
                                onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" sucmsg=" " nullmsg="必填" type="number" />
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 dashboard-widget-content">积分</div>
                        </div>
                      <%--    <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>配送范围：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                <input id="birthday"  name="deliverScope"  value="${sysParm.deliverScope}"  class="date-picker form-control col-md-6 col-xs-12"  datatype="/^[1-9]\d{0,7}$/" errormsg="请输入8位以内的正整数!" sucmsg=" " nullmsg="必填" type="text" />
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 dashboard-widget-content">米</div>
                        </div> --%>
                        <%--  <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>首页视频链接：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                <input id="birthday"  name="videoLink"    value="${sysParm.videoLink}"  class="date-picker form-control col-md-6 col-xs-12" datatype="url" errormsg="请输入正确的网址!" sucmsg=" " nullmsg="必填" type="text"  placeholder="请输入视频源链接,格式如:http://100051.hls.syun.tv/live/MCLZ02.m3u8" />
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div> --%>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>游戏图片(<em>450*300</em>)：
                            
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-item form-item-wd">
                        <c:if test="${empty sysParm.videoLink}">
	                  		<img src="<%=request.getContextPath()%>/images/upload.png" alt="" style="width:150px;height: 100px">
	                        <input class="form-file" type="file" name="file" id="goodsPic1" value="" placeholder="">
	                        <input type="hidden" name="videoLink"  id="gpic1"  datatype="*" sucmsg="&nbsp;" nullmsg="必填" />
	                	</c:if>
	                    	<c:if test="${not empty sysParm.videoLink}">
	                  		<img src="${sysParm.videoLink}" alt="" style="width:150px;height: 100px">
	                        <input class="form-file" type="file" name="file" id="goodsPic1" value="" placeholder="">
	                        <input type="hidden" name="videoLink"  id="gpic1"  />
	                      <span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
	                  </c:if>   
	                   			<span class="col-md-12 col-sm-12 red form-yz Validform_checktip"></span>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>游戏链接：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                <input   name="advertImage" placeholder="请输入游戏链接,如果是外网链接,请以http://开头"     value="${sysParm.advertImage}"  class="date-picker form-control col-md-6 col-xs-12" datatype="url" errormsg="请输入正确的网址!" sucmsg=" " nullmsg="必填" type="text" />
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div> 
                        
                         <%-- <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>图片链接2：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                <input   name="imgLinkB" placeholder="请输入广告链接,如果是外网链接,请以http://开头"     value="${sysParm.imgLinkB}"  class="date-picker form-control col-md-6 col-xs-12" datatype="url" errormsg="请输入正确的网址!" sucmsg=" " nullmsg="必填" type="text" />
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div> --%>
                        
                    <%--      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required red">*</span>图片链接3：
                        	</label>
                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                <input   name="imgLinkC" placeholder="请输入广告链接,如果是外网链接,请以http://开头"     value="${sysParm.imgLinkC}"  class="date-picker form-control col-md-6 col-xs-12" datatype="url" errormsg="请输入正确的网址!" sucmsg=" " nullmsg="必填" type="text" />
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div> --%>
                        
                   <%--       <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required red">*</span>商户电话：</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="first-name"  name="bookDiscount" value="${sysParm.bookDiscount}" class="form-control col-md-7 col-xs-12" datatype="m" maxlength="14" sucmsg=" " errormsg="请填写正确的手机号码!"  nullmsg="必填"  type="text">
                                <span class="col-xs-12 col-md-12  red form-yz Validform_checktip"></span>
                            </div>
                        </div> --%>
                        
                        
                        <div class="ln_solid"></div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="b" class="btn btn-success">提交</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
                
                
                <style> /*写了个样式把Validform_msg强制掩藏了*/
                     #Validform_msg{display:none !important;} 
     			 </style>
                
<script>

        $(function() {
        	 //时间
            $('#datestart').datetimepicker({
            	 format: 'hh:ii',
                 autoclose: true,
                 keyboardNavigation: false,
                 startView: 1,
                 minuteStep: 10,
                 minView: 0,
                 maxView: 1
            }).on("click", function(ev) {
                $("#datestart").datetimepicker("setEndDate", $("#dateend").val());
            });

            $('#dateend').datetimepicker({
            	 format: 'hh:ii',
                 autoclose: true,
                 keyboardNavigation: false,
                 startView: 1,
                 minuteStep: 10,
                 minView: 0,
                 maxView: 1
            }).on("click", function(ev) {
                $("#dateend").datetimepicker("setStartDate", $("#datestart").val());
              
            });
        	   
                
            
            //第二段时间
            $('#datestart1').datetimepicker({
           	 format: 'hh:ii',
                autoclose: true,
                keyboardNavigation: false,
                startView: 1,
                minuteStep: 10,
                minView: 0,
                maxView: 1
           }).on("click", function(ev) {
        	   $("#datestart1").datetimepicker("setStartDate", $("#dateend").val());
               $("#datestart1").datetimepicker("setEndDate", $("#dateend1").val());
           });

           $('#dateend1').datetimepicker({
           	 format: 'hh:ii',
                autoclose: true,
                keyboardNavigation: false,
                startView: 1,
                minuteStep: 10,
                minView: 0,
                maxView: 1
           }).on("click", function(ev) {
               $("#dateend1").datetimepicker("setStartDate", $("#datestart1").val());
           });
           
           
           $(function(){
        	   //上传图片
        	   $(".form-file").on("change", function() {
        	       var imgFile = $(this)[0];//当前第一个jq对象 jquery对象就是一个数组对象.
        	       var id=$(this).attr("id");
        	       var filextension = imgFile.value.substring(imgFile.value.lastIndexOf("."), imgFile.value.length);
        	       filextension = filextension.toLowerCase();
        	       if ((filextension != '.jpg') && (filextension != '.gif') && (filextension != '.jpeg') && (filextension != '.png') && (filextension != '.bmp')) {
        	           alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
        	           imgFile.focus();
        	       } else {
        	           var path;
        	           if (document.all){//IE
        	               imgFile.select();
        	               path = document.selection.createRange().text;
        	               imgFile.previousSbiling.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")"; //使用滤镜效果 
        	           } else{//FF
        	          	 console.log(11111);
        	          	  $.ajaxFileUpload({  
        	  	  	            url:"<%=request.getContextPath()%>/uploadPic/filesUpload.html",
        	  	  	            secureuri:false,  
        	  	  	            fileElementId:id,//file标签的id  
        	  	  	            dataType: 'json',//返回数据的类型  
        	  	  	            //data:{name:'logan'},//一同上传的数据  
        	  	  	            success: function (data, status) {  
        	  	  	            	$("#"+id).siblings('img')
        	  	  	            	$("#"+id).siblings('img').attr('src',data);
        	  	  	            	$("#"+id).siblings('input[type=hidden]').val(data);
        	  	  	            	
//        	   	  	            	 $(imgFile).siblings("img").attr("src", data)
        	  	  	            },
        	  	  	            error: function (data, status, e) {  
        	  	  	            	alert("文件上传失败!");   
        	  	  	            }  
        	  	  	        });
        	           }
        	        }
        	   	});
        	   });
                
            //正确性校验
            $("#demo-form2").Validform({
                tiptype: function(msg, o, cssctl) { //验证信息出现位置
                    var objtip = $(o.obj).parents(".form-group").find(".Validform_checktip");
                    cssctl(objtip, o.type);
                    objtip.text(msg);
                },
                tiptype: 3,
                ajaxPost : true,
    			callback : function(data) {
    				if (data.success) {
    					changeRightMenu('<%=request.getContextPath()%>/system/showParam.html');
    					 layer.msg('更新成功',{icon:1});
    				} else {
    					layer.msg('更新失败',{icon:5});
    				}
    			}
            });
        });
    </script>
