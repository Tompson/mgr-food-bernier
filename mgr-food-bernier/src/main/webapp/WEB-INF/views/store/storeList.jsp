<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
 <%@ taglib prefix="t" uri="/WEB-INF/tld/deyi.tld"%>
<div class="form-inline">
	<form id="deliverMan_table_form">
				<!-- 账号登录类型 -->
                 <div class="form-group">
                     <input type="text" class="form-control" placeholder="门店名称" name="storeName">
                 </div>
                 <div class="form-group">
                     <input type="text" class="form-control" placeholder="帐号" name="account">
                 </div> 
                   <div class="form-group">
                     <input type="text" class="form-control" placeholder="手机号" name="contactPhone">
                 </div>
                 <div class="form-group">
                     <select name="storeType" class="form-control">
                   <option value="">类型</option>
                   <option value="1">加盟店</option>
                   <option value="2">自营店</option>
               		</select>
                 </div>
                 <div class="form-group">
                     <select name="status" class="form-control">
                   <option value="">状态</option>
                   <option value="1">启用</option>
                   <option value="2">禁用</option>
               		</select>
                 </div>
                 <div class="form-group btn-group-vertical">
                     <button type="button" class="btn btn-primary" onclick="query()">查询</button>
                </div>
                <div class="form-group btn-group-vertical alignright">
                	<t:buttonOut url="/store/toStoreAdd.html">
                     <a href="javascript:void(0)" class="btn btn-primary" onclick="storeAdd()">添加门店</a>
                     </t:buttonOut>
                   <%--  <a href="javascript:void(0)" class="btn btn-primary" onclick="changeRightMenu('<%=request.getContextPath()%>/store/toStoreAdd.html')">添加门店</a> --%>
                </div>
               </form>
               
               
               
               
               
               
    </div>
          
            
            <!-- 表里面的数据和分页里面的数据 -->
            <div class="mt20">
                <table id="deliverMan_table"></table>
            </div>
            
            <style> /*写了个样式把Validform_msg强制掩藏了*/
                 #Validform_msg{display:none !important;} 
 			 </style>
            
         <!-- 模态框（Modal） -->
<div class="modal fade" id="admin_dialog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" id="admin_dialog_show">
	
	</div>
</div>

<script type="text/javascript">
var url  = '<%=request.getContextPath()%>/store/page.html';  //TODO
var syslog_events = {
		'click input[type=checkbox]':function(e,value,row,index){
			
			console.log("row:"+row.status);//1启用2禁用
			var _this=this;
			if(row.status == 1){ //启用就禁用
					$.ajax({ 
	      				url: '<%=request.getContextPath()%>/store/disableState.html?id='+row.id, 
	      				context: document.body, 
	      				async: false,
	      				success: function(vo){
		      				if(vo.success){
		      					layer.msg('操作成功!',{icon:1});
		      					$(_this).prop("checked",false);
		      					row.status=2;
		      				}else{
		      					layer.msg('操作失败!',{icon:5});
		      					$(_this).prop("checked",true);
		      					row.status=1;
		      				}
	      				}
	      			});
			}else{//禁用就启用
				$.ajax({
	    			url:'<%=request.getContextPath()%>/store/enableState.html?id='+row.id,
	    			context: document.body, 
	    			async:false,
	    			success: function(vo){
	      				if(vo.success){
	      					layer.msg('操作成功!',{icon:1});
	      					$(_this).prop("checked",true);
	      					row.status=1;
	      				}else{
	      					layer.msg('操作失败!',{icon:5});
	      					$(_this).prop("checked",false);
	      					row.status=2;
	      				}
	  				},
	  				error:function(vo){
	  					console.log(vo);
	  				}
	    		});
			}
		},
		'click button[type=button]':function(e,value,row,index){
     		layer.confirm('确定删除吗?', {btn: ['确定','取消']},  
     	  		function(){
     	  			$.ajax({ 
     	          		url: '<%=request.getContextPath()%>/store/deleteStore.html?id='+row.id, 
     	          		context: document.body, 
     	          		async: false,
     	          	 	dataType:'json',
     	          		success: function(vo){
     	          			$('#deliverMan_table').bootstrapTable('refresh', null);
     	          			if(vo.success){
     	          				layer.msg('删除成功!',{icon:1});
     	          			}else{
     	          				layer.msg('删除失败!',{icon:5});
     	          			}
     	          		}
     	          	});
     	  		}
     	  	);
    	},
    	'click .edit':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/store/toStoreEdit.html?id='+row.id;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	},
    	'mouseenter .ewm-img':function(e,value,row,index){//鼠标悬停
    		console.log(1);
   		   var evm_offset=$(this).offset();
   	        $("<div class='img-da'><img src="+ $(this).attr("src")+" alt=''></div>").appendTo($("body"));
   	        $(".img-da").css({left:evm_offset.left+50,top:evm_offset.top-100})
    	},
    	'mouseleave .ewm-img':function(e,value,row,index){//鼠标离开
    		 $(".img-da").remove(); 
    	},
    	'click .resetPasswd':function(e,value,row,index){
    		layer.confirm('确定重置密码吗？',{btn:['确认','取消']},
    		function(){
    			 $.ajax({
    				 url:'<%=request.getContextPath()%>/store/resetStorePasswd.html?id='+row.id,
    				 context:document.body,
    				 async:false,
    				 dataType:'json',
    				 success:function(vo){
    					 $('#deliverMan_table').bootstrapTable('refresh', null);
  	          			if(vo.success){
  	          				layer.msg('重置成功!',{icon:1});
  	          			}else{
  	          				layer.msg('重置失败!',{icon:5});
  	          			}
    				 }
    			 });
    		}
    	 );
    	},
    	'click .updateIsActivity':function(e,value,row,index){
    		 $.ajax({
				 url:'<%=request.getContextPath()%>/store/updateIsActivity.html?id='+row.id,
				 context:document.body,
				 async:false,
				 dataType:'json',
				 success:function(vo){
					 $('#deliverMan_table').bootstrapTable('refresh', null);
	          			if(vo.success){
	          				layer.msg('成功!',{icon:1});
	          			}else{
	          				layer.msg('失败!',{icon:5});
	          			}
				 }
			 });
    	},
    	'click .addStoreGoods':function(e,value,row,index){
    		var url='<%=request.getContextPath()%>/store/toaddStoreGoods.html?id='+row.id;
    		$("#admin_dialog_show").load(url); //编辑不用清空
    		$("#admin_dialog").modal('show');
    	}
    	
    	
};


var deliverMan_columns = [
/* {field: 'id',title: '编号',valign: 'top',sortable: false}, */
{field: 'storeName',title: '门店名称',valign: 'top',sortable: false},
{field: 'account',title: '帐号',valign: 'top',sortable: false},
{field: 'contactPhone',title: '手机号',valign: 'top',sortable: false},
{field: 'address',title: '门店地址',valign: 'top',sortable: false},
{field: 'storeType',title: '类型',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter3},
{field: 'status',title: '禁用/启用',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter1},
{field: 'createDate',title: '注册时间',valign: 'top',sortable: false,events:syslog_events, formatter:operateFormatter4},
{field: 'option',title: '操作',valign: 'top',sortable: false,events:syslog_events,formatter : operateFormatter}
];

$('#deliverMan_table').bootstrapTable({
	method: 'post',
	url: url,
	cache: false,
	striped: true,
	pagination: true,
	pageList: [10,20,50],
	pageSize:10,
	pageNumber:1,
	search: false,
	sidePagination:'server',
	queryParams: queryParams,
	showColumns: false,
	showRefresh: false,
	clickToSelect: true,
	showToggle:false,
	showPaginationSwitch:false,
	cardView:false,
	columns: deliverMan_columns
	});
	
	function queryParams(params) {
		var temp = {
					limit:params.limit,
					offset:params.offset,
					sort:params.sort,
					object:$("#deliverMan_table_form").serializeObject()
				}
		return temp
	}
	function query(){//条件查询
		$('#deliverMan_table').bootstrapTable('refresh', null);
	}
	function operateFormatter4(value,row,index){
		console.log(row.createDate)
		var date = new Date(row.createDate);
		var time1 = date.Format("yyyy-MM-dd hh:mm:ss");
		return time1;
	}
	function operateFormatter2(value,row,index){
		console.log(value)
		if(!!!value){
			return ['<image  class="ewm-img" width="50" height="50" src="../images/upload.png" alt="" />'].join('');
		}else{
			return ['<image  class="ewm-img" width="50" height="50" src="'+value+'" alt="" />'].join('');
		}
		
	}
	
	//编辑和删除图标
	function operateFormatter(value,row,index){
		if(row.isActivity==1){
			return ['<t:buttonOut url="/store/toStoreEdit.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs edit">',
					'编辑','</a></t:buttonOut><t:buttonOut url="/store/deleteStore.html"><button type="button" class="btn  btn-danger btn-xs">',
					'删除','</button></t:buttonOut><t:buttonOut url="/store/resetStorePasswd.html"><button type="button" class="btn  btn-danger btn-xs resetPasswd">',
					'重置密码','</button></t:buttonOut><t:buttonOut url="/store/updateIsActivity.html"><button type="button" class="btn  btn-danger btn-xs updateIsActivity">',
			'参与平台优惠','</button></t:buttonOut><t:buttonOut url="/store/toaddStoreGoods.html"><button type="button" class="btn  btn-danger btn-xs addStoreGoods">',
			'添加菜品','</button></t:buttonOut>'].join('');
		}
		if(row.isActivity==2){
			return ['<t:buttonOut url="/store/toStoreEdit.html"><a href="javascript:void(0)" class="btn  btn-warning btn-xs edit">',
					'编辑','</a></t:buttonOut><t:buttonOut url="/store/deleteStore.html"><button type="button" class="btn  btn-danger btn-xs">',
					'删除','</button></t:buttonOut><t:buttonOut url="/store/resetStorePasswd.html"><button type="button" class="btn  btn-danger btn-xs resetPasswd">',
					'重置密码','</button></t:buttonOut><t:buttonOut url="/store/updateIsActivity.html"><button type="button" class="btn  btn-danger btn-xs updateIsActivity">',
					'退出平台优惠','</button></t:buttonOut><t:buttonOut url="/store/toaddStoreGoods.html"><button type="button" class="btn  btn-danger btn-xs addStoreGoods">',
					'添加菜品','</button></t:buttonOut>'].join('');
		}
		
	}
	//编辑和删除图标
	function operateFormatter3(value,row,index){
		if(row.storeType==1){
			return ['加盟店'].join('');
		}
		if(row.storeType==2){
			return['自营店'].join('');
		}
	}
	//启用和禁用图标
	 function operateFormatter1(value,row,index){
		if(row.status==1){//1启用 2停用     
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" checked="checked">'+
	 		'<span class="ace-tg"></span></div>';
		}
		if(row.status==2){
			return '<div class="ace-tab"><input class="ace-ck" type="checkbox" name="" value="" >'+
	 		'<span class="ace-tg"></span></div>';
		}
}
	//添加配送员
    function storeAdd(){
    	var url= '<%=request.getContextPath()%>/store/toStoreAdd.html';
    	$("#admin_dialog_show").empty();
		$("#admin_dialog_show").load(url);//增加需要先清空再加载
		$("#admin_dialog").modal('show');
	  }
	

    
	
</script>