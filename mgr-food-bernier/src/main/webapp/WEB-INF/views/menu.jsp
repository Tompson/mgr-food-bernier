  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 	 <div class="col-md-3 left_col">
                <div class="scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="javascript:void(0)" class="site_title"><i class="fa fa-cog"></i> <span>饭好粥道</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="<%=request.getContextPath()%>/images/img.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>欢迎您,</span>
                            <h2>${userInfo.userName}</h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <aside id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                          <ul class="nav side-menu">
                                <li><a href="javascript:void(0)" onclick="changeRightMenu('<%=request.getContextPath()%>/system/indexmain.html')"><i class="fa fa-home"></i> Home <span ></span></a></li>
			                     <c:forEach items="${menuList}" var="level1Menu">
				      			   <c:if test="${level1Menu.menuLevel=='1' }">
									<li>
										 <li><a><i class="${level1Menu.menuClass}"></i> ${level1Menu.menuName} <span class="fa fa-chevron-down"></span></a>
										
						                <ul class="nav child_menu">
											<c:forEach items="${menuList}" var="level2Menu">
												<c:if test="${level1Menu.menuId==level2Menu.parentMenuId }">
								                 	 <li>
								                 		<a onclick="changeRightMenu('<%=request.getContextPath()%>${level2Menu.menuUrl}')">
								                 			${level2Menu.menuName}
								                 		</a>
								                 	 </li>
							                 	</c:if>
											</c:forEach>
						                 	
						                </ul>
									</li>
								    </c:if>
				 			     </c:forEach> 
                           <%--  
                                 <li><a><i class="fa fa-jpy"></i> 代金券管理 <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a onclick="changeRightMenu('<%=request.getContextPath()%>/voucher/exchangeList.html')">积分兑换代金券</a></li>
                                    	<li><a onclick="changeRightMenu('<%=request.getContextPath()%>/voucher/bookList.html')">预定返代金券</a></li>
                                    	<li><a onclick="changeRightMenu('<%=request.getContextPath()%>/voucher/drawList.html')">抽奖代金券</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-money"></i> 订单管理<span>(<em id="refreshMenu">${statOrderStatusEqUntreated}</em>)</span> <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                   	    <li><a onclick="changeRightMenu('<%=request.getContextPath()%>/sysOrder/list.html')">订单列表</a></li>
                                        <li><a onclick="changeRightMenu('<%=request.getContextPath()%>/orderComment/list.html')">订单评论</a></li>
                                         <li><a onclick="changeRightMenu('<%=request.getContextPath()%>/deliverManComment/list.html')">配送员评论</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-user"></i>会员管理 <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                     	<li><a onclick="changeRightMenu('<%=request.getContextPath()%>/member/list.html')">会员列表</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-gear"></i>系统管理 <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a onclick="changeRightMenu('<%=request.getContextPath()%>/sysUser/list.html')">管理员列表</a></li>
                                    	<li><a onclick="changeRightMenu('<%=request.getContextPath()%>/deliverMan/list.html')">配送员管理</a></li>
                                    	<li><a onclick="changeRightMenu('<%=request.getContextPath()%>/system/showParam.html')">系统参数</a></li>
                                    	<li><a onclick="changeRightMenu('<%=request.getContextPath()%>/groupPurchase/list.html')">团购团餐说明申请</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-line-chart"></i>数据统计 <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a onclick="changeRightMenu('<%=request.getContextPath()%>/statDeliverMan/list.html')">配送统计</a></li>
                                        <li><a onclick="changeRightMenu('<%=request.getContextPath()%>/statGoodsSales/list.html')">菜品销售统计</a></li>
                                    </ul>
                                </li>  --%>
                            </ul>   
                        </div>
                    </aside>
                </div>
            </div>