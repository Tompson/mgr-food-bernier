window.timeList=null;//全局变量

function loginOut(){
	layer.confirm('你确定退出系统?', {
		  btn: ['确定 ','取消 '] //按钮
		},
		function(){
			$("#loginOutForm").submit();
			}
		)}


function getFormInfo(formId){  
	
	var info = new Array();    
	getInputInfo(formId,info) + getSelectInfo(formId,info)
	var params = "";
	for (var j = 0; j < info.length; j++){  
		if(j == 0){
			params = "?"+info[j];
		}else{
			params = params + "&"+info[j];
		}
	}
    return params;     
}  

function getInputInfo(formId,info){  
	var form = document.getElementById(formId,info);   
	if(!!!form){
		return;
	}
	var elements = new Array();    
	var tagElements = form.getElementsByTagName('input');  
    for (var j = 0; j < tagElements.length; j++){  
    	var tagElement = tagElements[j];
    	if(tagElement.type.toLowerCase() == 'text'){
    		elements.push(tagElement);  
    	}
    	if(tagElement.type.toLowerCase() =='hidden'){
    		elements.push(tagElement);  
    	}
    }  
    for (var i = 0; i < elements.length; i++){  
         var element = elements[i]
         var name = element.name;    
         var value = encodeURIComponent(element.value);
         if("" != value){
        	 info.push(name+"="+value);  
         }
    }  
}


function getSelectInfo(formId,info){  
	var form = document.getElementById(formId);    
	if(!!!form){
		return;
	}
	var tagElements = form.getElementsByTagName('select');    
    var params = "";
    for (var i = 0; i < tagElements.length; i++){  
         var select = tagElements[i];
         var name = select.name;    
         var value = select.options[select.selectedIndex].value;
         if("" != value){
        	 info.push(name+"="+value);  
         }
    }  
}

//查询
function query(url){
	 var params = getFormInfo('query_from');
	
	 url = url+params;
	 $("#page_data").empty();
	 $("#page_data").load(url);
}

//分页请求
function toPage(url,pageNo){
	 var params = getFormInfo('query_from');
	 //url = url+params+"&pageNo="+pageNo;
	 
	 if(!!!params || params.length==0){
		 url = url+params+"?pageNo="+pageNo;
	 }else{
		 url = url+params+"&pageNo="+pageNo;
	 }
	 $("#page_data").empty();
	 $("#page_data").load(url);
}

//echarts x轴合理显示
function getEchart(params){
	var newParamsName = "";// 最终拼接成的字符串
    var paramsNameNumber = params.length;// 实际标签的个数
    var provideNumber = 3;// 每行能显示的字的个数
    var rowNumber = Math.ceil(paramsNameNumber / provideNumber);// 换行的话，需要显示几行，向上取整
    /**
     * 判断标签的个数是否大于规定的个数， 如果大于，则进行换行处理 如果不大于，即等于或小于，就返回原标签
     */
    // 条件等同于rowNumber>1
    if (paramsNameNumber > provideNumber) {
        /** 循环每一行,p表示行 */
        for (var p = 0; p < rowNumber; p++) {
            var tempStr = "";// 表示每一次截取的字符串
            var start = p * provideNumber;// 开始截取的位置
            var end = start + provideNumber;// 结束截取的位置
            // 此处特殊处理最后一行的索引值
            if (p == rowNumber - 1) {
                // 最后一次不换行
                tempStr = params.substring(start, paramsNameNumber);
            } else {
                // 每一次拼接字符串并换行
                tempStr = params.substring(start, end) + "\n";
            }
            newParamsName += tempStr;// 最终拼成的字符串
        }

    } else {
        // 将旧标签的值赋给新标签
        newParamsName = params;
    }
    //将最终的字符串返回
    return newParamsName
}


/**
 * xiongqq 2016-07-14
 * 当我们点击启用和禁用的时候可以使用
 * @param url  请求的路径
 * @param id   向请求的路径发送一个Key 为ID的一个值
 * @param callbackurl 请求成后返回的页面
 * @param pageNo	指定返回哪一个页面
 */
function start_able(url,data,callbackurl,pageNo){
	$.ajax({ 
		url: url, 
		data:data,
		context: document.body, 
		async: true,
		success: function(result){
			console.info(result.message);
			if(result.success){
				var msg = (!!!result.message) ? 'Success' : result.message;
				layer.msg(msg,{icon:1});
                toPage(callbackurl, pageNo); //成功调一下,刷新数据
			}else{
				var msg = (!!!result.message) ? 'Fail' : result.message;
				layer.msg(msg,{icon:5}); 
				toPage(callbackurl, pageNo);  //失败调一下,刷新数据
			}
		}
	});
}


function confirm_dialog(url,data,callbackurl,title,pageNo){
		pageNo = typeof(pageNo) == "undefined" ? 1 : pageNo;
		
		layer.confirm('<spring:message code="public.isDelete"/>', {
		  btn: ['<spring:message code="public.submit"/>','<spring:message code="public.cancel"/>'] //按钮
		}, 
		function(){
			$.ajax({ 
        		url: url, 
        		context: document.body, 
        		async: false,
        		data:data,
        		success: function(result){
        			if(result.success){
        				toPage(callbackurl,pageNo);
        				layer.closeAll();
        				result.msg(result.message,{icon:1});
        			}else{
        				layer.msg(result.message,{icon:5});
        			}
        		}
        	});
		});
}


//扩展jquery  用于使用post提交数据 且跳转页面
$.extend({
    StandardPost:function(url,args){
        var body = $(document.body),
            form = $("<form method='post'></form>"),
            input;
        form.attr({"action":url});
        $.each(args,function(key,value){
            input = $("<input type='hidden'>");
            input.attr({"name":key});
            input.val(value);
            form.append(input);
        });

        form.appendTo(document.body);
        form.submit();
        document.body.removeChild(form[0]);
    }
});

//扩展jquery  用于使用get提交数据 且刷新页面
$.extend({
    StandardGet:function(url,args){
    	var temp = '';
        $.each(args,function(key,value){
        	temp+='&'+key;
        	temp+='='+value
        });
        if(!!temp){
        	temp=temp.substr(1);
        	temp='?'+temp;
        }
        location.href = url+temp;
    }
});

//获取项目路径
function getContextPath(){   
    var pathName = document.location.pathname;   
    var index = pathName.substr(1).indexOf("/");   
    var result = pathName.substr(0,index+1);   
    return result;   
}  
var projectPath = getContextPath(); //全局变量的一个方法


//对Date的扩展，将 Date 转化为指定格式的String
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
//例子： 
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
var o = {
   "M+": this.getMonth() + 1, //月份 
   "d+": this.getDate(), //日 
   "h+": this.getHours(), //小时 
   "m+": this.getMinutes(), //分 
   "s+": this.getSeconds(), //秒 
   "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
   "S": this.getMilliseconds() //毫秒 
};
if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
for (var k in o)
if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
return fmt;
}


$.fn.serializeObject = function()    
{    
   var o = {};    
   var a = this.serializeArray(); 
   $.each(a, function() {
       if (o[this.name]) {    
           if (!o[this.name].push) {    
               o[this.name] = [o[this.name]];    
           }    
           o[this.name].push(this.value || '');    
       } else {
    	  
    	   '' != this.value ? o[this.name] = this.value : null;
           
       }    
   });    
   return o;    
};
